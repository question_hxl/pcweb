<br>

# 51教育服务
> 51教育服务官方网站

## 开发说明
> 1. npm install : 安装相关依赖,主要是gulp等构建工具

<br>

> 2. npm start: 运行服务，可在http://localhost:80访问

<br>

> 3. gulp watch : 修改样式实时监听 

<br>

> 4. gulp doc : 生成文档，成功之后可在http://localhost:8899查看文档

<br>

## 项目地址
> [https://gitee.com/question_hxl/pcweb](https://gitee.com/question_hxl/pcweb)