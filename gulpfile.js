/**
 * Created by 贺小雷 on 2016/10/14.
 */
var gulp  = require('gulp'),
    minifycss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    jsDoc = require('gulp-jsdoc3'),
    connect = require('gulp-connect');

var cssFiles = [
    'app/assets/css/*.css',
    'app/directive-extend/css/*.css',
    '!app/assets/css/buildAnswerSheet.css'
];

var cssLibs = [
    'app/assets/script/bootstrap-3.3.4/css/bootstrap.min.css',
    'app/bower_components/font-awesome/css/font-awesome.min.css',
    'app/dist/css/AdminLTE.min.css',
    'app/dist/css/hover.css',
    'app/dist/css/animate.min.css',
    'app/bower_components/rating/jquery.raty.css',
    'app/bower_components/angular-loading-bar/build/loading-bar.css',
    'app/bower_components/ngDialog/ngDialog.css',
    'app/bower_components/ngDialog/ngDialog-theme-plain.css',
    'app/bower_components/ngDialog/ngDialog-theme-default.css'
];

var docCfg = {
    "tags": {
        "allowUnknownTags": true
    },
    "opts": {
        "destination": "./docs/gen"
    },
    "plugins": [
        "plugins/markdown"
    ],
    "templates": {
        "cleverLinks": false,
        "monospaceLinks": false,
        "default": {
            "outputSourceFiles": true
        },
        "path": "ink-docstrap",
        "theme": "cerulean",
        "navType": "vertical",
        "linenums": true,
        "dateFormat": "MMMM Do YYYY, h:mm:ss a"
    }
};

gulp.task('minifycss', function() {
    return gulp.src(cssFiles)
        .pipe(concat('dist.css'))
        .pipe(gulp.dest('app/assets/dist-css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('app/assets/dist-css'));
});

gulp.task('minify-lib-css', function(){
    return gulp.src(cssLibs)
        .pipe(concat('lib.css'))
        .pipe(gulp.dest('app/assets/dist-lib'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('app/assets/dist-lib'));
});

gulp.task('clean', function(cb) {
    return del(['app/assets/dist-css/*'], cb);
});

gulp.task('cleanDoc', function(cb){
    return del(['docs/*'], cb);
});

gulp.task('doc',['cleanDoc'], function(cb){
    gulp.src(['README.md', 'app/service/api/api.common.js'],{read: false})
        .pipe(jsDoc(docCfg, function(){
            connect.serverClose();
            connect.server({
                port: 8899,
                root: 'docs/gen',
                index: 'index.html'
            });
        }));
});


gulp.task('default', ['clean'], function() {
    gulp.start('minifycss');
});

gulp.task('watch', function(){
    gulp.watch(cssFiles, ['default']);
});
