/**
 * Created by 贺小雷 on 2017/3/8.
 */
'use strict';

const Path = require('path');
const Hapi = require('hapi');

//默认配置代理地址，测试设置为192.168.1.108
const config = {
    PROXY_HOST: '51.lyclass.com',
    PROXY_PORT: 80,
    LOCAL_PORT: 80,
    HOME_HTML: 'index.html'
};

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'app')
            }
        }
    }
});
server.connection({ port: config.LOCAL_PORT });

server.register([require('inert'), require('h2o2')], function (err) {

    if (err) {
        throw err;
    }

    server.route({
        method: 'GET',
        path: '/',
        config: {
            handler: function(request, reply) {
                var path = Path.join(__dirname, 'app', config.HOME_HTML);
                reply.file(path);
            }
        }
    });

    server.route({
        method: '*',
        path: '/{param*}',
        config: {
            handler: function(request, reply) {
                if(request.params.param.indexOf('.ashx') >=0) {
                    return reply.proxy({
                        host: config.PROXY_HOST,
                        port: config.PROXY_PORT,
                        protocol: 'http',
                        passThrough : true
                    });
                }else if((request.params.param.indexOf('api/') === 0 || request.params.param.indexOf('/api/') === 0) && request.params.param.indexOf('.ashx') < 0){
                    console.log(request.params.param);
                    return reply.proxy({
                        host: 'api.51jyfw.com',
                        port: '83',
                        protocol: 'http',
                        passThrough : true
                    });
                }else {
                    var path = Path.join(__dirname, 'app', request.params.param);
                    reply.file(path);
                }
            },
            payload: {
                output: 'data',
                parse: false,
                maxBytes: 100000000
            }
        }
    });

    server.start(function (err) {
        if (err) {
            throw err;
        }
        console.info('Server running at:', server.info.uri);
    });
});
