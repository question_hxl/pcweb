define([], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$sce', 'ngDialog', function ($http, $scope, $rootScope, $state, $sce, ngDialog) {

        //获取班级列表
        $scope.noClassdata = false;
        $http.get($rootScope.baseUrl + '/Interface0027.ashx').success(function (data) {
            if (data && data.msg.length != 0) {
                $scope.noClassdata = false;
                $scope.classId = data.msg[0].classId
                $scope.classList = data.msg;
                $scope.className = data.msg[0].name;//绑定当前班级
                getStudentList($scope.classId);
            } else {
                //无班级信息
                $scope.noClassdata = true;
                ngDialog.open({
                    template:
                        '<p>暂无班级数据!</p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                    plain: true,
                    className: 'ngdialog-theme-plain'
                });
                return false;
            }
        });

        //tab切换班级
        $scope.tabClass = function (curclass) {
            $scope.classId = curclass.classId;
            $scope.className = curclass.name;
            getStudentList($scope.classId);
        };

        //获取学生list
        $scope.noStudata = false;
        var getStudentList = function (classId) {
            $http({
                method: 'post',
                url: $rootScope.baseUrl + '/Interface0249.ashx',
                data: {
                    classFLnkId: classId,
                }
            }).success(function (data) {
                if (data && _.isArray(data.msg) && data.msg.length != 0) {
                    $scope.noStudata = false;
                    $scope.stuList = data.msg;
                    var stuLen = data.msg.length,
                        halfLen = Math.ceil(stuLen / 2);
                    $scope.stuList_right = data.msg.slice(0, halfLen);
                    $scope.stuList_left = data.msg.slice(halfLen, stuLen);
                    $('.right .anTab ul .searchstu').css('margin', '1%');
                } else {
                    //无学生信息
                    $scope.noStudata = true;
                }
            });
        };

        //跳转个人信息
        $scope.gostuInfo = function (Sfid) {
            $state.go('myApp.capacity', {
                Sfid: Sfid,
                ClassFlnkid: $scope.classId,
                className: $scope.className
            });
        };

        $scope.checkStuErrorBook = function(stu){
            console.log(stu);
            window.open("#/stu-error-book?studentId=" + stu.userFLnkId + '&isOpenNewWin=' +1);
            // $state.go('stuErrorBook', {
            //     studentId: stu.userFLnkId
            // });
        }
    }]
});

