/// <reference path="test_allknolpoint.js" />
define(['underscore-min', 'outexcel'], function (_, outExcel) {
	return ['$http', '$scope', '$rootScope', '$state', '$sce', 'ngDialog', '$location',function ($http, $scope, $rootScope, $state, $sce, ngDialog, $location) {
		var isallknolpoint = $location.path();
		if (isallknolpoint == '/test_allknolpoint') {
			$("#shares").hide();
		} else {
			$("#shares").fadeIn();
		}

        $scope.exportExcel = function(table, name){
            outExcel.exportTable(table, name);
        };

		//从班级分析点击过来
		var params = $location.search();
		var classId = params.ClassFlnkid;
		$scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
		var subjectName = $scope.currentUser.subjectName,
			schoolName = $scope.currentUser.schoolName;

		// 获取老师班级
		$http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function (data) {
			if (!!data && data.msg) {
				$scope.classList = data.msg;
				$scope.currentClassId = $scope.classList[0].classId;
				$scope.clasName = $scope.classList[0].name;
				getKnowledgeList();
			} else {//无班级数据

			}
		});

		// tips
		$scope.moveOver = function () {
			$("[data-toggle='tooltip']").tooltip({
				html: true
			});
		}

		//tab点击切换
		$scope.selectSubject = function (classInfo) {
			$scope.clasName = classInfo.name;
			$scope.currentClassId = classInfo.classId;
			getKnowledgeList();
		};

		//  根据班级ID获取知识点
		$scope.noStudata = false;
		$scope.noklginfo = false;
		var getKnowledgeList = function () {
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0267.ashx',
				data: {
					ClassFlnkID: $scope.currentClassId || classId
				}
			}).success(function (data) {
				if (!!data && data.code != 2) {
					$scope.noklginfo = false;
					$scope.KnowledgeList = data.msg;

					$http({//获取学生列表
						method: 'post',
						url: $rootScope.baseUrl + '/Interface0233.ashx',
						data: {
							classFLnkId: $scope.currentClassId || classId
						}
					}).success(function (data) {
						if (data && data.msg) {
							$scope.noStudata = false;
							$scope.stuList = data.msg;
							$scope.schoolId = data.msg[0].studentName;
							$scope.keynames = [];
							$scope.keyIds = [];
							_.each($scope.stuList, function (val, key) {
								$scope.keynames.push(val.studentName);
								$scope.keyIds.push(val.studentFLnkId);
							});

							$scope.keynames.unshift('全班');
							$scope.stuLen = $scope.keynames.length;//控制显示列宽stuList.length 

							var list = _.map($scope.KnowledgeList, function (item) {
								var rates = [];
								_.each($scope.keynames, function (name) {
									var userInKlgList = _.find(item.MasterRate, function (rt) {
										return rt.UserName === name;
									});
									rates.push({
										UserName: name,
										rate: userInKlgList ? userInKlgList.rate : 0
									});
								});
								var zkRate = _.find(item.MasterRate, function (rt) {
									return rt.UserName === '中考概率';
								});
								return {
									KnowledgeName: item.KnowledgeName,
									MasterRate: rates,
									zkrate: zkRate && zkRate.rate || 0
								}
							});
							$scope.KnowledgeList = list;
						} else {
							//无学生信息
							$scope.noStudata = true;
						}
					});
				} else {  //无知识点数据
					$scope.noklginfo = true;
				}
			})
		};
		$scope.$on('repeat-render-finish', function () {
			setTimeout(function () {
				var $thead = $('table thead'), $tbody = $('table tbody');
				var originOffset = $thead.offset().top;
				var widths = [], vWidths = [], vHeight = [];
				_.each($thead.find('th'), function(item){
					widths.push($(item).width());
				});
				_.each($tbody.find('th'), function(item){
					vWidths.push($(item).width());
					vHeight.push($(item).height());
				});
				$('table').css({
					position: 'relative',
					overflow: 'hidden'
				});
				$(window).scroll(function () {
					var top = $(window).scrollTop();
					$tbody.find('th').each(function(index, item){
						$(item).width(vWidths[index]);
						$(item).height(vHeight[index]);
					});
					$thead.find('th').each(function(index, item){
						$(item).width(widths[index]);
					});
					if (top > originOffset) {
						$thead.css({
							position: 'absolute',
							top: top - originOffset + 'px',
							'z-index': '120'
						});
					}else {
						$thead.css({
							position: 'static',
							top: top - originOffset + 'px'
						});
					}

					if ($(this).scrollLeft() > 0) {
						$tbody.find('th').css({
							'position': 'absolute',
							'left': $(this).scrollLeft() + 'px',
							'z-index': '99'
						});
					} else {
						$tbody.find('th').css({
							'position': 'static',
							'left': 0
						});
					}
				});
			}, 1000)

		});
	}]
});

/* $scope.KnowledgeList = {//模拟数据，MasterRate掌握度百分比（遍历集合对象）
					0: {
						KnowledgeName: "平行线的性质",
						MasterRate: {
							"全班": "20", "张三": "100", "李四": "20", "王五": "10",
							"张三1": "100", "李四1": "20", "王五1": "10",
							"张三2": "100", "李四2": "20", "王五2": "10",
							"张三3": "100", "李四3": "20", "王五3": "10",
							"张三4": "100", "李四4": "20", "王五4": "10",
							"张三5": "100", "李四5": "20", "王五5": "10",
							"张三6": "100", "李四6": "20", "王五6": "10",
							"张三7": "100", "李四7": "20", "王五7": "10",
							"张三8": "100", "李四8": "20", "王五8": "10",
							"张三9": "100", "李四9": "20", "王五9": "10",
							"张三10": "100", "李四10": "20", "王五10": "10"
						},
						ZkRate: "70"
					},       
				};*/
