define([], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$sce', '$cookies',
		function ($http, $scope, $rootScope, $state, $sce,$cookies) {
    	var sessionId = $cookies.sessionId;
    	$scope.theme = config.theme;

    	$scope.checkReport = function(){
    		window.open('http://report.wxy100.com/#!/login?sessionId='+sessionId + '&isOpenNewWin=' + 1);
    	};
	}];
});