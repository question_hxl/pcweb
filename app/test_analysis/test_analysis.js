define(['jquery.dataTables', 'highcharts', 'echarts'], function (dataTables,highcharts,echarts) {
	return ['$http', '$scope', '$rootScope', '$state', '$sce', 'ngDialog', function ($http, $scope, $rootScope, $state, $sce, ngDialog) {
		$scope.$root.title = 'q1we';
		$scope.Isgrade = true; // 初始概要
		$scope.IsStudent = false; // 初始学生战斗力
		$scope.subject = "概要";
		$scope.IsLook = true;
		var user = angular.fromJson(sessionStorage.getItem('currentUser') || undefined);
		$scope.tabCick = function () {
			$scope.Isgrade = true;
			$scope.IsStudent = false;
			$scope.subject = "概要";
		}

		$scope.nodata = false;
		// 获取老师班级
		$http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function (data) {
			$scope.classList = data.msg;
			$scope.currentClass = $scope.classList[0].name;
		});

		// 根据班级ID 获取信息
		$scope.selectSubject = function (data, classId, name) {
			$scope.classId = classId;
			$scope.subject = data;
			$scope.Isgrade = false;
			$scope.IsStudent = true;
			$scope.noklginfo = false;
			//查看全部分析
			$scope.gotoallstu = function () {
				location.href = "#/analyClass?ClassFlnkid=" + classId;
			}

			//查看全部知识点
			$scope.getallklgpoint = function () {
				location.href = "#/test_allknolpoint?ClassFlnkid=" + classId;
			}

			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0152.ashx',
				data: {
					ClassFlnkid: classId
				}
			}).success(function (res) {
				//alert(res.msg.length);
				if (res.msg.length != 0) {
					$scope.ClassStudent = res.msg
				} else {
					$scope.IsStudent = false;
					$scope.nodata = true;
					ngDialog.open({
						template:
						'<p>无班级年级对比信息</p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					return false;
				}
			}).then(function () {
				// $http({ // 学生战斗力
				// 	method: 'post',
				// 	url: $rootScope.baseUrl + '/Interface0153.ashx',
				// 	data: {
				// 		ClassFlnkid: classId
				// 	}
				// }).success(function (data) {
				// 	var Proportion = [];
				// 	for (var i = 0; i < data.msg.length; i++) {
				// 		//build
				// 		var r = data.msg[i];
				// 		Proportion.push([r.PartName, Number(r.Proportion), r.Section]);
				// 	}
				// 	// 学生战斗力
				// 	$('#container').highcharts({
				// 		chart: {
				// 			plotBackgroundColor: null,
				// 			plotBorderWidth: null,
				// 			plotShadow: false
				// 		},
				// 		title: {
				// 			text: null
				// 		},
				// 		tooltip: {
				// 			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				// 		},
				// 		exporting: {
				// 			enabled: false
				// 		},
				// 		plotOptions: {
				// 			pie: {
				// 				size: '50%',
				// 				allowPointSelect: true,
				// 				cursor: 'pointer',
				// 				dataLabels: {
				// 					enabled: true,
				// 					color: '#000000',
				// 					connectorColor: '#000000',
				// 					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				// 				}
				// 			}
				// 		},
				// 		series: [{
				// 			type: 'pie',
				// 			name: '学习力',
				// 			data: Proportion
				// 		}],
				// 		lang: {
				// 			noData: "无数据"
				// 		},
				// 		noData: {
				// 			position: {
				// 				align: 'center'
				// 			},
				// 			attr: {
				// 				'stroke-width': 1,
				// 				stroke: '#cccccc'
				// 			},
				// 			style: {
				// 				fontWeight: 'bold',
				// 				fontSize: '20px',
				// 				color: '#F8710E'
				// 			}
				// 		},
				// 		credits: {
				// 			enabled: false,
				// 		},
				// 		legend: {
				// 			enabled: true
				// 		}
				// 	});
				// })
				$http.post($rootScope.baseUrl + '/generalQuery.ashx', {
					Proc_name: 'GetGradeClasss',
					SchoolFid: user.schoolFId,
					GradNum: user.GradeNo
				}).then(function (res) {
					$scope.gradeClassList = res.data.msg;
					$scope.gradeClassList = _.filter($scope.gradeClassList,function (resa) {
						return  resa.FLnkID !== classId;
					});
					$scope.gradeClassList = _.sortBy($scope.gradeClassList,'SmallNum');
					$scope.gradeClassList.unshift({ClassName:'请选择',Avgsc : '0'});
					$scope.selectClass = $scope.gradeClassList[0];
				});
				$http.post($rootScope.baseUrl + '/Interface0156.ashx', {
					ClassFlnkid: classId
				}).success(function (data) {
					$scope.testList = data.msg;
					if (_.isArray($scope.testList) && $scope.testList.length) {
						$scope.noExam = true;
						$scope.currentExamFlnk = $scope.testList[0];
						$scope.avgScore = [];
						$scope.avgGradeScore = [];
						$scope.datax = [];
						$scope.testList.reverse();
						_.each($scope.testList,function (item) {
							$scope.avgScore.push(item.Avgsc);
							$scope.avgGradeScore.push(item.GAvgsc);
							$scope.datax.push((item.ExamDate).substring(5, 10));
						});
						getEcharts()
					} else {
						$scope.noExam = false;
					}
				})
					.then(function () { // 战斗力排名
						$http({
							method: 'post',
							url: $rootScope.baseUrl + '/Interface0154.ashx',
							data: {
								ClassFlnkid: classId
							}
						}).success(function (res) {

							var t1 = [], t2 = [], t3 = [], t4 = [];
							for (var i = 0 ; i < res.msg.length; i++) {
								if (res.msg[i].Type == '1') {  //战斗力前五名
									var up_down = 0;
									if (parseInt(res.msg[i].ChangeNum) === 0) {
										up_down = res.msg[i].ChangeNum;
									} else if (parseInt(res.msg[i].ChangeNum) > 0) {
										up_down = '↑' + res.msg[i].ChangeNum.replace('-', '') + '';
									} else if (parseInt(res.msg[i].ChangeNum) < 0) {
										up_down = '↓' + res.msg[i].ChangeNum.replace('-', '') + '';
									}
									t1.push({
										'UserName': res.msg[i].UserName,
										'Serial': res.msg[i].Serial,
										'ChangeNum': res.msg[i].ChangeNum,
										'up_down': up_down,
										'Pows': res.msg[i].Pows,
										'UserFlnkID': res.msg[i].UserFlnkID
									})
								} else if (res.msg[i].Type == '2') { // 战斗力后五名
									var up_down = 0;
									if (parseInt(res.msg[i].ChangeNum) === 0) {
										up_down = res.msg[i].ChangeNum;
									} else if (parseInt(res.msg[i].ChangeNum) > 0) {
										up_down = '↑' + res.msg[i].ChangeNum.replace('-', '') + '';
									} else if (parseInt(res.msg[i].ChangeNum) < 0) {
										up_down = '↓' + res.msg[i].ChangeNum.replace('-', '') + '';
									}
									t2.push({
										'UserName': res.msg[i].UserName,
										'Serial': res.msg[i].Serial,
										'ChangeNum': res.msg[i].ChangeNum,
										'up_down': up_down,
										'Pows': res.msg[i].Pows,
										'UserFlnkID': res.msg[i].UserFlnkID
									})
								} else if (res.msg[i].Type == '3') { // 成绩提升前五名
									var up_down = 0;
									if (parseInt(res.msg[i].ChangeNum) === 0) {
										up_down = res.msg[i].ChangeNum;
									} else if (parseInt(res.msg[i].ChangeNum) > 0) {
										up_down = '↑' + res.msg[i].ChangeNum.replace('-', '') + '';
									} else if (parseInt(res.msg[i].ChangeNum) < 0) {
										up_down = '↓' + res.msg[i].ChangeNum.replace('-', '') + '';
									}
									t3.push({
										'UserName': res.msg[i].UserName,
										'Serial': res.msg[i].Serial,
										'ChangeNum': res.msg[i].ChangeNum,
										'up_down': up_down,
										'UserFlnkID': res.msg[i].UserFlnkID,
										'Pows': res.msg[i].Pows
									})
								} else if (res.msg[i].Type == '4') { // 成绩下降后五名
									var up_down = 0;
									if (parseInt(res.msg[i].ChangeNum) === 0) {
										up_down = res.msg[i].ChangeNum;
									} else if (parseInt(res.msg[i].ChangeNum) > 0) {
										up_down = '↑' + res.msg[i].ChangeNum.replace('-', '') + '';
									} else if (parseInt(res.msg[i].ChangeNum) < 0) {
										up_down = '↓' + res.msg[i].ChangeNum.replace('-', '') + '';
									}
									t4.push({
										'UserName': res.msg[i].UserName,
										'Serial': res.msg[i].Serial,
										'ChangeNum': res.msg[i].ChangeNum,
										'up_down': up_down,
										'UserFlnkID': res.msg[i].UserFlnkID,
										'Pows': res.msg[i].Pows
									})
								}
							}

							$scope.t1List = t1;
							$scope.t2List = t2;
							$scope.t3List = t3;
							$scope.t4List = t4;

							//跳转个人信息
							$scope.gothisstuinfo = function (Sfid) {
								location.href = "#/capacity?Sfid=" + Sfid + "&ClassFlnkid=" + classId + "";
							}
						})
					});
				//知识点
				//知识点接口调用
				//$('#stuAnalysis').dataTable().fnDestroy();
				//知识点table绘制,
				//{ "mDataProp": "ExamRate", "sWidth": "10%" }
				//{
				//	"aTargets": [6], "mRender": function (data, type, full) {
				//		return '<div class="gay">' + data + '%</div>';;
				//	}
				//},
				// 学生信息列表
				//调用学生信息接口
				$('#stuAnalysis').dataTable().fnDestroy();
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0164.ashx',
					data: {
						ClassFlnkid: classId
					}
				}).success(function (data) {
					var tablesort = data.msg;
					$scope.stuAnalysistable.fnAddData(tablesort);
				});
				//绘制学生信息表格
				$scope.stuAnalysistable = $('#stuAnalysis').dataTable({
					"aoColumns": [{
						"mDataProp": "Order",
						"sWidth": "14%"
					}, {
						"mDataProp": "Name",
						"sWidth": "15%"
					}, {
						"mDataProp": "Pow",
						"sWidth": "14%"
					}, {
						"mDataProp": "LastExamSc",
						"sWidth": "14%"
					}, {
						"mDataProp": "ActiveDegree",
						"sWidth": "14%"
					}, {
						"mDataProp": "CompensateNum",
						"sWidth": "14%"
					}, {
						"mDataProp": "UserFLnkID",
						"sWidth": "15%"
					}],
					"aoColumnDefs": [
						{
							"aTargets": [6], "mRender": function (data, type, full) {
							return '<a class="homeviewbtnana" href="#/capacity?Sfid='+data+'&ClassFlnkid='+classId+'">查看详细</a>'
						}
						},
						{
							"aTargets": [5], "mRender": function (data, type, full) {
							return data;
						}
						},
						{
							"aTargets": [4], "mRender": function (data, type, full) {
							return data;
						}
						},
						{
							"aTargets": [3], "mRender": function (data, type, full) {

							return data;
						}
						},
						{
							"aTargets": [2], "mRender": function (data, type, full) {
							return data;
						}
						},
						{
							"aTargets": [1], "mRender": function (data, type, full) {
							return '<a href="#/capacity?Sfid=' + full.UserFLnkID + '&ClassFlnkid=' + classId + '">' + data + '</a>';
						}
						},
						{
							"aTargets": [0], "mRender": function (data, type, full) {
							return data;
						}
						}
					],
					"oLanguage": {
						"sProcessing": "处理中...",
						"sLengthMenu": "显示 _MENU_ 项结果",
						"sZeroRecords": "没有匹配结果",
						"sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
						"sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
						"sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
						"sInfoPostFix": "",
						"sSearch": "搜索：",
						"sUrl": "",
						"sEmptyTable": "表中数据为空",
						"sLoadingRecords": "载入中...",
						"sInfoThousands": ",",
						"oPaginate": {
							"sFirst": "首页",
							"sPrevious": "上页",
							"sNext": "下页",
							"sLast": "末页"
						}
					},
					"bDestroy": true,//允许重新加载表格对象数据
					"bRetrieve": true,
					"bProcessing": false,
					"bLengthChange": false,
					"bPaginate": false,
					"bSort": true //排序功能
				});
				$('#stuAnalysis').dataTable().fnClearTable();
			});
		};

		function getEcharts(){
			$http.post($rootScope.baseUrl + '/Interface0160.ashx', {
				ClassFlnkid: $scope.classId,
				ExamFlnkID: $scope.currentExamFlnk.ExamFlnkID
			}).success(function (data) {
				$scope.pie = _.filter(data.msg, function (item) {
					return item.TypeID === '1';
				});
				$scope.datapie = [];
				_.each($scope.pie, function (item) {
					if (item.fScores !== "0.0") {
						$scope.datapie.push({'value': '' + item.fScores + '', 'name': '' + item.Title + ''})
					}
				});
				echarts.init(document.getElementById('container')).setOption({
					tooltip: {
						trigger: 'item',
						formatter: "{a} <br/>{b}: {d}%"
					},
					legend: {
						x: 'left',
						y: 'center',
						orient: 'vertical',
						data: $scope.datapie
					},
					calculable: true,
					series: [{
						name: '学生成绩分布',
						type: 'pie',
						radius: '65%',
						center: ['60%', '45%'],
						color: ["#5DC7C7", "#989CFF", "#F95659", "#3F80C4", "#8CC43F"],
						// labelLine: {normal: {show: false}},
						itemStyle: {
							normal: {
								label: {
									show: true,
									formatter: function (d) {
										return d.percent.toFixed(0) + '%';
									},
									// position: 'inner',
									textStyle: {
										fontSize: '10'
									}
								}
							}
						},
						data: $scope.datapie
					}]
				});
				getEcharts2();
			})
		}

		function getEcharts2() {
			if($scope.testList !== undefined){
				var myChart = echarts.init(document.getElementById('containers'));
				var option = {
					tooltip: {
						trigger: "item",
						formatter: "{a} <br/>{b} : {c}"
					},
					legend: {
						orient: 'horizontal',
						x: 'center',
						data: ["年级平均成绩","班级平均成绩"]
					},
					// grid:{y:100},
					xAxis: [
						{
							type: "category",
							name: "x",
							splitLine: {show: false},
							data: $scope.datax,
							axisLabel:{
								show: true,
								rotate: 45,
								interval:0,
								margin:5
							}
						}
					],
					yAxis: [
						{
							type: "value",
							name: "y"
						}
					],
					calculable: true,
					series: [
						{
							name: "年级平均成绩",
							type: "line",
							data: $scope.avgGradeScore,
							itemStyle: {
								normal: {
									color: '#ff4c79',
									lineStyle: {
										color: '#ff4c79'
									}
								}
							}

						},
						{
							name: "班级平均成绩",
							type: "line",
							data: $scope.avgScore,
							itemStyle: {
								normal: {
									color: '#2dc0e8',
									lineStyle: {
										color: '#2dc0e8'
									}
								}
							}

						},
						{
							name: "对比班级平均成绩",
							type: "line",
							data: $scope.avgScore1,
							itemStyle: {
								normal: {
									color: '#F49E2F',
									lineStyle: {
										color: '#F49E2F'
									}
								}
							}

						}
					]
				};
				if($scope.selectClass.ClassName !== '请选择'){
					option.legend.data.push('对比班级平均成绩');
				}
				if($scope.testList.length <= 18){
					option.xAxis[0].axisLabel.rotate = 0;
				}
				myChart.setOption(option);
			}
		}

		$scope.$watch('selectClass',function (val) {
			if($scope.selectClass !== undefined){
				if($scope.selectClass.ClassName !== '请选择'){
					$http.post($rootScope.baseUrl + '/Interface0156.ashx', {
						ClassFlnkid: val.FLnkID
					}).success(function (data) {
							$scope.testList1 = [];
							$scope.avgScore1 = [];
							data.msg.reverse();
							_.each($scope.testList,function (item,index) {
								_.each(data.msg,function (itema) {
									if(item.ExamFlnkID ===itema.ExamFlnkID){
										$scope.testList1[index] = itema;
										return
									}
								});
								if($scope.testList1[index] === undefined){
									$scope.testList1[index] = item;
									$scope.testList1[index].Avgsc = '0';
								}
							});
							_.each($scope.testList1, function (item) {
								$scope.avgScore1.push(item.Avgsc);
							});
							getEcharts2();
					}
					);
				}else {
					_.each($scope.testList1, function (item,index) {
						$scope.avgScore1[index] = '';
					});
					getEcharts2();
				}
			}
		});
		//概要
		$('#summary').dataTable().fnDestroy();
		//概要接口调用
		$http.get($rootScope.baseUrl + '/Interface0151.ashx').success(function (data) {
			$scope.summary = data.msg;

			// var summary = data.msg;
			// $.each(summary, function (index, item) {
			// 	if (item.ClassName === '全年级') {
			// 		item.index = '';
			// 	} else {
			// 		item.index = index;
			// 	}
			// });
			// $scope.summarytable.fnAddData(summary);
		});
		//概要表格绘制
		// $scope.summarytable = $('#summary').dataTable({
		// 	"aoColumns": [
		// 		{ "mDataProp": "index", "sWidth": "8%" },
		// 		{ "mDataProp": "ClassName", "sWidth": "8%", "sClass": "f_black" },
		// 		{ "mDataProp": "Power", "sWidth": "10%", "sClass": "f_blue" },
		// 		{ "mDataProp": "AvgSc", "sWidth": "10%", "sClass": "f_red" },
		// 		{ "mDataProp": "GoodNum", "sWidth": "12%" },
		// 		{ "mDataProp": "PassRate", "sWidth": "10%" },
		// 		{ "mDataProp": "UpNum", "sWidth": "12%" },
		// 		{ "mDataProp": "DownNum", "sWidth": "12%" },
		// 		{ "mDataProp": "ClassFlnkid", "sWidth": "18%" }
		// 	],
		// 	"aoColumnDefs": [
		// 		{
		// 			"aTargets": [8], "mRender": function (data, type, full) {
		// 			if (full.IsOwner == 1) {
		// 				//return bind2Scope('<div class="homeviewbtnana">查看详细</div>');
        //
		// 				//return this.html('<div class="homeviewbtnana" ng-click="selectSubject(full.ClassName,full.ClassFlnkid)">查看详细</div>');
		// 			} else {
		// 				if (full.PowerOrder != 0) {
		// 					return '<span>非执教班，无法查看</span>';
		// 				} else {
		// 					return '';
		// 				}
		// 			}
        //
		// 		}
		// 		},
		// 		{
		// 			"aTargets": [5], "mRender": function (data, type, full) {
		// 			return data + "%";
		// 			//进度条展示示例
		// 			//return data + "%<div class=\"progress\"><div aria-valuetransitiongoal=\"" + data + "\" class=\"progress-bar bg-color-teal\" style=\"width: " + data + "%;\" aria-valuenow=\"" + data + "\"> </div></div>";
		// 		}
		// 		}
		// 	],
		// 	"oLanguage": {
		// 		"sProcessing": "处理中...",
		// 		"sLengthMenu": "显示 _MENU_ 项结果",
		// 		"sZeroRecords": "没有匹配结果",
		// 		"sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
		// 		"sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
		// 		"sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
		// 		"sInfoPostFix": "",
		// 		"sSearch": "搜索：",
		// 		"sUrl": "",
		// 		"sEmptyTable": "表中数据为空",
		// 		"sLoadingRecords": "载入中...",
		// 		"sInfoThousands": ",",
		// 		"oPaginate": {
		// 			"sFirst": "首页",
		// 			"sPrevious": "上页",
		// 			"sNext": "下页",
		// 			"sLast": "末页"
		// 		}
		// 	},
		// 	"bDestroy": true,//允许重新加载表格对象数据
		// 	"bRetrieve": true,
		// 	"bProcessing": false,
		// 	"bLengthChange": false,
		// 	"bPaginate": false,
		// 	"bAutoWidth": true,
		// 	"bFilter": false,//禁用搜索
		// 	"bLengthChange": false//禁用分页显示
		// });

		// $('#summary').dataTable().fnClearTable();

		// tips
		$scope.moveOver = function () {
			$("[data-toggle='tooltip']").tooltip({
				html: true
			});
		};
	}]
});

