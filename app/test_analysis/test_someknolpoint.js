define(['jquery.dataTables'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$sce', 'ngDialog', function ($http, $scope, $rootScope, $state, $sce, ngDialog) {

        // 获取老师班级
        $scope.noClaData = false;
        $http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function (data) {
            if (data && !!data.msg && data.msg.length != 0) {
                $scope.noClaData = false;
                $scope.classList = data.msg;
                $scope.curClaName = $scope.classList[0].name;
                $scope.classId = $scope.classList[0].classId;
                getSomeklgName($scope.classId);
            } else {
                $scope.noClaData = true;
            }
        });

        //查看全部知识点
        $scope.getallklgpoint = function () {
            var url = "#/test_allknolpoint?ClassFlnkid=" + $scope.classId + '&isOpenNewWin=' + 1;
            window.open(url);
        }

        // 根据班级ID 获取信息 
        $scope.selectSubject = function (curclass) {
            $scope.classId = curclass.classId;
            $scope.curClaName = curclass.name;
            getSomeklgName($scope.classId);
        };

        //  知识点
        $scope.noklginfo = false;
        var getSomeklgName = function (classId) {
            $http({
                method: 'post',
                url: $rootScope.baseUrl + '/Interface0155.ashx',
                data: {
                    ClassFlnkid: classId
                }
            }).success(function (data) {
                if (data.msg != '') {
                    $scope.noklginfo = false;
                    var KnowledgeList = data.msg;
                    $scope.KnowledgeList.fnAddData(KnowledgeList);
                } else {
                    $scope.noklginfo = true;
                }
            });

            //知识点table绘制
            $scope.KnowledgeList = $('#klgAnalysisSmall').dataTable({
                "aoColumns": [
				{ "mDataProp": "KnowledgeName", "sWidth": "29%" },
				{ "mDataProp": "CoverRate", "sWidth": "10%" },
				{ "mDataProp": "MasterRate", "sWidth": "10%" },
				{ "mDataProp": "GradeMasterRate", "sWidth": "15%" },
				{ "mDataProp": "CheckTimes", "sWidth": "12%" },
				{ "mDataProp": "CompensateTimes", "sWidth": "12%" },
				{ "mDataProp": "ExamRate", "sWidth": "12%" }
                ],
                "aoColumnDefs": [
					{
					    "aTargets": [6], "mRender": function (data, type, full) {
					        return '<div class="percentDate">' + data + '%</div>';
					    }
					},
					{
					    "aTargets": [3], "mRender": function (data, type, full) {
					        return data + '%';
					    }
					},
					{
					    "aTargets": [2], "mRender": function (data, type, full) {
					        return data + '%';
					    }
					},
					{
					    "aTargets": [1], "mRender": function (data, type, full) {
					        return data + '%';
					    }
					},
					{
					    "aTargets": [1], "mRender": function (data, type, full) {
					        return '<div data-toggle="tooltip" data-placement="right" ng-mouseover="moveOver()" class="t_overflow" title="' + data + '">' + data + '</div>';
					    }
					}
                ],
                //"fnRowCallback": function (nRow, aData, iDisplayIndex) {
                //	$("td:first", nRow).html(iDisplayIndex + 1);//设置序号位于第一列，并顺次加一
                //	return nRow;
                //},
                "oLanguage": {
                    "sProcessing": "处理中...",
                    "sLengthMenu": "显示 _MENU_ 项结果",
                    "sZeroRecords": "没有匹配结果",
                    "sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
                    "sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
                    "sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
                    "sInfoPostFix": "",
                    "sSearch": "搜索：",
                    "sUrl": "",
                    "sEmptyTable": "表中数据为空",
                    "sLoadingRecords": "载入中...",
                    "sInfoThousands": ",",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "上页",
                        "sNext": "下页",
                        "sLast": "末页"
                    }
                },
                "bDestroy": true,//允许重新加载表格对象数据
                "bRetrieve": true,
                "bProcessing": false,
                "bPaginate": false,
                "bAutoWidth": true,
                "bFilter": false,//禁用搜索
                "bLengthChange": false,//禁用分页显示
                "bSort": false
            });
            $('#klgAnalysisSmall').dataTable().fnClearTable();

        };

        // tips
        $scope.moveOver = function () {
            $("[data-toggle='tooltip']").tooltip({
                html: true
            });
        }
    }]
})

