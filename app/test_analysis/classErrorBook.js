/**
 * Created by zy on 2017/5/25 0025.
 */
define(['jquery.datetimepicker'], function () {
    return ['$scope', '$http', '$location', '$rootScope', '$q', 'constantService', 'paperService', 'Upload', 'resourceUrl','apiCommon',
        function ($scope, $http, $location, $rootScope, $q, constantService, paperService, Upload, resourceUrl, apiCommon) {

            var user = JSON.parse(sessionStorage.getItem('currentUser'));
            $scope.isShowAnswer = true;
            $scope.checkResult = false;
            $scope.startErrorRate = 40;

            $http.post($rootScope.baseUrl + '/Interface0219A.ashx',{
                gradeId: user.GradeId,
                subjectId: user.subjectId
            }).success(function (res) {
                if (_.isArray(res.msg)) {
                    $scope.questionTypeList = res.msg;
                    $scope.questionTypeList = _.filter($scope.questionTypeList, function (item) {
                        return item.qtypeName !== '';
                    });
                    $scope.questionTypeList.unshift({
                        qtypeName: '全部'
                    });
                    $scope.curQuestionType = $scope.questionTypeList[0];
                    getPeriodList();
                }else {
                    $scope.questionTypeList = [];
                }
            });
            function getPeriodList() {
                // $http.get(resourceUrl +'/BaseUser/GetPeriod?graderNum='+ user.GradeNo.toString()).success(function (res) {
                //     var period = +res.ResultObj;
                //     $scope.periodList = [
                //         {name: period},
                //         {name: period - 1},
                //         {name: period - 2}
                //     ];
                //     $scope.curPeriod = $scope.periodList[0];
                //     classList();
                // });
            }
            classList();
            function classList() {
                $http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function (res) {
                    if(_.isArray(res.msg)){
                        $scope.ClassList = res.msg;
                        var classDetailList;
                        $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                            Proc_name: 'GetGradeClasss',
                            SchoolFid: user.schoolFId,
                            GradNum: user.GradeNo
                        }).then(function (rep) {
                            if(_.isArray(rep.data.msg)){
                                classDetailList = rep.data.msg;
                                _.each($scope.ClassList, function (item) {
                                    _.each(classDetailList, function (detailItem) {
                                       if(item.classId === detailItem.FLnkID) {
                                           item.Period = detailItem.Period
                                       }
                                    });
                                    item.ClassName = item.name;
                                    item.FLnkID = item.classId;
                                });
                                $scope.ClassList.unshift({
                                    ClassName: '全年级',
                                    FLnkID: ''
                                });
                                $scope.curClass = $scope.ClassList[0];
                                //$scope.searchErrorBook();
                            }
                        });
                    }
                });
            }

            $scope.selectQuestionType = function (item) {
                $scope.curQuestionType = item;
                //$scope.searchErrorBook();
            };
            $scope.$watch('curClass',function (newValue,oldValue) {
                if(newValue === oldValue || newValue === undefined || oldValue === undefined){
                    return;
                }
               // $scope.searchErrorBook();
            });
            //TODO 获取错题 支持分页获取 如果获取全部则不传试卷id，只传页数
            function getErrorQuestion() {
                $scope.paperList = [];
                var qtypeName = $scope.curQuestionType.qtypeName;
                if(qtypeName === '全部') {
                    qtypeName = ''
                }
                if(!$scope.curClass.FLnkID) {
                    $scope.curClass.Period = $scope.ClassList[1].Period;
                }
                var param = {
                    Period: $scope.curClass.Period,
                    SchoolFlnkID: user.schoolFId,
                    SubjectFlnkID: user.subjectId,
                    GradeNum: user.GradeNo,
                    ClassFlnkId: $scope.curClass.FLnkID,
                    ExamEndTime: $('#end_datetimepicker').val() || '',
                    ExamBeginTime: $('#start_datetimepicker').val() || '',
                    // CorrectMinRate: (100 - +$scope.startErrorRate) || '',
                    CorrectMaxRate: (100 - +$scope.startErrorRate) || '',
                    Qtype: qtypeName
                };
                $http.post(resourceUrl +'/Exam/GetUserExamLostQuestionView',param).success(function (res) {
                    if(res.Flag){
                        var kmQuestions = res.ResultObj.kmQuestions,
                            baseUserExamLostQuestion = res.ResultObj.baseUserExamLostQuestion;
                        _.each(baseUserExamLostQuestion, function (item) {
                            item.Answer = item.answer;
                            item.OptionOne = item.option_a;
                            item.OptionTwo = item.option_b;
                            item.OptionThree = item.option_c;
                            item.OptionFour = item.option_d;
                            item.orders = item.Distance;
                        });
                        if(kmQuestions === null) {
                            $scope.paperList = baseUserExamLostQuestion;
                        } else {
                            _.each(kmQuestions, function (parent) {
                                var qstDetail = [];
                                parent.ExExamList = [];
                                _.each(baseUserExamLostQuestion, function (detail) {
                                    if(parent.FLnkID === detail.parentId) {
                                        qstDetail.push(detail)
                                    }
                                });
                                if(qstDetail.length !== 0) {
                                    for (var i = 0, j = qstDetail.length; i < j; i++) {
                                        if (parent.ExExamList.indexOf(qstDetail[i].ExExamFlnkID) === -1) {
                                            parent.ExExamList.push(qstDetail[i].ExExamFlnkID);
                                        }
                                    }
                                    parent.question = qstDetail;
                                }
                            });
                            _.each(kmQuestions, function (qst) {
                                for (var i = 0; i < qst.ExExamList.length; i++) {
                                    qst.ExExamFlnkID = qst.ExExamList[i];
                                    qst.sub = _.filter(qst.question, function (item) {
                                        return item.ExExamFlnkID === qst.ExExamFlnkID;
                                    });
                                    _.each(qst.sub, function (item) {
                                        if(item.TaskSubOrder && item.Mode !== 'A'){
                                            qst.orders = item.orders;
                                            item.orders = item.TaskSubOrder
                                        }
                                    });
                                    qst.sub.sort(function (a, b) {
                                        return a.orders - b.orders;
                                    });
                                    qst.mode = qst.sub[0].Mode;
                                    qst.ExName = qst.sub[0].ExName;
                                    if(i !== 0){
                                        kmQuestions.push(qst);
                                    }
                                }
                            });
                            var qstAllList = _.filter(baseUserExamLostQuestion, function (item) {
                                return (item.parentId === '00000000-0000-0000-0000-000000000000' || item.parentId === null || item.parentId === '');
                            });
                            $scope.paperList = _.filter($scope.paperList, function (item) {
                                return item.ExExamFlnkID
                            });
                            $scope.paperList = kmQuestions.concat(qstAllList);
                        }
                    } else {
                        $scope.paperList = [];
                    }
                });
            }

            $scope.searchErrorBook = function () {
                var endTime = $('#end_datetimepicker').val(),
                    startTime = $('#start_datetimepicker').val();
                if (endTime && startTime && endTime < startTime) {
                    constantService.alert('结束时间需大于开始时间！');
                    return false;
                }
                if($scope.checkResult){
                    return false;
                }
                getErrorQuestion();
            };
            $scope.check = function () {
              if($scope.startErrorRate > 0 && $scope.startErrorRate<=100)  {
                  $scope.checkResult = false;
                  //$scope.searchErrorBook();
              }else {
                  $scope.checkResult = true;
              }
            };
            $scope.downloadErrorBook = function (val) {
                if($scope.paperList === undefined) {
                    return
                }else {
                    if($scope.paperList.length === 0){
                        return
                    }
                }
                var area = $('.main-container ul.error-qst-list').clone();
                var str = '<!DOCTYPE html><html>';
                str += '<head><meta charset="utf-8">';
                str += "<link type='text/css' rel='stylesheet' href='" + location.origin + "/errorBookDownload/exportErrorBookStyle.css'/>";
                str += "<link type='text/css' rel='stylesheet' href='" + location.origin + "/directive-extend/css/directive.css'/>";
                str += '</head>';
                str += '<body>';
                str += area.get(0).outerHTML;
                str += '</body></html>';
                apiCommon.exportHtml2Pdf({
                    content: str,
                    type: 'A4',
                    name: '错题本'
                }).then(function (res) {
                    if (res.data.code === 0) {
                        var link = $('#download-link');
                        link.attr('href', res.data.msg);
                        link.attr('download', '学生错题本.pdf');
                        link.find('#downloadlabel').click();
                    } else {
                        constantService.alert('下载错题本失败！');
                    }
                });
            };
            $('#start_datetimepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: "Y-m-d",
                todayButton: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    $('#start_datetimepicker').datetimepicker('hide');
                }
            });
            $('#end_datetimepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: "Y-m-d",
                todayButton: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    $('#end_datetimepicker').datetimepicker('hide');
                }
            });

        }];
});

