define([], function(){
    return function($scope, constantService){
        $scope.alert = function(){
            constantService.alert('hello');
        };
        $scope.modal = function(){
            constantService.modal({
                title: '请输入',
                scope: $scope,
                // template: '<div class="my-data" ng-click="hello()">你好</div>'
                templateUrl: 'test-page/static.Tmpl.html',
                type: 'confirm',
                cancelText: 'cancel',
                okText: 'ok',
                cancelFn: function(){
                    $scope.hello();
                },
                okFn: function(){
                    $scope.ok();
                }
            });
        };
        $scope.hello = function(){
            alert('hello');
        };
        $scope.ok = function(){
            constantService.alert('1111');
        }
    };
});