
define([ 'underscore-min', 'jquerymedia', 'ckeditor', 'dateFomart'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', 'constantService', 'bes',
        function ($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce, constantService, bes) {
        var search = $location.search();
        var unifiedId = search.unifiedId;
        var exName = search.exName;
        var isParent = search.isParent;
        var subjectName = search.subjectName;
        var efid = search.efid;
        var submitNum = search.submitNum;
        if (search.targetId){
            $http.post('' + '/BootStrap/EncryptUnified/getPaperConfig.ashx',{
                unifiedId:search.targetId
            }).then(function (res) {
                if (res.data.code == '0'){
                    $scope.modelId = res.data.msg.FID;
                }
            });
        }
        $scope.paperData = [];
        $scope.paperData.push({exam : exName,subjectName : subjectName,efid : efid,submitNum : submitNum});
        $scope.isCheckedAllRight = true;
        $scope.isCheckedAll = true;
        $scope.isChecked = true;
        $scope.selectContentList = [];
        $scope.selectContentLists = [];
        $scope.orginalList = [];
        $scope.targetList = [];


        //返回
            $scope.back = function () {
                $state.go('myApp.examCenter')
            };
            $scope.queueList = [
                {name: '首页', url: 'myApp.masterHome'},
                {name: '考试中心', url: 'myApp.examCenter'},
                {name: 'ocr提交', url: 'myApp.submitRefer'}];

            /*bes.send('/BootStrap/schoolmanager/getMyUnifieds.ashx', {
                targetId: unifiedId,
                isParent: isParent
            },{
                method : 'post'
            }).then(function (res) {
                if (_.isArray(res.data.msg)){
                    $scope.examlist = res.data.msg;
                    _.each($scope.examlist,function (value,key) {
                        $scope.unitTask = $scope.examlist[key].unitTasks;
                        _.each($scope.unitTask,function (value,key) {
                            $scope.unitT = $scope.unitTask[key].FLnkId;
                            if ($scope.unitT === unifiedId){
                                $scope.examId = $scope.unitTask[key].ExamFId;
                                $scope.unifiedFId = $scope.unitTask[key].UnifiedFId;
                                $http.post('/BootStrap/Interface/generalQuery.ashx',{
                                    Proc_name : 'Proc_GetOcrCommitControl',
                                    RelationFlnkid : unifiedId
                                }).then(function (res) {
                                    $scope.targetList = [];
                                    if (res.data.msg.length !== 0) {
                                        $scope.ocRRes = res.data.msg[0].OCRResultLimit;
                                        $scope.targetList = JSON.parse($scope.ocRRes);
                                    }else {
                                        $scope.targetList = [];
                                    }
                                });
                                $http.post($rootScope.baseUrl + '/Interface0181.ashx', {
                                    examId : $scope.examId
                                }).then(function(res){
                                    if (_.isArray(res.data.msg)){
                                        $scope.length = [];
                                        $scope.textList = res.data.msg;
                                        _.each($scope.textList,function (value,key) {
                                            $scope.length = $scope.length.concat($scope.textList[key].question);
                                        });
                                        $scope.lengths = $scope.length.length;
                                    }
                                });
                            }
                        })
                    })
                }
            });*/

            //查看原卷
            $http.post('' + '/BootStrap/EncryptUnified/getPaperConfig.ashx', {
                unifiedId: unifiedId
            }).success(function (data) {
                $scope.paperDate = data.msg;
            });
            $scope.thispath = $location.path();
            $scope.viewPaper = function () {
               window.open('#/composing?pathss=' + $scope.thispath + '&ExamFlnkID=' + $scope.paperDate.FLnkID + '&isExamCenter=' + 1 + '&isOpenNewWin=1');
            };


            //跳转查看答题卡页面
            $scope.goAnswerSheet = function () {
                window.open('#/answerSheet?unifiedId=' + unifiedId + '&isOpenNewWin=1')
            };

            //获取ocr提交结果
            $http.post('/BootStrap/schoolmanager/getUnifiedOcrCommit.ashx',{
                unifiedItemFId : unifiedId
            }).then(function (res) {
                if (_.isArray(res.data.msg) && res.data.msg.length > 0){
                    $scope.orgina = res.data.msg;
                    var index = Math.floor((Math.random()*$scope.orgina.length));
                    $scope.orginalLis = $scope.orgina[index].OcrStr.split('|');
                    _.each($scope.orginalLis,function (item,index) {
                        $scope.orginalList.push({dis : (index+1).toString(),questionAnswer:item,orders:(index+1).toString()});
                    });
                    for (var i = 0; i<$scope.orginalList.length; i++){
                        if (new RegExp("-").test($scope.orginalList[i].questionAnswer)){
                            $scope.orginalList[i].questionAnswer = ""
                        }
                    }
                }
                $scope.$watch('selectContentList',function (newValue) {
                    if ($scope.orginalList.length>0){
                        if (newValue.length == $scope.orginalList.length){
                            $scope.isCheckedAll = false;
                        }else {
                            if (newValue.length < $scope.orginalList.length){
                                $scope.isCheckedAll = true;
                            }
                        }
                    }
                    if (newValue.length>0){
                        $scope.isRight = false;
                    }else {
                        $scope.isRight = true;
                    }
                },true);
                $scope.$watch('selectContentLists',function (newValue) {
                    if ($scope.targetList.length>0){
                        if (newValue.length === $scope.targetList.length){
                            $scope.isCheckedAllRight = false;
                        }else {
                            if (newValue.length < $scope.targetList.length){
                                $scope.isCheckedAllRight = true;
                            }
                        }
                    }
                    if (newValue.length>0){
                        $scope.isLeft = false;
                    }else {
                        $scope.isLeft = true;
                    }
                },true);

                $scope.checkedAll = function (flag) {
                    if(flag === 2){
                        //TODO 原数组全选
                        if ($scope.orginalList.length>0){
                            if ($scope.isCheckedAll){
                                $scope.selectContentList = [];
                                _.each($scope.orginalList,function (value) {
                                    $scope.isNoSelect(value);
                                    if (!value.isNoSelectt){
                                        $scope.selectContentList.push(value);
                                    }
                                });
                                $scope.selectContentList = _.uniq($scope.selectContentList);
                            }else {
                                $scope.isCheckedAll = true;
                                $scope.selectContentList = [];
                            }
                        }

                    }else{
                        //TODO 目标数组全选
                        if ($scope.targetList.length>0){
                            if ($scope.isCheckedAllRight){
                                $scope.isCheckedAllRight = false;
                                $scope.selectContentLists = $scope.selectContentLists.concat($scope.targetList);
                                $scope.selectContentLists = _.uniq($scope.selectContentLists);
                            }else {
                                $scope.isCheckedAllRight = true;
                                $scope.selectContentLists = [];
                            }
                        }
                    }

                };
                $scope.isOragin = function () {
                    $scope.isList = [];
                    _.each($scope.orginalList,function (value) {
                        $scope.isNoSelect(value);
                        if (!value.isNoSelectt){
                            $scope.isList.push(value);
                        }
                    });
                    if ($scope.orginalList.length === 0 || $scope.isList.length===0){
                        $('#isOragin').css({'cursor':'default'});
                    }else {
                        $('#isOragin').css({'cursor':'pointer'});
                    }
                };
                $scope.isCursor = function () {
                    if ($scope.targetList.length===0){
                        $('#isCursor').css({'cursor':'default'});
                    }else {
                        $('#isCursor').css({'cursor':'pointer'});
                    }
                };

                //题的长度
                $scope.orginLength = $scope.orginalList.length;
                $scope.tarLength = $scope.targetList.length;
                //todo 监听题号数组长度的变化
                $scope.$watch('targetList',function (newValue,oldValue) {
                    $scope.tarLength = newValue.length;
                },true);
            });
            //点击左侧数组
            $scope.checked = function (item) {
                if($scope.isContentChecked(item)) {
                    var index = _.findIndex($scope.selectContentList, function (qstitem) {
                        return qstitem.dis === item.dis;
                    });
                    $scope.selectContentList.splice(index,1);
                }else{
                    if (!item.isNoSelectt){
                        $scope.selectContentList.push(item);
                    }else {
                        $scope.selectContentList = _.uniq($scope.selectContentList);
                    }
                }
            };
            $scope.isNoSelect = function (item) {
                item.isNoSelectt = false;
                _.each($scope.targetList,function (value) {
                    if (value.dis === item.dis){
                        item.isNoSelectt = true;
                    }
                });
            };
            $scope.isContentChecked = function (item) {
                return !!_.find($scope.selectContentList,function (qstitem) {
                    return qstitem.dis === item.dis
                });
            };

            //点击右侧数组
            $scope.checkedX = function (item) {
                if($scope.isContentCheckedX(item)) {
                    var index = _.findIndex($scope.selectContentLists, function (qstitem) {
                        return qstitem.dis === item.dis;
                    });
                    $scope.selectContentLists.splice(index,1);
                }else{
                    $scope.selectContentLists.push(item);
                }
            };
            if ($scope.selectContentList = []){
                $scope.isCheckedAll = true;
            }
            if ($scope.selectContentLists = []){
                $scope.isCheckedAllRight = true;
            }
            $scope.isContentCheckedX = function (item) {
                return !!_.find($scope.selectContentLists,function (qstitem) {
                    return qstitem.dis === item.dis
                });
            };

            //点击向右按钮执行的函数
            $scope.goRight = function () {
                if (!$scope.isRight){
                    if ($scope.selectContentList.length <= 0){
                        $scope.isCheckedAll = true;
                        constantService.showSimpleToast('请先选择原卷!');
                        return false;
                    }else {
                        $scope.targetList = $scope.targetList.concat($scope.selectContentList);
                        $scope.targetList = _.uniq($scope.targetList);
                        $scope.lengths = $scope.lengths ? $scope.lengths : $scope.orginLength;
                        $scope.$watch('targetList',function (newValue,oldValue) {
                            if (newValue.length > $scope.lengths){
                                constantService.alert('不能超过试卷题目数量!');
                                $scope.targetList = _.uniq(oldValue);
                            }else {
                                $scope.ordersArr = [];
                                _.each($scope.targetList,function (value) {
                                    $scope.ordersArr.push(value.orders);
                                });
                                $scope.ordersArr = _.uniq($scope.ordersArr);
                                var lengthss = $scope.ordersArr.length;
                                if (lengthss < $scope.targetList.length){
                                    constantService.alert('原题号重复!');
                                    $scope.targetList = $scope.targetList.splice(0,oldValue.length);
                                }
                            }
                        },true);
                        $scope.selectContentList = [];
                        $scope.isCheckedAll = true;
                    }
                }
            };
            //点击向左按钮执行的函数
            $scope.goLeft = function () {
                if (!$scope.isLeft){
                    if ($scope.selectContentLists.length <= 0){
                        constantService.showSimpleToast('请选择目标卷!');
                        $scope.isCheckedAllRight = true;
                        return false;
                    }
                    else{
                        $scope.targetList = _.difference($scope.targetList,$scope.selectContentLists);
                        $scope.selectContentLists = [];
                        $scope.isCheckedAllRight = true;
                        return;
                    }
                }
            };
            $scope.$watch('targetList',function (newValue) {
                if (newValue.length>0){
                    $scope.isColor = false;
                }else {
                    $scope.isColor = true;
                }
            });
            //点击提交按钮执行的函数
            $scope.submit = function () {
                if ($scope.targetList.length>0){
                    constantService.confirm('确认提交','确定提交修改结果吗?',['确定','取消'],function () {
                        $scope.controLstr = [];
                        $scope.unitTaskData = [];
                        _.each($scope.targetList,function (res) {
                            $scope.controLstr = {
                                orders : res.orders.toString(),
                                dis: res.dis.toString()
                            };
                            $scope.unitTaskData.push($scope.controLstr);
                        });
                        $scope.unitTaskData = JSON.stringify($scope.unitTaskData);
                        var UnifiedFID = $scope.FlnkID?$scope.FlnkID:unifiedId;
                        $http.post('' + '/BootStrap/Interface/CreateOcrCommitControl.ashx',{  //保存修改
                            flnkid  : '',
                            relationFlnkid : UnifiedFID,
                            Type : 3,
                            controlStr : $scope.unitTaskData
                        }).then(function (res) {
                            if (res.data.code === 0){
                                constantService.alert('保存成功!');
                            }else {
                                constantService.alert(res.data.msg);
                            }
                        });
                    });
                }else {

                }
            };



            //todo input框的值
            $scope.modelId = $scope.paperData[0].efid;

            //框失去焦点时执行的函数
            $scope.changeDis = function (item,targetList,index) {
                if (item.dis != undefined && item.dis != ''){
                    if (item.dis != item.orders){
                        if (isNaN(item.dis)){
                            constantService.alert('请输入数字!');
                            item.dis = parseInt(item.orders);
                            return;
                        }
                        if (item.dis > $scope.lengths){
                                constantService.alert('题号不能大于试卷题目数量!');
                                item.dis = parseInt (item.orders);
                                return;
                        } else if (item.dis == 0){
                            constantService.alert('题号不能为零!');
                            item.dis = item.orders;
                            return;
                        }else {
                            var isRepeat = !!_.find(targetList, function (resq) {
                             if(item.orders != resq.orders){
                                 return item.dis === resq.dis;
                                }
                             });
                             if (isRepeat) {
                                 constantService.alert('输入重复!');
                                 item.dis = item.orders;
                                 return;
                             }
                        }
                    }
                }else{
                    constantService.alert('题号不能为空!');
                    item.dis = item.orders;
                    return;
                }

            };
            function transQueryString(){
                var querystr = [];
                for(var key in search) {
                    querystr.push(key + '=' + search[key]);
                }
                return querystr.join('&');
            }
            //搜索试卷执行的函数
            $scope.searchPaper = function () {
                $scope.selectContent = [];
                bes.send('/BootStrap/schoolmanager/getMyUnifieds.ashx',{}, {
                    method: 'post'
                }).then(function (res) {
                    if (_.isArray(res.data.msg)) {
                        $scope.examList = res.data.msg;
                        $scope.unitTaskssa = [];
                        $scope.unitTaskss = [];
                        _.each($scope.examList,function (value) {
                            $scope.unitTask = value.unitTasks;
                            _.each($scope.unitTask,function (value) {
                                if (value.efid){
                                    $scope.unitTaskssa.push(value)
                                }
                            });
                        });
                        var statusArray = [];
                        _.each($scope.unitTaskssa,function (value) {
                            if (value.status>5){
                                statusArray.push(value)
                            }
                        });
                        $scope.unitTaskss = _.difference($scope.unitTaskssa,statusArray);
                       _.each($scope.unitTaskss,function (value) {
                           value.obj = value.efid + value.Name;
                       });
                        $scope.paperLength = $scope.unitTaskss.length;
                    } else {
                        $scope.examList = [];
                        $scope.unitTaskss = [];
                    }
                }, function () {
                    $scope.examList = [];
                    $scope.unitTaskss = [];
                });
                    ngDialog.open({
                        template: '<div class="submit-alert"><div class="submit-alert-title">选择试卷' +
                        '<div class="ngdialogClose pull-right" ng-click="closeThisDialog()">x</div></div>' +
                        '<div class="submit-search"><div class="inputBox"><input type="text" placeholder="输入考试名或卷号搜索试卷" ng-model="search" /></div></div>' +
                        '<div class="submit-alert-main"><div class="submit-alert-main-box">' +
                        '<table cellspacing="0" cellpadding="0" width="100%">' +
                        '<tr><td width="10%">选择</td><td width="10%">卷号</td><td width="10%">科目</td><td width="50%">考试名</td><td width="20%">创建时间</td></tr>' +
                        '<tr ng-repeat="task in unitTaskss | filter:{obj:search} track by $index" ng-show="unitTaskss.length>0"><td width="10%"><span class="check-btn" ng-class="{checked:isContentCheck(task)}" ng-click="check(task)"></span></td><td width="10%" ng-bind="task.efid"></td><td width="10%" ng-bind="task.subjectName"></td><td title="{{task.ExName}}" class="over" width="50%" ng-bind="task.Name"></td><td width="20%" ng-bind="task.ExamTime | formatDate:\'yyyy-MM-DD\'"></td></tr>' +
                        '</table>' +
                        '<div class="text-center mt10" style="color:red" ng-show="paperLength===0">暂无试卷信息！</div></div><button class="btn btn-confirm" ng-click="deterSelect()">确定选择</button></div>' +
                        '</div>',
                        className: 'ngdialog-theme-default',
                        appendClassName: 'submitRefer-assigin',
                        plain: true,
                        scope: $scope
                    });
                    $scope.check = function (task) {
                        $scope.selectContent = [];
                        if($scope.isContentCheck(task)) {
                            var index = _.findIndex($scope.selectContent, function (qstitem) {
                                return qstitem.FLnkId === task.FLnkId;
                            });
                            $scope.selectContent.splice(index,1);
                        }else{
                            $scope.selectContent.push(task);
                        }
                    };
                    $scope.isContentCheck = function (task) {
                        return !!_.find($scope.selectContent,function (qstitem) {
                            return qstitem.FLnkId === task.FLnkId
                        });
                    };
                //点击确认选择按钮执行的函数
                $scope.deterSelect = function () {
                    if ($scope.selectContent.length>0){
                        $scope.examId = $scope.selectContent[0].ExamFId;
                        $scope.modelId = $scope.selectContent[0].efid;
                        $scope.FlnkID = $scope.selectContent[0].FLnkId;
                        var href = location.origin + location.pathname + location.search +'#/submitRefer';
                        search.targetId = $scope.FlnkID;
                        history.pushState(null, '', href + '?' + transQueryString());
                        $http.post($rootScope.baseUrl + '/Interface0181.ashx', {
                            examId : $scope.examId
                        }).then(function(res){
                            if (_.isArray(res.data.msg)){
                                $scope.length = [];
                                $scope.textList = res.data.msg;
                                _.each($scope.textList,function (value,key) {
                                    $scope.length = $scope.length.concat($scope.textList[key].question);
                                });
                                $scope.lengths = $scope.length.length;
                                ngDialog.close();
                            }
                        });
                        $('#view').hide();
                        $('#text').show();
                        $http.post('' + '/BootStrap/EncryptUnified/getPaperConfig.ashx', {
                            unifiedId: $scope.FlnkID
                        }).then(function (res) {
                            if (res.data.code == '0'){
                                $scope.paper = res.data.msg;
                            }else {
                                constantService.alert(res.data.msg);
                            }
                        },function () {
                            constantService.alert('服务器错误!')
                        });
                        $scope.thispath = $location.path();
                        $scope.viewText = function () {
                            window.open('#/composing?pathss=' + $scope.thispath + '&ExamFlnkID=' + $scope.paper.FLnkID + '&isExamCenter=' + 1);
                        };
                        /*$http.post('/BootStrap/Interface/generalQuery.ashx',{
                            Proc_name : 'Proc_GetOcrCommitControl',
                            RelationFlnkid : $scope.FlnkID
                        }).then(function (res) {
                            if (res.data.msg.length !== 0) {
                                $scope.ocRRes = res.data.msg[0].OCRResultLimit;
                                $scope.targetList = JSON.parse($scope.ocRRes);
                            }else {
                                $scope.targetList = [];
                            }
                        });*/
                    }else {
                       constantService.alert('请选择试卷！');
                    }
                };
            };
        }];
});

