/**
 * Created by Hexl on 2016/4/15.
 */
angular.module('customizeDirective', [])
	.directive('editInPlace', function () {
    return {
        restrict: 'AE',
        replace: true,
        templateUrl: loadHtml('/directive-extend/template/editInPlace.html'),
        scope: {
            eitStr: '=',
			placeholder: '@',
			blurCb: '&'
        },
        link: function (scope, $ele, attrs) {
			scope.attr = attrs;
        },
        controller: ['$scope',function ($scope) {
            $scope.editable = !!$scope.eitStr;
			$scope.warning = !$scope.eitStr;
			$scope.temp = $scope.eitStr;
            $scope.saveStr = function () {
            	if($scope.attr['blurCb'] && $scope.blurCb && _.isFunction($scope.blurCb)) {
					var isOK = $scope.blurCb({
						text: $scope.temp
					});
					if(isOK) {
						$scope.editable = !!$scope.temp;
						$scope.warning = !$scope.temp;
						$scope.eitStr = $scope.temp;
					}else {
						$scope.editable = false;
						$scope.warning = true;
					}
				}else {
					$scope.editable = !!$scope.temp;
					$scope.warning = !$scope.temp;
					$scope.eitStr = $scope.temp;
				}
            };
            $scope.editText = function (event) {
                $scope.editable = false;
				$scope.warning = false;
                var target = event.target.parentNode;
                setTimeout(function () {
                    $(target).find('input').focus();
                }, 0);
            };
        }]
    }
})
	.directive('treeView1', [function () {
    return {
        restrict: 'E',
        templateUrl: loadHtml('/treeView.html'),
        scope: {
            treeData: '=',
            canChecked: '=',
            textField: '@',
            itemClicked: '&',
            itemCheckedChanged: '&',
            itemTemplateUrl: '@',
            currentUnit: '='
        },
        controller: ['$scope', function ($scope) {

            $scope.itemExpended = function (item, $event) {
                item.$$isExpend = !item.$$isExpend;
                $event.stopPropagation();
            };

            $scope.getItemIcon = function (item) {
                var isLeaf = $scope.isLeaf(item);
                if (isLeaf) {
                    $scope.choLast = true;
                    return 'fa';
                } else {
                    $scope.choLast = false;
                }
                return item.$$isExpend ? 'fa fa-minus-square' : 'fa fa-plus-square';
            };

            $scope.isLeaf = function (item) {
                //if (item.unit[0].unitName == '') {
                //    return !item.unit || !item.unit.length;
                //} else {
                return !item.unit || !item.unit.length || !item.unit[0].unitName;
                //}
                
            };

            $scope.isCurrentLeaf = function (item) {
                return $scope.isLeaf(item) && item.unitId === $scope.currentUnit;
            };

            $scope.warpCallback = function (item, $event) {
                $scope.$parent.paginationConf.currentPage = 1;
                if (item.unitId == undefined || item.unitId == null) {
                    return false;
                } else {
                    $scope.currentUnit = item.unitId;
                    console.log($scope.currentUnit);
                }
            };
        }]
    }
}])
	//星级展示指令
	.directive('star', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('../directive-extend/template/star.html'),
	        scope: {
	            starVal: '=',
	            clickCb: '&',
	            disable: '=',
				unclickable: '='
	        },
	        controller: ['$scope',function ($scope) {
	            $scope.starVal = (+$scope.starVal || 1);
	            $scope.starValList = [1, 2, 3, 4, 5];
	            if ($scope.disable) {
	                $scope.starValList = _.range(1, $scope.starVal + 1);
	            }
	            $scope.defSrc = "../../assets/images/star-off.png";
	            $scope.activeSrc = "../../assets/images/star-on.png";
	            $scope.setStarVal = function (item) {
	                if ($scope.disable || $scope.unclickable) {
	                    return;
	                }
	                $scope.starVal = item;
                    $scope.clickCb({
                        data: $scope.starVal
					});
	            }
	        }]
	    }
	})
	.directive('fontStar', function () {
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: loadHtml('../directive-extend/template/font-star.html'),
			scope: {
				starVal: '=',
				clickCb: '&',
				disable: '=',
				unclickable: '='
			},
			controller: ['$scope', function ($scope) {
				$scope.starVal = (+$scope.starVal || 0);
				$scope.starValList = [1, 2, 3, 4, 5];
				if ($scope.disable) {
					$scope.starValList = _.range(1, $scope.starVal + 1);
				}
				$scope.setStarVal = function (item) {
					if ($scope.disable || $scope.unclickable) {
						return;
					}
					$scope.starVal = item;
				}
			}]
		}
	})
	//计数器指令，支持- +修改输入框数值
	.directive('counter', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('../directive-extend/template/counter.html'),
	        scope: {
	            countNumber: '='
	        },
	        controller: ['$scope', function ($scope) {
	            $scope.minus = function () {
	                if ($scope.countNumber > 0) {
	                    $scope.countNumber--;
	                }
	            };
	            $scope.plus = function () {
	                $scope.countNumber++;
	            }
	        }]
	    }
	})
	//试题编辑器指令
	.directive('questionEditor', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('../directive-extend/template/questionEditor.html'),
	        scope: {
	            question: '=',
	            knowledges: '=',
	            subjectId: '@',
	            saveCb: '&'
	        },
	        link: function (scope, element, attr) {
	            //初始化ckeditor
	            CKEDITOR.replaceAll();
	            var instances = CKEDITOR.instances;
	            scope.question.tempTitle = '';
	            scope.question.tempOptionOne = '';
	            scope.question.tempOptionTwo = '';
	            scope.question.tempOptionThree = '';
	            scope.question.tempOptionFour = '';

	            instances['question-content'].on('change', function () {
	                scope.$apply(function () {
	                    scope.question.tempTitle = instances['question-content'].getData();
	                });
	            });
	            instances['question-a'].on('change', function () {
	                scope.$apply(function () {
	                    scope.question.tempOptionOne = instances['question-a'].getData();
	                });
	            });
	            instances['question-b'].on('change', function () {
	                scope.$apply(function () {
	                    scope.question.tempOptionTwo = instances['question-b'].getData();
	                });
	            });
	            instances['question-c'].on('change', function () {
	                scope.$apply(function () {
	                    scope.question.tempOptionThree = instances['question-c'].getData();
	                });
	            });
	            instances['question-d'].on('change', function () {
	                scope.$apply(function () {
	                    scope.question.tempOptionFour = instances['question-d'].getData();
	                });
	            });
	            scope.save = function () {
	                var question = {
	                    QFLnkID: scope.QFLnkID || '',
	                    title: instances['question-content'].getData(),
	                    optionOne: instances['question-a'].getData(),
	                    optionTwo: instances['question-b'].getData(),
	                    optionThree: instances['question-c'].getData(),
	                    optionFour: instances['question-d'].getData()
	                };
	                if (scope.isChoice) {
	                    question.answer = scope.question.Answer;
	                } else {
	                    question.answer = instances['question-answer'].getData();
	                }
	                scope.saveCb({
	                    data: question
	                });
	            };
	        },
	        controller: ['$scope', '$rootScope', '$http', 'constantService', 'ngDialog', 'searchEngineUrl',
				function ($scope, $rootScope, $http, constantService, ngDialog, searchEngineUrl) {
	            //如果传入了question对象，则认为是修改题目，否则视为添加题目
	            $scope.QFLnkID = $scope.question.QFLnkID || '';
	            $scope.queryResults = [];
	            if ($scope.QFLnkID && $scope.QFLnkID !== constantService.EMPTY_QFLNKID) {
	                $http.post($rootScope.baseUrl + '/Interface0208.ashx', {
	                    qFlnkID: $scope.QFLnkID
	                }).then(function (res) {
	                    var questionTemp;
	                    var order = $scope.question.orders;
	                    if (_.isArray(res.data.msg)) {
	                        var detail = res.data.msg[0];
	                        questionTemp = {
	                            title: detail.title,
	                            orders: $scope.question.orders,
	                            ShowType: _.find($scope.showTypeList, function (type) {
	                                return type.id === detail.ShowType
	                            }),
	                            OptionOne: detail.OptionA,
	                            OptionTwo: detail.OptionB,
	                            OptionThree: detail.OptionC,
	                            OptionFour: detail.OptionD,
	                            knowledgeId: detail.strknowledgeId,
	                            knowledge: $scope.question.knowledges,
	                            analysis: detail.Analysis,
	                            qtypeName: $scope.question.QTypeName,
	                            qtypeId: $scope.question.QTypeId,
	                            typeId: detail.OptionA === '' ? '0' : '1',
	                            DifficultLevel: detail.DifficultLevel,
	                            Answer: $.trim($scope.question.Answer),
	                            QFLnkID: detail.QFLnkID,
	                            scource: detail.source
	                        };
	                        $scope.question = questionTemp;
	                    }
	                });
	            }
	            $scope.isChoice = $scope.question.typeId === '1';
	            $scope.isEmpty = !$scope.QFLnkID || $scope.QFLnkID === constantService.EMPTY_QFLNKID;
	            $scope.showQuery = false;
	            $scope.queryDone = false;
	            // var instances = CKEDITOR.instances;

	            $scope.backToQEditor = function () {
	                $scope.showQuery = false;
	            };
	            $scope.selectQuestion = function (res) {
	                ngDialog.closeAll();
	                $scope.saveCb({
	                    data: res,
	                    isReplace: true
	                });
	            };
	            $scope.queryRelated = function () {
	                $scope.queryResults = [];
	                $scope.queryDone = false;
	                if ($scope.question.tempTitle === '') {
	                    constantService.alert('请输入题干内容！');
	                    return;
	                }
	                $scope.showQuery = true;
	                var instances = CKEDITOR.instances;
	                var key = instances['question-content'].document.getBody().getText();
	                //去除输入文字中空格
	                var keyArray = key.split('');
	                var newKey = _.filter(keyArray, function (item) {
	                    return item !== ' ';
	                });
	                var newStr = newKey.join('');
	                $http.get(searchEngineUrl + '?q=Title:' + encodeURIComponent(newStr)
						+ '&fq=TypeId:' + $scope.question.qtypeId + '&fq=subjectId:' + $scope.subjectId + '&wt=json').then(function (res) {
						    $scope.queryDone = true;
						    $scope.queryResults = res.data.response.docs;
						}, function () {
						    $scope.queryDone = true;
						    $scope.queryResults = [];
						});
	            };
	        }]
	    }
	})
	.directive('analysisEditor', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('directive-extend/template/analysisEditor.html'),
	        scope: {
	            question: '=',
	            saveCb: '&'
	        },
	        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
	            CKEDITOR.replaceAll();
	            var instances = CKEDITOR.instances;
	            $scope.save = function () {
	                $scope.question.analysis = instances['question-analysis'].getData();
	                ngDialog.closeAll();
	                $scope.saveCb({
	                    data: {
	                        analysis: instances['question-analysis'].getData()
	                    }
	                });
	            };
	        }]
	    }
	})
	.directive('answerEditor', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('directive-extend/template/AnswerEditor.html'),
	        scope: {
	            question: '=',
	            saveCb: '&'
	        },
	        controller: ['$scope', 'ngDialog', 'constantService', function ($scope, ngDialog, constantService) {
	        	console.log($scope.question);
	            CKEDITOR.replaceAll();
	            var instances = CKEDITOR.instances;
	            $scope.q = {
	                isAAnswer: false,
	                isBAnswer: false,
	                isCAnswer: false,
	                isDAnswer: false
	            };
	            var options = ['A', 'B', 'C', 'D'], answer = $scope.question.Answer || $scope.question.answer;
				$scope.answerContent = answer;
	            if ($scope.question.OptionOne !== '' && !!$scope.question.OptionOne) {
	                _.each(options, function (item) {
	                    if (answer.indexOf(item) >= 0) {
	                        $scope.q['is' + item + 'Answer'] = true;
	                    } else {
	                        $scope.q['is' + item + 'Answer'] = false;
	                    }
	                });
	            }
	            $scope.save = function () {
	                ngDialog.closeAll();
	                var answer = '';
	                if ($scope.question.OptionOne) {
	                    var temp = [];
	                    _.each(options, function (item) {
	                        if ($scope.q['is' + item + 'Answer']) {
	                            temp.push(item);
	                        }
	                    });
	                    answer = temp.join(',')
	                } else {
	                    answer = instances['question-answer'].getData();
	                }
	                $scope.saveCb({
	                    data: {
	                        answer: answer
	                    }
	                });
	            };
                $scope.addMultiAnswerTip = function(){
                    constantService.multiAnswerTip();
                };
	        }]
	    }
	})
	.directive('knowledgeEditor', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('directive-extend/template/knowledgeEditor.html'),
	        scope: {
	            question: '=',
	            saveCb: '&',
	            knowledgeList: '=',
	            tagName: '@'
	        },
	        controller: ['$scope', 'ngDialog',function ($scope, ngDialog) {
	            if ($scope.question) {
	                var relatedKlg = _.map($scope.question.knowledge, function (item) {
	                    return {
	                        unitId: item.id,
	                        unitName: item.name
	                    }
	                });
	                $scope.chosenKlgs = _.uniq(relatedKlg, function (item) {
	                    return item.unitId;
	                });
	            } else {
	                $scope.chosenKlgs = [];
	            }
	            var klgListAll = $scope.knowledgeList;
	            $scope.currentUnit = $scope.knowledgeList[0];
	            $scope.currentSection = $scope.currentUnit.unit || [];
	            $scope.currentSubSection = $scope.currentSection[0] && $scope.currentSection[0].unit || [];

	            $scope.choseChapter = function (unit) {
	                $scope.currentUnit = unit;
	                $scope.currentSection = unit.unit;
	                $scope.currentSubSection = [];
	            };
	            $scope.isCurrentUnit = function (unit) {
	                return $scope.currentUnit.unitName === unit.unitName;
	            };
	            $scope.isEmptySubSection = function (unit) {
	                return _.isEmpty(unit);
	            };
	            $scope.choseSection = function (unit) {
	                if (_.isEmpty(unit.unit)) {
	                    if (isSelected(unit)) {
	                        var index = findIndex(unit);
	                        $scope.chosenKlgs.splice(index, 1);
	                    } else {
	                        $scope.chosenKlgs.push({
	                            unitId: unit.unitId,
	                            unitName: unit.unitName
	                        });
	                    }
	                } else {
	                    $scope.currentSubSection = unit.unit;
	                }
	            };
	            $scope.updateSelection = function (e, unit) {
	                e.stopPropagation();
	                var checked = e.target.checked;
	                if (!_.isEmpty(unit.unit)) {
	                    //非最后一级则把所有的都放入chosenKlgs中
	                    $scope.currentSubSection = unit.unit;
	                    if (!checked) {
	                        _.each(unit.unit, function (u) {
	                            if (isSelected(u)) {
	                                var index = findIndex(u);
	                                $scope.chosenKlgs.splice(index, 1);
	                            }
	                        });
	                    } else {
	                        _.each(unit.unit, function (u) {
	                            if (!isSelected(u)) {
	                                $scope.chosenKlgs.push({
	                                    unitId: u.unitId,
	                                    unitName: u.unitName
	                                });
	                            }
	                        });
	                    }
	                } else {
	                    if (!checked) {
	                        if (isSelected(unit)) {
	                            var index = findIndex(unit);
	                            $scope.chosenKlgs.splice(index, 1);
	                        }
	                    } else {
	                        if (!isSelected(unit)) {
	                            $scope.chosenKlgs.push({
	                                unitId: unit.unitId,
	                                unitName: unit.unitName
	                            });
	                        }
	                    }
	                }
	            };
	            $scope.isSelected = function (unit) {
	                if (_.isEmpty(unit.unit)) {
	                    return isSelected(unit);
	                } else {
	                    return !_.find(unit.unit, function (item) {
	                        return !isSelected(item);
	                    });
	                }
	            };
	            $scope.removeKlg = function (unit) {
	                var index = findIndex(unit);
	                if (index >= 0) {
	                    $scope.chosenKlgs.splice(index, 1);
	                }
	            };
	            $scope.selectCur = function (klg) {
	                if (isSelected(klg)) {
	                    $scope.removeKlg(klg);
	                } else {
	                    $scope.chosenKlgs.push(klg);
	                }
	                console.log($scope.chosenKlgs);
	            };
	            var klglist = [];
	            for (var i = 0; i < klgListAll.length; i++) {//第一层
	                for (var j = 0; j < klgListAll[i].unit.length; j++) {//第二层
	                    if (!klgListAll[i].unit[j].unit || !klgListAll[i].unit[j].unit.length) {//如果标记0就只有两层
	                        klglist.push({
	                            'unitId': klgListAll[i].unit[j].unitId,
	                            'unitName': klgListAll[i].unit[j].unitName
	                        })
	                    } else {
	                        for (var k = 0; k < klgListAll[i].unit[j].unit.length; k++) {//第三层
	                            klglist.push({
	                                'unitId': klgListAll[i].unit[j].unit[k].unitId,
	                                'unitName': klgListAll[i].unit[j].unit[k].unitName
	                            })
	                        }
	                    }
	                }
	            }
	            $scope.klgsList = klglist;

	            $scope.isKnowledgeRelated = function (knowledge) {
	                return isSelected(knowledge);
	            };

	            $scope.currentMod = 1;
	            //查询知识点
	            $scope.searchMod = function (modType) {
	                $scope.currentMod = modType;
	            };

	            $scope.save = function () {
	                ngDialog.closeAll();
	                $scope.saveCb({
	                    data: $scope.chosenKlgs
	                });
	            };
	            function isSelected(unit) {
	                return !!_.find($scope.chosenKlgs, function (item) {
	                    return item.unitId === unit.unitId;
	                });
	            }

	            function findIndex(unit) {
	                var idx = -1;
	                _.each($scope.chosenKlgs, function (item, index) {
	                    if (item.unitId === unit.unitId) {
	                        idx = index;
	                        return;
	                    }
	                });
	                return idx;
	            }
	        }]
	    };
	})
	.directive('questionViewer', function () {
	    return {
	        restrict: 'AE',
	        replace: false,
	        templateUrl: loadHtml('../directive-extend/template/questionViewer.html'),
	        scope: {
	            question: '=',
	            showAnswer: '@',
	            editable: '@',
	            showError: '@',
	            showDiff: '@',
                isShowAnswer: '@',
                isShowStuAnswer: '@',
				needShowDis:'@'
	        },
	        link: function (scope, ele, attr) {

	        }
	    }
	})
	.directive('qTypeSelector', function () {
	    return {
	        restrict: 'AE',
	        replace: true,
	        templateUrl: loadHtml('directive-extend/template/questionTypeSelector.html'),
	        scope: {
	            qTypeList: '=',
	            saveCb: '&'
	        },
	        link: function (scope, ele, attr) {

	        },
	        controller: ['$scope', 'ngDialog',function ($scope, ngDialog) {
	            $scope.currentType = $scope.qTypeList[0];
	            $scope.save = function () {
	                if ($scope.currentType) {
	                    ngDialog.closeAll();
	                    $scope.saveCb({
	                        data: $scope.currentType
	                    });
	                }
	            }
	        }]
	    }
	})
	.directive('preventDefault', function () {
	    return {
	        restrict: 'A',
	        link: function (scope, $ele) {
	            $ele.click(function (e) {
	                e.preventDefault();
	            });
	        }
	    }
	})
	.directive('repeatFinish', function () {
	    return {
	        link: function (scope, element, attr) {
	            if (scope.$last == true) {
	                scope.$emit('repeat-render-finish');
	            }
	        }
	    }
	})
	.directive('stuAnswerViewer', function () {
		    return {
		        restrict: 'AE',
		        replace: true,
		        templateUrl: loadHtml('directive-extend/template/stuAnswerViewer.html'),
		        scope: {
		            cutMeta: '=',
		            stuPaper: '=',
		            number: '='
		        },
		        controller: ['$scope', 'ngDialog',function ($scope, ngDialog) {
		            if ($scope.cutMeta && !!$scope.cutMeta.position) {
		                var allCuts = typeof $scope.cutMeta.position === 'string' ? JSON.parse($scope.cutMeta.position) : $scope.cutMeta.position;
		                var qstRelatedCut = _.find(allCuts, function (item) {
		                    return item.order + '' === $scope.number + '';
		                });
		                if (qstRelatedCut) {
		                    $scope.posList = qstRelatedCut.position;
		                } else {
		                    $scope.posList = [];
		                }
		            } else {
		                $scope.posList = [];
		            }
		        }]
		    }
		})
	.directive('tableSortable', function () {
	    return {
	        restrict: 'A',
	        scope: {
	            tableData: '=',
	            tableThKeys: '=',
	            headSelector: '@',
	            sortCol: '@'
	        },
	        link: function (scope, ele, attr) {
	            var $table = $(ele);
	            var sortCol = (scope.sortCol || '').split(',');
	            var sorColArray = _.isEmpty(sortCol) ? _.range(0, $scope.tableData.length) : sortCol;
	            var th = scope.headSelector || 'th';
	            var $ths = $table.find(th);
	            var keys = scope.tableThKeys;
	            scope.$watch('tableData', function (val) {
	                _.each(sorColArray, function (item, index) {
	                    (function (col, i) {
	                        $($ths[col]).css('cursor', 'pointer');
	                        $($ths[col]).off('click').on('click', function () {
	                            if ($(this).hasClass('asc')) {
	                                //如果已经顺序排列，则点击之后倒序排列
	                                scope.$apply(function () {
	                                    scope.tableData.sort(function (a, b) {
	                                        var isNaN = _.isNaN(+a[keys[i]]);
	                                        if (isNaN) {
	                                            return a[keys[i]].localeCompare(b[keys[i]]);
	                                        } else {
	                                            return +a[keys[i]] < +b[keys[i]];
	                                        }
	                                    });
	                                });
	                                $(this).removeClass('asc').addClass('desc');
	                            } else {
	                                //否则正序排列
	                                scope.$apply(function () {
	                                    scope.tableData.sort(function (a, b) {
	                                        var isNaN = _.isNaN(+a[keys[i]]);
	                                        if (isNaN) {
	                                            return a[keys[i]] > b[keys[i]];
	                                        } else {
	                                            return +a[keys[i]] > +b[keys[i]];
	                                        }
	                                    });
	                                });
	                                $(this).removeClass('desc').addClass('asc');
	                            }
	                        });
	                    })(+item, index);
	                });
	            });
	        }
	    }
	})
	.directive('cusSelect', function(){
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: loadHtml('directive-extend/template/cusSelect.html'),
			scope: {
				options: '=',
				csModel: '=',
				repeatKey: '@'
			},
			controller: ['$scope',function($scope){
				$scope.showOption = function(){
					$scope.isShowOption = !$scope.isShowOption;
				};
				$scope.select = function(option){
					$scope.csModel = option;
					$scope.isShowOption = false;
				}
			}]
		}
	})
	.directive('noData', function(){
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: loadHtml('/directive-extend/template/no-data.html'),
			scope: {
				text: '@'
			}
		}
	})
	.directive('lazyImg', function(){
		return {
			restrict: 'A',
			replace: false,
			link: function(scope, ele, attr){
				var src = attr['lazyImg'];
				$(ele).css({
					opacity: '0.5',
					'-webkit-transition': 'all ease-in .2s',
					'-moz-transition': 'all ease-in .2s',
					'-ms-transition': 'all ease-in .2s',
					'-o-transition': 'all ease-in .2s',
					transition: 'all ease-in .2s'
				});
				function lazyLoad(){
					var scrollTop = $(window).scrollTop();
					var imgTop = $(ele).offset().top;
					if(imgTop > 0 && scrollTop > (imgTop - 250)) {
						$(ele).attr('src', src).on('load', function(){
							$(this).css({
								opacity: '1'
							});
						});
					}
				}
				$(window).off('scroll.lazyImg_' + src).on('scroll.lazyImg_' + src, function(){
					lazyLoad();
				});
				scope.$on('$destroy', function(){
					$(window).off('scroll.lazyImg_' + src);
				});
				lazyLoad();
			}
		}
	})
	.directive('wyCheckBox', function(){
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: loadHtml('/directive-extend/template/wy-check-box.html'),
			scope: {
				wyModel: '=',
				value: '=',
				label: '@'
			},
			controller: ['$scope',function($scope){
				$scope.isCheck = function(){
					return _.isEqual($scope.wyModel, $scope.value);
				};
				$scope.check = function(){
					$scope.wyModel = $scope.value;
				};
			}]
		}
	})
	.directive('scale', function(){
		return {
			restrict: 'A',
			scope: {
				zoom: '='
			},
			link: function (scope, element, attrs) {
				element.bind('load', function(){
					var scale = +scope.zoom || 1;
					$(element).css({
						position: 'relative',
						'margin-left': 'auto',
						'margin-right': 'auto',
						'-webkit-transform': 'scale(' + scale + ')',
						'-o-transform': 'scale(' + scale + ')',
						'-ms-transform': 'scale(' + scale + ')',
						'-moz-transform': 'scale(' + scale + ')',
						'transform': 'scale(' + scale + ')',
						'margin-top': (+scale - 1) * $(element).height() / 2 +'px'
					});
				});
				scope.$watch('zoom', function(val){
					setTimeout(function(){
						$(element).css({
							position: 'relative',
							'margin-left': 'auto',
							'margin-right': 'auto',
							'-webkit-transform': 'scale(' + val + ')',
							'-o-transform': 'scale(' + val + ')',
							'-ms-transform': 'scale(' + val + ')',
							'-moz-transform': 'scale(' + val + ')',
							'transform': 'scale(' + val + ')',
							'margin-top': (+val - 1) * $(element).height() / 2 +'px'
						});
					}, 10);
				});
			}
		};
	})
	.directive('breadcrumbTrail', function(){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: loadHtml('/directive-extend/template/queue-list.html'),
			scope: {
				queueList: '='
			}
		}
	})
	.directive('switch', function(){
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: loadHtml('/directive-extend/template/switch.html'),
			scope: {
				model: '=',
				onToggle: '&',
				origin: '='
			},
			link: function(scope, element, attr){
				scope.toggle = function(){
					scope.model = !scope.model;
					if(scope.onToggle) {
						scope.onToggle({
							data: scope.model
						});
					}
				};
			}
		}
	})
    .directive('throttleClick', function($parse){
        return {
            restrict: 'A',
            link: function(scope, ele, attr){
                var fn = $parse(attr['throttleClick']);
                //点击间隔时间
                var throttleTime = +attr['throttleTime'] || 500;
                var ignoreClass = attr['ignoreClass'] === '1';
                var isDisabled = false, timer = null;
                ele.on('click', function(e){
                    var callback = function(){
                        if(!isDisabled) {
                            isDisabled = true;
                            fn(scope, {$event: e});
                            clearTimeout(timer);
                            timer = setTimeout(function(){
                                isDisabled = false;
                                !ignoreClass && ele.removeClass('throttle-disable');
                            }, throttleTime);
                            !ignoreClass && ele.addClass('throttle-disable');
                        }
                    };
                    scope.$apply(callback);
                });
            }
        }
    })
	.directive('autoSelect', function(){
		return {
			restrict: 'A',
			link: function(scope, $ele, attr){
				$($ele).off('focus').on('focus', function(){
					$(this).get(0).select();
				});
			}
		}
	});