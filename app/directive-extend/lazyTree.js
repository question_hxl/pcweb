/**
 * Created by Administrator on 2016/6/30.
 */
angular.module('lazyTree', [])
.directive('lazyTree', function(){
   return {
       restrict: 'E',
       templateUrl: loadHtml('directive-extend/template/lazyTree.html'),
       replace: true,
       scope: {
           treeData: '=',
           itemClicked: '&',
           currentUnit: '=',
           triggerInit: '='
       },
       controller: ['$scope',function($scope){
           $scope.expandTree = function(e, data){
               e.preventDefault();
               e.stopPropagation();
               if(data.unit && data.unit.length > 0) {
                   if(data.$$isExpand) {
                       data.$$isExpand = false;
                       $scope.currentUnit = data.unitId;
                       $scope.itemClicked({
                           unitId: $scope.currentUnit
                       });
                   }else {
                       $scope.currentUnit = data.unitId;
                       data.$$isExpand = true;
                       if(!data.renderUnit) {
                           data.renderUnit = data.unit;
                           _.each(data.unit, function(item){
                               item.parentId = data.unitId;
                           });
                       }
                       initLastLeaf(data);
                       $scope.itemClicked({
                           unitId: $scope.currentUnit
                       });
                   }
               }else {
                   //触发点击回调
                   $scope.currentUnit = data.unitId;
                   $scope.itemClicked({
                       unitId: data.unitId
                   });
               }
           };

           $scope.isCurrent = function(data){
               if(data.unitId === $scope.currentUnit) {
                   return true;
               }else {
                   if(data.unit) {
                       return !!_.find(data.unit, function(item){
                           return $scope.isCurrent(item);
                       });
                   }else {
                       return false;
                   }
               }
           };

           $scope.$watch('triggerInit', function(val, oldVal){
               if(val && _.isArray($scope.treeData) && $scope.treeData.length > 0) {
                   initLastLeaf($scope.treeData[0]);
                   $scope.itemClicked({
                       unitId: $scope.currentUnit,
                       force: true
                   });
               }
           });
           function initLastLeaf(unit){
               if(unit.unit && unit.unit.length > 0) {
                   unit.renderUnit = unit.unit;
                   unit.$$isExpand = true;
                   $scope.currentUnit = unit.unitId;
                   // initLastLeaf(unit.unit[0]);
               } else {
                   unit.$$isExpand = true;
                   $scope.currentUnit = unit.unitId;
               }
           }
       }]
   } 
});