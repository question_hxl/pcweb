/**
 * Created by Administrator on 2016/5/25.
 */
angular.module('richDirective', ['customizeDirective'])
		.directive('richQViewer', function () {
			return {
				restrict: 'AE',
				replace: true,
				templateUrl: loadHtml('../directive-extend/template/richQViewer.html'),
				transclude: true,
				scope: {
					question: '=',
					showAnswer: '@',
					isShowAnswer: '@',
					isShowStuAnswer: '@',
					editable: '@',
					showError: '@',
					showDiff: '@',
					clickCb: '&',
					activeQ: '=',
					dblClickCb: '&',
					needShowDis:'@',
					bubble: '@'
				},
				link: function (scope, $ele, attrs) {
					if(attrs['bubble'] === 'true') {
						scope.bubble = true;
					}else {
						scope.bubble = false;
					}
				},
				controller: ['$scope', 'utilService', function ($scope, utilService) {
					if($scope.question && typeof $scope.question.ShowType === 'string') {
						$scope.question.ShowType = _.find(utilService.showTypeList, function(t){
							return t.id === $scope.question.ShowType;
						});
					}
					$scope.selectQst = function (q, isSub, e) {
						!$scope.bubble && e.stopPropagation();
						if ($scope.clickCb && _.isFunction($scope.clickCb)) {
							$scope.clickCb({
								data: q
							});
						}
					};
					$scope.dblClickQst = function (q, isSub, e) {
						!$scope.bubble && e.stopPropagation();
						if ($scope.dblClickCb && _.isFunction($scope.dblClickCb)) {
							$scope.dblClickCb({
								data: q
							});
						}
					};
				}]
			}
		})
		.directive('scrollFix', function () {
			return {
				restrict: 'AC',
				link: function (scope, $ele, attrs) {
					var $toFixEle = $($ele);
					var offsetY = +$toFixEle.offset().top;
					var width = $toFixEle.innerWidth();
					var offsetX = (+$toFixEle.offset().left + width) / $(window).innerWidth();
					var minus = $(window).innerWidth() - (+$toFixEle.offset().left + width);

					$(window).off('scroll.scrollFix').on('scroll.scrollFix', function () {
						var scrollTop = $(window).scrollTop();
						if (!_.isEmpty($toFixEle)) {
							if (scrollTop >= offsetY) {
								var left = "calc(" + offsetX * 100 + "% - " + width + "px)";
								$toFixEle.css({
									position: 'fixed',
									top: 0,
									'max-height': $(window).height() + 'px',
									overflow: 'auto',
									width: width + 'px'
								});
								if(Math.abs(minus) > 20) {
                                    $toFixEle.css({
                                        left: left
                                    });
								}else {
                                    $toFixEle.css({
                                        right: 0
                                    });
								}
							} else {
								$toFixEle.css({
									position: 'static',
									'max-height': 'none'
								});
							}
						}
					});
				}
			};
		})
		.directive('stepBox', function () {
			return {
				restrict: 'AE',
				replace: true,
				templateUrl: loadHtml('directive-extend/template/stepBox.html'),
				scope: {
					steps: '=',
					activeIndex: '='
				},
				link: {
					post: function (scope, $ele, attr) {
						var setMargin = function () {
							var STEP_WIDTH = 40;
							var boxWidth  = $ele.width() - 40,
							    totalStep = scope.steps.length,
							    margin    = (Math.floor(boxWidth - STEP_WIDTH * totalStep) - (boxWidth / 100)) / (totalStep - 1);
							scope.margin = margin;
							scope.marginPercent = scope.margin / boxWidth * 100;
							scope.processWidth = 'calc(' + scope.margin / STEP_WIDTH * 100 + '% + 40px)';
						};
						scope.$watch('steps', function () {
							setMargin();
						}, true);
						$(window).resize(function () {
							setMargin();
						})
					}
				},
				controller: ['$scope',function ($scope) {
					$scope.activeIndex = +$scope.activeIndex;
				}]
			}
		})
		.directive('richQEditor', function () {
			return {
				restrict: 'AE',
				replace: true,
				templateUrl: loadHtml('directive-extend/template/richQEditor.html'),
				scope: {
					saveCb: '&',
					subjectId: '@',
					qTypeId: '@'
				},
				link: function (scope, $ele, attr) {
					CKEDITOR.replaceAll();
				},
				controller: ['$scope', '$http', 'constantService', 'searchEngineUrl',
					function ($scope, $http, constantService, searchEngineUrl) {
					//默认步骤
					$scope.steps = ['选择试题模式', '添加试题'];
					//默认当前步骤下标
					$scope.stepActiveIndex = 0;
					//默认模式
					$scope.mode = '0';
					$scope.$watch('mode', function (newVal, oldVal) {
						//切换模式时，修改步骤数，初始化子题数量
						if (newVal === '1') {
							$scope.steps = ['选择试题模式', '添加父题', '添加子题'];
							$scope.modeType = 'A';
							$scope.subQstList = [{}];
						} else {
							$scope.steps = ['选择试题模式', '添加试题'];
							$scope.subQstList = [{}];
						}
					});
					$scope.goPart2 = function () {
						$scope.stepActiveIndex = 1;
						if ($scope.mode === '0') {
							replaceEditorByStep($scope.stepActiveIndex);
						} else if ($scope.mode === '2') {
							var instances = CKEDITOR.instances;
							$scope.searchKey = instances['searchQTitle'].getData();
							var key = instances['searchQTitle'].document.getBody().getText();
							//去除输入文字中空格
							var keyArray = key.split('');
							var newKey = _.filter(keyArray, function (item) {
								return item !== ' ';
							});
							var newStr = newKey.join('');
							getSearchResult(newStr);
						}
					};
					$scope.goPart3 = function () {
						var instances = CKEDITOR.instances;
						if (instances['fQTitle'].getData() === '') {
							constantService.alert('请先录入父题题干！');
							return;
						}
						$scope.stepActiveIndex = 2;
						replaceEditorByStep($scope.stepActiveIndex);
					};
					$scope.addSubQst = function () {
						$scope.subQstList.push({});
						$scope.steps.push('添加子题');
						$scope.stepActiveIndex += 1;
						replaceEditorByStep($scope.stepActiveIndex);
					};
					$scope.finish = function () {
						if ($scope.mode === '1') {
							//有父题和多个子题
							finishMainAndSub();
						} else {
							//只有1题且已知activeStepIndex = 1
							finishNormal();
						}
					};
                        $scope.addMultiAnswerTip = function(){
                            constantService.multiAnswerTip();
                        };
					$scope.back = function () {
						if ($scope.stepActiveIndex > 0) {
							$scope.stepActiveIndex--;
							//由于子题的节点是动态生成的，所以在回退的时候，如果是子题模式，并且当前步骤是子题模块，需要替换CKEDITOR插件
							if ($scope.stepActiveIndex > 1 && $scope.mode === '1') {
								replaceEditorByStep($scope.stepActiveIndex);
							}
						}
					};
					$scope.nextQst = function () {
						$scope.stepActiveIndex += 1;
						replaceEditorByStep($scope.stepActiveIndex);
					};
					$scope.choseQst = function (q) {
						$scope.saveCb({
							data: q,
							isSearch: true
						});
					};

					function replaceEditorByStep(step) {
						setTimeout(function () {
							CKEDITOR.replace('sQTitle-' + step);
							CKEDITOR.replace('sQOptionA-' + step);
							CKEDITOR.replace('sQOptionB-' + step);
							CKEDITOR.replace('sQOptionC-' + step);
							CKEDITOR.replace('sQOptionD-' + step);
							CKEDITOR.replace('sQAnswer-' + step);
							(function (s) {
								//自执行函数，添加监听函数，子题改变时，同步录入到子题数组中（包括普通模式）
								var instances = CKEDITOR.instances;
								var minusStep = 1;
								if ($scope.mode === '1') {
									minusStep = 2;
								} else {
									minusStep = 1;
								}
								var activeSubQst = $scope.subQstList[s - minusStep];
								instances['sQTitle-' + s].on('change', function (e) {
									activeSubQst.title = e.editor.getData();
								});
								instances['sQOptionA-' + s].on('change', function (e) {
									activeSubQst.OptionOne = e.editor.getData();
								});
								instances['sQOptionB-' + s].on('change', function (e) {
									activeSubQst.OptionTwo = e.editor.getData();
								});
								instances['sQOptionC-' + s].on('change', function (e) {
									activeSubQst.OptionThree = e.editor.getData();
								});
								instances['sQOptionD-' + s].on('change', function (e) {
									activeSubQst.OptionFour = e.editor.getData();
								});
								instances['sQAnswer-' + s].on('change', function (e) {
									activeSubQst.Answer = e.editor.getData();
								});
							})(step);
						}, 0);
					}

					function finishMainAndSub() {
						//已知父题，未知子题个数
						var subQsts = _.map($scope.subQstList, function (item) {
							return {
								isObjective: item.isChoice,
								title: item.title,
								OptionOne: item.OptionOne,
								OptionTwo: item.OptionTwo,
								OptionThree: item.OptionThree,
								OptionFour: item.OptionFour,
								answer: getSubQAnswer(item),
								mp3file: item.mp3file
							}
						});
						var instances = CKEDITOR.instances;
						var fQst = {
							title: instances['fQTitle'].getData(),
							mode: $scope.modeType,
							sub: subQsts
						};
						$scope.saveCb({
							data: fQst
						});
						var uploadMp3Array = [];
						if($scope.mp3fileParent) {
							uploadMp3Array.push(uploadMP3($scope.mp3fileParent));
						}
						_.each(subQsts, function(item){
							if(item.mp3file) {
								uploadMp3Array.push(uploadMP3(item.mp3file));
							}
						});
						if(uploadMp3Array.length > 0) {
							$q.all(uploadMp3Array).then(function(){
								var args = $.makeArray(arguments[0]);
								if($scope.mp3fileParent) {
									fQst.mp3 = args.shift();
								}
								_.each(subQsts, function(item, index){
									if(item.mp3file) {
										item.mp3 = args[index];
									}
								});
								$scope.saveCb({
									data: fQst
								});
							});
						}else {
							$scope.saveCb({
								data: fQst
							});
						}
					}

					function getSearchResult(key) {
						$scope.queryResults = [];
						$scope.queryDone = false;
						var queryStr = '?q=Title:' + encodeURIComponent(key);
						if ($scope.qTypeId) {
							queryStr += '&fq=TypeId:' + $scope.qTypeId;
						}
						if ($scope.subjectId) {
							queryStr += '&fq=subjectId:' + $scope.subjectId;
						}
						queryStr += '&wt=json';
						$http.get(searchEngineUrl + queryStr).then(function (res) {
							$scope.queryDone = true;
							if (_.isArray(res.data.response.docs)) {
								var realArray = _.filter(res.data.response.docs, function (item) {
									return +item.Isdeleted === 0;
								});
								$scope.queryResults = _.map(realArray, function (item, index) {
									return {
										orders: index + 1,
										title: item.Title,
										fLnkId: item.FLnkID,
										OptionOne: item.OptionOne,
										OptionTwo: item.OptionTwo,
										OptionThree: item.OptionThree,
										OptionFour: item.OptionFour,
										ShowType: {id: item.ShowType},
										Answer: item.Answer
									}
								})
							}
						}, function () {
							$scope.queryDone = true;
							$scope.queryResults = [];
						});
					}

					function finishNormal() {
						//已知activeStepIndex = 1
						var subQst = $scope.subQstList[0];
						var qst = {
							isObjective: subQst.isChoice,
							title: subQst.title,
							OptionOne: subQst.OptionOne,
							OptionTwo: subQst.OptionTwo,
							OptionThree: subQst.OptionThree,
							OptionFour: subQst.OptionFour,
							answer: getSubQAnswer(subQst)
						};
						if(subQst.mp3file) {
							uploadMP3(subQst.mp3file).then(function(msg){
								qst.mp3 = msg;
								$scope.saveCb({
									data: qst
								});
							});
						}else {
							$scope.saveCb({
								data: qst
							});
						}
					}
						$scope.changeMP3 = function(subQst){
							if(subQst.mp3file) {
								subQst.fileSrc = window.URL.createObjectURL(subQst.mp3file);
							}else {
								subQst.fileSrc = '';
							}
						};

						$scope.updateParentMP3 = function(file){
							if(file) {
								$scope.parentMp3Src = window.URL.createObjectURL(file);
							}else {
								$scope.parentMp3Src = '';
							}
						};

						$scope.cancelParentFile = function(){
							$scope.mp3fileParent = '';
							$scope.parentMp3Src = '';
						};

						$scope.cancelSubMP3File = function(subQst){
							subQst.mp3file = '';
							subQst.fileSrc = '';
						};

						function uploadMP3(data){
							var defer = $q.defer();
							Upload.upload({
								url: $rootScope.baseUrl + '/uploadRes.ashx',
								data: [data]
							}).then(function(res){
								if(res.data.code === 0) {
									defer.resolve(res.data.msg);
								}else {
									constantService.showSimpleToast('MP3上传失败，请重试！');
									defer.reject();
								}
							}, function(){
								constantService.showSimpleToast('MP3上传失败，服务器错误！');
								defer.reject();
							});
							return defer.promise;
						}
						function getSubQAnswer(subQst) {
						var answer = '';
						if (subQst.isChoice) {
							var answerArr = [];
							if (subQst.isAAnswer) {
								answerArr.push('A');
							}
							if (subQst.isBAnswer) {
								answerArr.push('B');
							}
							if (subQst.isCAnswer) {
								answerArr.push('C');
							}
							if (subQst.isDAnswer) {
								answerArr.push('D');
							}
							answer = answerArr.join(',');
						} else {
							answer = subQst.Answer;
						}
						return answer;
					}
				}]
			}
		})
		.directive('richQModifier', function () {
			return {
				restrict: 'AE',
				replace: true,
				templateUrl: loadHtml('directive-extend/template/richQModifier.html'),
				scope: {
					saveCb: '&',
					question: '=',
					analysisVisible: '='
				},
				link: function (scope) {
					console.log(scope.question);
				},
				controller: ['$scope', '$q', '$rootScope', 'constantService', 'Upload',
					function ($scope, $q, $rootScope, constantService, Upload) {
					CKEDITOR.replaceAll();
                    $scope.question.Analysis = $scope.question.Analysis || $scope.question.analysis || '';
					var answer = $scope.question.Answer || $scope.question.answer || '';
					var options = ['A', 'B', 'C', 'D'];
					$scope.q = {
						isAAnswer: false,
						isBAnswer: false,
						isCAnswer: false,
						isDAnswer: false
					};
					if ($scope.question.OptionOne !== '') {
						_.each(options, function (item) {
							if (answer.indexOf(item) >= 0) {
								$scope.q['is' + item + 'Answer'] = true;
							} else {
								$scope.q['is' + item + 'Answer'] = false;
							}
						});
					}
					$scope.question.mp3file = $scope.question.mp3;
					$scope.updateMP3Preview = function(question){
						if(question.newMP3) {
							question.mp3file = window.URL.createObjectURL(question.newMP3);
						}else {
							question.mp3file = '';
						}
					};
					$scope.cancelUpdate = function(){
						$scope.question.mp3file = $scope.mp3;
					};
					$scope.addMultiAnswerTip = function(){
						constantService.multiAnswerTip();
					};
					$scope.save = function () {
						var instances = CKEDITOR.instances;
						var modifiedObj = {
							title: instances['question-content'].getData()
						};
						var newAnswer = '';
						if (!$scope.question.sub || _.isEmpty($scope.question.sub)) {
							_.extend(modifiedObj, {
								optionOne: instances['question-a'].getData(),
								optionTwo: instances['question-b'].getData(),
								optionThree: instances['question-c'].getData(),
								optionFour: instances['question-d'].getData(),
								OptionOne: instances['question-a'].getData(),
								OptionTwo: instances['question-b'].getData(),
								OptionThree: instances['question-c'].getData(),
								OptionFour: instances['question-d'].getData()
							});
							if (modifiedObj.optionOne !== '') {
								var temp = [];
								_.each(options, function (item) {
									if ($scope.q['is' + item + 'Answer']) {
										temp.push(item);
									}
								});
								newAnswer = temp.join(',')
							} else {
								newAnswer = instances['question-answer'].getData()
							}
							_.extend(modifiedObj, {
								answer: newAnswer,
								Answer: newAnswer
							});
							var newAnalysis = instances['question-analysis'].getData();
                            _.extend(modifiedObj, {
                                analysis: newAnalysis,
                                Analysis: newAnalysis
                            });
						}
						if($scope.question.newMP3 || $scope.question.mp3file !== $scope.question.mp3) {
							uploadMP3($scope.question.newMP3).then(function(msg){
								modifiedObj.mp3 = msg;
								$scope.saveCb({
									data: _.extend($scope.question, modifiedObj)
								});
							});
						}else{
                            $scope.saveCb({
                                data: _.extend($scope.question, modifiedObj)
                            });
						}
					};
					$scope.$on('$destory-ck', function(){
						_.each(CKEDITOR.instances, function (item) {
							if (item.document) {
								item.document.clearCustomData();
								item.document.removeAllListeners();
							}
							if (item.window && item.window.getFrame()) {
								item.window.getFrame().clearCustomData();
								item.window.getFrame().removeAllListeners();
							}
							$(item.container).remove();
							item.destroy(true);
							item = null;
						});
						_.each(CKEDITOR.filter.instances, function (item, index) {
							CKEDITOR.filter.instances[index] = null;
							delete CKEDITOR.filter.instances[index];
						});
					});
					function uploadMP3(data){
						var defer = $q.defer();
						Upload.upload({
							url: $rootScope.baseUrl + '/uploadRes.ashx',
							data: [data]
						}).then(function(res){
							if(res.data.code === 0) {
								defer.resolve(res.data.msg);
							}else {
								constantService.showSimpleToast('MP3上传失败，请重试！');
								defer.reject();
							}
						}, function(){
							constantService.showSimpleToast('MP3上传失败，服务器错误！');
							defer.reject();
						});
						return defer.promise;
					}
				}]
			}
		})
		.directive('normalQEditor', function () {
			return {
				restrict: 'AE',
				replace: true,
				templateUrl: loadHtml('directive-extend/template/normalQEditor.html'),
				scope: {
					saveCb: '&'
				},
				controller: ['$scope','constantService', function ($scope, constantService) {
					$scope.question = {
						isAAnswer: false,
						isBAnswer: false,
						isCAnswer: false,
						isDAnswer: false,
						isChoice: false
					};
					CKEDITOR.replaceAll();
					var answer = $scope.question.Answer;
					var options = ['A', 'B', 'C', 'D'];
                    $scope.addMultiAnswerTip = function(){
                        constantService.multiAnswerTip();
                    };
					$scope.save = function () {
						var instances = CKEDITOR.instances;
						var modifiedObj = {
							title: instances['question-content'].getData(),
							OptionOne: instances['question-a'].getData(),
							OptionTwo: instances['question-b'].getData(),
							OptionThree: instances['question-c'].getData(),
							OptionFour: instances['question-d'].getData()
						};
						var newAnswer = '';
						if ($scope.question.isChoice) {
							var temp = [];
							_.each(options, function (item) {
								if ($scope.question['is' + item + 'Answer']) {
									temp.push(item);
								}
							});
							newAnswer = temp.join(',');
						} else {
							newAnswer = instances['question-answer'].getData();
						}
						_.extend(modifiedObj, {
							answer: newAnswer,
							Answer: newAnswer,
							isChoice: $scope.question.isChoice
						});
						$scope.saveCb({
							data: modifiedObj
						});
					}
				}]
			}
		})
    .directive('editContent', function(){
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'directive-extend/template/edit-content.html',
            scope: {
                str: '=',
                cb: '&'
            },
            link: function(scope, element, attr){
                $(element).off('blur').on('blur', function(){
                    var text = $(this).text();
                    var isLegal = true;
                    if(attr.cb) {
                        isLegal = scope.cb({
                            text: text
                        });
                    }
                    if(isLegal) {
                        scope.$apply(function(){
                            scope.str = text;
                        });
                    }
                });
            }
        };
    })
;