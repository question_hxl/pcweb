/**
 * Created by Hexl on 2016/4/20.
 */
define(['underscore-min', 'jquery'], function () {
	return ['$http', '$scope', '$rootScope', '$state', '$location', '$q','ngDialog', '$timeout', '$sce',
		function ($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce) {

			$scope.iszuoguo = true;;
			var params = $location.search();
			var action = params.action,
				ExFLnkID = params.ExamId,
				TaskFLnkID = params.TaskId;
			var usedTime = 0;
			$scope.usedTime = getShowTime(usedTime);
			$scope.action = action;
			$http.post($rootScope.baseUrl + '/Interface0216.ashx',{
					examId: ExFLnkID
			}).then(function(res){
				var paper = res.data;
				if(paper) {
					$scope.paperTitle = paper.examName;
					$scope.groupList = paper.msg;
					_.each($scope.groupList, function(group){
						group.type = group.QTypeName;
						group.totalNum = +group.QNumber;
						group.avgScore = (+group.QTypeScores / +group.QNumber).toFixed(0);
						group.totalScore = (+group.QTypeScores).toFixed(0);
						group.questionList = group.question;
						if(+action === 2) {
							_.each(group.questionList, function(item){
								if(item.MyAnswer.toUpperCase().trim() === item.Answer.toUpperCase().trim()) {
									item.isRight = true;
								}else {
									item.isWrong = true;
								}
							});
						}
					});
				}
			});

			$scope.goQuestion = function(question){
				var order = question.orders;
				var $ele = $('#' + order);
				var ot = $ele.offset().top;
				$(window).scrollTop(ot);
			};

			$scope.submitPaper = function(){
				var answers = [];
				var rights = 0, wrong = 0, total = 0;
				_.each($scope.groupList, function(group){
					var questions = group.question;
					_.each(group.questionList, function(question, index){
						var score = '0';
						if(question.Answer.toUpperCase().trim() === question.stuAnswer) {
							score = question.score;
							rights += 1;
						}else {
							wrong += 1;
						}
						total += 1;
						answers.push({
							qFlnkId: question.FLnkID,
							scores: score,
							answer: question.stuAnswer || ''
						});
					});
				});
				$http.post($rootScope.baseUrl + '/Interface0217.ashx',{
					msg: answers,
					examFlnkId: ExFLnkID,
					userFLnkID: '',
					taskFlnkId: TaskFLnkID
				}).then(function(res){
					if (res.data.code === 0) {
						$scope.iszuoguo = false;
						ngDialog.open({
							template:
									'<p>一共<span style="color:#007ACC;">' + total + '</span>题，正确<span style="color:#ABDA5A;">' + rights + '</span>题，错误<span style="color:#DE5145;">' + wrong + '</span>题</p>' +
									'<div class="ngdialog-buttons">' +
									'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-plain'
						});
						return false;
						//alert('一共'+total+'题,正确'+rights+'题，错误'+wrong+'题！');
						$state.go('myApp.onlineSolution', {
							ExamId: ExFLnkID,
							action: 2,
							TaskId: TaskFLnkID
						});
					}
				});
			};

			/**
			 * 计时器
			 */
			setInterval(function(){
				usedTime += 1;
				$scope.usedTime = getShowTime(usedTime);
				$scope.$apply();
			}, 1000);
			function getShowTime(sec){
				return addZero(Math.floor(sec / 60)) + ':' + addZero(sec % 60);
			}
			function addZero(num){
				if(num >= 10) {
					return num + '';
				}else {
					return '0' + num;
				}
			}
			/**
			 * 页面滚动时固定右边
			 * @type {*|jQuery|HTMLElement}
			 */
			var $toFixEle = $('.onlineSolution .right');
			var offsetY = +$toFixEle.offset().top,
				offsetX = +$toFixEle.offset().left,
				width = $toFixEle.width();
			$(window).scroll(function () {
				var scrollTop = $(window).scrollTop();
				if (!_.isEmpty($toFixEle)) {
					if (scrollTop >= offsetY) {
						$toFixEle.css({
							position: 'fixed',
							left: offsetX,
							top: 0,
							'max-height': $(window).height() + 'px',
							overflow: 'auto'
						});
					} else {
						$toFixEle.css({
							position: 'static',
							right: 0,
							top: 0,
							'max-height': 'none'
						});
					}
				}
			});
		}
	];
});

