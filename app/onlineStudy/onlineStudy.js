/**
 * Created by Hexl on 2016/4/20.
 */
define(['underscore-min'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$location', '$q','ngDialog', '$timeout', '$sce',
        function ($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce) {
            $scope.currentMode = 1;
            $scope.papers = [];
            var unfinishPaper =[], finishPaper = [];
            $http.get($rootScope.baseUrl + '/Interface0215.ashx').then(function(res){
                if(res.data && res.data.msg) {
                    var papers = res.data.msg;
                    if(_.isArray(papers)) {
                        var groupPaper = _.groupBy(papers, function(item){
                            return item.IsFinish;
                        });
                        unfinishPaper = groupPaper['0'];
                        finishPaper = groupPaper['1'];
                        $scope.papers = unfinishPaper;
                    }
                }
            });

            $scope.changeMode = function(mode){
                $scope.currentMode = +mode;
            };

            $scope.goSolution = function(paper){
                $state.go('myApp.onlineSolution', {
                    ExamId: paper.ExFLnkID,
                    action: 1,
                    TaskId: paper.TaskFLnkID
                });
            };

            $scope.checkPaper = function(paper){
                $state.go('myApp.onlineSolution', {
                    ExamId: paper.ExFLnkID,
                    action: 2,
                    TaskId: paper.TaskFLnkID
                });
            };

            $scope.$watch('currentMode', function(mode){
                if(mode) {
                    if(mode === 1) {
                        $scope.papers = unfinishPaper;
                    }else {
                        $scope.papers = finishPaper;
                    }
                }
            });
        }
    ]
});

