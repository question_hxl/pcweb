/**
 * Created by 贺小雷 on 2016/10/8.
 */
(function($){
    $.fn.wyCarousel = function(option){
        var defaults = {
            step: 'all',
            auto: false,
            preClass: '.pre',
            nextClass: '.next'
        };
        $.extend(defaults, option);
        $(this).find('ul').css({
            position: 'relative',
            width: '9999px',
            transition: 'all ease .4s'
        });
        var $box = $(this);
        var $item = $(this).find('ul li');
        var itemWidth = $item.innerWidth() + 5,
            viewPortWidth = $(this).innerWidth() - 40;
        var countPerPage = Math.floor(viewPortWidth / itemWidth);
        var currentPage = 0;
        var total = $item.length;
        var totalPage = Math.ceil(total / countPerPage);
        if(totalPage === 1) {
            $(this).find(defaults.preClass).hide();
            $(this).find(defaults.nextClass).hide();
        }else {
            $(this).find(defaults.preClass).addClass('disable');
        }
        $(this).find(defaults.nextClass).click(function(){
            if(currentPage < totalPage - 1) {
                currentPage++;
                move();
            }
        });

        $(this).find(defaults.preClass).click(function(){
            if(currentPage > 0) {
                currentPage--;
                move();
            }
        });

        function move(){
            $box.find('ul').css({
                left: (-1 * currentPage * itemWidth * countPerPage) + 'px'
            });
            $box.find(defaults.preClass).removeClass('disable');
            $box.find(defaults.nextClass).removeClass('disable');
            if(currentPage >= totalPage - 1) {
                $box.find(defaults.nextClass).addClass('disable');
            }else if(currentPage === 0) {
                $box.find(defaults.preClass).addClass('disable');
            }
        }
    }
})(jQuery);