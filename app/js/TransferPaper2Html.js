/**
 * Created by 贺小雷 on 2016/9/28.
 */
(function($, _){
    var INLINE_TAGS = ['a', 'b','bdo', 'big', 'br', 'code', 'em', 'i', 'img', 'input', 'label', 'q' ,'s', 'small', 'span', 'strike', 'strong', 'sub', 'sup', 'u'];
    $.PaperTransfer = function(paper){
        this.paper = paper;
        this.normalStyle = "font-size:12pt;line-height:12pt;" +
            'font-family:宋体;mso-fareast-font-family:宋体;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;' +
            'mso-fareast-language:ZH-CN;mso-bidi-language:AR-SA';
        this.$main = $('<div class="box" lang=ZH-CN style="'+this.styleText+'">');
        this.NoMap = {
            0: '',
            1: '一',
            2: '二',
            3: '三',
            4: '四',
            5: '五',
            6: '六',
            7: '七',
            8: '八',
            9: '九',
            10: '十'
        };
    };

    $.PaperTransfer.prototype = {
        init: function(){
            this.$paperTitle = $('<h3 style="' + this.normalStyle + '">').text(this.paper.examName).css({
                'font-size': '18pt',
                'font-weight': 'bold',
                'text-align': 'center',
                'line-height': '20pt'
            }).appendTo(this.$main);
            $('<br>').appendTo(this.$main);
            $('<br>').appendTo(this.$main);
        },
        parseGroups: function(groups){
            var self = this;
            var defer = $.Deferred();
            var count = 0;
            var deferArray = [];
            _.each(groups, function(group, index){
                var titleText = group.Dtype + (group.Explain || ('（'+ group.QTypeScores + '分）'));
                var $groupTitle = $('<h4 style="'+self.normalStyle+'">').text(self.getCNNoByIndex(index+1) + '、'+ titleText).appendTo(self.$main);
                var $groupBox = $('<p style="'+self.normalStyle+'">').appendTo(self.$main);
                (function(g, gb){
                    _.each(g.question, function(q){
                        deferArray.push(self.parseQuestion(q, gb));
                    });
                })(group, $groupBox);
            });
            $.when.apply(this, deferArray).done(function(){
                defer.resolve();
            });
            return defer;
        },
        parseQuestion: function(qst, box){
            var defer = $.Deferred();
            var deferList = [];
            var self = this;
            if(qst.title) {
                var title = '';
                if(qst.Mode === 'A') {
                    title = this.getToRenderContent(qst.title, '', true);
                }else {
                    title = this.getToRenderContent(qst.title, qst.orders, qst.noSplitor);
                }
                var $title = $('<p style="'+this.normalStyle+'">').html(title).appendTo(box).css({
                    'font-size': '10.5pt',
                    'line-height': '0pt'
                });
                deferList.push(this.dealWithImgInNode($title));
            }
            if(qst.Mode === 'A') {
                var $subBox = $('<p style="'+this.normalStyle+'">').appendTo(box).css({
                    'font-size': '10.5pt',
                    'line-height': '0pt'
                });
                _.each(qst.sub, function(s){
                    deferList.push(self.parseQuestion(s, $subBox));
                });
            }else if(qst.Mode === 'B') {
                var $subBox = $('<p style="'+this.normalStyle+'">').appendTo(box).css({
                    'font-size': '10.5pt',
                    'line-height': '0pt'
                });
                _.each(qst.sub, function(s, index){
                    s.orders = '（' + (index + 1) + '）';
                    s.noSplitor = true;
                    deferList.push(self.parseQuestion(s, $subBox));
                });
            }else {
                if(qst.ShowType === '0') {
                    if(qst.OptionOne) {
                        var optionA = this.getToRenderContent(qst.OptionOne, 'A', false, true);
                        var $optionA = $('<p style="'+this.normalStyle+'">').html(optionA).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionA));
                    }
                    if(qst.OptionTwo) {
                        var optionB = this.getToRenderContent(qst.OptionTwo, 'B', false, true);
                        var $optionB = $('<p style="'+this.normalStyle+'">').html(optionB).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionB));
                    }
                    if(qst.OptionThree) {
                        var optionC = this.getToRenderContent(qst.OptionThree, 'C', false, true);
                        var $optionC = $('<p style="'+this.normalStyle+'">').html(optionC).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionC));
                    }
                    if(qst.OptionFour) {
                        var optionD = this.getToRenderContent(qst.OptionFour, 'D', false, true);
                        var $optionD = $('<p style="'+this.normalStyle+'">').html(optionD).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionD));
                    }
                }else if(qst.ShowType === '1') {
                    if(qst.OptionOne || qst.OptionTwo) {
                        var optionA = this.getToRenderContent(qst.OptionOne, 'A', false, true);
                        var optionB = this.getToRenderContent(qst.OptionTwo, 'B', false, true);
                        var optionHtml = optionA + '&nbsp;&nbsp;' + optionB;
                        var $optionDom = $('<p style="'+this.normalStyle+'">').html(optionHtml).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionDom));
                    }
                    if(qst.OptionThree || qst.OptionFour) {
                        var optionC = this.getToRenderContent(qst.OptionThree, 'C', false, true);
                        var optionD = this.getToRenderContent(qst.OptionFour, 'D', false, true);
                        var optionHtml = optionC + '&nbsp;&nbsp;' + optionD;
                        var $optionDom = $('<p style="'+this.normalStyle+'">').html(optionHtml).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionDom));
                    }
                }else {
                    if(qst.OptionOne || qst.OptionTwo || qst.OptionThree || qst.OptionFour) {
                        var optionA = this.getToRenderContent(qst.OptionOne, 'A', false, true);
                        var optionB = this.getToRenderContent(qst.OptionTwo, 'B', false, true);
                        var optionC = this.getToRenderContent(qst.OptionThree, 'C', false, true);
                        var optionD = this.getToRenderContent(qst.OptionFour, 'D', false, true);
                        var optionHtml = optionA + '&nbsp;&nbsp;' + optionB + '&nbsp;&nbsp;' + optionC + '&nbsp;&nbsp;' + optionD;
                        var $optionDom = $('<p style="'+this.normalStyle+'">').html(optionHtml).appendTo(box).css({
                            'font-size': '10.5pt',
                            'line-height': '0pt'
                        });
                        deferList.push(this.dealWithImgInNode($optionDom));
                    }
                }
            }
            $.when.apply(this, deferList).done(function(res){
                defer.resolve();
            });
            return defer;
        },
        run: function(){
            var defer = $.Deferred();
            var self = this;
            this.init();
            this.parseGroups(this.paper.msg).done(function(){
                self.parseAnswer4Paper(self.paper.msg);
                defer.resolve();
            });
            return defer;
        },
        getToRenderContent: function(text, pre, noSplitor, isOption){
            var context = '';
            var splitor = noSplitor ? '' : '.';
            try{
                var tagName = $(text).get(0).tagName;
                if(tagName && !!$(text).html() && text.startsWith('<')) {
                    var closeTagIndex = text.indexOf('>');
                    if(this.isInlineTag(tagName)) {
                        context = pre + splitor + text;
                    }else {
                        if(isOption) {
                            if($(text).length === 1 && $.trim(text).endsWith('>')) {
                                context = pre + splitor + text.slice(closeTagIndex + 1, - (tagName.length + 4));
                            }else {
                                context = text.slice(0, closeTagIndex + 1) + pre + splitor + text.slice(closeTagIndex + 1);
                            }
                        }else {
                            context = text.slice(0, closeTagIndex + 1) + pre + splitor + text.slice(closeTagIndex + 1);
                        }
                    }
                }else {
                    context = (text && pre + splitor + text) || '';
                }
            }catch(e) {
                context = (text && pre + splitor + text) || '';
            }
            context = context.replace(/&nbsp;/gi, ' ');
            if(isOption) {
                context = context.replace(/<br>/gi, '');
                context = context.replace(/<br.\/>/ig, '');
            }
            context = context.replace(/\n/ig, '');
            return context;
        },
        dealWithImgInNode: function(node){
            var defer = $.Deferred();
            var $allEle = node.find('*'), $allImg = node.find('img'), $allTable = node.find('table');
            var self = this;
            $allEle.each(function(j, ele){
                $(ele).get(0).style.cssText = $(ele).get(0).style.cssText + ';' + self.normalStyle;
                $(ele).css({
                    'font-size': '10.5pt',
                    'line-height': '0pt'
                });
            });
            $allTable.addClass('MsoTableGrid');
            var count = 0;
            if($allImg.length > 0) {
                $allImg.each(function(j, img){
                    var url = $(img).attr('src');
                    (function(e){
                        self.img2Base64(url, function(data, height, width){
                            count++;
                            if(data) {
                                var eleWidth = parseInt(e.attr('width')) || width,
                                    eleHeight = parseInt(e.attr('height')) || height;
                                var offset = (height * 0.75 * (eleWidth / width) - 10) / 2;
                                var styleText = 'position:relative;mso-text-raise:'+(-1 * offset) + 'pt;top:'+offset+'pt;';
                                e.replaceWith($("<span style='"+styleText+"'><img src='"+e.attr('src')+"'" +
                                    " width='"+eleWidth+"px' height='"+eleHeight+"px'></span>"));
                            }
                            if(count >= $allImg.length) {
                                defer.resolve();
                            }
                        });
                    })($(img));
                });
            }else {
                defer.resolve();
            }
            return defer;
        },
        img2Base64: function(url, callback, outputFormat){
            var canvas = document.createElement('CANVAS'),
                ctx = canvas.getContext('2d'),
                img = new Image;
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img,0,0);
                var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                callback.call(this, dataURL, img.height, img.width);
                canvas = null;
            };
            img.onerror = function(){
                callback.call(this, '', 0, 0);
            };
            img.src = url;
        },
        isInlineTag: function(tagname){
            return $.inArray(tagname.toLowerCase(), INLINE_TAGS) >= 0;
        },
        getCNNoByIndex: function (index) {
            if (!index) {
                return '';
            }
            var shang = Math.floor(index / 10),
                yu = index % 10;
            var cnStr = '';
            if (shang > 1) {
                cnStr = this.NoMap[shang] + this.NoMap[10] + this.NoMap[yu];
            } else if (shang === 1) {
                cnStr = this.NoMap[10] + this.NoMap[yu];
            } else {
                cnStr = this.NoMap[yu];
            }
            return cnStr;
        },
        parseAnswer4Paper: function(groups){
            $('<span lang=EN-US style=\'font-size:12.0pt;line-height:150%;font-family:"Times New Roman";' +
                'mso-fareast-font-family:宋体;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;' +
                'mso-fareast-language:ZH-CN;mso-bidi-language:AR-SA\'><br clear=all  ' +
                'style=\'mso-special-character:page-break;page-break-before:always\'></span>').appendTo(this.$main);
            $('<h3 style="' + this.normalStyle + '">').text(this.paper.examName + '答案').css({
                'font-size': '18pt',
                'font-weight': 'bold',
                'text-align': 'center',
                'line-height': '20pt'
            }).appendTo(this.$main);
            $('<br>').appendTo(this.$main);
            $('<br>').appendTo(this.$main);
            var self = this;
            _.each(groups, function(group, index){
                var titleText = group.Dtype + (group.Explain || ('（'+ group.QTypeScores + '分）'));
                var $groupTitle = $('<h4 style="'+self.normalStyle+'">').text(self.getCNNoByIndex(index+1) + '、'+ titleText).appendTo(self.$main);
                var $groupBox = $('<p style="'+self.normalStyle+'">').appendTo(self.$main);
                _.each(group.question, function(qst){
                    if(qst.Mode === 'A') {
                        _.each(qst.sub, function(s){
                            $('<p style="'+self.normalStyle+'">').html(self.getToRenderContent(s.Answer, s.orders)).appendTo($groupBox);
                        });
                    }else if(qst.Mode === 'B') {
                        $('<p style="'+self.normalStyle+'">').html(self.getToRenderContent('&nbsp;', qst.orders)).appendTo($groupBox);
                        _.each(qst.sub, function(s, index){
                            s.orders = '（' + (index + 1) + '）';
                            $('<p style="'+self.normalStyle+'">').html(self.getToRenderContent(s.Answer, s.orders, true)).appendTo($groupBox);
                        });
                    }else {
                        $('<p style="'+self.normalStyle+'">').html(self.getToRenderContent(qst.Answer, qst.orders)).appendTo($groupBox);
                    }
                });
            });
        },
        exportToDoc: function(){
            var self = this;
            var str = '<html xmlns:v="urn:schemas-microsoft-com:vml"' +
                ' xmlns:o="urn:schemas-microsoft-com:office:office"' +
                ' xmlns:w="urn:schemas-microsoft-com:office:word"' +
                ' xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"' +
                ' xmlns="http://www.w3.org/TR/REC-html40">';
            str+='<head><meta charset="utf-8"><meta name=ProgId content=Word.Document><meta name=Generator content="Microsoft Word 14">' +
                '<meta name=Originator content="Microsoft Word 14"><!--[if gte mso 9]><xml><w:WordDocument><w:View>Print</w:View><w:TrackMoves>false</w:TrackMoves>' +
                '<w:TrackFormatting/><w:ValidateAgainstSchemas/><w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid><w:IgnoreMixedContent>false</w:IgnoreMixedContent>' +
                '<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText><w:DoNotPromoteQF/><w:LidThemeOther>EN-US</w:LidThemeOther><w:LidThemeAsian>ZH-CN</w:LidThemeAsian>' +
                '<w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript><w:Compatibility><w:BreakWrappedTables/><w:SnapToGridInCell/><w:WrapTextWithPunct/><w:UseAsianBreakRules/>' +
                '<w:DontGrowAutofit/><w:SplitPgBreakAndParaMark/><w:DontVertAlignCellWithSp/><w:DontBreakConstrainedForcedTables/><w:DontVertAlignInTxbx/><w:Word11KerningPairs/>' +
                '<w:CachedColBalance/><w:UseFELayout/></w:Compatibility><w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel><m:mathPr><m:mathFont m:val="Cambria Math"/>' +
                '<m:brkBin m:val="before"/><m:brkBinSub m:val="--"/><m:smallFrac m:val="off"/><m:dispDef/><m:lMargin m:val="0"/> <m:rMargin m:val="0"/><m:defJc m:val="centerGroup"/>' +
                '<m:wrapIndent m:val="1440"/><m:intLim m:val="subSup"/><m:naryLim m:val="undOvr"/></m:mathPr></w:WordDocument></xml><![endif]--><style>' +
                'table{mso-style-name:网格型;mso-tstyle-rowband-size:0;mso-tstyle-colband-size:0;mso-style-priority:59;mso-style-unhide:no;' +
                'border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.5pt solid windowtext;' +
                'mso-border-insidev:.5pt solid windowtext;mso-para-margin:0cm;mso-para-margin-bottom:.0001pt;mso-pagination:widow-orphan;' +
                'font-size:10.5pt;font-family:"Times New Roman","serif";mso-ascii-font-family:"Times New Roman";mso-ascii-theme-font:minor-latin;' +
                'mso-hansi-font-family:"Times New Roman";mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";' +
                'mso-bidi-theme-font:minor-bidi;}</style></head>';
            str += "<body>";
            this.run().done(function(){
                str += self.$main.html();
                str += "</body></html>";
                var blob = new Blob([str], {
                    type: "application/msword;charset=utf-8"
                });
                saveAs(blob, self.paper.examName + ".doc");
            });
        },
        getHtml: function(){
            var defer = $.Deferred();
            var self = this;
            var str = '<html xmlns:v="urn:schemas-microsoft-com:vml"' +
                ' xmlns:o="urn:schemas-microsoft-com:office:office"' +
                ' xmlns:w="urn:schemas-microsoft-com:office:word"' +
                ' xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"' +
                ' xmlns="http://www.w3.org/TR/REC-html40">';
            str+='<head><meta charset="utf-8"><meta name=ProgId content=Word.Document><meta name=Generator content="Microsoft Word 14">' +
                '<meta name=Originator content="Microsoft Word 14"><!--[if gte mso 9]><xml><w:WordDocument><w:View>Print</w:View><w:TrackMoves>false</w:TrackMoves>' +
                '<w:TrackFormatting/><w:ValidateAgainstSchemas/><w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid><w:IgnoreMixedContent>false</w:IgnoreMixedContent>' +
                '<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText><w:DoNotPromoteQF/><w:LidThemeOther>EN-US</w:LidThemeOther><w:LidThemeAsian>ZH-CN</w:LidThemeAsian>' +
                '<w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript><w:Compatibility><w:BreakWrappedTables/><w:SnapToGridInCell/><w:WrapTextWithPunct/><w:UseAsianBreakRules/>' +
                '<w:DontGrowAutofit/><w:SplitPgBreakAndParaMark/><w:DontVertAlignCellWithSp/><w:DontBreakConstrainedForcedTables/><w:DontVertAlignInTxbx/><w:Word11KerningPairs/>' +
                '<w:CachedColBalance/><w:UseFELayout/></w:Compatibility><w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel><m:mathPr><m:mathFont m:val="Cambria Math"/>' +
                '<m:brkBin m:val="before"/><m:brkBinSub m:val="--"/><m:smallFrac m:val="off"/><m:dispDef/><m:lMargin m:val="0"/> <m:rMargin m:val="0"/><m:defJc m:val="centerGroup"/>' +
                '<m:wrapIndent m:val="1440"/><m:intLim m:val="subSup"/><m:naryLim m:val="undOvr"/></m:mathPr></w:WordDocument></xml><![endif]--><style>' +
                'table{mso-style-name:网格型;mso-tstyle-rowband-size:0;mso-tstyle-colband-size:0;mso-style-priority:59;mso-style-unhide:no;' +
                'border:solid windowtext 1.0pt;mso-border-alt:solid windowtext .5pt;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.5pt solid windowtext;' +
                'mso-border-insidev:.5pt solid windowtext;mso-para-margin:0cm;mso-para-margin-bottom:.0001pt;mso-pagination:widow-orphan;' +
                'font-size:10.5pt;font-family:"Times New Roman","serif";mso-ascii-font-family:"Times New Roman";mso-ascii-theme-font:minor-latin;' +
                'mso-hansi-font-family:"Times New Roman";mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";' +
                'mso-bidi-theme-font:minor-bidi;}</style></head>';
            str += "<body>";
            this.run().done(function(){
                str += self.$main.html();
                str += "</body>";
                defer.resolve(str);
            });
            return defer;
        }
    };
})(jQuery, _);