/**
 * Created by 赵文东 on 2017/2/23.
 */
define(['dragable'],function () {
    return ['$scope','$http','$rootScope','ngDialog','constantService','$location','$state',function ($scope,$http,$rootScope,ngDialog,constantService,$location,$state) {
        //自动回到顶部
        $(document).scrollTop(0);
        var unifiedId = $location.search().unifiedId;
        $scope.unifiedName = $location.search().unifiedName;
        $scope.data = {
            allQuestionList : [],
            allQType : []
        };
        $scope.queueList = [
            {name: '首页', url: 'myApp.masterHome'},
            {name: '考试中心', url: 'myApp.examCenter'},
            {name: '新建空白卷', url: 'myApp.newBlankPaper'}];
        $scope.back = function () {
            $state.go('myApp.examCenter');
        };
        //正整数验证
        var testpostive = /^[0-9]*[1-9][0-9]*$/;
        //0.5整数倍验证
        var testhalf = /^\d+(\.[05])?$/;
        $scope.data.allQuestionList = persetdata($scope.data.allQuestionList);
        $http.post($rootScope.baseUrl + '/generalQuery.ashx',{Proc_name:'Proc_GetKmQuestionsTypeLimit'}).then(function (res) {
            res.data.msg = handelQtype(res.data.msg);
            $scope.data.allQType = res.data.msg;
        },function (res) {
            $scope.data.allQType = [];
        });
        $scope.addnewQst = function (qstitem,index) {
            if(!$scope.data.allQType.length>0){
                constantService.showSimpleToast('获取题目类型失败!');
                return;
            }
            var dialog = ngDialog.open({
                template:'qstType',
                className: 'ngdialog-theme-default',
                appendClassName:'newBlankPaper-addnewQst',
                scope:$scope,
                closeByDocument:false,
                controller:function ($scope) {
                    return function () {
                        $scope.selectQtype = qstitem.qtype;
                        $scope.currentQstNum = qstitem.qstcount;
                        $scope.selectQstType = function (value) {
                            $scope.selectQtype = value;
                            $scope.data.allQuestionList[index].qtype = $scope.selectQtype;
                            $scope.data.allQuestionList[index].qtypename = $scope.selectQtype.showTypeName;
                            $scope.data.allQuestionList[index].qstcount = $scope.currentQstNum;
                            $scope.data.allQuestionList[index].name = !!$scope.data.allQuestionList[index].name ? $scope.data.allQuestionList[index].name : $scope.selectQtype.showTypeName;
                            $scope.selectQtype = [];
                            $scope.currentQstNum = null;
                            if($scope.data.allQuestionList[index].isplaceholder){
                                $scope.data.allQuestionList[index].isplaceholder = false;
                                $scope.data.allQuestionList = persetdata($scope.data.allQuestionList);
                            }
                            ngDialog.closeAll();
                        };
                    }
                }
            });
        };
        function persetdata(list) {
            var temppersetdata = {qtypename:'添加题型',
                qtype:undefined,
                qstcount:undefined,
                beginnum:undefined,
                endnum:undefined,
                qstscore:undefined,
                totalscore:undefined,
                name:undefined,
                isplaceholder:true,
                isbegnocorrect:false,
                isendnocorrect:true,
                isscorelegal:true,
                avgChange: true,
                totalChange: true
            };
            list.push(temppersetdata);
            return list;
        }
        //dragable 拖拽插件处理
        /*var dragable = $('#newBlankPaper').dragable({
            ele: '.questionitem',
            container: '.questionlist',
            dragstartCb: function (e, data) {
            },
            ondragCb: function (ev, data) {

            },
            dragendCb: function (ev, $target) {
                var dragEleOrder = +(_.find($(ev.target).parent().children(),function (item) {
                    return $(item).hasClass('order');
                }).innerHTML)-1;
                var aimEleOrder = +(_.find($target.children(),function (item) {
                    return $(item).hasClass('order');
                }).innerHTML)-1;
                $scope.$apply(function () {
                    var fromele = $scope.data.allQuestionList[dragEleOrder];
                    var toele = $scope.data.allQuestionList[aimEleOrder];
                    $scope.data.allQuestionList.splice(dragEleOrder,1);
                    var newaimEleOrder = _.findIndex($scope.data.allQuestionList,toele);
                    $scope.data.allQuestionList.splice(newaimEleOrder,0,fromele);
                });
            }
        });*/
        $scope.deleteCurQst = function (item,index) {
            if(item.isplaceholder){
                return;
            }
            $scope.data.allQuestionList.splice(index,1);
        };
        $scope.savePaper = function () {
            var reqlist = [];
            for(var i = 0;i<$scope.data.allQuestionList.length;i++){
                if($scope.judgeNum($scope.data.allQuestionList[i].qstscore)) {
                    return
                }
                if($scope.data.allQuestionList[i].isplaceholder){
                    continue;
                }else{
                    if($scope.data.allQuestionList[i].isbegnocorrect&&$scope.data.allQuestionList[i].isendnocorrect
                        &&$scope.data.allQuestionList[i].isscorelegal&&$scope.data.allQuestionList[i].name){
                        $scope.data.allQuestionList[i].qstcount = (($scope.data.allQuestionList[i].endnum === undefined || $scope.data.allQuestionList[i].endnum === null)?$scope.data.allQuestionList[i].beginnum:$scope.data.allQuestionList[i].endnum) - $scope.data.allQuestionList[i].beginnum + 1;
                        $scope.data.allQuestionList[i].totalscore = $scope.data.allQuestionList[i].qstscore * $scope.data.allQuestionList[i].qstcount;
                        reqlist.push({
                            groupname:$scope.data.allQuestionList[i].name,
                            grouptype:$scope.data.allQuestionList[i].qtype.QtypeId,
                            startNo:$scope.data.allQuestionList[i].beginnum,
                            endNo:($scope.data.allQuestionList[i].endnum === undefined || $scope.data.allQuestionList[i].endnum === null)?$scope.data.allQuestionList[i].beginnum:$scope.data.allQuestionList[i].endnum,
                            questionScore:$scope.data.allQuestionList[i].qstscore || 0,
                            groupScore:$scope.data.allQuestionList[i].totalscore || 0,
                            groupQno:$scope.data.allQuestionList[i].qstcount
                        });
                    }else{
                        constantService.alert('您还有题目组信息未输入正确或者信息缺失!');
                        return;
                    }
                }
            }
            for( var m = 0, len = $scope.data.allQuestionList.length - 1; m < len; m ++) {
                for(var n = m + 1; n < len; n ++) {
                    if($scope.data.allQuestionList[m].name === $scope.data.allQuestionList[n].name) {
                        if(!($scope.data.allQuestionList[m].beginnum > $scope.data.allQuestionList[n].endnum || $scope.data.allQuestionList[m].endnum < $scope.data.allQuestionList[n].beginnum)) {
                            constantService.alert('题目名称都一致的组，题号不能重复');
                            return
                        }
                    }
                }
            }
            if(reqlist.length === 0){
                constantService.alert('请录入题目信息在保存！');
            }else{
                $http.post('' + '/BootStrap/schoolmanager/saveNewBlankPaper.ashx',{
                    unifiedId:unifiedId,
                    questionList:reqlist
                }).then(function (res) {
                    if(+res.data.code === 0){
                        setOrders(reqlist);
                    }else{
                        constantService.alert('保存失败，稍后再试！');
                    }
                },function (res) {
                    constantService.alert('保存失败，稍后再试！');
                });
            }
        };
        // 单独设置题号，先查询统考信息，在调保存接口
        function setOrders(reqlist) {
            var param = [];
            var i;
            _.each(reqlist, function (item) {
                for(i = 0; i < item.groupQno; i ++) {
                    param.push({
                        dType: item.groupname,
                        showDis: +item.startNo + i,
                        fullScore: item.questionScore
                    })
                }
            });
            $http.post('' + '/BootStrap/EncryptUnified/getPaperConfig.ashx', {
                unifiedId: unifiedId
            }).then(function (res) {
                if(+res.data.code === 0) {
                    var curIndex = 0;
                    _.each(res.data.msg.meta, function (qst) {
                        _.each(qst.question, function (item, index) {
                            curIndex += 1;
                            param[curIndex - 1].qFlnkId = item.qFlnkId;
                            param[curIndex - 1].orders = item.orders;
                            param[curIndex - 1].answer = item.answer;
                            param[curIndex - 1].difficultLevel = item.difficultLevel;
                            param[curIndex - 1].missScore = item.missScore;
                        });
                    });
                    $http.post('' + '/BootStrap/schoolmanager/updatePaperConfig.ashx', {//保存页面修改
                        unifiedId: unifiedId,
                        meta: param,
                        needResetEncrypt: 0
                    }).then(function (req) {
                        if (req.data.code === 0) {
                            constantService.alert('保存成功!',function () {
                                history.go(-1);
                            });
                        }
                    });
                }
            });
        }
        $scope.checkNo = function (no,index,from) {
            if($scope.data.allQuestionList[index].endnum-$scope.data.allQuestionList[index].beginnum>99){
                constantService.alert('最大题目数不可以超过100');
                $scope.data.allQuestionList[index].endnum = $scope.data.allQuestionList[index].beginnum+99
            }else{
                //当结束题号为空时 添加一道题目 非错误情况
                if(!(from === 1) && ($scope.data.allQuestionList[index].endnum === undefined || $scope.data.allQuestionList[index].endnum === null)){
                    $scope.data.allQuestionList[index].isendnocorrect = true;
                    return;
                }
                if(no === undefined || no === null){
                    from===1?$scope.data.allQuestionList[index].isbegnocorrect = false:$scope.data.allQuestionList[index].isendnocorrect = false;
                    return;
                }
                if(!testpostive.test(no)){
                    from===1?$scope.data.allQuestionList[index].isbegnocorrect = false:$scope.data.allQuestionList[index].isendnocorrect = false;
                    constantService.alert('题号仅能为正整数!');
                }else{
                    if((!!$scope.data.allQuestionList[index].beginnum&&!!$scope.data.allQuestionList[index].endnum)&&
                        ($scope.data.allQuestionList[index].beginnum>$scope.data.allQuestionList[index].endnum)){
                        $scope.data.allQuestionList[index].isendnocorrect = false;
                        constantService.alert('结束题号应大于开始题号!');
                        return;
                    }else if((!!$scope.data.allQuestionList[index].beginnum && !!$scope.data.allQuestionList[index].endnum) &&
                    ($scope.data.allQuestionList[index].beginnum <= $scope.data.allQuestionList[index].endnum)){
                        $scope.data.allQuestionList[index].isbegnocorrect = true;
                        $scope.data.allQuestionList[index].isendnocorrect = true;
                    } else {
                        from===1?$scope.data.allQuestionList[index].isbegnocorrect = true:$scope.data.allQuestionList[index].isendnocorrect = true;
                    }
                }
            }
            if($scope.data.allQuestionList[index].totalChange && !!$scope.data.allQuestionList[index].totalscore && !!$scope.data.allQuestionList[index].endnum&&!!$scope.data.allQuestionList[index].beginnum) {
                $scope.data.allQuestionList[index].avgChange = false;
                $scope.data.allQuestionList[index].qstscore = $scope.data.allQuestionList[index].totalscore / +($scope.data.allQuestionList[index].endnum-$scope.data.allQuestionList[index].beginnum + 1);
            }else if($scope.data.allQuestionList[index].avgChange && !!$scope.data.allQuestionList[index].qstscore && !!$scope.data.allQuestionList[index].endnum&&!!$scope.data.allQuestionList[index].beginnum) {
                $scope.data.allQuestionList[index].totalChange = false;
                $scope.data.allQuestionList[index].totalscore = !!$scope.data.allQuestionList[index].endnum&&!!$scope.data.allQuestionList[index].beginnum&&!!$scope.data.allQuestionList[index].qstscore?+(($scope.data.allQuestionList[index].endnum-$scope.data.allQuestionList[index].beginnum+1)*$scope.data.allQuestionList[index].qstscore):'';
            }
            $scope.totalScoreAll = 0;
            for (var i = 0, len = $scope.data.allQuestionList.length; i < len - 1; i++){
                $scope.totalScoreAll += $scope.data.allQuestionList[i].totalscore ? $scope.data.allQuestionList[i].totalscore : 0
            }
        };
        $scope.checkScore = function (score, index, type) {
            if (score === undefined) {
                return;
            }
            if (score < 0) {
                constantService.alert('分数不可以为负!');
                $scope.data.allQuestionList[index].qstscore = 0;
            } else {
                if (!(testpostive.test(score) || testhalf.test(score) || !score)) {
                    constantService.alert('分数仅能为0.5的整数倍!');
                } else {
                    if (+type === 1) {
                        if (score > 100) {
                            constantService.alert('分数不可以大于100分!');
                            $scope.data.allQuestionList[index].qstscore = 100;
                            //$scope.data.allQuestionList[index].isscorelegal = true;
                        } else {
                            $scope.data.allQuestionList[index].totalChange = !(!!score);
                           // $scope.data.allQuestionList[index].isscorelegal = true;
                        }
                        $scope.data.allQuestionList[index].totalscore = !!$scope.data.allQuestionList[index].endnum&&!!$scope.data.allQuestionList[index].beginnum&&!!$scope.data.allQuestionList[index].qstscore?+(($scope.data.allQuestionList[index].endnum-$scope.data.allQuestionList[index].beginnum+1)*$scope.data.allQuestionList[index].qstscore):''
                    } else {
                        //$scope.data.allQuestionList[index].avgChange = true;
                        if(!!$scope.data.allQuestionList[index].endnum&&!!$scope.data.allQuestionList[index].beginnum&&!!$scope.data.allQuestionList[index].totalscore) {
                            var avg = $scope.data.allQuestionList[index].totalscore / +($scope.data.allQuestionList[index].endnum-$scope.data.allQuestionList[index].beginnum + 1);
                            $scope.data.allQuestionList[index].qstscore = avg;
                            $scope.data.allQuestionList[index].avgChange = !(!!score);
                        } else {
                            $scope.data.allQuestionList[index].qstscore = '';
                            $scope.data.allQuestionList[index].avgChange = true;
                        }
                    }
                    $scope.totalScoreAll = 0;
                    for (var i = 0, len = $scope.data.allQuestionList.length; i < len - 1; i++){
                        $scope.totalScoreAll += $scope.data.allQuestionList[i].totalscore ? $scope.data.allQuestionList[i].totalscore : 0
                    }
                }
            }
        };
        $scope.judgeNum = function (num) {
            if(num) {
                return !testhalf.test(num)
            }
        };
        $scope.goBack = function () {
            history.go(-1);
        };
        /**
         * 将获取到的题目类型中 选择题展示为 单选题 非选择题展示为解答题及其他
         * 修改后仅作为展示使用 不影响平台交互及功能 故增加字段以示区分
         */
        function handelQtype(list) {
            _.each(list,function (item) {
                if(item.QtypeId === 338){
                    item.showTypeName = '单选题';
                }else if(item.QtypeId === 307){
                    item.showTypeName = '解答题等其他';
                }else{
                    item.showTypeName = item.Name;
                }
            });
            return list;
        }
    }]
});