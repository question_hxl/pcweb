/**
 * Created by zy on 2016/9/21.
 */
define(['underscore-min', '../service/assignRouter'], function () {
    return ['$rootScope', '$scope', '$state', 'ngDialog', '$timeout', 'constantService', 'cartService', 'apiCommon',
        function ($rootScope, $scope, $state, ngDialog, $timeout, constantService, cartService, apiCommon) {
            $scope.isCur = true;
            $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
            $scope.currentgradeId = $scope.currentUser.GradeId;
            var userInFro;
            $scope.Grade = [
                {GradeName: '高一', GradeId: '500', gradeNum: '10'},
                {GradeName: '高二', GradeId: '600', gradeNum: '11'},
                {GradeName: '高三', GradeId: '700', gradeNum: '12'}];

            var schFId = $scope.currentUser.schoolFId;
            $scope.currentpharseId = $scope.currentUser.pharseId;
            $scope.currentclassFLnkID = [];
            apiCommon.GetTeacherDetailView($scope.currentUser.fid).then(function (resp) {
                userInFro = resp.data.ResultObj;
                var classes = [];
                _.each(userInFro.TeacherSubjectClass, function (item) {
                    classes.push(item.ClassName);
                });
                $scope.cuser = {
                    gradeNum: userInFro.GradNo + '',
                    pharseId: $scope.currentUser.pharseId + '',
                    userFId: $scope.currentUser.fid,
                    classNames: classes.join(',')
                };
                if (userInFro.TeacherSubjectClass.length) {
                    $scope.cuser.editionId = userInFro.TeacherSubjectClass[0].EditionId + '';
                    $scope.cuser.subjectID = userInFro.TeacherSubjectClass[0].subjectId + '';
                } else {
                    $scope.cuser.editionId = '';
                    $scope.cuser.subjectID = '';
                }
                $scope.selectClassroom = $scope.cuser.classNames;
                $scope.currentGradeNum = $scope.cuser.gradeNum;
                $scope.currenteditionId = $scope.cuser.editionId;
                getKeMu($scope.cuser.pharseId);
            });

            function getKeMu(pharseId) {
                apiCommon.getSubjectList({
                    pharseId: pharseId
                }).success(function (res) {
                    if (_.isArray(res.msg)) {
                        $scope.keMu = _.uniq(res.msg, 'subjectId');
                        if ($scope.cuser.subjectID !== '0') {
                            var sid = _.findIndex($scope.keMu, {subjectId: $scope.cuser.subjectID});
                            if (sid !== -1) {
                                $scope.currentsubjectId = $scope.keMu[sid].subjectId;
                                $scope.currentsubjectFLnkID = $scope.keMu[sid].subjectFLnkId;
                                getPubLishing($scope.cuser.pharseId, $scope.cuser.subjectID, 1);
                            } else {
                                $scope.currentsubjectId = [];
                                $scope.currentsubjectFLnkID = [];
                            }
                        }
                    } else {
                        $scope.keMu = [];
                    }
                });
            }

            function getPubLishing(pharseId, subjectID, isfirst) {
                apiCommon.PublishGradeList({
                    pharseId: pharseId,
                    subjectId: subjectID
                }).success(function (res) {
                    if (_.isArray(res.msg) && res.msg !== "数量为空！") {
                        //出版社
                        $scope.pubLishing = _.uniq(res.msg, 'editionName');
                        var sid = _.findIndex($scope.pubLishing, {editionId: $scope.cuser.editionId});
                        if (isfirst && sid === -1) {
                            $scope.currenteditionId = '';
                        } else {
                            sid = sid >= 0 ? sid : 0;
                            $scope.currenteditionId = $scope.pubLishing[sid].editionId;
                        }
                        //教材
                        $scope.allGrading = res.msg;
                        $scope.greadings = _.filter($scope.allGrading, function (item) {
                            return item.editionId === $scope.currenteditionId;
                        });
                        if ($scope.currentpharseId === '3') {
                            $scope.greadings.unshift({
                                editionId: "",
                                editionName: "",
                                gradeId: "",
                                gradeName: "— — — — —请选择— — — — —"
                            });
                        }
                        var sidd = _.findIndex($scope.greadings, {gradeId: $scope.currentUser.GradeId});

                        if (!isfirst || sidd !== -1) {
                            if ($scope.currentpharseId === '3') {
                                sidd = sidd >= 1 ? sidd : 1;
                            } else {
                                sidd = sidd >= 0 ? sidd : 0;
                            }
                            $scope.selectGreading = $scope.greadings[sidd].gradeName;
                            $scope.selectGreadings = $scope.greadings[sidd];
                            $scope.selectGradeId = $scope.greadings[sidd].gradeId;
                        } else {
                            $scope.selectGreading = '';
                            $scope.selectGreadings = $scope.greadings[0];
                            $scope.selectGradeId = '';
                        }

                    } else {
                        $scope.pubLishing = [];
                        if ($scope.currentpharseId === '3') {
                            $scope.greadings = [{
                                editionId: "",
                                editionName: "",
                                gradeId: "",
                                gradeName: "— — — — —请选择— — — — —"
                            }];
                            $scope.selectGreadings = $scope.greadings[0];
                        } else {
                            $scope.greadings = [];
                        }
                    }
                    if ($scope.cuser.pharseId !== '3') {
                        if ($scope.selectGradeId !== '') {
                            getClassroom($scope.cuser.pharseId, $scope.selectGradeId, schFId, isfirst);
                        }
                    } else {
                        getgrade($scope.cuser.gradeNum);
                        if ($scope.Grade.GradeId !== '') {
                            getClassroom($scope.cuser.pharseId, $scope.Grade.GradeId, schFId, isfirst);
                        }
                    }
                });
            }

            function getgrade(gradeNum) {
                if ($scope.cuser.pharseId === '3') {
                    if (gradeNum === '10') {
                        $scope.Grade.GradeId = '500';
                    } else if (gradeNum === '11') {
                        $scope.Grade.GradeId = '600';
                    } else if (gradeNum === '12') {
                        $scope.Grade.GradeId = '700';
                    }
                }
            }

            function getClassroom(pharseId, gradeId, schoolFLnkID, isfirst) {
                apiCommon.generalQuery({
                    Proc_name: 'TeachValidChoiceClass',
                    selfName: $scope.currentUser.SelfName,
                    subjectId: $scope.currentsubjectId,
                    gradNum: $scope.currentpharseId === '3' ? $scope.currentGradeNum : '',
                    gradId: gradeId
                })
                    .success(function (res) {
                        if (_.isArray(res.msg) && res.msg[0]) {
                            $scope.classroom = _.uniq(res.msg, 'className');
                            if (_.indexOf($scope.selectClassroom, ',') >= 0) {
                                var ids = $scope.selectClassroom.split(',');
                                ids.forEach(function (item) {
                                    $scope.currentclassFLnkID.push(_.find($scope.classroom, {className: item}).classFLnkID);
                                });
                            } else {
                                var sid = _.findIndex($scope.classroom, {className: $scope.cuser.classNames});
                                if (sid >= 0) {
                                    $scope.currentclassFLnkID.push($scope.classroom[sid].classFLnkID);
                                } else {
                                    if (!isfirst) {
                                        $scope.currentclassFLnkID.push($scope.classroom[0].classFLnkID);
                                    } else {
                                        $scope.currentclassFLnkID = [];
                                    }
                                }
                            }
                        }
                    });
            }

            $scope.$watch('selectGreadings', function (newValue, oldValue) {
                if ((newValue === oldValue) || !newValue) {
                    return;
                }
                $scope.currentgradeId = newValue.gradeId;
            });

            //修改科目
            $scope.changeSub = function (va) {
                if ($scope.currentsubjectId === va) {
                    return;
                }
                $scope.currentsubjectId = va;
                var sid = _.findIndex($scope.keMu, {subjectId: $scope.currentsubjectId});
                sid = sid >= 0 ? sid : 0;
                $scope.currentsubjectFLnkID = $scope.keMu[sid].subjectFLnkId;
                getPubLishing($scope.cuser.pharseId, va, 0);
            };
            //修改出版社
            $scope.changePubLishing = function (va) {
                if ($scope.currenteditionId === va) {
                    return;
                }
                $scope.currenteditionId = va;
                $scope.greadings = _.filter($scope.allGrading, function (item) {
                    return item.editionId === $scope.currenteditionId;
                });
                if ($scope.currentpharseId === '3') {
                    $scope.greadings.unshift({
                        editionId: "",
                        editionName: "",
                        gradeId: "",
                        gradeName: "— — — — —请选择— — — — —"
                    });
                    $scope.selectGreadings = $scope.greadings[1];
                }
            };
            //修改教材——非高中
            $scope.changeGreading = function (val, va) {
                if ($scope.selectGreading === val) {
                    return;
                }
                $scope.currentgradeId = va;
                $scope.selectGreading = val;
                if (!($scope.currentpharseId === '3')) {
                    apiCommon.generalQuery({
                        Proc_name: 'TeachValidChoiceClass',
                        selfName: $scope.currentUser.SelfName,
                        subjectId: $scope.currentsubjectId,
                        gradNum: $scope.currentpharseId === '3' ? $scope.currentGradeNum : '',
                        gradId: $scope.currentgradeId
                    })
                        .success(function (res) {
                            if (_.isArray(res.msg)) {
                                $scope.classroom = _.uniq(res.msg, 'className');
                                $scope.currentclassFLnkID = [];
                            } else {
                                $scope.classroom = [];
                            }
                        });
                }
            };
            //修改年级
            $scope.changeGrade = function (val) {
                if ($scope.currentGradeNum === val) {
                    return;
                }
                $scope.currentGradeNum = val;
                getgrade(val);
                apiCommon.generalQuery({
                    Proc_name: 'TeachValidChoiceClass',
                    selfName: $scope.currentUser.SelfName,
                    subjectId: $scope.currentsubjectId,
                    gradNum: $scope.currentpharseId === '3' ? $scope.currentGradeNum : '',
                    gradId: $scope.selectGreadings.gradeId
                })
                    .success(function (res) {
                        if (_.isArray(res.msg)) {
                            $scope.classroom = _.uniq(res.msg, 'className');
                            $scope.currentclassFLnkID = [];
                        } else {
                            $scope.classroom = [];
                        }
                    });
            };
            //修改班级
            $scope.changeClassroom = function (va) {
                var index = _.indexOf($scope.currentclassFLnkID, va);
                if (index >= 0) {
                    $scope.currentclassFLnkID.splice(index, 1);
                } else {
                    $scope.currentclassFLnkID.push(va);
                }
            };

            $scope.save = function () {
                var data = {
                    FLnkID: userInFro.FLnkID,
                    UserName: userInFro.UserName,
                    CreateByUserID: userInFro.CreateByUserID,
                    pharseId: +$scope.cuser.pharseId,
                    SchoolFLnkId: userInFro.SchoolFLnkId,
                    LoginName: userInFro.LoginName,
                    SelfName: userInFro.SelfName,
                    SFID: userInFro.SFID || '',
                    Email: userInFro.Email || '',
                    Mobile: userInFro.Mobile || '',
                    Telephone: userInFro.Telephone || '',
                    Sex: userInFro.Sex || '',
                    Birthday: userInFro.Birthday || '',
                    Career: userInFro.Career,
                    RoleId: userInFro.RoleId,
                    CarrerNew: userInFro.CarrerNew
                };
                data.SubjectId = +$scope.currentsubjectId|| '';
                data.editionId = $scope.currenteditionId || '';
                data.GradNo = +$scope.currentGradeNum|| '';
                data.BaseTeacherClassSubject = [];
                data.gradeID = $scope.currentgradeId || '';
                data.ClassFLnkID = '';

                _.each($scope.currentclassFLnkID, function (classFLnkID) {
                    data.BaseTeacherClassSubject.push({
                        SubjectFLnkID: $scope.currentsubjectFLnkID,
                        ClassFLnkID: classFLnkID,
                        EditionId: $scope.currenteditionId,
                        GradeId: $scope.currentgradeId,
                        IsDeleted: false
                    })
                });
                _.each(userInFro.TeacherSubjectClass, function (item) {
                    item.IsSame = false;
                    _.each(data.BaseTeacherClassSubject, function (classFLnkID) {
                        if(item.ClassFLnkID === classFLnkID.ClassFLnkID) {
                            item.IsSame = true;
                            classFLnkID.FLnkID = item.FLnkID;
                        }
                    });
                });
                _.each(userInFro.TeacherSubjectClass, function (item) {
                    if(!item.IsSame) {
                        data.BaseTeacherClassSubject.push({
                            SubjectFLnkID: item.SubjectFLnkID,
                            ClassFLnkID: '' + item.ClassFLnkID,
                            EditionId: '' + item.EditionId,
                            GradeId: '' + item.GradeId,
                            FLnkID: item.FLnkID,
                            IsDeleted: true
                        });
                    }
                });
                if(+$scope.cuser.pharseId !== 3 && $scope.currentgradeId) {
                    apiCommon.getGradeNum(+$scope.currentgradeId).then(function (resd) {
                        if(resd.data.Flag) {
                            data.GradNo = resd.data.ResultObj;
                            apiCommon.updateBaseTeacher(data).then(function (res) {
                                if (res.data.Flag) {
                                    constantService.alert('保存成功，请重新登录！', function(){
                                        //logout
                                        apiCommon.logOut().then(function (resp) {
                                            sessionStorage.removeItem('currentUser');
                                            cartService.clearLocalCart();
                                            $state.go('login');
                                        });
                                    });
                                } else {
                                    constantService.alert(res.data.Message);
                                }
                            });
                        }
                    });
                }else {
                    apiCommon.updateBaseTeacher(data).then(function (res) {
                        if (res.data.Flag) {
                            constantService.alert('保存成功，请重新登录！', function(){
                                //logout
                                apiCommon.logOut().then(function (resp) {
                                    sessionStorage.removeItem('currentUser');
                                    cartService.clearLocalCart();
                                    $state.go('login');
                                });
                            });
                        } else {
                            constantService.alert(res.data.Message);
                        }
                    });
                }
            };
        }]
});