/**
 * Created by tangquanbin on 2016/9/27 0027.
 */
angular.module('myService',[])
.factory('myService', function (ngDialog) {
    return {
        showSimpleText: (function() {
            return function(text) {
                var dialogObj = ngDialog.open({
                    template: '<div class="toast-title">' + text + '</div>',
                    className: 'ngdialog-theme-default',
                    appendClassName: '',
                    closeByDocument: true,
                    plain: true
                });
            };
        })()
    }
})
