/**
 * Created by tangquanbin on 2016/9/28 0028.
 */
angular.module('myDirective', [])
    .directive('mySelect', function () {
        return {
            restrict: 'AE',
            replace: true,
            templateUrl: 'directive/template/mySelect.html',
            transclude : true,
            scope: {
                selecttitle: '=', //默认选中值
                lidata: '=',  ///数据集如['张三','李四','王五']
                clickChange: '&',   //选项变化时事件
                isShow: '@',//是否显示
                ulcss: '=', //下拉框宽度
                key:'@',//绑定对象的属性
                bindcss:'='//绑定选中样式
            },
            link: function ($scope, $ele, attrs) {

            },
            controller: function ($scope) {
                $scope.isObj =  !!$scope.key;
                $scope.showMe = false;

                $scope.ulCss = function () {//下拉框样式
                    return $scope.ulcss || {width:'120px'}
                };
         
                $scope.activateLi = function (data) {//选中样式
                    $scope.isBind = $scope.isObj ?  $scope.selecttitle[$scope.key] === data[$scope.key] : $scope.selecttitle === data;
                    if($scope.isBind && $scope.bindcss){
                        return $scope.bindcss || {background:'#58D0E1'};
                    }
                };

                $scope.toggle = function () {//点击隐藏
                    $scope.showMe = !$scope.showMe;
                };
                $scope.clickLi = function (data_) {//选中隐藏列表 并绑定值
                    $scope.selecttitle =  data_ ;
                    $scope.showMe = !$scope.showMe;
                };
                $scope.$watch('selecttitle', function () {
                    $scope.clickChange();
                });
            }
        }
    })
