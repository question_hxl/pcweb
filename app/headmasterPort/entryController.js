/**
 * Created by 贺小雷 on 2016/9/26.
 */
angular.module('entry', [])
.controller('entryController',['$rootScope','$scope','$state','$http', '$cookies', 'myService',
    function($rootScope, $scope, $state, $http,$cookies,myService){
    $scope.user  = angular.fromJson(sessionStorage.getItem('currentUser') || undefined);
    $scope.isShow = false;
    $scope.associatedAccount = [];

    var sessionId = $cookies.sessionId;

    $scope.logo = '/assets/skin/' + window.config.theme + '/img/logo.png';
    $scope.copy = window.config.copyInfo[window.config.theme];
    $scope.theme = window.config.theme;

    //用于设置当前菜单
    $scope.activeNav = 'home';
    $scope.$on('$stateChangeSuccess', function (val, state) {
        $scope.activeNav = state.data.belong;
    });
    //导航选中
    $scope.gotonav = function (nav) {
        $scope.activeNav = nav;
    };
    //logo点击
    $scope.logolink = function () {
        $state.go('main.home');
    };
    //多账户关联模拟登陆
    $scope.simulationLanding = function (item) {
        var fId = item.userFId;
        $http.post($rootScope.baseUrl + '/Interface0231A.ashx', {
            userFId: fId
        }).success(function (data) {
            if(data.code === 0) {
                //模拟登录获取临时密码成功
                var tempPwd = data.msg;
                $http.post($rootScope.baseUrl + '/Interface0114.ashx').then(function (resp) {
                    sessionStorage.removeItem('currentUser');
                    window.location.href = location.origin + '/' + config.homePageMap[config.theme] + '#/login?n=' + fId + '&p=' + tempPwd + '&isEm=1';
                });
            }else {
                myService.showSimpleText('切换登录失败！');
            }
        });
    };
    // 显示退出
    $scope.toggleMenu = function () {
        $scope.isShow = !$scope.isShow;
    };
    //退出
    $scope.logout = function () {
        $http.post($rootScope.baseUrl + '/Interface0114.ashx').then(function (resp) {
            sessionStorage.removeItem('currentUser');
            location.href = location.origin + '/' + config.homePageMap[config.theme] + '#/login';
        });
    };
        $scope.checkNewReport = function(){
            window.open('http://report.wxy100.com/#!/login?sessionId='+sessionId + '&isOpenNewWin=' + 1);
        };
    //获取关联账号
    $http.post($rootScope.baseUrl + '/Interface0299.ashx').then(function (resp) {
        if(_.isArray(resp.data.msg)){
            $scope.associatedAccount = resp.data.msg;
        }else{
            $scope.associatedAccount = [];
        }
    },function () {
        $scope.associatedAccount = [];
    });
    //问候语
    (function time(curDate) {
        if (curDate >= 7 && curDate <= 9) {
            $scope.sayHello = '早上好';
        }else if (curDate > 9 && curDate <= 12) {
            $scope.sayHello = '上午好';
        }else if (curDate > 12 && curDate <= 18) {
            $scope.sayHello = '下午好';
        } else {
            $scope.sayHello = '晚上好';
        }
    })(new Date().getHours())
}]);