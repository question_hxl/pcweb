/**
 * Created by zy on 2017/5/31 0031.
 */
angular.module('tableHomeworkAnalysis', [])
    .controller('tableHomeworkController', function($rootScope, $scope, $http, $q, constantService){
        var user = angular.fromJson(sessionStorage.getItem('currentUser'));
        $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
            Proc_name: 'getSchoolGrade',
            schoolfId: user.schoolFId
        }).success(function (res) {
            $scope.pharseList = res.msg;
            $scope.currentPharse = $scope.pharseList[0];
            getSubjectList();
        });
        //获取学科列表
        function getSubjectList() {
            $http.post($rootScope.baseUrl + '/Interface0220B.ashx', {pharseId: user.pharseId}).then(function (res) {
                if (_.isArray(res.data.msg)) {
                    $scope.subjectList = res.data.msg;
                    $scope.curSubject = $scope.subjectList[0];
                    getPaperList();
                }
            });
        }
        function getPaperList(){
            $scope.paperList = [];
            $('.box ul').stop(false, true).animate({left: 0});
            if(!_.isEmpty($scope.currentPharse)) {
                $scope.lowScore = '40';
                $scope.highScore = '80';
                return $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    Proc_name:'Proc_GradJobList',
                    schoolFLnkID: user.schoolFId,
                    grad: +$scope.currentPharse.GradeNum,
                    subjectId: $scope.curSubject.subjectId
                }).then(function(res){
                    if(res.data && _.isArray(res.data.msg)) {
                        $scope.paperList = res.data.msg;
                        _.each($scope.paperList, function (item) {
                            item.ExamDate = (item.ExamDate.split(" "))[0];
                            item.ExamFlnkID = item.ExExamFLnkID;
                        });
                        $scope.currentPaper = $scope.paperList[0];
                        getGradeData();
                    }else {
                        $scope.paperList = [];
                        $scope.currentPaper = {};
                    }
                });
            }else {
                $scope.paperList = [];
                $scope.currentPaper = {};
            }
        }
        function getGradeData() {
            if(!$scope.currentPaper || _.isEmpty($scope.currentPaper)) {
                return;
            }
            var url = $rootScope.baseUrl + '/generalQuery.ashx';
            var params = {
                Proc_name: 'Proc_GradJobDetail',
                schoolFid: user.schoolFId,
                period: $scope.currentPaper.Period,
                examFid: $scope.currentPaper.ExExamFLnkID,
                highScore: +$scope.highScore,
                lowScore: +$scope.lowScore
            };
            $http.post(url, params).then(function(res){
                if(_.isArray(res.data.msg)){
                    $scope.lowScorer =  $scope.lowScore;
                    $scope.highScorer = $scope.highScore;
                    $scope.classInFroList = _.groupBy(res.data.msg, 'ClassName');
                }
            });
        }

        $scope.selectSubjedt = function (item) {
            $scope.curSubject = item;
            getPaperList();
        };
        $scope.selectPhase = function (item) {
            $scope.currentPharse = item;
            getPaperList();
        };
        $scope.selectSubject = function(subject){
            if(subject.classId !== $scope.currentPharse.classId) {
                $scope.currentPharse = subject;
                getPaperList();
            }
        };
        $scope.showStudent = function (item , status) {
            var students = '', studentList = [];
            item = _.sortBy(item, 'studentId');
            studentList = _.filter(item, function (value) {
                if(status === 1 || status === 2) {
                    return value.status === status;
                }else  if(status === 3){
                    return +value.studentSc >= +$scope.highScore && value.status === 1;
                }else  if(status === 4){
                    return +value.studentSc < +$scope.lowScore && value.status === 1;
                }
            });
            _.each(studentList, function (value, index) {
                if(value.studentName.indexOf('，') === -1){
                    if(index !== studentList.length - 1){
                        value.studentName += '，';
                    }
                }
                students = students + value.studentName;
            });
            if(students === ''){
                return
            }
            constantService.alert(students);
        };
        $scope.selectCur = function(paper){
            $scope.currentPaper = paper;
            getGradeData();
        };
        $scope.scrollLeft = function(){
            if(!$('.moveBox ul').is(":animated")) {
                if(parseInt($('.moveBox ul').css('left')) < 0) {
                    $('.moveBox ul').stop(false, true).animate({
                        'left': '+=824px'
                    });
                }
            }
        };
        $scope.scrollRight = function(){
            if(!$('.moveBox ul').is(":animated")) {
                if(Math.abs(parseInt($('.moveBox ul').css('left'))) < 206 * ($scope.paperList.length - 4) && $('.moveBox ul').width() > 824) {
                    $('.moveBox ul').stop(false, true).animate({
                        'left': '-=824px'
                    });
                }
            }
        };
    });
