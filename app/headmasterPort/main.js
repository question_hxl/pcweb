/**
 * Created by 贺小雷 on 2016/9/26.
 */
angular.module('headmasterApp', [
    'ui.router',
    'ngCookies',
    'ngDialog',
    'angular-loading-bar',
    'customizeDirective',
    'myService',
    'myFilter',
    'myDirective',
    'entry',
    'home',
    'report',
    'tableHomeworkAnalysis',
    'customService'
])
.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'cfpLoadingBarProvider',function($stateProvider, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider){
    if(window.config.theme === '51') {
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div><img src="../img/loading-logo.png">' +
            '</div><span class="fa fa-spinner fa-pulse"></span></div>';
    }
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('main', {
            url: "",
            templateUrl: 'entry.html',
            controller: "entryController",
            data : {
                belong:'home'
            }
        })
        .state('main.home', {
            url: "/",
            templateUrl: 'home/home.html',
            controller: "homeController",
            data : {
                belong:'home'
            }
        })
        .state('main.report', {
            url: "/report",
            templateUrl: 'report/report.html',
            controller: "reportController",
            data : {
                belong:'report'
            }
        })
        .state('main.tableHomeworkAnalysis', {
            url: "/tableHomeworkAnalysis",
            templateUrl: 'tableHomework/tableHomeworkAnalysis.html',
            controller: "tableHomeworkController",
            data : {
                belong:'tableHomeworkAnalysis'
            }
        });
    $httpProvider.interceptors.push(['$q', '$rootScope', function($q, $rootScope) {
        return {
            request: function(request){
                return request;
            },
            response: function(response) {
                var url = response.config.url;
                var data = response.data;

                // 排除登陆接口返回 code = 1 的场景
                if (url.indexOf('/BootStrap/Interface') >= 0 && url.indexOf('Interface0001.ashx' === -1) && data.code === 1) {
                    sessionStorage.removeItem('currentUser');
                    $rootScope.$broadcast('SESSION_TIMEOUT');
                    return $q.reject(response);
                } else {
                    return response;
                }
            }
        };
    }]);
}])
.run(function($rootScope){
    var currentUser = sessionStorage.getItem('currentUser');
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        if(_.isEmpty(currentUser)) {
            event.preventDefault();
            $rootScope.$broadcast('CLEAR_CART_CACHE');
            location.href = location.origin + '/index.html#/login';
        }else if(angular.fromJson(currentUser).role !== '校长') {
            event.preventDefault();
            location.href = location.origin + '/index.html';
        }
    });

    $rootScope.$on('SESSION_TIMEOUT', function () {
        $rootScope.$broadcast('CLEAR_CART_CACHE');
        location.href = location.origin + '/index.html#/login';
    });
    $rootScope.baseUrl = '/BootStrap/Interface';
});