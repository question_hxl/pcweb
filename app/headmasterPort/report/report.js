/**
 * Created by tangquanbin on 2016/9/27 0027.
 */
angular.module('report', [])
    .controller('reportController', function($rootScope, $scope, $state, $http, $q, myService, ngDialog){
        var adminUrl = '/BootStrap/Interface';
        var managerUrl = 'http://admin.51jyfw.com';
        var user = angular.fromJson(sessionStorage.getItem('currentUser'));
        console.log(user);
        var UNIFIED_NAME_TYPE_MAP = {
            0: '综合统考',
            1: '文科子统考',
            2: '理科子统考',
            3: '综合统考'
        };
        $scope.gradeListClasses = [
            {
                id: '1',
                name: '一年级'
            }, {
                id: '2',
                name: '二年级'
            }, {
                id: '3',
                name: '三年级'
            }, {
                id: '4',
                name: '四年级'
            }, {
                id: '5',
                name: '五年级'
            }, {
                id: '6',
                name: '六年级'
            }, {
                id: '7',
                name: '七年级'
            }, {
                id: '8',
                name: '八年级'
            }, {
                id: '9',
                name: '九年级'
            }, {
                id: '10',
                name: '高一'
            }, {
                id: '11',
                name: '高二'
            }, {
                id: '12',
                name: '高三'
            }
        ];//年级列表
        $scope.subjectList = [];//学科列表
        $scope.paperList = [];//试卷列表

        $scope.selectPaperList = [];
        $scope.selectUnifiedList = [];

        $scope.data = {
            nianJiId: "",//年级id
            pharseId: user.pharseId,//学段id (1/2/3)
            schoolFLnkID: user.schoolFId,//学校id
            subjectId: ''
        };
        if($scope.data.pharseId === '3'){
            $scope.gradeListClass = $scope.gradeListClasses.slice(9,12);
        }else if($scope.data.pharseId === '2') {
            $scope.gradeListClass = $scope.gradeListClasses.slice(6, 9);
        }else {
            $scope.gradeListClass = $scope.gradeListClasses.slice(0,6);
        }
        getSubjectList();
        //查询试卷
        $scope.searchPaper = function () {
            var req = {
                key:$scope.data.paperName || "",
                gradeNum:$scope.data.nianJiId,
                subjectId:$scope.data.subjectId
            };
            if(!$scope.data.nianJiId){
                myService.showSimpleText('请选择年级！');
                return;
            }
            $scope.selectPaperList = [];
            $scope.selectUnifiedList = [];
            getPaperList(req);
            getUnifiedList();
        };

        //点击试卷
        $scope.getPaper = function (paper) {
            var baseUrl = managerUrl + '/class-grid/index/index_new.html#',
                URL= baseUrl+ '/?schoolId='+$scope.data.schoolFLnkID+'&paperId='+ paper.examId+'&session='+ paper.session;
            window.open(URL);
        };

        $scope.toggleSetUnified = function(){
            var uniqList = _.uniq($scope.selectPaperList, function(item){
                return item.subjectId;
            });
            if(uniqList.length < $scope.selectPaperList) {
                myService.showSimpleText('存在重复的学科，无法设置统考！');
                return;
            }
            if($scope.selectPaperList.length > 0) {
                $scope.isSettingUnified = !$scope.isSettingUnified;
            }else {
                myService.showSimpleText('请先选择试卷，再尝试设置统考！');
            }
        };

        $scope.selectPaper = function(paper){
            if($scope.isPaperSelected(paper)) {
                var index = _.findIndex($scope.selectPaperList, function(p){
                    return p.examId === paper.examId;
                });
                $scope.selectPaperList.splice(index, 1);
            }else {
                $scope.selectPaperList.push(paper);
            }
        };

        $scope.isPaperSelected = function(paper){
            return !!_.find($scope.selectPaperList, function(p){
                return p.examId === paper.examId;
            });
        };

        $scope.checkReport = function(paper, e){
            e.preventDefault();
            e.stopPropagation();
            var baseUrl = managerUrl + '/class-grid/index/index_new.html#',
                URL= baseUrl+ '/?schoolId='+$scope.data.schoolFLnkID+'&paperId='+ paper.examId+'&session='+ paper.session;
            window.open(URL);
        };

        $scope.selectUnified = function(unified){
            var isExitSameType = _.find($scope.selectUnifiedList, function(item){
                return item.uType === unified.uType;
            });
            if(unified.uType !== '0' && unified.uType !== '3' && !isExitSameType) {
                $scope.selectUnifiedList.push(unified);
            }else if($scope.isUnifiedSelect(unified)) {
                var index = _.findIndex($scope.selectUnifiedList, function(item){
                    return item.FLnkId === unified.FLnkId;
                });
                $scope.selectUnifiedList.splice(index, 1);
            }
        };

        $scope.setUnified = function(type){
            $scope.isSettingUnified = false;
            var html = '您将设置考试：' + _.pluck($scope.selectPaperList, 'examName').join('、') + '为：' + UNIFIED_NAME_TYPE_MAP[type] + '，点击确认继续。';
            ngDialog.open({
                template: '<div class="set-unified-name"><p style="border-bottom: 1px solid #ccc;">' + html + '</p>'+
                '<div class="row" style="text-align: center;margin-top: 20px;">' +
                '<label>设置统考名称：<input ng-model="unifiedName" placeholder="请输入统考名称"></label>' +
                '</div><div class="row" style="text-align: center;margin-top: 20px;">' +
                '<div class="btn btn-primary" ng-click="doSetUnified()" style="min-width: 120px;">确定</div></div></div>',
                className: 'ngdialog-theme-default',
                appendClassName: 'set-unified-name',
                scope: $scope,
                plain: true,
                controller: function($scope){
                    return function(){
                        $scope.doSetUnified = function(){
                            if(!$scope.unifiedName) {
                                myService.showSimpleText('请输入统考名称！');
                                return;
                            }
                            ngDialog.close();
                            //todo  设置统考
                            $http.post(adminUrl + '/Interface0296A.ashx', {
                                examFId: _.pluck($scope.selectPaperList, 'examId').join(','),
                                // period: $scope.selectPaperList[0].session,
                                schoolFId: user.schoolFId,
                                unifiedName: $scope.unifiedName,
                                type: type
                            }).then(function(res){
                                if(res.data.code === 0) {
                                    myService.showSimpleText('设置统考成功！');
                                    $scope.unifiedList.unshift({
                                        FLnkId: res.data.msg,
                                        Name: $scope.unifiedName,
                                        uType: type + '',
                                        pFLnkId: '',
                                        pName: ''
                                    });
                                }else {
                                    myService.showSimpleText(res.data.msg);
                                }
                            });
                        };
                    }
                }
            });
        };

        $scope.isUnifiedSelect = function(unified){
            return !!_.find($scope.selectUnifiedList, function(item){
                return item.FLnkId === unified.FLnkId;
            });
        };

        $scope.unitUnified = function(){
            //todo 合并统考
            var wenIndex = _.findIndex($scope.selectUnifiedList, function(u){
                return u.uType === '1';
            });
            var liIndex = _.findIndex($scope.selectUnifiedList, function(u){
                return u.uType === '2';
            });
            if(wenIndex >= 0 && liIndex >= 0) {
                var html = '您将合并统考：' + _.pluck($scope.selectUnifiedList, 'Name').join('、') + '为：' + UNIFIED_NAME_TYPE_MAP['3'] + '，点击确认继续。';
                ngDialog.open({
                    template: '<div class="set-unified-name"><p style="border-bottom: 1px solid #ccc;">' + html + '</p>'+
                    '<div class="row" style="text-align: center;margin-top: 20px;">' +
                    '<label>设置合并统考名称：<input ng-model="unifiedName" placeholder="请输入合并统考名称"></label>' +
                    '</div><div class="row" style="text-align: center;margin-top: 20px;">' +
                    '<div class="btn btn-primary" ng-click="doUnitUnified()" style="min-width: 120px;">确定</div></div></div>',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'set-unified-name',
                    scope: $scope,
                    plain: true,
                    controller: function($scope){
                        return function(){
                            $scope.doUnitUnified = function(){
                                if(!$scope.unifiedName) {
                                    myService.showSimpleText('请输入合并统考名称！');
                                    return;
                                }
                                ngDialog.close();
                                //todo  合并统考
                                $http.post(adminUrl + '/Interface0296B.ashx', {
                                    unifieds : _.pluck($scope.selectUnifiedList, 'FLnkId').join(','),
                                    schoolFId: user.schoolFId,
                                    unifiedName: $scope.unifiedName
                                }).then(function(res){
                                    if(res.data.code === 0) {
                                        myService.showSimpleText('设置统考成功！');
                                        if(wenIndex > liIndex) {
                                            $scope.unifiedList.splice(wenIndex, 1);
                                            $scope.unifiedList.splice(liIndex, 1);
                                        }else {
                                            $scope.unifiedList.splice(liIndex, 1);
                                            $scope.unifiedList.splice(wenIndex, 1);
                                        }
                                        $scope.unifiedList.unshift({
                                            FLnkId: res.data.msg,
                                            Name: $scope.unifiedName,
                                            uType: '3',
                                            pFLnkId: '',
                                            pName: '',
                                            sub: $scope.selectUnifiedList
                                        });
                                        $scope.selectUnifiedList = [];
                                    }else {
                                        myService.showSimpleText(res.data.msg);
                                    }
                                }, function(){
                                    myService.showSimpleText('设置统考失败，服务器错误！');
                                });
                            };
                        }
                    }
                });
            }else {
                myService.showSimpleText('请分别选择文科统考和理科统考');
            }
        };

        $scope.getTypeName = function(type){
            return UNIFIED_NAME_TYPE_MAP[type];
        };

        $scope.submitAnalysis = function(e, unified){
            e.stopPropagation();
            e.preventDefault();
            $http.post(adminUrl + '/generalQuery.ashx', {
                Proc_name: 'GetUnifieTasks',
                UnifiedFId: unified.FLnkId
            }).then(function(res){
                var tasks = _.filter(res.data.msg, function(item){
                    return item.StatusStep !== '4';
                });
                checkAndSubmit(tasks);
            }, function(res){
                myService.showSimpleText('查询统考考试任务失败，请重试！');
            });
        };

        $scope.sendWxMsg = function(e, unified){
            e.stopPropagation();
            e.preventDefault();
            $http.post(adminUrl + '/unifiedSendWxMsg.ashx', {
                unifiedId: unified.FLnkId
            }).then(function(res){
                if(res.data.code === 0) {
                    myService.showSimpleText('微信消息发送成功！');
                }else {
                    myService.showSimpleText('微信消息发送失败！');
                }
            });
        };

        var failClass = [], successClass = [];

        function checkAndSubmit(tasks){
            if(tasks.length > 0){
                doAnalysis(tasks[0].cFlnkID, tasks[0].ExExamFLnkID, tasks[0].ClassName, tasks[0].subjectName).then(function(res){
                    successClass.push(tasks[0]);
                    tasks.shift();
                    checkAndSubmit(tasks);
                }, function(res){
                    failClass.push(tasks[0]);
                    tasks.shift();
                    checkAndSubmit(tasks);
                });
            }else {
                if(failClass.length > 0 || successClass.length > 0) {
                    var failTip = '';
                    _.each(failClass, function(c, index){
                        failTip += c.ClassName + c.subjectName;
                        if(index < failClass.length - 1) {
                            failTip += '、';
                        }
                    });
                    myService.showSimpleText('提交分析完成，共成功分析' + successClass.length + '次考试任务' +
                        (failClass.length > 0 ? (', 分析失败的任务为：' + failTip) + '。' : ', 无分析失败的班级。'));
                }else {
                    myService.showSimpleText('没有可以提交分析的考试任务！');
                }
                failClass = [];
                successClass = [];
            }
        }

        function doAnalysis(classId, examId, className, subjectName){
            var dialog = ngDialog.open({
                template: '<div class="img-box"><div class="text">正在为您分析' + className + subjectName + '考试数据，请稍后...</div><div class="icon">' +
                '<span class="fa fa-spinner fa-pulse"></span></div></div>',
                className: 'ngdialog-theme-default',
                appendClassName: 'img-loading-tip',
                closeByDocument: false,
                plain: true,
                showClose: false,
                scope: $scope
            });
            var defer = $q.defer();
            (function(d){
                $http({
                    method: 'post',
                    url: adminUrl + '/Interface0207.ashx',
                    data: {
                        classFlnkID: classId,
                        examFlnkID: examId
                    },
                    ignoreLoadingBar: true
                }).then(function(res){
                    if(res.data.code === 0) {
                        ngDialog.close(d.id);
                        defer.resolve(true);
                    }else {
                        ngDialog.close(d.id);
                        defer.reject(false);
                    }
                }, function(){
                    ngDialog.close(d.id);
                    defer.reject(false);
                });
            })(dialog);
            return defer.promise;
        }

        function getSubjectList() {
            $http({
                method: 'post',
                url:adminUrl + '/Interface0220B.ashx',
                data: {
                    pharseId: $scope.data.pharseId
                }
            }).success(function (res) {
                if(_.isArray(res.msg) && res.msg.length ){
                    $scope.subjectList = res.msg;
                }
            })
        }

        function getPaperList(req) {
            $http({
                method: 'post',
                url:adminUrl + '/Interface0237A.ashx',
                data:req
            }).then(function (res) {
                if(_.isArray(res.data.msg) && res.data.msg.length){
                    $scope.paperList = res.data.msg;
                }else {
                    $scope.paperList = [];
                }
            }, function(){
                $scope.paperList = [];
            });
        }

        function getUnifiedList(){
            $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                Proc_name: 'GetSchoolUnifieds',
                schoolFlnkid: user.schoolFId,
                gradeNum: +$scope.data.nianJiId
            }).then(function(res){
                console.log(res);
                if(res.data.code === 0 && _.isArray(res.data.msg)) {
                    $scope.unifiedList = parseUnifiedData(res.data.msg);
                }else {
                    $scope.unifiedList = [];
                }
            }, function(res){
                $scope.unifiedList = [];
            });
        }

        function parseUnifiedData(data){
            var parsedData = [];
            _.each(data, function(item){
                if(item.pFLnkId) {
                    var parent = _.find(parsedData, function(d){
                        return d.FLnkId === item.pFLnkId;
                    });
                    if(parent) {
                        var sub = parent.sub || [];
                        sub.push(item);
                        parent.sub = sub;
                    }else {
                        parent = {
                            FLnkId: item.pFLnkId,
                            Name: item.pName,
                            uType: '3',
                            sub: [item]
                        };
                        parsedData.push(parent);
                    }
                }else {
                    parsedData.push(item);
                }
            });
            return parsedData;
        }
        $scope.checkGradeBookReport = function(paper, e){
            e.preventDefault();
            e.stopPropagation();
            var baseUrl = managerUrl + '/class-grid/index/gradeBookIndex.html#',
            URL= baseUrl+ '/?schoolId='+$scope.data.schoolFLnkID+'&unifiedId='+ paper.FLnkId+'&uType='+paper.uType+'&name='+paper.Name;
            window.open(URL);
        };
    });
