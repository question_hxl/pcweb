define(['outexcel', 'underscore-min'], function (outExcel) {
	return ['$rootScope', '$http', '$scope', '$location', '$sce', function ($rootScope, $http, $scope, $location, $sce) {
		var params = $location.search();
		$scope.params = params;
		$scope.nopagedata = false;
		if (params.ExamFlnkID != 'undefined') {
			$scope.nopagedata = false;
			// 班级名称
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0169.ashx',
				data: {
					ClassFlnkID: params.ClassFlnkID,
					ExamFlnkID: params.ExamFlnkID
				}
			}).success(function (res) {
				$scope.reports = res.msg[0];
			}).then(function () {
				$http.post($rootScope.baseUrl + '/Interface0142.ashx', {
					"classId": params.ClassFlnkID,
					"examId": params.ExamFlnkID,
					"tableModel": "5"
				}).then(function (resp) {
					$scope.tableDatas = resp.data.msg;
					//lst = $scope.tableDatas;
					//$scope.CtableDatas = $scope.tableDatas[0];
					//$scope.CtableDatas.test = $scope.CtableDatas.题型班级;
					//console.log($scope.CtableDatas);
					$scope.columns = [];
					var strth = "";
					for (var key in $scope.tableDatas[0]) {
						//alert("Key是:" + key);
						//alert("对应的值是:" + $scope.tableDatas[0][key]);
						strth = strth + "<th >" + key + "</th>";

					}


					//不带学生题号的
					var strnoth = "";
					for (var key in $scope.tableDatas[0]) {
						if (key == '学生题号') {
							key = '姓名';
						}
						if (key.indexOf("题") >= 0) {
							var jqkey = key.substring(1);
							key = jqkey.substring(0, jqkey.length - 1);
						}
						//alert("Key是:" + key);
						//alert("对应的值是:" + $scope.tableDatas[0][key]);
						strnoth = strnoth + "<th class='noth' style='width:30px;'>" + key + "</th>";
					}
					var strtr = "";
					_.each($scope.tableDatas, function (item) {
						strtr = strtr + "<tr>" + strnoth + "</tr><tr>";
						for (var value in item) {
							//alert("Key是:" + key);
							//alert("对应的值是:" + $scope.tableDatas[0][key]);
							var strvalues = item[value];
							if (typeof (strvalues) == 'number') {
								+item[value].toFixed(2);
							} else {
								strvalues = strvalues;
							}
							strtr = strtr + "<td class='notd' style='width:30px;'>" + strvalues + "</td>";
						}
						strtr = strtr + "</tr><tr><td colspan=" + resp.data.msg.length + "></td></tr>";
					});
					//$scope.columns.content = ss;
					//var vm = lst[0].题型班级;
					//$scope.columns = [];
					$scope.columns.push({ content: $sce.trustAsHtml(strth) });
					$scope.columns.push({ contents: $sce.trustAsHtml(strtr) });
				})
			}).then(function () {
				$http.post($rootScope.baseUrl + '/Interface0142.ashx', {
					"classId": params.ClassFlnkID,
					"examId": params.ExamFlnkID,
					"tableModel": "6"
				}).then(function (resp) {
					$scope.tableDatas = resp.data.msg;
					//lst = $scope.tableDatas;
					//$scope.CtableDatas = $scope.tableDatas[0];
					//$scope.CtableDatas.test = $scope.CtableDatas.题型班级;
					//console.log($scope.CtableDatas);
					$scope.columns2 = [];
					var strth = "";
					for (var key in $scope.tableDatas[0]) {
						//alert("Key是:" + key);
						//alert("对应的值是:" + $scope.tableDatas[0][key]);
						strth = strth + "<th >" + key + "</th>";
					}

					var strtr = "";
					_.each($scope.tableDatas, function (item) {
						strtr = strtr + "<tr>";
						for (var value in item) {
							//alert("Key是:" + key);
							//alert("对应的值是:" + $scope.tableDatas[0][key]);
							var strvalue = item[value];
							if (typeof (strvalue) == 'number') {
								+item[value].toFixed(2);
							} else {
								strvalue = strvalue;
							}
							strtr = strtr + "<td >" + strvalue + "</td>";
						}
						strtr = strtr + "</tr>";
					});
					//$scope.columns.content = ss;
					//var vm = lst[0].题型班级;
					//$scope.columns = [];
					$scope.columns2.push({ content: $sce.trustAsHtml(strth) });
					$scope.columns2.push({ contents: $sce.trustAsHtml(strtr) });

					console.log($scope.columns2);
				});
			})
		} else {
			$scope.nopagedata = true;
			ngDialog.open({
				template:
						'<p>无试卷信息</p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
				plain: true,
				className: 'ngdialog-theme-plain'
			});
			return false;
		}

		$scope.exportExcel = function(table, name){
            outExcel.exportTable(table, name);
		}
	}]
})