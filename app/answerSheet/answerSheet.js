/**
 * Created by Administrator on 2017/5/22.
 */
define([ 'underscore-min', 'jquerymedia', 'ckeditor', 'dateFomart'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$location', 'ngDialog',
        function ($http, $scope, $rootScope, $state, $location, ngDialog) {
            var unifiedId = $location.search().unifiedId;
            $scope.logoSrc = '/assets/skin/' + window.config.theme + '/img/loading-logo.png';
            $http.post('/BootStrap/schoolmanager/getUnifiedOcrCommit.ashx',{
                unifiedItemFId : unifiedId
            }).then(function (res) {
                if (_.isArray(res.data.msg)){
                    $scope.imgList = res.data.msg;
                    $scope.jpgList = [];
                    _.each($scope.imgList,function (value) {
                        $scope.currentJpgList = [];
                        $scope.jpgList.push({Accessory : value.Accessory});
                        $scope.currentJpg = $scope.jpgList[0].Accessory;
                    });
                }
                $scope.length = $scope.jpgList.length;
                $scope.focusIndex=0;
                $scope.focus=function(index){
                    $scope.focusIndex=index;
                    $scope.currentJpg = $scope.jpgList[index].Accessory;
                }
            })
        }]
});
