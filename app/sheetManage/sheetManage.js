/**
 * Created by 贺小雷 on 2016/12/13.
 */
define([], function(){
    return ['$rootScope', '$http', '$scope', '$state', '$q', '$location', '$timeout', 'ngDialog', 'constantService',function($rootScope, $http, $scope, $state, $q, $location, $timeout, ngDialog, constantService){
        $scope.sheetList = [];
        $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
        $scope.getAnswerSheet = function(){
            $http.post($rootScope.baseUrl + '/queryTeacherAnswerSheet.ashx').then(function(res){
                if(res.data.code == 0) {
                    $scope.sheetList = res.data.msg;
                }else {
                    $scope.sheetList = [];
                }
            }, function(){
                $scope.sheetList = [];
            });
        };
        $scope.getAnswerSheet();
        $scope.buildSheet = function(){
            window.open('#/buildAnswerSheet' + '?isOpenNewWin=' + 1);
        };

        $scope.editSheet = function(sheet){
            window.open('#/buildAnswerSheet?sheetId='+sheet.sheetId + '&isOpenNewWin=' + 1);
        };

        $scope.delAnswerSheet = function(sheet){
            ngDialog.open({
                template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
                '<div class="nextMarking-title">请确认是否删除该答题卡</div>'+
                '<div class="nextMarking-btn" style="margin:0 40px" ng-click="sure()">确定</div>'+
                '<div class="nextMarking-btn" ng-click="closeThisDialog()">取消</div>'
                +'</div>'+'</div>',
                className: 'ngdialog-theme-default ngdialog-related',
                scope: $scope,
                closeByDocument: false,
                plain: true
            });
            $scope.sure = function(){
                ngDialog.close();
                $http.post('BootStrap/Interface/delAnswerSheet.ashx', {
                    sheetId : sheet.sheetId
                }).then(function(res){
                    if(res.data.code === 0){
                        $scope.getAnswerSheet();
                        constantService.alert('删除成功！');
                    }else {
                        constantService.alert('删除失败！');
                    }
                })
            }
        };
    }];
});