﻿//testpaperCtrl
define(['underscore-min', 'star-rating'], function () {
	return ['$http', '$scope', '$rootScope', '$state', '$location', '$sce', 'ngDialog', 'paperService',
		function ($http, $scope, $rootScope, $state, $location, $sce, ngDialog, paperService) {
		var searchfrom = $location.search();
		var ExAnswerFlnkID = $scope.ExAnswerFlnkID = searchfrom.ExAnswerFlnkID;
		var ClassFlnkID = $scope.ClassFlnkID = searchfrom.ClassFlnkID;
		var ExamFlnkID = $scope.ExamFlnkID = searchfrom.ExamFlnkID;

		//获取试卷头部
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0169.ashx',
			data: {
				ClassFlnkID: searchfrom.ClassFlnkID,
				ExamFlnkID: searchfrom.ExamFlnkID
			}
		}).success(function (res) {
			$scope.topinfoList = res.msg;
		});

		//查看解析
		$scope.viewAnalysis = function (values) {
			if (values == "") {
				$scope.values = '<div>暂无数据</div>';
			} else {
				$scope.values = values;
			}
			ngDialog.open({
				template: 'template/viewAnalysis.html',
				className: 'ngdialog-theme-plain',
				scope: $scope,
				preCloseCallback: function () { //回调

				}
			})
		};

		$scope.samenodata = false;
		//同题测试
		$scope.samestest = function (QFlnkID, Diff) {
			$scope.currentDiff = Diff;
			$scope.currentQFlnkID = QFlnkID;
			$http.post($rootScope.baseUrl + '/Interface0185.ashx', {
				QFlnkID: QFlnkID,
				Diff: Diff,
				ExamFlnkID: ExamFlnkID
			}).success(function (data) {
				if (data.code != '3') {
					var samequestionList = [];
					for (var i = 0; i < data.msg.length; i++) {
						samequestionList.push({
							'Answer': $sce.trustAsHtml(data.msg[i].Answer),
							'Diff': data.msg[i].Diff,
							'Knowledges': data.msg[i].Knowledges,
							'Option_a': $sce.trustAsHtml(data.msg[i].Option_a),
							'Option_b': $sce.trustAsHtml(data.msg[i].Option_b),
							'Option_c': $sce.trustAsHtml(data.msg[i].Option_c),
							'Option_d': $sce.trustAsHtml(data.msg[i].Option_d),
							'Qtpye': data.msg[i].Qtpye,
							'Title': $sce.trustAsHtml(data.msg[i].Title)
						})
					}
					$scope.samequestionList = samequestionList;

					ngDialog.open({
						template: 'template/sametest.html',
						className: 'ngdialog-theme-plain',
						scope: $scope,
						preCloseCallback: function () { //回调
						}
					});
					$scope.$on('ngDialog.templateLoaded', function () {
						setTimeout(function () {
							$('.dialog-start').raty({ // 等级
								score: function () {
									return '' + $scope.currentDiff;
								},
								path: function () {
									return this.getAttribute('data-path');
								},
								click: function (score, evt) {
									if ($scope.currentDiff !== score) {
										$scope.currentDiff = '' + score;
										$http.post($rootScope.baseUrl + '/Interface0185.ashx', {
											QFlnkID: $scope.currentQFlnkID,
											Diff: $scope.currentDiff,
											ExamFlnkID: ExamFlnkID
										}).success(function (data) {
											if (data.code == '3') {
												$scope.samenodata = true;
												$scope.samequestionList = '';
												return false;
											} else {
												$scope.samenodata = false;

												var samequestionList = [];
												for (var i = 0; i < data.msg.length; i++) {
													samequestionList.push({
														'Answer': $sce.trustAsHtml(data.msg[i].Answer),
														'Diff': data.msg[i].Diff,
														'Knowledges': data.msg[i].Knowledges,
														'Option_a': $sce.trustAsHtml(data.msg[i].Option_a),
														'Option_b': $sce.trustAsHtml(data.msg[i].Option_b),
														'Option_c': $sce.trustAsHtml(data.msg[i].Option_c),
														'Option_d': $sce.trustAsHtml(data.msg[i].Option_d),
														'Qtpye': data.msg[i].Qtpye,
														'Title': $sce.trustAsHtml(data.msg[i].Title)
													})
												}
												$scope.samequestionList = samequestionList;
											}

										});
									}
								}
							});
						}, 100);
					});
				} else {
					ngDialog.open({
						template:
								'<p>' + data.msg + '</p>' +
								'<div class="ngdialog-buttons">' +
								'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					return false;
				}
			});

		};

		//查看做题信息
		$scope.answerAnalysis = function (QFlnkID) {
			$scope.answerArea = '<div>&nbsp;</div>';
			$http.post($rootScope.baseUrl + '/Interface0186A.ashx', {
				ClassFlnkID: searchfrom.ClassFlnkID,
				ExamFlnkID: searchfrom.ExamFlnkID,
				QFlnkID: QFlnkID
			}).success(function (data) {
				var answerAnalysisList = $scope.answerAnalysisList = data.msg;

				var analysisListChoseRight = [];
				var analysisListChoseWrong = [];
				for (var i = 0; i < answerAnalysisList.length; i++) {
					if (+answerAnalysisList[i].fIsRight == 1) {
						analysisListChoseRight.push({
							'Accessory': answerAnalysisList[i].Accessory,
							'Answer': answerAnalysisList[i].Answer,
							'IsObjective': answerAnalysisList[i].IsObjective,//0非选择，1选择
							'UserAnswer': answerAnalysisList[i].UserAnswer,
							'UserName': answerAnalysisList[i].UserName,
							'fIsRight': answerAnalysisList[i].fIsRight,
							'fScores': answerAnalysisList[i].fScores
						})
					}

					if (+answerAnalysisList[i].fIsRight < 1) {
						analysisListChoseWrong.push({
							'Accessory': answerAnalysisList[i].Accessory,
							'Answer': answerAnalysisList[i].Answer,
							'IsObjective': answerAnalysisList[i].IsObjective,//0非选择，1选择
							'UserAnswer': answerAnalysisList[i].UserAnswer,
							'UserName': answerAnalysisList[i].UserName,
							'fIsRight': answerAnalysisList[i].fIsRight,
							'fScores': answerAnalysisList[i].fScores
						})
					}
				}
				$scope.analysisListChoseRight = analysisListChoseRight;
				$scope.analysisListChoseWrong = analysisListChoseWrong;
				$scope.rightren = analysisListChoseRight.length;
				$scope.wrongren = analysisListChoseWrong.length;

				$scope.stuListA = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('A') >= 0;
				});
				$scope.stuListB = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('B') >= 0;
				});
				$scope.stuListC = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('C') >= 0;
				});
				$scope.stuListD = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('D') >= 0;
				});
			});
			ngDialog.open({
				template: 'template/answerAnalysis.html',
				className: 'ngdialog-theme-default',
				appendClassName: 'ngdialog-theme-zxz',
				scope: $scope,
				preCloseCallback: function () { //回调

				}
			})
			//$http.post($rootScope.baseUrl + '/Interface0186.ashx', {
			//	ClassFlnkID: searchfrom.ClassFlnkID,
			//	ExamFlnkID: searchfrom.ExamFlnkID,
			//	QFlnkID: QFlnkID
			//}).success(function (data) {
			//	var answerAnalysisList = $scope.answerAnalysisList = data.msg;
			//	var analysisListChose = [];
			//	var analysisListNoChose = [];
			//	for (var i = 0; i < answerAnalysisList.length; i++) {
			//		if (answerAnalysisList[i].Type == '选择题') {
			//			analysisListChose.push({
			//				'Users': answerAnalysisList[i].Users,
			//				'Type': answerAnalysisList[i].Type,
			//				'Serial': answerAnalysisList[i].Serial,
			//				'IsRight': answerAnalysisList[i].IsRight,
			//				'Answer': answerAnalysisList[i].Answer
			//			})
			//		} else if (answerAnalysisList[i].Type == '非选择题') {

			//			var isRight = answerAnalysisList[i].IsRight === '1' ? '正确' : '错误';
			//			var names = answerAnalysisList[i].Users;
			//			analysisListNoChose.push({
			//				'names': isRight + ':' + names,
			//				'IsRight': answerAnalysisList[i].IsRight
			//			});
			//		}
			//	}
			//	$scope.analysisListChose = analysisListChose;
			//	$scope.analysisListNoChose = analysisListNoChose;
			//});
			//ngDialog.open({
			//	template: 'template/answerAnalysis.html',
			//	className: 'ngdialog-theme-plain',
			//	scope: $scope,
			//	preCloseCallback: function () { //回调

			//	}
			//})
		};

		//获取题目
        paperService.getPaperDataById(searchfrom.ExamFlnkID).then(function(res){
            var paperMeta = res.data.msg;
            $http.post($rootScope.baseUrl + '/Interface0161B.ashx', {
                ClassFlnkid: searchfrom.ClassFlnkID,
                ExamFlnkID: searchfrom.ExamFlnkID,
                Type: 0
            }).then(function(resp){
                var errorQuestions = [], questionExamList;
                if(resp.data.code === 0) {
                    var allErrors = _.filter(resp.data.msg, function(item){
                        return +item.clostrate > 0;
                    });
                    _.each(allErrors, function(errorItem){
                        var group = _.find(paperMeta, function(g){
                            return g.Dtype === errorItem.dtype;
                        });
                        var tempG = _.find(errorQuestions, function(item){
                            return item.Dtype === group.Dtype;
                        });
                        if(!tempG) {
                            tempG = {Dtype: errorItem.dtype, question: []};
                            errorQuestions.push(tempG);
                        }
						var qst = _.find(group.question, function(q){
							if(q.isMain && q.sub.length){
								var isSub = false;
								_.each(q.sub, function (sub) {
									if(sub.QFLnkID === errorItem.qflnkid || sub.FLnkID === errorItem.qflnkid) {
										isSub = true;
                                        if(sub.Mode === 'a' || sub.Mode === 'A') {
                                            errorItem.Serial = sub.orders ? sub.orders : sub.dis;
                                        } else {
                                            errorItem.Serial = sub.dis ? sub.dis : (sub.orders + '.' + sub.SubOrder);
                                        }
                                        errorItem.Knowledges =  sub.knowledges;
                                        errorItem.Score = sub.score;
                                        errorItem.Parse = sub.analysis;
                                        errorItem.Diff = sub.DifficultLevel;
                                        errorItem.startDiff = viewStar(parseInt(sub.DifficultLevel));
                                        errorItem.Title = q.title + sub.title;
                                        errorItem.Answer = sub.Answer;
                                        errorItem.Option_a = $sce.trustAsHtml(sub.OptionOne);
                                        errorItem.Option_b = $sce.trustAsHtml(sub.OptionTwo);
                                        errorItem.Option_c = $sce.trustAsHtml(sub.OptionThree);
                                        errorItem.Option_d = $sce.trustAsHtml(sub.OptionFour);
									}
								});
								return isSub;
							} else {
								if(q.QFLnkID === errorItem.qflnkid) {
									errorItem.Serial = q.orders;
                                    errorItem.Knowledges =  q.knowledges;
                                    errorItem.Score = q.score;
                                    errorItem.Parse = q.analysis;
                                    errorItem.Diff = q.DifficultLevel;
                                    errorItem.startDiff = viewStar(parseInt(q.DifficultLevel));
                                    errorItem.Title = q.title;
                                    errorItem.Answer = q.Answer;
                                    errorItem.Option_a = $sce.trustAsHtml(q.OptionOne);
                                    errorItem.Option_b = $sce.trustAsHtml(q.OptionTwo);
                                    errorItem.Option_c = $sce.trustAsHtml(q.OptionThree);
                                    errorItem.Option_d = $sce.trustAsHtml(q.OptionFour);
								}
								return q.QFLnkID === errorItem.qflnkid
							}
						});
						if(qst) {
                            errorItem.ClassRightNum = +errorItem.totalnum - +errorItem.lostnum;
                            errorItem.ClassErrorNum = errorItem.lostnum;
                            errorItem.ClassErrorRate = errorItem.clostrate.toFixed(1);
                            errorItem.GradeErrorRate = errorItem.glostrate.toFixed(1);
                            errorItem.QFlnkID = errorItem.qflnkid;
                            errorItem.Source = qst.source || res.data.examName;
                            qst && tempG.question.push(errorItem);
						}
                    });
                }
                questionExamList = errorQuestions;
                $scope.sub1List = [];
                _.each(questionExamList, function (item, index) {
                	if(index === 0) {
                        $scope.sub1List = item.question;
					} else {
                        $scope.sub1List = $scope.sub1List.concat(item.question);
					}
                });
                $scope.sub1List = _.sortBy($scope.sub1List, 'Serial');
                sessionStorage.setItem("getallques", JSON.stringify($scope.sub1List));
				// 根据当前数字显示五角星
				function viewStar(num) {
					var str = '';
					for (var i = 0; i < num; i++) {
						str = str + '<span class="fa fa-star orange"></span>'
					}
					return $sce.trustAsHtml(str);
				}
            })
        });
		// $http({
		// 	method: 'post',
		// 	url: $rootScope.baseUrl + '/Interface0161.ashx',
		// 	data: {
		// 		ClassFlnkID: searchfrom.ClassFlnkID,
		// 		ExamFlnkID: searchfrom.ExamFlnkID,
		// 		Type: 0
		// 	}
		// }).success(function (data) {
        //
		// 	$scope.allquenum = data.msg.length;
		// 	var sub1 = [];
		// 	for (var i = 0 ; i < data.msg.length ; i++) {
		// 		if (data.msg[i].LostSc != 0) {
		// 			sub1.push({
		// 				'Serial': +data.msg[i].Serial,
		// 				'Title': $sce.trustAsHtml(data.msg[i].Title),
		// 				'Diff': data.msg[i].Diff,
		// 				'Qtype': data.msg[i].Qtype,
		// 				'Score': data.msg[i].Score,
		// 				'LostSc': data.msg[i].LostSc,
		// 				'ClassErrorRate': data.msg[i].ClassErrorRate,
		// 				'Parse': data.msg[i].Parse,
		// 				'ClassRightNum': data.msg[i].ClassRightNum,
		// 				'ClassErrorNum': data.msg[i].ClassErrorNum,
		// 				'GradeErrorRate': data.msg[i].GradeErrorRate,
		// 				'Source': $sce.trustAsHtml(data.msg[i].Source),
		// 				'Option_a': $sce.trustAsHtml(data.msg[i].Option_a),
		// 				'Option_b': $sce.trustAsHtml(data.msg[i].Option_b),
		// 				'Option_c': $sce.trustAsHtml(data.msg[i].Option_c),
		// 				'Option_d': $sce.trustAsHtml(data.msg[i].Option_d),
		// 				'Knowledges': $sce.trustAsHtml(data.msg[i].Knowledges),
		// 				'QFlnkID': data.msg[i].QFlnkID,
		// 				'Answer': $sce.trustAsHtml(data.msg[i].Answer),
		// 				'startDiff': viewStar(parseInt(data.msg[i].Diff)),
		// 				'Option': $sce.trustAsHtml(data.msg[i].Option_a + data.msg[i].Option_b + data.msg[i].Option_c + data.msg[i].Option_d),
		// 				PTitle: data.msg[i].PTitle
		// 			})
		// 		};
		// 	}
		// 	$scope.sub1List = _.sortBy(sub1, 'Serial');
		// 	sessionStorage.setItem("getallques", JSON.stringify($scope.sub1List));
		// });

		//PPT模式
		$scope.gopptmode = function () {
			window.open('#/EveryQuestion?ClassFlnkID=' + searchfrom.ClassFlnkID + '&ExamFlnkID=' + ExamFlnkID + '' + '&isOpenNewWin=' + 1);
		}
	}]
});
