/**
 * Created by Administrator on 2016/6/1.
 */
define([],function () {
    return ['$scope', '$rootScope', '$http', '$location', '$cookies', '$q', 'constantService', 'ngDialog',function ($scope, $rootScope, $http, $location, $cookies, $q, constantService, ngDialog) {
        var screenHeight = $(window).height() - 120;
        $('.right').height(screenHeight);

        var taskId = $location.search().taskId, action = $location.search().action, examId = $location.search().examId;
        $scope.action = action;
        var errorRange = 20;

        $scope.questionMap = [];
        $scope.maskList = [];

        $scope.startNum = 1;
        $scope.cutName = '';
        var start = 1;

        /**
         * 页面流程：根据地址栏传输的id，获取到试卷的详细内容，包括试卷的图片，
         * 在图片容器中展示图片；在图片容器中监听鼠标拖拽事件，处理相应逻辑
         */
        function supportCut(preCount, preWidth, pos){
            var count = preCount || 0;
            var $imgContainer = $('.paper-box');
            var isMouseDown = false;
            var isSHIFTDown = false;
            var firstDragWidth = preWidth || 0, firstDragPos = pos || null;
            var paperStartPoint = $imgContainer.offset();
            var dx, dy, mx, my, ux, uy;
            var startX, startY, width, height;
            var $mask = $('<div class="mask">');
            var newMask = null;
            $imgContainer.off('mousedown').on('mousedown', function(e){
                e.preventDefault();
                e.stopPropagation();
                isMouseDown = true;
                var ps = transPosition({
                    left: e.pageX,
                    top: e.pageY
                });
                dx = ps.left;
                dy = ps.top;
                $('.mask').removeClass('current');
                newMask = $mask.clone();
                newMask.css({
                    position: 'absolute',
                    width: 0,
                    height: 0,
                    left: dx + 'px',
                    top: dy + 'px',
                    opacity: '0.8'
                }).appendTo($imgContainer).addClass('current');
                startX = dx;
                startY = dy;
            });
            $imgContainer.off('mousemove').on('mousemove', function(e){
                e.preventDefault();
                e.stopPropagation();
                if(isMouseDown) {
                    var ps = transPosition({
                        left: e.pageX,
                        top: e.pageY
                    });
                    mx = ps.left;
                    my = ps.top;
                    newMask.css({
                        width: mx - dx + 'px',
                        height: my - dy + 'px'
                    });
                }
            });
            $imgContainer.off('mouseup').on('mouseup', function(e){
                handlerMouseUp(e);
            });
            $imgContainer.off('mouseleave').on('mouseleave', function(e){
                handlerMouseUp(e);
            });

            $scope.copy = function(q){
                ngDialog.open({
                    template: 'copyDialog',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'cut-copy-alert',
                    closeByDocument: true,
                    scope: $scope,
                    controller: function ($scope, $http) {
                        return function () {
                            $scope.ok = function(){
                                ngDialog.close();
                                var index = _.findIndex($scope.questionMap, function(item){
                                    return item.no === q.no;
                                });
                                var noInMaskList = _.filter($scope.maskList, function(item){
                                    return item.no === q.no;
                                });
                                for(var i = 0; i < $scope.copyTime; i++) {
                                    var newCut = {
                                        position: q.position,
                                        no: q.no + i + 1,
                                        order: (+q.order + i + 1) + ''
                                    };
                                    $scope.questionMap.push(newCut);
                                    _.each(noInMaskList, function(mask){
                                        var newMask = {
                                            no: q.no + i + 1,
                                            order: (+q.order + i + 1) + '',
                                            left: mask.left,
                                            top: mask.top,
                                            width: mask.width,
                                            height: mask.height
                                        };
                                        $scope.maskList.push(newMask);
                                    });
                                }
                                count += $scope.copyTime;
                            }
                        };
                    }
                })
            };

            function handlerMouseUp(e){
                if(isMouseDown) {
                    isMouseDown = false;
                    var ps = transPosition({
                        left: e.pageX,
                        top: e.pageY
                    });
                    ux = ps.left;
                    uy = ps.top;
                    if(Math.abs(ux - dx) < 5) {
                        newMask.remove();
                    }else {
                        //计算切图的宽高和起始位置
                        width = Math.abs(ux - dx);
                        height = Math.abs(uy - dy);
                        if(ux <= dx) {
                            startX = ux;
                        }
                        if(uy <= dy) {
                            startY = uy;
                        }
                        //SHIFT按键用于在跨页时，将多次切图合并到上一题里，第一次不考虑SHIFT键
                        if(isSHIFTDown && count > 0) {
                            width = fixWidth(width);
                            startX = fixPos({x: startX, y: startY}).x;
                            newMask.attr('id', count);
                            newMask.addClass(count).css({
                                width: width + 'px',
                                left: startX
                            });

                            var lastQ = _.find($scope.questionMap, function(q){
                                return q.no === count;
                            });
                            lastQ.position.push({
                                x: startX,
                                y: startY,
                                width: width,
                                height: height
                            });
                            $scope.$apply(function(){
                                $scope.maskList.push({
                                    left: startX,
                                    top: startY,
                                    width: width,
                                    height: height,
                                    no: count,
                                    order: lastQ.order
                                });
                            });
                            newMask.remove();
                        }else {
                            if(count === 0 || count === start - 1) {
                                firstDragWidth = width;
                                firstDragPos = {
                                    x: startX,
                                    y: startY
                                };
                            }else {
                                width = fixWidth(width);
                                startX = fixPos({x: startX, y: startY}).x;
                            }
                            count++;
                            $scope.currentCount = count;
                            newMask.attr('id', count);
                            newMask.addClass(count).css({
                                width: width + 'px',
                                left: startX
                            });

                            $scope.$apply(function(){
                                $scope.questionMap.push({
                                    position: [{
                                        x: startX.toFixed(1),
                                        y: startY.toFixed(1),
                                        width: width.toFixed(1),
                                        height: height.toFixed(1)
                                    }],
                                    no: count,
                                    order: count + ''
                                });
                                $scope.maskList.push({
                                    left: startX,
                                    top: startY,
                                    width: width,
                                    height: height,
                                    no: count,
                                    order: count + ''
                                });
                            });
                        }
                        newMask.remove();
                    }
                }else {
                    if(newMask != null && newMask != undefined){
                        newMask.remove();
                    }
                }
            }
            /**
             * 图片容器出现滚动条时，拖动滚动条会触发mousedown及mousemove/mouseup事件，导致计算一次切图
             * 此处监听scroll事件为修改该bug
             */
            $imgContainer.off('scroll').on('scroll', function(){
                isMouseDown = false;
            });
            $(window).off('keydown').on('keydown', function(e){
                if(e.shiftKey) {
                    isSHIFTDown = true;
                }
            }).off('keyup').on('keyup', function(e){
                if(e.keyCode === 16 || e.which === 16) {
                    isSHIFTDown = false;
                }
            });
            function transPosition(p){
                paperStartPoint = $imgContainer.offset();
                var scrollLeft = $imgContainer.scrollLeft();
                var px = p.left, py = p.top;
                var ox = px - paperStartPoint.left + scrollLeft,
                    oy = py - paperStartPoint.top;
                return {
                    left: ox,
                    top: oy
                }
            }

            function fixWidth(width){
                //首次切图后会记录第一次拖拽的宽度，后面再次切图以第一次切图校准，
                // 如果误差在容错范围（errorRange）之内，则将该次切图宽度设为首页记录的宽度，
                if(Math.abs(width - firstDragWidth) <= errorRange) {
                    return firstDragWidth;
                }else {
                    return width;
                }
            }

            function fixPos(pos) {
                if(Math.abs(pos.x - firstDragPos.x) <= errorRange) {
                    return {
                        x: firstDragPos.x,
                        y: pos.y
                    };
                }else {
                    return pos;
                }
            }
        }

        $scope.showCurrent = function(q){
            $scope.currentCount = q.no;
        };

        $scope.$watch('currentCount', function(newVal){
            if(newVal) {
                $('.mask').removeClass('current');
                $('.mask[id=' + newVal + ']').addClass('current');
                $('.mask.'+newVal).addClass('current');
            }
        });

        $scope.setStartNum = function(){
            start = $scope.startNum;
        };

        $http.post($rootScope.baseUrl + '/Interface0251.ashx', {
            flnkId: taskId
        }).then(function(res){
            if(_.isArray(res.data.msg)) {
                //入口，初始化切图任务元数据和名称、图片地址等，根据状态判断切图的展示情况
                $scope.taskMeta = res.data.msg[0];
                $scope.cutName = $scope.taskMeta.cutName;
                tryAccessory($scope.taskMeta.accessory);
                if(action === 'add') {
                    //如果是新增切片，则不需要加载之前的数据
                    supportCut();
                }else if(action === 'edit') {
                    //如果是修改，则加载之前的切片，从之前的位置开始切图
                    var positionArray = JSON.parse($scope.taskMeta.position);
                    $scope.questionMap = positionArray;
                    initMaskListByCutMeta($scope.questionMap);
                    var firstPos = _.first(positionArray);
                    var lastCount = _.last(positionArray).no;
                    var firstWidth = firstPos.position[0].width,
                        firstPosition = {
                            x: firstPos.position[0].x,
                            y: firstPos.position[0].y
                        };
                    supportCut(lastCount, firstWidth, firstPosition);
                }else {
                    //否则，不支持拖拽切图
                    $scope.currentCutId = $scope.taskMeta.cutFLnkId;
                    $scope.questionMap = JSON.parse($scope.taskMeta.position);
                    initMaskListByCutMeta($scope.questionMap);
                }
            }else {
                constantService.alert('加载试卷异常！');
            }
        });

        $scope.merge = function(q, index){
            if(index > 0) {
                var lastQst = $scope.questionMap[index - 1];
                if(lastQst.order.indexOf('.') > 0) {
                    //如果上一题是子题
                    var array = lastQst.order.split('.');
                    var lastQOrder = +array[0], lastQIdx = +array[1];
                    q.order = lastQOrder + '.' + (lastQIdx + 1);
                }else {
                    //如果不是
                    var lastOrder = lastQst.order;
                    lastQst.order = lastOrder + '.' + 1;
                    q.order = lastOrder + '.' + 2;
                }
            }
        };

        $scope.delete = function(q){
            var index = _.findIndex($scope.questionMap, function(item){
                return item.no === q.no;
            });
            var noInMaskList = _.filter($scope.maskList, function(item){
                return item.no === q.no;
            });
            $scope.questionMap.splice(index, 1);
            _.each(noInMaskList, function(item){
                var mIndex = _.findIndex($scope.maskList, function(m){
                    return item.no === m.no;
                });
                $scope.maskList.splice(mIndex, 1);
            });
            $('.mask[id=' + q.no + ']').remove();
            $('.mask.'+q.no).remove();
        };

        $scope.save = function(){
            var cutName = $scope.cutName || '';
            if(!cutName) {
                constantService.alert('请输入切图名称！切图名称将用于本次标记切图！');
                return;
            }
            $http.post($rootScope.baseUrl + '/Interface0225.ashx', {
                cutName: cutName,
                examFlnkID: examId,
                isFinish: 0,
                accessory: $scope.paperSrc,
                taskFLnkID: taskId,
                position: JSON.stringify($scope.questionMap)
            }).then(function(res){
                constantService.alert('保存切图成功！');
            });
        };

        $scope.submit = function(){
            var cutName = $scope.cutName || '';
            if(!cutName) {
                constantService.alert('请输入切图名称！切图名称将用于本次标记切图！');
                return;
            }
            $http.post($rootScope.baseUrl + '/Interface0225.ashx', {
                cutName: cutName,
                examFlnkID: examId,
                isFinish: 1,
                accessory: $scope.paperSrc,
                taskFLnkID: taskId,
                position: JSON.stringify($scope.questionMap)
            }).then(function(res){
                constantService.showConfirm('恭喜您，切图保存成功！是否应用到其他同期考试？', ['我要应用', '暂不需要'], function(){
                    $scope.apply2Other(res.data.msg);
                }, function(){
                    window.history.go(-1);
                });
            });
        };

        $scope.apply2Other = function(cutFId){
            cutFId = cutFId || $scope.currentCutId;
            ngDialog.open({
                template: 'apply2Paper',
                className: 'ngdialog-theme-default',
                appendClassName: 'task-apply-dialog',
                closeByDocument: true,
                scope: $scope,
                controller: function ($scope, $http) {
                    return function () {
                        $scope.$watch('relatedTaskList', function(val){
                            var existUnselect = !!_.find(val, function(task){
                                return !task.isSelect;
                            });
                            $scope.isAllSelected = !existUnselect;
                        }, true);

                        $scope.toggleSelect = function(){
                            _.each($scope.relatedTaskList, function(task){
                                task.isSelect = $scope.isAllSelected;
                            });
                        };

                        $scope.apply = function () {
                            var selectTask = _.filter($scope.relatedTaskList, function(task){
                                return task.isSelect;
                            });
                            var taskIds = _.pluck(selectTask, 'taskFLnkID');
                            var deferList = [];
                            _.each(taskIds, function(id){
                                deferList.push($http.post($rootScope.baseUrl + '/Interface0227.ashx', {
                                    cutId: cutFId,
                                    taskId: id
                                }));
                            });
                            $q.all(deferList).then(function(){
                                ngDialog.close();
                                var tipStr = _.map(selectTask, function(ts){
                                    return ts.examName + '（' + ts.ClassName + '）';
                                }).join('、');
                                constantService.alert('恭喜你，已成功为任务' + tipStr + '使用切图《' + $scope.cutName + '》！');
                            }, function(){
                                ngDialog.close();
                                constantService.alert('使用切图失败！');
                            });
                        };
                    }
                }
            });
        };


        $scope.enterFullScreen = function(){
            var $dom = $('.do-cut .box-body')[0];
            if($dom.mozRequestFullScreen) {
                $dom.mozRequestFullScreen();
            }else if($dom.webkitRequestFullScreen) {
                $dom.webkitRequestFullScreen();
            }else if($dom.msRequestFullScreen) {
                $dom.msRequestFullScreen();
            }else if($dom.requestFullScreen) {
                $dom.requestFullScreen();
            }
        };

        $scope.$watch('questionMap', function(newVal){
            if(action === 'view') {
                return;
            }
            if(newVal && newVal.length > 0) {
                var initCount = start - 1;
                _.each(newVal, function(item, index){
                    if(item.order.indexOf('.') > 0) {
                        //如果是子题编号
                        var mainOrder = +item.order.split('.')[0],
                            subOrder = +item.order.split('.')[1];
                        if(subOrder > 1) {
                            item.order = initCount + '.' + subOrder;
                        }else {
                            initCount++;
                            item.order = initCount + '.' + subOrder;
                        }
                    }else {
                        initCount++;
                        item.order = initCount + '';
                    }
                })
            }else {
                $scope.maskList = [];
            }
        }, true);

        function initMaskListByCutMeta(questionMap){
            var masks = [];
            _.each(questionMap, function(item){
                _.each(item.position, function(p){
                    masks.push({
                        left: p.x,
                        top: p.y,
                        width: p.width,
                        height: p.height,
                        no: item.no,
                        order: item.order
                    });
                });
            });
            $scope.maskList = masks;
        }

        function getPeriodTask(){
            $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                Proc_name: 'GetPeriodTasks',
                taskFid: taskId
            }).then(function(res){
                if(_.isArray(res.data.msg)) {
                    $scope.relatedTaskList = res.data.msg;
                }else {
                    $scope.relatedTaskList = [];
                }
            });
        }
        getPeriodTask();

        var tryAccessoryCount = 0;

        function tryAccessory(url) {
            if(url.indexOf('img.51jyfw.com/ftp/') === -1) {
                url = 'http://img.51jyfw.com/ftp/' + url;
            }
            var $img = $('<img>').attr('src', url);
            $img.on('error', function(){
                tryAccessoryCount++;
                if(tryAccessoryCount >= 10) {
                    constantService.alert('本试卷无图片文件！');
                    return;
                }
                $http.post($rootScope.baseUrl + '/Interface0251.ashx', {
                    flnkId: taskId
                }).then(function(res){
                    if(_.isArray(res.data.msg) && res.data.msg.length > 0) {
                        tryAccessory(res.data.msg[0].accessory);
                    }
                });
            }).on('load', function(){
                $scope.$apply(function(){
                    $scope.paperSrc = url;
                });
            });
        }
    }];
});