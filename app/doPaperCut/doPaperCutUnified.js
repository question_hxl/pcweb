/**
 * Created by Administrator on 2016/6/1.
 */
define([],function () {
    return ['$scope', '$rootScope', '$http', '$location', '$cookies', '$q', 'constantService', 'ngDialog',function ($scope, $rootScope, $http, $location, $cookies, $q, constantService, ngDialog) {
        var screenHeight = $(window).height();

        var taskId = $location.search().taskId, action = $location.search().action, examId = $location.search().examId, unifiedId = $location.search().unifiedId;
        $scope.action = action;
        var errorRange = 20;

        $scope.maskList = [];

        $scope.startNum = 1;
        $scope.cutName = '';
        var start = 0;

        /**
         * 页面流程：根据地址栏传输的id，获取到试卷的详细内容，包括试卷的图片，
         * 在图片容器中展示图片；在图片容器中监听鼠标拖拽事件，处理相应逻辑
         */
        function supportCut(preCount, preWidth, pos){
            var count = preCount || 0;
            var $imgContainer = $('.paper-box');
            var isMouseDown = false;
            var isSHIFTDown = false;
            var firstDragWidth = preWidth || 0, firstDragPos = pos || null;
            var paperStartPoint = $imgContainer.offset();
            var dx, dy, mx, my, ux, uy;
            var startX, startY, width, height;
            var $mask = $('<div class="mask">');
            var newMask = null;
            $imgContainer.off('mousedown').on('mousedown', function(e){
                e.preventDefault();
                e.stopPropagation();
                isMouseDown = true;
                var ps = transPosition({
                    left: e.pageX,
                    top: e.pageY
                });
                dx = ps.left;
                dy = ps.top;
                $('.mask').removeClass('current');
                newMask = $mask.clone();
                newMask.css({
                    position: 'absolute',
                    width: 0,
                    height: 0,
                    left: dx + 'px',
                    top: dy + 'px',
                    opacity: '0.8'
                }).appendTo($imgContainer).addClass('current');
                startX = dx;
                startY = dy;
            });
            $imgContainer.off('mousemove').on('mousemove', function(e){
                e.preventDefault();
                e.stopPropagation();
                if(isMouseDown) {
                    var ps = transPosition({
                        left: e.pageX,
                        top: e.pageY
                    });
                    mx = ps.left;
                    my = ps.top;
                    newMask.css({
                        width: mx - dx + 'px',
                        height: my - dy + 'px'
                    });
                }
            });
            $imgContainer.off('mouseup').on('mouseup', function(e){
                handlerMouseUp(e);
            });
            $imgContainer.off('mouseleave').on('mouseleave', function(e){
                handlerMouseUp(e);
            });

            function handlerMouseUp(e){
                if(isMouseDown) {
                    isMouseDown = false;
                    var ps = transPosition({
                        left: e.pageX,
                        top: e.pageY
                    });
                    ux = ps.left;
                    uy = ps.top;
                    if(Math.abs(ux - dx) < 5) {
                        newMask.remove();
                    }else {
                        //计算切图的宽高和起始位置
                        width = Math.abs(ux - dx);
                        height = Math.abs(uy - dy);
                        if(ux <= dx) {
                            startX = ux;
                        }
                        if(uy <= dy) {
                            startY = uy;
                        }
                        //SHIFT按键用于在跨页时，将多次切图合并到上一题里，第一次不考虑SHIFT键
                        if(isSHIFTDown && count > 0) {
                            width = fixWidth(width);
                            startX = fixPos({x: startX, y: startY}).x;
                            newMask.attr('id', count);
                            newMask.addClass(count).css({
                                width: width + 'px',
                                left: startX
                            });

                            var q = findUnifiedQstBySortOrder(start);
                            q.cutMeta.position.push({
                                x: +(startX.toFixed(1)),
                                y: +(startY.toFixed(1)),
                                width: +(width.toFixed(1)),
                                height: +(height.toFixed(1))
                            });
                            $scope.$apply(function(){
                                $scope.maskList.push({
                                    left: startX,
                                    top: startY,
                                    width: width,
                                    height: height,
                                    no: count,
                                    showOrder: start + ''
                                });
                            });
                            newMask.remove();
                        }else {
                            if(count === 0 || count === start - 1) {
                                firstDragWidth = width;
                                firstDragPos = {
                                    x: startX,
                                    y: startY
                                };
                            }else {
                                width = fixWidth(width);
                                startX = fixPos({x: startX, y: startY}).x;
                            }
                            count++;
                            start++;
                            $scope.currentCount = start;
                            newMask.attr('id', count);
                            newMask.addClass(count).css({
                                width: width + 'px',
                                left: startX
                            });
                            var q = findUnifiedQstBySortOrder(start);
                            $scope.$apply(function(){
                                q.cutMeta = {
                                    position: [{
                                        x: +(startX.toFixed(1)),
                                        y: +(startY.toFixed(1)),
                                        width: +(width.toFixed(1)),
                                        height: +(height.toFixed(1))
                                    }],
                                    no: count,
                                    showOrder: start + ''
                                };
                                $scope.maskList.push({
                                    left: startX,
                                    top: startY,
                                    width: width,
                                    height: height,
                                    no: count,
                                    showOrder: start + ''
                                });
                            });
                        }
                        newMask.remove();
                    }
                }else {
                    if(newMask != null && newMask != undefined){
                        newMask.remove();
                    }
                }
                getCuttedCount();
            }
            /**
             * 图片容器出现滚动条时，拖动滚动条会触发mousedown及mousemove/mouseup事件，导致计算一次切图
             * 此处监听scroll事件为修改该bug
             */
            $imgContainer.off('scroll').on('scroll', function(){
                isMouseDown = false;
            });
            $(window).off('keydown').on('keydown', function(e){
                if(e.shiftKey) {
                    isSHIFTDown = true;
                }
            }).off('keyup').on('keyup', function(e){
                if(e.keyCode === 16 || e.which === 16) {
                    isSHIFTDown = false;
                }
            });
            function transPosition(p){
                paperStartPoint = $imgContainer.offset();
                var scrollLeft = $imgContainer.scrollLeft();
                var scrollTop  = $imgContainer.scrollTop();
                var px = p.left, py = p.top;
                var ox = px - paperStartPoint.left + scrollLeft,
                    oy = py - paperStartPoint.top + scrollTop;
                return {
                    left: ox,
                    top: oy
                }
            }

            function fixWidth(width){
                //首次切图后会记录第一次拖拽的宽度，后面再次切图以第一次切图校准，
                // 如果误差在容错范围（errorRange）之内，则将该次切图宽度设为首页记录的宽度，
                if(Math.abs(width - firstDragWidth) <= errorRange) {
                    return firstDragWidth;
                }else {
                    return width;
                }
            }

            function fixPos(pos) {
                if(Math.abs(pos.x - firstDragPos.x) <= errorRange) {
                    return {
                        x: firstDragPos.x,
                        y: pos.y
                    };
                }else {
                    return pos;
                }
            }
        }

        $scope.startCutCurrent = function(q){
            start = q.sortOrder - 1;
            deleteCut(q);
        };

        $scope.delete = function(q){
            deleteCut(q);
        };

        function deleteCut(q){
            q.cutMeta = null;
            var filterred = _.filter($scope.maskList, function(mask){
                return mask.showOrder === (q.sortOrder + '');
            });
            _.each(filterred, function(item){
                var index = _.findIndex($scope.maskList, function(mask){
                    return mask.showOrder === item.showOrder;
                });
                $scope.maskList.splice(index, 1);
            });
        }

        $scope.showCurrent = function(q){
            $scope.currentCount = q.sortOrder;
        };

        //加载统考配置，初始化题目列表
        $http.post($rootScope.baseUrl + '/Interface0181.ashx', {
            examId: examId,
            unifiedItemFid: unifiedId
        }).then(function(res){
            var unifiedCfg = res.data.UnifiedItemTemplate,
                groups = res.data.msg;
            var disIdMap = {};
            _.each(unifiedCfg, function(cfg){
                var key = cfg.DType + '@@' + cfg.ShowDis;
                if(disIdMap[key]) {
                    disIdMap[key].push(cfg.QFlnkID);
                }else {
                    disIdMap[key] = [cfg.QFlnkID];
                }
            });
            var mixedGroups = [];
            for(var key in disIdMap) {
                var dtype = key.split('@@')[0], showDis = key.split('@@')[1];
                var pushedDtype = _.find(mixedGroups, function(g){
                    return g.dtype === dtype;
                });
                if(!!pushedDtype) {
                    pushedDtype.questions.push({
                        showDis: showDis,
                        ids: disIdMap[key],
                        meta: []
                    });
                }else {
                    mixedGroups.push({
                        dtype: dtype,
                        questions: [{
                            showDis: showDis,
                            ids: disIdMap[key],
                            meta: []
                        }]
                    });
                }
            }
            var qsts = [];
            _.each(groups, function(g){
                _.each(g.question, function(q){
                    if(q.sub && q.sub.length > 0) {
                        _.each(q.sub, function(s){
                            s.parent = q;
                        });
                        qsts = qsts.concat(q.sub);
                    }else {
                        qsts.push(q);
                    }
                });
            });
            var sortOrder = 1;
            _.each(mixedGroups, function(g){
                _.each(g.questions, function(item){
                    item.sortOrder = sortOrder++;
                     _.each(item.ids, function(id){
                         var metaQ = getQstById(qsts, id);
                         item.meta.push(metaQ);
                         var order = metaQ.orders + '';
                         if(metaQ.parent && metaQ.parent.Mode === 'B') {
                             order = metaQ.parent.orders + '.' + metaQ.qtorders;
                         }
                         if(item.orders) {
                             item.orders.push(order);
                         }else {
                             item.orders = [order];
                         }
                     });
                });
            });
            $scope.totalCount = sortOrder - 1;
            $scope.unifiedGroups = mixedGroups;
            fillCutMeta();
        });

        function getQstById(qsts, id){
            return _.find(qsts, function(q){
                return id === q.FLnkID;
            });
        }

        function findUnifiedQstBySortOrder(sortOrder){
            var finded;
            for(var i = 0; i < $scope.unifiedGroups.length; i++) {
                var g = $scope.unifiedGroups[i];
                finded = _.find(g.questions, function(q){
                    return +q.sortOrder === +sortOrder;
                });
                if(finded) break;
            }
            return finded;
        }

        function fillCutMeta(){
            $http.post($rootScope.baseUrl + '/Interface0251.ashx', {
                flnkId: taskId
            }).then(function(res){
                if(_.isArray(res.data.msg)) {
                    //入口，初始化切图任务元数据和名称、图片地址等，根据状态判断切图的展示情况
                    $scope.taskMeta = res.data.msg[0];
                    $scope.cutName = $scope.taskMeta.cutName;
                    tryAccessory($scope.taskMeta.accessory);
                    if(action === 'add') {
                        //如果是新增切片，则不需要加载之前的数据
                        supportCut();
                    }else if(action === 'edit') {
                        //如果是修改，则加载之前的切片，从之前的位置开始切图
                        var positionArray = JSON.parse($scope.taskMeta.position);
                        var lastCount = initMaskListByCutMeta(positionArray);
                        var firstPos = _.first(positionArray);
                        var firstWidth = +firstPos.position[0].width,
                            firstPosition = {
                                x: +firstPos.position[0].x,
                                y: +firstPos.position[0].y
                            };
                        supportCut(lastCount, firstWidth, firstPosition);
                    }else {
                        //否则，不支持拖拽切图
                        $scope.currentCutId = $scope.taskMeta.cutFLnkId;
                        initMaskListByCutMeta(JSON.parse($scope.taskMeta.position));
                        console.log($scope.taskMeta.position);
                        console.log(JSON.parse($scope.taskMeta.position))
                    }
                    getCuttedCount();
                }else {
                    constantService.alert('加载试卷异常！');
                }
            });
        }

        function parseCutMetaFromUnifiedGroup(){
            var cutMap = [];
            _.each($scope.unifiedGroups, function(ug){
                //分组下的所有showDis数组
                _.each(ug.questions, function(ugq){
                    //如果有切图数据，则为所有orders添加切图
                    if(ugq.cutMeta) {
                        _.each(ugq.orders, function(order){
                            var systemOrderCut = JSON.parse(JSON.stringify(ugq.cutMeta));
                            systemOrderCut.order = order;
                            cutMap.push(systemOrderCut);
                        });
                    }
                });
            });
            console.log(cutMap);
            return cutMap;
        }

        function getCuttedCount(){
            var count = 0;
            _.each($scope.unifiedGroups, function(ug){
                //分组下的所有showDis数组
                _.each(ug.questions, function(ugq){
                    //如果有切图数据，则为所有orders添加切图
                    if(ugq.cutMeta) {
                        count++;
                    }
                });
            });
            $scope.cuttedCount = count;
        }

        $scope.save = function(){
            var cutName = $scope.cutName || '';
            if(!cutName) {
                constantService.alert('请输入切图名称！切图名称将用于本次标记切图！');
                return;
            }
            var cutData = parseCutMetaFromUnifiedGroup();
            $http.post($rootScope.baseUrl + '/Interface0225.ashx', {
                cutName: cutName,
                examFlnkID: examId,
                isFinish: 0,
                accessory: $scope.paperSrc,
                taskFLnkID: taskId,
                position: JSON.stringify(cutData)
            }).then(function(res){
                constantService.alert('保存切图成功！');
            });
        };

        $scope.submit = function(){
            var cutName = $scope.cutName || '';
            if(!cutName) {
                constantService.alert('请输入切图名称！切图名称将用于本次标记切图！');
                return;
            }
            $http.post($rootScope.baseUrl + '/Interface0225.ashx', {
                cutName: cutName,
                examFlnkID: examId,
                isFinish: 1,
                accessory: $scope.paperSrc,
                taskFLnkID: taskId,
                position: JSON.stringify(parseCutMetaFromUnifiedGroup())
            }).then(function(res){
                if($scope.relatedTaskList && $scope.relatedTaskList.length > 0) {
                    constantService.showConfirm('恭喜您，切图保存成功！是否应用到其他同期考试？', ['我要应用', '暂不需要'], function(){
                        $scope.apply2Other(res.data.msg);
                    }, function(){
                        window.history.go(-1);
                    });
                }else {
                    constantService.alert('恭喜您，切图保存成功!', function(){
                        window.history.go(-1);
                    });
                }
            });
        };

        $scope.showHowTo = function(){
            ngDialog.open({
                template: 'howToTip',
                className: 'ngdialog-theme-default',
                appendClassName: 'task-apply-dialog',
                closeByDocument: true,
                scope: $scope
            });
        };

        $scope.apply2Other = function(cutFId){
            cutFId = cutFId || $scope.currentCutId;
            ngDialog.open({
                template: 'apply2Paper',
                className: 'ngdialog-theme-default',
                appendClassName: 'task-apply-dialog',
                closeByDocument: true,
                scope: $scope,
                controller: function ($scope, $http) {
                    return function () {
                        $scope.$watch('relatedTaskList', function(val){
                            var existUnselect = !!_.find(val, function(task){
                                return !task.isSelect;
                            });
                            $scope.isAllSelected = !existUnselect;
                        }, true);

                        $scope.toggleSelect = function(){
                            _.each($scope.relatedTaskList, function(task){
                                task.isSelect = $scope.isAllSelected;
                            });
                        };

                        $scope.apply = function () {
                            var selectTask = _.filter($scope.relatedTaskList, function(task){
                                return task.isSelect;
                            });
                            var taskIds = _.pluck(selectTask, 'taskFLnkID');
                            var deferList = [];
                            _.each(taskIds, function(id){
                                deferList.push($http.post($rootScope.baseUrl + '/Interface0227.ashx', {
                                    cutId: cutFId,
                                    taskId: id
                                }));
                            });
                            $q.all(deferList).then(function(){
                                ngDialog.close();
                                var tipStr = _.map(selectTask, function(ts){
                                    return ts.examName + '（' + ts.ClassName + '）';
                                }).join('、');
                                constantService.alert('恭喜你，已成功为任务' + tipStr + '使用切图《' + $scope.cutName + '》！');
                            }, function(){
                                ngDialog.close();
                                constantService.alert('使用切图失败！');
                            });
                        };
                    }
                }
            });
        };

        function initMaskListByCutMeta(questionMap){
            var masks = [];
            var count = 0;
            _.each($scope.unifiedGroups, function(ug){
                _.each(ug.questions, function(q){
                    //同一题号下的切图都是一样的，通过第一个系统题号获取切图
                    var order = q.orders[0];
                    var cutData = _.find(questionMap, function(p){
                        return (p.order+"") === (order+"");
                    });
                    if(cutData) {
                        q.cutMeta = cutData;
                        start = q.sortOrder;
                        cutData.sortOrder = q.sortOrder;
                        count++;
                        _.each(cutData.position, function(p){
                            masks.push({
                                left: p.x,
                                top: p.y,
                                width: p.width,
                                height: p.height,
                                no: cutData.no,
                                showOrder: cutData.order + ''
                            });
                        });
                    }
                });
            });
            $scope.maskList = masks;
            return count;
        }

        function getPeriodTask(){
            $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                Proc_name: 'GetPeriodTasks',
                taskFid: taskId
            }).then(function(res){
                if(_.isArray(res.data.msg)) {
                    $scope.relatedTaskList = res.data.msg;
                }else {
                    $scope.relatedTaskList = [];
                }
            });
        }
        getPeriodTask();

        var tryAccessoryCount = 0;

        function tryAccessory(url) {
            if(url.indexOf('img.51jyfw.com/ftp/') === -1) {
                url = 'http://img.51jyfw.com/ftp/' + url;
            }
            var $img = $('<img>').attr('src', url);
            $img.on('error', function(){
                tryAccessoryCount++;
                if(tryAccessoryCount >= 10) {
                    constantService.alert('本试卷无图片文件！');
                    return;
                }
                $http.post($rootScope.baseUrl + '/Interface0251.ashx', {
                    flnkId: taskId
                }).then(function(res){
                    if(_.isArray(res.data.msg) && res.data.msg.length > 0) {
                        tryAccessory(res.data.msg[0].accessory);
                    }
                });
            }).on('load', function(){
                $scope.$apply(function(){
                    $scope.paperSrc = url;
                });
            });
        }
    }];
});