/**
 * Created by Administrator on 2017/2/16 0016.
 */
define(['underscore-min', 'jqprint', '../../service/assignRouter'], function () {
    return ['$scope', '$state', 'ngDialog', '$timeout', 'apiCommon',
        function ($scope, $state, ngDialog, $timeout, apiCommon) {
            var user = angular.fromJson(sessionStorage.getItem('currentUser'));
            var param1 = {
                Proc_name: 'getSchoolGrade',
                schoolfId: user.schoolFId
            };
            apiCommon.generalQuery(param1).success(function (res) {
                $scope.pharseList = res.msg;
                if(!$scope.pharseList.length) {
                    $scope.isEmpty = true;
                }
            });
            var param2 = {
                Proc_name: 'GetGradeClasss',
                SchoolFid: user.schoolFId,
                GradNum: '0'
            };
            apiCommon.generalQuery(param2).then(function (res) {
                $scope.curClassList = res.data.msg;
                _.each($scope.curClassList, function (item) {
                    _.each($scope.pharseList, function (GradeName) {
                        if (+item.GradeNum === GradeName.GradeNum) {
                            item.gradeName = GradeName.gradeName;
                        }
                    })
                });
                //过滤垃圾班级
                $scope.curClassList = _.filter($scope.curClassList, function (item) {
                    return item.SmallNum !== null;
                });
                $scope.curClassList = _.sortBy($scope.curClassList, 'SmallNum');
                $scope.curClassList = _.sortBy($scope.curClassList, 'GradeNum');
            });

            $scope.curPharse = [];
            $scope.curClass = [];
            $scope.selectGrade = function (item) {
                var index = _.indexOf($scope.curPharse, item.gradeName);
                if (index >= 0) {
                    $scope.curPharse.splice(index, 1);
                } else {
                    $scope.curPharse.push(item.gradeName);
                }
                var param = {
                    Proc_name: 'GetGradeClasss',
                    SchoolFid: user.schoolFId,
                    GradNum: item.GradeNum
                };
                apiCommon.generalQuery(param).then(function (res) {
                    if (_.isArray(res.data.msg) && res.data.msg[0]) {
                        var curGrade = res.data.msg;
                        curGrade = _.sortBy(curGrade, 'SmallNum');
                        curGrade = _.sortBy(curGrade, 'GradeNum');
                        if (index >= 0) {
                            _.each(curGrade, function (ret) {
                                var i = _.indexOf($scope.curClass, ret.FLnkID);
                                if (i >= 0)
                                    $scope.curClass.splice(i, 1);
                            });
                        } else {
                            _.each(curGrade, function (ret) {
                                if (_.indexOf($scope.curClass, ret.FLnkID) === -1) {
                                    $scope.curClass.push(ret.FLnkID);
                                }
                            });
                        }
                    }
                });
            };
            $scope.selectClass = function (item) {
                var index = _.indexOf($scope.curClass, item.FLnkID);
                if (index >= 0) {
                    $scope.curClass.splice(index, 1);
                } else {
                    $scope.curClass.push(item.FLnkID);
                }
            };
            $scope.goWxCode = function (index) {
                if ($scope.curClass.length > 0) {
                    if(index) {
                        window.open("#/examOutCode?allOrSelf=1" + '&QrCode=1' + '&wxFull=1' + '&ClassFlnkID=' + $scope.curClass + '&isOpenNewWin=' + 1);
                    } else {
                        window.open("#/wxOutCode?classFLnkId=" + $scope.curClass + '&isOpenNewWin=' + 1);
                    }
                } else {
                    $scope.noClass = true;
                    $timeout(function () {
                        $scope.noClass = false;
                    }, 900);
                }

            };
        }]
});