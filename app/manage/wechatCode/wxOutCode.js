/**
 * Created by Administrator on 2017/2/16 0016.
 */
define(['jqprint', '../../service/assignRouter'], function () {
    return ['$scope', '$location', '$q', 'apiCommon',
        function ($scope, $location, $q, apiCommon) {
            var user = angular.fromJson(sessionStorage.getItem('currentUser'));
            $scope.classFLnkId = $location.search().classFLnkId;
            $scope.classID = $scope.classFLnkId.split(',');
            $scope.noStudata = false;
            var deferList = [];
            _.each($scope.classID, function (val) {
                var param = {
                    classFLnkId: val,
                    schoolFLnkId: user.schoolFId,
                    needWx: 1
                };
                deferList.push(apiCommon.getStudentList(param));
            });
            $q.all(deferList).then(function (res) {
                $scope.stuList = [];
                _.each(res, function (item) {
                    if (item.data && _.isArray(item.data.msg) && item.data.msg.length) {
                        $scope.className = item.data.msg[0].className;
                        $scope.ss = item.data.msg;
                        _.each($scope.ss, function (data) {
                            $scope.stuList.push(data);
                        });
                    }
                });
                $scope.stuList = _.sortBy($scope.stuList, 'studentSmallNo');
                $scope.stuList = _.sortBy($scope.stuList, 'classNo');
                if ($scope.stuList.length === 0) {
                    //无学生信息
                    $scope.noStudata = true;
                }
            });
            $scope.onprint = function () {
                $("#btnPrint").jqprint({
                    operaSupport: false
                });
            }
        }]
});


