/**
 * Created by Administrator on 2017/2/21 0021.
 */
define(['underscore-min', 'jquery.datetimepicker', '../../service/assignRouter'], function () {
    return ['$scope', '$state', 'ngDialog', '$timeout', '$location', 'constantService', 'apiCommon',
        function ($scope, $state, ngDialog, $timeout, $location, constantService, apiCommon) {

            var user = angular.fromJson(sessionStorage.getItem('currentUser'));
            $scope.schoolName = user.schoolName;
            $scope.studentFLnkId = $location.search().studentFLnkId;
            $scope.currentclassFLnkID = $location.search().classFLnkId;
            $scope.new = $location.search().New;
            $scope.errorShow = false;
            $scope.queueList = [
                {name: '首页',url:'myApp.masterHome'},
                {name: '师生管理',url:'myApp.t_s_manage.s_manageHome'},
                {name: '学生管理',url:'myApp.t_s_manage.masterStuCtrl'},
                {name: '修改资料',url:'myApp.editStudent'}];

            if ($scope.currentclassFLnkID && $scope.studentFLnkId) {
                apiCommon.GetSingleBaseUser($scope.studentFLnkId).then(function (resp) {
                    $scope.stuInfro = resp.data.ResultObj;
                    $scope.stuInfro.EmpID = $scope.stuInfro.EmpID;
                    $scope.currentGradeNum = $scope.stuInfro.GradNo;
                    $scope.studentSFID = $scope.stuInfro.SFID;
                    classList();
                });
            }else {
                $scope.stuInfro = [];
            }
            apiCommon.generalQuery({
                Proc_name: 'getSchoolGrade',
                schoolfId: $scope.user.schoolFId
            }).success(function (res) {
                $scope.pharseList =res.msg;
            });
            function classList() {
                apiCommon.generalQuery({
                    Proc_name: 'GetGradeClasss',
                    SchoolFid: user.schoolFId,
                    GradNum: $scope.currentGradeNum
                }).then(function (res) {
                    $scope.classroom = res.data.msg;
                    $scope.classroom = _.sortBy($scope.classroom, 'SmallNum');
                })
            }

            //修改年级
            $scope.changeGrade = function (val) {
                if ($scope.currentGradeNum === val) {
                    return;
                }
                $scope.currentGradeNum = val;
                classList();
            };
            $scope.changeClass = function (val) {
                $scope.currentclassFLnkID = val
            };
            $scope.save = function () {
                $scope.successShow = false;
                $scope.stuInfro.Birthday = $('#timepicker').val() ||'';
                if ($scope.stuInfro.UserName && $scope.stuInfro.UserName.length > 20) {
                    constantService.showSimpleToast('名字最大长度20个字符!');
                    return;
                }
                var data = {
                    FLnkID: $scope.stuInfro.FLnkID,
                    CreateByUserID: user.fid,
                    SchoolFLnkId: user.schoolFId,
                    pharseId: user.pharseId,
                    UserName: $scope.stuInfro.UserName,
                    SelfName : $scope.stuInfro.SelfName,
                    LoginName: '',
                    ClassFLnkID: $scope.currentclassFLnkID,
                    Email: $scope.stuInfro.Email || '',
                    Telephone: $scope.stuInfro.Telephone || '',
                    Sex: $scope.stuInfro.Sex || '',
                    GradNo: $scope.currentGradeNum,
                    Birthday: $scope.stuInfro.Birthday || '',
                    EmpID: $scope.stuInfro.EmpID,
                    SmallNum: +$scope.stuInfro.SmallNum,
                    SFID: $scope.stuInfro.SFID,
                    Career: '学生',
                    CarrerNew: '学生',
                    RoleId: '5'
                };
                var defer = apiCommon.UpdateBaseUserByStudent(data);
                if($scope.new === '2'){
                    data.FLnkID = '';
                    data.LoginName = $scope.stuInfro.SelfName;
                    defer = apiCommon.InsertBaseUserByStudent(data);
                }
                var require = [(data.FLnkID && $scope.new === '1') || (data.FLnkID === '' && $scope.new === '2')]&& data.UserName && data.SelfName && data.ClassFLnkID && data.SmallNum && data.EmpID && data.CreateByUserID;
                if(require){
                    defer.then(function (res) {
                        if(res.data.Flag){
                            constantService.alert(res.data.Message,function () {
                                location.href = "#/s_manage/studentCtrl?classFLnkId=" + $scope.currentclassFLnkID + '&GradeNum=' + $scope.currentGradeNum;
                            })
                        } else {
                            constantService.alert(res.data.Message);
                        }
                    });
                }else {
                    constantService.alert('信息未填写完整，请完善！');
                }
            };
            $scope.back = function () {
                history.back();
            };
            $('#timepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: 'Y-m-d',
                todayButton: true,
                maxDate: new Date(),
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    $('#timepicker').datetimepicker('hide');
                }
            });
        }]
});