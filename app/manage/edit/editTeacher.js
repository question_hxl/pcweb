/**
 * Created by Administrator on 2017/2/17 0017.
 */
define(['underscore-min', 'jquery.datetimepicker'], function () {
    return ['$rootScope', '$http', '$scope', '$state', 'ngDialog', '$timeout', '$location', 'resourceUrl', 'constantService', '$q',
        function ($rootScope, $http, $scope, $state, ngDialog, $timeout, $location, resourceUrl, constantService, $q) {

            var user = angular.fromJson(sessionStorage.getItem('currentUser'));
            $scope.userFId = $location.search().userFId;
            $scope.New = $location.search().New;         //1:修改 2：新增
            $scope.currentclassFLnkID = [];
            $scope.curRole = {};
            var gradeNum = '', GRADEID;
            $scope.queueList = [
                {name: '首页',url:'myApp.masterHome'},
                {name: '师生管理',url:'myApp.t_s_manage.s_manageHome'},
                {name: '教师管理',url:'myApp.t_s_manage.masterTeacherCtrl'},
                {name: '修改资料',url:'myApp.edit'}];

            if ($scope.New === '1') {
                $http.get(resourceUrl + '/BaseUser/GetTeacherDetailView?teacherId='+$scope.userFId).then(function (resp) {
                    $scope.cuser = resp.data.ResultObj;
                    $scope.cuser.pharseId = $scope.cuser.pharseId ? $scope.cuser.pharseId : user.pharseId; //维护: 有些老师pharseId为null
                    $scope.teacherName = $scope.cuser.UserName;
                    $scope.curRole = {
                        RoleID :$scope.cuser.RoleId,
                        RoleName: $scope.cuser.CarrerNew
                    };
                    $scope.selectClassroom = $scope.cuser.TeacherSubjectClass;
                    $scope.cursubjectId = $scope.cuser.SubjectId;
                    if(+$scope.curRole.RoleID === 4) {
                        if($scope.selectClassroom.length !== 0){
                            $scope.cuserDetail = $scope.cuser.TeacherSubjectClass[0];
                            $scope.curClassFLnkID = $scope.cuserDetail.classFId;
                            $scope.currentGradeNum = $scope.cuserDetail.GradeNum;
                            $scope.curGradeNum = $scope.cuserDetail.GradeNum;
                            $scope.curClassGradeNum = $scope.cuserDetail.GradeNum;
                            $scope.currentgradeId = $scope.cuserDetail.GradeId;
                            GRADEID = $scope.cuserDetail.GradeId;
                            $scope.currentsubjectId = $scope.cuserDetail.subjectId;
                            $scope.currentsubjectFLnkID = $scope.cuserDetail.SubjectFLnkID;
                            $scope.currenteditionId = $scope.cuserDetail.EditionId;
                        }
                    }else {
                        $scope.currentGradeNum = $scope.cuser.GradNo;
                        $scope.curGradeNum = $scope.cuser.GradNo;
                        $scope.curClassGradeNum = $scope.cuser.GradNo;
                        $scope.curClassFLnkID = $scope.cuser.ClassFLnkID;
                    }
                    gradeNum = $scope.currentGradeNum;
                    if (+$scope.curRole.RoleID === 2) {
                        getKeMu();
                    }else if (+$scope.curRole.RoleID === 4) {
                        getKeMu();
                        getPublish($scope.currentsubjectId, 1);
                    }else if(+$scope.curRole.RoleID === 3){
                        getClassroom($scope.curClassGradeNum);
                    }
                    parentSonUser();
                });
            }else {
                $scope.cuserDetail = [];
                $scope.cuser = [];
                $scope.cuser.pharseId = user.pharseId;
            }
            function parentSonUser() {
                //账号关联，查主、子账号
                $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    Proc_name: 'Proc_getRelationUsers',
                    userflnkid: $scope.userFId
                }).success(function (data) { /*加载老师列表*/
                    $scope.curUser = {
                        parentUser : ''
                    };
                    if (_.isArray(data.msg) && data.msg.length) {
                        var RelationList = data.msg, sonList = [];
                        _.each(RelationList, function (item) {
                            if(!item.RelationID) {
                                $scope.curUser.parentUser = item.UserName;
                                $scope.curUser.id = item.FLnkID
                            }else {
                                sonList.push(item);
                            }
                        });
                        $scope.curUser.son = sonArray2Str(sonList);
                    }
                });
            }

            function sonArray2Str(item) {
                var temStr = [];
                if (_.isArray(item) && item.length > 0) {
                    for (var i = 0; i < item.length; i++) {
                        temStr.push(item[i].UserName);
                    }
                }
                if (temStr.length > 0) {
                    return temStr.toString();
                } else {
                    return '';
                }
            }

            $scope.relatedAccount = function () {
                $http.post($rootScope.baseUrl + '/Interface0300.ashx', {
                    schoolFId: user.schoolFId
                }).then(function (resp) {
                    if (_.isArray(resp.data.msg)) {
                        var temList = [];
                        for (var i = 0; i < resp.data.msg.length; i++) {
                            if (resp.data.msg[i].userFId === $scope.userFId) {
                                continue;
                            } else {
                                temList.push(resp.data.msg[i]);
                            }
                        }
                        $scope.currentRelateList = temList;
                        ngDialog.open({
                            template: 'paSonUser',
                            className: 'ngdialog-theme-default',
                            appendClassName: 'paSonUser-confirm-to-setting',
                            closeByDocument: true,
                            scope: $scope,
                            controller: function ($scope) {
                                return function () {
                                    $scope.binded = {
                                        name: $scope.curUser.parentUser === '' ? '无' : $scope.curUser.parentUser,
                                        id: $scope.curUser.id === '' ? '无' : $scope.curUser.id
                                    };
                                    $scope.bindAcc = function (id, name) {
                                        $http.post($rootScope.baseUrl + '/Interface0301.ashx', {
                                            userFId: $scope.userFId,
                                            pUserFId: id
                                        }).then(function (resp) {
                                            if (resp.data.code === 0) {
                                                $scope.binded.name = name;
                                                $scope.binded.id = id;
                                                parentSonUser();
                                                ngDialog.close();
                                            } else {
                                                constantService.showSimpleToast('操作失败!');
                                            }
                                        }, function () {
                                            constantService.showSimpleToast('操作失败!');
                                        });
                                    };
                                }
                            }
                        });
                    } else {
                        constantService.showSimpleToast('获取关联账号失败!');
                    }
                }, function (resp) {
                    constantService.showSimpleToast('获取关联账号失败!');
                });
            };

            $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                Proc_name: 'getSchoolGrade',
                schoolfId: $scope.user.schoolFId
            }).success(function (res) {
                $scope.phaseList =res.msg;
                getRoleList();
            });

            function getRoleList() {
                $http.get(resourceUrl + '/BaseUser/GetBaseRoleList').then(function (resp) {
                    $scope.roleList = resp.data.ResultObj;
                    $scope.roleList = _.filter($scope.roleList, function (item) {
                        return  item.RoleID !== 5;
                    });
                });
            }
            function getKeMu() {
                $http.post($rootScope.baseUrl + '/Interface0220B.ashx', {
                    pharseId: $scope.cuser.pharseId
                }).success(function (res) {
                    if (_.isArray(res.msg)) {
                        $scope.keMu = _.uniq(res.msg, 'subjectId');
                    } else {
                        $scope.keMu = [];
                    }
                });
            }
            function getPublish(subjectID, isfirst) {
                $http.post($rootScope.baseUrl + '/Interface0281.ashx', {
                    pharseId: $scope.cuser.pharseId,
                    subjectId: subjectID
                }).success(function (res) {
                    if (_.isArray(res.msg) && res.msg !== "数量为空！") {
                        //出版社
                        $scope.pubLishing = _.uniq(res.msg, 'editionName');
                        if($scope.New === '1' && $scope.selectClassroom.length !== 0){
                            if($scope.cuserDetail && $scope.cuserDetail.EditionId){
                                var sid = _.findIndex($scope.pubLishing, {editionId: $scope.cuserDetail.EditionId.toString()});
                                if (isfirst && sid === -1) {
                                    $scope.currenteditionId = '';
                                } else {
                                    sid = sid >= 0 ? sid : 0;
                                    $scope.currenteditionId = $scope.pubLishing[sid].editionId;
                                }
                            }else {
                                $scope.currenteditionId = '';
                            }
                        }else {
                            $scope.currenteditionId = $scope.pubLishing[0].editionId;
                        }
                        //教材
                        $scope.allGrading = res.msg;
                        $scope.allGrading.unshift({
                            editionId: "",
                            editionName: "",
                            gradeId: "",
                            gradeName: "— — — — —请选择— — — — —"
                        });
                        $scope.teaching = _.filter($scope.allGrading, function (item) {
                            return item.editionId === $scope.currenteditionId || item.editionId === "";
                        });
                        if($scope.New === '1' && $scope.selectClassroom.length !== 0){
                            if($scope.cuserDetail && $scope.cuserDetail.GradeId){
                                var sidd = _.findIndex($scope.teaching, {gradeId: $scope.cuserDetail.GradeId.toString()});
                                if (!isfirst || sidd !== -1) {
                                    sidd = sidd >= 1 ? sidd : 1;
                                    $scope.curTeaching = $scope.teaching[sidd];
                                } else {
                                    $scope.curTeaching = $scope.teaching[0];
                                }
                            }else {
                                $scope.curTeaching = $scope.teaching[0];
                            }
                        }else {
                            $scope.curTeaching = $scope.teaching[0];
                        }
                    } else {
                        $scope.pubLishing = [];
                        $scope.teaching = [];
                    }
                    if ($scope.currentGradeNum) {
                        getClassroom($scope.currentGradeNum, isfirst);
                    }
                });
            }
            function getClassroom(gradeNum, isfirst) {
                var defer;
                if(+$scope.cuser.pharseId !== 3 && +$scope.curRole.RoleID === 4) {
                    if($scope.curTeaching && $scope.curTeaching.gradeId) {
                        defer = $http.post($rootScope.baseUrl + '/Interface0258.ashx', {
                            pharseId: $scope.cuser.pharseId,
                            gradeId: $scope.curTeaching.gradeId,
                            schoolFLnkID: user.schoolFId
                        })
                    } else {
                        $scope.classroom = [];
                        return
                    }
                } else {
                    defer = $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                        Proc_name: 'GetGradeClasss',
                        SchoolFid: user.schoolFId,
                        GradNum: gradeNum
                    })
                }
                defer.success(function (res) {
                    if (_.isArray(res.msg) && res.msg[0]) {
                        $scope.classroom = res.msg;
                        if(+$scope.cuser.pharseId !== 3 && +$scope.curRole.RoleID === 4) {
                            _.each(res.msg, function (item) {
                                item.FLnkID = item.classFLnkID;
                                item.ClassName = item.className
                            });
                        } else {
                            $scope.classroom = _.sortBy($scope.classroom, 'SmallNum');
                        }
                        if(isfirst){
                            _.each($scope.selectClassroom, function (item) {
                                $scope.currentclassFLnkID.push(item.ClassFLnkID);
                            });
                        }
                    } else {
                        $scope.classroom = [];
                    }
                });
            }
            $scope.$watch('curTeaching', function (newValue, oldValue) {
                if ((newValue === oldValue) || !newValue) {
                    return;
                }
                $scope.currentgradeId = newValue.gradeId;
                if(+$scope.cuser.pharseId !== 3 && oldValue) {
                    getClassroom(0)
                }
            });
            //修改角色
            $scope.changeRole = function (val) {
                if (+$scope.curRole.RoleID === +val.RoleID) {
                    return;
                }
                $scope.curRole = val;
                if (+val.RoleID === 4 || +val.RoleID === 2) {
                    getKeMu();
                }
                if (+val.RoleID === 4 && !($scope.curTeaching && $scope.curTeaching.gradeId)) {
                    $scope.classroom = [];
                }
            };
            //修改科目
            $scope.changeSub = function (va) {
                if(+$scope.curRole.RoleID !== 2) {
                    if ($scope.currentsubjectId === va.subjectId) {
                        return;
                    }
                    $scope.currentsubjectFLnkID = va.subjectFLnkId;
                    $scope.currentsubjectId = va.subjectId;
                    getPublish(va.subjectId, 0);
                } else{
                    if($scope.cursubjectId === va.subjectId) {
                        $scope.cursubjectId = '';
                    } else {
                        $scope.cursubjectId = va.subjectId;
                    }
                }
            };
            //修改出版社
            $scope.changePublish = function (va) {
                if ($scope.currenteditionId === va) {
                    return;
                }
                $scope.currenteditionId = va;
                $scope.teaching = _.filter($scope.allGrading, function (item) {
                    return item.editionId === $scope.currenteditionId || item.editionId === '';
                });
                $scope.curTeaching = $scope.teaching[0];
            };
            //修改年级
            $scope.changeGrade = function (val,item) {
                if(item === 2){
                    if($scope.curGradeNum === val) {
                        $scope.curGradeNum = 0;
                    } else {
                        $scope.curGradeNum = val;
                    }
                }else if(item === 3){
                    $scope.curClassGradeNum = val;
                    getClassroom(val, 0);
                }else {
                    $scope.currentGradeNum = val;
                    getClassroom(val, 0);
                }
            };
            //修改班级
            $scope.changeClassroom = function (va, item) {
                if(item === 4){
                    var require = ($scope.currentGradeNum && gradeNum && +$scope.currentGradeNum !== +gradeNum) || +GRADEID !== +$scope.currentgradeId;
                    if(require){
                        $scope.currentclassFLnkID = [];
                        $scope.currentclassFLnkID.push(va.FLnkID);
                        gradeNum = va.GradeNum;
                        GRADEID = $scope.currentgradeId;
                    }else {
                        var index = _.indexOf($scope.currentclassFLnkID, va.FLnkID);
                        if (index >= 0) {
                            $scope.currentclassFLnkID.splice(index, 1);
                        } else {
                            $scope.currentclassFLnkID.push(va.FLnkID);
                        }
                    }
                } else {
                    if ($scope.curClassFLnkID === va.FLnkID) {
                        return;
                    }
                    $scope.curClassFLnkID = va.FLnkID;
                }

            };

            $scope.save = function () {
                $scope.errorShow = false;
                $scope.successShow = false;
                $scope.cuser.birthday = $('#timepicker').val() ||'';
                if (!$scope.cuser.Mobile || $scope.cuser.Mobile.length === 0) {
                    constantService.showSimpleToast('请输入联系电话!');
                    return;
                }
                var data = {
                    FLnkID: $scope.cuser.FLnkID,
                    UserName: $scope.cuser.UserName,
                    CreateByUserID: user.fid,
                    pharseId: $scope.cuser.pharseId,
                    SchoolFLnkId: user.schoolFId,
                    LoginName: $scope.cuser.SelfName,
                    SelfName: $scope.cuser.SelfName,
                    SFID: $scope.cuser.SFID || '',
                    Email: $scope.cuser.Email || '',
                    Mobile: $scope.cuser.Mobile || '',
                    Telephone: $scope.cuser.Mobile || '',
                    Sex: $scope.cuser.Sex || '',
                    Birthday: $scope.cuser.Birthday || '',
                    Career: $scope.curRole.RoleName,
                    RoleId: $scope.curRole.RoleID,
                    CarrerNew: $scope.curRole.RoleName
                };
                var data4,data3,data2,param;
                if (+$scope.curRole.RoleID === 4){    //老师
                    var gradeId = $scope.curTeaching ? $scope.curTeaching.gradeId : '';
                    data4 = data;
                    data4.SubjectId = $scope.currentsubjectId || '';
                    data4.editionId = $scope.currenteditionId || '';
                    data4.GradNo = $scope.currentGradeNum || '';
                    data4.BaseTeacherClassSubject = [];
                    data4.gradeID = gradeId || '';
                    data4.ClassFLnkID = '';
                    _.each($scope.currentclassFLnkID, function (classFLnkID) {
                        data4.BaseTeacherClassSubject.push({
                            SubjectFLnkID: $scope.currentsubjectFLnkID,
                            ClassFLnkID: classFLnkID,
                            EditionId: $scope.currenteditionId,
                            GradeId: gradeId,
                            IsDeleted: false
                        })
                    });
                    _.each($scope.selectClassroom, function (oldItem) {
                        _.each(data4.BaseTeacherClassSubject, function (newItem) {
                            if(newItem.ClassFLnkID === oldItem.ClassFLnkID){
                                oldItem.IsDeleted = true;
                                newItem.FLnkID = oldItem.FLnkID;
                            }
                        });
                    });
                    _.each($scope.selectClassroom, function (oldItem) {
                        if(!oldItem.IsDeleted){
                            data4.BaseTeacherClassSubject.push({
                                SubjectFLnkID: $scope.currentsubjectFLnkID,
                                ClassFLnkID: oldItem.ClassFLnkID,
                                EditionId: $scope.currenteditionId,
                                GradeId: $scope.curTeaching.gradeId,
                                FLnkID: oldItem.FLnkID,
                                IsDeleted: true
                            })
                        }
                    });
                    param = data4;
                }else if (+$scope.curRole.RoleID === 3){ // 班主任
                    data3 = data;
                    data3.GradNo = $scope.curClassGradeNum;
                    data3.ClassFLnkID = $scope.curClassFLnkID;
                    data3.SubjectId = '';
                    data3.editionId = '';
                    data3.gradeID = '';
                    param = data3;
                }else if (+$scope.curRole.RoleID === 2){   //年级主任
                    data2 = data;
                    data2.GradNo = $scope.curGradeNum || '';
                    data2.SubjectId = $scope.cursubjectId || '';
                    data2.ClassFLnkID = '';
                    data2.editionId = '';
                    data2.gradeID = '';
                    param = data2;
                }else {   //校长|| 校管理员
                    param = data;
                    param.GradNo = '';
                    param.SubjectId = '';
                    param.ClassFLnkID = '';
                    param.editionId = '';
                    param.gradeID = '';
                }
                //修改角色处理
                if(+$scope.curRole.RoleID !== 4) {
                    param.BaseTeacherClassSubject = [];
                    _.each($scope.selectClassroom, function (oldItem) {
                        param.BaseTeacherClassSubject.push({
                            SubjectFLnkID: $scope.currentsubjectFLnkID,
                            ClassFLnkID: oldItem.ClassFLnkID,
                            EditionId: $scope.currenteditionId,
                            GradeId: $scope.curTeaching ? $scope.curTeaching.gradeId : '',
                            FLnkID: oldItem.FLnkID,
                            IsDeleted: true
                        })
                    });
                }
                var url = '/BaseUser/UpdateBaseUserByTeacher';
                if($scope.New === '2'){
                    param.FLnkID = '';
                    param.LoginName = $scope.cuser.SelfName;
                    url = '/BaseUser/InsertBaseUserByTeacher';
                }
                var require = [(param.FLnkID &&$scope.New === '1') ||(param.FLnkID === '' &&$scope.New === '2')] && param.UserName && param.SelfName && param.RoleId;
                // else if($scope.New === '3') {
                //     param.userFLnkID = $scope.userFId;
                //     require = param.addOrmodify === '3' && param.roleId;
                // }

               // var teaRequire = param.classFLnkID && param.subjectID && param.editionId && param.gradeID;
                if(require){
                    if(+$scope.curRole.RoleID === 3) {
                        isExistClassMaster($scope.curRole.RoleID, $scope.curClassFLnkID).then(function(resp){
                            if (resp.Flag) {
                                sureSave(gradeId, url, param);
                            } else {
                                constantService.confirm('提示', resp.Message + '，是否修改', ['确定', '取消'], function () {
                                    sureSave(gradeId, url, param);
                                }, function () {
                                    ngDialog.closeAll();
                                });
                                return resp.Flag
                            }
                        });
                    } else {
                        sureSave(gradeId, url, param);
                    }
                }else {
                    constantService.alert('信息未填写完整，请完善！');
                }
            };
            function isExistClassMaster(RoleId, ClassLnkID){
                var deferred = $q.defer();
                $http.post(resourceUrl + '/BaseClass/IsExsitsClassMaster', {
                    RoleId: RoleId,
                    ClassLnkID: ClassLnkID
                }).success(function (res) {
                    deferred.resolve(res);
                }, function (res) {
                    defer.reject(res);
                });
                return deferred.promise;
            }
            function getGradeNum(gradeId){
                var deferred = $q.defer();
                $http.get(resourceUrl + '/BaseUser/GetGradeNum?gradeId=' + gradeId).success(function (res) {
                    deferred.resolve(res);
                }, function (res) {
                    defer.reject(res);
                });
                return deferred.promise;
            }
            function updateBaseUser(url, param) {
                $http.post(resourceUrl + url, param).then(function (res) {
                    if (res.data.Flag) {
                        constantService.alert(res.data.Message, function () {
                            history.back();
                        })
                    } else {
                        constantService.alert(res.data.Message);
                    }
                });
            }
            function sureSave(gradeId, url, param) {
                if (+$scope.cuser.pharseId !== 3 && gradeId) {
                    getGradeNum(gradeId).then(function (resp) {
                        param.GradNo = resp.ResultObj;
                        updateBaseUser(url, param);
                    });
                }else {
                    updateBaseUser(url, param);
                }
            }
            $scope.back = function () {
                history.back();
            };
            $('#timepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: 'Y-m-d',
                todayButton: true,
                maxDate: new Date(),
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    $('#timepicker').datetimepicker('hide');
                }
            });
        }]
});