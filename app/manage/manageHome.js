/**
 * Created by Administrator on 2017/2/22 0022.
 */
define(['underscore-min', 'jquery.datetimepicker', '../service/assignRouter'], function () {
    return ['$location', '$scope', '$state', 'resourceUrl', '$timeout', 'ngDialog', 'constantService','$q', 'Upload', 'apiCommon',
        function ($location, $scope, $state, resourceUrl, $timeout, ngDialog, constantService, $q, Upload, apiCommon) {
            $scope.user = angular.fromJson(sessionStorage.getItem('currentUser'));
            var schoolUpstatus = 0;//学校升级状态 0 不需要升级 1 选择升级 2 强制升级
            //查询学校升级
            getSchoolUpStatus().then(function (res) {
                schoolUpstatus = res.data.msg;
                if(res.data.msg === '2'){
                    constantService.confirm('学校升级提示','若选择升级，学校中所以可升级班级都将进行升级操作，若不升级将不能导入新的数据',['一键升级','暂不升级'],function () {
                        schoolUp().then(function (resdata) {
                            constantService.alert('升级成功!');
                            //升级成功后将本地状态置为不需升级
                            schoolUpstatus = '0';
                            //刷新页面数据
                            handleInitPage($scope.user.schoolFId).then(function (resg) {
                                $scope.pharseList = resg.data.msg;
                                getClassList();
                            });
                        },function (resdata) {
                            constantService.alert((!!resdata.data.msg?resdata.data.msg:'升级操作失败请稍后再试!'));
                        });
                    })
                }
            });
            handleInitPage($scope.user.schoolFId).then(function (res) {
                $scope.pharseList = res.data.msg;
                getClassList();
            });
            // apiCommon.generalQuery({
            //     Proc_name: 'getSchoolGrade',
            //     schoolfId: $scope.user.schoolFId
            // }).success(function (res) {
            //     $scope.pharseList = res.msg;
            //     getClassList();
            // });

            function getClassList() {
                apiCommon.generalQuery({
                    Proc_name: 'GetGradeClasss',
                    SchoolFid: $scope.user.schoolFId,
                    GradNum: '0'
                }).then(function (res) {
                    $scope.classList = res.data.msg;
                    $scope.classList = _.sortBy($scope.classList, 'SmallNum');
                    apiCommon.getClassList({
                        schoolFLnkId: $scope.user.schoolFId
                    }).then(function (res) {
                        _.each($scope.classList, function (klass) {
                            _.each(res.data.msg, function (item) {
                                if (item.classFLnkId === klass.FLnkID) {
                                    klass.openSubject = item.openSubject;
                                    klass.studentNum = item.studentNum;
                                }
                            });
                        });
                        _.each($scope.pharseList, function (pharse) {
                            pharse.ClassList = [];
                            _.each($scope.classList, function (klass) {
                                klass.openSubject = klass.openSubject.split(",").join("  ");
                                klass.Period = klass.Period? klass.Period: klass.Session;
                                if (pharse.GradeNum === klass.GradeNum) {
                                    pharse.ClassList.push(klass);
                                }
                            });
                        });
                    });
                });
            }

            $scope.cancel = function () {
                ngDialog.close();
            };
            $scope.teaCtrl = function (tea) {
                location.href = "#/s_manage/teacherCtrl?classFLnkId=" + tea.FLnkID;
            };
            $scope.stuCtrl = function (stu) {
                location.href = "#/s_manage/studentCtrl?classFLnkId=" + stu.FLnkID + '&GradeNum=' + stu.GradeNum;
            };
            var isTeacher = '4';
            $scope.addData = function (name, index) {
                ngDialog.open({
                    template: 'manage/importData.html',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'importData-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: false,
                    controller: function ($scope) {
                        return function () {
                            $scope.userName = name;
                            isTeacher = index;
                        }
                    }
                });

            };
            $scope.uploadDataCheck = function (picFile, name) {
                if (!picFile) {
                    constantService.alert('请选择文件！');
                    return;
                } else {
                    if (picFile.name) {
                        var stuFiletype = picFile.name.substr(picFile.name.lastIndexOf(".")).toLowerCase();
                        if (!((stuFiletype === '.xls') || (stuFiletype === '.xlsx'))) {
                            constantService.alert('文件格式不对！');
                            return;
                        }
                    } else {
                        constantService.alert("请命名文件后重新上传！");
                        return;
                    }
                }
                ngDialog.close();
                $scope.userName = name;
                $scope.allowUpLoad = true;
                var url;
                if(+isTeacher === 4) {
                    url = '/File/UploadTeacherFile';
                } else {
                    url = '/File/UploadStudentFile';
                }
                Upload.upload({
                    url: resourceUrl + url,
                    data: {
                        file: picFile,
                        fileKey: isTeacher,
                        roleId: isTeacher,
                        schoolId: $scope.user.schoolFId
                    }
                }).then(function (resp) {
                    if(resp.data.Flag){
                        $scope.tableList = resp.data.ResultObj;
                        _.each($scope.tableList, function (item) {
                            if(item.FailReason){
                                $scope.allowUpLoad = false;
                            }
                        });
                        ngDialog.open({
                            template: 'uploadCheck',
                            className: 'ngdialog-theme-plain',
                            appendClassName: 'uploadCheck-confirm-to-setting',
                            scope: $scope,
                            closeByDocument: false,
                            controller: function ($scope) {
                                return function () {
                                    $scope.uploadData = function () {
                                        var sureDefer;
                                        if(+isTeacher === 4) {
                                            sureDefer = apiCommon.sureImportTeacherList($scope.tableList)
                                        } else {
                                            sureDefer = apiCommon.sureImportStudentList($scope.tableList)
                                        }
                                        sureDefer.then(function (response) {
                                            ngDialog.close();
                                            constantService.alert(response.data.Message);
                                        }, function () {
                                            ngDialog.close();
                                            constantService.alert('上传异常！');
                                        });
                                    };
                                }
                            }
                        });
                    }else {
                        constantService.alert(resp.data.Message);
                    }
                });
            };
            $scope.delClass = function (klass) {
                var message;
                if(+klass.studentNum) {
                    message = '该班已有' + +klass.studentNum + '名学生，'
                } else {
                    message = '';
                }
                constantService.confirm('删除',message + '确认删除'+ klass.ClassName + '?', [''],function () {
                    apiCommon.deleteBaseClass({
                        FLnkID: klass.FLnkID
                    }).then(function (resp) {
                        if(resp.data.Flag){
                            _.each($scope.pharseList, function (grade) {
                                if(klass.GradeNum === grade.GradeNum) {
                                    var r = _.findIndex(grade.ClassList, {FLnkID: klass.FLnkID});
                                    grade.ClassList.splice(r, 1);
                                }
                            })
                        }
                    });
                },$scope);
            };
            $scope.addClass = function (index, klass) {
                // $scope.upDownList = [
                //     {name: '上', id: 0},
                //     {name: '下', id: 1}
                // ];
                // $scope.curHalfSession = $scope.upDownList[0];
                $scope.isAddClass = index; // 1:添加；2:编辑
                ngDialog.open({
                    template: 'editInFro',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'editInFro-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: false,
                    controller: function ($scope) {
                        return function () {
                            apiCommon.generalQuery({
                                Proc_name:'Proc_getSchoolGrade',
                                schoolflnkid: $scope.user.schoolFId
                            }).success(function (res) {
                                if(res.msg && _.isArray(res.msg)){
                                    $scope.schoolAllPharseList = res.msg;
                                    if(index === 2){
                                        apiCommon.GetSingerBaseClassListPageViewDetail(klass.FLnkID).then(function (res) {
                                            if(res.data.Flag) {
                                                getMaster();
                                                $scope.curTypeClassTypeID = res.data.ResultObj.Type;
                                                $scope.curTeacherFLnkID = res.data.ResultObj.MasterTeacher;
                                                $scope.curTypeName = res.data.ResultObj.TypeName;
                                            }
                                        });
                                    } else {
                                        getMaster();
                                    }
                                }
                            });

                            function getMaster() {
                                var param = {
                                    ClassFLnkID: "",
                                    SchoolFLnkId: $scope.user.schoolFId,
                                    RoleId: '3',
                                    Page: {
                                        PageIndex: 1,
                                        PageSize: 10000
                                    }
                                };
                                apiCommon.GetPageUserListPageView(param).then(function (resp) {
                                    if(resp.data.Flag){
                                        $scope.teacherList = resp.data.ResultObj.DataSource;
                                        $scope.teacherList.unshift({UserName: '---请选择---'});
                                        $scope.curTeachers = $scope.teacherList[0];
                                        apiCommon.GetBaseClassTypeList().then(function (res) {
                                            $scope.science = res.data.ResultObj;
                                            $scope.curTypes = $scope.science[0];
                                            $scope.curGrades = $scope.schoolAllPharseList[0];
                                            if(index === 2) {
                                                _.each($scope.teacherList, function (item) {
                                                    if(item.FLnkID=== $scope.curTeacherFLnkID){
                                                        $scope.curTeachers = item;
                                                    }
                                                });
                                                if(!$scope.curTeachers){
                                                    $scope.curTeachers = $scope.teacherList[0];
                                                }
                                                _.each($scope.schoolAllPharseList, function (item) {
                                                    if(item.GradeNum === klass.GradeNum) {
                                                        $scope.curGrades = item;
                                                    }
                                                });
                                                _.each($scope.science, function (item) {
                                                    if(item.TypeName === $scope.curTypeName) {
                                                        $scope.curTypes = item;
                                                    }
                                                });
                                                $scope.SmallNum = +klass.SmallNum;
                                                $scope.className = klass.ClassName;
                                                $scope.Period = +klass.Period;
                                            }
                                        });
                                    }
                                });
                            }
                            $scope.$watch('curGrades', function (val) {
                                if(!val){
                                    return
                                }
                                $scope.Period = +val.Period;
                            });
                            $scope.saveClass = function (index) {
                                var data ={
                                    FLnkID: '',
                                    ClassName: $scope.className,
                                    CreateByUserID: $scope.user.fid,
                                    SchoolFlnkID: $scope.user.schoolFId,
                                    GradeNum: $scope.curGrades.GradeNum,
                                    Session: '',
                                    SmallNum: $scope.SmallNum,
                                    UpOrDown: 0,
                                    Period: $scope.Period,
                                    Type: $scope.curTypes.ClassTypeID,
                                    UserFLnkId: $scope.curTeachers.FLnkID || '',
                                    RoleId: "3"
                                };
                                if( index=== 2) {
                                    data.FLnkID = klass.FLnkID;
                                    data.CreateByUserID = klass.CreateByUserID || '';
                                    data.UpOrDown = klass.UpOrDown;
                                    data.Period = $scope.Period;
                                }
                                var require = (data.Type !== null && data.Type !== undefined) && data.ClassName && data.GradeNum && data.Period && data.SmallNum, url;
                                if (require) {
                                    var defer;
                                    if(index === 2) {
                                        defer = apiCommon.UpdateBaseClassMaster(data);
                                    }else {
                                        defer = apiCommon.InsertBaseClassMaster(data);
                                    }
                                    defer.then(function (resp) {
                                        if(resp.data.Flag){
                                            ngDialog.close();
                                            if(index === 2) {
                                                _.each($scope.pharseList, function (grade) {
                                                    if(klass.GradeNum === grade.GradeNum) {
                                                        var r = _.findIndex(grade.ClassList, {FLnkID: klass.FLnkID});
                                                        grade.ClassList[r].SmallNum = $scope.SmallNum;
                                                        grade.ClassList[r].Period = $scope.Period;
                                                        grade.ClassList[r].ClassName = $scope.className;
                                                        klass.SmallNum = $scope.SmallNum;
                                                        klass.Period = $scope.Period;
                                                        klass.ClassName = $scope.className;
                                                    }
                                                })
                                            }
                                            constantService.alert(resp.data.Message,function () {
                                                if(index === 1) {
                                                    getClassList();
                                                }
                                            });
                                        }else {
                                            constantService.alert(resp.data.Message);
                                        }
                                    });
                                }else {
                                    constantService.alert('信息未填写完整，请完善！');
                                }
                            }
                        }
                    }
                });
            };

            $scope.outCode = function (index, code) {
                ngDialog.open({
                    template: 'printCode',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'printCode-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: false,
                    controller: function ($scope) {
                        return function () {
                            $scope.QrCodeModel = 2;//排列模式：AABBCC
                            $scope.QrCode = 1;      //打印份数
                            $scope.QrcModel = function (QrCodeModel) {
                                if ($scope.QrCodeModel !== QrCodeModel) {
                                    $scope.QrCodeModel = QrCodeModel;
                                }
                            };
                            $scope.code = index;//区分班级、年级
                            $scope.currentclassFLnkID = [];
                            $scope.goQrCode = function (res) {
                                if (res === 1) {
                                    $scope.currentclassFLnkID.push(code.FLnkID);
                                } else {
                                    _.each(code.ClassList, function (item) {
                                        $scope.currentclassFLnkID.push(item.FLnkID);
                                    })
                                }
                                ngDialog.close();
                                if ($scope.QrCodeModel === 1) {
                                    window.open("#/examOutCode?allOrSelf=" + '1&QrCode=' + $scope.QrCode + '&ClassFlnkID=' + $scope.currentclassFLnkID + '&isOpenNewWin=' + 1);
                                } else {
                                    window.open("#/examOutCode?allOrSelf=" + '1&QrCode2=' + $scope.QrCode + '&ClassFlnkID=' + $scope.currentclassFLnkID + '&isOpenNewWin=' + 1);
                                }
                            };
                        }
                    }
                });
            };
            $scope.outChatCode = function (code) {
                $scope.curClass = [];
                _.each(code.ClassList, function (item) {
                    $scope.curClass.push(item.FLnkID);
                });
                window.open("#/wxOutCode?classFLnkId=" + $scope.curClass + '&isOpenNewWin=' + 1);
            };
            function setXY() {
                if ($('#manageHome').offset().left > 160) {
                    $('.keyMap').css({'left': ($('#manageHome').offset().left - 160) + 'px', 'opacity': '1'});
                } else {
                    $('.keyMap').css({'left': 0, 'opacity': '0.01'})
                }
            }
            setXY();
            $(window).resize(function() {
                if ($state.current.name === 'myApp.t_s_manage.s_manageHome') {
                    setXY();
                }
            });
            $scope.curIndex = 0;
            var stopMinute = true;
            $scope.goGrade = function (grade, itemIndex) {
                stopMinute = false;
                $scope.curIndex = itemIndex;
                _.each($scope.pharseList, function (item, index) {
                    if(itemIndex === index) {
                        $('.gradeMap' + index).addClass('current');
                    }else {
                        $('.gradeMap' + index).removeClass('current');
                    }
                });
                var height = $('#grade' + itemIndex).offset().top;
                $(document).scrollTop(height);
                $timeout(function () {
                    stopMinute = true;
                }, 15);
            };
            window.onscroll = function () {
                if(!stopMinute || !$scope.pharseList.length) {
                    return
                }
                if (window.location.hash === '#/s_manage/manageHome') {
                    var height,curHeight, maxHeight;
                    height = $(document).scrollTop();
                    maxHeight = $('#grade' + ($scope.pharseList.length - 1)).offset().top;
                    _.each($scope.pharseList, function (item, index) {
                        $('.gradeMap' + index).removeClass('current');
                    });
                    for (var i = 0; i < $scope.pharseList.length; i++) {
                        curHeight = $('#grade' + i).offset().top;
                        if (i === 0 && $scope.pharseList.length > 1 && height < ($('#grade' + 1).offset().top)){
                            $scope.curIndex = 0;
                            break
                        } else if (i === $scope.pharseList.length - 1 && height > curHeight) {
                            $scope.curIndex = i;
                            break
                        } else if((i !== 0) && (height < curHeight && height > $('#grade' + (i - 1)).offset().top)){
                            if(height !== maxHeight) {
                                $scope.curIndex = i- 1;
                            }
                            break
                        }
                    }
                    $('.gradeMap' + $scope.curIndex).addClass('current');
                } else {
                    window.onscroll = null;
                }
            };

            $scope.upgrade = function () {
                if(schoolUpstatus === '0'){
                    constantService.alert('暂不需要升级!');
                    return;
                }else{
                    schoolUp().then(function (res) {
                        constantService.alert('升级成功!');
                        //升级成功后将本地状态置为不需升级
                        schoolUpstatus = '0';
                        //刷新页面数据
                        handleInitPage($scope.user.schoolFId).then(function (resg) {
                            $scope.pharseList = resg.msg;
                            getClassList();
                        });
                    },function (res) {
                        constantService.alert((!!res.data.msg?res.data.msg:'升级操作失败请稍后再试!'));
                    });
                }
            };
            /**
             *  检查升级
             *  schoolmanager/getSchoolUpStatus.ashx  msg返回 0 1 2 ,0=不需要，1=选择，2=强制
             */
            function getSchoolUpStatus() {
                var defer = $q.defer();
                apiCommon.getSchoolUpStatus().then(function (res) {
                    if(res.data.code === 0){
                        defer.resolve(res);
                    }else{
                        defer.reject(res);
                    }
                },function (res) {
                    defer.reject(res);
                });
                return defer.promise;
            }

            /**
             * 学校升级：
             *  schoolmanager/schoolUp.ashx
             */
            function schoolUp(data) {
                var defer = $q.defer();
                apiCommon.SchoolUp().then(function (res) {
                    if(res.data.code === 0){
                        defer.resolve(res);
                    }else{
                        defer.reject(res);
                    }
                },function (res) {
                    defer.reject(res);
                });
                return defer.promise;
            }

            /**
             * 页面初始化方法
             */
            function handleInitPage(schoolfId) {
                var defer = $q.defer();
                apiCommon.generalQuery({
                    Proc_name: 'getSchoolGrade',
                    schoolfId: schoolfId
                }).then(function (res) {
                    if(!res.data.msg.length) {
                        $scope.isEmpty = true
                    }
                    defer.resolve(res)
                },function (res) {
                    defer.reject(res);
                });
                return defer.promise;
            }
        }]
});