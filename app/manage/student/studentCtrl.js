/**
 * Created by Administrator on 2017/2/16 0016.
 */
define(['underscore-min', '../../service/assignRouter'], function () {
    return ['$scope', '$q', '$state', 'ngDialog', 'Upload', 'resourceUrl', '$location', '$timeout', 'constantService', 'apiCommon',
        function ($scope, $q, $state, ngDialog, Upload, resourceUrl, $location, $timeout, constantService, apiCommon) {
            var user = angular.fromJson(sessionStorage.getItem('currentUser')),
                state = window.history.state || {},
                historyClassId = state.classId;
            $scope.classFLnkId = historyClassId ? historyClassId : $location.search().classFLnkId;
            $scope.GradeNum = $location.search().GradeNum;
            $scope.showPharse = false;
            $scope.currentUserFId = [];
            getGradeList();
            function getGradeList() {
                apiCommon.generalQuery({
                    Proc_name: 'getSchoolGrade',
                    schoolfId: user.schoolFId
                }).success(function (res) {
                    if(_.isArray(res.msg)){
                        $scope.pharseList =res.msg;
                        _.each($scope.pharseList, function (item) {
                            if (+$scope.GradeNum === item.GradeNum) {
                                $scope.curPharse = item;
                            }
                        });
                        if (!$scope.curPharse || $scope.curPharse === undefined || $scope.curPharse === null) {
                            $scope.curPharse = $scope.pharseList[0];
                        }
                        getClassList();
                    }
                });
            }
            function getClassList() {
                apiCommon.generalQuery({
                    Proc_name: 'GetGradeClasss',
                    SchoolFid: user.schoolFId,
                    GradNum: '0'
                }).then(function (res) {
                    if(_.isArray(res.data.msg)){
                        $scope.classList = res.data.msg;
                        $scope.classList = _.sortBy($scope.classList, 'SmallNum');
                        $scope.classList = _.sortBy($scope.classList, 'GradeNum');
                        $scope.selectGrade($scope.curPharse, 1);
                    }
                });
            }

            $scope.selectGrade = function (Grade, isfirst) {
                $scope.curPharse = Grade;
                $scope.curClassList = [];
                $scope.currentUserFId = [];
                $scope.classIdList = [];
                _.each($scope.classList, function (item) {
                    if (($scope.curPharse.GradeNum || $scope.GradeNum) === item.GradeNum) {
                        $scope.curClassList.push(item);
                        $scope.classIdList.push(item.FLnkID);
                    }
                });
                if ($scope.curClassList.length !== 0) {
                    if (isfirst) {
                        _.each($scope.curClassList, function (item) {
                            if (item.FLnkID === $scope.classFLnkId) {
                                $scope.curClass = item;
                                $scope.curClassId = [];
                                $scope.curClassId.push(item.FLnkID);
                            }
                        });
                        if (!$scope.curClassId) {
                            $scope.curClass = $scope.curClassList[0];
                            $scope.curClassId = [];
                            $scope.curClassId.push($scope.curClassList[0].FLnkID);
                        }
                    } else {
                        $scope.curClass = $scope.curClassList[0];
                        $scope.curClassId = [];
                        $scope.curClassId.push($scope.curClassList[0].FLnkID);
                    }
                }
                $scope.showPharse = false;
            };

            $scope.toggle = function(){
                $scope.showPharse = !$scope.showPharse;
            };

            $scope.selectClass = function (index, Class) {
                if (index === 1) {
                    $scope.curClass = Class;
                    $scope.curClassId = [];
                    $scope.currentUserFId = [];
                    $scope.curClassId.push(Class.FLnkID);
                    var newState = {
                        classId: $scope.curClass ? $scope.curClass.FLnkID : ''
                    };
                    window.history.replaceState(newState, '',null);
                } else {
                    //全部班级
                    //$scope.curClassId = $scope.classIdList;
                }
            };
            $scope.$watch('curClassId', function (newValue, oldValue) {
                if ((newValue === oldValue) || !newValue) {
                    return;
                }
                $scope.filterStu = {};
                $scope.filterStu.name = '';
                var deferList = [];
                _.each(newValue, function (val) {
                    deferList.push(apiCommon.getStudentList({
                        classFLnkId: val,
                        schoolFLnkId: user.schoolFId
                    }));
                });
                $q.all(deferList).then(function (res) {
                    $scope.stuList = [];
                    _.each(res, function (item) {
                        if (item.data && _.isArray(item.data.msg) && item.data.msg.length) {
                            $scope.className = item.data.msg[0].className;
                            _.each(item.data.msg, function (val) {
                                $scope.stuList.push(val);
                            });
                        }
                    });
                    $scope.curStuList = $scope.stuList;
                });
            });
            $scope.$watch('filterStu.name', function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }
                if (newValue) {
                    $scope.curStuList = _.filter($scope.stuList, function (item) {
                        return item.studentName.indexOf(newValue) !== -1;
                    });
                } else {
                    $scope.curStuList = $scope.stuList;
                }
            });
            $scope.allSelect = function () {
                if ($scope.currentUserFId.length === $scope.curStuList.length) {
                    $scope.currentUserFId = [];
                    $scope.allChoose = false;
                } else {
                    $scope.currentUserFId = [];
                    $scope.allChoose = true;
                    _.each($scope.curStuList, function (item) {
                        var cd = $scope.currentUserFId.indexOf(item.studentFLnkId);
                        if (cd === -1) {
                            $scope.currentUserFId.push(item.studentFLnkId);
                        }
                    })
                }
            };
            $scope.selectUser = function (stu) {
                var index = _.indexOf($scope.currentUserFId, stu.studentFLnkId);
                if (index >= 0) {
                    $scope.currentUserFId.splice(index, 1);
                } else {
                    $scope.currentUserFId.push(stu.studentFLnkId);
                }
                if($scope.currentUserFId.length !== $scope.curStuList.length) {
                    $scope.allChoose = false;
                } else {
                    $scope.allChoose = true;
                }
            };
            $scope.editStu = function (index, stu) {
                if (index === 1) {
                    location.href = "#/editStudent?New=" + index + '&studentFLnkId=' + stu.studentFLnkId + '&classFLnkId=' + stu.classFLnkId;
                } else {
                    location.href = "#/editStudent?New=" + index;
                }
            };

            $scope.outCode = function (index, code) {
                if (index === 2) {
                    $scope.studentNo = code.studentFLnkId;
                    window.open("#/examOutCode?allOrSelf=" + '2&ClassFlnkID=' + $scope.curClassId + '&studentNo=' + $scope.studentNo + '&isOpenNewWin=' + 1);
                } else {
                    ngDialog.open({
                        template: 'printCode',
                        className: 'ngdialog-theme-plain',
                        appendClassName: 'printCode-confirm-to-setting',
                        scope: $scope,
                        closeByDocument: false,
                        controller: function ($scope) {
                            return function () {
                                $scope.QrCodeModel = 2;//排列模式：AABBCC
                                $scope.QrCode = 1;      //打印份数
                                $scope.QrcModel = function (QrCodeModel) {
                                    if ($scope.QrCodeModel !== QrCodeModel) {
                                        $scope.QrCodeModel = QrCodeModel;
                                    }
                                };
                                $scope.goQrCode = function () {
                                    ngDialog.close();
                                    if ($scope.QrCodeModel === 1) {
                                        window.open("#/examOutCode?allOrSelf=" + '1&QrCode=' + $scope.QrCode + '&ClassFlnkID=' + $scope.curClassId + '&isOpenNewWin=' + 1);
                                    } else {
                                        window.open("#/examOutCode?allOrSelf=" + '1&QrCode2=' + $scope.QrCode + '&ClassFlnkID=' + $scope.curClassId + '&isOpenNewWin=' + 1);
                                    }
                                };
                            }
                        }
                    });
                }
            };
            $scope.outChatCode = function () {
                window.open("#/wxOutCode?classFLnkId=" + $scope.curClassId + '&isOpenNewWin=' + 1);
            };

            //关闭ngDialog
            $scope.closeDialog = function () {
                ngDialog.close();
            };
            //重置密码
            $scope.reSetPsw = function (stu) {
                var students = [];
                if (stu) {
                    students.push(stu.studentFLnkId);
                } else {
                    if ($scope.currentUserFId.length < 2) {
                        $scope.noStu = true;
                        $timeout(function () {
                            $scope.noStu = false;
                        }, 900);
                        return
                    }
                    students = $scope.currentUserFId;
                }
                constantService.confirm('重置密码','确定将密码重置为123456吗？', [''], function () {
                    var param =[];
                    _.each(students, function (item) {
                        param.push({UserLnkID: item, RoleID: '5'});
                    });
                    apiCommon.resetUserPassword(param).then(function (resp) {
                        if(resp.data.Flag){
                            constantService.alert('新密码为：123456');
                        }
                    });
                },$scope);
            };

            //删除学生
            $scope.delStu = function (stu) {
                var students = [];
                if (stu) {
                    students.push(stu.studentFLnkId);
                } else {
                    if ($scope.currentUserFId.length < 2) {
                        $scope.noStu = true;
                        $timeout(function () {
                            $scope.noStu = false;
                        }, 900);
                        return
                    }
                    students = $scope.currentUserFId;
                }
                constantService.confirm('删除账号','确定删除？', [''], function () {
                    var param =[];
                    _.each(students, function (item) {
                        param.push({UserLnkID: item, RoleID: '5'});
                    });
                    apiCommon.deleteBaseUser(param).then(function (resp) {
                        if(resp.data.Flag){
                            _.each(students, function (student) {
                                var r = _.findIndex($scope.curStuList, {studentFLnkId: student});
                                $scope.curStuList.splice(r, 1);
                            })
                        }
                    });
                },$scope);
            };

            //批量修改资料
            $scope.allReInFro = function () {
                if ($scope.currentUserFId.length < 2) {
                    $scope.noStu = true;
                    $timeout(function () {
                        $scope.noStu = false;
                    }, 900);
                    return
                }
                ngDialog.open({
                    template: 'stuModifyAll',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'printCode-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: false,
                    controller: function ($scope) {
                        return function () {
                            $scope.$watch('curGradeName', function (newval, oldval) {
                                if (newval === oldval || !newval || newval === null) {
                                    return
                                }
                                $scope.curList = [];
                                _.each($scope.classList, function (item) {
                                    if (newval.GradeNum === item.GradeNum && item.FLnkID !== $scope.curClassId[0]) {
                                        $scope.curList.push(item);
                                    }
                                });
                            });
                            $scope.goModify = function () {
                                if (!$scope.curClassName) {
                                    $scope.noClass = true;
                                    $timeout(function () {
                                        $scope.noClass = false;
                                    }, 900);
                                    return
                                }
                                if ($scope.curGradeName && $scope.curClassName) {
                                    apiCommon.updateStudentClass({
                                        StudentLnkID: $scope.currentUserFId,
                                        ClassLnkID: $scope.curClassName.FLnkID,
                                        GradeNum: $scope.curGradeName.GradeNum
                                    }).then(function (resp) {
                                        ngDialog.close();
                                        if(resp.data.Flag){
                                            constantService.alert(resp.data.Message, function () {
                                                _.each($scope.currentUserFId, function (student) {
                                                    var r = _.findIndex($scope.curStuList, {studentFLnkId: student});
                                                    $scope.curStuList.splice(r, 1);
                                                })
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
            };

            $scope.importData = function (name) {
                ngDialog.open({
                    template: 'manage/importData.html',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'importData-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: false,
                    controller: function ($scope) {
                        return function () {
                            $scope.userName = name;
                        }
                    }
                });
            };
            $scope.uploadDataCheck = function (picFile, name) {
                if (!picFile) {
                    constantService.alert('请选择文件！');
                    return;
                } else {
                    if (picFile.name) {
                        var stuFiletype = picFile.name.substr(picFile.name.lastIndexOf(".")).toLowerCase();
                        if (!((stuFiletype === '.xls') || (stuFiletype === '.xlsx'))) {
                            constantService.alert('文件格式不对！');
                            return;
                        }
                    } else {
                        constantService.alert("请命名文件后重新上传！");
                        return;
                    }
                }
                ngDialog.close();
                $scope.userName = name;
                $scope.allowUpLoad = true;
                Upload.upload({
                    url: resourceUrl + '/File/UploadStudentFile',
                    data: {
                        file: picFile,
                        fileKey: '5',
                        roleId: '5',
                        schoolId: user.schoolFId
                    }
                }).then(function (resp) {
                    if(resp.data.Flag){
                        $scope.tableList = resp.data.ResultObj;
                        $scope.EmpIDErrorMessage = '';
                        $scope.SFIDErrorMessage = '';
                        var i, len = $scope.tableList.length;
                        _.each($scope.tableList, function (item, index) {
                            if(item.FailReason){
                                $scope.allowUpLoad = false;
                            }
                            for(i = index + 1; i < len; i++) {
                                if(item.SFID &&item.SFID === $scope.tableList[i].SFID){
                                    item.SFIDError = true;
                                    $scope.tableList[i].SFIDError = true;
                                    $scope.SFIDErrorMessage = '唯一学号重复，';
                                    $scope.allowUpLoad = false;
                                } else if(!item.SFID && item.EmpID === $scope.tableList[i].EmpID){
                                    item.EmpIDError = true;
                                    $scope.tableList[i].EmpIDError = true;
                                    $scope.EmpIDErrorMessage = '校内学号重复，';
                                    $scope.allowUpLoad = false;
                                }
                            }
                        });
                        ngDialog.open({
                            template: 'uploadCheck',
                            className: 'ngdialog-theme-plain',
                            appendClassName: 'uploadCheck-confirm-to-setting',
                            scope: $scope,
                            closeByDocument: false,
                            controller: function ($scope) {
                                return function () {
                                    $scope.uploadData = function () {
                                        apiCommon.sureImportStudentList($scope.tableList).then(function (response) {
                                            ngDialog.close();
                                            constantService.alert(response.data.Message, function () {
                                                getGradeList();
                                            });
                                        }, function () {
                                            ngDialog.close();
                                            constantService.alert('上传异常！');
                                        });
                                    };
                                }
                            }
                        });
                    }else {
                        constantService.alert(resp.data.Message);
                    }
                });
            };
            $scope.goTop = function () {
                $(document).scrollTop(0)
            };
        }]
});