/**
 * Created by Administrator on 2017/2/23 0023.
 */
define(['underscore-min', 'jqprint', 'jqrcode', 'pagination', '../../service/assignRouter'], function () {
    return ['$scope', '$state', 'ngDialog', '$timeout', '$q', '$location', 'apiCommon',
        function ($scope, $state, ngDialog, $timeout, $q, $location, apiCommon) {
            var user = angular.fromJson(sessionStorage.getItem('currentUser'));
            $scope.allOrSelf = $location.search().allOrSelf; //1：班级；2：单个学生
            $scope.QrCode = $location.search().QrCode;    // ABCABC排版模式，打印分数
            $scope.QrCode2 = $location.search().QrCode2;  //AABBCC排版模式，打印分数
            $scope.ClassFlnkID = $location.search().ClassFlnkID;
            $scope.currStudentNo = $location.search().studentNo;
            var wxFull = $location.search().wxFull;    //微信打印，一个班占一页
            var totalNum = 84;
            $scope.s = [];
            $scope.ClassFlnkID = $scope.ClassFlnkID.split(',');
            $scope.qrcodePageConf = {
                currentPage: 1,
                itemsPerPage: totalNum * 20
            };
            var qrcodeCache = {};
            var $container = $('#out-code');
            var $tempContainer = $('#temp-qr');
            var $clone = $('<div class="out-exam-code"> ' +
                '<div class="examCode"></div>' +
                '<div class="detail"><div class="name"></div><div class="classUserNo"></div><div class="schoolmn"></div>' +
                '</div></div>');

            var handGradeName = [
                {gradeName: '一年级', GradNum: 1}, {gradeName: '二年级', GradNum: 2}, {gradeName: '三年级', GradNum: 3},
                {gradeName: '四年级', GradNum: 4}, {gradeName: '五年级', GradNum: 5}, {gradeName: '六年级', GradNum: 6},
                {gradeName: '初一', GradNum: 7}, {gradeName: '初二', GradNum: 8}, {gradeName: '初三', GradNum: 9},
                {gradeName: '高一', GradNum: 10}, {gradeName: '高二', GradNum: 11}, {gradeName: '高三', GradNum: 12}];
            var deferList = [];
            _.each($scope.ClassFlnkID, function (val) {
                deferList.push(apiCommon.getStudentList({
                    classFLnkId: val,
                    schoolFLnkId: user.schoolFId,
                    needWx: wxFull ? 1 : ''
                }));
            });
            $q.all(deferList).then(function (res) {
                if ($scope.allOrSelf === '1') {
                    _.each(res, function (item) {
                        $scope.stuCodes = [];
                        if (item.data && Array.isArray(item.data.msg) && item.data.msg.length) {
                            $scope.className = item.data.msg[0].className;
                            $scope.ss = item.data.msg;
                            for (var i = 0; i < $scope.QrCode || i < $scope.QrCode2; i++) {
                                if(wxFull) {     // 微信打印不足满页的用空的补满
                                    var times = Math.ceil($scope.ss.length / totalNum);
                                    for (var j = 0; j < totalNum * times; j++) {
                                        $scope.stuCodes.push($scope.ss[j] ? $scope.ss[j] : {'isWx' : 1});
                                    }
                                } else {
                                    _.each($scope.ss, function (items) {
                                        $scope.stuCodes.push(items);
                                    })
                                }
                            }
                            if ($scope.QrCode2 !== undefined) {
                                $scope.stuCodes.sort(function (a, b) {
                                    return a.studentNo - b.studentNo;
                                });
                                $scope.stuCodes.sort(function (a, b) {
                                    return a.studentSmallNo - b.studentSmallNo;
                                });
                            }
                        }
                        _.each($scope.stuCodes, function (item) {
                            $scope.s.push(item);
                        });
                    });
                } else {
                    _.each(res[0].data.msg, function (item) {
                        if (item.studentFLnkId === $scope.currStudentNo) {
                            for (var i = 0; i < totalNum; i++) {
                                $scope.s.push(item);
                            }
                        }
                    });
                }
                $scope.qrcodePageConf.totalItems = $scope.s.length;
                var currentPageData = $scope.s.slice(0, $scope.qrcodePageConf.itemsPerPage);
                showQRCode(currentPageData);
            });
            $scope.$watch('qrcodePageConf.currentPage', function (val, oldVal) {
                if (val && val !== oldVal) {
                    var start = $scope.qrcodePageConf.currentPage === 1 ? 0 : ($scope.qrcodePageConf.currentPage - 1) * $scope.qrcodePageConf.itemsPerPage;
                    var end = start + $scope.qrcodePageConf.itemsPerPage;
                    var data = $scope.s.slice(start, end);
                    showQRCode(data);
                }
            });
            function showQRCode(fileList) {
                $container.empty();
                var i = 1, n = 0, $sss;
                $sss = $('<div id="examPrint" class="examPrintUsl"><div class="bottom-page">&nbsp;' + $scope.qrcodePageConf.currentPage + '-' + i + '</div></div>');
                _.each(fileList, function (s, index) {
                    _.each(handGradeName, function (GradeName) {
                        if (+s.gradeName === GradeName.GradNum) {
                            s.gradeNameNew = GradeName.gradeName;
                        }
                    });
                    if (s.classNo && (s.classNo.toString().length === 1)) {
                        s.classNo = '0' + s.classNo;
                    }
                    if (s.studentSmallNo) {
                        if (s.studentSmallNo.toString().length === 1) {
                            s.studentSmallNo = '0' + s.studentSmallNo;
                        }
                    } else {
                        s.studentSmallNo = '00';
                    }
                    var $sc = $clone.clone();
                    var $codeArea = $sc.find('.examCode');
                    var $name = $sc.find('.name');
                    var $classUserNo = $sc.find('.classUserNo');
                    var $schoolmn = $sc.find('.schoolmn');
                    $name.text(s.studentName || '');
                    $schoolmn.text(s.schoolName || '');
                    $classUserNo.text((s.gradeNameNew && s.classNo && s.studentSmallNo) ? (s.gradeNameNew + s.classNo + '班(' + s.studentSmallNo + ')') : '');
                    if(wxFull && !s.isWx) {
                        $('<img>').attr({'src': 'http://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=' + s.img, width:"64", height:"64"}).appendTo($codeArea);
                    }  else if (!wxFull){
                        var qrcodeData = getQrCode('', s.classNo + ',' + s.studentNo);
                        $('<img>').attr('src', qrcodeData).appendTo($codeArea);
                    }
                    if (n < totalNum) {
                        n += 1;
                        $sc.appendTo($sss);
                        if (index === fileList.length - 1) {
                            $sss.appendTo($container);
                        }
                    } else {
                        i = i + 1;
                        n = 1;
                        $sss.appendTo($container);
                        $sss = $('<div id = examPrint' + i + ' class="examPrintUsl"><div class="bottom-page">&nbsp;' + $scope.qrcodePageConf.currentPage + '-' + i + '</div></div>');
                        $sc.appendTo($sss);
                    }
                });
            }

            function getQrCode(node, text) {
                if (qrcodeCache[text]) {
                    return qrcodeCache[text];
                } else {
                    node = $tempContainer.empty().get(0);
                    var qrcode = new QRCode(node, {
                        text: text,
                        width: 64,
                        height: 64,
                        colorDark: '#000000',
                        colorLight: '#ffffff',
                        correctLevel: QRCode.CorrectLevel.L
                    });
                    var $el = $(qrcode._el).find('canvas').get(0);
                    var imgData = $el.toDataURL();
                    qrcodeCache[text] = imgData;
                    qrcode = null;
                    $tempContainer.empty();
                    return imgData;
                }
            }

            $scope.doPrint = function () {
                $('#out-code').jqprint({
                    operaSupport: false,
                    importCss: true,
                    debug: false,
                    printContainer: true,
                    // cssHref: 'schoolManageView/schoolManage.css'
                    // cssHref: 'schoolManageView/outExamCode.css'
                });
            };
        }]
});


