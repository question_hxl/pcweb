/**
 * Created by Administrator on 2017/2/16 0016.
 */
define(['underscore-min', 'jqprint', 'jqrcode', 'pagination'], function () {
    return ['$scope', '$state', 'ngDialog', '$timeout', 'apiCommon',
        function ($scope, $state, ngDialog, $timeout, apiCommon) {
            var user = angular.fromJson(sessionStorage.getItem('currentUser'));
            apiCommon.generalQuery({
                Proc_name: 'getSchoolGrade',
                schoolfId: user.schoolFId
            }).success(function (res) {
                $scope.pharseList = res.msg;
                if(!$scope.pharseList.length) {
                    $scope.isEmpty = true;
                }
            });
            apiCommon.generalQuery({
                Proc_name: 'GetGradeClasss',
                SchoolFid: user.schoolFId,
                GradNum: '0'
            }).then(function (res) {
                $scope.classAllList = res.data.msg;
                _.each($scope.classAllList, function (item) {
                    _.each($scope.pharseList, function (GradeName) {
                        if (+item.GradeNum === GradeName.GradeNum) {
                            item.gradeName = GradeName.gradeName;
                        }
                    })
                });
                //过滤垃圾班级
                $scope.classAllList = _.filter($scope.classAllList, function (item) {
                    return item.SmallNum !== null;
                });
                $scope.classAllList = _.sortBy($scope.classAllList, 'SmallNum');
                $scope.classAllList = _.sortBy($scope.classAllList, 'GradeNum');
            });

            $scope.currentPharse = [];
            $scope.currentclassFLnkID = [];
            $scope.QrCodeModel = 2;
            $scope.QrCode = 1;

            $scope.QrcModel = function (QrCodeModel) {
                if ($scope.QrCodeModel !== QrCodeModel) {
                    $scope.QrCodeModel = QrCodeModel;
                    curList();
                }
            };
            $scope.curGraded = function (item) {
                var index = _.indexOf($scope.currentPharse, item.gradeName);
                if (index >= 0) {
                    $scope.currentPharse.splice(index, 1);
                } else {
                    $scope.currentPharse.push(item.gradeName);
                }
                apiCommon.generalQuery({
                    Proc_name: 'GetGradeClasss',
                    SchoolFid: user.schoolFId,
                    GradNum: item.GradeNum
                }).then(function (res) {
                    if (_.isArray(res.data.msg) && res.data.msg[0]) {
                        var curGrade = res.data.msg;
                        curGrade = _.sortBy(curGrade, 'SmallNum');
                        curGrade = _.sortBy(curGrade, 'GradeNum');
                        if (index >= 0) {
                            _.each(curGrade, function (ret) {
                                var i = _.indexOf($scope.currentclassFLnkID, ret.FLnkID);
                                if (i >= 0)
                                    $scope.currentclassFLnkID.splice(i, 1);
                            });
                        } else {
                            _.each(curGrade, function (ret) {
                                if (_.indexOf($scope.currentclassFLnkID, ret.FLnkID) === -1) {
                                    $scope.currentclassFLnkID.push(ret.FLnkID);
                                }
                            });
                        }
                    }
                });
            };
            $scope.curClassed = function (item) {
                var index = _.indexOf($scope.currentclassFLnkID, item.FLnkID);
                if (index >= 0) {
                    $scope.currentclassFLnkID.splice(index, 1);
                } else {
                    $scope.currentclassFLnkID.push(item.FLnkID);
                }
            };
            $scope.qrCodeNum = function (num) {
                $scope.QrCode = num;
            };
            $scope.$watch('QrCode', function (newval, oldval) {
                curList();
            });

            function curList() {
                $scope.stuCodes = [];
                $('preview').empty();
                $scope.ss = [
                    {studentName: '1班A',studentNo: '1',classNo: 1},
                    {studentName: '1班B',studentNo: '2',classNo: 1},
                    {studentName: '1班C',studentNo: '3',classNo: 1},
                    {studentName: '1班D',studentNo: '4',classNo: 1},
                    {studentName: '1班E',studentNo: '5',classNo: 1},
                    {studentName: '1班F',studentNo: '6',classNo: 1},
                    {studentName: '1班G',studentNo: '7',classNo: 1},
                    {studentName: '1班H',studentNo: '8',classNo: 1},
                    {studentName: '1班I',studentNo: '9',classNo: 1},
                    {studentName: '1班J',studentNo: '10',classNo: 1},
                    {studentName: '1班K',studentNo: '11',classNo: 1},
                    {studentName: '1班L',studentNo: '12',classNo: 1},
                    {studentName: '1班M',studentNo: '13',classNo: 1},
                    {studentName: '1班N',studentNo: '14',classNo: 1},
                    {studentName: '2班A',studentNo: '1',classNo: 2},
                    {studentName: '2班B',studentNo: '2',classNo: 2},
                    {studentName: '2班C',studentNo: '3',classNo: 2},
                    {studentName: '2班D',studentNo: '4',classNo: 2},
                    {studentName: '2班E',studentNo: '5',classNo: 2},
                    {studentName: '2班F',studentNo: '6',classNo: 2},
                    {studentName: '2班G',studentNo: '7',classNo: 2},
                    {studentName: '2班H',studentNo: '8',classNo: 2},
                    {studentName: '2班I',studentNo: '9',classNo: 2},
                    {studentName: '2班J',studentNo: '10',classNo: 2},
                    {studentName: '2班K',studentNo: '11',classNo: 2},
                    {studentName: '2班L',studentNo: '12',classNo: 2},
                    {studentName: '2班M',studentNo: '13',classNo: 2},
                    {studentName: '2班N',studentNo: '14',classNo: 2}
                ];
                for (var i = 0; i < $scope.QrCode; i++) {
                    _.each($scope.ss, function (item) {
                        $scope.stuCodes.push(item);
                    })
                }
                if ($scope.QrCodeModel === 2) {
                    $scope.stuCodes.sort(function (a, b) {
                        return +a.studentNo - +b.studentNo;
                    });
                    $scope.stuCodes = _.sortBy($scope.stuCodes, 'classNo');
                }
                if ($scope.stuCodes.length > 84) {
                    $scope.stuCodes = $scope.stuCodes.slice(0, 84);
                }
            }

            $scope.goQrCode = function () {
                if ($scope.currentclassFLnkID.length !== 0) {
                    if ($scope.QrCodeModel === 1) {
                        window.open("#/examOutCode?allOrSelf=" + '1&QrCode=' + $scope.QrCode + '&SchoolFid=' + user.schoolFId + '&ClassFlnkID=' + $scope.currentclassFLnkID + '&isOpenNewWin=' + 1);
                    } else {
                        window.open("#/examOutCode?allOrSelf=" + '1&QrCode2=' + $scope.QrCode + '&SchoolFid=' + user.schoolFId + '&ClassFlnkID=' + $scope.currentclassFLnkID + '&isOpenNewWin=' + 1);
                    }
                } else {
                    $scope.noClass = true;
                    $timeout(function () {
                        $scope.noClass = false;
                    }, 900);
                }
            };

        }]
});


