/**
 * Created by Administrator on 2017/2/16 0016.
 */
define(['underscore-min', '../../service/assignRouter'], function () {
    return ['$scope', '$location', '$timeout', 'Upload', 'ngDialog', 'constantService', 'resourceUrl', 'apiCommon',
        function ($scope, $location, $timeout, Upload, ngDialog, constantService, resourceUrl, apiCommon) {
            var user = angular.fromJson(sessionStorage.getItem('currentUser')),
                state = window.history.state || {},
                historyGradNo = state.GradNo,
                historyClassName = state.ClassName,
                historySubjectName = state.SubjectName,
                historyUserName = state.UserName,
                historyRoleId = state.RoleId;
            $scope.classFLnkId = $location.search().classFLnkId;
            $scope.currentUserFId = [];
            $scope.curSubject = '';
            $scope.filterTeacher = historyUserName || '';
            //角色列表
            apiCommon.getBaseRoleList().then(function (resp) {
                $scope.roleList = resp.data.ResultObj;
                $scope.roleList = _.filter($scope.roleList, function (item) {
                   return  item.RoleID !== 5;
                });
                if($scope.classFLnkId) {
                    $scope.curRole = $scope.roleList[3];
                }
                if(historyRoleId) {
                    var id = _.findIndex($scope.roleList, {RoleID: +historyRoleId});
                    $scope.curRole = $scope.roleList[id];
                }
                gradeList();
                subjectList();
            });
            //年级列表
            function gradeList() {
                apiCommon.generalQuery({
                    Proc_name: 'getSchoolGrade',
                    schoolfId: user.schoolFId
                }).success(function (res) {
                    $scope.phaseList =res.msg;
                    $scope.phaseList.unshift({
                        GradeNum: 0,
                        gradeName: "全部"
                    });
                    $scope.curPhase = $scope.phaseList[0];
                    if(historyGradNo || +historyGradNo === 0) {
                        var id = _.findIndex($scope.phaseList, {GradeNum: +historyGradNo});
                        $scope.curPhase = $scope.phaseList[id];
                    }
                });
            }

            //获取学科列表
            function subjectList() {
                apiCommon.getSubjectList({pharseId: user.pharseId}).then(function (res) {
                    if (_.isArray(res.data.msg)) {
                        $scope.subjectList = res.data.msg;
                        $scope.subjectList.unshift({
                            subjectFLnkId: "",
                            subjectId: "",
                            subjectName: "全部"
                        });
                        $scope.curSubject = $scope.subjectList[0];
                        if(historySubjectName) {
                            var id = _.findIndex($scope.subjectList, {subjectName: historySubjectName});
                            $scope.curSubject = $scope.subjectList[id];
                        }
                    }
                });
            }

            //获取班级
            function getClass(GradNum, index) {
                apiCommon.generalQuery({
                    Proc_name: 'GetGradeClasss',
                    SchoolFid: user.schoolFId,
                    GradNum: GradNum
                }).then(function (res) {
                    $scope.classList = res.data.msg;
                    $scope.classList = _.sortBy($scope.classList, 'SmallNum');
                    $scope.classList = _.sortBy($scope.classList, 'GradeNum');
                    $scope.classList.unshift({
                        ClassID: "",
                        ClassName: "全部",
                        CreateByUserID: "",
                        FLnkID: "",
                        GradeNum: "",
                        Period: "",
                        SchoolFlnkID: ""
                    });
                    $scope.curClassList = $scope.classList;
                    if($scope.classFLnkId && index) {
                        _.each($scope.curClassList, function (item) {
                            if (item.FLnkID === $scope.classFLnkId) {
                                $scope.curKLass = item;
                            }
                        });
                        $scope.getTeacherList();
                    }else {
                        $scope.curKLass = $scope.curClassList[0];
                        if(historyClassName && GradNum === +historyGradNo) {
                            var id = _.findIndex($scope.curClassList, {ClassName: historyClassName});
                            $scope.curKLass = $scope.curClassList[id];
                            $scope.getTeacherList();
                        }
                    }
                });
            }
            //获取老师列表
            $scope.getTeacherList = function(index) {
                if(!$scope.curRole) {
                    constantService.alert('请选择角色');
                    return
                }
                var param = {
                    SchoolFLnkId: user.schoolFId,
                    GradNo: $scope.curPhase.GradeNum || '0',
                    RoleId: $scope.curRole.RoleID,
                    UserName: $scope.filterTeacher,
                    Page: {
                        PageIndex: 1,
                        PageSize: 2000
                    }
                };
                if(+$scope.curRole.RoleID === 2){
                    param.GradNo = $scope.curPhase.GradeNum;
                    param.SubjectName = $scope.curSubject.subjectName === '全部' ? '': $scope.curSubject.subjectName;
                }else if(+$scope.curRole.RoleID === 3){
                    param.GradNo = $scope.curPhase.GradeNum;
                    param.ClassName = $scope.curKLass.ClassName === '全部' ? '': $scope.curKLass.ClassName;
                }else if(+$scope.curRole.RoleID === 4){
                    param.GradNo = $scope.curPhase.GradeNum;
                    param.ClassName = $scope.curKLass.ClassName === '全部' ? '': $scope.curKLass.ClassName;
                    param.SubjectName = $scope.curSubject.subjectName === '全部' ? '': $scope.curSubject.subjectName;
                } else {
                    param.GradNo = '0';
                }
                getUserList(param);

            };
            function getUserList(param) {
                var defer;
                if($scope.curRole.RoleID !== 4) {
                    defer = apiCommon.getMasterListPageView(param);
                }else {
                    if(param.ClassName) {
                        param.ClassName += ','
                    }
                    if(param.SubjectName) {
                        param.SubjectName += ','
                    }
                    defer = apiCommon.getUserListPageView(param);
                }
                defer.then(function (res) {
                    $scope.allChoose = false;
                    $scope.allTeacherList = res.data.ResultObj.DataSource;
                    $scope.teacherList = [];
                    _.each($scope.allTeacherList, function (item) {
                        _.each($scope.phaseList, function (grade) {
                            if(+item.GradNo === +grade.GradeNum){
                                item.gradeName = grade.gradeName
                            }
                        });
                        $scope.teacherList[$scope.teacherList.length] = item;
                    });
                    historyClassName = '';
                    var newState = {
                        GradNo: param.GradNo,
                        ClassName: $scope.curKLass ? $scope.curKLass.ClassName : '',
                        SubjectName: $scope.curSubject.subjectName,
                        UserName: param.UserName,
                        RoleId: param.RoleId
                    };
                    window.history.replaceState(newState, '',null);
                });
            }
            $scope.$watch('curPhase', function (newValue, oldValue) {
                if ((newValue === oldValue) || !newValue) {
                    return;
                }
                var index = oldValue ? 0 : 1;
                if(!$scope.curRole || ($scope.curRole&& +$scope.curRole.RoleID !== 2) || !oldValue) {
                    getClass($scope.curPhase.GradeNum, index);
                }
            });
            $scope.selectUser = function (teacher) {
                var index = _.indexOf($scope.currentUserFId, teacher.FLnkID);
                if (index >= 0) {
                    $scope.currentUserFId.splice(index, 1);
                } else {
                    $scope.currentUserFId.push(teacher.FLnkID);
                }
                if($scope.currentUserFId.length !== $scope.teacherList.length) {
                    $scope.allChoose = false;
                } else {
                    $scope.allChoose = true;
                }
            };
            $scope.allSelect = function () {
                if(typeof($scope.teacherList) === "undefined") {
                    return
                }else {
                    if($scope.teacherList.length === 0) {
                        return
                    }
                }
                if ($scope.currentUserFId.length === $scope.teacherList.length) {
                    $scope.allChoose = false;
                    $scope.currentUserFId = [];
                } else {
                    $scope.currentUserFId = [];
                    $scope.allChoose = true;
                    _.each($scope.teacherList, function (item) {
                        // var cd = $scope.currentUserFId.indexOf(item.FLnkID);
                        // if(cd === -1){
                        //     $scope.currentUserFId.push(item.FLnkID);
                        // }
                        $scope.currentUserFId.push(item.FLnkID);
                    })
                }
            };

            $scope.editTeacher = function (index, teacher) {
                if (index === 1) {
                    location.href = "#/editTeacher?New=" + index + '&userFId=' + teacher.FLnkID;
                } else if (index === 2){
                    location.href = "#/editTeacher?New=" + index;
                }
                // else {
                //     if ($scope.currentUserFId.length < 2) {
                //         $scope.noTea = true;
                //         $timeout(function () {
                //             $scope.noTea = false;
                //         }, 900);
                //         return
                //     }
                //     location.href = "#/editTeacher?New=" + index + '&userFId=' + $scope.currentUserFId;
                // }
            };

            $scope.closeDialog = function () {
                ngDialog.close();
            };
            $scope.delTeacher = function (teacher) {
                var teachers = [];
                if (teacher) {
                    teachers.push(teacher.FLnkID);
                } else {
                    if ($scope.currentUserFId.length < 2) {
                        $scope.noTea = true;
                        $timeout(function () {
                            $scope.noTea = false;
                        }, 900);
                        return
                    }
                    teachers = $scope.currentUserFId;
                }
                constantService.confirm('删除账号','确定删除？', [''],function () {
                    var param =[];
                    _.each(teachers, function (item) {
                        param.push({UserLnkID: item, RoleID: '4'});
                    });
                    apiCommon.deleteBaseUser(param).then(function (resp) {
                        if(resp.data.Flag){
                            _.each(teachers, function (teacher) {
                                var r = _.findIndex($scope.teacherList, {FLnkID: teacher});
                                $scope.teacherList.splice(r, 1);
                            })
                        }
                    });
                },$scope);
            };

            $scope.reSetTeaPsw = function (teacher) {
                var teachers = [];
                if (teacher) {
                    teachers.push(teacher.FLnkID);
                } else {
                    if ($scope.currentUserFId.length < 2) {
                        $scope.noTea = true;
                        $timeout(function () {
                            $scope.noTea = false;
                        }, 900);
                        return
                    }
                    teachers = $scope.currentUserFId;
                }
                constantService.confirm('重置密码','确定将密码重置为123456吗？', [''],function () {
                    var param =[];
                    _.each(teachers, function (item) {
                        param.push({UserLnkID: item, RoleID: '4'});
                    });
                    apiCommon.resetUserPassword(param).then(function (resp) {
                        if(resp.data.Flag){
                            constantService.alert('新密码为：123456');
                        }
                    });
                },$scope);

            };
            $scope.importData = function (name) {
                ngDialog.open({
                    template: 'manage/importData.html',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'importData-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: false,
                    controller: function ($scope) {
                        return function () {
                            $scope.userName = name;
                        }
                    }
                });
            };
            $scope.uploadDataCheck = function (picFile, name) {
                if (!picFile) {
                    constantService.alert('请选择文件！');
                    return;
                } else {
                    if (picFile.name) {
                        var stuFiletype = picFile.name.substr(picFile.name.lastIndexOf(".")).toLowerCase();
                        if (!((stuFiletype === '.xls') || (stuFiletype === '.xlsx'))) {
                            constantService.alert('文件格式不对！');
                            return;
                        }
                    } else {
                        constantService.alert("请命名文件后重新上传！");
                        return;
                    }
                }
                ngDialog.close();
                $scope.userName = name;
                $scope.allowUpLoad = true;
                Upload.upload({
                    url: resourceUrl + '/File/UploadTeacherFile',
                    data: {
                        file: picFile,
                        fileKey: '4',
                        roleId: '4',
                        schoolId: user.schoolFId
                    }
                }).then(function (resp) {
                    if(resp.data.Flag){
                        $scope.tableList = resp.data.ResultObj;
                        _.each($scope.tableList, function (item) {
                            if(item.FailReason){
                                $scope.allowUpLoad = false;
                            }
                        });
                        ngDialog.open({
                            template: 'uploadCheck',
                            className: 'ngdialog-theme-plain',
                            appendClassName: 'uploadCheck-confirm-to-setting',
                            scope: $scope,
                            closeByDocument: false,
                            controller: function ($scope) {
                                return function () {
                                    $scope.uploadData = function () {
                                        apiCommon.sureImportTeacherList($scope.tableList).then(function (response) {
                                            ngDialog.close();
                                            constantService.alert(response.data.Message);
                                        }, function () {
                                            ngDialog.close();
                                            constantService.alert('上传异常！');
                                        });
                                    };
                                }
                            }
                        });
                    }else {
                        constantService.alert(resp.data.Message);
                    }
                });
            };
            $scope.goTop = function () {
                $(document).scrollTop(0)
            };
        }]
});