define(['underscore-min'], function() {
	// controller
    return ['$http', '$scope', '$rootScope', '$state', '$sce', 'ngDialog', 'constantService', function ($http, $scope, $rootScope, $state, $sce, ngDialog, constantService) {
		
		// 获取班级
		$http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function(data) {
			$scope.ClassList = data.msg;
			$scope.CurrentClassID = data.msg[0].classId;
			$scope.classBox = data.msg[0].classId
		});
		// 年级ID
		/*$http.get($rootScope.baseUrl + '/Interface0051A.ashx').success(function(data) {
			$scope.gradeList = data.course;
			$scope.gradeBox = data.course[0].gradeId
		});*/
	
		// 组卷
		$scope.sendMsg = function(){
			   
				var selectedQuestionList = _.filter($scope.pageList, function (page) {
				   return page.isSelected;
				});
				if (selectedQuestionList.length === 0) {
				    constantService.alert('请选择一些题目进行组卷');
						return false;
				}
				var ExamFlnkIDS ='';
			   
				for(var j = 0 ; j < selectedQuestionList.length; j++ ){
					ExamFlnkIDS +=  selectedQuestionList[j].ExamFlnkID + ',' ; 
				}
				ExamFlnkIDS = ExamFlnkIDS.substr(0, ExamFlnkIDS.length - 1);
				$http.post($rootScope.baseUrl + '/Interface0184B.ashx', {
						ExamFlnkID: ExamFlnkIDS,
						ClassFLnkID: $scope.classBox,
					   
				}).success(function(res){
					 var datemsg = res.msg;
					 datemsg = JSON.stringify(datemsg);
					 window.localStorage.setItem('answerList',datemsg)  // 存储题目
					 window.location.href='#/paperComposing?action=1&gradeid='+ $scope.gradeId
				})
		}
		// 获取年级
		/* $http.get($rootScope.baseUrl + '/Interface0051A.ashx').success(function (res) {
			$scope.gradeList = res.course; 
			$scope.Currentgrade = res.course[0].gradeId ; 
		}) 
		 */
		$scope.goback = function(){
			$state.go('myApp.questionIndex')
		}
	   
		$scope.$watch('classBox', function() {
		 if($scope.CurrentClassID!== undefined){
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0183.ashx',
				data: {
					ClassFLnkID: $scope.classBox,
					
				}
			}).success(function(res) {
				$scope.pageList = res.msg; 
				$scope.gradeId =  res.GradeId
			})
		  }
			
		})
	}];
});