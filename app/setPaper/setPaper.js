/**
 * Created by clc on 2017/2/28.
 */
define(['jquery.datetimepicker', 'underscore-min', 'jquerymedia', 'ckeditor', 'dateFomart', '../service/assignRouter'], function () {
    return ['$scope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', 'constantService', 'bes', 'apiCommon',
        function ($scope, $state, $location, $q, ngDialog, $timeout, $sce, constantService, bes, apiCommon) {
            var search = $location.search();
            var unifiedId = search.unifiedId;
            var action = search.action;
            var isParent = search.isParent;
            $scope.submitNum = search.submitNum;
            var user = JSON.parse(sessionStorage.getItem('currentUser'));
            $scope.schoolName = user.schoolName;
            $scope.action = 'ADD_PARENT';
            $scope.isAllTeacher = false;
            var ExamNameChangeAuto = true
                ,ExamNameChangeBefore; // 试卷名是否要自动改变
            if (unifiedId) {
                if (action === 'ADD') {
                    $scope.action = 'ADD_SUB';
                } else {
                    $scope.action = 'MODIFY';
                }
            } else {
                $scope.action = 'ADD_PARENT';
            }
            $scope.currentPageDesc = '新建考试';
            $scope.markModes = [{
                id: 1,
                name: '线上批阅'
            }, {
                id: 2,
                name: '线下誊分'
            }];
            $scope.data = {
                //默认难度3
                diff: 3,
                examDate: getTodayDate(),
                markMode: 2
            };
            $scope.curDay =  $scope.data.examDate ;
            //返回
            $scope.back = function () {
                $state.go('myApp.examCenter');
            };
            if ($scope.action === 'ADD_SUB') {
                //如果是创建子统考，则先获取父统考信息，部分信息不予修改
                $scope.queueList = [
                    {name: '首页',url:'myApp.masterHome'},
                    {name: '考试中心',url:'myApp.examCenter'},
                    {name: '添加考试',url:'myApp.setPaper'}];
                $scope.currentPageDesc = '添加考试';
                initExamTime();
                getUnifiedInfo().then(function(res){
                    if(res.data.code === 0) {
                        var meta = res.data.msg[0];
                        if(meta) {
                            var existSubjects = _.pluck(meta.unitTasks, 'SubjectId');
                            $scope.data = {
                                examName: meta.unifiedName,
                                examDate: meta.unifiedDate.slice(0, 10),
                                diff: meta.pDiff || 3,
                                markMode: 2,
                                disabledSubjects: existSubjects,
                                gradeNum: meta.GradeNum
                            };
                            getExamTypes(meta.ExamType);
                            getGradeList(meta);
                        }
                    }
                });
            }else if($scope.action === 'MODIFY'){
                var parenUnifiedId = search.parenUnifiedId;
                //如果是修改考试信息，则先获取考试信息，初始化页面
                $scope.queueList = [
                    {name: '首页',url:'myApp.masterHome'},
                    {name: '考试中心',url:'myApp.examCenter'},
                    {name: '编辑考试',url:'myApp.setPaper'}];
                $scope.currentPageDesc = '编辑考试';
                initExamTime();
				apiCommon.generalQuery({
					Proc_name: 'Proc_getUnifiedItemManager',
					unifiedItemFid: unifiedId
				}).then(function(res){
				    if(res.data.code === 0) {
						$scope.unifiedAdmins = res.data.msg;
                    }
					getUnifiedInfo().then(function (res) {
						//返回的结果是父统考包含该子统考，需要前端过滤
						if(res.data.code === 0) {
							if(res.data.msg[0]) {
								$scope.taskMeta = [];
								var taskMeta = _.find(res.data.msg[0].unitTasks, function(task){
									return task.FLnkId === unifiedId;
								});
								$scope.taskMeta = taskMeta;
								if(taskMeta) {
									$scope.data.examName = taskMeta.Name;
                                    ExamNameChangeBefore = taskMeta.Name;
									$scope.data.examDate = taskMeta.ExamTime.slice(0, 10);
									getExamTypes(taskMeta.ExamType);
									$scope.data.diff = taskMeta.Diff;
									$scope.data.markMode = taskMeta.MarkMode;
									$scope.data.gradeNum = taskMeta.GradeNum;
									$scope.currentSubjectId = taskMeta.SubjectId;
									$scope.subjectName = taskMeta.subjectName;
									getAdminList(taskMeta.GradeNum, taskMeta.SubjectId);
									getGradeList(taskMeta);
								}
							}
						}
					});
                })
            }else {
                //否则，
                $scope.queueList = [
                    {name: '首页',url:'myApp.masterHome'},
                    {name: '考试中心',url:'myApp.examCenter'},
                    {name: '新建考试',url:'myApp.setPaper'}];
                $scope.currentPageDesc = '新建考试';
                getExamTypes();
                initExamTime();
                getGradeList();
            }

            function getTodayDate() {
                var now = new Date();
                var year = now.getFullYear(),
                    month = now.getMonth() + 1,
                    day = now.getDate();
                month = month < 10 ? ('0' + month) : month;
                day = day < 10 ? ('0' + day) : day;
                return year + '-' + month + '-' + day;
            }

            //初始化时间选择器插件
            function initExamTime() {
                $('#datetimepicker').datetimepicker({
                    lang: "ch",
                    timepicker: false,
                    format: "Y-m-d",
                    todayButton: true,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) {
                        $('#datetimepicker').datetimepicker('hide');
                    }
                });
            }

            $scope.changeDate = function (date) {
                var currentToday = getTodayDate();
                if(Date.parse(date) < Date.parse(currentToday)){
                    constantService.alert('考试时间不能小于当天!');
                    $scope.data.examDate = currentToday;
                }
            };
            //初始化试卷类型
            function getExamTypes(type) {
                return apiCommon.examType().then(function (res) {
                    if (_.isArray(res.data.msg)) {
                        $scope.examTypeList = res.data.msg;
                        if (type) {
                            $scope.data.examType = _.find($scope.examTypeList, function (res) {
                                return res.ExType === type;
                            });
                        } else {
                            $scope.data.examType = $scope.examTypeList[4];
                        }
                    } else {
                        $scope.examTypeList = [];
                        $scope.data.examType = null;
                    }
                });
            }

            $scope.currentGradeNum = [];
            $scope.currentGradeName = [];
            //根据学段初始化年级
            function getGradeList(gradeNum) {
                apiCommon.generalQuery({
                    Proc_name: 'getSchoolGrade',
                    schoolfId: user.schoolFId
                }).then(function (res) {
                    if (res.data.code === 0) {
                        res.data.msg = filterGradeId(res.data.msg);
                        $scope.gradeList = res.data.msg;
                        if($scope.action === 'ADD_PARENT'){
                            $scope.currentGradeNum = res.data.msg[0].GradeNum;
                            $scope.currentGradeName = res.data.msg[0].gradeName;
                        }else if($scope.action === 'ADD_SUB'){
                            $scope.currentGradeNum = gradeNum.GradeNum;
                            $scope.currentGradeName = gradeNum.gradeName;
                        }else if($scope.action === 'MODIFY'){
                            $scope.currentGradeNum = gradeNum.GradeNum;
                            $scope.currentGradeName = gradeNum.gradeName;
                        }
                        apiCommon.getSubjectNameByGradeNum(+$scope.currentGradeNum).success(function (res) {
                            res.ResultObj = filterSubject(res.ResultObj);
                            $scope.subjectList = res.ResultObj;
                            if($scope.subjectList.length === 1) {
                                $scope.subjectList[0].isSelected = true
                            }
							apiCommon.getUnifiedAllowManager({
								schoolId: user.schoolFId,
								grade: gradeNum ? gradeNum.GradeNum : $scope.currentGradeNum
							}).then(function(adminRes){
							    _.each($scope.subjectList, function(sbj){
							        sbj.admins = _.filter(adminRes.data.msg, function(item){
							            return item.SubjectId === sbj.subjectid;
                                    });
                                });
                            })
                        })
                    } else {
                        $scope.gradeList = [];
                    }
                });
            }

            //根据年级初始化学科
            $scope.getSubject = function (GradeNum) {
                $scope.currentGradeNum = GradeNum;
                apiCommon.getSubjectNameByGradeNum(+GradeNum).success(function (res) {
                    $scope.subjectList = filterSubject(res.ResultObj);
                })
            };

            function getAdminList(gradeNum, subjectId) {
                $scope.isAllTeacher = false;
                apiCommon.getUnifiedAllowManager({
                    schoolId: user.schoolFId,
                    grade: gradeNum,
                    subjectId: subjectId
                }).then(function(res){
                    if(res.data.code === 0) {
                        $scope.teacherList = _.map(res.data.msg, function(item){
                            return _.pick(item, 'FLnkID', 'UserName', 'SubjectId');
                        });
                        if($scope.unifiedAdmins) {
                            _.each($scope.teacherList, function(item){
                                var isSelected = _.find($scope.unifiedAdmins, function(adminItem){
                                    return adminItem.UserFlnkId === item.FLnkID;
                                });
                                if(!!isSelected) {
                                    item.isSelected = true;
                                }
                            });
                        }
                        if($scope.action === 'ADD_PARENT') {
							var chosenSubject = _.find($scope.subjectList, function(sbj) {
								return !!sbj.isSelected;
							});
							if(chosenSubject) {
								$scope.currentSbjTeacherList = _.filter($scope.teacherList, function(teacher){
									return +teacher.SubjectId === +chosenSubject.subjectid;
								});
								$scope.otherTeacherList = _.filter($scope.teacherList, function(teacher){
									return +teacher.SubjectId !== +chosenSubject.subjectid;
								});
                            }else {
								$scope.currentSbjTeacherList = [];
								$scope.otherTeacherList = [];
                            }
                        }else if($scope.action === 'ADD_SUB'){
                            if($scope.data.chosedSubject) {
								$scope.currentSbjTeacherList = _.filter($scope.teacherList, function(teacher){
									return +teacher.SubjectId === +$scope.data.chosedSubject;
								});
								$scope.otherTeacherList = _.filter($scope.teacherList, function(teacher){
									return +teacher.SubjectId !== +$scope.data.chosedSubject;
								});
                            }else {
								$scope.currentSbjTeacherList = [];
								$scope.otherTeacherList = [];
                            }
                        }else {
                            if($scope.currentSubjectId) {
								$scope.currentSbjTeacherList = _.filter($scope.teacherList, function(teacher){
									return +teacher.SubjectId === +$scope.currentSubjectId;
								});
								$scope.otherTeacherList = _.filter($scope.teacherList, function(teacher){
									return +teacher.SubjectId !== +$scope.currentSubjectId;
								});
                            }else {
								$scope.currentSbjTeacherList = [];
								$scope.otherTeacherList = [];
                            }
                        }

                    }else {
                        $scope.teacherList = [];
                    }
                });
            }

            $scope.subjectName = [];
            if($scope.action === 'ADD_PARENT'){
                $scope.$watch('subjectList',function () {
                    var selectedSubjects = _.filter($scope.subjectList, function (sbj) {
                        return !!sbj.isSelected;
                    });
                    if(selectedSubjects.length === 1) {
                        $scope.subjectName = selectedSubjects[0].subjectName;
                    }else{
                        $scope.subjectName = [];
                    }
                    if(ExamNameChangeAuto) {
                        getExamName();
                    }
                }, true);
            }else if($scope.action === 'ADD_SUB'){
                $scope.$watch('data.chosedSubject',function () {
                    _.each($scope.subjectList,function (item) {
                        if(item.subjectid == $scope.data.chosedSubject){
                            $scope.subjectName = item.subjectName;
                        }
                    });
                    if(ExamNameChangeAuto) {
                        getExamName();
                    }
                },true);
            }

            if($scope.action != 'MODIFY'){
                $scope.$watch('currentGradeNum',function () {
                    _.each($scope.gradeList,function (item) {
                        if(item.GradeNum === $scope.currentGradeNum){
                            $scope.currentGradeName = item.gradeName;
                        }
                    });
                    if(ExamNameChangeAuto) {
                        getExamName();
                    }
                });
                $scope.$watch('data.examType',function () {
                    if(!!$scope.data.examType && ExamNameChangeAuto){
                        getExamName();
                    }
                }, true);
            }

            function getExamName() {
                if(!!$scope.currentGradeName && !!$scope.subjectName &&!!$scope.data.examType && !!$scope.data.examType.ExType){
                    $scope.data.examName = $scope.schoolName + $scope.currentGradeName +$scope.subjectName+$scope.data.examType.ExType;
                    ExamNameChangeBefore = $scope.data.examName;
                }
            }
            
            $scope.checkAutoChange = function (value) {
                if(value === ExamNameChangeBefore) {
                    return
                }else {
                    ExamNameChangeBefore = value;
                    ExamNameChangeAuto = false;
                }
            };
            //获取统考信息
            function getUnifiedInfo() {
                return bes.send('/BootStrap/schoolmanager/getMyUnifieds.ashx', {
                    targetId: unifiedId,
                    isParent: isParent
                }, {
                    method: 'post'
                });
            }

            $scope.updateAdminList = function(){
                if($scope.action === 'ADD_PARENT') {
                    if($scope.isMultiSubject()) {
                        return;
                    }else {
                        var selectedSubjects = _.filter($scope.subjectList, function (sbj) {
                            return !!sbj.isSelected;
                        });
                        if(selectedSubjects.length === 1) {
                            if($scope.currentGradeNum) {
                                getAdminList($scope.currentGradeNum, selectedSubjects[0].subjectid);
                            }
                        }else{
                            $scope.teacherList = [];
                        }
                    }
                }else if($scope.action === 'ADD_SUB'){
                    if($scope.currentGradeNum && $scope.data.chosedSubject) {
                        getAdminList($scope.currentGradeNum, $scope.data.chosedSubject);
                    }
                }
            };
            $scope.radioCheck = function (subjectId) {
                $scope.checkSubject = subjectId;
                if($scope.currentGradeNum && $scope.checkSubject) {
                    getAdminList($scope.currentGradeNum, $scope.checkSubject);
                }
            };
            $scope.isSubjectDisable = function(subjectId){
                return !!_.find($scope.data.disabledSubjects, function(item){
                    return +item === +subjectId;
                });
            };

            $scope.isMultiSubject = function(){
                var selectedSubjects = _.filter($scope.subjectList, function (sbj) {
                    return !!sbj.isSelected;
                });
                return selectedSubjects.length > 1;
            };

            $scope.setEditAdminSbj = function(sbj){
                $scope.toSetAdminSubject = sbj.subjectid;
            };

            $scope.initAdminStatus = function(sbj){
                if(!$scope.toSetAdminSubject) {
					var selected = _.find($scope.subjectList, function (item) {
                        return !!item.isSelected;
					});
					$scope.toSetAdminSubject = selected && selected.subjectid;
                }
            };

            function getSelectedAdmins(){
                var admins = _.filter($scope.teacherList, function(t){
                    return !!t.isSelected;
                });
                return _.pluck(admins, 'FLnkID').join(',');
            }
            //保存修改
            $scope.savePaper = function () {
                if (!$scope.data.examName || !$.trim($scope.data.examName)) {
                    constantService.alert('请输入考试名称！');
                    return;
                }
                var date = $('#datetimepicker').val();
                if(date === ''){
                    date = getTodayDate();
                }
                var param;
                if($scope.action === 'ADD_PARENT') {
                    //添加综合统考
                    var selectedSubjects = _.filter($scope.subjectList, function (sbj) {
                        return !!sbj.isSelected;
                    });
                    if (!selectedSubjects || selectedSubjects.length === 0) {
                        constantService.alert('请至少选择一门参加考试的学科！');
                        return;
                    }
                    var visibleUsers = _.map(selectedSubjects, function(item){
                        return {
                            subjectId: item.subjectid,
                            userIds: _.pluck(_.filter(item.admins, function(ad){
								return !!ad.isSelected
							}), 'FLnkID').join(',')
                        }
                    });
                    console.log(visibleUsers);
                    param = {
                        examName: $scope.data.examName,
                        examDate: date,
                        examType: $scope.data.examType.ExType,
                        diff: $scope.data.diff,
                        subjectIds: _.pluck(selectedSubjects, 'subjectid').join(','),
                        gradeNum: $scope.currentGradeNum,
                        visibleUser: visibleUsers,
                        markMode: $scope.data.markMode
                    };
                    bes.send('/BootStrap/schoolmanager/createExam.ashx', param, {
                        method: 'post'
                    }).then(function(res){
                        if(res.data.code === 0) {
                            constantService.alert('创建考试成功!');
                            $state.go('myApp.examCenter');
                        }
                    });
                }else if($scope.action === 'ADD_SUB') {
                    //如果是添加单科统考
                    if(!$scope.data.chosedSubject) {
                        constantService.alert('请选择一门参加考试的学科！');
                        return;
                    }
                    param = {
                        unifiedId: unifiedId,
                        examName: $scope.data.examName,
                        examType: $scope.data.examType.ExType,
                        examDate: date,
                        diff: $scope.data.diff,
                        subjectId: $scope.data.chosedSubject,
						visibleUser: getSelectedAdmins(),
                        markMode: $scope.data.markMode
                    };
                    bes.send('/BootStrap/schoolmanager/createSubUnifiedTask.ashx', param, {
                        method: 'post'
                    }).then(function(res){
                        if(res.data.code === 0) {
                            constantService.alert('新增考试成功!');
                            $state.go('myApp.examCenter');
                        }
                    });
                }else {
                        param = {
                            unifiedId: unifiedId,
                            examName: $scope.data.examName,
                            examDate: date,
                            diff: $scope.data.diff,
                            subjectId: $scope.currentSubjectId,
                            visibleUser: getSelectedAdmins(),
                            markMode: $scope.data.markMode,
                            examType: $scope.data.examType.ExType
                        };
                        bes.send('/BootStrap/schoolmanager/updateUnifiedItem.ashx', param, {
                            method: 'post'
                        }).then(function(res){
                            if(res.data.code === 0) {
                                if(parenUnifiedId) {
                                    apiCommon.modifyIntegrateUnifedName({
                                        unifiedId:parenUnifiedId,
                                        unifedName:$scope.data.examName,
                                        examType:$scope.data.examType.ExType
                                    }).then(function (res) {
                                        if(+res.data.code === 0){
                                            constantService.alert('编辑考试成功!');
                                            $state.go('myApp.examCenter');
                                        }
                                    },function (res) {
                                        constantService.alert(res.data.msg);
                                    });
                                } else {
                                    constantService.alert('编辑考试成功!');
                                }

                            }else{
                                constantService.alert(res.data.msg);
                            }
                        });
                    // if($scope.submitNum > 0){
                    //     ngDialog.open({
                    //         template: '<div class="batchScore">' + '<div class="batchScore-nav">编辑保存</div>' +
                    //         '<div class="batchScore-title">本次统考已经有试卷提交，再次编辑将重置所有考试信息!</div>' +
                    //         '<div class="batchScore-click"><div class="batchScore-btn" ng-click="save()" style="margin:20px 20px 0 116px">确定</div>' +
                    //         '<div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin-top:20px">取消</div></div>' +
                    //         '</div>',
                    //         className: 'ngdialog-theme-default ngdialog-setValue',
                    //         scope: $scope,
                    //         closeByDocument: false,
                    //         plain: true
                    //     });
                    //     $scope.save = function () {
                    //         savePaper();
                    //         ngDialog.close();
                    //     }
                    // }else{
                    //     ngDialog.open({
                    //         template: '<div class="batchScore">' + '<div class="batchScore-nav">编辑保存</div>' +
                    //         '<div class="batchScore-title">改动学科将重置本试卷的所以统考信息，请确认保存!</div>' +
                    //         '<div class="batchScore-click"><div class="batchScore-btn" ng-click="save()" style="margin:20px 20px 0 116px">确定</div>' +
                    //         '<div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin-top:20px">取消</div></div>' +
                    //         '</div>',
                    //         className: 'ngdialog-theme-default ngdialog-setValue',
                    //         scope: $scope,
                    //         closeByDocument: false,
                    //         plain: true
                    //     });
                    //     $scope.save = function () {
                    //         savePaper();
                    //         ngDialog.close();
                    //     }
                    // }
                    // function savePaper() {
                    //     param = {
                    //         unifiedId: unifiedId,
                    //         examName: $scope.data.examName,
                    //         examDate: date,
                    //         diff: $scope.data.diff,
                    //         subjectId: $scope.checkSubject ,
                    //         visibleUser: getSelectedAdmins(),
                    //         markMode: $scope.data.markMode
                    //     };
                    //     bes.send('/BootStrap/schoolmanager/updateUnifiedItem.ashx', param, {
                    //         method: 'post'
                    //     }).then(function(res){
                    //         if(res.data.code === 0) {
                    //             constantService.alert('恭喜您，编辑考试成功！即将前往考试中心。', function(){
                    //                 $state.go('myApp.examCenter');
                    //             });
                    //         }else{
                    //             constantService.alert(res.data.msg);
                    //         }
                    //     });
                    // }
                }
            };

            $scope.filterTeacher = function(item){
				if($scope.isAllTeacher) {
					return true;
				}else {
				    var chosenSubject = _.find($scope.subjectList, function(sbj) {
				        return !!sbj.isSelected;
                    });
				    if(chosenSubject) {
						return (''+item.SubjectId) === (''+chosenSubject.subjectid) || !item.SubjectId;
                    }else {
				        return !item.SubjectId;
                    }
				}
            };

            $scope.viewAllTeacher = function(){
				$scope.isAllTeacher = true;
            };
            /**
             * 过滤年级
             */
            function filterGradeId(list) {
                var cUserGrade = JSON.parse(window.sessionStorage.getItem('currentUser')).GradeNo;
                var afterfilter = _.filter(list,function (item) {
                    return +item.GradeNum === +cUserGrade;
                });
                if(afterfilter.length>0){
                    return afterfilter;
                }else{
                    return list;
                }
            }

            /**
             * 根据当前用户科目过滤学科
             */
            function filterSubject(list) {
                var cUserSubject = JSON.parse(window.sessionStorage.getItem('currentUser')).subjectId;
                var afterfilter = _.filter(list,function (item) {
                    return +item.subjectid === cUserSubject;
                });
                if(afterfilter.length > 0){
                    return afterfilter;
                }else{
                    return list;
                }
            }
        }]
});