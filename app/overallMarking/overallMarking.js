/**
 * Created by 赵文东 on 2016/11/9.
 */
define(['jquerymedia'], function(){
    return ['$rootScope','$scope', '$http','$location', '$q', 'constantService', 'paperService', 'utilService', 'ngDialog','$state',
        function( $rootScope,$scope, $http,$location,$q, constantService, paperService, utilService, ngDialog,$state){
            $('#overallMarking').css('height',$(window).height());
            $(window).off('resize').on('resize', function(e){
                $('#overallMarking').css('height',$(window).height());
            });

            var search = $location.search();
            var examId = search.ExamFlnkID;
            var classId = search.ClassFlnkID;
            var taskId = search.TaskFId;
            $scope.isAllowMark = search.allowMark;
            $scope.examName = search.ExamName;
            var unifiedItemFid = search.UnifiedItemFid;

            $scope.logoSrc = '/assets/skin/' + window.config.theme + '/img/loading-logo.png';
            $scope.copy = window.config.themeCompanyNameMap[window.config.theme];
            $scope.no_data_img = '/assets/images/51jyfw-nopic.jpg';
            $scope.data = {
                currentStuList : [],
                currentQuestionsArr:[],
                currentPaperName : '',
                currentPaperImg : '',
                currentStu:null,
                currentPaperMyScore:0,
                currentPaperAccessory:'',
                isShowpdf:false

            };
            //存储获取到的试卷配置数据
            var paperConfigData = null;
            //存储合并数据 在开始展示时找出合并的题目 在提交分数的时候拆开赋值的依据 每次切换学生的时候需要重新赋值 数据里面包含 showDis 展示题号
            //hiddenQstlist 被合并题目（不包含合并题目的第一题） showQst 被合并题目的第一题
            var combinDetailList = [];

            /**
             * type值：-1：未批改；0：已批改；2：无数据
             */
            $http.post($rootScope.baseUrl + '/Interface0045A.ashx', {
                classId: classId,
                examFLnkId: examId
            }).then(function(resp) {
                if(_.isArray(resp.data.studentList)){
                    $scope.data.currentStuList = resp.data.studentList;
                    _.each($scope.data.currentStuList,function (i) {
                        i.type = +i.type;
                    });
                    //对学生列表 按照批阅状态进行排序
                    $scope.data.currentStuList.sort(function(a, b){
                        return a.type - b.type;
                    });
                }
                //从未批阅的开始
                for (var i = 0; i < $scope.data.currentStuList.length; i++) {
                    if ($scope.data.currentStuList[0].type == -1) {
                        $scope.data.currentStu = $scope.data.currentStuList[i];
                        break;
                    }
                }
                //如果没有为批阅的则从第一个开始
                if(!$scope.data.currentStu){
                    $scope.data.currentStu = $scope.data.currentStuList[0];
                }
                //获取错误题目信息
                getErrQst();
            },function (resp) {
                constantService.alert('获取学生列表失败！');
            });

            $scope.$watch('data.currentStu', function(newValue, oldValue){
                if(newValue === oldValue){
                    return;
                }
                if( !newValue){
                    return;
                }
                $http.post($rootScope.baseUrl + '/Interface0187.ashx', {
                    ExamFLnkID: examId,
                    UserFLnkID: $scope.data.currentStu.studentId
                }).then(function (resp) {
                    if (resp.data.img == null) {
                        $scope.data.currentPaperImg = $scope.no_data_img;
                    } else {
                        var tempimgsplit= resp.data.img.split('/');
                        tempimgsplit[tempimgsplit.length-1] = '0' + tempimgsplit[tempimgsplit.length-1];
                        $scope.data.currentPaperImg = tempimgsplit.join('/');
                        $scope.data.currentPaperAccessory = resp.data.Accessory;
                        if($scope.data.isShowpdf){
                            showPdf($scope.data.currentPaperAccessory);
                        }
                        $scope.data.currentPaperName = resp.data.ExName;
                        //$scope.data.currentQuestionsArr数组中[0]为组名，[1]为题目数组，[2]布尔值判断是否展开或者收起，[3]标志位判断该组题目中是否存在错误题目,[4]每组题目学生得分
                        $scope.data.currentQuestionsArr = _.pairs(_.groupBy(resp.data.msg, 'dType'));
                        $scope.data.currentPaperTotalScore = 0;
                        _.each($scope.data.currentQuestionsArr,function (i) {
                            var everyGroupMyTotalScore = 0;//每组题目学生总得分
                            _.each(i[1],function (item) {
                                item.allscores = +item.allscores;
                                item.order = +item.order;
                                item.scores = !!item.scores? +item.scores : item.score;
                                item.beChanged = false;
                                //计算试卷总分
                                $scope.data.currentPaperTotalScore = $scope.data.currentPaperTotalScore + item.allscores;
                                //判断题目列表中是否有后纠错题目
                                if(!!$scope.data.currentStu.errQstList && $scope.data.currentStu.errQstList.length > 0){
                                    for(var j = 0; j < $scope.data.currentStu.errQstList.length; j++){
                                        if(item.order === +$scope.data.currentStu.errQstList[j].order){
                                            item.isError = true;
                                        }
                                    }
                                }
                                if(item.scores === '' || item.scores === null || item.scores < 0 || item.scores > item.allscores || item.scores * 10 % 5 !== 0){
                                    item.isError = true;
                                }else{
                                    item.isError = false;
                                }
                                everyGroupMyTotalScore = everyGroupMyTotalScore + (!!item.scores? +item.scores : 0);
                            });
                            i.push(i[0] === '选择题'? false:true);
                            //查找该组中是否存在后纠错题目
                            i.push(!!_.find(i[1],function (k) {
                                return !!k.isError;
                            }));
                            i.push(everyGroupMyTotalScore);
                        });
                        getPaperConfig();
                    }
                    calStuScore();
                },function (resp) {
                    constantService.alert('获取学生试卷信息失败！')
                });
                scrollToCurrentStu();
            });
            $scope.expandList = function (index) {
                $scope.data.currentQuestionsArr[index][2] = !$scope.data.currentQuestionsArr[index][2];
            };

            $scope.submitScore = function () {
                var questionListArry = [];
                var errOrders = [];
                var newPage = _.map($scope.data.currentQuestionsArr, function(group, i) {
                    group = handleCompletion(combinDetailList,group);
                    var questionsL = _.map(group[1], function(item, index) {
                        if(+item.scores > +item.allscores || +item.scores < 0 ||  +item.scores * 10 % 5 !== 0 || item.scores === '' || item.scores === undefined || +item.scores === null){
                            errOrders.push(item.order);
                        }
                        return {
                            'qFlnkId': item.qFlnkId,
                            'scores': item.scores
                        }
                    });
                    return {
                        quesitonBox: questionsL
                    }
                });
                checkSetPaperList();
                if(errOrders.length){
                    var orders = errOrders.join(',');
                    /*ngDialog.openConfirm({
                        template: '<p>'+'第'+orders+'题的得分不合理，请核对!'+'</p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button>',
                        className: 'ngdialog-theme-plain',
                        scope: $scope,
                        plain: true
                    });*/
                    constantService.alert('第'+orders+'题的得分不合理，请核对!');
                    return;
                }
                var greatObj = function() {
                    angular.forEach(newPage, function(item) {
                        angular.forEach(item.quesitonBox, function(item) {
                            questionListArry.push(item)
                        })
                    });
                    return questionListArry;
                };

                var msgDate = greatObj();

                $http({
                    method: 'POST',
                    url: $rootScope.baseUrl + '/Interface0188.ashx',
                    data: {
                        examFlnkId: examId,
                        userFlnkId: $scope.data.currentStu.studentId,
                        msg: msgDate
                    }
                }).then(function(resp) {
                    $scope.data.currentStu.type = 0;
                    /*ngDialog.openConfirm({
                        template: '<p> ' + resp.data.msg + ' </p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button>',
                        className: 'ngdialog-theme-plain',
                        scope: $scope,
                        plain: true
                    });*/
                    constantService.alert(resp.data.msg);
                    calStuScore();
                    next();
                },function (resp) {
                    constantService.alert('分数提交失败！');
                    calStuScore();
                });
            };
            function getErrQst() {
                /*后纠错*/
                $http.post('/BootStrap/EncryptUnified/AiTaskAssessment.ashx', {//数据有问题的学生
                    taskFid:taskId
                }).then(function(resp) {
                    for(var i = 0;i<resp.data.msg.length;i++){
                        for(var j = 0;j<$scope.data.currentStuList.length;j++){
                            if(resp.data.msg[i].userfid === $scope.data.currentStuList[j].studentId){
                                $scope.data.currentStuList[j].errQstList = resp.data.msg[i].qOrder;
                            }
                        }
                    }
                },function (resp) {
                    console.log('请求纠错接口失败！');
                });
            }
            //处理输入的分数
            $scope.inputScore = function (question,index,group,e) {
                var tar = e.target;
                $(tar).css({
                    'color': '#000'
                });
                var errorflag = true;
                if(!checkScoreIllegal(question.scores,question.allscores,false)){
                    errorflag = true;
                }else{
                    errorflag = false;
                }
                var groupindex = _.findIndex($scope.data.currentQuestionsArr,group);
                if(groupindex>=0){
                    $scope.data.currentQuestionsArr[groupindex][1][index].beChanged = true;
                    $scope.data.currentQuestionsArr[groupindex][1][index].isError = errorflag;
                }
                calStuScore();
                check4Error();
            };
            //计算当前学生得分
            function calStuScore() {
                var score = 0;
                _.each($scope.data.currentQuestionsArr,function (item) {
                    var everyGroupMyTotalScore = 0;
                    _.each(item[1],function (i) {
                        score = score + (!!i.scores ? +i.scores : 0);
                        everyGroupMyTotalScore = everyGroupMyTotalScore + (!!i.scores? +i.scores : 0);
                    });
                    item[4] = everyGroupMyTotalScore;
                });
                $scope.data.currentPaperMyScore = score;
            }
            //修改分数后判断该组内是否还有错误题目
            function check4Error() {
                _.each($scope.data.currentQuestionsArr,function (item) {
                    var temflag = false;
                    _.each(item[1],function (i) {
                        if(i.isError){
                            temflag = true;
                            return false;
                        }
                    });
                    item[3] = temflag;
                });
            }
            //设置默认图片展示比例
            $scope.imgWidth = $('.stu-answer-box').width() - 40;
            $scope.scale = 1;

            $scope.showOriginal = function(){
                $scope.imgWidth = $('.stu-answer-box').width() - 40;
                $scope.scale = 1;
            };

            $scope.minus = function(){
                $scope.imgWidth = (($scope.imgWidth - 80 > 500) ? ($scope.imgWidth - 80) : 500);
                if($scope.scale > 0.2){
                    $scope.scale = $scope.scale - 0.1;
                }
            };

            $scope.plus = function(){
                $scope.imgWidth = $scope.imgWidth + 80;
                if($scope.scale < 2){
                    $scope.scale = $scope.scale + 0.1;
                }
            };
            $(window).off('keyup.over').on('keyup.over', function(e){
                e.preventDefault();
                e.stopPropagation();
                $scope.$apply(handleSmallkeybord(e));

            });
            function handleSmallkeybord(e){
                switch(e.keyCode){
                    case 39:
                        //右箭头
                        if(closeDialog()){
                            ngDialog.closeAll();
                        } else {
                            next(true);
                        }
                        break;
                    case 37:
                        //左箭头
                        if(closeDialog()){
                            ngDialog.closeAll();
                        } else {
                            pre();
                        }
                        break;
                    default:
                        break;
                }
            }
            $scope.markStudent = function(stu){
                if(stu.type === 2){
                    return;
                }
                $scope.data.currentStu = stu;
            };

            function next(canShowTip){
                //var index = $scope.data.currentStu.index;
                var index = _.findIndex($scope.data.currentStuList,$scope.data.currentStu);
                for(; index < $scope.data.currentStuList.length;){
                    if(index === $scope.data.currentStuList.length - 1){
                        if(!!canShowTip){
                            constantService.alert('已经到最后了，不能再向后了！');
                        }
                        return;
                    } else {
                        index = index + 1;
                    }
                    if($scope.data.currentStuList[index].type === 2){
                        continue;
                    } else {
                        break;
                    }
                }
                $scope.data.currentStu = $scope.data.currentStuList[index];
            }

            function pre(){
                var index = _.findIndex($scope.data.currentStuList,$scope.data.currentStu);
                if(index === 0){
                    constantService.alert('已经是第一个了，不能再向前了！');
                } else {
                    $scope.data.currentStu = $scope.data.currentStuList[index - 1];
                }
            }



            $scope.preventDefault = function(e){
                switch(e.keyCode){
                    case 187:
                    case 107:
                    case 189:
                    case 109:
                        e.preventDefault();
                        e.stopPropagation();
                        break;
                    default:
                        break;
                }
            };
            $rootScope.$on('cfpLoadingBar:started', function(){
                //TODO
            });

            function closeDialog(){
                var loglist = ngDialog.getOpenDialogs();
                if(_.isArray(loglist) && loglist.length > 0){
                    return true
                } else {
                    return false;
                }
            }

            function scrollToCurrentStu(){
                var $keyBoards = $('.keyboards');
                var boxOffset = $keyBoards.offset().top,
                    boxHeight = $keyBoards.height(),
                    listHeight = $keyBoards.find('ul').height();
                var index = _.findIndex($scope.data.currentStuList,$scope.data.currentStu);
                setTimeout(function(){
                    if($('#' + index).offset()){
                        var keyOffset = $('#' + index).offset().top + $keyBoards.scrollTop();
                        $keyBoards.scrollTop(keyOffset - boxHeight / 2 - boxOffset);
                    }
                }, 100);
            }

            function checkScoreIllegal(score, max,isShowAlert){
                if(+score < 0 || +score > max || +score * 10 % 5 !== 0 || score === '' || score === null){
                    if(isShowAlert){
                        constantService.alert('分数必须为0.5的整数倍，且不能大于本题总分值，请重新批阅！');
                    }
                    return false;
                } else {
                    return true;
                }
            }
            $scope.goBackButton = function () {
                //window.history.go(-1);//回到上一个页面
                //回到列表页面
                $state.go('myApp.marking');
            };
            $scope.inputSelect = function (e) {
                e.target.select();
            };

            $scope.showpdf = function () {
                $scope.data.isShowpdf = !$scope.data.isShowpdf;
                if($scope.data.isShowpdf){
                    showPdf($scope.data.currentPaperAccessory);
                }
            };
            $scope.backRelated = function(){
                $http({
                    method:'post',
                    url: '/BootStrap/Interface/resetTaskStatus.ashx',
                    data:{
                        taskFLnkId : taskId
                    }
                }).then(function(resp){
                    if(resp.data.code == '0'){
                        resp.data.status = '1';
                        $state.go('myApp.relatedpaper',{
                            ExamFlnkID:examId,
                            ClassFlnkid:classId,
                            TaskFId:taskId,
                            allowMark:$scope.isAllowMark,
                            ExamName:$scope.examName,
                            UserFLnkID:$scope.data.currentStu.studentId,
                            isShowAll:true,
                            turnto:0,
                            UnifiedItemFid:unifiedItemFid
                        })
                    }
                })

            };
            function showPdf(url){
                $('#overallMarkingPdf').attr('href', url).media({
                    'width': $('.stu-answer-box').width(),
                    'height': $(window).height()-210,
                    'margin-top': '0'
                });
            }

            /**
             * 题号合并改造
             * 支持题号后批阅
             */
            //获取配置列表
            function getPaperConfig(currentQstArr){
                if((unifiedItemFid+'') === '0' || unifiedItemFid === undefined || unifiedItemFid === null){
                    return;
                }else{
                    rquestPaperConfig(unifiedItemFid).then(function (res) {
                        if(!!res.data.msg.meta&& res.data.msg.meta.length>0){
                            paperConfigData = res.data.msg.meta;
                            insertShowDis($scope.data.currentQuestionsArr,paperConfigData);
                            checkSetPaperList();
                        }
                    });
                }
            }
            /**
             * 对照配置表合并题目
             */
            function checkSetPaperList() {
                combinDetailList = [];
                _.each($scope.data.currentQuestionsArr,function (item) {
                    var combinlist = filterCombin(item[1]);
                    if(combinlist.length>0){
                        _.each(combinlist,function (h) {
                            _.each(item[1],function (n) {
                                if(n.qFlnkId === h.showQst.qFlnkId){
                                    n = h.showQst;
                                }
                            });
                            _.each(h.hiddenQstlist,function (m) {
                                item[1] = _.without(item[1],m);
                            });
                        });
                        combinDetailList.push({dtype:item[0],combin:combinlist});
                    }
                });
            }
            function rquestPaperConfig(unifiedFid) {
                var defer = $q.defer();
                $http.post('' + '/BootStrap/EncryptUnified/getPaperConfig.ashx', {
                    unifiedId: unifiedFid
                }).then(function(res){
                    defer.resolve(res);
                }, function(res){
                    defer.reject(res)
                });
                return defer.promise;
            }
            function filterCombin(list) {
                var tempShowDisList = [];
                _.each(list,function (item) {
                    if(item.showDis !== undefined && item.showDis !== null){
                        tempShowDisList.push(item.showDis);
                    }
                });
                tempShowDisList = _.uniq(tempShowDisList);
                var showdisGroup = [];
                _.each(tempShowDisList,function (item) {
                    var tempQst = _.filter(list,function (h) {
                        return h.showDis === item;
                    });
                    if(tempQst.length>1){
                        var tempbeChanged = _.find(tempQst,function (e) {
                            return e.beChanged === true;
                        });
                        tempQst[0].beChanged = (tempbeChanged === undefined ? false : true);
                        var tempisError = _.find(tempQst,function (f) {
                            return f.isError === true;
                        });
                        tempQst[0].isError = (tempisError === undefined ? false : true);
                        var tempallscores = 0;
                        _.each(tempQst,function (g) {
                            tempallscores = g.allscores + tempallscores;
                        });
                        tempQst[0].allscores = tempallscores;
                        var undefinedscore = _.filter(tempQst,function (f) {
                            return f.scores === undefined || f.scores === null;
                        });
                        if(undefinedscore.length === tempQst.length){
                            tempQst[0].scores = undefined;
                        }else{
                            var temnumscores = 0;
                            _.each(tempQst,function (t) {
                                if(t.scores !== undefined && t.scores !== null){
                                    temnumscores = t.scores + temnumscores;
                                }
                            });
                            tempQst[0].scores = temnumscores;
                        }
                        showdisGroup.push({showDis:item,hiddenQstlist:_.without(tempQst,tempQst[0]),showQst:tempQst[0]});
                    }
                });
                return showdisGroup;
            }
            function insertShowDis(orginlist,configlist) {
                for(var i = 0; i < orginlist.length; i++){
                    _.each(orginlist[i][1],function (item) {
                        _.each(configlist,function (h) {
                            var tempQst = _.find(h.question,function (n) {
                                return n.qFlnkId === item.qFlnkId;
                            });
                            if(tempQst !== undefined){
                                item.showDis = tempQst.showDis;
                            }
                        });
                    });
                }
            }
            //提交分数的时候 要把合并的题目补全
            function handleCompletion(combinlist,group) {
                _.each(combinlist,function (item) {
                    if(item.dtype === group[0]){
                        _.each(item.combin,function (s) {
                            _.each(group[1],function (h,num) {
                                if(h.qFlnkId === s.showQst.qFlnkId){
                                    var thandlelist = [];
                                    s.hiddenQstlist.unshift(s.showQst);
                                    thandlelist = handleCombineQstScore(s.hiddenQstlist,h.scores);
                                    _.each(thandlelist,function (n,index) {
                                        if(index === 0){
                                            h = n;
                                        }else{
                                            group[1].splice(num+index,0,n);
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                return group;
            }
            //处理分数 将合并的分数分给其他题目
            function handleCombineQstScore(qstlist,allscore) {
                var allscore = allscore;
                var allqstnum = qstlist.length;
                _.each(qstlist,function (q) {
                    q.scores = 0;
                });
                for(;Math.floor(allscore/allqstnum)>=0;){
                    var t = Math.floor(allscore/allqstnum);
                    if(t>0){
                        _.each(qstlist,function (item) {
                            item.scores = (+item.scores)+t;
                        });
                    }else if(t==0&&(allscore%allqstnum>0)){
                        for(var i = 0;i<allscore%allqstnum;i++){
                            qstlist[i].scores = (+qstlist[i].scores)+1;
                        }
                        break;
                    }else{
                        break;
                    }
                    allscore = allscore - allqstnum * t;
                }
                return qstlist;
            }
        }]
});