/**
 * Created by Rookie_Zoe on 2015/6/13.
 */
'use strict';
angular.module('myApp.sourceManage', [])
    .controller('sourceManageCtrl', ['$scope', '$http', '$rootScope', '$log', 'KPMService' ,'dialogService', function($scope, $http, $rootScope, $log, KPMService, dialogService) {
        var jQ = angular.element;

        $scope.srcTypes = ['课件', '视频', '书籍', '考卷', '微课', '图片', '其他'];
        //$scope.qTypes = ['全部','系统题库', '个人题库', '学校题库', '共享题库'];
        //$scope.srcRecommends = ['全部','很高', '高', '中等', '一般'];
        $scope.srcTypestatus = {
            "types": ["", "", "", "", "", "", ""]
        };
        $scope.choosenQType = '';
        $scope.choosenRecommend = '';
        $scope.choosenSrcType = '';
        $scope.uploadJson = {};
        $scope.choosenAll = false;


        $scope.coursesList = [];
        $scope.selectedCourse = {};

        $scope.chaptersList = [];
        $scope.selectedSection = {};

        $scope.sourceList = [];
        $scope.sourceListShow = false;
        $scope.sourceListMsg = false;
        $scope.uploadAreaShow = false;
        $scope.showSearch = true;

        $scope.checkAllCategory = false;
        $scope.$watch('checkAllCategory', function(boo){
            if(boo){
                for (var i in $scope.srcTypes) {
                    $scope.srcTypestatus['status' + i] = true;
                }
            }else {
                for (var i in $scope.srcTypes) {
                    $scope.srcTypestatus['status' + i] = false;
                }
            }
            $scope.setSrcTypes();
        });

        $scope.$watch('uploadAreaShow', function(boo){
            if(boo){
                $scope.qTypes = ['系统题库', '个人题库', '学校题库', '共享题库'];
                $scope.srcRecommends = ['很高', '高', '中等', '一般'];
            }else {
                $scope.qTypes = ['全部','系统题库', '个人题库', '学校题库', '共享题库'];
                $scope.srcRecommends = ['全部','很高', '高', '中等', '一般'];
            }
        });


        KPMService.getCourses().then(function(resp) {
            if (!!resp && !!resp.data && !!resp.data.course) {
                $scope.coursesList = resp.data.course;
                $scope.selectedCourse = $scope.coursesList[0];
            } else {
                $scope.coursesList = [{
                    'courseId': '-1',
                    'courseName': '没有数据'
                }];
                $scope.selectedCourse = $scope.coursesList[0];
            }
        });

        $scope.getChapters = function(courseId) {
            KPMService.getChaptersById(courseId).then(function(resp) {
                if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object") {
                    $scope.chaptersList = resp.data.msg;
                    $scope.selectedSection = $scope.chaptersList[0] && $scope.chaptersList[0].section[0];
                } else {
                    $scope.chaptersList = [];
                    $scope.selectedSection = {};
                }
            });
        };

        $scope.getSourceList = function() {
            var questionType = $scope.choosenQType, recommend = $scope.choosenRecommend;
            if($scope.choosenQType === '全部'){
                var tmpQType = $scope.qTypes.slice(1);
                questionType = tmpQType.join(',');
            }
            if($scope.choosenRecommend === '全部'){
                var tmpRType = $scope.srcRecommends.slice(1);
                recommend = tmpRType.join(',');
            }
            KPMService.getSourceList({
                'sectionId': $scope.selectedSection.sectionId || "",
                'type1': $scope.srcTypestatus.types[0] || "",
                'type2': $scope.srcTypestatus.types[1] || "",
                'type3': $scope.srcTypestatus.types[2] || "",
                'type4': $scope.srcTypestatus.types[3] || "",
                'type5': $scope.srcTypestatus.types[4] || "",
                'type6': $scope.srcTypestatus.types[5] || "",
                'type7': $scope.srcTypestatus.types[6] || "",
                'questionType': questionType || '',
                'recommend': recommend || ""
            }).then(function(resp) {
                if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object") {
                    $scope.sourceList = resp.data.msg;
                    $scope.sourceListShow = true;
                    $scope.sourceListMsg = false;
                } else {
                    $scope.sourceList = [];
                    $scope.sourceListShow = false;
                    $scope.sourceListMsg = true;
                }
            });
        };

        $scope.setSrcTypes = function() {
            var typesTemp = [];
            for (var i in $scope.srcTypes) {
                if (!!$scope.srcTypestatus['status' + i]) {
                    typesTemp.push($scope.srcTypes[i]);
                } else {
                    typesTemp.push('');
                }
            }
            $log.debug(typesTemp);
            $scope.srcTypestatus.types = typesTemp;
        };

        $scope.setChoosenSource = function(source) {
            $log.debug(source);
        };

        $scope.chooseAllSources = function() {
            for (var i in $scope.sourceList) {
                $scope.sourceList[i].choosen = $scope.choosenAll;
            }
        };

        $scope.download = function() {
            console.log($scope.sourceList);
            var choosedFile = _.filter($scope.sourceList, function(source){
                return !!source.choosen;
            });
            if(_.isEmpty(choosedFile)){
                dialogService.showAlertDialog({
                    text: '请至少选择一个课件！',
                    type: 'warning'
                }, function() {
                   
                });
            }else {
                var urls = _.map(choosedFile, function(file){
                    return file.url;
                });
                Downer(urls);
            }
        };

        $scope.upload = function() {
            $log.debug($scope.uploadJson);
            $('#sourcemanage #upload_loading').css({
                opacity: 1,
                'z-index': 1000
            });
            KPMService.uploadSource({
                'sectionId': $scope.selectedSection.sectionId || "",
                'type': $scope.choosenSrcType || "",
                'questionType': $scope.choosenQType || "",
                'recommend': $scope.choosenRecommend || "",
                "acessory": $scope.uploadJson.acessory || "",
                "memo": $scope.uploadJson.memo || "",
                "rname": $scope.uploadJson.rname || "",
            }).then(function(resp) {
                $('#sourcemanage #upload_loading').css({
                    opacity: 0,
                    'z-index': '-1'
                });
                if (resp.data.code === 0) {
                    alert('上传成功');
                }
            });
        };

        $scope.showUploadArea = function() {
            $scope.showSearch = false;
            $scope.sourceListShow = false;
            $scope.sourceListMsg = false;
            $scope.uploadAreaShow = true;
            $scope.srcTypestatus = {
                "types": ["", "", "", "", "", "", ""]
            };
            $scope.choosenQType = '';
            $scope.choosenRecommend = '';
            $scope.choosenSrcType = '';
            $scope.uploadJson = {};
        };

        $scope.showSearchArea = function() {
            $scope.showSearch = true;
            $scope.sourceListShow = false;
            $scope.sourceListMsg = false;
            $scope.uploadAreaShow = false;
            $scope.srcTypestatus = {
                "types": ["", "", "", "", "", "", ""]
            };
            $scope.choosenQType = '';
            $scope.choosenRecommend = '';
            $scope.choosenSrcType = '';
            $scope.uploadJson = {};
        };

        $scope.$watch('selectedCourse', function(newVal) {
            $scope.srcTypestatus = {
                "types": ["", "", "", "", "", "", ""]
            };
            $scope.choosenQType = '';
            $scope.choosenRecommend = '';
            $scope.choosenAll = false;
            $scope.sourceList = [];
            $scope.sourceListShow = false;
            $scope.sourceListMsg = false;
            $scope.uploadAreaShow = false;
            $scope.uploadJson = {};
            if (jQ.isEmptyObject(newVal) || newVal.courseId === '-1') {
                $scope.chaptersList = [];
                $scope.selectedSection = {};
                return;
            }
            $scope.getChapters(newVal.courseId);
        });

        $scope.$watch('selectedSection', function(newVal) {
            $scope.srcTypestatus = {
                "types": ["", "", "", "", "", "", ""]
            };
            $scope.choosenQType = '';
            $scope.choosenRecommend = '';
            $scope.choosenAll = false;
            $scope.sourceList = [];
            $scope.sourceListShow = false;
            $scope.sourceListMsg = false;
            $scope.uploadAreaShow = false;
            $scope.uploadJson = {};
            if (jQ.isEmptyObject(newVal)) {
                return;
            }
        });
    }]);
