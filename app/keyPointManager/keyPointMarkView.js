/**
 * Created by Rookie_Zoe on 2015/6/13.
 */
'use strict';
angular.module('myApp.keyPointMark', [])
    .controller('keyPointMarkCtrl', ['$scope', '$http', '$rootScope', '$log', '$sce', '$q', 'KPMService', 'dialogService',
        function($scope, $http, $rootScope, $log, $sce, $q, KPMService, dialogService) {
            var jQ = angular.element;

            $scope.knMasters = ['重点', '难点', '掌握', '理解'];
            $scope.knUseTimes = [1, 4, 8, 12];
            $scope.knRecommends = ['很高', '高', '中等', '一般'];
            $scope.knOpenRanges = ['自己可见', '班级可见', '全校可见', '全站共享'];


            $scope.coursesList = [];
            $scope.selectedCourse = {};

            $scope.chaptersList = [];
            $scope.selectedSection = {};

            $scope.keyPointsJson = {};
            $scope.keyPointsList = [];
            $scope.keyPointsRange = [];
            $scope.selectedRange = {};
            $scope.keyPointsJsonShow = false;
            $scope.keyPointsRangeShow = false;
            $scope.keyPointsJsonError = false;
            $scope.keyPointsRangeError = false;
            $scope.isUpdating = false;
            $scope.isEditing = false;
            $scope.showList = true;

            KPMService.getCourses().then(function(resp) {
                if (!!resp && !!resp.data && !!resp.data.course) {
                    $scope.coursesList = resp.data.course;
                    $scope.selectedCourse = $scope.coursesList[0];
                } else {
                    $scope.coursesList = [{
                        'courseId': '-1',
                        'courseName': '没有数据'
                    }];
                    $scope.selectedCourse = $scope.coursesList[0];
                }
            });

            $scope.getChapters = function(courseId) {
                KPMService.getChaptersById(courseId).then(function(resp) {
                    if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object") {
                        $scope.chaptersList = resp.data.msg;
                        $scope.selectedSection = $scope.chaptersList[0] && $scope.chaptersList[0].section[0];
                    } else {
                        $scope.chaptersList = [];
                        $scope.selectedSection = {};
                    }
                });
            };

            $scope.showListArea = function() {
                if ($scope.showList) {
                    return;
                } else {
                    $scope.showList = true;
                    $scope.isUpdating = false;
                    $scope.isEditing = false;
                    $scope.getRangeKnPointsById($scope.selectedCourse.courseId);
                }
            };

            $scope.showUpdateArea = function() {
                if (!$scope.showList) {
                    return;
                } else {
                    $scope.showList = false;
                    $scope.isUpdating = true;
                    $scope.isEditing = false;
                    $scope.getKeyPointsById($scope.selectedSection.sectionId);
                }
            };

            $scope.getRangeKnPointsById = function(courseId) {
                $scope.init();
                KPMService.getRangeKnPointsById(courseId).then(function(resp) {
                    $scope.isUpdating = false;
                    if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object") {
                        $log.debug(resp.data.msg);
                        $scope.keyPointsRange = resp.data.msg;
                        $scope.keyPointsRangeShow = true;
                        $scope.keyPointsRangeError = false;
                    } else {
                        $scope.keyPointsRange = [];
                        $scope.keyPointsRangeShow = false;
                        $scope.keyPointsRangeError = true;
                    }
                });
            };

            $scope.editorKeyPointRange = function(range) {
                $scope.init();
                if (!range) {
                    return;
                }
                $scope.selectedRange = range;
                KPMService.getRangeDetailById(range.tfId).then(function(resp) {
                    if (!!resp && !!resp.data) {

                        $('#leftlistarea').show();
                        $('#backButton').show();
                        $('#rightarea').removeClass('rightarea');

                        $scope.isEditing = true;
                        $scope.keyPointsJson = resp.data;
                        $log.debug(resp.data);
                        if (!!resp.data.msg && typeof(resp.data.msg) === "object") {
                            $scope.getKeyPointsById($scope.keyPointsJson.sectionId, true, resp.data.msg);
                        }
                    } else {


                        $scope.isUpdating = false;
                        $scope.isEditing = false;
                        $scope.keyPointsList = [];
                        $scope.keyPointsJsonShow = false;
                        $scope.keyPointsJsonError = true;
                    }
                });
            };

            $scope.goBack = function(range) {
                $('#leftlistarea').hide();
                $('#backButton').hide();
                $('#rightarea').addClass('rightarea');
                $scope.isEditing = false;
                $scope.isUpdating = false;
                $scope.getRangeKnPointsById($scope.selectedCourse.courseId);
            };

            $scope.deleteKeyPointRange = function(range) {
                dialogService.showConfirmDialog({
                    text: '你确定要删除吗？',
                    type: 'warning'
                }, function() {
                    KPMService.delPointsRangeById(range.tfId).then(function(resp) {
                        if (!!resp && !!resp.data && resp.data.code === 0) {
                            $scope.getRangeKnPointsById($scope.selectedCourse.courseId);
                        }
                    });
                });
            };

            $scope.getKeyPointsById = function(sectionId, isModify, rangeList) {
                if (!isModify) {
                    $scope.init();
                }
                KPMService.getKeyPointsById(sectionId).then(function(resp) {
                    if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object" && resp.data.msg.length > 0) {
                        $log.debug(resp.data.msg);
                        $scope.keyPointsList = angular.forEach(angular.fromJson(resp.data.msg), function(keyPoint) {
                            keyPoint.trustedKC = $sce.trustAsHtml(keyPoint.knowledgeContent);
                        });
                        $scope.keyPointsJsonShow = true;
                        $scope.keyPointsJsonError = false;
                        $scope.isUpdating = true;
                        if (!!isModify) {
                            _.each($scope.keyPointsList, function(keyPoint1) {
                                _.each(rangeList, function(keyPoint2) {
                                    if (keyPoint2.knowledgeId === keyPoint1.knowledgeId) {
                                        keyPoint1.demand = keyPoint2.demand;
                                    }
                                });
                            });
                        }
                    } else {
                        $scope.isUpdating = false;
                        $scope.isEditing = false;
                        $scope.keyPointsList = [];
                        $scope.keyPointsJsonShow = false;
                        $scope.keyPointsJsonError = true;
                    }
                });
            };

            $scope.saveRangeKnPoints = function() {
                var data = $scope.keyPointsJson;
                if (!data.beginTime || !data.endTime) {
                    dialogService.showAlertDialog({
                        title: '请选择开放时间和关闭时间！',
                        text: "时间为必填选项",
                        confirmButtonText: "我知道了",
                        type: 'warning'
                    });
                    return;
                }
                data.tfId = !!$scope.selectedRange ? $scope.selectedRange.tfId : "";
                data.subjectID = $scope.selectedCourse.courseId;
                data.sectionId = $scope.selectedSection.sectionId;
                data.msg = _.map($scope.keyPointsList, function(keyPoint) {
                    return {
                        "knowledgeId": keyPoint.knowledgeId || "",
                        "demand": keyPoint.demand || ""
                    }
                });

                KPMService.saveRangeKnPoints(data).then(function(resp) {
                    if (!!resp && !!resp.data && resp.data.code == 0) {
                        var dfdArr = [];
                        _.each(data.msg, function(item) {
                            if (!!item.demand) {
                                var dfd = KPMService.saveRangeDetail({
                                    "tfid": resp.data.tfid || data.tfId || "",
                                    "sectionId": data.sectionId || "",
                                    "knowledgeId": item.knowledgeId || "",
                                    "demand": item.demand || ""
                                });
                                dfdArr.push(dfd);
                            }
                        });
                        $q.all(dfdArr).then(function() {
                            dialogService.showAlertDialog({
                                title: '重点添加成功！',
                                confirmButtonText: "我知道了"
                            });
                        }, function() {
                            dialogService.showAlertDialog({
                                title: '重点添加失败！',
                                text: '请重新添加',
                                confirmButtonText: "我知道了",
                                type: 'warning'
                            });
                        })
                    }
                });
            };

            $scope.outputToDoc = function() {
                var tmpData = $scope.selectedRange;
                var data = {
                    'tfid':tmpData.tfId,
                    'tfName':tmpData.KeyName
                };
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0119.ashx',
                    data: data
                }).then(function(resp) {
                    var blob = new Blob([resp.data.msg], {
                        type: "text/plain;charset=utf-8"
                    });
                    saveAs(blob, tmpData.KeyName + ".doc");
                });
            };

            $scope.init = function() {
                $scope.keyPointsJson = {};
                $scope.keyPointsList = [];
                $scope.keyPointsRange = [];
                //$scope.selectedRange = {};
                $scope.keyPointsJsonShow = false;
                $scope.keyPointsRangeShow = false;
                $scope.keyPointsJsonError = false;
                $scope.keyPointsRangeError = false;
            };

            $scope.$watch('selectedCourse', function(newVal) {
                if (jQ.isEmptyObject(newVal) || newVal.courseId === '-1') {
                    $scope.chaptersList = [];
                    $scope.selectedSection = {};
                    $scope.init();
                    return;
                }
                $scope.getChapters(newVal.courseId);
                $scope.getRangeKnPointsById(newVal.courseId);
            });

            $scope.$watch('selectedSection', function(newVal) {
                if (jQ.isEmptyObject(newVal)) {
                    $scope.init();
                    return;
                }
                if ($scope.keyPointsJsonShow || $scope.keyPointsJsonError) {
                    $scope.getKeyPointsById($scope.selectedSection.sectionId);
                }
            });

            jQ('#dtpickerbegin').datetimepicker({
                lang: "ch",
                format: "Y-m-d H:i:s",
                timepicker: true,
                yearStart: 1900,
                yearEnd: 2100,
                step: 1,
                todayButton: true,
                scrollInput:false
            });

            jQ('#dtpickerend').datetimepicker({
                lang: "ch",
                format: "Y-m-d H:i:s",
                timepicker: true,
                yearStart: 1900,
                yearEnd: 2100,
                step: 1,
                todayButton: true,
                scrollInput:false
            });
        }
    ]);
