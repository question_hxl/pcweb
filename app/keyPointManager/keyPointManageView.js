/**
 * Created by Rookie_Zoe on 2015/6/14.
 */
'use strict';
angular.module('myApp.keyPointManage', [])
    .controller('keyPointManageCtrl', ['$scope', '$http', '$rootScope', '$log', '$sce', 'KPMService', 'dialogService', function($scope, $http, $rootScope, $log, $sce, KPMService, dialogService) {
        var jQ = angular.element;

        $scope.knTypes = ['知识点', '题型分析', '易错点'];
        $scope.knDemands = ['重点', '难点', '掌握', '理解'];

        $scope.coursesList = [];
        $scope.selectedCourse = {};

        $scope.chaptersList = [];
        $scope.selectedSection = {};

        $scope.keyPointsList = [];
        $scope.selectedKeyPoint = {};
        $scope.keyPointsListShow = false;
        $scope.addBtnShow = false;
        $scope.editorShow = false;
        $scope.newKeyPoint = {};

        KPMService.getCourses().then(function(resp) {
            if (!!resp && !!resp.data && !!resp.data.course) {
                $scope.coursesList = resp.data.course;
                $scope.selectedCourse = $scope.coursesList[0];
            } else {
                $scope.coursesList = [{
                    'courseId': '-1',
                    'courseName': '没有数据'
                }];
                $scope.selectedCourse = $scope.coursesList[0];
            }
        });

        $scope.getChapters = function(courseId) {
            KPMService.getChaptersById(courseId).then(function(resp) {
                if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object") {
                    $scope.chaptersList = resp.data.msg;
                    $scope.selectedSection = $scope.chaptersList[0] && $scope.chaptersList[0].section[0];
                } else {
                    $scope.chaptersList = [];
                    $scope.selectedSection = {};
                }
            });
        };

        $scope.getKeyPointsById = function(sectionId) {
            KPMService.getKeyPointsById(sectionId).then(function(resp) {
                if (!!resp && !!resp.data && !!resp.data.msg && typeof(resp.data.msg) === "object") {

                    $scope.keyPointsList = angular.forEach(angular.fromJson(resp.data.msg), function(keyPoint) {
                        keyPoint.trustedKC = $sce.trustAsHtml(keyPoint.knowledgeContent);
                    });
                    $scope.keyPointsListShow = true;
                    $scope.addBtnShow = true;
                } else {
                    $scope.keyPointsList = [];
                    $scope.keyPointsListShow = false;
                    $scope.addBtnShow = false;
                }
            });
        };

        $scope.showEditorFromKeyPoint = function(point) {
            if (!jQ.isEmptyObject(point) && !jQ.isEmptyObject($scope.selectedSection)) {
                $scope.selectedKeyPoint = point;
                $scope.setNewKeyPoint();
                $scope.editorShow = true;
                jQ(document).scrollTop(jQ(document).height());
            } else {
                $scope.selectedKeyPoint = {};
                $scope.editorShow = false;
                $log.debug('不允许操作');
            }
        };

        $scope.deleteKeyPoint = function(point) {
            dialogService.showConfirmDialog({
                text: '你确定要删除吗？',
                type: 'warning',
                confirmButtonText: "确定删除",
                cancelButtonText: "不删除",
                closeOnConfirm: false
            }, function() {
                if (!jQ.isEmptyObject(point) && !jQ.isEmptyObject($scope.selectedSection)) {
                    KPMService.delKeyPointById(point.knowledgeId).then(function(resp) {
                        if (resp.data.code === 0) {
                            $scope.abortKeyPoint();
                            $scope.getKeyPointsById($scope.selectedSection.sectionId);
                        } else if (resp.data.code === 2) {

                            dialogService.showAlertDialog({
                                title: "删除知识点!",
                                text: resp.data.msg,
                                confirmButtonText: "我知道了",
                                type: 'warning'
                            });
                            //setTimeout(function () {

                            //    dialogService.showAlertDialog({
                            //        title: "删除知识点!",
                            //        text: resp.data.msg,
                            //        confirmButtonText: "我知道了",
                            //        type: 'warning'
                            //    });
                            //}, 400);
                            
                        }
                    });
                } else {
                    $log.debug('不允许操作');
                }
            });

        };

        $scope.showEditor = function() {
            if (!jQ.isEmptyObject($scope.selectedSection)) {
                $scope.editorShow = true;
            } else {
                $scope.editorShow = false;
                $log.debug('不允许操作');
            }
        };

        $scope.addKeyPoint = function() {
            //var knContent = $scope.kinderEditor.html();
            var knContent = CKEDITOR.instances.kindereditor.getData();
            if (!jQ.isEmptyObject($scope.newKeyPoint) && !jQ.isEmptyObject($scope.selectedSection) || !!knContent) {
                $scope.newKeyPoint.sectionId = $scope.selectedSection.sectionId;
                if (!jQ.isEmptyObject($scope.selectedKeyPoint)) {
                    $scope.newKeyPoint.knowledgeId = $scope.selectedKeyPoint.knowledgeId;
                }
                if (!!knContent) {
                    $scope.newKeyPoint.knowledgeContent = knContent;
                }
                $scope.editorShow = true;

                KPMService.addKeyPoint($scope.newKeyPoint).then(function(resp) {
                    if (resp.data.code === 0) {
                        $scope.abortKeyPoint();
                        $scope.getKeyPointsById($scope.selectedSection.sectionId);
                    } else if (resp.data.code === 2) {
                        dialogService.showAlertDialog({
                            title: "保存知识点!",
                            text: resp.data.msg,
                            confirmButtonText: "我知道了",
                            type: 'warning'
                        });
                    }
                });
            } else {
                $scope.editorShow = false;
                $log.debug('不允许操作');
            }
        };

        $scope.blankKeyPoint = function() {
            $scope.newKeyPoint = {};
            //$scope.kinderEditor.html('');
            if(CKEDITOR && CKEDITOR.instances && CKEDITOR.instances.kindereditor){
                CKEDITOR.instances.kindereditor.setData('');
            }else {
                CKEDITOR.document.getById('kindereditor').setHtml('');
            }

        };

        $scope.abortKeyPoint = function() {
            $scope.selectedKeyPoint = {};
            $scope.editorShow = false;
            $scope.blankKeyPoint();
        };

        $scope.setNewKeyPoint = function() {
            $scope.newKeyPoint = {};
            $scope.newKeyPoint.knowledge = $scope.selectedKeyPoint.knowledge;
            //$scope.kinderEditor.html($scope.selectedKeyPoint.knowledgeContent);
            if(CKEDITOR && CKEDITOR.instances && CKEDITOR.instances.kindereditor){
                CKEDITOR.instances.kindereditor.setData($scope.selectedKeyPoint.knowledgeContent);
            }else {
                CKEDITOR.document.getById('kindereditor').setHtml($scope.selectedKeyPoint.knowledgeContent);
            }
            $scope.newKeyPoint.comment = $scope.selectedKeyPoint.comment;
            $scope.newKeyPoint.knowledgeNature = $scope.selectedKeyPoint.knowledgeNature;
            $scope.newKeyPoint.demand = $scope.selectedKeyPoint.demand;
        };

        $scope.$watch('selectedCourse', function(newVal) {
            if (jQ.isEmptyObject(newVal) || newVal.courseId === '-1') {
                $scope.chaptersList = [];
                $scope.selectedSection = {};
                $scope.keyPointsList = [];
                $scope.keyPointsListShow = false;
                $scope.addBtnShow = false;
                return;
            }
            $scope.editorShow = false;
            $scope.blankKeyPoint();
            $scope.getChapters(newVal.courseId);
        });

        $scope.$watch('selectedSection', function(newVal) {
            if (jQ.isEmptyObject(newVal)) {
                $scope.keyPointsList = [];
                $scope.keyPointsListShow = false;
                $scope.addBtnShow = false;
                return;
            }
            $scope.editorShow = false;
            $scope.blankKeyPoint();
            $scope.getKeyPointsById(newVal.sectionId);
        });

        //$scope.kinderEditor = KindEditor.create('#kindereditor', {
        //    width: '100%',
        //    height: '194px',
        //    resizeType: 1,
        //    items: [
        //        'source', '|', 'undo', 'redo', '|', 'template', 'cut', 'copy', 'paste',
        //        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
        //        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
        //        'superscript', 'quickformat', '/',
        //        'selectall', 'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
        //        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image',
        //        'table', 'hr', 'pagebreak', 'anchor', 'link', 'unlink'
        //    ],
        //    uploadJson: $rootScope.baseUrl + '/Interface0113.ashx'
        //});
        $scope.kinderEditor = CKEDITOR.replace('kindereditor', {
            filebrowserUploadUrl: $rootScope.baseUrl + '/Interface0001A.ashx'
        });
    }]);
