/**
 * Created by Rookie_Zoe on 2015/6/13.
 */
'use strict';
angular.module('myApp.KPMService', [])
    .service('KPMService', ['$q', '$http', '$rootScope', '$log', 'Upload', function($q, $http, $rootScope, $log, Upload) {
        return {

            getCourses: function() {
                return $http({
                    method: 'get',
                    url: $rootScope.baseUrl + '/Interface0051.ashx',
                    data: {}
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0051.ashx', resp);
                });
            },
            getChaptersById: function(courseId) {
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0052.ashx',
                    data: {
                        courseId: courseId
                    }
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0052.ashx', resp);
                });
            },

            getAllChapters: function() {
                return $http({
                    method: 'get',
                    url: $rootScope.baseUrl + '/Interface0036.ashx',
                    data: {}
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0036.ashx', resp);
                });
            },

            getKeyPointsById: function(sectionId) {
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0060.ashx',
                    data: {
                        "sectionId": sectionId
                    }
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0060.ashx', resp);
                });
            },

            addKeyPoint: function(data) {
                var req = {
                    "sectionId": data.sectionId || "",
                    "knowledgeId": data.knowledgeId || "",
                    "knowledge": data.knowledge || "",
                    "knowledgeNature": data.knowledgeNature || "",
                    "demand": data.demand || "",
                    "knowledgeContent": data.knowledgeContent || "",
                    "comment": data.comment || "",
                    "order": data.order || 0
                };
                $log.debug(req);
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0061.ashx',
                    data: req
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0061.ashx', resp);
                });
            },

            delKeyPointById: function(knowledgeId) {
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0061A.ashx',
                    data: {
                        "knowledgeId": knowledgeId
                    }
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0061A.ashx', resp);
                });
            },

            getRangeKnPointsById: function(courseId) {
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0062A.ashx',
                    data: {
                        "subjectId": courseId
                    }
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0062A.ashx', resp);
                });
            },

            getRangeDetailById: function(tfId) {
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0062B.ashx',
                    data: {
                        "tfId": tfId
                    }
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0062B.ashx', resp);
                });
            },

            delPointsRangeById: function(tfId) {
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0062C.ashx',
                    data: {
                        'tfId': tfId
                    }
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0062C.ashx', resp);
                });
            },

            saveRangeKnPoints: function(data) {
                var req = {
                    "tfid": data.tfId || "",
                    "subjectID": data.subjectID || "",
                    "sectionId": data.sectionId || "",
                    "name": data.name || "",
                    "time": parseInt((data.time || 0), 10),
                    "recommend": data.recommend || "",
                    "open": data.open || "",
                    "comment": data.comment || "",
                    "accessory": data.accessory || "",
                    "beginTime": data.beginTime || "",
                    "endTime": data.endTime || ""
                }
                $log.debug(req);
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0063.ashx',
                    data: req
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0063.ashx', resp);
                });
            },

            saveRangeDetail: function(data) {
                $log.debug(data);
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0063A.ashx',
                    data: data
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0063A.ashx', resp);
                });
            },

            getSourceList: function(data) {
                $log.debug(data);
                return $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0064.ashx',
                    data: data
                }).then(null, function(resp) {
                    $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0064.ashx', resp);
                });
            },

            uploadSource: function(data) {
                $log.debug(data);
                var defer = $q.defer();
                Upload.upload({
                    url: $rootScope.baseUrl + '/Interface0113.ashx',
                    file: data.acessory[0]
                }).success(function(resp) {
                    if (resp.error === 0) {
                        data.acessory = resp.url;
                        return $http({
                            method: 'post',
                            url: $rootScope.baseUrl + '/Interface0065.ashx',
                            data: data
                        }).then(defer.resolve, function(resp) {
                            $log.debug('[ajax error]', 'interface: ', $rootScope.baseUrl + '/Interface0065.ashx', resp);
                            defer.reject(resp);
                        });
                    }
                }).error(function(data, status) {
                    $log.debug('error status: ' + status);
                    defer.reject(data);
                });
                return defer.promise;
            }
        };
    }]);
