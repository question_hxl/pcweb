/**
 * Created by zy on 2017/7/26 0026.
 */
define([''], function () {
    return ['$http', '$scope', '$location', '$state',
        function ($http, $scope, $location, $state) {
            var examFLnkId = $location.search().examFLnkId;
            $scope.toolList = [
                {
                    group: [
                        {name: '修改分数', url: '', id: 1},
                        {name: '处理问题卷', url: '', id: 2},
                        {name: '扫描监控', url: '', id: 3},
                        {name: '修改试卷类型', url: '', id: 10},
                        {name: '添加小题属性', url: '', id: 11},
                        {name: '修改考生及考试信息', url: '', id: 12}
                    ]
                },
                {
                    group: [
                        {name: '阅卷质量监控', url: '', id: 4},
                        {name: '考后上传试卷', url: '', id: 5},
                        {name: '阅卷任务量分配', url: '', id: 6},
                        {name: '单双选切换', url: '', id: 13}
                    ]
                },
                {
                    group: [
                        {name: '剔除统计', url: '', id: 7},
                        {name: '综合学科单科报告', url: '', id: 8},
                        {name: '分数换算', url: '', id: 9}
                    ]
                }
            ];

            $scope.goDetail = function (value) {
                $state.go(value.url, {
                    examFLnkId: examFLnkId
                });
            };

            // 滑动事件
            var $cur = 1, $w = $('.moveBox').width(), $len = 0, ulList, $pages;
            $scope.scrollToLeft = function () {
                getLen();
                if ($cur === 1) {
                    ulList.stop(false, true).animate({left: '-=' + $w * ($pages - 1)});
                    $cur = $pages;
                } else {
                    ulList.stop(false, true).animate({left: '+=' + $w});
                    $cur--;
                }
            };
            $scope.scrollToRight = function () {
                getLen();
                if ($cur === $pages) {
                    ulList.stop(false, true).animate({left: 0});
                    $cur = 1;
                } else {
                    ulList.stop(false, true).animate({left: '-=' + $w});
                    $cur++;
                }
            };
            function getLen() {
                ulList = $('.moveBox ul');
                var $ulLen = ulList.length, i;
                for(i = 0; i < $ulLen; i++ ) {
                    if($len < ulList[i].children.length) {
                        $len = ulList[i].children.length;
                    }
                }
                $pages = Math.ceil($len / 3);
                ulList.css('width', $len * 313);
            }

        }]
});