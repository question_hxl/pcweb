/**
 * Created by 贺小雷 on 2017-07-11.
 */
define(['pagination', 'dragable', 'FileSaver', 'ckeditor', 'TransferPaper2Html'], function () {
	return ['$rootScope', '$http', '$scope', '$q', 'ngDialog', '$sce', '$state', '$timeout', '$location', 'paperService', 'constantService', 'utilService', 'cartService',
		function ($rootScope, $http, $scope, $q, ngDialog, $sce, $state, $timeout, $location, paperService, constantService, utilService, cartService) {
			var user = JSON.parse(sessionStorage.getItem('currentUser'));
			$scope.paperTitle = '';
			$scope.paperSubtitle = '';
			$scope.paperTime = 45;
			$scope.paperScore = 100;
			$scope.exType = '';
			$scope.isShowKlg = false;
			$scope.isShowAnswer = false;
			$scope.showTypes = utilService.showTypeList;
			$scope.knowledgeList = null;
			$scope.pharseId = user.pharseId;
			$scope.subjectId = user.subjectId;

			var search = $location.search();
			if(search.bankPosition && search.examType) {
                $scope.isModified = true;
			} else {
                $scope.isModified = false;
			}
			var examId = $scope.examId = search.examId;
			$scope.bankPosition = search.bankPosition;

			initPaperData(examId);

			//修改Dtype时，检验长度（<=15）、唯一性；
			$scope.checkDtypeLegal = function (text, group) {
				if(!text) {
					return false;
				}
				if (text.length > 15) {
					constantService.showSimpleToast('分组名称不能超过15个字符！');
					return false;
				} else {
					var toCheckList = _.filter($scope.groups, function (g) {
						return g.Dtype !== group.Dtype;
					});
					var isExist = _.find(toCheckList, function (g) {
						return g.Dtype === text;
					});
					if (isExist) {
						constantService.showSimpleToast('分组名称不能重复，请重新输入！');
						return false;
					} else {
						group.Dtype = text;
						return true;
					}
				}
			};

			$scope.selectQst = function(data, group){
				$scope.currentQuestion = data;
				$scope.currentGroup = group;
			};

			$scope.addFav = function(question){ // type 1  试卷  2 题目
				$http.post($rootScope.baseUrl + '/Interface0204.ashx', {
					resourceFlnkID : question.FLnkID,
					type : 2
				}).success(function(data){
					question.IsCollection = '1';
					constantService.alert('收藏成功！');
				})
			};

			$scope.removeFav = function(question){
				$http.post($rootScope.baseUrl + '/Interface0205.ashx', {
					resourceFlnkID : question.FLnkID
				}).success(function(data){
					question.IsCollection = '0';
					constantService.alert('取消收藏成功！');
				});
			};

			$scope.modifyCurrentQuestionScore = function(){
				if($scope.currentQuestion){
					isIegal4Save();
					if($scope.currentQuestion.isSub) {
						//如果是子题，则同步修改父题分数
						var parent = _.find($scope.currentGroup.question, function(q){
							return q.QFLnkID === $scope.currentQuestion.mainId;
						});
						parent.score = paperService.getParentScore(parent);
					}else if($scope.currentQuestion.isMain && $scope.currentQuestion.sub && $scope.currentQuestion.sub.length > 0) {
						//如果是父题，则同步修改子题分数
						var scoreList = paperService.assignScore($scope.currentQuestion.score, $scope.currentQuestion.sub.length);
						_.each($scope.currentQuestion.sub, function(s, index){
							s.score = scoreList[index];
						});
					}
					var group = _.find($scope.groups, function(group){
						return group.Dtype === $scope.currentGroup.Dtype;
					});
					group.QTypeScores = paperService.calcGroupScore(group);
					$scope.paperScore = paperService.getTotalScore($scope.groups);
					$scope.currentQuestion.isScoreModified = true;
				}
			};

			$scope.changeCurrentQust = function (currentQuestion) {
                if(!(((currentQuestion.score * 10) % 10 === 5) || ((currentQuestion.score * 10) % 10 === 0))){
                    constantService.alert('分数仅能为0.5整数倍!');
                    $scope.currentQuestion.score = 0;
                    var group = _.find($scope.groups, function(group){
                        return group.Dtype === $scope.currentGroup.Dtype;
                    });
                    group.QTypeScores = paperService.calcGroupScore(group);
                    $scope.paperScore = paperService.getTotalScore($scope.groups);
                }
                $scope.paperScore = paperService.getTotalScore($scope.groups);
            };

			$scope.modifyGroupScore = function(group){
				if(group){
					$scope.isModifyGroupScore = true;
					var groupsQsts = [];
					_.each(group.question, function(q){
						if(q.sub && q.sub.length > 0) {
							groupsQsts = groupsQsts.concat(q.sub);
						}else {
							groupsQsts.push(q);
						}
					});
					//先为子题分配分数，再重新计算父题分数
					var scoreList = paperService.assignScore(+group.QTypeScores, groupsQsts.length);
					_.each(groupsQsts, function(item, index){
						item.score = scoreList[index];
					});
					_.each(group.question, function(q){
						if(q.sub && q.sub.length > 0) {
							var qscore = 0;
							_.each(q.sub, function(s){
								qscore += s.score;
							});
							q.score = qscore;
						}
					});
				}
				$scope.paperScore = paperService.getTotalScore($scope.groups);
			};

			$scope.changeGroupScore = function (group) {
                if(!(((group.QTypeScores * 10) % 10 === 5) || ((group.QTypeScores * 10) % 10 === 0))){
                    constantService.alert('分数仅能为0.5整数倍!');
                    group.QTypeScores = 0;
                    $scope.paperScore = paperService.getTotalScore($scope.groups);
                }
            };

			$scope.modifyKlg = function () {
				getKlgList().then(function(data){
					!$scope.knowledgeList && ($scope.knowledgeList = data);
					ngDialog.open({
						template: '/klgEditor.html',
						className: 'ngdialog-theme-default',
						appendClassName: 'dialog-edit-paper',
						closeByDocument: false,
						scope: $scope
					});
				}, function(){
					constantService.alert('未获取到知识点！');
				});
			};
			$scope.doModifyKlg = function (data) {
				var klgIds = _.pluck(data, 'unitId').join(',');
				$http.post($rootScope.baseUrl + '/Interface0206.ashx', {
					qFlnkId: $scope.currentQuestion.QFLnkID,
					strKnowledgeId: klgIds
				}).then(function (res) {
					if (+res.data.code === 0) {
						$scope.currentQuestion.knowledge = _.map(data, function (item) {
							return {
								id: item.unitId,
								name: item.unitName
							};
						});
						$scope.currentQuestion.knowledges = '';
						_.each($scope.currentQuestion.knowledge,function (item, index) {
							if(index===0){
								$scope.currentQuestion.knowledges += item.name
							}else {
								$scope.currentQuestion.knowledges += ','+item.name
							}
						});
						constantService.showSimpleToast('修改考点成功！');
					} else {
						constantService.showSimpleToast('修改考点失败！');
					}
				}, function () {
					constantService.showSimpleToast('修改考点失败！');
				});
			};

			$scope.saveEditQst = function(data){
				ngDialog.closeAll();
				var params = {
					msg : [],
					qId : data.QID,
					title : data.title,
					optionA : data.optionOne,
					optionB : data.optionTwo,
					optionC : data.optionThree,
					optionD : data.optionFour,
					answer : data.answer,
					mp3 : data.mp3
				};
				if($scope.toEditQuestion.isSub){
					var mainQst = paperService.getMainQuestionBySub($scope.toEditQuestion, $scope.currentGroup);
					params.msg.push({
						qFlnkId : mainQst.FLnkID,
						type : '0'
					});
					var subIds = _.map(mainQst.sub, function(item){
						var type = item.FLnkID === $scope.toEditQuestion.FLnkID ? '1' : '0';
						return {
							qFlnkId : item.FLnkID,
							type : type
						};
					});
					params.msg = params.msg.concat(subIds);
				} else if(!_.isEmpty($scope.toEditQuestion.sub)){
					params.msg.push({
						qFlnkId : $scope.toEditQuestion.FLnkID,
						type : '1'
					});
					var subIds = _.map($scope.toEditQuestion.sub, function(item){
						return {
							qFlnkId : item.FLnkID,
							type : '0'
						};
					});
					params.msg = params.msg.concat(subIds);
				} else {
					params.msg.push({
						qFlnkId : $scope.toEditQuestion.FLnkID,
						type : '1'
					});
				}
				$http.post($rootScope.baseUrl + '/Interface0206A.ashx', params).then(function(res){
					if(res.data.code === 0){
						var ids = res.data.msg.split(',');
						if($scope.toEditQuestion.isSub){
							var mainQst = paperService.getMainQuestionBySub($scope.toEditQuestion, $scope.currentGroup);
							_.extend(mainQst, {
								QFLnkID : ids[0],
								fLnkId : ids[0],
								FLnkID : ids[0]
							});
							_.each(mainQst.sub, function(item, index){
								_.extend(item, {
									QFLnkID : ids[index + 1],
									fLnkId : ids[index + 1],
									FLnkID : ids[index + 1],
									mainId : ids[0]
								});
							});
						} else if($scope.toEditQuestion.isMain){
							_.extend($scope.toEditQuestion, {
								QFLnkID : ids[0],
								fLnkId : ids[0],
								FLnkID : ids[0]
							});
							_.each($scope.toEditQuestion.sub, function(item, index){
								_.extend(item, {
									QFLnkID : ids[index + 1],
									fLnkId : ids[index + 1],
									FLnkID : ids[index + 1],
									mainId : ids[0]
								});
							});
						} else {
							_.extend($scope.toEditQuestion, {
								QFLnkID : ids[0],
								fLnkId : ids[0],
								FLnkID : ids[0]
							});
						}
						constantService.showConfirm('题目已修改成功，是否需要同步修改知识点？', ['确定'], function(){
							$scope.modifyKlg();
						});
					}
				})
			};

			$scope.editQuestion = function(question){
				$scope.toEditQuestion = question;
				ngDialog.open({
					template : 'editQuestion',
					plain : false,
					scope : $scope,
					className : 'ngdialog-theme-default',
					appendClassName : 'edit-q-dialog'
				});
			};

			$scope.changeQuestion = function(cb, group){
				var args = Array.prototype.slice.call(arguments, 1), curGroup = arguments;
				ngDialog.open({
					template : 'changeQuestion',
					plain : false,
					scope : $scope,
					className : 'ngdialog-theme-default',
					appendClassName : 'replace-q-dialog',
					controller: function($scope){
						return function(){
							$scope.qBelong = 1;
							$scope.level = 1;
							$scope.paginationConf = {
								currentPage: 1,
								totalItems: 0
							};
							$scope.search = function(){
								searchPage($scope.KeyWord, $scope.qBelong, $scope.level);
							};
							$scope.prevPage = function(){
								if($scope.paginationConf.currentPage === 1) {
									return;
								}
								$scope.paginationConf.currentPage -= 1;
								searchPage($scope.KeyWord, $scope.qBelong, $scope.level, $scope.paginationConf.currentPage);
							};
							$scope.nextPage = function(){
								if($scope.paginationConf.currentPage === $scope.paginationConf.totalItems) {
									return;
								}
								$scope.paginationConf.currentPage += 1;
								searchPage($scope.KeyWord, $scope.qBelong, $scope.level, $scope.paginationConf.currentPage);
							};

							$scope.searchItemClick = function(question){
                                args = Array.prototype.slice.call(curGroup, 1);
								args.unshift(question);
								cb.apply(this, args);
                                $scope.isModified = true;
							};

							function searchPage(key, belong, level, page){
								getQuestions(key, belong, level, page, group).then(function(res){
									var temporaryList = res.msg;
									_.each($scope.groups, function (groupItem) {
										_.each(groupItem.question, function (questionItem) {
											_.each(temporaryList, function (searchItem) {
                                                searchItem.knowledge = [];
												_.each(searchItem.kFlnkIDs, function (idItem) {
                                                    searchItem.knowledge.push({id : idItem});
                                                });
												if(searchItem.FLnkID.toUpperCase( ) === questionItem.FLnkID.toUpperCase( )) {
                                                    searchItem.tempStatus = 1
												}
                                            })
                                        });
                                    });
                                    $scope.searchList = _.filter(temporaryList, function (tempItem) {
										return +tempItem.tempStatus !== 1
                                    });
									$scope.paginationConf.totalItems = res.PageNum;
								}, function(){
									$scope.searchList = [];
								});
							}
						}
					}
				});
			};

			$scope.doReplace = function(data){
				if(checkQstRepeat(data)) {
					constantService.alert('不能重复添加相同题目！');
					return;
				}
				if(!$scope.knowledgeList) {
                    getKlgList().then(function(res) {
                        !$scope.knowledgeList && ($scope.knowledgeList = res);
                        matchKnown(data);
                        var qst = paperService.parseQstForPaper(data);
                        _.extend($scope.currentQuestion, qst);
                        ngDialog.closeAll();
                    });
				} else {
                    matchKnown(data);
                    var qst = paperService.parseQstForPaper(data);
                    _.extend($scope.currentQuestion, qst);
                    ngDialog.closeAll();
				}

			};

			$scope.doInsert = function(data){
				if(checkQstRepeat(data)) {
					constantService.alert('不能重复添加相同题目！');
					return;
				}
				if(!$scope.knowledgeList) {
                    getKlgList().then(function(res) {
                        !$scope.knowledgeList && ($scope.knowledgeList = res);
                        matchKnown(data);
                        var qst = paperService.parseQstForPaper(data);
                        //为题目设置默认分数0分，如果是子题，设置默认模式为内部编号模式
                        if(qst.sub && qst.sub.length > 0) {
                            qst.score = 0;
                            qst.mode = qst.Mode = 'B';
                            _.each(qst.sub, function(s){
                                s.score = 0;
                            });
                        }else {
                            qst.score = 0;
                        }
                        var currentGroup = $scope.currentGroup;
                        var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                        currentGroup.question.splice(index + 1, 0, qst);
                        ngDialog.closeAll();
                    });
				}else {
                    matchKnown(data);
                    var qst = paperService.parseQstForPaper(data);
                    //为题目设置默认分数0分，如果是子题，设置默认模式为内部编号模式
                    if(qst.sub && qst.sub.length > 0) {
                        qst.score = 0;
                        qst.mode = qst.Mode = 'B';
                        _.each(qst.sub, function(s){
                            s.score = 0;
                        });
                    }else {
                        qst.score = 0;
                    }
                    var currentGroup = $scope.currentGroup;
                    var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                    currentGroup.question.splice(index + 1, 0, qst);
                    ngDialog.closeAll();
				}
			};
			
			function matchKnown(data) {
                if(data.knowledge.length && !data.knowledge[0].name) {
                    _.each(data.knowledge, function (itemSelf) {
                        _.each($scope.knowledgeList, function (item) {
                            if(item.unitId === itemSelf.id) {
                                itemSelf.name = item.unitName;
                                return
                            }
                            _.each(item.unit, function (itemDetail) {
                                if(itemDetail.unitId === itemSelf.id) {
                                    itemSelf.name = itemDetail.unitName;
                                    return
                                }
                                _.each(itemDetail.unit, function (knownList) {
                                    if(knownList.unitId === itemSelf.id) {
                                        itemSelf.name = knownList.unitName;
                                    }
                                });
                            });

                        });
                    });
                }
            }
			$scope.addQstForGroup = function(data, group){
				if(checkQstRepeat(data)) {
					constantService.alert('不能重复添加相同题目！');
					return;
				}
				var qst = paperService.parseQstForPaper(data);
				//为题目设置默认分数0分，如果是子题，设置默认模式为内部编号模式
				if(qst.sub && qst.sub.length > 0) {
					qst.score = 0;
					qst.mode = qst.Mode = 'B';
					_.each(qst.sub, function(s){
						s.score = 0;
					});
				}else {
					qst.score = 0;
				}
				group.question.push(qst);
				ngDialog.closeAll();
			};

			$scope.autoChooes = function(){
				if(!$scope.currentQuestion){
					constantService.alert('请选择要自动换题目的题目');
					return false;
				}
				var originalId;
				if($scope.currentQuestion.originalId) {
					originalId = $scope.currentQuestion.originalId;
				}else {
					originalId = $scope.currentQuestion.originalId = $scope.currentQuestion.QFLnkID;
				}
				$http.post($rootScope.baseUrl + '/Interface0198B.ashx', {
					qFLnkID : originalId, // 试题的ID 试题的等级
					diff : $scope.currentQuestion.DifficultLevel,
					excludeIds: getParentQuestionIds($scope.groups),
					qtypeid: $scope.currentQuestion.qtypeId
				}).success(function(res){
					if(!_.isArray(res.msg)){
						constantService.alert('未查询到适合的题目');
						return false;
					}
					for(var i = 0; i< res.msg.length; i++) {
						if(!checkQstRepeat(res.msg[i])) {
							var toInsertQst = paperService.parseQstForPaper(res.msg[i]);
                            toInsertQst.knowledge = $scope.currentQuestion.knowledge;  //自动换题知识点与原来的题一样
							_.extend($scope.currentQuestion, toInsertQst);
                            $scope.isModified = true;
							return;
						}
					}
					constantService.alert('未查询到适合的题目');
				}).error(function(){
					constantService.alert('未查询到适合的题目！');
				});
			};

			$scope.delArr = function(){
				if(!$scope.currentQuestion){
					constantService.alert('请选择要删除的题目');
					return false;
				}
                constantService.confirm('提示','确定删除本题？删除本题之后无法恢复，您可以通过插入题目来重新添加题目。', ['删除'], function(){
                    var currentGroup = $scope.currentGroup;
                    var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                    currentGroup.question.splice(index, 1);
                    currentGroup.QNumber = +currentGroup.QNumber - 1;
                    $scope.currentQuestion = '';
                    $scope.isModified = true;
                });
			};

			$scope.changeMode = function(){
				if($scope.currentQuestion.mode === 'A'){
					$scope.currentQuestion.mode = 'B';
				} else if($scope.currentQuestion.mode === 'B'){
					$scope.currentQuestion.mode = 'A';
				}
                $scope.isModified = true;
			};

			$scope.getGroupLen = function(group){
				var len = 0;
				_.each(group.question, function(q){
					if((q.mode ==='A' || q.Mode === 'A') && q.sub && q.sub.length > 0) {
						len += q.sub.length;
					}else {
						len += 1;
					}
				});
				return len;
			};

			$scope.deleteGroup = function(index){
                constantService.confirm('提示','确定要删除本分组吗？删除之后无法恢复，也无法再添加分组。', ['删除'], function(){
                    $scope.groups.splice(index, 1);
                    $scope.isModified = true;
                });
			};

			$scope.jumpToQ = function(q){
				var topL1 = $("#" + q.orders + "").offset().top;
				$(document).scrollTop(topL1);
			};

			$scope.savePaper = function(){
				var allqst = paperService.getAllQuestionInPaper($scope.groups);
				if(checkExmType(allqst)){
					constantService.alert('机房评测/课堂评测中不能加入非选择题!');
					return;
				}
				if(checkScore(allqst)){
					return;
				}
				if(!$scope.paperTitle){
					constantService.alert('请设置试卷名称！');
					return false;
				}
				if(!$scope.paperTime || $scope.paperTime <= 0){
					if(!$scope.times){
						constantService.alert('请输入考试时间！');
					} else if($scope.times <= 0){
						constantService.alert('考试时间格式不正确！');
					}
					return false;
				}
				if(!$scope.paperScore){
					constantService.alert('请设置题目分数！');
					return false;
				}
				if(!(/^\d+(\.)?$/.test(+$scope.paperScore))) {
					constantService.alert('总分必须为整数');
					return;
				}
				if(+$scope.paperScore > 300){
					constantService.alert('您设置的试卷分数过高，请调整分数！');
					return false;
				}
				var isGroupEmpty = !!_.find($scope.groups, function(group){
					return !group.question || group.question.length === 0;
				});
				if(isGroupEmpty) {
					constantService.showConfirm('检查到试卷中存在部分分组没有题目，如果继续保存，这些分组将会丢失，是否继续保存试卷？', ['继续保存'], doSavePaper);
				}else {
					doSavePaper();
				}
			};

			$scope.downloadPaper = function(){
				constantService.showConfirm('请确保下载之前先保存试卷！', ['继续下载'], function(){
					$http.post($rootScope.baseUrl + '/Interface0181.ashx', {
						examId: examId
					}).then(function(res){
						var paperTransfer = new $.PaperTransfer(res.data);
						paperTransfer.exportToDoc();
					});
				});
			};

			//监听问题列表变化
			$scope.$watch('groups', function(){
				if(_.isArray($scope.groups)){
					$scope.paperScore = paperService.getTotalScore($scope.groups);
					paperService.setQuestionsOrder($scope.groups);
					generateSubQsts($scope.groups);
				}
			}, true);

			function initPaperData(examId) {
				//如果存在examId，则是编辑试卷，否则为新建试卷
				if (examId) {
					paperService.getPaperDataById(examId).then(function (res) {
						$scope.exType = res.data.examType;
						$scope.paperTime = +res.data.spendtime; // 获取题目做题时间
						$scope.groups = _setExplain(res.data.msg);
						$scope.paperTitle = res.data.examName || '';
						$scope.paperSubtitle = res.data.SmallTitle || '';
						$scope.gradeId = res.data.GradeId;
						$scope.pharseId = res.data.PharseId;
						$scope.subjectId = res.data.SubjectId;
						$scope.paperScore = paperService.getTotalScore(res.data.msg);
						//setHomeWorkScore();
						generateSubQsts($scope.groups);
					}, function (res) {
						constantService.alert('获取试卷内容失败！');
					});
				} else {
					var cachedData = JSON.parse(localStorage.getItem('answerList'));
					$scope.exType = search.examType || '补偿出卷';
					$scope.paperTime = 45;
					paperService.setQuestionsOrder(cachedData);
					$scope.groups = _setExplain(paperService.parsePaperData(cachedData));
					$scope.paperTitle = search.examTitle || '';
					$scope.paperSubtitle = '';
					$scope.paperScore = 0;
					$scope.gradeId = search.gradeid;
					//setHomeWorkScore();
					generateSubQsts($scope.groups);
				}
			}

			function generateSubQsts(groups){
				_.each(groups, function(g){
					g.aloneOrderQsts = [];
					_.each(g.question, function(q){
						if(q.mode === 'A' || q.Mode === 'A') {
							g.aloneOrderQsts = g.aloneOrderQsts.concat(q.sub);
						}else {
							g.aloneOrderQsts.push(q);
						}
					});
				})
			}

			function _setExplain(list) {
				_.map(list, function (item, index) {
					if (!item.Dtype) {
						item.Dtype = item.QTypeName;
					}
					if (typeof item.isCustomizeExplain === 'undefined') {
						item.isCustomizeExplain = !!item.explain;
					}
					if (!item.explain || !item.isCustomizeExplain) {
						item.explain = item.explain ? item.explain : ('（共' + item.question.length + '小题）');
					}
				});
				return list;
			}

			function setHomeWorkScore() {
				if ($scope.exType === '作业') {
					var paperScore = 0;
					_.each($scope.groups, function (group) {
						var groupScore = 0;
						_.each(group.question, function (q) {
							if (q.mode === 'A') {
								_.each(q.sub, function (s) {
									s.score = 1;
									groupScore += 1;
								});
							} else {
								q.score = 1;
								groupScore += 1;
							}
						});
						paperScore += groupScore;
						group.QTypeScores = groupScore;
					});
					$scope.paperScore = paperScore;
				}
			}

			function isIegal4Save(){
				if($scope.currentQuestion.isMain && $scope.currentQuestion.sub && $scope.currentQuestion.sub.length > 0){
					var score = 0;
					var temscore = Math.floor($scope.currentQuestion.score * 10 / $scope.currentQuestion.sub.length);
					if(!(((temscore) % 10 === 5) || ((temscore) % 10 === 0))){
						score = Math.floor(temscore / 10);
					} else {
						score = temscore / 10;
					}
					for(var i = 0; i < $scope.currentQuestion.sub.length; i++){
						$scope.currentQuestion.sub[i].score = score;
					}
				}
			}

			function getKlgList(){
				var defer = $q.defer();
				if($scope.knowledgeList) {
					defer.resolve($scope.knowledgeList);
				}else {
					$http({
						method: 'post',
						url: $rootScope.baseUrl + '/Interface0213.ashx',
						data: {
							subjectId: $scope.subjectId,
							pharseId: $scope.pharseId
						}
					}).success(function (res) {
						if (!_.isArray(res.msg)) {
							defer.reject([]);
						} else {
							defer.resolve(res.msg);
						}
					}).error(function(){
						defer.reject([]);
					});
				}
				return defer.promise;
			}

			function getQuestions(keyWord, sourceType, level, page, group){
				var defer = $q.defer();
				$http.post($rootScope.baseUrl + '/Interface0194A.ashx',{
					type : ($scope.currentQuestion && $scope.currentQuestion.qtypeId) || (group && group.QTypeId) || '',
					chapterId : "",
					province : '全部',
					year : '全部',
					level : '全部',
					name : keyWord,
					startTime : "",
					stoptime : "",
					source : '',
					sourceType : sourceType,
					knowlegeName : '',
					diff : level,
					CurPage : page || 1,
					gradeId : $scope.gradeId,
					pageSize : 10,
					subjectId:$scope.subjectId,
					pharse:$scope.pharseId
				}).then(function(res){
					if(+res.data.Count > 0) {
						defer.resolve(res.data);
					}else {
						defer.reject();
					}
				}, defer.reject);
				return defer.promise;
			}

			function checkQstRepeat(question){
				for(var i = 0, len = $scope.groups.length; i < len; i++) {
					var groupQsts = $scope.groups[i].question;
					var qInGroup = _.find(groupQsts, function(q){
						return q.QFLnkID === (question.FLnkID || question.QFLnkID);
					});
					if(qInGroup) {
						return true;
					}
				}
				return false;
			}

			function checkExmType(list){
				if($scope.exType === '课堂评测'){
					for(var i = 0; i < list.length; i++){
						if(list[i].sub && list[i].sub.length > 0){
							for(var j = 0; j < list[i].sub.length; j++){
								if(list[i].sub[j].isObjective === '0'){
									return true;
								}
							}
						} else if(list[i].isObjective === '0'){
							return true;
						}
					}
					return false;
				} else {
					return false;
				}
			}

			function checkScore(list){
				for(var i = 0; i < list.length; i++){
					if(list[i].sub && list[i].sub.length > 0){
						for(var j = 0; j < list[i].sub.length; j++){
							if(isIegal(list[i].sub[j].score)){
								return true;
							}
						}
					} else {
						if(isIegal(list[i].score)){
							return true;
						}
					}
				}
				return false;
			}

			function isIegal(score){
				if(!(/^\d+(\.\d+)?$/.test(score))){
					constantService.alert('分数仅能为非负数字!');
					return true;
				}
				if(score > 99) {
					constantService.alert('存在题目分数过高，请检查试卷！');
					return true;
				}
				if(score <= 0){
                    constantService.alert('分数必须大于0，请检查试卷！');
                    return true;
				}
				if(!(((score * 10) % 10 === 5) || ((score * 10) % 10 === 0))){
					constantService.alert('分数仅能为0.5整数倍!');
					return true;
				}
			}

			function getParentQuestionIds(groups){
				var ids = [];
				_.each(groups, function(g){
					_.each(g.question, function(q){
						if(q.QID) {
                            ids.push(q.QID);
						}
					});
				});
				return ids.join(',');
			}

			function doSavePaper(){
				var param = {
					examName : $scope.paperTitle || '',
					smallTitle : $scope.paperSubtitle || '',
					time : $scope.paperTime,
					scores : $scope.paperScore,
					examFLnkID : examId || 0,
					exType : $scope.exType || '',
					msg : paperService.getQstsForSubmit($scope.groups),
					gradeId : $scope.gradeId,
					isTeacher : '1'
				};
				if(!examId) {
					param.fLnkID = (+$scope.bankPosition === 3 ? user.fid : (+$scope.bankPosition === 2 ? user.schoolFId : ''));
				}
				$http.post($rootScope.baseUrl + '/Interface0219.ashx', param).then(function(res){
					if(res.data.code === 0) {
                        // var cachedData = JSON.parse(localStorage.getItem('answerList')),baskets = [], basketUsed = [];
                        // _.each(cachedData, function (item) {
                        //     _.each(item.question, function (qst) {
                        //         baskets.push(qst.FLnkID);
                        //     })
                        // });
                        // _.each(baskets, function (item) {
                        //     for(var i = 0, len = $scope.groups.length; i < len; i++) {
                        //         var groupQsts = $scope.groups[i].question;
                        //         _.each(groupQsts, function(q){
                        //             if(q.QFLnkID === item){
                        //                 basketUsed.push(item)
                        //             }
                        //         });
                        //     }
                        // });
						localStorage.removeItem('answerList');
                        if(search.type === '1') {
                            constantService.confirm('提示','恭喜您，组卷成功！是否需要删除试题篮中已使用的题目？', ['删除'], function(){
								cartService.clearDatabaseCart().then(function(){
									$rootScope.$emit('baseketList_number_change');
								});
                            });
						}
						$location.path('/paperComposing').search({
							'examId' : res.data.msg
						});
					}else {
						constantService.alert('保存试卷失败，请重试！');
					}
				});
			}

            function clearCart(qIds){
                cartService.removeCart(qIds.join(',')).then(function(res){
                    if(res.data.code === 0) {
                        $scope.$emit('baseketList_number_change');
                    }
                });
            }
			/**
			 * 拖动处理
			 */
			var groupId = '';
			//dragable 拖拽插件处理
			var dragable = $('.paperComposing-new').dragable({
				ele : '.q-meta',
				container : '.group-container',
				dragstartCb : function(e, data){
					groupId = $(e.target).parents('.group-container')[0].id;
					dragable.setDataTransfer('currentId', e.currentTarget.id);
				},
				dragendCb : function(ev, $target){
					var targetId = $target[0].id;
					var data = dragable.getDataTransfer("currentId");
					var group = _.find($scope.groups, function(group){
						return groupId === group.Dtype;
					});
					var questions = group.question;

					var targetQuestion = _.find(questions, function(question){
							return question.QFLnkID === targetId;
						}),
						targetIndex = paperService.getQuestionIndexInGroup(targetQuestion, group);
					var originQuestion = _.find(questions, function(question){
							return question.QFLnkID === data;
						}),
						originIndex = paperService.getQuestionIndexInGroup(originQuestion, group);
					$scope.$apply(function(){
						if(targetIndex > originIndex){
							questions.splice(targetIndex + 1, 0, originQuestion);
							questions.splice(originIndex, 1);
						} else if(targetIndex < originIndex){
							questions.splice(originIndex, 1);
							questions.splice(targetIndex + 1, 0, originQuestion);
						}
					});
				}
			});
			/**
			 * 处理题型分组排序
			 */
			var dragableGroup = $('.paperComposing-new').dragable({
				ele : '.g-item',
				container : '.paper-brief',
				dragstartCb : function(e, data){
					dragableGroup.setDataTransfer('currentGroup', e.currentTarget.id);
				},
				dragendCb : function(ev, $target){
					var targetId = $target[0].id;
					var data = dragableGroup.getDataTransfer("currentGroup");
					var targetGroup = _.find($scope.groups, function(group){
						return targetId === group.Dtype;
					});
					var originGroup = _.find($scope.groups, function(group){
						return data === group.Dtype;
					});
					var targetIndex = paperService.getGroupIndexInGroups(targetGroup, $scope.groups),
						originIndex = paperService.getGroupIndexInGroups(originGroup, $scope.groups);
					$scope.$apply(function(){
						if(targetIndex > originIndex){
							$scope.groups.splice(targetIndex + 1, 0, originGroup);
							$scope.groups.splice(originIndex, 1);
						} else if(targetIndex < originIndex){
							$scope.groups.splice(originIndex, 1);
							$scope.groups.splice(targetIndex + 1, 0, originGroup);
						}
					});
				}
			});

            $scope.$on('$stateChangeStart', function(event, toState, toParams) {
                if(toState.name !== 'myApp.paperComposing' && $scope.isModified) {
                    var isLeave = confirm('确定离开试卷编辑页面？如果尚未保存试卷，已制作的内容将无法恢复。');
                    if(!isLeave) {
                        event.preventDefault();
                    }else {
                        window.onbeforeunload = null;
                    }
                }else {
                    window.onbeforeunload = null;
                }
            });
            window.onbeforeunload = function(){
                if($scope.isModified) {
                    return true;
                }
            };
		}];
});