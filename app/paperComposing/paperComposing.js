define(['jquery.datetimepicker', 'underscore-min', 'star-rating', 'pagination', 'dragable', 'FileSaver', 'ckeditor', 'TransferPaper2Html'], function(){
    return ['$rootScope', '$http', '$scope', '$q', 'ngDialog', '$sce', '$state', '$timeout', '$location',
        'paperService', 'constantService', 'utilService',
        function($rootScope, $http, $scope, $q, ngDialog, $sce, $state, $timeout, $location, paperService, constantService, utilService){
            var USER = JSON.parse(sessionStorage.getItem('currentUser'));
            var subjectname = USER.subjectName;
            var subjectIdList = [{subjectName:'信息技术',subjectId:'10'},{subjectName:'化学',subjectId:'9'},{subjectName:'地理',subjectId:'5'},
                {subjectName:'语文',subjectId:'1'},{subjectName:'物理',subjectId:'8'},{subjectName:'英语',subjectId:'3'},
                {subjectName:'英语',subjectId:'7'},{subjectName:'历史',subjectId:'4'},{subjectName:'英语复习',subjectId:'32'},
                {subjectName:'数学',subjectId:'2'},{subjectName:'政治',subjectId:'6'}];
            $scope.subjectId = '';
            _.each(subjectIdList,function (res) {
                if(res.subjectName === subjectname){
                    $scope.subjectId = res.subjectId
                }
            });
            var action = $location.search().action; //统一获取地址栏参数
            var examName;
            var PARPER_BIG_TITLE = '编辑大标题';
            var examFLnkID = 0;
            var examId = $location.search().examId;
            var PAPER_SMALL_TITLE = '编辑小标题';
            var firstFlag = true;
            $scope.isnew = false;
            $scope.times = '45';
            $scope.showTypes = utilService.showTypeList;
            $scope.qBelong = 1;
            //试卷保存位置
            var qbank = undefined;//默认给 3 个人卷库 2 校本卷库
            if(action == 1){ // 补偿出题
                var answerList = localStorage.getItem('answerList');
                $scope.QuestionListBox = answerList;
                answerList = angular.fromJson(answerList);
                $scope.exType = '补偿出卷';
                $scope.gradeId = $location.search().gradeid;
                $scope.isShowText = true;
                if(_.isArray(answerList)){
                    paperService.setQuestionsOrder(answerList);
                    $scope.askBoxTypeList = $scope.QuestionListBox = paperService.parsePaperData(answerList);
                    $scope.QuestionListBox = _setExplain($scope.QuestionListBox);
                }
            } else if(action == 3){ //卷库过来的
                $scope.isnew = true;
                $scope.gradeId = $location.search().gradeid;
                paperService.getPaperDataById(examId).then(function(res){
                    $scope.answer = false;
                    $scope.exType = res.data.examType;
                    $scope.kd = false;
                    $scope.times = res.data.spendtime; // 获取题目做题时间
                    $scope.QuestionListBox = res.data.msg;
                    $scope.askBoxTypeList = res.data.msg;
                    $scope.testpaperName = res.data.examName || PARPER_BIG_TITLE;
                    $scope.smalltitle = res.data.SmallTitle || PAPER_SMALL_TITLE;
                    $scope.score = paperService.getTotalScore(res.data.msg);
                    $scope.QuestionListBox = _setExplain($scope.QuestionListBox);
                    setHomeWorkScore();
                }, function(res){
                    constantService.alert('获取试卷内容失败！');
                });
            } else {//章节出题
                qbank = +localStorage.getItem('qbank');
                var parsejson = JSON.parse(localStorage.getItem('askList'));
                var pardatas = parsejson;
                var askBox = pardatas;
                $scope.exType = askBox[0].exType;
                $scope.gradeId = askBox[0].gradeId;
                if(askBox.length == 0){
                    $state.go('myApp.questionIndex');
                    return false;
                }
                $scope.isShowText = false;
                examName = askBox[0].testpaperName; // 试卷名称
                //$scope.$watch('testpaperName', function () {
                //	examName = $scope.testpaperName;
                //});
                $scope.smalltitle = PAPER_SMALL_TITLE;
                $scope.testpaperName = examName || PAPER_SMALL_TITLE;
                var data = pardatas;
                var url = $rootScope.baseUrl + '/Interface0179.ashx';
                if(data.length > 0 && data[0].KnowledgeId){
                    url = $rootScope.baseUrl + '/Interface0211B.ashx';
                }

                $http({
                    method : 'post',
                    url : url,
                    data : {
                        msg : data
                    }
                }).success(function(res){
                    $scope.answer = false;
                    $scope.kd = false;
                    var paperData = res.msg;
                    paperService.setQuestionsOrder(paperData);
                    $scope.QuestionListBox = $scope.askBoxTypeList = paperService.parsePaperData(paperData);
                    $scope.QuestionListBox = _setExplain($scope.QuestionListBox);
                    setHomeWorkScore();
                });
            }

            function _setExplain(list){
                _.map(list, function(item, index){
                    if(!item.Dtype) {
                        item.Dtype = item.QTypeName;
                    }
                    item.explain = item.explain ? item.explain : ('（共' + item.QNumber + '小题）');
                });
                return list;
            }

            $scope.score = 0;

            function setHomeWorkScore(){
                if($scope.exType === '作业') {
                    var paperScore = 0;
                    _.each($scope.QuestionListBox, function(group){
                        var groupScore = 0;
                        _.each(group.question, function(q){
                            if(q.mode === 'A') {
                                _.each(q.sub, function(s){
                                    s.score = 1;
                                    groupScore += 1;
                                });
                            }else {
                                q.score = 1;
                                groupScore += 1;
                            }
                        });
                        paperScore += groupScore;
                        group.QTypeScores = groupScore;
                    });
                    $scope.score = paperScore;
                }
            }
            // 自动换题目
            $scope.autoChooes = function(){
                if(!$scope.currentQuestion){
                    constantService.alert('请选择要自动换题目的题目');
                    return false;
                }
                // 根据索引 返回 当前题目在数组里面的信息
                var currentGroup = $scope.currentGroup;
                var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                $scope.before = currentGroup.question[index];

                $http({
                    method : 'post',
                    url : $rootScope.baseUrl + '/Interface0198.ashx',
                    data : {
                        qFLnkID : $scope.before.QFLnkID, // 试题的ID 试题的等级
                        diff : $scope.before.DifficultLevel
                    }
                }).success(function(res){
                    if(res.code == '2'){
                        constantService.alert('无相关数据');
                        return false;
                    } else {
                        //alert(res.code);
                        if(!_.isArray(res.msg)){
                            constantService.alert('无相关数据');
                            return false;
                        }
                        if(res.msg[0] && !isExist(res.msg[0].QID, currentGroup)){
                            var toInsertQst = paperService.parseQstForPaper(res.msg[0]);
                            _.extend($scope.before, toInsertQst);
                        } else {
                            $scope.autoChooes();
                        }
                    }
                })
            };

            function isExist(id, group){
                return !!_.find(group.question, function(item){
                    return item.QID + '' === id + '';
                });
            }

            // 布置试卷
            $scope.sendPaper = function(){
                if($scope.testpaperName){
                    // $('.paperComposing .sendPaperButton').css({
                    //     'background': '#6C6FC0'
                    // })
                    $http({
                        method : 'post',
                        url : $rootScope.baseUrl + '/Interface0150.ashx',
                        data : {}
                    }).success(function(res){
                        var tempArray = res.msg;

                        if(tempArray.length > 0){
                            var classStr = '<div style="height:100px;width:100%;">';
                            classStr += '<div class="selectValues"><div style="display:inline-block;margin-right: 10px;">布置对象: </div>';
                            for(var i = 0; i < tempArray.length; i++){
                                classStr += '<input data-id="' + tempArray[i].classId + '" type="checkbox" class="addPointer" id="popUpDialog_' + i + '" /><lable class="addPointer inplable" for="popUpDialog_' + i + '">' + tempArray[i].name + '</lable>';
                            }
                            classStr += '</div>';
                            classStr += '<div><div style="display:inline-block;padding-left: 30px;margin-top: 20px;"><div class="popUpDialog_beginTime">测试时间:</div><input id="popUpDialog_beginTime" readonly="readonly" placeholder="开始时间"/></div>';
                            classStr += '<div style="display:inline-block"><div class="popUpDialog_endTime">至:</div><input id="popUpDialog_endTime" readonly="readonly" placeholder="结束时间"/></div></div>';
                            classStr += '</div>';
                            ngDialog.open({
                                template : classStr + '<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">布置试卷</button></div>',
                                plain : true,
                                className : 'ngdialog-theme-plain bigger',
                                preCloseCallback : function(){
                                    var domList = $('.selectValues input');
                                    var classFLnkID = '';
                                    for(var i = 0; i < domList.length; i++){
                                        if(domList[i].checked){
                                            if(i === domList.length - 1)
                                                classFLnkID += domList[i].getAttribute('data-id');
                                            else
                                                classFLnkID += domList[i].getAttribute('data-id') + ',';
                                        }
                                    }
                                    var param = {
                                        examName : $scope.testpaperName,
                                        examFLnkID : $scope.testpaperId,
                                        classFLnkID : classFLnkID,
                                        startTime : $('#popUpDialog_beginTime').val(),
                                        stopTime : $('#popUpDialog_endTime').val()
                                    };
                                    //弹框提示
                                    var msg = '';
                                    if(!param.examFLnkID){
                                        msg = '非法的试卷ID';
                                    } else if(!param.classFLnkID){
                                        msg = '请选择班级';
                                    } else if(!param.startTime){
                                        msg = '请选择开始时间';
                                    } else if(!param.stopTime){
                                        msg = '请选择结束时间';
                                    }

                                    if(msg !== ''){
                                        ngDialog.open({
                                            template : '<p>' + msg + '</p>' +
                                            '<div class="ngdialog-buttons">' +
                                            '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                                            plain : true,
                                            className : 'ngdialog-theme-plain'
                                        });
                                        return false;
                                    } else {
                                        $http({
                                            method : 'post',
                                            url : $rootScope.baseUrl + '/Interface0218.ashx',
                                            data : param
                                        }).success(function(res){
                                            $('.sendPaperButton').hide();
                                            ngDialog.open({
                                                template : '<p>布置试卷成功</p>' +
                                                '<div class="ngdialog-buttons">' +
                                                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                                                plain : true,
                                                className : 'ngdialog-theme-plain'
                                            });
                                        })
                                    }


                                }
                            });
                            addCalendarClick();
                        } else {
                            ngDialog.open({
                                template : '<p>没有查询到班级</p>' +
                                '<div class="ngdialog-buttons">' +
                                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                                plain : true,
                                className : 'ngdialog-theme-plain'
                            });
                        }
                    })
                }

            };

            function addCalendarClick(){
                if($('#popUpDialog_beginTime').length > 0){
                    $('#popUpDialog_beginTime').datetimepicker({
                        lang : "ch",
                        timepicker : false,
                        format : "Y-m-d",
                        todayButton : false,
                        start : new Date(),
                        scrollInput : false,
                        onChangeDateTime : function(dp, $input){

                            $('#popUpDialog_beginTime').datetimepicker('hide');
                        }
                    });

                    $('#popUpDialog_endTime').datetimepicker({
                        lang : "ch",
                        timepicker : false,
                        format : "Y-m-d",
                        todayButton : false,
                        start : new Date(),
                        scrollInput : false,
                        onChangeDateTime : function(dp, $input){
                            if($input.val() < $('#popUpDialog_beginTime').val()){
                                ngDialog.open({
                                    template : '<p>结束时间需大于开始时间</p>' +
                                    '<div class="ngdialog-buttons">' +
                                    '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                                    plain : true,
                                    className : 'ngdialog-theme-plain'
                                });
                            } else {
                                $('#popUpDialog_endTime').datetimepicker('hide');
                            }
                        }
                    });
                } else {
                    setTimeout(function(){
                        addCalendarClick();
                    }, 100);
                }
            }

            $scope.$watch('testpaperName', function(){
                examName = $scope.testpaperName;
            });
            if(action == 1){
                $scope.smalltitle = $scope.smalltitle || PAPER_SMALL_TITLE;
                $scope.testpaperName = $scope.testpaperName || PARPER_BIG_TITLE;
                examName = $scope.testpaperName; // 试卷名称
                $scope.gradeId = $location.search().gradeid; // 年级ID
            } else if(action == ''){
                //$scope.gradeId = askBox[0].gradeId; // 年级ID
            }

            // 保存试卷
            $scope.savePager = function(){
                var allqst = paperService.getAllQuestionInPaper($scope.QuestionListBox);
                if(checkExmType(allqst)){
                    constantService.alert('机房评测/课堂评测中不能加入非选择题!');
                    return;
                }
                if(checkScore(allqst)){
                    return;
                }
                $scope.paperid = $location.search().examId; // 试卷ID
                if(!$scope.testpaperName){
                    constantService.alert('请设置试卷名称！');
                    return false;
                }

                if(!$scope.times || $scope.times <= 0){
                    if(!$scope.times){
                        constantService.alert('请输入考试时间！');
                    } else if($scope.times <= 0){
                        constantService.alert('考试时间格式不正确！');
                    }
                    return false;
                }
                if(!$scope.score){
                    constantService.alert('请设置题目分数！');
                    return false;
                }
                if(+$scope.score > 300){
                    constantService.alert('您设置的试卷分数过高，请调整分数！');
                    return false;
                }
                var isExistIllegalScore = _.find($scope.QuestionListBox, function(group){
                    var zeroScore = _.find(group.question, function(qst){
                        return +qst.score <= 0 || +qst.score > 99;
                    });
                    return !!zeroScore;
                });
                if(isExistIllegalScore){
                    constantService.alert('存在题目分数异常，请检查试卷！');
                    return false;
                }
                if(action == 3){
                    examName = $scope.testpaperName; // 试卷名称
                    examFLnkID = $scope.paperid;
                }
                var newPage = [];
                var newPager_Page = [];

                // 生成新的试卷数据
                var greatObj = function(){
                    var arr = [];
                    angular.forEach($scope.QuestionListBox, function(group, index){
                        angular.forEach(group.question, function(item, index){
                            arr.push({
                                qFLnkID : item.FLnkID,
                                qtypeName : item.QTypeName,
                                qScores : item.score,
                                Order : item.orders,
                                ShowType : item.ShowType && item.ShowType.id || '0'
                            });
                        });
                    });
                    return arr;
                }
                var msgDate = greatObj();
                var reCat = /^\d+(\.)?$/;

                var list = _.map($scope.QuestionListBox, function(item, index){
                    var lists = _.map(item.question, function(item, index){
                        return {
                            score : item.score
                        }
                    });
                });


                if(reCat.test($scope.score)){
                    $('#buttonsave').attr('disabled', true);//添加disabled属性
                    $('#buttonsave').css({
                        'background' : '#757575',
                        'color' : '#fff'
                    })
                    $http({
                        method : 'post',
                        url : $rootScope.baseUrl + '/Interface0219.ashx',
                        data : {
                            examName : examName === PARPER_BIG_TITLE ? PARPER_BIG_TITLE : examName,
                            smallTitle : $scope.smalltitle === PAPER_SMALL_TITLE ? '' : $scope.smalltitle,
                            time : $scope.times,
                            scores : $scope.score,
                            examFLnkID : examFLnkID,
                            exType : $scope.exType || '',
                            msg : paperService.getQstsForSubmit($scope.QuestionListBox),
                            gradeId : $scope.gradeId,
                            isTeacher : '1',
                            fLnkID : (+qbank === 3 ? (JSON.parse(sessionStorage.getItem('currentUser')).fid):(+qbank === 2 ? JSON.parse(sessionStorage.getItem('currentUser')).schoolFId:''))
                        }
                    }).success(function(res){
                        if(res.code == 0){
                            $('#buttonsave').attr('disabled', false); //移除disabled属性
                            $('#buttonsave').css({
                                'background' : '#41B29C'
                            });
                            $scope.msgs = res.msg;
                            $scope.testpaperId = res.msg;
                            // $http({
                            //     method: 'post',
                            //     url: $rootScope.baseUrl + '/Interface0118A.ashx',
                            //     data: {
                            //         examName: examName,
                            //         examId: res.msg
                            //     }
                            // }).success(function (res) {
                            //     var blob = new Blob([res.msg], {
                            //         type: "text/plain;charset=utf-8"
                            //     });
                            //     saveAs(blob, examName + ".doc");
                            // });
                            localStorage.removeItem('askList'); // 清除缓存
                            localStorage.removeItem('answerList'); //清除补偿存储
                            ngDialog.open({
                                template : '<p>保存成功</p>' +
                                '<div class="ngdialog-buttons">' +
                                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                                plain : true,
                                className : 'ngdialog-theme-default',
                                preCloseCallback : function(value){
                                    $scope.isnew = true;
                                    if($scope.exType != '班级作业'){
                                        $('.sendPaperButton').hide();
                                        $('.savePaperButton').show();
                                    } else {
                                        $('.sendPaperButton').show();
                                        $('.savePaperButton').show();
                                    }
                                    //$location.path('/pathss').search({ "pathss": "/paperBankmy", 'ExamFlnkID': $scope.msgs, 'action': 0, 'gradeid': $scope.gradeId });
                                    $location.path('/paperComposing').search({
                                        'examId' : $scope.msgs,
                                        'action' : 3,
                                        'gradeid' : $scope.gradeId
                                    });


                                }
                            });
                            return false;
                        } else {
                            $('#buttonsave').attr('disabled', false); //移除disabled属性
                            $('#buttonsave').css({
                                'background' : '#41B29C'
                            })
                            ngDialog.open({
                                template : '<p>' + res.msg + '</p>' +
                                '<div class="ngdialog-buttons">' +
                                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                                plain : true,
                                className : 'ngdialog-theme-default',
                                preCloseCallback : function(value){
                                    //$state.go('myApp.paperBankmy');
                                }
                            });
                            return false;
                        }
                    })
                    return false;
                } else {
                    constantService.alert('总分必须为整数');
                    return false;
                }
            };
            // 点击切换样式
            $scope.selectCur = function(question, group){
                $scope.currentQuestion = question;
                $scope.currentGroup = group;
            };

            // 返回当前点击题目所在位置
            var getposition = function(FLnkID){
                var wz = $scope.QuestionListBox.indexOf(FLnkID);
                return wz;
            };
            // 删除题目
            $scope.delArr = function(){
                if(!$scope.currentQuestion){
                    constantService.alert('请选择要删除的题目');
                    return false;
                }
                var currentGroup = $scope.currentGroup;
                var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                currentGroup.question.splice(index, 1);
                currentGroup.QNumber = +currentGroup.QNumber - 1;
                $scope.currentQuestion = '';
                // 更新缓存
                if($scope.QuestionListBox.length == 0){
                    localStorage.removeItem('askList');
                    $state.go("myApp.chapters");
                    return false;
                }
            };
            // 根据年级调取题目类型
            $scope.selectType = function($event){
                //alert($scope.gradeId + "1");
                var tag = $event.target;
                var gradeBoxs = {
                    gradeId : $scope.gradeId
                };
                $http.post($rootScope.baseUrl + '/Interface0197.ashx', gradeBoxs).success(function(data){
                    $scope.typeClassList = data.msg;
                    $scope.gradeBoxList = $(tag).val()
                })
            };
            // 更换题目
            $scope.changeQuestion = function(){
                // 判断是否选中题目
                if(!$scope.currentQuestion){
                    constantService.alert('请选择要替换的题目');
                    return false;
                }
                var currentGroup = $scope.currentGroup;
                var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                $scope.showNoData = false;
                // 根据索引 返回 当前题目在数组里面的信息
                $scope.before = currentGroup.question[index];
                $http.get($rootScope.baseUrl + '/Interface0051A.ashx').success(function(data){
                    $scope.gradeList = data.course;
                    $scope.gradeBoxList = data.course[0].gradeId;
                    $scope.searchDialogForChange = true;
                    $scope.qBelong = 1;
                    ngDialog.open({
                        template : 'changeQuestion',
                        className : 'ngdialog-theme-plain',
                        scope : $scope,
                        preCloseCallback : function(){
                            $scope.searchList = '';
                            $scope.paginationConf.totalItems = 0;
                            $scope.paginationConf.currentPage = 1; // 回复默认页数
                        }
                    });
                    $scope.$on('ngDialog.templateLoaded', function(){
                        setTimeout(function(){
                            $('.dialog-start').raty({ // 等级
                                score : function(){
                                    $scope.Difficult = +$(this).attr('data-score');
                                    return $(this).attr('data-score');
                                },
                                path : function(){
                                    return this.getAttribute('data-path');
                                },
                                click : function(score, evt){
                                    if(score == null){
                                        $scope.Difficult = 0;
                                    } else {
                                        $scope.Difficult = score;
                                    }

                                },
                                cancel : function(){
                                    cancel: true;
                                }
                            });
                        }, 100);
                    });
                }).then(function(){
                    var grade = {
                        gradeId : $scope.gradeBoxList
                    };
                    $http.post($rootScope.baseUrl + '/Interface0197.ashx', grade).success(function(data){
                        $scope.typeClassList = data.msg;
                        $scope.currentType = _.find($scope.typeClassList, function(type){
                            return type.qtypeId === $scope.currentGroup.QTypeId;
                        });
                        $scope.typeClassbox = data.msg[0].qtypeId;
                    });
                });
            };
            // 获取替换的题目信息
            $scope.Qreplace = function(index){
                var qst = paperService.parseQstForPaper($scope.searchList[index]);
                if($scope.searchDialogForChange){
                    _.extend($scope.currentQuestion, qst);
                } else {
                    var currentGroup = $scope.currentGroup;
                    var index = paperService.getQuestionIndexInGroup($scope.currentQuestion, currentGroup);
                    currentGroup.question.splice(index + 1, 0, qst);
                }
                // 关闭弹出层
                ngDialog.closeAll();
            };
            // 搜索
            $scope.diff = 1;
            /*var checkBox = "";
             $scope.isSelect = function (id, $event) {
             var checkeds = $event.target;

             if (checkeds.checked) {
             checkBox += id + ",";
             } else {
             checkBox = checkBox.replace(id + ",", "");
             }
             return checkBox;
             };*/
            $scope.search = function(source, KeyWord, Knowledge, Diff){

                /*if (checkBox == '') {
                 alert('请选择题库类型');
                 return false;
                 }*/
                if(KeyWord == undefined){
                    KeyWord = '';
                }
                if(source == undefined){
                    source = '';
                }
                if(Knowledge == undefined){
                    Knowledge = ''
                }
                //if (action != '') {
                //  $scope.gradeId = $location.search().gradeid;
                //}

                $scope.saveSearchDate = {
                    QBelong : $scope.qBelong,
                    KeyWord : KeyWord,
                    knowlegeName : Knowledge,
                    diff : Diff || $scope.Difficult,
                    CurPage : 1,
                    gradeId : $scope.gradeId || $location.search().gradeid,
                    QtypeId : $scope.currentType.qtypeId,
                    pageSize : 10,
                    chapterId : '' || $scope.chapterId,
                    source : source
                }
                if(action == '4'){
                    //alert(4);
                    $http({
                        method : 'post',
                        url : $rootScope.baseUrl + '/Interface0194.ashx',
                        data : {
                            type : $scope.currentType.qtypeId,
                            chapterId : "",
                            province : '全部',
                            year : '全部',
                            level : '全部',
                            name : KeyWord,
                            startTime : "",
                            stoptime : "",
                            source : source,
                            sourceType : $scope.qBelong,
                            knowlegeName : Knowledge,
                            diff : Diff || $scope.Difficult,
                            CurPage : 1,
                            gradeId : $scope.gradeId,
                            pageSize : 1,
                            pharse:USER.pharseId,
                            subjectId:$scope.subjectId
                        }

                    }).success(function(res){
                        if(res.code == 2){
                            $scope.showNoData2 = false;
                            $scope.showNoData = true;
                            $scope.showNoData0 = false;
                            // $('.pagination').css("display", "none");
                            return false;
                        }
                        if(!_.isArray(res.msg)){
                            $scope.searchList = [];
                            $scope.showNoData2 = false;
                            $scope.showNoData = false;
                            $scope.showNoData0 = true;
                            return;
                        }
                        $scope.showNoData2 = false;
                        $scope.showNoData = false;
                        $scope.showNoData0 = true;
                        var list = _.map(res.msg, function(item, index){

                            var isShowAnswer;
                            if((item.QTypeName == '选择题' || item.QTypeName == '单项选择') && item.OptionOne != ''){
                                isShowAnswer = true;
                            } else {
                                isShowAnswer = false;
                            }
                            return {
                                FLnkID : item.FLnkID,
                                QID : item.QID,
                                title : item.title,
                                OptionOne : item.OptionOne,
                                OptionTwo : item.OptionTwo,
                                OptionThree : item.OptionThree,
                                OptionFour : item.OptionFour,
                                DifficultLevel : item.DifficultLevel,
                                knowledges : item.knowledges,
                                knowledge : item.knowledge,
                                knowledgeNames : _.pluck(item.knowledge, 'name').join(','),
                                Answer : item.Answer,
                                QTypeName : item.QTypeName,
                                QTypeId : item.QTypeId,
                                startDiff : viewStar(item.DifficultLevel),
                                score : 0, // 初始每道题目的分数
                                IsCollection : item.IsCollection,
                                ShowType : item.ShowType,
                                isShowAnswer : isShowAnswer
                            }
                        });
                        $scope.searchList = _.filter(list, function(item){
                            return !checkQuestionRepeat(item);
                        });
                        $scope.paginationConf.totalItems = res.PageNum;
                        $scope.paginationConf.currentPage = 1; // 回复默认页数
                        $scope.nextPage = function(){
                            if($scope.paginationConf.currentPage<$scope.paginationConf.totalItems){
                                $scope.paginationConf.currentPage += 1;
                            }
                        };
                        $scope.prevPage = function(){
                            if($scope.paginationConf.currentPage>1){
                                $scope.paginationConf.currentPage -= 1;
                            }

                        }
                    })
                } else if(action == '5' || action == '1' || action == '3'){//有知识点

                    if(Knowledge == '' && KeyWord == ''){
                        $scope.showNoData2 = true;
                        $scope.showNoData = false;
                        $scope.showNoData0 = false;
                        return false;
                    } else {
                        $scope.showNoData2 = false;
                        $scope.showNoData = false;
                        $scope.showNoData0 = true;
                        var source = source;
                        $http({
                            method : 'post',
                            url : $rootScope.baseUrl + '/Interface0194A.ashx',
                            data : {
                                knowledgeId : 0,
                                type : $scope.currentType.qtypeId,
                                chapterId : "",
                                province : '全部',
                                year : '全部',
                                level : '全部',
                                name : KeyWord,
                                startTime : "",
                                stoptime : "",
                                source : source,
                                sourceType : $scope.qBelong,
                                knowlegeName : Knowledge,
                                diff : Diff || $scope.Difficult,
                                CurPage : 1,
                                gradeId : $scope.gradeId,
                                pageSize : 10,
                                subjectId:$scope.subjectId,
                                pharse:USER.pharseId
                            }

                        }).success(function(res){
                            if(res.code == 2){
                                $scope.showNoData2 = false;
                                $scope.showNoData = true;
                                $scope.showNoData0 = false;


                                $('.pagination').css("display", "none");
                                return false;
                            }
                            //if (!_.isArray(res.msg)) {
                            //	$scope.searchList = [];
                            //	$scope.showNoData = true;
                            //	return;
                            //}
                            $scope.showNoData2 = false;
                            $scope.showNoData = false;
                            $scope.showNoData0 = true;

                            var list = _.map(res.msg, function(item, index){

                                var isShowAnswer;
                                if((item.QTypeName == '选择题' || item.QTypeName == '单项选择') && item.OptionOne != ''){
                                    isShowAnswer = true;
                                } else {
                                    isShowAnswer = false;
                                }
                                return {
                                    FLnkID : item.FLnkID,
                                    QID : item.QID,
                                    title : item.title,
                                    OptionOne : item.OptionOne,
                                    OptionTwo : item.OptionTwo,
                                    OptionThree : item.OptionThree,
                                    OptionFour : item.OptionFour,
                                    DifficultLevel : item.DifficultLevel,
                                    knowledges : item.knowledges,
                                    knowledge : item.knowledge,
                                    knowledgeNames : _.pluck(item.knowledge, 'name').join(','),
                                    Answer : item.Answer,
                                    QTypeName : item.QTypeName,
                                    QTypeId : item.QTypeId,
                                    startDiff : viewStar(item.DifficultLevel),
                                    score : 0, // 初始每道题目的分数
                                    IsCollection : item.IsCollection,
                                    ShowType : item.ShowType,
                                    isShowAnswer : isShowAnswer,
                                    KeyWord:KeyWord
                                }
                            });
                            $scope.searchList = _.filter(list, function(item){
                                return !checkQuestionRepeat(item);
                            });
                            $scope.paginationConf.totalItems = res.Count;
                            $scope.paginationConf.currentPage = 1 // 回复默认页数

                        })
                    }

                }
            }

            /*function getQBelong() {
             var qBelong = [];
             var checkboxs = $('.changeQuestion').find('input[type="checkbox"]');
             _.each(checkboxs, function (checkbox) {
             if (checkbox.checked) {
             qBelong.push($(checkbox).val());
             }
             });
             return qBelong.join(',');
             }*/

            /*分页*/
            //$scope.paginationConf.totalItems = false;
            $scope.paginationConf = {
                currentPage : '',
                itemsPerPage : 10
            };
            $scope.$watch('paginationConf.currentPage + paginationConf.itemsPerPage', function(){
                var source = source;
                if($scope.paginationConf.currentPage === 1){
                    return;
                }
                if(firstFlag){
                    firstFlag = false;
                    return;
                }
                if(action == '4'){
                    var req = angular.copy($scope.saveSearchDate) || {};
                    req.chapterId = '';
                    req.type = $scope.currentType.qtypeId;
                    req.province = '全部';
                    req.year = '全部';
                    req.level = '全部';
                    req.year = '全部';
                    req.name = '';
                    req.sourceType = $scope.qBelong;
                    req.startTime = "";
                    req.stoptime = "";
                    req.CurPage = $scope.paginationConf.currentPage;
                    req.pageSize = $scope.paginationConf.itemsPerPage;
                    req.gradeId = $scope.gradeId;
                    req.pharse=USER.pharseId;
                    req.subjectId=$scope.subjectId;
                    $http.post($rootScope.baseUrl + '/Interface0194.ashx', req).success(function(resp){
                        if(resp.code != 2){
                            var list = _.map(resp.msg, function(item, index){
                                if((item.QTypeName == '选择题' || item.QTypeName == '单项选择') && item.OptionOne != ''){
                                    var isShowAnswer = true;
                                } else {
                                    var isShowAnswer = false;
                                }
                                return {
                                    FLnkID : item.FLnkID,
                                    QID : item.QID,
                                    title : item.title,
                                    OptionOne : item.OptionOne,
                                    OptionTwo : item.OptionTwo,
                                    OptionThree : item.OptionThree,
                                    OptionFour : item.OptionFour,
                                    DifficultLevel : item.DifficultLevel,
                                    knowledges : item.knowledges,
                                    Answer : item.Answer,
                                    QTypeName : item.QTypeName,
                                    QTypeId : item.QTypeId,
                                    startDiff : viewStar(item.DifficultLevel),
                                    datascore : 0, // 初始每道题目的分数
                                    IsCollection : item.IsCollection,
                                    ShowType : item.ShowType,
                                    isShowAnswer : isShowAnswer
                                }
                            });
                            $scope.searchList = _.filter(list, function(item){
                                return !checkQuestionRepeat(item);
                            });
                            $scope.paginationConf.totalItems = resp.Count;
                        } else {
                            $scope.showNoData = true;
                        }
                    });
                } else if(action == '5' || action == '1' || action == '3'){
                    var req = angular.copy($scope.saveSearchDate) || {};
                    req.source = req.source;
                    req.chapterId = '';
                    req.type = $scope.currentType.qtypeId;
                    req.province = '全部';
                    req.year = '全部';
                    req.level = '全部';
                    req.year = '全部';
                    req.name = '';
                    req.sourceType = $scope.qBelong;
                    req.startTime = "";
                    req.stoptime = "";
                    req.CurPage = $scope.paginationConf.currentPage;
                    req.pageSize = $scope.paginationConf.itemsPerPage;
                    req.gradeId = $scope.gradeId;
                    req.pharse=USER.pharseId;
                    req.subjectId=$scope.subjectId;
                    //req.chapterId = '';
                    //req.type = $scope.currentType.qtypeId;
                    //req.province = '全部';
                    //req.year = '全部';
                    //req.level = '全部';
                    //req.year = '全部';
                    //req.name = '';
                    //req.sourceType = getQBelong();
                    //req.startTime = "";
                    //req.stoptime = "";
                    //req.CurPage = $scope.paginationConf.currentPage;
                    //req.pageSize = $scope.paginationConf.itemsPerPage;
                    //req.gradeId = $scope.gradeId

                    $http.post($rootScope.baseUrl + '/Interface0194A.ashx', req).success(function(resp){
                        if(resp.code != 2){
                            $scope.showNoData = false;
                            var list = _.map(resp.msg, function(item, index){
                                if((item.QTypeName == '选择题' || item.QTypeName == '单项选择') && item.OptionOne != ''){
                                    isShowAnswer = true;
                                } else {
                                    isShowAnswer = false;
                                }
                                return {
                                    FLnkID : item.FLnkID,
                                    QID : item.QID,
                                    title : item.title,
                                    OptionOne : item.OptionOne,
                                    OptionTwo : item.OptionTwo,
                                    OptionThree : item.OptionThree,
                                    OptionFour : item.OptionFour,
                                    DifficultLevel : item.DifficultLevel,
                                    knowledges : item.knowledges,
                                    Answer : item.Answer,
                                    QTypeName : item.QTypeName,
                                    QTypeId : item.QTypeId,
                                    startDiff : viewStar(item.DifficultLevel),
                                    datascore : 0, // 初始每道题目的分数
                                    IsCollection : item.IsCollection,
                                    ShowType : item.ShowType,
                                    isShowAnswer : isShowAnswer
                                }
                            });
                            $scope.searchList = _.filter(list, function(item){
                                return !checkQuestionRepeat(item);
                            });
                            $scope.paginationConf.totalItems = resp.Count;
                        } else {
                            $scope.showNoData = true;
                        }
                    });
                }
            });
            //检查题目是否重复
            function checkQuestionRepeat(question){
                return !!_.find($scope.QuestionListBox, function(group){
                    return !!_.find(group.question, function(item){
                        return item.QID === question.QID;
                    });
                });
            }

            // 根据当前数字显示五角星
            function viewStar(num){
                var str = '';
                for(var i = 0; i < num; i++){
                    str = str + '<span class="fa fa-star fa-lg orange"></span>'
                }
                return str;
            }

            /**
             * 二期需求，添加收藏
             */
            $scope.addFav = function(question){ // type 1  试卷  2 题目
                $http.post($rootScope.baseUrl + '/Interface0204.ashx', {
                    resourceFlnkID : question.FLnkID,
                    type : 2
                }).success(function(data){
                    question.IsCollection = '1';
                    constantService.alert('收藏成功！');
                })
            };

            $scope.removeFav = function(question){
                $http.post($rootScope.baseUrl + '/Interface0205.ashx', {
                    resourceFlnkID : question.FLnkID
                }).success(function(data){
                    question.IsCollection = '0';
                    constantService.alert('取消收藏成功！');
                });
            };
            /**
             * 二期需求：插入新题
             */
            $scope.addNewQuestion = function(){
                $http.get($rootScope.baseUrl + '/Interface0051A.ashx').success(function(data){
                    $scope.gradeList = data.course;
                    $scope.gradeBoxList = data.course[0].gradeId;
                    $scope.searchDialogForChange = false;
                    $scope.qBelong = 1;
                    ngDialog.open({
                        template : 'changeQuestion',
                        className : 'ngdialog-theme-plain',
                        scope : $scope,
                        preCloseCallback : function(){
                            $scope.searchList = '';
                            $scope.paginationConf.totalItems = '';
                            $scope.paginationConf.currentPage = '';// 回复默认页数
                            $scope.showNoData = true;
                        }
                    });
                    $scope.$on('ngDialog.templateLoaded', function(){
                        setTimeout(function(){
                            $('.dialog-start').raty({ // 等级
                                score : function(){
                                    $scope.Difficult = +$(this).attr('data-score');
                                    return $(this).attr('data-score');
                                },
                                path : function(){
                                    return this.getAttribute('data-path');
                                },
                                click : function(score, evt){
                                    $scope.Difficult = score;
                                }
                            });
                        }, 100);
                    });
                }).then(function(){
                    var grade = {
                        gradeId : $scope.gradeBoxList
                    };
                    $http.post($rootScope.baseUrl + '/Interface0197.ashx', grade).success(function(data){
                        $scope.typeClassList = data.msg;
                        $scope.currentType = _.find($scope.typeClassList, function(type){
                            return type.qtypeId === $scope.currentGroup.QTypeId;
                        });
                        $scope.typeClassbox = data.msg[0].qtypeId;
                    })
                });
            };
            $scope.modifyCurrentQuestionScore = function(){
                if($scope.currentQuestion){
                    isIegal4Save();
                    var group = _.find($scope.QuestionListBox, function(group){
                        return group.Dtype === $scope.currentGroup.Dtype;
                    });
                    group.QTypeScores = paperService.calcGroupScore(group);
                    $scope.score = paperService.getTotalScore($scope.QuestionListBox);
                    $scope.currentQuestion.isScoreModified = true;
                }
            };

            $scope.modifyGroupScore = function(group){
                if(group){
                    $scope.isModifyGroupScore = true;
                    var len = group.question.length;
                    var groupScore = group.QTypeScores;
                    var evgScore = groupScore / len;
                    _.each(group.question, function(item){
                        item.score = evgScore;
                        if(item.sub && item.sub.length > 0){
                            var subscore = evgScore / item.sub.length;
                            _.each(item.sub, function(s){
                                s.score = subscore;
                            });
                        }
                    });
                }
                $scope.score = paperService.getTotalScore($scope.QuestionListBox);
            };

            $scope.saveEditQst = function(data){
                ngDialog.closeAll();
                //TODO 另加一道题到校本题库，并替换掉当前题目
                var params = {
                    msg : [],
                    qId : data.QID,
                    title : data.title,
                    optionA : data.optionOne,
                    optionB : data.optionTwo,
                    optionC : data.optionThree,
                    optionD : data.optionFour,
                    answer : data.answer,
                    mp3 : data.mp3
                };
                if($scope.toEditQuestion.isSub){
                    var mainQst = paperService.getMainQuestionBySub($scope.toEditQuestion, $scope.currentGroup);
                    params.msg.push({
                        qFlnkId : mainQst.FLnkID,
                        type : '0'
                    });
                    var subIds = _.map(mainQst.sub, function(item){
                        var type = item.FLnkID === $scope.toEditQuestion.FLnkID ? '1' : '0';
                        return {
                            qFlnkId : item.FLnkID,
                            type : type
                        };
                    });
                    params.msg = params.msg.concat(subIds);
                } else if(!_.isEmpty($scope.toEditQuestion.sub)){
                    params.msg.push({
                        qFlnkId : $scope.toEditQuestion.FLnkID,
                        type : '1'
                    });
                    var subIds = _.map($scope.toEditQuestion.sub, function(item){
                        return {
                            qFlnkId : item.FLnkID,
                            type : '0'
                        };
                    });
                    params.msg = params.msg.concat(subIds);
                } else {
                    params.msg.push({
                        qFlnkId : $scope.toEditQuestion.FLnkID,
                        type : '1'
                    });
                }
                $http.post($rootScope.baseUrl + '/Interface0206A.ashx', params).then(function(res){
                    if(res.data.code === 0){
                        var ids = res.data.msg.split(',');
                        if($scope.toEditQuestion.isSub){
                            var mainQst = paperService.getMainQuestionBySub($scope.toEditQuestion, $scope.currentGroup);
                            _.extend(mainQst, {
                                QFLnkID : ids[0],
                                fLnkId : ids[0],
                                FLnkID : ids[0]
                            });
                            _.each(mainQst.sub, function(item, index){
                                _.extend(item, {
                                    QFLnkID : ids[index + 1],
                                    fLnkId : ids[index + 1],
                                    FLnkID : ids[index + 1],
                                    mainId : ids[0]
                                });
                            });
                        } else if($scope.toEditQuestion.isMain){
                            _.extend($scope.toEditQuestion, {
                                QFLnkID : ids[0],
                                fLnkId : ids[0],
                                FLnkID : ids[0]
                            });
                            _.each($scope.toEditQuestion.sub, function(item, index){
                                _.extend(item, {
                                    QFLnkID : ids[index + 1],
                                    fLnkId : ids[index + 1],
                                    FLnkID : ids[index + 1],
                                    mainId : ids[0]
                                });
                            });
                        } else {
                            _.extend($scope.toEditQuestion, {
                                QFLnkID : ids[0],
                                fLnkId : ids[0],
                                FLnkID : ids[0]
                            });
                        }
                        constantService.showConfirm('题目已修改成功，是否需要同步修改知识点？', ['确定'], function(){
                            $scope.modifyKlg();
                        });
                    }
                })
            };

            $scope.editQuestion = function(question){
                $scope.toEditQuestion = question;
                ngDialog.open({
                    template : 'editQuestion',
                    plain : false,
                    scope : $scope,
                    className : 'ngdialog-theme-default',
                    appendClassName : 'edit-q-dialog',
                    preCloseCallback : function(){
                        $scope.$broadcast('$destory-ck');
                    }
                });
            };

            //监听问题列表变化
            $scope.$watch('QuestionListBox', function(){
                if(_.isArray($scope.QuestionListBox)){
                    $scope.score = paperService.getTotalScore($scope.QuestionListBox);
                    paperService.setQuestionsOrder($scope.QuestionListBox);
                }
            }, true);
            /**
             * 拖动处理
             */
            var groupId = '';
            //dragable 拖拽插件处理
            var dragable = $('.paperComposing').dragable({
                ele : '.paper-content',
                container : '.group-content',
                dragstartCb : function(e, data){
                    groupId = $(e.target).parents('.group-content')[0].id;
                    dragable.setDataTransfer('currentId', e.currentTarget.id);
                },
                dragendCb : function(ev, $target){
                    var targetId = $target[0].id;
                    var data = dragable.getDataTransfer("currentId");
                    var group = _.find($scope.QuestionListBox, function(group){
                        return groupId === group.Dtype;
                    });
                    var questions = group.question;

                    var targetQuestion = _.find(questions, function(question){
                            return +question.orders === +targetId;
                        }),
                        targetIndex = paperService.getQuestionIndexInGroup(targetQuestion, group);
                    var originQuestion = _.find(questions, function(question){
                            return +question.orders === +data;
                        }),
                        originIndex = paperService.getQuestionIndexInGroup(originQuestion, group);
                    $scope.$apply(function(){
                        if(targetIndex > originIndex){
                            questions.splice(targetIndex + 1, 0, originQuestion);
                            questions.splice(originIndex, 1);
                        } else if(targetIndex < originIndex){
                            questions.splice(originIndex, 1);
                            questions.splice(targetIndex + 1, 0, originQuestion);
                        }
                    });
                }
            });
            /**
             * 处理题型分组排序
             */
            var dragableGroup = $('.paperComposing').dragable({
                ele : '.question-group',
                container : '.group-area',
                dragstartCb : function(e, data){
                    dragableGroup.setDataTransfer('currentGroup', e.currentTarget.id);
                },
                dragendCb : function(ev, $target){
                    var targetId = $target[0].id;
                    var data = dragableGroup.getDataTransfer("currentGroup");
                    var targetGroup = _.find($scope.QuestionListBox, function(group){
                        return targetId === group.Dtype;
                    });
                    var originGroup = _.find($scope.QuestionListBox, function(group){
                        return data === group.Dtype;
                    });
                    var targetIndex = paperService.getGroupIndexInGroups(targetGroup, $scope.QuestionListBox),
                        originIndex = paperService.getGroupIndexInGroups(originGroup, $scope.QuestionListBox);
                    $scope.$apply(function(){
                        if(targetIndex > originIndex){
                            $scope.QuestionListBox.splice(targetIndex + 1, 0, originGroup);
                            $scope.QuestionListBox.splice(originIndex, 1);
                        } else if(targetIndex < originIndex){
                            $scope.QuestionListBox.splice(originIndex, 1);
                            $scope.QuestionListBox.splice(targetIndex + 1, 0, originGroup);
                        }
                    });
                }
            });

            $scope.changeMode = function(){
                if($scope.currentQuestion.mode === 'A'){
                    $scope.currentQuestion.mode = 'B';
                } else if($scope.currentQuestion.mode === 'B'){
                    $scope.currentQuestion.mode = 'A';
                }
            };

            function isIegal4Save(){
                if($scope.currentQuestion.isMain && $scope.currentQuestion.sub && $scope.currentQuestion.sub.length > 0){
                    var score = 0;
                    var temscore = Math.floor($scope.currentQuestion.score * 10 / $scope.currentQuestion.sub.length);
                    if(!(((temscore) % 10 === 5) || ((temscore) % 10 === 0))){
                        score = Math.floor(temscore / 10);
                    } else {
                        score = temscore / 10;
                    }
                    for(var i = 0; i < $scope.currentQuestion.sub.length; i++){
                        $scope.currentQuestion.sub[i].score = score;
                    }
                }
            }

            function checkScore(list){
                for(var i = 0; i < list.length; i++){
                    if(list[i].sub && list[i].sub.length > 0){
                        for(var j = 0; j < list[i].sub.length; j++){
                            if(isIegal(list[i].sub[j].score)){
                                return true;
                            }
                        }
                    } else {
                        if(isIegal(list[i].score)){
                            return true;
                        }
                    }
                }
                return false;
            }

            function isIegal(score){
                if(!(/^\d+(\.\d+)?$/.test(score))){
                    constantService.alert('分数仅能为非负数字!');
                    return true;
                }
                if(!(((score * 10) % 10 === 5) || ((score * 10) % 10 === 0))){
                    constantService.alert('分数仅能为.5整数倍!');
                    return true;
                }
            }

            function checkExmType(list){
                if($scope.exType === '课堂评测'){
                    for(var i = 0; i < list.length; i++){
                        if(list[i].sub && list[i].sub.length > 0){
                            for(var j = 0; j < list[i].sub.length; j++){
                                if(list[i].sub[j].isObjective === '0'){
                                    return true;
                                }
                            }
                        } else if(list[i].isObjective === '0'){
                            return true;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }

            // $http({
            //     method: 'post',
            //     url: $rootScope.baseUrl + '/Interface0181.ashx',
            //     data: {
            //         examId: examId
            //     }
            // }).success(function (res) {
            //     $scope.paperSubjectId = res.SubjectId;
            //     var pharseId = res.PharseId;
            //     initKnowledgeList($scope.paperSubjectId, pharseId);
            // });

            initKnowledgeList();

            //获取知识点，初始化加题知识点选择
            function initKnowledgeList(subjectId, pharseId) {
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0213.ashx',
                    // data: {
                    //     subjectId: subjectId,
                    //     pharseId: pharseId
                    // }
                }).success(function (res) {
                    if (!_.isArray(res.msg)) {
                        $scope.knowledgeList = [];
                    } else {
                        $scope.knowledgeList = res.msg;
                    }
                });
            }
            $scope.modifyKlg = function () {
                ngDialog.open({
                    template: '/klgEditor.html',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'dialog-edit-paper',
                    closeByDocument: false,
                    scope: $scope
                });
            };
            $scope.doModifyKlg = function (data) {
                var klgIds = _.pluck(data, 'unitId').join(',');
                $http.post($rootScope.baseUrl + '/Interface0206.ashx', {
                    qFlnkId: $scope.currentQuestion.QFLnkID,
                    strKnowledgeId: klgIds
                }).then(function (res) {
                    if (+res.data.code === 0) {
                        $scope.currentQuestion.knowledge = _.map(data, function (item) {
                            return {
                                id: item.unitId,
                                name: item.unitName
                            };
                        });
                        $scope.currentQuestion.knowledgeNames = '';
                        _.each($scope.currentQuestion.knowledge,function (item, index) {
                            if(index===0){
                                $scope.currentQuestion.knowledgeNames += item.name
                            }else {
                                $scope.currentQuestion.knowledgeNames += ','+item.name
                            }
                        });
                        constantService.showSimpleToast('修改考点成功！');
                    } else {
                        constantService.showSimpleToast('修改考点失败！');
                    }
                }, function () {
                    constantService.showSimpleToast('修改考点失败！');
                });
            };
            //修改Dtype时，检验长度（<=15）、唯一性；
            $scope.checkDtypeLegal = function(text, group){
                if(text.length > 15) {
                    constantService.showSimpleToast('分组名称不能超过15个字符！');
                    return false;
                }else {
                    var toCheckList = _.filter($scope.groupList, function(g){
                        return g.Dtype !== group.Dtype;
                    });
                    var isExist = _.find(toCheckList, function(g){
                        return g.Dtype === text;
                    });
                    if(isExist) {
                        constantService.showSimpleToast('分组名称不能重复，请重新输入！');
                        return false;
                    }else {
                        return true;
                    }
                }
            };
        }
    ]
});
