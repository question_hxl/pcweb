define(['underscore-min'], function () {
	return['$rootScope','$http','$scope','bes',
		function ($rootScope, $http, $scope, bes) {
		$scope.isTabCon = false;
		$scope.isTab = true;
		$scope.currTab = true;

		var user = JSON.parse(sessionStorage.getItem('currentUser'));

		var GradeId = $scope.GradeId = user.GradeId;

		//感兴趣的试卷
        /*bes.send('BootStrap/Interface/Interface0182.ashx', {
            curPage: 1,
            gradeId: GradeId,
            level: "全部",
            name: "",
            pageSize: 10,
            province: "全部",
            sourceType: 1,
            startTime: "",
            stoptime: "",
            type: "全部",
            year: "全部"
		}, {
        	method: 'post'
		}).success(function(res){
			if(_.isArray(res.msg)) {
                var tkList = $scope.tkList = res.msg;
                $scope.tkList = tkList.slice(0, 2);
			}else {
                $scope.tkList = [];
			}
		});*/
		// $http({
		// 	method: 'post',
		// 	url: $rootScope.baseUrl + '/Interface0182.ashx',
		// 	data: {
		//
		// 	}
		// }).success(function (res) {
		// 	var tkList = $scope.tkList = res.msg;
		// 	$scope.tkList = tkList.slice(0, 2);
		// })
		//试卷跳转
		$scope.gothispaper = function (flinkid) {
			location.href = "#/composing?ExamFlnkID=" + flinkid;
		};

		// 资源更新
		$http.get($rootScope.baseUrl + '/Interface0202.ashx').success(function (data) {
			//$scope.resourcesList = data.msg[0];
			$scope.resourcesList = data;
            if(_.isArray(data.exams)) {
                var tkList = $scope.tkList = data.exams;
                $scope.tkList = tkList.slice(0, 2);
            }else {
                $scope.tkList = [];
            }
		});

		// 班级列表
		$http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function (data) {
			$scope.ClassList = data.msg;
			$scope.CurrentClassID = data.msg[0].classId;
			$scope.ClassName = data.msg[0].name
		}).then(function () {

			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0164.ashx',
				data: {
					ClassFlnkid: $scope.CurrentClassID,
				}
			}).success(function (data) {
				// var tableDatas = data.msg;
				// var studentFlnkId = [];
				// if(tableDatas && _.isArray(tableDatas)) {
				// 	$.each(tableDatas, function (index, item) {
				// 		studentFlnkId.push(item.UserFLnkID)
				// 	});
				// }
				// sessionStorage.setItem('studentFlnkIds', studentFlnkId);
			})

			//个人历史成绩跳转
			$scope.gouserinfohis = function (Sfid) {
				location.href = "#/capacity?Sfid=" + Sfid + "&ClassFlnkid=" + $scope.CurrentClassID + "";
			}
			// 班级分析详细信息
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0201.ashx',
				data: {
					ClassFLnkID: $scope.CurrentClassID,
				}
			}).success(function (res) {
				$scope.ClassMsg = res.msg[0];
			})
		}).then(function () {
			// 龙虎榜
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0203.ashx',
				data: {
					ClassFlnkID: $scope.CurrentClassID
				}
			}).success(function (res) {
				$scope.orderBox = res.msg;
				$scope.topTen = _.where($scope.orderBox, { Type: '1' }) //  qian 10
				$scope.topTen = $scope.topTen.slice(0, 7);
				$scope.beforeTen = _.where($scope.orderBox, { Type: '2' }) // xiajiang 10
				$scope.beforeTen = $scope.beforeTen.slice(0, 7);
				$scope.upTen = _.where($scope.orderBox, { Type: '3' }) // 上升 10
				$scope.upTen = $scope.upTen.slice(0, 7);
				$scope.downTen = _.where($scope.orderBox, { Type: '4' }) // 下降 10
				$scope.downTen = $scope.downTen.slice(0, 7);
			})
		});

		// 点击班级获取信息
		$scope.selectCur = function (id, name) {
			$scope.CurrentClassID = id;
			$scope.ClassName = name
			$http.post($rootScope.baseUrl + '/Interface0201.ashx', {
				ClassFlnkID: id
			}).success(function (res) {
				$scope.ClassMsg = res.msg[0];
			})
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0203.ashx',
				data: {
					ClassFlnkID: id
				}
			}).success(function (res) {
				$scope.orderBox = res.msg;
				$scope.topTen = _.where($scope.orderBox, { Type: '1' }) //  qian 10
				$scope.topTen = $scope.topTen.slice(0, 7);
				$scope.beforeTen = _.where($scope.orderBox, { Type: '2' }) // xiajiang 10
				$scope.beforeTen = $scope.beforeTen.slice(0, 7);
				$scope.upTen = _.where($scope.orderBox, { Type: '3' }) // 上升 10
				$scope.upTen = $scope.upTen.slice(0, 7);
				$scope.downTen = _.where($scope.orderBox, { Type: '4' }) // 下降 10
				$scope.downTen = $scope.downTen.slice(0, 7);
			})
		};

		//  战斗力 和平均分
		$http.get($rootScope.baseUrl + '/Interface0200.ashx').success(function (data) {
			if (data.msg != '') {
				$scope.combatBox = data.msg[0]; // 战斗力
				$scope.Averagescore = data.msg[1]; // 平均分
				// 取出2个班的战斗力
				var combatBox = {
					ClassName: data.msg[0].ClassName,
					Pow: data.msg[0].Pow,
					PowGoodNum: data.msg[0].PowGoodNum,
					PowGradeAvg: data.msg[0].PowGradeAvg,
					PowPassNum: data.msg[0].PowPassNum,
					PowGradeOrder: data.msg[0].PowGradeOrder,
					OtherClassName: data.msg[1].ClassName,
					OtherPow: data.msg[1].Pow,
					OtherPowGoodNum: data.msg[1].PowGoodNum,
					OtherPowGradeAvg: data.msg[1].PowGradeAvg,
					OtherPowPassNum: data.msg[1].PowPassNum,
					OtherPowGradeOrder: data.msg[1].PowGradeOrder,
				}
				$scope.combatBoxList = combatBox;
				// 取出2个班的平均分
				var Averagescore = {
					ClassName: data.msg[0].ClassName,
					Sc: data.msg[0].Sc,
					ScGoodNum: data.msg[0].ScGoodNum,
					ScGradeAvg: data.msg[0].ScGradeAvg,
					ScGradeOrder: data.msg[0].ScGradeOrder,
					ScPassNum: data.msg[0].ScPassNum,
					OtherClassName: data.msg[1].ClassName,
					OtherSc: data.msg[1].Sc,
					OtherScGoodNum: data.msg[1].ScGoodNum,
					OtherScGradeAvg: data.msg[1].ScGradeAvg,
					OtherScGradeOrder: data.msg[1].ScGradeOrder,
					OtherScPassNum: data.msg[1].ScPassNum
				}
				$scope.AveragescoreList = Averagescore;
				$scope.victory = $scope.fail = $scope.pin = false;
				//  平均分比拼
				if (Averagescore.Sc > Averagescore.OtherSc) {
					$scope.fail = true
				} else if (Averagescore.Sc < Averagescore.OtherSc) {
					$scope.victory = true
				} else if (Averagescore.Sc == Averagescore.OtherSc) {
					$scope.pin = true
				}
				// 战斗力比拼
				if (combatBox.Pow > combatBox.OtherPow) {
					$scope.fail_Pow = true
				} else if (combatBox.Pow < combatBox.OtherPow) {
					$scope.victory_Pow = true
				} else if (combatBox.Pow == combatBox.OtherPow) {
					$scope.pin_Pow = true
				}
			} else {
				ngDialog.open({
					template:
							'<p>班级信息不全，无法对比</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					plain: true,
					className: 'ngdialog-theme-plain'
				});
				return false;
			}
		});

		$scope.tab = function (obj) {
			if (obj == 'grade') {
				$scope.isTabCon = false;
				$scope.isTab = true;
				$scope.currTab = true;
			} else {
				$scope.isTabCon = true;
				$scope.isTab = false;
				$scope.currTab = false;
			}
		}

		// 概要
		$http.get($rootScope.baseUrl + '/Interface0151.ashx').success(function (data) {
			var newarr = [];
			for (var i = 0 ; i < data.msg.length ; i++) {
				newarr.push({
					"PowerOrder": data.msg[i].PowerOrder,
					"ClassName": data.msg[i].ClassName,
					"ClassFlnkid": data.msg[i].ClassFlnkid,
					"Power": data.msg[i].Power,
					"AvgSc": data.msg[i].AvgSc,
					"GoodNum": data.msg[i].GoodNum,
					"PassRate": data.msg[i].PassRate,
					"UpNum": data.msg[i].UpNum,
					"DownNum": data.msg[i].DownNum,
					"IsOwner": data.msg[i].IsOwner
				})
			}
			$scope.gradelist = newarr;
		});

		//表格排序
		$scope.tablethKeys = ['PowerOrder', 'ClassName', 'Power', 'AvgSc', 'GoodNum', 'PassRate', 'UpNum', 'DownNum'];

	}];
});