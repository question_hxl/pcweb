﻿define([], function () {
	return ['$rootScope', '$http', '$scope', '$q', '$location', function ($rootScope, $http, $scope, $q, $location) {

		var getsearch = $location.search();//统一获取地址栏参数

		//中考概率近三年下拉框
		$scope.years = [{ 'id': 'A', 'name': '近三年' },
			{ 'id': 'B', 'name': '2013' },
			{ 'id': 'C', 'name': '2014' },
			{ 'id': 'D', 'name': '2015' }
		];
		//默认选中近三年
		$scope.inyear = $scope.years[0];

		//控制每一章
		$scope.zhangclick = { 'show': false };
		$scope.zhangclick = function (item) {
			getjie(item);
			if (item.IsKnowledge != 1) {
				item.show = !item.show;
			}
		};

		//控制每一节
		$scope.jieclick = { 'show': false };
		$scope.jieclick = function (veritem) {
			getzsd(veritem);
			if (veritem.IsKnowledge != 1) {
				veritem.show = !veritem.show;
			}
		};

		//控制每一单元
		$scope.unitclick = { 'show': false };
		$scope.unitclick = function (poiitem) {
			getunit(poiitem);
			if (poiitem.IsKnowledge != 1) {
				poiitem.show = !poiitem.show;
			}
		};


		//监听班级获取学生
		$scope.$watch('currentClass', function () {
			getStudentList();
		});

		//监听学生获取章
		$scope.$watch('currentStudent', function () {
			getzhang();
		});

		//获取班级
		$http.get($rootScope.baseUrl + '/Interface0150.ashx').success(function (data) {
			$scope.classes = data.msg;
		});

		//获取学生列表
		function getStudentList() {
			//$scope.classname = $scope.currentClass.name;
			$http.post($rootScope.baseUrl + '/Interface0045.ashx', {
				classId: $scope.currentClass.classId,
			}).then(function (resp) {
				$scope.studentList = resp.data.studentList;
			});
		}

		//获取章
		function getzhang() {
			//$scope.stuname = $scope.currentStudent.studentName;
			if ($scope.currentStudent != null) {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: 0,
					ChapterID: 0
				}).then(function (resp) {
					$scope.itemss = resp.data.msg;
					$scope.items = $scope.itemss;
				});
			} else {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: 0,
					ChapterID: 0
				}).then(function (resp) {
					$scope.itemss = resp.data.msg;
					$scope.items = $scope.itemss;
				});
			}
		}

		//获取节
		function getjie(chapter) {
			console.log(chapter);
			if ($scope.currentStudent != null) {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: chapter.Level,
					ChapterID: chapter.NodeID
				}).then(function (resp) {
					chapter.veritems = resp.data.msg;
					$scope.veritems = chapter.veritems;
				});
			} else {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: chapter.Level,
					ChapterID: chapter.NodeID
				}).then(function (resp) {
					chapter.veritems = resp.data.msg;
					$scope.veritems = chapter.veritems;
				});
			}
		}

		//获取知识点
		function getzsd(jie) {
			if ($scope.currentStudent != null) {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: jie.Level,
					ChapterID: jie.NodeID
				}).then(function (resp) {
					jie.poiitems = resp.data.msg;
					$scope.poiitems = jie.poiitems;
				});
			} else {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: jie.Level,
					ChapterID: jie.NodeID
				}).then(function (resp) {
					jie.poiitems = resp.data.msg;
					$scope.poiitems = jie.poiitems;
				});
			}
		}

		//获取单元
		function getunit(unit) {
			if ($scope.currentStudent != null) {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: unit.Level,
					ChapterID: unit.NodeID
				}).then(function (resp) {
					unit.unititems = resp.data.msg;
					$scope.unititems = unit.unititems;
				});
			} else {
				$http.post($rootScope.baseUrl + '/Interface0171.ashx', {
					ClassFlnkID: $scope.currentClass.classId,
					UserFlnkID: $scope.currentStudent.studentId,
					Level: unit.Level,
					ChapterID: unit.NodeID
				}).then(function (resp) {
					unit.unititems = resp.data.msg;
					$scope.unititems = unit.unititems;
				});
			}
		}


	}]
})