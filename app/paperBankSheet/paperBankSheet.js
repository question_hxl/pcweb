/**
 * Created by Administrator on 2017/1/11 0011.
 */
define([], function () {
    return ['$rootScope', '$http', '$scope', '$state', '$q', '$location', '$timeout', 'ngDialog', 'constantService',function ($rootScope, $http, $scope, $state, $q, $location, $timeout, ngDialog, constantService) {
        $scope.paper = $location.search();
        var unifiedId = $scope.paper.unifiedId;
        $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
        $scope.examname = $location.search().examname;
        $scope.getAnswerSheet = function(){
            $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                Proc_name: 'GetAnswerSheet',
                examId: $scope.paper.FLnkID,
                SheetName: '',
                sheetId: ""
            }).then(function(res){
                if(res.data.code === 0 && _.isArray(res.data.msg) && res.data.msg.length > 0) {
                    $scope.sheetList = res.data.msg;
                }else {
                    $scope.sheetList = [];
                }
            }, function(){
                $scope.sheetList = [];
            });
        };
        $scope.getAnswerSheet();
        //返回
        $scope.back = function () {
            $state.go('myApp.examCenter');
        };
        $scope.queueList = [
            {name: '首页',url:'myApp.masterHome'},
            {name: '考试中心',url:'myApp.examCenter'},
            {name: '生成答题卡',url:'myApp.paperBankSheet'}];

        $scope.editAnswerSheet = function (sheet) {
            ngDialog.close();
            window.open('#/buildAnswerSheet?sheetId=' + sheet.sheetId + (unifiedId ? ('&unifiedId=' + unifiedId) : '') +'&action=' + $scope.paper.action + '&MarkMode=' + $scope.paper.MarkMode + '&isOpenNewWin=1');
        };
        $scope.rebuildSheet = function () {
            ngDialog.close();
            window.open('#/buildAnswerSheet?examId=' + $scope.paper.FLnkID + '&examFID=' + $scope.paper.FID + '&isClear=1'+ (unifiedId ? ('&unifiedId=' + unifiedId) : '') +'&action=' + $scope.paper.action + '&MarkMode=' + $scope.paper.MarkMode + '&isOpenNewWin=1');
        };
        $scope.buildSheetFromPaper = function(){
            window.open('#/buildAnswerSheet?examId=' + $scope.paper.FLnkID + '&examFID=' + $scope.paper.FID + (unifiedId ? ('&unifiedId=' + unifiedId) : '') +'&action=' + $scope.paper.action + '&MarkMode=' + $scope.paper.MarkMode + '&isOpenNewWin=1');
        };
        $scope.buildUnit = function(){
            window.open('#/buildAnswerSheet?examId=' + $scope.paper.FLnkID + '&examFID=' + $scope.paper.FID + '&isUnit=1' + (unifiedId ? ('&unifiedId=' + unifiedId) : '') +'&action=' + $scope.paper.action + '&MarkMode=' + $scope.paper.MarkMode + '&isOpenNewWin=1');
        };
        $scope.delAnswerSheet = function(sheet){
            ngDialog.open({
                template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
                '<div class="nextMarking-title">请确认是否删除该答题卡</div>'+
                '<div class="nextMarking-btn" style="margin:0 40px" ng-click="sure()">确定</div>'+
                '<div class="nextMarking-btn" ng-click="closeThisDialog()">取消</div>'
                +'</div>'+'</div>',
                className: 'ngdialog-theme-default ngdialog-related',
                scope: $scope,
                closeByDocument: false,
                plain: true
            });
            $scope.sure = function(){
                ngDialog.close();
                $http.post('BootStrap/Interface/delAnswerSheet.ashx', {
                    sheetId : sheet.sheetId
                }).then(function(res){
                    if(res.data.code === 0){
                        $scope.getAnswerSheet();
                        constantService.alert('删除成功！');
                    }else {
                        constantService.alert('删除失败！');
                    }
                })
            }
        };
        //查看原卷
        $scope.thispath = $location.path();
        $scope.goPaper = function () {
            window.open('#/composing?pathss=' + $scope.thispath + '&ExamFlnkID=' + $scope.paper.FLnkID + '&action=0&isExamCenter=' + 0 + '&isOpenNewWin=1');
        };
    }]
});