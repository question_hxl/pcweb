/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var Event = window.Event = {};
    Event.notices = [];
    Event.once = function(e, fn){
        var event = {};
        event[e] = fn;
        event['type'] = 'ONCE';
        Event.notices.push(event);
        return Event;
    };
    Event.on = function(e, fn){
        var event = {};
        event[e] = fn;
        Event.notices.push(event);
        return Event;
    };
    Event.off = function(e){
        var del = function(){
            var index = _.findIndex(Event.notices, function(n){
                return !!n[e]
            });
            if(index !== -1) {
                Event.notices.splice(index, 1);
            }
            if(_.findIndex(Event.notices, function(n){
                    return !!n[e]
                }) >= 0) {
                del();
            }
        };
        del();
        return Event;
    };
    Event.trigger = function(e, data){
        for(var i = 0; i < Event.notices.length; i++) {
            var notice = Event.notices[i];
            if(notice[e]) {
                if(notice.type === 'ONCE') {
                    notice[e](data);
                    Event.off(e);
                }else {
                    notice[e](data);
                }
            }
        }
        return Event;
    };
})();