/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetUtil = window.sheetUtil = window.sheetUtil || {};
    var refreshTimer = null;
    sheetUtil.PAGE_SIZE_A3 = 175;
    sheetUtil.PAGE_SIZE_8K = 162;
    sheetUtil.INLINE_TAGS = ['a', 'b','bdo', 'big', 'br', 'code', 'em', 'i', 'img', 'input', 'label', 'q' ,'s', 'small', 'span', 'strike', 'strong', 'sub', 'sup', 'u'];
    sheetUtil.html2standard = function(){
        var node;
        try{
            node = $(html);
        }catch(e) {
            node = $('<div>').html(html);
        }
        var str = '';
        node.each(function(index, item){
            str += item.outerHTML;
        });
        if(str === '') {
            str = '<div>' + html + '</div>';
        }
        return str;
    };
    sheetUtil.PAGE_SIZE = {
        A3: {
            width: 420,
            height: 294
        },
        A4: {
            width: 210,
            height: 295
        },
        '8K': {
            width: 390,
            height: 267
        },
        '16K': {
            width: 195,
            height: 268
        }
    };
    sheetUtil.MM2PX_RATIO = getMM2PXRatio();
    sheetUtil.isPageSizeLarge = function(pageSize){
        return (pageSize === 'A4' || pageSize === 'A3');
    };
    sheetUtil.typeNameMap = {
        'OPTION_GROUP': '选择题',
        'TITLE': '标题',
        'FILL_BLANK': '填空题',
        'BLANK': '空行',
        'SOLVE': '解答题',
        'SOLVE_MAIN': '解答题',
        'SOLVE_HALF': '解答题',
        'COMPOSITION': '作文题',
        'COMPOSITION_HALF': '作文题',
        'COMPOSITION_WITH_SCORE': '作文题'
    };
    sheetUtil.cloneObj = function(dest, source){
        var getType = function(o){
            var _t;
            return ((_t = typeof o) === 'object' ? o === null && 'null' ||
            Object.prototype.toString.call(o).slice(8, -1): _t).toLowerCase();
        };
        for(var p in source){
            if(getType(source[p]) === 'array' || getType(source[p]) === 'object'){
                dest[p] = getType(source[p]) === 'array' ? [] : {};
                sheetUtil.cloneObj(dest[p], source[p]);
            }else {
                dest[p] = source[p];
            }
        }
    };
    /**
     * 计算html高度，html中可能含有图片。为满足大部分场景以及同步获取高度的需求，先给出一个不准确的高度，再
     * 监听图片加载成功，通过回调修改高度，触发页面重新绘制。
     * @param $dom         dom节点
     * @param width        宽度
     * @param lineheight   设置文字行高
     * @param fn           回调方法
     * @returns {*}
     */
    sheetUtil.calcDOMHeight = function($dom, width, lineheight, fn){
        var $parent = $('.buildAnswerCard') || $('body');
        var $clone = $($dom).clone();
        $clone.css({
            fontFamily: '宋体',
            fontSize: '14px',
            opacity: '0',
            width: width + 'mm',
            minWidth: 0,
            'display': 'inline-block',
            'word-break': 'break-all',
			'position': 'relative'
        }).appendTo($parent);
        if(lineheight) {
            $clone.css({
                lineHeight: lineheight
            });
        }
        function checkReCalc(){
            if(loaded === total) {
                fn && fn($clone.height());
                $clone.remove();
            }
        }
        var $imgs = $clone.find('img');
        var loaded = 0, cached = 0;
        var total = $imgs.length;
        if(total > 0) {
            $imgs.each(function(){
                if($(this).get(0).complete) {
                    cached ++;
                    if(cached === total) {
                        setTimeout(function(){
                            fn && fn($clone.height());
                            $clone.remove();
                        }, 10);
                    }
                }else {
                    $(this).on('load', function(){
                        loaded ++;
                        checkReCalc();
                    }).on('error', function(){
                        loaded++;
                        checkReCalc();
                    });
                }
            });
        }else {
            var heightPlain = $clone.height();
            $clone.remove();
            return heightPlain;
        }
        var height = $clone.height();
        // $clone.remove();
        return height;
    };
    sheetUtil.getContentWidth = function(pageSize){
        return this.isPageSizeLarge(pageSize) ? sheetUtil.PAGE_SIZE_A3 : sheetUtil.PAGE_SIZE_8K;
    };
    sheetUtil.parseContentInline = function(text, pre, noSplitor, isOption){
        var context = '';
        var splitor = noSplitor ? '' : '.';
        try{
            var tagName = $(text).get(0).tagName;
            if(tagName && !!$(text).html() && text.startsWith('<')) {
                var closeTagIndex = text.indexOf('>');
                if(this.isInlineTag(tagName)) {
                    context = pre + splitor + text;
                }else {
                    if(isOption) {
                        if($(text).length === 1 && $.trim(text).endsWith('>')) {
                            context = pre + splitor + text.slice(closeTagIndex + 1, - (tagName.length + 4));
                        }else {
                            context = text.slice(0, closeTagIndex + 1) + pre + splitor + text.slice(closeTagIndex + 1);
                            context = '<div style="display:inline-block;">' + context + '</div>';
                        }
                    }else {
                        context = text.slice(0, closeTagIndex + 1) + pre + splitor + text.slice(closeTagIndex + 1);
                    }
                }
            }else {
                context = (text && pre + splitor + text) || '';
            }
        }catch(e) {
            context = (text && pre + splitor + text) || '';
        }
        // context = context.replace(/&nbsp;/g, ' ');
        context = context.replace(/\n/g, '');
        context = context.replace(/\r\n/ig, '');
        context = context.replace(/\r/ig, '');
		context = context.replace(/↵/ig, '');
		context = context.replace(/undefined/ig, '');
        if(isOption) {
            context = context.replace(/<br>/ig, '');
            context = context.replace(/<br.\/>/ig, '');
        }
        return context;
    };
    sheetUtil.isInlineTag = function(tagname){
        return $.inArray(tagname.toLowerCase(), sheetUtil.INLINE_TAGS) >= 0;
    };
    sheetUtil.refreshDelay = function(isIgnore){
        clearTimeout(refreshTimer);
        refreshTimer = setTimeout(function(){
            Event.trigger('REBUILD_PAGE', isIgnore);
        }, 500);
    };

	sheetUtil.guessHeightWithScore = function(score){
		score = +score || 0;
		var MIN_HEIGHT = 20, MAX_HEIGHT = 80;
		var height = score * 6 + 4.5;
		return height < MIN_HEIGHT ? MIN_HEIGHT : (height > MAX_HEIGHT ? MAX_HEIGHT : height);
	};

    function getMM2PXRatio(){
        var $box = $('<div>').css({
            width: '10mm',
            height: '10mm',
            opacity: '0',
            lineHeight: '6mm'
        }).appendTo($('body'));
        var ratio = $box.width() / 10;
        $box.remove();
        return ratio;
    }
})();