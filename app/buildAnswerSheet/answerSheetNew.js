/**
 * Created by 贺小雷 on 2017/01/04.
 */
define(['EventBus', 'sheetUtil', 'Class', 'jquery-barcode', 'AnswerTip', 'BlackBlock', 'Blank', 'Composition', 'ExamName', 'FillBlank', 'OptionGroup', 'Page',
	'ScoreBar', 'SecretBox', 'SheetComp', 'Solve', 'Title', 'XZTComp', 'content4Unit', 'Paragraph', 'UnitFillBlank', 'ng-file-upload'], function () {
	return ['$scope', '$http', '$rootScope', '$location', '$state', '$q', 'paperService', 'constantService', 'ngDialog', 'apiCommon',
		function ($scope, $http, $rootScope, $location, $state, $q, paperService, constantService, ngDialog, apiCommon) {
			loadCss('assets/css/buildAnswerSheet.css');
			var search = $location.search();
			var EXAMID = search.examId;
			var EXAMNO = search.examFID;
			var SHEETID = search.sheetId;
			var ISCLEAR = search.isClear == '1';
			var ISUNIT = search.isUnit === '1';
			var unifiedId = search.unifiedId || '';
			var action = search.action;
			$scope.canSaveAnother = !!SHEETID;
			$scope.isBuildFromPaper = !!EXAMID;
			$scope.isModified = false;
			var page;
			var examData = {};
			$scope.markMode = [{
				value: false,
				title: '先阅后扫（默认）'
			}, {
				value: true,
				title: '先扫后阅'
			}];
			$scope.secretMode = 'PAINT';
			$scope.isPagination = true;
			$scope.isSchool = true;
			$scope.optionGroupMode = 'AUTO';
			sheetUtil.optionGroupMode = 'AUTO';

			$scope.contentHeight = $(window).height();
			$(window).off('resize.sheet').on('resize.sheet', function (e) {
				$scope.$apply(function () {
					$scope.contentHeight = $(window).height();
				});
			});

			window.Event.off('REBUILD_PAGE').on('REBUILD_PAGE', function (isIgnore) {
				buildPage(true, !!isIgnore);
			});

			window.Event.off('PAGE_ELE_DEACTIVE').on('PAGE_ELE_DEACTIVE', function (data) {
				_.each(page.components, function (comp) {
					if (comp.deactive && _.isFunction(comp.deactive)) {
						comp.deactive();
					}
				});
			});

			Event.off('RESET_PAPER_NO').on('RESET_PAPER_NO', function (comp) {
				ngDialog.open({
					template: 'setPaperNo',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog reset-paperno',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.done = function () {
								ngDialog.close();
								comp.setPaperNo($scope.newPaperNo);
								$scope.isModified = true;
							};
						};
					}
				});
			});

			window.Event.off('MODIFY_SOLVE_SCORE').on('MODIFY_SOLVE_SCORE', function (comp) {
				ngDialog.open({
					template: 'editSolveScore',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog modify-comp',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.mode = 'normal';
							$scope.height = 50;
							$scope.compType = comp.type;
							$scope.numPerLine = comp.type === 'FILL_BLANK' ? comp.orders.length : 1;
							$scope.numPerQst = comp.type === 'FILL_BLANK' ? comp.count : 1;
							$scope.num = 800;
							$scope.compositionScore = comp.score;
							$scope.score = comp.score;
							$scope.done = function () {
								ngDialog.close();
								if (comp.type === $scope.compType) {
									if (comp.type === 'FILL_BLANK') {
										//如果是一行2题，只可修改为一行一题，修改时需要替换掉当前题，并在后面插入一题
										if (comp.orders.length === 2) {
											if (+$scope.numPerLine === 1) {
												page.replaceComp(comp.orderInPage, new sheetElement.FillBlankQ([comp.orders[0]], $scope.numPerQst, $scope.currentPageSize));
												page.insertAfterComp(comp.orderInPage, new sheetElement.FillBlankQ([comp.orders[1]], $scope.numPerQst, $scope.currentPageSize))
											}
										} else {
											var orders = _.range(+comp.orders[0], +comp.orders[0] + +$scope.numPerLine);
											page.replaceComp(comp.orderInPage, new sheetElement.FillBlankQ(orders, $scope.numPerQst, $scope.currentPageSize));
										}
									} else if (comp.type === 'SOLVE_MAIN') {
										var qstScore = 0;
										var sub = [];
										if ($scope.mode === 'normal') {
											qstScore = +$scope.score;
										} else {

											var subScores = ($scope.subScore || '').replace(/，/g, ',').split(',');
											_.each(subScores, function (score) {
												if (+score) {
													sub.push({
														score: +score || 0
													});
													qstScore += (+score || 0);
												}
											});
										}
										comp.setScoreAndMode($scope.mode, qstScore, sub);
										var pComp = page.findComponentByOrder(comp.orderInPage);
										pComp.mode = $scope.mode;
										pComp.score = qstScore;
										pComp.sub = sub;
									} else if (comp.type === 'COMPOSITION_WITH_SCORE') {
										var pComp = page.findComponentByOrder(comp.orderInPage);
										pComp.setScore($scope.compositionScore);
										pComp.setNum($scope.num);
									}
								} else {
									if ($scope.compType === 'FILL_BLANK') {
										var orders = _.range($scope.startOrder, $scope.startOrder + +$scope.numPerLine);
										if (ISUNIT) {
											if (comp.type === 'SOLVE_MAIN') {
												orders = [comp.order];
											}
											page.replaceComp(comp.orderInPage, new sheetElement.UnitFillBlank(comp.innerHtml, orders, $scope.numPerQst, $scope.currentPageSize));
										} else {
											page.replaceComp(comp.orderInPage, new sheetElement.FillBlankQ(orders, $scope.numPerQst, $scope.currentPageSize));
										}
									} else if ($scope.compType === 'SOLVE_MAIN') {
										var qstScore = 0;
										var sub = [];
										if ($scope.mode === 'normal') {
											qstScore = +$scope.score;
										} else {
											var subScores = ($scope.subScore || '').replace(/，/g, ',').split(',');
											_.each(subScores, function (score) {
												if (+score) {
													sub.push({
														score: +score || 0
													});
													qstScore += (+score || 0);
												}
											});
										}
										page.replaceComp(comp.orderInPage, new sheetElement.SolveFull($scope.startOrder, qstScore, sub, $scope.mode, '', ''));
									} else if ($scope.compType === 'COMPOSITION_WITH_SCORE') {
										page.replaceComp(comp.orderInPage, new sheetElement.Composition($scope.compositionScore, $scope.num));
									}
								}
								buildPage(true);
							};
						}
					}
				})
			});

			window.Event.off('UPDATE_PARENT_SOLVE_HTML').on('UPDATE_PARENT_SOLVE_HTML', function (data) {
				var solveFull = page.findComponentByOrder(data);
				solveFull.updateHtml();
			});

			window.Event.off('ADD_PARENT_SOLVE_HEIGHT').on('ADD_PARENT_SOLVE_HEIGHT', function (data) {
				var parentInPage = _.find(page.components, function (item) {
					return item.orderInPage === data.order;
				});
				if (parentInPage) {
					parentInPage.height += data.height;
				}
			});

			Event.off('MERGE_COMP').on('MERGE_COMP', function (comp) {
				if (comp.orders.length === 2) {
					//拆分1行1题成1行2题
					var orders = comp.orders.slice();
					comp.setOrder(orders.slice(0, 1));
					page.insertAfterComp(comp.orderInPage, new sheetElement.FillBlankQ(orders.slice(1), 1, $scope.currentPageSize));
					buildPage(true);
				} else {
					//合并两题成1行
					var currentCompIndex = _.findIndex(page.components, function (c) {
						return c.orderInPage === comp.orderInPage;
					});
					var nextComp = page.components[currentCompIndex + 1];
					if (nextComp && nextComp.type === 'FILL_BLANK' && nextComp.orders.length === 1) {
						//如果下个组件是填空题，执行合并操作
						comp.setOrder([comp.orders[0], comp.orders[0] + 1]);
						//删除被合并的组件
						page.removeComponent(nextComp.orderInPage);
						buildPage(true);
					}
				}
			});

			Event.off('CHANGE_FB_SPACE').on('CHANGE_FB_SPACE', function (comp) {
				ngDialog.open({
					template: 'changeFbSpace',
					className: 'ngdialog-theme-default',
					appendClassName: 'simple-toast',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.currentSpaceCount = comp.count;
							$scope.done = function () {
								comp.setCount(+$scope.currentSpaceCount);
								ngDialog.close();
								buildPage(true);
							};
						};
					}
				});
			});

			Event.off('UPDATE_CURRENT_COMP').on('UPDATE_CURRENT_COMP', function (data) {
				$scope.currentComponentOrder = data;
			});

			Event.off('DEL_COMP').on('DEL_COMP', function (compOrder) {
				if (compOrder !== -1) {
					if ($scope.currentComponentOrder === compOrder) {
						$scope.currentComponentOrder = -1;
					}
					page.removeComponent(compOrder);
					buildPage(true);
				}
			});

			Event.off('ADD_COMP').on('ADD_COMP', function (comp) {
				if (comp) {
					$scope.compName = sheetUtil.typeNameMap[comp.type];
					if (comp.type === 'TITLE') {
						var cloneObj = {};
						sheetUtil.cloneObj(cloneObj, comp);
						delete cloneObj.orderInPage;
						page.insertAfterComp(comp.orderInPage, cloneObj);
						buildPage(true);
					} else if (comp.type === 'OPTION_GROUP') {
						ngDialog.open({
							template: 'componentCopy',
							className: 'ngdialog-theme-default',
							appendClassName: 'common-dialog answer-sheet-dialog add-solve',
							closeByDocument: false,
							scope: $scope,
							controller: function ($scope, $http) {
								return function () {
									$scope.sOrder = comp.num + comp.startOrder;
									$scope.eOrder = $scope.sOrder + comp.num;
									$scope.done = function () {
										ngDialog.close();
										page.insertAfterComp(comp.orderInPage, new sheetElement.OptionGroup($scope.eOrder - $scope.sOrder, 1, $scope.sOrder, comp.pageSize));
										buildPage(true);
									};
								}
							}
						});
					} else if (comp.type === 'FILL_BLANK') {
						ngDialog.open({
							template: 'componentCopy',
							className: 'ngdialog-theme-default',
							appendClassName: 'common-dialog answer-sheet-dialog add-solve',
							closeByDocument: false,
							scope: $scope,
							controller: function ($scope, $http) {
								return function () {
									$scope.sOrder = comp.orders[comp.orders.length - 1] + 1;
									$scope.eOrder = $scope.sOrder + 1;
									$scope.done = function () {
										ngDialog.close();
										var count = $scope.eOrder - $scope.sOrder + 1;
										if (comp.orders.length === 1) {
											for (var i = 0; i < count; i++) {
												page.insertAfterComp(comp.orderInPage, new sheetElement.FillBlankQ([$scope.sOrder + i], comp.count, comp.pageSize));
											}
										} else {
											var half = Math.floor(count / 2);
											var left = count % 2;
											if (left) {
												page.insertAfterComp(comp.orderInPage, new sheetElement.FillBlankQ([$scope.eOrder], comp.count, comp.pageSize));
											}
											for (var j = 0; j < half; j++) {
												page.insertAfterComp(comp.orderInPage, new sheetElement.FillBlankQ([$scope.sOrder + j * 2, $scope.sOrder + j * 2 + 1], comp.count, comp.pageSize));
											}
										}
										buildPage(true);
									};
								}
							}
						});
					} else if (comp.type === 'SOLVE_MAIN' || comp.type === 'SOLVE_HALF') {
						ngDialog.open({
							template: 'componentCopy',
							className: 'ngdialog-theme-default',
							appendClassName: 'common-dialog answer-sheet-dialog add-solve',
							closeByDocument: false,
							scope: $scope,
							controller: function ($scope, $http) {
								return function () {
									$scope.sOrder = comp.order + 1;
									$scope.eOrder = $scope.sOrder + 1;
									$scope.done = function () {
										ngDialog.close();
										var pComp = page.findComponentByOrder(comp.orderInPage);
										var count = $scope.eOrder - $scope.sOrder + 1;
										for (var i = count - 1; i >= 0; i--) {
											var cloneObj = new sheetElement.SolveFull(pComp.order + i, pComp.score, pComp.sub, pComp.mode, pComp.height,
												pComp.img, pComp.mainHtml, pComp.halfHtml, pComp.lineheight);
											page.insertAfterComp(comp.orderInPage, cloneObj);
											page.insertAfterComp(comp.orderInPage, new sheetElement.Blank(2));
										}
										buildPage(true);
									};
								}
							}
						});
					}
				}
			});

			Event.off('ADD_XZT_COMP').on('ADD_XZT_COMP', function (comp) {
				ngDialog.open({
					template: 'addXZTTmpl',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog insert-img',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.done = function () {
								comp.addXZTNode($scope.xztOrders);
								ngDialog.close();
								$scope.isModified = true;
							};
						};
					}
				});
			});

			Event.off('INSERT_IMG').on('INSERT_IMG', function (comp) {
				var currentComp = page.findComponentByOrder(comp.orderInPage);
				var currentQst = getQstByOrder(currentComp.order);
				var $title = $('<p>').html(currentQst && currentQst.title || '');
				$scope.qstImgList = [];
				var $imgs = $title.find('img');
				$imgs.each(function () {
					$scope.qstImgList.push($(this).attr('src'));
				});
				ngDialog.open({
					template: 'img-upload',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog insert-img',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope, $http, Upload) {
						return function () {
							$scope.selectedImgs = [];
							$scope.choseImg = function (img) {
								if ($scope.isImgSelected(img)) {
									var index = _.findIndex($scope.selectedImgs, function (item) {
										return item === img;
									});
									$scope.selectedImgs.splice(index, 1);
								} else {
									$scope.selectedImgs.push(img);
								}
							};
							$scope.isImgSelected = function (img) {
								return !!_.find($scope.selectedImgs, function (item) {
									return item === img;
								});
							};
							$scope.done = function () {
								ngDialog.close();
								$scope.isModified = true;
								if ($scope.imgUpload) {
									Upload.upload({
										url: $rootScope.baseUrl + '/Interface0001C.ashx',
										data: [$scope.imgUpload]
									}).then(function (res) {
										if (res.data.code === 0) {
											$('<img>').attr('src', res.data.msg).appendTo(comp.$dom.find('.solve-qst-content'));
										} else {
											constantService.showSimpleToast('图片上传失败，请重试！');
										}
									}, function () {
										constantService.showSimpleToast('图片上传失败，请重试！');
									});
									_.each($scope.selectedImgs, function (imgSrc) {
										$('<img>').attr('src', imgSrc).appendTo(comp.$dom.find('.solve-qst-content'));
									});
								} else {
									_.each($scope.selectedImgs, function (imgSrc) {
										$('<img>').attr('src', imgSrc).appendTo(comp.$dom.find('.solve-qst-content'));
									});
								}
							};
						};
					}
				})
			});

			window.Event.off('MODIFY_OPTION_GROUP').on('MODIFY_OPTION_GROUP', function (comp) {
				ngDialog.open({
					template: 'modifyOption',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog modify-option',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.optionCount = comp.num;
							$scope.compName = sheetUtil.typeNameMap[comp.type];
							$scope.mode = comp.optionMode || 'normal';
							$scope.count = comp.optionCount;
							$scope.done = function () {
								ngDialog.close();
								comp.setCount($scope.optionCount);
								comp.setOptionMode(+$scope.count, $scope.mode);
								buildPage(true);
							};
						};
					}
				});
			});

			Event.off('MODIFY_UNIT_FILL_BLANK').on('MODIFY_UNIT_FILL_BLANK', function (comp) {
				ngDialog.open({
					template: 'modifyUnitFillBlank',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog modify-option',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.compType = comp.type;
							$scope.currentSpaceCount = comp.count;
							$scope.mode = 'normal';
							$scope.done = function () {
								ngDialog.close();
								if ($scope.compType === comp.type) {
									comp.setCount(+$scope.currentSpaceCount);
								} else {
									if ($scope.compType === 'SOLVE_MAIN') {
										var qstScore = 0;
										var sub = [];
										if ($scope.mode === 'normal') {
											qstScore = +$scope.score;
										} else {
											var subScores = ($scope.subScore || '').replace(/，/g, ',').split(',');
											_.each(subScores, function (score) {
												if (+score) {
													sub.push({
														score: +score || 0
													});
													qstScore += (+score || 0);
												}
											});
										}
										page.replaceComp(comp.orderInPage, new sheetElement.SolveFull(+comp.order, qstScore, sub, $scope.mode, '', '', comp.html, ''));
									}
								}
								buildPage(true);
							};
						}
					}
				});
			});

			window.Event.off('SET_LINE_HEIGHT').on('SET_LINE_HEIGHT', function (data) {
				var currentComp = page.findComponentByOrder(data.comp.orderInPage);
				if (currentComp.type === 'SOLVE' || currentComp.type === 'SOLVE_MAIN' || currentComp.type === 'SOLVE_HALF') {
					currentComp.setLineHeight(data.lh + 'mm');
				}
			});

			$scope.deactiveAllComp = function () {
				Event.trigger('PAGE_ELE_DEACTIVE');
			};

			$scope.addQuestionTitle = function () {
				if ($scope.currentComponentOrder >= 0) {
					page.insertAfterComp($scope.currentComponentOrder, new sheetElement.Title());
				} else {
					page.pushComponent(new sheetElement.Title());
				}
				buildPage(true);
			};
			$scope.addMultipeChoice = function (cb) {
				ngDialog.open({
					template: 'addOptionGroup',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog add-option',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.count = 4;
							$scope.mode = 'normal';
							$scope.done = function () {
								ngDialog.close();
								var num = +$scope.num;
								if (!num) {
									constantService.showSimpleToast('请输入数量！');
									return;
								}
								if (cb) {
									cb(new sheetElement.OptionGroup(num, 1, $scope.startOrder, $scope.currentPageSize, +$scope.count, $scope.mode));
								} else {
									if ($scope.currentComponentOrder >= 0) {
										page.insertAfterComp($scope.currentComponentOrder, new sheetElement.OptionGroup(num, 1, $scope.startOrder, $scope.currentPageSize, +$scope.count, $scope.mode));
									} else {
										page.pushComponent(new sheetElement.OptionGroup(num, 1, $scope.startOrder, $scope.currentPageSize, +$scope.count, $scope.mode));
									}
									$scope.startOrder += +num;
									buildPage(true);
								}
							};
						}
					}
				});
			};
			$scope.addFillBlank = function (cb) {
				ngDialog.open({
					template: 'addFillEmptyQst',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog add-fill-blank',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.numPerLine = 1;
							$scope.numPerQst = 1;
							$scope.done = function () {
								ngDialog.close();
								var orders = _.range($scope.startOrder, $scope.startOrder + +$scope.numPerLine);
								if (cb) {
									cb(new sheetElement.FillBlankQ(orders, $scope.numPerQst, $scope.currentPageSize));
								} else {
									$scope.startOrder += +$scope.numPerLine;
									if ($scope.currentComponentOrder >= 0) {
										page.insertAfterComp($scope.currentComponentOrder, new sheetElement.FillBlankQ(orders, $scope.numPerQst, $scope.currentPageSize));
									} else {
										page.pushComponent(new sheetElement.FillBlankQ(orders, $scope.numPerQst, $scope.currentPageSize));
									}
									buildPage(true);
								}
							};
						}
					}
				});
			};
			$scope.addSolveQst = function (cb) {
				ngDialog.open({
					template: 'addSolveQst',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog add-solve',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope, $http, Upload) {
						return function () {
							$scope.mode = 'normal';
							$scope.height = 50;
							$scope.done = function () {
								ngDialog.close();
								if ($scope.file) {
									Upload.upload({
										url: $rootScope.baseUrl + '/Interface0001C.ashx',
										data: [$scope.file]
									}).then(function (res) {
										if (res.data.code === 0) {
											buildSolve(res.data.msg);
										} else {
											buildSolve();
										}
									}, function () {
										buildSolve();
									});
								} else {
									buildSolve();
								}
								function buildSolve(imgSrc) {
									var img = imgSrc && {src: imgSrc} || '';
									var qstScore = 0;
									var sub = [];
									if ($scope.mode === 'normal') {
										qstScore = +$scope.score;
									} else {
										var subScores = ($scope.subScore || '').replace(/，/g, ',').split(',');
										_.each(subScores, function (score) {
											if (+score) {
												sub.push({
													score: +score || 0
												});
												qstScore += (+score || 0);
											}
										});
									}
									if (cb) {
										cb(new sheetElement.Blank(2));
										cb(new sheetElement.SolveFull($scope.startOrder, qstScore, sub, $scope.mode, +$scope.height, img));
									} else {
										if ($scope.currentComponentOrder >= 0) {
											page.insertAfterComp($scope.currentComponentOrder, new sheetElement.SolveFull($scope.startOrder, qstScore, sub, $scope.mode, +$scope.height, img));
											page.insertAfterComp($scope.currentComponentOrder, new sheetElement.Blank(2));
										} else {
											page.pushComponent(new sheetElement.Blank(2));
											page.pushComponent(new sheetElement.SolveFull($scope.startOrder, qstScore, sub, $scope.mode, +$scope.height, img));
										}

										$scope.startOrder += 1;
										buildPage(true);
									}
								}
							};
						}
					}
				});
			};
			$scope.addComposition = function (cb) {
				ngDialog.open({
					template: 'addCompositionTmpl',
					className: 'ngdialog-theme-default',
					appendClassName: 'common-dialog answer-sheet-dialog add-composition',
					closeByDocument: false,
					scope: $scope,
					controller: function ($scope) {
						return function () {
							$scope.num = 800;
							$scope.done = function () {
								ngDialog.close();
								if (cb) {
									cb(new sheetElement.Composition($scope.compositionScore, +$scope.num));
								} else {
									if ($scope.currentComponentOrder >= 0) {
										page.insertAfterComp($scope.currentComponentOrder, new sheetElement.Composition($scope.compositionScore, +$scope.num));
										page.insertAfterComp($scope.currentComponentOrder, new sheetElement.Blank(2));
									} else {
										page.pushComponent(new sheetElement.Blank(2));
										page.pushComponent(new sheetElement.Composition($scope.compositionScore, +$scope.num));
									}
									$scope.startOrder += 1;
									buildPage(true);
								}
							};
						}
					}
				});
			};
			$scope.addBlankPlaceholder = function () {
				page.pushComponent(new sheetElement.Blank(2));
				buildPage(true);
			};
			$scope.doPrint = function () {
				Event.trigger('PAGE_ELE_DEACTIVE');
				$('.bac-ctx').jqprint({
					operaSupport: false,
					importCss: true,
					cssHref: 'assets/css/buildAnswerSheet.css'
				});
			};
			$scope.changePageSize = function (size) {
				page.setPageSize(size);
				$scope.currentPageSize = size;
				// _.each(page.components, function(item){
				//     if(item.setPageSize) {
				//         item.setPageSize(size);
				//     }
				// });
				buildPage(true);
			};
			$scope.insertBeforePlaceholder = function () {
				if ($scope.currentComponentOrder !== -1) {
					page.insertComponent($scope.currentComponentOrder, new sheetElement.Blank());
					buildPage(true);
				}
			};
			$scope.addCompHeight = function () {
				if ($scope.currentComponentOrder !== -1) {
					var comp = _.find(page.components, function (item) {
						return item.orderInPage === $scope.currentComponentOrder;
					});
					if (comp.addHeight) {
						comp.addHeight(1);
						sheetUtil.refreshDelay(false);
					}
				}
			};
			$scope.minusCompHeight = function () {
				if ($scope.currentComponentOrder !== -1) {
					var comp = _.find(page.components, function (item) {
						return item.orderInPage === $scope.currentComponentOrder;
					});
					if (comp.addHeight) {
						comp.addHeight(-1);
						sheetUtil.refreshDelay(false);
					}
				}
			};
			$scope.removeComponent = function () {
				if ($scope.currentComponentOrder !== -1) {
					page.removeComponent($scope.currentComponentOrder);
					buildPage(true);
				}
			};
			$scope.setLineHeight = function (lineheight, e) {
				e.preventDefault();
				e.stopPropagation();
				$scope.isShowLineHeightBox = false;
				if ($scope.currentComponentOrder !== -1) {
					var currentComp = page.findComponentByOrder($scope.currentComponentOrder);
					if (currentComp.type === 'SOLVE' || currentComp.type === 'SOLVE_MAIN' || currentComp.type === 'SOLVE_HALF') {
						currentComp.setLineHeight(lineheight + 'mm');
					}
				}
			};
			$scope.drawLine = function () {
				if ($scope.currentComponentOrder !== -1) {
					var currentComp = page.findComponentByOrder($scope.currentComponentOrder);
					if (currentComp.type === 'SOLVE' || currentComp.type === 'SOLVE_MAIN' || currentComp.type === 'SOLVE_HALF') {
						currentComp.drawLine();
					}
				}
			};
			$scope.insertComponent = function () {
				if ($scope.currentComponentOrder !== -1) {
					ngDialog.open({
						template: 'insertComponentTmpl',
						className: 'ngdialog-theme-default',
						appendClassName: 'simple-toast',
						closeByDocument: false,
						scope: $scope,
						controller: function ($scope) {
							return function () {
								$scope.chosedComp = 1;
								$scope.done = function () {
									ngDialog.close();
									var comp;
									switch ($scope.chosedComp) {
										case 1:
											comp = new sheetElement.Title();
											page.insertComponent($scope.currentComponentOrder, comp);
											buildPage(true);
											break;
										case 2:
											$scope.addMultipeChoice(function (comp) {
												page.insertComponent($scope.currentComponentOrder, comp);
												buildPage(true);
											});
											break;
										case 3:
											$scope.addFillBlank(function (comp) {
												page.insertComponent($scope.currentComponentOrder, comp);
												buildPage(true);
											});
											break;
										case 4:
											$scope.addSolveQst(function (comp) {
												page.insertComponent($scope.currentComponentOrder, comp);
												buildPage(true);
											});
											break;
										case 5:
											$scope.addComposition(function (comp) {
												page.insertComponent($scope.currentComponentOrder, comp);
												buildPage(true);
											});
											break;
										default:
											break;
									}
								};
							}
						}
					});
				} else {
					constantService.showSimpleToast('请先选中一个组件');
				}
			};

			function getSheetData4Save() {
				var dataToSave = {
					pageSize: page.config.pageSize,
					schoolName: page.schoolName,
					isMarkOnLine: $scope.isMarkOnLine,
					isSchool: $scope.isSchool,
					isPagination: $scope.isPagination,
					isShowHalfScore: $scope.showHalfScore,
					isUnit: ISUNIT,
					secretMode: $scope.secretMode,
                    optionGroupMode: $scope.optionGroupMode,
					components: []
				};
				var compData = [];
				_.each(page.components, function (comp) {
					switch (comp.type) {
						case 'OPTION_GROUP':
							compData.push({
								type: comp.type,
								num: comp.num,
								mode: comp.mode,
								startOrder: comp.startOrder,
								optionMode: comp.optionMode,
								optionCount: comp.optionCount
							});
							break;
						case 'TITLE':
							compData.push({
								type: comp.type,
								title: comp.title,
								height: comp.height
							});
							break;
						case 'FILL_BLANK':
							compData.push({
								type: comp.type,
								orders: comp.orders,
								height: comp.height,
								sapceCount: comp.count
							});
							break;
						case 'EXAM_NAME':
							compData.push({
								type: comp.type,
								examName: comp.name,
								height: comp.height
							});
							break;
						case 'ANSWER_TIP':
							compData.push({
								type: comp.type,
								paperNo: comp.paperNo,
								height: comp.height
							});
							break;
						case 'BLANK':
							compData.push({
								type: comp.type,
								height: comp.height
							});
							break;
						case 'SOLVE':
							compData.push({
								type: comp.type,
								height: comp.height,
								order: comp.order,
								score: comp.score,
								sub: comp.sub,
								mode: comp.mode,
								mainHtml: comp.mainHtml,
								halfHtml: comp.halfHtml,
								lineHeight: comp.lineHeight
							});
							break;
						case 'COMPOSITION':
							compData.push({
								type: comp.type,
								score: comp.score,
								num: comp.num
							});
							break;
						case 'PARAGRAPH':
							compData.push({
								type: comp.type,
								$content: comp.$content,
								height: comp.height
							});
							break;
						case 'UNIT_FILL_BLANK':
							compData.push({
								type: comp.type,
								html: comp.html,
								height: comp.height,
								order: comp.order,
								count: comp.count
							});
							break;
						default:
							break;
					}
				});
				dataToSave.components = compData;
				return dataToSave;
			}

			$scope.saveSheet = function () {
				var dataToSave = getSheetData4Save();
				$http.post($rootScope.baseUrl + '/saveAnswerSheet.ashx', {
					sheetId: SHEETID || '',
					examId: EXAMID,
					answerSheetMeta: JSON.stringify(dataToSave),
					answerSheetName: page.components[0].text
				}).then(function (res) {
					if (res.data.code === 0) {
						$scope.isModified = false;
						constantService.showSimpleToast('恭喜您，答题卡保存成功！');
						location.hash = '#/buildAnswerSheet?sheetId=' + res.data.msg;
						if (unifiedId && action === '1') {
							//如果是从考试中心过来，保存完之后同步状态
							$http.post('/BootStrap/schoolmanager/updateUnifiedItemStep.ashx', {
								unifiedItemId: unifiedId,
								step: +search.MarkMode !==1 ? 4 : 3
							}).error(function () {
								constantService.alert('同步考试状态失败！');
							});
						}
					} else {
						constantService.showSimpleToast(res.data.msg);
					}
				}, function () {
					constantService.showSimpleToast('答题卡保存失败，服务器错误！');
				});
			};

			$scope.saveAs = function () {
				if ($scope.examIdForSheet) {
					var dataToSave = getSheetData4Save();
					$http.post($rootScope.baseUrl + '/saveAnswerSheet.ashx', {
						sheetId: '',
						examId: $scope.examIdForSheet,
						answerSheetMeta: JSON.stringify(dataToSave),
						answerSheetName: page.components[0].text
					}).then(function (res) {
						if (res.data.code === 0) {
							constantService.showSimpleToast('恭喜您，答题卡保存成功！');
						} else {
							constantService.showSimpleToast(res.data.msg);
						}
					}, function () {
						constantService.showSimpleToast('答题卡保存失败，服务器错误！');
					});
				}
			};

			$scope.exportPdf = function () {
				Event.trigger('PAGE_ELE_DEACTIVE');
				var area = $('.bac-ctx').clone();
				area.find('.page-box').css({
					'margin-top': 0
				});
				var str = '<!DOCTYPE html><html>';
				str += '<head><meta charset="utf-8">';
				str += "<link type='text/css' rel='stylesheet' href='" + location.origin + "/assets/css/buildAnswerSheet.css'/>";
				str += '</head>';
				str += '<body>';
				str += area.html();
				str += '</body></html>';
				// str = str.replace(/&nbsp;/gi, '<span style=\"font-family: \"Arial\"\">&nbsp;</span>');
				apiCommon.exportHtml2Pdf({
                    content: str,
                    type: $scope.currentPageSize,
                    name: page.components[0].text.replace(/\s+/g, '')
				}).then(function (res) {
					if (res.data.code === 0) {
						var link = $('#download-link');
						link.attr('href', res.data.msg);
						link.attr('download', page.components[0].text + '答题卡.pdf');
						link.find('#downloadlabel').click();
					} else {
						constantService.showSimpleToast('下载答题卡失败！');
					}
				});
			};

			$scope.resetSheet = function () {
				constantService.showConfirm('重新制作之后已制作的内容将无法恢复，确定需要重新制作答题卡吗？', ['确定'], function () {
					page.components = page.components.slice(0, 2);
					buildPage(true);
				});
			};

			$scope.rebuildSheet = function () {
				sheetUtil.isMarkOnLine = $scope.isMarkOnLine;
				page.setOption({
					secretMode: $scope.secretMode,
					isPagination: $scope.isPagination,
					isSchool: $scope.isSchool,
                    optionGroupMode: $scope.optionGroupMode
				});
				buildPage(true);
			};

			$scope.showLineHeightBox = function () {
				$scope.isShowLineHeightBox = !$scope.isShowLineHeightBox;
			};

			$scope.checkPaper = function () {
				window.open('#/composing?ExamFlnkID=' + EXAMID + '&action=0&isOpenNewWin=1');
			};

			function buildPage(flag, ignoreModify) {
				if (flag && !ignoreModify) {
					$scope.isModified = true;
				}
				page && page.build(!!flag);
				page && page.activeCurrentComp($scope.currentComponentOrder);
			}

			$scope.currentPageSize = '8K';
			$scope.currentComponentOrder = -1;
			$scope.startOrder = 1;
			$scope.isMarkOnLine = false;
			sheetUtil.isMarkOnLine = false;
			$scope.isOnlySheet = true;
			$scope.isShowLineHeightBox = false;
			$scope.showHalfScore = false;
			sheetUtil.showHalfScore = false;

			$scope.reBuildOnToggle = function(val){
				sheetUtil.showHalfScore = !!val;
				$scope.showHalfScore = !!val;
				buildPage(true);
			};

			$scope.setOptionGroupMode = function(){
				sheetUtil.optionGroupMode = $scope.optionGroupMode;
				buildPage(true);
			};

			if (SHEETID) {
				$http.post($rootScope.baseUrl + '/generalQuery.ashx', {
					Proc_name: 'GetAnswerSheet',
					examId: '',
					SheetName: '',
					sheetId: SHEETID
				}).then(function (res) {
					if (res.data.code === 0) {
						var meta = res.data.msg[0];
						$scope.examIdForSheet = EXAMID = meta.examId;
						$scope.isBuildFromPaper = !!meta.examId;
						var sheetMeta = JSON.parse(meta.sheetMeta);
						sheetUtil.isMarkOnLine = $scope.isMarkOnLine = !!sheetMeta.isMarkOnLine;
						$scope.isPagination = typeof sheetMeta.isPagination === 'undefined' ? true : sheetMeta.isPagination;
						$scope.isSchool = typeof sheetMeta.isSchool === 'undefined' ? true : sheetMeta.isSchool;
						sheetUtil.showHalfScore = $scope.showHalfScore = !!sheetMeta.isShowHalfScore;
						ISUNIT = !!sheetMeta.isUnit;
						$scope.secretMode = sheetMeta.secretMode || 'PAINT';
                        sheetUtil.optionGroupMode = $scope.optionGroupMode = sheetMeta.optionGroupMode || 'AUTO';
						page = new window.sheetElement.Page([], {
							pageSize: sheetMeta.pageSize
						}, $('#print-area'), false, 1, 1, 1, sheetMeta.schoolName, {
							secretMode: sheetMeta.secretMode || $scope.secretMode,
							isPagination: sheetMeta.isPagination || $scope.isPagination,
							isSchool: sheetMeta.isSchool || $scope.isSchool,
                            optionGroupMode: sheetMeta.optionGroupMode || $scope.optionGroupMode
						});
						page.setPageSize(sheetMeta.pageSize);
						$scope.currentPageSize = sheetMeta.pageSize;
						_.each(sheetMeta.components, function (comp) {
							switch (comp.type) {
								case 'OPTION_GROUP':
									page.pushComponent(new sheetElement.OptionGroup(comp.num, comp.mode, comp.startOrder, $scope.currentPageSize, comp.optionCount, comp.optionMode));
									break;
								case 'TITLE':
									page.pushComponent(new sheetElement.Title(comp.title, comp.height));
									break;
								case 'FILL_BLANK':
									page.pushComponent(new sheetElement.FillBlankQ(comp.orders, comp.sapceCount, $scope.currentPageSize));
									break;
								case 'EXAM_NAME':
									page.pushComponent(new sheetElement.ExamName(comp.examName, comp.height));
									break;
								case 'ANSWER_TIP':
									page.pushComponent(new sheetElement.AnswerTip(comp.paperNo, comp.height));
									break;
								case 'BLANK':
									page.pushComponent(new sheetElement.Blank(comp.height));
									break;
								case 'SOLVE':
									page.pushComponent(new sheetElement.SolveFull(comp.order, comp.score, comp.sub, comp.mode, comp.height, '', comp.mainHtml, comp.halfHtml, comp.lineHeight));
									break;
								case 'COMPOSITION':
									page.pushComponent(new sheetElement.Composition(comp.score, comp.num));
									break;
								case 'PARAGRAPH':
									page.pushComponent(new sheetElement.Paragraph(comp.$content, $scope.currentPageSize, comp.height));
									break;
								case 'UNIT_FILL_BLANK':
									page.pushComponent(new sheetElement.UnitFillBlank(comp.html, comp.order, comp.count, $scope.currentPageSize, comp.height));
									break;
								default:
									break;
							}
						});
						buildPage(true, true);
					}
				});
			} else {
				page = new sheetElement.Page([], {
					pageSize: $scope.currentPageSize
				}, $('#print-area'), false, 1, 1, 1, '', {
					secretMode: $scope.secretMode,
					isPagination: $scope.isPagination,
					isSchool: $scope.isSchool,
                    optionGroupMode: $scope.optionGroupMode
				});
				page.setPageSize($scope.currentPageSize);
				if (EXAMID) {
					initPaperData(EXAMID, unifiedId).then(function (res) {
						examData = res;
						page.setSchoolName(examData.SchoolName);
						var enComp = new sheetElement.ExamName(examData.examName);
						var tipComp = new sheetElement.AnswerTip(EXAMNO ? ('00' + EXAMNO) : '00000000');
						page.pushComponent(enComp);
						page.pushComponent(tipComp);
						if (!ISCLEAR) {
							_.each(examData.msg, function (group, index) {
								var cnIndex = constantService.getCNNoByIndex(index + 1);
								var groupTitle = group.Dtype + (!!group.Explain ? group.Explain : ('（共' + group.QNumber + '题，共' + group.QTypeScores + '分）'));
								page.pushComponent(new sheetElement.Title(cnIndex + '、' + groupTitle));
								fillCard(group.question, true);
							});
						}
						buildPage(true, true);
					});
				} else {
					var enComp = new sheetElement.ExamName();
					var tipComp = new sheetElement.AnswerTip('00000000');
					page.pushComponent(enComp);
					page.pushComponent(tipComp);
					buildPage(true, true);
				}
			}

			function countObjective(questions) {
				var count = 0;
				var index = _.findIndex(questions, function (item) {
					return item.isObjective === '0';
				});
				if (index === -1) {
					count = questions.length;
				} else {
					count = index;
				}
				return count;
			}

			function countFillBlank(questions) {
				var count = 0;
				var index = _.findIndex(questions, function (item) {
					return (item.QTypeName !== '填空题' && item.QTypeName !== '判断题') || item.isObjective === '1';
				});
				if (index === -1) {
					count = questions;
				} else {
					count = questions.slice(0, index);
				}
				return count;
			}

			var modeAParents;
			var renderedParents = [];

			function fillCard(questions, isReFill) {
				var allQuestions = getAllQstsWithOrder(questions);
				var count = countObjective(allQuestions);
				var lastQsts;
				if (isReFill) {
					modeAParents = getModeAParents(questions);
				}
				if (count > 0) {
					page.pushComponent(new sheetElement.OptionGroup(count, 1, +allQuestions[0].showDis, $scope.currentPageSize));
					if (ISUNIT) {
						_.each(allQuestions, function (q) {
							if (q.parentMode === 'A' && $.inArray(q.parentId, renderedParents) === -1) {
								var parent = _.find(modeAParents, function (item) {
									return item.FLnkID === q.parentId;
								});
								_.each(new sheetElement.Content4Unit([parent], $scope.currentPageSize).init().paragraphs, function (p) {
									page.pushComponent(p);
								});
								renderedParents.push(q.parentId);
							}
							if(unifiedId) {
								var optionContent = new sheetElement.Content4Unit(q.originMeta, $scope.currentPageSize);
								optionContent.init();
								_.each(optionContent.paragraphs, function (p) {
									page.pushComponent(p);
								});
							}else {
								var optionContent = new sheetElement.Content4Unit([q], $scope.currentPageSize);
								optionContent.init();
								_.each(optionContent.paragraphs, function (p) {
									page.pushComponent(p);
								});
							}
						});
					}
				}
				if (!ISUNIT) {
					var blankCount = countFillBlank(allQuestions);
					if (blankCount.length > 0) {
						var len = Math.ceil(blankCount.length / 2);
						for (var k = 0; k < blankCount.length; k += 2) {
							if ((k + 1) < blankCount.length) {
								page.pushComponent(new sheetElement.FillBlankQ([+blankCount[k].showDis, +blankCount[k + 1].showDis], 1, $scope.currentPageSize));
							} else {
								page.pushComponent(new sheetElement.FillBlankQ([+blankCount[k].showDis], 1, $scope.currentPageSize));
							}
						}
					}
					lastQsts = allQuestions.slice(count + blankCount.length);
				} else {
					lastQsts = allQuestions.slice(count);
				}
				for (var i = 0; i < lastQsts.length; i++) {
					var q = lastQsts[i];
					if (ISUNIT) {
						if (q.isObjective !== '1' && q.parentMode === 'A' && $.inArray(q.parentId, renderedParents) === -1) {
							var parent = _.find(modeAParents, function (item) {
								return item.FLnkID === q.parentId;
							});
							_.each(new sheetElement.Content4Unit([parent], $scope.currentPageSize).init().paragraphs, function (p) {
								page.pushComponent(p);
							});
							renderedParents.push(q.parentId);
						}
					}
					if (q.isObjective === '1' && (q.mode !== 'B' && q.Mode !=='B')) {
						fillCard(lastQsts.slice(i));
						break;
						return;
					} else {
						if ((q.mode !== 'B' && q.Mode !=='B') && (q.QTypeName === '填空题' || q.QTypeName === '判断题')) {
							if (ISUNIT) {
								var tempQ = lastQsts[i];
								if(unifiedId) {
									tempQ = lastQsts[i].originMeta[0];
								}
								page.pushComponent(new sheetElement.UnitFillBlank(tempQ.title, tempQ.showDis, tempQ.count || 1, $scope.currentPageSize));
							} else {
								fillCard(lastQsts.slice(i));
								break;
								return;
							}
						} else if (q.QTypeName.indexOf('作文') >= 0 && (q.mode !== 'B' && q.Mode !=='B')) {
							if (ISUNIT) {
								var qList = questions;
								if(unifiedId) {
									var metas = [];
									_.each(questions, function(item){
										if(item.originMeta) {
											metas = metas.concat(item.originMeta || []);
										}else {
											metas.push(item);
										}
									});
									qList = metas;
								}
								var compositionContent = new sheetElement.Content4Unit(qList, $scope.currentPageSize);
								compositionContent.init();
								_.each(compositionContent.paragraphs, function (p) {
									page.pushComponent(p);
								});
							}
							page.pushComponent(new sheetElement.Composition(+q.score, 960));
						} else {
							var sub = [];
							_.each(q.sub, function (item) {
								sub.push({
									score: +item.score || 1
								});
							});
							var mainHtml = '';
							var h;
							page.pushComponent(new sheetElement.Blank(2));
							if (ISUNIT) {
								if(unifiedId) {
									if(q.originMeta) {
										var content = new sheetElement.Content4Unit(q.originMeta, $scope.currentPageSize);
										content.init(true);
										_.each(content.paragraphs, function (p) {
											mainHtml += '<div>' + p.$content + '</div>';
										});
									}else {
										var subQsts = [];
										_.each(q.sub, function(item){
											subQsts = subQsts.concat(item.originMeta);
										});
										var qst = {
											mode: q.Mode,
											title: q.title,
											sub: subQsts,
											score: q.score
										};
										var content = new sheetElement.Content4Unit([qst], $scope.currentPageSize);
										content.init(true);
										_.each(content.paragraphs, function (p) {
											mainHtml += '<div>' + p.$content + '</div>';
										});
									}
								}else {
									var content = new sheetElement.Content4Unit([q], $scope.currentPageSize);
									content.init(true);
									_.each(content.paragraphs, function (p) {
										mainHtml += '<div>' + p.$content + '</div>';
									});
								}
								console.log(q);
								var domHeight = sheetUtil.calcDOMHeight(mainHtml, sheetUtil.getContentWidth($scope.currentPageSize)) / sheetUtil.MM2PX_RATIO;
								var guessHeight = sheetUtil.guessHeightWithScore(+q.score);
								h = domHeight + guessHeight;
								page.pushComponent(new sheetElement.SolveFull(q.showDis, +q.score, sub, sub.length > 0 ? 'sub' : 'normal', h, '', mainHtml));
							} else {
								page.pushComponent(new sheetElement.SolveFull(q.showDis, +q.score, sub, sub.length > 0 ? 'sub' : 'normal', sheetUtil.guessHeightWithScore(+q.score), ''));
							}
						}
					}
				}
			}

			function getAllQstsWithOrder(questions) {
				var allQsts = [];
				_.each(questions, function (q) {
					if (q.Mode === 'A' || q.mode === 'A') {
						allQsts = allQsts.concat(q.sub);
					} else if (q.Mode === 'B' || q.mode === 'B') {
						allQsts.push(q);
					} else {
						allQsts.push(q);
					}
				});
				return allQsts;
			}

			function getModeAParents(questions) {
				var res = [];
				_.each(questions, function (item) {
					if (item.mode === 'A' || item.Mode === 'A') {
						res.push({
							title: item.title,
							FLnkID: item.FLnkID,
							mode: 'A'
						});
						_.each(item.sub, function (s) {
							s.parentId = item.FLnkID;
							s.parentMode = 'A';
						});
					}
				});
				return res;
			}

			function getQstByOrder(order) {
				var qst;
				if (_.isEmpty(examData)) {
					return qst;
				} else {
					var allqsts = paperService.getAllQuestionInPaper(examData.msg);
					qst = _.find(allqsts, function (item) {
						return +item.orders === +order;
					});
					return qst;
				}
			}

			function initPaperData(examId, unifiedId) {
				var defer = $q.defer();
				if (unifiedId) {
					$http.post('/BootStrap/EncryptUnified/getPaperConfig.ashx', {
						unifiedId: unifiedId
					}).then(function (res) {
						var meta = res.data.msg.meta;
						getPaperMeta(examId).then(function (examRes) {
							var metaQuestions = [];
							_.each(meta, function (item) {
								metaQuestions = metaQuestions.concat(item.question);
							});
							_.each(examRes.msg, function (group) {
								_.each(group.question, function (q) {
									if (q.mode === 'A' || q.Mode === 'A' || q.mode === 'B' || q.Mode === 'B') {
										_.each(q.sub, function (s) {
											var qInMeta = _.find(metaQuestions, function (mq) {
												return mq.qFlnkId === s.FLnkID;
											});
											if (qInMeta) {
												s.showDis = qInMeta.showDis;
											}
										})
									}else {
										var qInMeta = _.find(metaQuestions, function (mq) {
											return mq.qFlnkId === q.FLnkID;
										});
										if (qInMeta) {
											q.showDis = qInMeta.showDis;
										}
									}
								});
							});
							//处理合并给分的情况,
							if(!ISUNIT) {
								parsePaperForNormalSheet(examRes.msg);
							}else {
								parsePaperForUnitSheet(examRes.msg);
							}
							defer.resolve(examRes);
						}, defer.reject);
					}, defer.reject);
				} else {
					getPaperMeta(examId).then(function (examRes) {
						_.each(examRes.msg, function(group){
                            _.each(group.question, function (q) {
                                if (q.mode === 'A' || q.Mode === 'A' || q.mode === 'B' || q.Mode === 'B') {
                                    _.each(q.sub, function (s) {
                                        s.showDis = s.dis;
                                    });
                                }
                                q.showDis = q.dis;
                            });
						});
						defer.resolve(examRes);
					});
				}
				return defer.promise;
			}

			function parsePaperForNormalSheet(groups){
				_.each(groups, function(g){
					//利用map数据结构处理showDis一样（认为是合并给分）的情况
					var disQstMap = {};
					//记录内容放入MAP时的顺序，防止题目顺序混乱
					var pushSort = 1;
					_.each(g.question, function(q){
						if(q.mode === 'A' || q.Mode === 'A') {
							_.each(q.sub, function(s){
								var cachedQ = disQstMap[s.showDis];
								if(cachedQ) {
									cachedQ.isObjective = '0';
									cachedQ.QTypeName = '主观题';
									cachedQ.score = +s.score + +cachedQ.score;
								}else {
									s.pushSort = pushSort++;
									disQstMap[s.showDis] = s;
								}
							});
						} else if(q.mode === 'B' || q.Mode === 'B'){
							var subCache = {};
							var pushOrder = 0;
							_.each(q.sub, function(s){
								var cachedQ = subCache[s.showDis];
								if(cachedQ) {
									cachedQ.isObjective = '0';
									cachedQ.QTypeName = '主观题';
									cachedQ.score = +s.score + +cachedQ.score;
								}else {
									s.pushOrder = pushOrder++;
									subCache[s.showDis] = s;
								}
							});
							var qsts = [];
							for(var key in subCache) {
								qsts.push(subCache[key]);
							}
							qsts.sort(function(a, b){
								return a.pushOrder - b.pushOrder;
							});
							q.sub = qsts;
							q.pushSort = pushSort++;
							disQstMap[q.sub[0].showDis] = q;
						}else {
							var cachedQ = disQstMap[q.showDis];
							if(cachedQ) {
								cachedQ.isObjective = '0';
								cachedQ.QTypeName = '主观题';
								cachedQ.score = +q.score + +cachedQ.score;
							}else {
								q.pushSort = pushSort++;
								disQstMap[q.showDis] = q;
							}
						}
					});
					var qsts = [];
					for(var key in disQstMap) {
						qsts.push(disQstMap[key]);
					}
					qsts.sort(function(a, b){
						return a.pushSort - b.pushSort;
					});
					g.question = qsts;
				});
			}

			function parsePaperForUnitSheet(groups){
				_.each(groups, function(g){
					//利用map数据结构处理showDis一样（认为是合并给分）的情况
					var disQstMap = {};
					//记录内容放入MAP时的顺序，防止题目顺序混乱
					var pushSort = 1;
					_.each(g.question, function(q){
						if(q.mode === 'A' || q.Mode === 'A' || q.mode === 'B' || q.Mode === 'B'){
							var subCache = {};
							var pushOrder = 0;
							_.each(q.sub, function(s){
								var cachedQ = subCache[s.showDis];
								if(cachedQ) {
									cachedQ.isObjective = '0';
									cachedQ.QTypeName = '主观题';
									cachedQ.score = +s.score + +cachedQ.score;
									cachedQ.originMeta.push(s);
								}else {
									var tempQ = {
										isObjective: s.isObjective,
										QTypeName: s.QTypeName,
										score: +s.score,
										pushOrder: pushOrder++,
										showDis: s.showDis,
										originMeta: [s]
									};
									subCache[s.showDis] = tempQ;
								}
							});
							var qsts = [];
							for(var key in subCache) {
								qsts.push(subCache[key]);
							}
							qsts.sort(function(a, b){
								return a.pushOrder - b.pushOrder;
							});
							q.sub = qsts;
							q.pushSort = pushSort++;
							disQstMap[q.sub[0].showDis] = q;
						}else {
							var cachedQ = disQstMap[q.showDis];
							if(cachedQ) {
								cachedQ.isObjective = '0';
								cachedQ.QTypeName = '主观题';
								cachedQ.score = +q.score + +cachedQ.score;
								cachedQ.originMeta.push(q);
							}else {
								disQstMap[q.showDis] = {
									isObjective: q.isObjective,
									QTypeName: q.QTypeName,
									score: +q.score,
									pushSort: pushSort++,
									showDis: q.showDis,
									originMeta: [q]
								};
							}
						}
					});
					var qsts = [];
					for(var key in disQstMap) {
						qsts.push(disQstMap[key]);
					}
					qsts.sort(function(a, b){
						return a.pushSort - b.pushSort;
					});
					g.question = qsts;
					console.log(qsts);
				});
			}

			function getPaperMeta(examId, unifiedId){
				var defer = $q.defer();
				$http.post($rootScope.baseUrl + '/Interface0181.ashx', {
					examId: examId
				}).then(function (examRes) {
					if(examRes.data.msg && _.isArray(examRes.data.msg)) {
						_.each(examRes.data.msg, function(g){
							_.each(g.question, function(q){
								if(q.sub && q.sub.length > 0) {
									_.each(q.sub, function(s){
										replaceQuestionSpace(s);
									});
								}
								replaceQuestionSpace(q);
							});
						});
					}
					defer.resolve(examRes.data);
				}, defer.reject);
				return defer.promise;
			}

			function replaceQuestionSpace(q){
				var prop = ['title', 'OptionOne', 'OptionTwo', 'OptionThree', 'OptionFour'];
				for(var key in prop) {
					q[prop[key]] = doReplaceSpace(q[prop[key]]);
				}
			}

			function doReplaceSpace(str){
				return str.replace(/&nbsp;/gi, '<span style=\"font-family: \"Arial\"\">&nbsp;</span>');
			}

			// $(window).off('keyup').on('keyup', function (e) {
			// 	if (e.keyCode === 107 || e.keyCode === 187) {
			// 		e.preventDefault();
			// 		e.stopPropagation();
			// 		$scope.addCompHeight();
			// 	} else if (e.keyCode === 109 || e.keyCode === 189) {
			// 		e.preventDefault();
			// 		e.stopPropagation();
			// 		$scope.minusCompHeight();
			// 	}
			// });

			$scope.$on('$stateChangeStart', function (event, toState, toParams) {
				if (toState.name !== 'buildAnswerSheet' && $scope.isModified) {
					var isLeave = confirm('确定离开答题卡编辑页面？如果尚未保存答题卡，已制作的内容将无法恢复。');
					if (!isLeave) {
						event.preventDefault();
					} else {
						window.onbeforeunload = null;
					}
				} else {
					window.onbeforeunload = null;
				}
			});
			window.onbeforeunload = function () {
				if ($scope.isModified) {
					return true;
				}
			};
		}]
});