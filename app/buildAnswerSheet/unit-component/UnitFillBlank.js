/**
 * Created by 贺小雷 on 2017/1/22.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //填空题组组件
    sheetElement.UnitFillBlank = sheetElement.SheetComp.extend({
        construct: function(html, order, count, pageSize, height){
            this.MIN_HEIGHT = 9;
            this.html = html;
            this.order = order;
            this.count = count || 1;
            this.pageSize = pageSize;
            this.marginBottom = 0;
            this.preInit();
            if(height) {
                this.height = height;
            }
            this._super(this.height, 'UNIT_FILL_BLANK', false, true);
        },
        preInit: function() {
            var self = this;
            var $dom = $('<div>').html(sheetUtil.parseContentInline(this.html, this.order));
            var calcHeight = sheetUtil.calcDOMHeight($dom, this.calcItemBoxWidth() - (sheetUtil.isMarkOnLine ? -7 : 19.5), '25px', function(h){
                    self.setHeight(h / sheetUtil.MM2PX_RATIO);
                }) / sheetUtil.MM2PX_RATIO;
            if(calcHeight < this.MIN_HEIGHT) {
                this.height = this.MIN_HEIGHT;
            }else {
                this.height = calcHeight;
            }
        },
        init: function(){
            this.$dom = $('<div>').addClass('fill-blank-g sheet-ele').css({
                width: '100%',
                height: (this.height - 1) + 'mm',
                'padding-left': '3px',
                position: 'relative',
                marginBottom: '1mm'
            });
            if(!sheetUtil.isMarkOnLine) {
                var blackBlock = new sheetElement.BlackBlock();
                blackBlock.init().$dom.css({
                    position: 'absolute',
                    left: (-1 * blackBlock.width * sheetUtil.MM2PX_RATIO + 1) + 'px',
                    top: 0,
                    display: 'block'
                }).appendTo(this.$dom);
                blackBlock.init().$dom.css({
                    position: 'absolute',
                    top: 0
                }).appendTo(this.$dom);
            }
            var itemBox = $('<div>').addClass('fill-blank-item-box').css({
                width: this.calcItemBoxWidth() + 'mm',
                height: this.height - 1 + 'mm',
                display: 'inline-block',
                position: 'absolute',
                top: '0',
                right: '0'
            }).appendTo(this.$dom);
            var line = new Line(this.calcItemBoxWidth(), this.height - 1, this.marginBottom, this.html, this.order, this.count);
            line.render(itemBox);
            if(!sheetUtil.isMarkOnLine) {
                var scoreBox = new ScoreBoxForFillBlank(1, this.count, this.marginBottom);
                scoreBox.render(itemBox);
            }
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$dom.off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('PAGE_ELE_DEACTIVE');
                self.active();
            });
            this.$dom.off('dblclick').on('dblclick', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('MODIFY_UNIT_FILL_BLANK', self);
            });
            this.$dom.find('.fb-content').off('blur').on('blur', function(){
                var html = $(this).html();
				var text = $(this).text();
				var reg = /^\d+./;
				var matched = text.match(reg);
				if(matched) {
					self.order = parseInt(text.match(reg)[0]);
				}
                self.html = html.replace(self.order + '.', '');
            });
        },
        setPageSize: function(size){
            this.pageSize = size;
        },
        calcItemBoxWidth: function(){
            if(sheetUtil.isMarkOnLine) {
                return (sheetUtil.isPageSizeLarge(this.pageSize) ? sheetUtil.PAGE_SIZE_A3 : sheetUtil.PAGE_SIZE_8K);
            }else {
                var blackBlock = new sheetElement.BlackBlock();
                return (sheetUtil.isPageSizeLarge(this.pageSize) ? sheetUtil.PAGE_SIZE_A3 : sheetUtil.PAGE_SIZE_8K) - (+blackBlock.width + 2);
            }
        },
        setOrder: function(order){
            this.order = order;
        },
        setCount: function(count){
            this.count = count;
        },
        setHeight: function(height){
            if(height !== this.height) {
                this.height = height;
                this.$dom.css({
                    height: height + 'mm'
                });
                sheetUtil.refreshDelay(true);
            }
        }
    });

    //给分框组件
    var ScoreBoxForFillBlank = function(qstCount, spaceCount, marginBottom){
        this.qstCount = qstCount;
        this.spaceCount = spaceCount;
        this.boxItemWidth = 6.5;
        this.boxItemHeight = 4.65;
        this.marginBottom = marginBottom;
        if(this.qstCount > 1) {
            this.width = 2 * this.boxItemWidth + 2;
        }else {
            this.width = this.spaceCount * this.boxItemWidth;
        }
    };
    ScoreBoxForFillBlank.prototype = {
        init: function(){
            this.$dom = $('<div class="score-fb-box">').css({
                position: 'absolute',
                width: this.width + 'mm',
                height: this.boxItemHeight + 'mm',
                right: '0',
                top: 0
            });
            var $boxItem = $('<span>').css({
                display: 'block',
                width: this.boxItemWidth + 'mm',
                height: this.boxItemHeight + 'mm',
                border: '1px solid #000',
                position: 'absolute',
                bottom: '-0.5mm'
            });
            if(this.qstCount > 1) {
                $boxItem.clone().css({
                    left: '0'
                }).appendTo(this.$dom);
                $boxItem.clone().css({
                    right: '0'
                }).appendTo(this.$dom);
            }else {
                for(var i = 0; i < this.spaceCount; i++) {
                    $boxItem.clone().css({
                        left: i * this.boxItemWidth + 'mm',
                        borderRight: i === (this.spaceCount - 1) ? '1px solid #000' : 'none'
                    }).appendTo(this.$dom);
                }
            }
            return this;
        },
        render: function(node){
            this.init().$dom.appendTo(node);
            return this;
        }
    };
    //填空题横线组件
    var Line = function(width, height, marginBottom, html, order, spaceCount){
        this.order = order;
        this.spaceCount = spaceCount;
        var scoreBox = new ScoreBoxForFillBlank(1, 3);
        this.width = width - (sheetUtil.isMarkOnLine ? 0 : scoreBox.width);
        this.height = height;
        this.marginBottom = marginBottom;
        this.spaceWidth = 3;
        this.html = html;
    };
    Line.prototype = {
        init: function(){
            var self = this;
            this.$dom = $('<div class="fb-line">').css({
                position: 'absolute',
                width: this.width + 'mm',
                height: this.height + 'mm',
                left: 0,
                top: 0
            });
            var html = sheetUtil.parseContentInline(this.html, this.order);
            var $line = $('<div class="fb-content" contenteditable="true">').css({
                height: this.height + 'mm',
                top: '-5px',
                position: 'absolute',
                'word-break': 'break-all',
                'line-height': '25px'
            }).html(html).appendTo(this.$dom);
            return this;
        },
        render: function(node){
            this.init().$dom.appendTo(node);
            return this;
        }
    };
})();