/**
 * Created by 贺小雷 on 2017/1/20.
 */
(function(){
	var sheetElement = window.sheetElement = window.sheetElement || {};
	sheetElement.Paragraph = sheetElement.SheetComp.extend({
		construct: function($dom, pageSize, height, isInline){
			this.pageSize = pageSize;
			this.$content = $dom;
			this.isInline = !!isInline;
			this.isHeightCalced = false;
			this.preInit();
			this.height = height || this.height || sheetUtil.calcDOMHeight(this.$dom, sheetUtil.getContentWidth(this.pageSize)) / sheetUtil.MM2PX_RATIO;
			this._super(this.height, 'PARAGRAPH', false, true);
		},
		preInit: function(){
			this.$dom = $('<span class="unit-paragraph sheet-ele" style="display: block;position: relative;" contenteditable="true">');
			this.$dom.html(this.$content);
		},
		init: function() {
			this.preInit();
			var self = this;
			if(this.isHeightCalced) {
				this.height = this.height;
			}else {
				var calcHeight = sheetUtil.calcDOMHeight(this.$dom, sheetUtil.getContentWidth(this.pageSize), null, function(h){
						self.setHeight(h/sheetUtil.MM2PX_RATIO);
					}) / sheetUtil.MM2PX_RATIO;
				this.height = this.height || calcHeight;
				this.isHeightCalced = true;
			}
			this.$dom.css({
				margin: 0,
				padding: 0,
				fontSize: '14px',
				wordBreak: 'break-all',
				height: this.height + 'mm'
			});
			return this;
		},
		listenEvent: function(){
			var self = this;
			this.$dom.off('click').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('PAGE_ELE_DEACTIVE');
				self.active();
			});
			this.$dom.off('blur').on('blur', function(e){
				e.preventDefault();
				e.stopPropagation();
				self.$content = self.$dom.html();
			});
			this.supportImgDrag();
		},
		setHeight: function(height){
			if(height !== this.height) {
				this.height = height;
				this.$dom.css({
					height: height + 'mm'
				});
				sheetUtil.refreshDelay(true);
			}
		},
		/**
		 * 渲染完成后重新计算元素高度，因为初始计算字体渲染不正确导致计算不准确
		 */
		reCalcHeight: function(){
			var self = this;
			setTimeout(function(){
				self.$dom.css({
					height: 'auto'
				});
				var trueHeight = self.$dom.height() / sheetUtil.MM2PX_RATIO;
				self.setHeight(trueHeight);
			}, 50);
		},
		supportImgDrag: function(){
			var isMouseDown = false;
			var dx = 0, dy = 0, mx = 0, my = 0;
			var order = this.orderInPage;
			var that = this;
			this.$dom.delegate('img', 'mousedown.dragImg', function(e){
				e.preventDefault();
				e.stopPropagation();
				var $img = $(this);
				isMouseDown = true;
				dx = e.clientX;
				dy = e.clientY;
				var currentLeft = parseInt($img.css('left')) || 0,currentTop = parseInt($img.css('top')) || 0;
				$(window).off('mousemove.dragImg').on('mousemove.dragImg', function(e){
					if(isMouseDown) {
						e.preventDefault();
						e.stopPropagation();
						mx = e.clientX;
						my = e.clientY;
						$img.css({
							position: 'absolute',
							left: currentLeft + mx - dx + 'px',
							top: currentTop + my - dy + 'px'
						});
					}
				});
				$(window).off('mouseup.dragImg').on('mouseup.dragImg', function(e){
					isMouseDown = false;
					$(window).off('mouseup.dragImg').off('mousemove.dragImg');
					that.$content = that.$dom.html();
				});
			});
		}
	})
})();