/**
 * Created by 贺小雷 on 2017/1/19.
 */
(function(){
	var sheetElement = window.sheetElement = window.sheetElement || {};
	sheetElement.Content4Unit = sheetElement.SheetComp.extend({
		construct: function(questions, pageSize){
			this._super(0, 'CONTENT_4_UNIT');
			this.questions = questions;
			this.pageSize = pageSize;
			this.paragraphs = [];
		},
		init: function(withScore){
			var self = this;
			_.each(this.questions, function (q) {
				self.parseQuestion(q, withScore);
			});
			return this;
		},
		render: function(node){
			this.init();
			this.$dom.appendTo(node);
			return this;
		},
		parseQuestion: function(qst, withScore){
			var self = this;
			var optionA, optionB, optionC, optionD;
			var optionHtml;
			if(qst.title) {
				var title = '';
				qst.title = qst.title.replace(/↵/ig, '');
				qst.title = qst.title.replace(/<!--.*?-->/ig, '');
				if(qst.Mode === 'A' || qst.mode === 'A') {
					title = sheetUtil.parseContentInline(qst.title, '', true);
				}else {
					title = sheetUtil.parseContentInline(qst.title, withScore ? (qst.orders + '（' + qst.score + '分）') : qst.orders, qst.noSplitor);
				}
				// try{
				//     var $p = $('<div>').html(title);
				//     var $contents = $p.contents();
				// $contents.each(function(){
				// 	var $node = this;
				// 	if($node.nodeType === 1){
				// 		//如果是节点
				// 		self.paragraphs.push(new sheetElement.Paragraph($(this).get(0).outerHTML, self.pageSize));
				// 	}else {
				// 		//如果是文本节点
				//             if($(this).get(0).nodeValue) {
				// 			self.paragraphs.push(new sheetElement.Paragraph($(this).get(0).nodeValue, self.pageSize, '', true));
				//             }
				// 	}
				// });
				// }catch(e) {
				//     this.paragraphs.push(new sheetElement.Paragraph(title, this.pageSize));
				// }
				var $p = $('<div>').html(title);
				var nodeList = this.dealNode($p);
				_.each(nodeList, function(item){
					self.paragraphs.push(new sheetElement.Paragraph(item, self.pageSize));
				});
			}
			if(qst.Mode === 'A' || qst.mode === 'A') {
				_.each(qst.sub, function(s){
					self.parseQuestion(s);
				});
			}else if(qst.Mode === 'B' || qst.mode === 'B') {
				_.each(qst.sub, function(s, index){
					s.orders = '（' + (index + 1) + '）';
					s.noSplitor = true;
					self.parseQuestion(s);
				});
			}else {
				if(qst.ShowType === '0') {
					if(qst.OptionOne) {
						optionA = sheetUtil.parseContentInline(qst.OptionOne, 'A', false, true);
						this.paragraphs.push(new sheetElement.Paragraph(optionA, this.pageSize));
					}
					if(qst.OptionTwo) {
						optionB = sheetUtil.parseContentInline(qst.OptionTwo, 'B', false, true);
						this.paragraphs.push(new sheetElement.Paragraph(optionB, this.pageSize));
					}
					if(qst.OptionThree) {
						optionC = sheetUtil.parseContentInline(qst.OptionThree, 'C', false, true);
						this.paragraphs.push(new sheetElement.Paragraph(optionC, this.pageSize));
					}
					if(qst.OptionFour) {
						optionD = sheetUtil.parseContentInline(qst.OptionFour, 'D', false, true);
						this.paragraphs.push(new sheetElement.Paragraph(optionD, this.pageSize));
					}
				}else if(qst.ShowType === '1') {
					if(qst.OptionOne || qst.OptionTwo) {
						optionA = sheetUtil.parseContentInline(qst.OptionOne, 'A', false, true);
						optionB = sheetUtil.parseContentInline(qst.OptionTwo, 'B', false, true);
						optionHtml = optionA + '&nbsp;&nbsp;' + optionB;
						this.paragraphs.push(new sheetElement.Paragraph(optionHtml, this.pageSize));
					}
					if(qst.OptionThree || qst.OptionFour) {
						optionC = sheetUtil.parseContentInline(qst.OptionThree, 'C', false, true);
						optionD = sheetUtil.parseContentInline(qst.OptionFour, 'D', false, true);
						optionHtml = optionC + '&nbsp;&nbsp;' + optionD;
						this.paragraphs.push(new sheetElement.Paragraph(optionHtml, this.pageSize));
					}
				}else {
					if(qst.OptionOne || qst.OptionTwo || qst.OptionThree || qst.OptionFour) {
						optionA = sheetUtil.parseContentInline(qst.OptionOne, 'A', false, true);
						optionB = sheetUtil.parseContentInline(qst.OptionTwo, 'B', false, true);
						optionC = sheetUtil.parseContentInline(qst.OptionThree, 'C', false, true);
						optionD = sheetUtil.parseContentInline(qst.OptionFour, 'D', false, true);
						optionHtml = optionA + '&nbsp;&nbsp;' + optionB + '&nbsp;&nbsp;' + optionC + '&nbsp;&nbsp;' + optionD;
						this.paragraphs.push(new sheetElement.Paragraph(optionHtml, this.pageSize));
					}
				}
			}
		},
		dealNode: function($p){
			var $contents = $p.contents();
			var res = [];
			var self = this;
			$contents.each(function(){
				var node = this;
				if(node.nodeType === 3) {
					//如果是文本节点,如果小于50个字符，则附加到前一个上面，否则另起一行
					if(node.nodeValue.length < 50) {
						if(res.length > 0) {
							//如果存在前一个元素
							var $lastEle = $(res[res.length - 1]);
							// if(self.isZBHTag($lastEle.get(0).tagName.toUpperCase()) || sheetUtil.isInlineTag($lastEle.get(0).tagName)) {
							// res[res.length - 1] = res[res.length - 1] += node.nodeValue;
							// }else {
							// $lastEle.html($lastEle.html() + node.nodeValue);
							// res[res.length - 1] = $lastEle.get(0).outerHTML;
							// }
							$lastEle.html($lastEle.html() + node.nodeValue);
							res[res.length - 1] = $lastEle.get(0).outerHTML;
						}else {
							//否则创建元素
							var $ele = $('<p>').html(node.nodeValue);
							res.push($ele.get(0).outerHTML);
						}
					}else {
						var $ele = $('<p>').html(node.nodeValue);
						res.push($ele.get(0).outerHTML);
					}
				}else {
					if(res.length > 0) {
						var $lastEle = $(res[res.length - 1]);
						if(self.isZBHTag($(node).get(0).tagName) || sheetUtil.isInlineTag($(node).get(0).tagName)) {
							// res[res.length - 1] = res[res.length - 1] += $(node).get(0).outerHTML;
							$lastEle.html($lastEle.html() + $(node).get(0).outerHTML);
							res[res.length - 1] = $lastEle.get(0).outerHTML;
						}else {
							res.push($(node).get(0).outerHTML);
						}
					}else {
						res.push($(node).get(0).outerHTML);
					}

				}
			});
			return res;
		},
		isZBHTag: function(tagName){
			var zbhTags = ['IMG', 'BR', 'HR', 'INPUT'];
			return $.inArray(tagName, zbhTags) >= 0;
		}
	});
})();