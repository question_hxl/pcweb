/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
	var sheetElement = window.sheetElement = window.sheetElement || {};
	//选择题组组件类
	sheetElement.OptionGroup = sheetElement.SheetComp.extend({
		construct: function(num, mode, startOrder, pageSize, optionCount, optionMode){
			this.num = num;
			this.pageSize = pageSize;
			this.mode = mode || 1;  //type: 1  横向  type:2   纵向
			// this.SingleGroupHeight = this.isPageSizeLarge() ? 28 : 25;
			this.groupLength = Math.ceil(this.num / 20);
			this.startOrder = startOrder || 0;
			this.optionCount = optionCount || 4;
			this.optionMode = optionMode || 'normal';        //  或者'judge' （判断题模式）
			this._super(this.calcGroupHeight(), 'OPTION_GROUP');
		},
		init: function(){
			// this.SingleGroupHeight = this.isPageSizeLarge() ? 28 : 25;
			this.groupLength = Math.ceil(this.num / 20);
			// this.height = this.SingleGroupHeight * this.groupLength;
			this.$dom = $('<div class="sheet-ele">').addClass('option-group');
			for(var j = 0; j < this.groupLength; j++) {
				var leftCounts = this.num - 20 * j;
				var toRenderCount = leftCounts > 20 ? 20 : leftCounts;
				var rows = sheetUtil.optionGroupMode === 'FIXED' ? 5 : Math.ceil(toRenderCount / 4);
				var groupHeight = this.calcPerGroupHeight(toRenderCount);
				var $group = $('<div class="group clear">').css({
					height: groupHeight + 'mm',
					'padding-left': '3px'
				}).appendTo(this.$dom);
				var blackBlock = new sheetElement.BlackBlock();
				var blockBox = $('<div class="block-box">').css({
					width: '5mm',
					height: (groupHeight - 4) + 'mm',
					position: 'relative',
					float: 'left',
					top: '0.5mm'
				}).appendTo($group);
				blackBlock.init().$dom.css({
					position: 'absolute',
					left: '0',
					top: '0'
				}).appendTo(blockBox);
				blackBlock.init().$dom.css({
					position: 'absolute',
					left: '0',
					bottom: '0'
				}).appendTo(blockBox);
				var $groupBox = $('<div class="group-box">').css({
					width: ((this.isPageSizeLarge() ? sheetUtil.PAGE_SIZE_A3 : sheetUtil.PAGE_SIZE_8K) - 3.8) + 'mm',
					height: groupHeight + 'mm',
					position: 'absolute',
					left: '3.8mm'
				}).appendTo($group);
				var $block = $('<div class="group-block">').css({
					height: groupHeight + 'mm'
				});
				for(var i = 0; i < toRenderCount; i++) {
					var o = new sheetElement.Option(this.mode, +this.startOrder + (20 * j) + i, this.pageSize, (20 * j) + i, this.optionCount, this.optionMode, this);
					if(this.mode === 1 && (i + 1) % rows === 0) {
						o.render($block);
						$block.appendTo($groupBox);
						$block = $('<div class="group-block">').css({
							height: groupHeight + 'mm'
						});
					}else if(this.mode === 1){
						o.render($block);
						if((20 * j) + i + 1 === this.num) {
							$block.appendTo($groupBox);
						}
					}else {
						o.render(this.$dom);
					}
				}
			}
			return this;
		},
		listenEvent: function(){
			var self = this;
			this.$dom.off('click').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('PAGE_ELE_DEACTIVE');
				self.active();
			});
			this.$dom.off('dblclick').on('dblclick', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('MODIFY_OPTION_GROUP', self);
			});
		},
		isPageSizeLarge: function(){
			return (this.pageSize === 'A4' || this.pageSize === 'A3');
		},
		setPageSize: function(size){
			this.pageSize = size;
            this.height = this.calcGroupHeight();
		},
		setCount: function(count){
			this.num = count;
			this.groupLength = Math.ceil(this.num / 20);
			this.height = this.calcGroupHeight();
		},
		setOptionMode: function(optCount, mode){
			this.optionCount = optCount;
			this.optionMode = mode || 'normal';
		},
		calcGroupHeight: function(){
			var height = 0;
			for(var j = 0; j < this.groupLength; j++) {
				var leftCounts = this.num - 20 * j;
				var toRenderCount = leftCounts > 20 ? 20 : leftCounts;
				var rows = sheetUtil.optionGroupMode === 'FIXED' ? 5: Math.ceil(toRenderCount / 4);
				rows = rows < 2 ? 2 : rows;
				height += this.isPageSizeLarge() ? (5.3 * rows + 1.5) : (4.5 * rows + 2.5);
			}
			return height;
		},
		calcPerGroupHeight: function(count){
			var rows = sheetUtil.optionGroupMode === 'FIXED' ? 5: Math.ceil(count / 4);
			rows = rows < 2 ? 2 : rows;
			return this.isPageSizeLarge() ? (5.3 * rows + 1.5) : (4.5 * rows + 2.5);
		},
		updateStartOrder: function(order, count){
			this.startOrder = order - count;
			Event.trigger('REBUILD_PAGE');
		},
		setStartOrder: function(o){
			if(!this.startOrder) {
				this.startOrder = o;
			}
		}
	});

	//单个选择题组件对象
	sheetElement.Option = sheetElement.SheetComp.extend({
		construct: function(type, order, pageSize, sortOrder, optionCount, optionMode, parent){
			this.mode = type;
			this.order = order;
			this.sortOrder = sortOrder;
			this.parent = parent;
			this.pageSize = pageSize;
			this.dom = '';
			this.option = ['A', 'B', 'C', 'D'];
			this.optionCount = optionCount || 4;
			this.optionMode = optionMode;
			this._super(this.isPageSizeLarge() ? 5.3 : 4.5, 'OPTION', true, true);
		},
		init: function(){
			var self = this;
			this.$dom = $('<div class="option-box">').css({
				height: this.height + 'mm'
			});
			$('<span class="order" contenteditable="true">').text(this.order).appendTo(this.$dom).css({
				height: this.height + 'mm',
				'line-height': this.height + 'mm'
			}).off('blur').on('blur', function(){
				var text = $(this).text();
				if(text && +text && _.isNumber(+text) && +text !== self.order) {
					self.order = +text;
					self.parent.updateStartOrder(self.order, self.sortOrder);
				}
			});
			_.each(this.option, function(o, index){
				if(self.optionMode === 'judge' && self.optionCount === 2){
					var judgeText = ['T', 'F', 'A', 'A'];
					$('<span class="option">').text(judgeText[index]).appendTo(self.$dom).css({
						height: self.height + 'mm',
						'line-height': self.height + 'mm',
						opacity: ((index + 1) <= self.optionCount) ? '1' : '0'
					});
				} else {
					$('<span class="option">').text(o).appendTo(self.$dom).css({
						height: self.height + 'mm',
						'line-height': self.height + 'mm',
						opacity: ((index + 1) <= self.optionCount) ? '1' : '0'
					});
				}
			});
			if(this.mode === 1) {
				this.$dom.addClass('option-h');
			}else {
				this.$dom.addClass('option-v');
			}
			return this;
		},
		isPageSizeLarge: function(){
			return (this.pageSize === 'A4' || this.pageSize === 'A3');
		}
	});
})();