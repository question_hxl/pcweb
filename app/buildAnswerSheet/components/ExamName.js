/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function($, _){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //试卷名称组件
    sheetElement.ExamName = sheetElement.SheetComp.extend({
        construct: function(name, height){
            var HEIGHT = 18;
            this._super(HEIGHT, 'EXAM_NAME', true, true);
            this.name = name || '点击编辑试卷名';
        },
        init: function(){
            this.$dom = $('<div class="exam-title">').css({
                'text-align': 'center',
                height: this.height + 'mm',
                'font-size': '24px',
                display: 'flex',
                'justify-content': 'center',
                'align-items': 'center',
                'font-weight': 'bold',
                'padding': '0 20px'
            });
            $('<span class="exam-title-ctx" contenteditable="true">').html(this.name).css({
                'vertical-align': 'middle',
                'min-width': '50mm',
                display: 'inline-block',
                'max-width': '100%',
                'line-height': 1.2,
                'font-family': '宋体'
            }).appendTo(this.$dom);
            this.text = this.$dom.find('.exam-title-ctx').text();
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$dom.find('.exam-title-ctx').off('blur').on('blur', function(e){
                e.preventDefault();
                e.stopPropagation();
                self.name = $(this).html();
                self.text = $(this).text();
            });
        }
    });
})(jQuery, _);