/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //密封条组件
    sheetElement.SecretBox = function(config, secretType){
        this.width = 27;
        this.config = config;
        this.secretType = secretType || 'PAINT';
    };
    sheetElement.SecretBox.prototype = {
        init: function(){
            this.$dom = $('<div class="secret-box">').css({
                height: '100%',
                width: this.width + 'mm',
                'margin': 'auto',
                position: 'absolute',
                left: '0',
                top: '0'
            });
            var ocrReadImgSrc = location.origin + '/buildAnswerSheet/secretBox.jpg';
            var fillImgSrc = location.origin + '/buildAnswerSheet/secretBox-input.jpg';
            // var $container = $('<div class="container">').css({
            //     width: '100%',
            //     display: 'flex',
            //     'justify-content': 'space-around'
            // }).appendTo(this.$dom);
            // var $name = $('<div class="name"><span>姓名</span><span class="line"></span></div>').css({
            //     'align-self': 'flex-end'
            // });
            // $name.find('.line').css({
            //     width: '25mm',
            //     height: '5mm',
            //     display: 'inline-block',
            //     'border-bottom': '1px solid #000'
            // });
            // var tdStr = '';
            // for(var i = 0; i <= 9; i++) {
            //     tdStr += '<td>' + i + '</td>';
            // }
            // var $classNo = $('<div class="class-no">');
            // var $table = $('<table border="1" bordercolor="#000">' +
            //     '<tr><td rowspan="2">班级</td><td>&nbsp;</td>' + tdStr + '</tr>' +
            //     '<tr><td>&nbsp;</td>' + tdStr + '</tr></table>');
            // $table.appendTo($classNo);
            // var $stuNo = $('<div class="stu-no">');
            // var $tableStu = $('<table border="1" bordercolor="#000">' +
            //     '<tr><td rowspan="2">学号</td><td>&nbsp;</td>' + tdStr + '</tr>' +
            //     '<tr><td>&nbsp;</td>' + tdStr + '</tr></table>');
            // $tableStu.appendTo($stuNo);
            // $name.appendTo($container);
            // $classNo.appendTo($container);
            // $stuNo.appendTo($container);
            // $('<div class="matrixArea">').css({
            //     right: this.config.topMargin + 1.5 + 'mm'
            // }).text('二维码区域').appendTo($container);
            var img = this.secretType === 'PAINT' ? ocrReadImgSrc : fillImgSrc;
            var $img = $('<img>').css({
                width: '100%',
                height: '100%'
            }).attr('src', img).appendTo(this.$dom);
            return this;
        },
        render: function(node){
            this.init().$dom.appendTo(node);
        }
    };
})();