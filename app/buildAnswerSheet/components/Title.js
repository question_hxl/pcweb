/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //标题组件
    sheetElement.Title = sheetElement.SheetComp.extend({
        construct: function(title, height){
            var HEIGHT = height || 8;
            this._super(HEIGHT, 'TITLE');
            this.title = title || '点击编辑标题';
        },
        init: function(){
            this.$dom = $('<div class="sheet-ele"><div class="sheet-title-container"></div></div>').addClass('title').css({
                height: this.height + 'mm',
                'line-height': this.height + 'mm'
            });
            this.$titleContainer = this.$dom.find('.sheet-title-container').css({
                'overflow': 'hidden',
                width: '100%',
                height: '100%'
            });
            this.$textContainer = $('<div class="sheet-title" contenteditable="true">').css({
                width: '100%',
                height: '100%',
                lineHeight: this.height + 'mm'
            }).text(this.title).appendTo(this.$titleContainer);
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$textContainer.off('blur').on('blur', function(e){
                self.title = self.$textContainer.text();
                self.$textContainer.text(self.title);
            }).off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('PAGE_ELE_DEACTIVE');
                self.active();
            });
        }
    });
})();