/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
	var sheetElement = window.sheetElement = window.sheetElement || {};
	//解答题组件父类
	sheetElement.SolveComp = sheetElement.SheetComp.extend({
		construct: function(height, type, html, lineHeight) {
			this._super(height || 50, type);
			this.innerHtml = html || '';
			this.lineHeight = lineHeight || '6mm';
		},
		render: function(node){
			this.init().$dom.appendTo(node);
			this.listenEvent();
			this.supportHover();
			this.supportAddComp();
			this.supportDelete();
			this.supportDragResize(this.$dom);
			this.supportImgDrag();
			this.supportImgResize();
			// this.supportXZTComp();
			// this.supportImgUpload();
			this.supportActions();
			var $lineHeight = $('<div class="line-height-box"><ul class="lineheight-list">' +
				'<li data-lh="7"><span>7mm（默认）</span></li>' +
				'<li data-lh="8"><span>8mm</span></li>' +
				'<li data-lh="10"><span>10mm</span></li>' +
				'<li data-lh="12"><span>12mm</span></li>' +
				'<li data-lh="14"><span>14mm</span></li>' +
				'</ul></div>').css({
				display: 'none'
			});
			this.addAction('dist/img/lineheight.png', '设置行距', function(comp){
				$lineHeight.toggle();
				$lineHeight.find('li').click(function(e){
					e.preventDefault();
					e.stopPropagation();
					$lineHeight.hide();
					Event.trigger('SET_LINE_HEIGHT', {
						comp: comp,
						lh: $(this).data('lh')
					});
				});
			}, $lineHeight);
			this.addAction('dist/img/underline.png', '下划线', function(comp){
				comp.drawLine();
			});
			this.addAction('dist/img/space.png', '插入空格', function(comp){
				comp.drawSpace();
			});
			this.addAction('dist/img/image.png', '插入图片', function(comp){
				Event.trigger('INSERT_IMG', comp);
			});
			this.addAction('dist/img/xzt.png', '插入选做题内容', function(comp){
				Event.trigger('ADD_XZT_COMP', comp);
			});
		},
		supportDragResize: function(node){
			var self = this;
			var $arrow = $('<div class="arrow">').css({
				position: 'absolute',
				right: '10px',
				bottom: '10px',
				width: '16px',
				height: '20px',
				'z-index': '10',
				cursor: 'ns-resize'
			}).appendTo(node);
			var $title = $('<div class="info">').text('拖拽调整高度').css({
				position: 'absolute',
				display: 'block',
				width: '80px',
				right: '15px',
				color: '#999',
				fontSize: '12px',
				top: '2px'
			}).appendTo($arrow);
			var isMouseDown = false;
			var dy = 0, my = 0, uy = 0;
			var nodeHeight = node.height();
			$arrow.off('mousedown.arrow').on('mousedown.arrow', function(e){
				dy = e.clientY;
				isMouseDown = true;
				var currentHeight = node.height();
				$(window).off('mousemove.arrow').on('mousemove.arrow', function(eMove){
					if(isMouseDown) {
						my = eMove.clientY;
						node.css({
							height: currentHeight + my - dy + 'px'
						});
					}
				});
				$(window).off('mouseup.arrow').on('mouseup.arrow', function(eUp){
					isMouseDown = false;
					uy = eUp.clientY;
					var heightMinus = uy - dy;
					if(heightMinus !== 0) {
						node.css({
							height: heightMinus + nodeHeight + 'px'
						});
						self.addHeight(heightMinus / sheetUtil.MM2PX_RATIO);
						Event.trigger('REBUILD_PAGE');
					}
					$(window).off('mouseup.arrow').off('mousemove.arrow');
				});
			});
		},
		supportImgDrag: function(){
			var isMouseDown = false;
			var dx = 0, dy = 0, mx = 0, my = 0;
			var order = this.orderInPage;
			var that = this;
			this.$dom.delegate('img', 'mousedown.dragImg', function(e){
				e.preventDefault();
				e.stopPropagation();
				var $img = $(this);
				isMouseDown = true;
				dx = e.clientX;
				dy = e.clientY;
				var currentLeft = parseInt($img.css('left')) || 0,currentTop = parseInt($img.css('top')) || 0;
				$(window).off('mousemove.dragImg').on('mousemove.dragImg', function(e){
					if(isMouseDown) {
						e.preventDefault();
						e.stopPropagation();
						mx = e.clientX;
						my = e.clientY;
						$img.css({
							position: 'absolute',
							left: currentLeft + mx - dx + 'px',
							top: currentTop + my - dy + 'px'
						});
					}
				});
				$(window).off('mouseup.dragImg').on('mouseup.dragImg', function(e){
					isMouseDown = false;
					$(window).off('mouseup.dragImg').off('mousemove.dragImg');
					that.innerHtml = html2standard(that.$dom.find('.solve-qst-content').html());
					Event.trigger('UPDATE_PARENT_SOLVE_HTML', order);
				});
			});
		},
		supportImgResize: function(){
			var order = this.orderInPage;
			var that = this;
			this.$dom.delegate('img', 'mousewheel', function(e){
				e.preventDefault();
				e.stopPropagation();
				var imgWidth = $(this).width(),
					imgHeight = $(this).height();
				if(e.originalEvent.deltaY > 0) {
					//向下滚动滚轮，增大图片尺寸
					$(this).width(imgWidth + 20);
					$(this).css({height: 'auto'});
					$(this).css('max-height', 'none');
				}else {
					if(imgWidth > 20 && imgHeight > 20) {
						$(this).width(imgWidth - 20);
						$(this).css({height: 'auto'});
						$(this).css('max-height', 'none');
					}
				}
				that.innerHtml = html2standard(that.$dom.find('.solve-qst-content').html());
				Event.trigger('UPDATE_PARENT_SOLVE_HTML', order);
			});
		},
		drawLine: function(){
			var $box = this.$dom.find('.solve-qst-content');
			$box = ($box && $box.length > 0) ? $box : this.$dom;
			var $line = $('<span></span>').css({
				fontFamily: 'Arial,宋体',
				fontSize: '14px'
			}).appendTo($box);
			// $line.html('<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
			//     '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>');
			$line.html('_____________________________________________________________________________________');
			this.innerHtml += $line.get(0).outerHTML;
		},
		drawSpace: function(){
			var $box = this.$dom.find('.solve-qst-content');
			$box = ($box && $box.length > 0) ? $box : this.$dom;
			var $space = $('<span>&nbsp;</span>').css({
				fontFamily: 'Arial,宋体',
				fontSize: '14px'
			}).appendTo($box);
			this.innerHtml += $space.get(0).outerHTML;
		},
		setLineHeight: function(lineheight){
			var $box = this.$dom.find('.solve-qst-content');
			$box = ($box && $box.length > 0) ? $box : this.$dom;
			$box.css({
				'line-height': lineheight
			});
			$box.find('*').css({
				'line-height': 'inherit'
			});
			this.lineHeight = lineheight;
		},
		addHeight: function(height){
			Event.trigger('ADD_PARENT_SOLVE_HEIGHT', {
				order: this.orderInPage,
				height: height
			});
		},
		// supportXZTComp: function(){
		//     var that = this;
		//     var $addBtn = $('<div class="comp-add-sel operation-btn" title="添加选做题内容"></div>').css({
		//         width: '20px',
		//         height: '20px',
		//         position: 'absolute',
		//         left: '0',
		//         top: '-20px',
		//         background: 'url(dist/img/btn_mimax_a.png) 75px -5px',
		//         'background-color': '#fff',
		//         color: '#ccc',
		//         display: 'none',
		//         'z-index': '15',
		//         cursor: 'pointer',
		//         border: '1px solid #ccc'
		//     }).appendTo(this.$dom);
		//     $addBtn.off('click').on('click', function(e){
		//         e.preventDefault();
		//         Event.trigger('ADD_XZT_COMP', that);
		//     });
		// },
		addXZTNode: function(orders){
			var xzt = new sheetElement.XZTComp(orders);
			xzt.getDom().appendTo(this.$dom.find('.solve-qst-content'));
			this.innerHtml = html2standard(this.$dom.find('.solve-qst-content').html());
			Event.trigger('UPDATE_PARENT_SOLVE_HTML', this.orderInPage);
		},
		// supportImgUpload: function(){
		//     var self = this;
		//     var $del = $('<div class="comp-add operation-btn" title="插入图片"></div>').css({
		//         width: '20px',
		//         height: '20px',
		//         position: 'absolute',
		//         left: '20px',
		//         top: '-20px',
		//         background: 'url(dist/img/image.png)',
		//         'background-color': '#fff',
		//         'background-size': '100% 100%',
		//         color: '#ccc',
		//         display: 'none',
		//         'z-index': '15',
		//         cursor: 'pointer',
		//         border: '1px solid #ccc',
		//         'border-right': 'none'
		//     }).appendTo(this.$dom);
		//     $del.off('click').on('click', function () {
		//         Event.trigger('INSERT_IMG', self);
		//     });
		// },
		supportActions: function(){
			this.$optionBtnGroup = $('<div class="operation-btn-group"></div>').css({
				position: 'absolute',
				width: '160px',
				height: '25px',
				left: '-6px',
				top: '-23px',
				background: 'url(dist/img/sheet-operation-bg.png)',
				backgroundSize: '100% 100%',
				'z-index': '15',
				padding: '2px 5px'
			}).appendTo(this.$dom);
		},
		addAction: function(btnImg, title, cb, $toggleContent){
			var self = this;
			var $action = $('<div class="operation-btn" title="' + title + '"></div>').css({
				width: '30px',
				height: '20px',
				position: 'relative',
				background: 'url(' + btnImg + ')',
				backgroundSize: 'auto 100%',
				backgroundRepeat: 'no-repeat',
				backgroundPosition: '50%',
				display: 'block',
				cursor: 'pointer',
				float: 'left',
				marginRight: '0'
			}).appendTo(this.$dom).appendTo(this.$optionBtnGroup);
			if($toggleContent) {
				$toggleContent.appendTo($action).css({
					position: 'absolute',
					left: 0,
					top: '20px',
					width: '100px'
				});
			}
			$action.off('click').on('click', function (e) {
				e.preventDefault();
				e.stopPropagation();
				cb(self);
			});
			$action.off('dblclick').on('dblclick', function(e){
				e.preventDefault();
				e.stopPropagation();
			});
		}
	});

	//解答题组件
	sheetElement.SolveQst = sheetElement.SolveComp.extend({
		construct: function(order, score, sub, mode, height, img, html, lineheight){
			this._super(height || 50, 'SOLVE_MAIN', html, lineheight);
			this.order = order;
			this.score = score;
			this.sub = sub;
			this.mode = mode;
			this.img = img;
			this.innerHtml = html;
			this.lineHeight = lineheight || '6mm';
		},
		init: function(){
			var self = this;
			this.$dom = $('<div class="solve-qst-box sheet-ele">').css({
				width: '100%',
				height: this.height + 'mm',
				border: '1px solid #000',
				position: 'relative'
			});
			var scoreBarHeight = sheetUtil.isMarkOnLine ? 0 : 4.5;
			if(!sheetUtil.isMarkOnLine) {
				var scoreBar = new sheetElement.ScoreBar(this.score, this.sub, this.mode);
				var $scoreBar = $('<div class="score-bar-box">').css({
					width: '100%',
					height: scoreBarHeight + 'mm',
					position: 'relative'
				}).appendTo(this.$dom);
				scoreBar.render($scoreBar);
			}
			var $content = $('<div class="solve-qst-content" contenteditable="true">').css({
				width: '100%',
				height: this.height - scoreBarHeight + 'mm',
				'position': 'relative',
				padding: '1mm',
				'overflow': 'hidden',
				'line-height': this.lineHeight,
				fontFamily: 'Arial,宋体',
				fontSize: '14px'
			}).appendTo(this.$dom);
			$content.off('blur').on('blur', function(){
				self.innerHtml = html2standard($(this).html());
				Event.trigger('UPDATE_PARENT_SOLVE_HTML', self.orderInPage);
			});
			if(this.innerHtml) {
				$(html2standard(this.innerHtml)).appendTo($content);
			}else {
				$('<div class="solve-inner-def">').css({
					'font-family': 'Arial,宋体'
				}).text(this.order + '.（' + this.score + '分）').appendTo($content);
				if(this.img && this.img.src) {
					var imgMaxHeight = this.height - scoreBarHeight;
					var imgTop = 0;
					if(this.mode === 'sub' && this.sub && this.sub.length > 0) {
						var subHeight = (this.sub.length - 1) * scoreBarHeight;
						imgMaxHeight = imgMaxHeight - subHeight;
						imgTop = subHeight;
					}
					$('<img class="solve-inner-def">').attr('src', this.img.src).css({
						position: 'absolute',
						right: 0,
						top: imgTop + 'mm',
						width: this.img.width + 'mm',
						height: this.img.height + 'mm',
						'max-height': imgMaxHeight + 'mm'
					}).appendTo($content);
				}
				this.innerHtml = html2standard($content.html());
				Event.trigger('UPDATE_PARENT_SOLVE_HTML', this.orderInPage);
			}
			return this;
		},
		setScoreAndMode: function(mode, score, sub){
			this.mode = mode;
			this.score = score;
			this.sub = sub;
		},
		listenEvent: function(cb){
			var self = this;
			this.$dom.off('click').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('PAGE_ELE_DEACTIVE');
				self.active();
			});
			this.$dom.off('dblclick').on('dblclick', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('MODIFY_SOLVE_SCORE', self);
			});
		}
	});

	//跨页部分解答题组件
	sheetElement.SolveHalf = sheetElement.SolveComp.extend({
		construct: function(height, html, lineHeight){
			this._super(height, 'SOLVE_HALF', html, lineHeight);
		},
		init: function(){
			var self = this;
			this.$dom = $('<div class="solve-qst-box sheet-ele">').css({
				width: '100%',
				height: this.height + 'mm',
				border: '1px solid #000',
				'line-height': this.lineHeight,
				position: 'relative'
			});
			var $content = $('<div class="solve-qst-content" contenteditable="true">').css({
				width: '100%',
				height: this.height + 'mm',
				'position': 'relative',
				padding: '1mm',
				'line-height': this.lineHeight,
				fontFamily: 'Arial, 宋体',
				fontSize: '14px'
			}).appendTo(this.$dom);
			$content.off('blur').on('blur', function(){
				self.innerHtml = html2standard($(this).html());
				Event.trigger('UPDATE_PARENT_SOLVE_HTML', self.orderInPage);
			});
			if(this.innerHtml) {
				$(html2standard(this.innerHtml)).appendTo($content);
			}
			return this;
		},
		listenEvent: function(){
			var self = this;
			this.$dom.off('click').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('PAGE_ELE_DEACTIVE');
				self.active();
			});
		}
	});
	//整合解答题组件，放到Page中
	sheetElement.SolveFull = function(order, score, sub, mode, height, img, mainHtml, halfHtml, lineheight){
		this.order = order;
		this.score = score;
		this.sub = sub;
		this.mode = mode;
		this.img = img;
		this.height = height || sheetUtil.guessHeightWithScore(score);
		this.type = 'SOLVE';
		this.mainHtml = mainHtml;
		this.halfHtml = halfHtml;
		this.lineHeight = lineheight;
	};
	sheetElement.SolveFull.prototype = {
		init: function(leftHeight, boxHeight){
			var self = this;
			if(leftHeight > 15) {
				if(this.height > leftHeight) {
					this.comps = [new sheetElement.SolveQst(this.order, this.score, this.sub, this.mode, leftHeight, this.img, this.mainHtml, this.lineHeight)];
					var otherHeight = this.height - leftHeight;
					this.fillOther(otherHeight, boxHeight);
					return this.comps;
				}else {
					this.comps = new sheetElement.SolveQst(this.order, this.score, this.sub, this.mode, this.height, this.img, this.mainHtml, this.lineHeight);
					this.comps.orderInPage = this.orderInPage;
					return this.comps;
				}
			}else {
				if(this.height > boxHeight) {
					this.comps = [new sheetElement.SolveQst(this.order, this.score, this.sub, this.mode, boxHeight, this.img, this.mainHtml, this.lineHeight)];
					var otherHeight = this.height - boxHeight;
					this.fillOther(otherHeight, boxHeight);
					return this.comps;
				}else {
					this.comps = new sheetElement.SolveQst(this.order, this.score, this.sub, this.mode, this.height, this.img, this.mainHtml, this.lineHeight);
					this.comps.orderInPage = this.orderInPage;
					return this.comps;
				}
			}
		},
		fillOther: function(otherHeight, boxHeight){
			var html = this.halfHtml, self = this;
			if(this.renderedHalfHtml) {
				html = '';
			}
			if(otherHeight > boxHeight) {
				var oh = otherHeight - boxHeight;
				this.comps.push(new sheetElement.SolveHalf(boxHeight, html, this.lineHeight));
				this.renderedHalfHtml = true;
				this.fillOther(oh, boxHeight);
			}else {
				this.comps.push(new sheetElement.SolveHalf(otherHeight, html, this.lineHeight));
				_.each(this.comps, function(c){
					c.orderInPage = self.orderInPage;
				});
			}
		},
		addHeight: function(height){
			Event.trigger('ADD_PARENT_SOLVE_HEIGHT', {
				order: this.orderInPage,
				height: height
			});
		},
		drawLine: function(){
			if(_.isArray(this.comps)) {
				_.each(this.comps, function(c){
					if(c.type === 'SOLVE_MAIN' || c.type === 'SOLVE_HALF') {
						c.drawLine();
					}
				});
			}else if(_.isObject(this.comps)) {
				this.comps.drawLine();
			}
		},
		setLineHeight: function(lineheight){
			this.lineHeight = lineheight;
			if(_.isArray(this.comps)) {
				_.each(this.comps, function(c){
					if(c.type === 'SOLVE_MAIN' || c.type === 'SOLVE_HALF') {
						c.setLineHeight(lineheight);
					}
				});
			}else if(_.isObject(this.comps)) {
				this.comps.setLineHeight(lineheight);
			}
		},
		active: function(){
			if(_.isArray(this.comps)) {
				_.each(this.comps, function(c){
					c.active();
				});
			}else if(_.isObject(this.comps)) {
				this.comps.active();
			}
		},
		deactive: function(){
			if(_.isArray(this.comps)) {
				_.each(this.comps, function(c){
					c.deactive();
				});
			}else if(_.isObject(this.comps)) {
				this.comps.deactive();
			}
		},
		updateHtml: function(){
			var self = this;
			if(_.isArray(this.comps)) {
				_.each(this.comps, function(c){
					if(c.type === 'SOLVE_MAIN') {
						self.mainHtml = c.innerHtml;
					}else if(c.type === 'SOLVE_HALF') {
						self.halfHtml = c.innerHtml;
					}
				});
			}else if(_.isObject(this.comps)) {
				self.mainHtml = this.comps.innerHtml;
			}
		}
	};

	function html2standard(html){
		var $p = $('<div>').html(html);
		return dealNode($p).join('');
	}
	function dealNode($p){
		var $contents = $p.contents();
		var res = [];
		var self = this;
		$contents.each(function(){
			var node = this;
			if(node.nodeType === 3) {
				//如果是文本节点,如果小于50个字符，则附加到前一个上面，否则另起一行
				if(node.nodeValue.length < 50) {
					if(res.length > 0) {
						//如果存在前一个元素
						var $lastEle = $(res[res.length - 1]);
						// if(isZBHTag($lastEle.get(0).tagName.toUpperCase()) || sheetUtil.isInlineTag($lastEle.get(0).tagName)) {
						// 	res[res.length - 1] = res[res.length - 1] += node.nodeValue;
						// }else {
						// 	$lastEle.html($lastEle.html() + node.nodeValue);
						// 	res[res.length - 1] = $lastEle.get(0).outerHTML;
						// }
						$lastEle.html($lastEle.html() + node.nodeValue);
						res[res.length - 1] = $lastEle.get(0).outerHTML;
					}else {
						//否则创建元素
						var $ele = $('<p>').html(node.nodeValue);
						res.push($ele.get(0).outerHTML);
					}
				}else {
					var $ele = $('<p>').html(node.nodeValue);
					res.push($ele.get(0).outerHTML);
				}
			}else {
				if(res.length > 0) {
					var $lastEle = $(res[res.length - 1]);
					if(isZBHTag($(node).get(0).tagName) || sheetUtil.isInlineTag($(node).get(0).tagName)) {
						// res[res.length - 1] = res[res.length - 1] += $(node).get(0).outerHTML;
						$lastEle.html($lastEle.html() + $(node).get(0).outerHTML);
						res[res.length - 1] = $lastEle.get(0).outerHTML;
					}else {
						res.push($(node).get(0).outerHTML);
					}
				}else {
					res.push($(node).get(0).outerHTML);
				}
			}
		});
		return res;
	}
	function isZBHTag(tagName){
		var zbhTags = ['IMG', 'BR', 'HR', 'INPUT'];
		return $.inArray(tagName, zbhTags) >= 0;
	}
})();
