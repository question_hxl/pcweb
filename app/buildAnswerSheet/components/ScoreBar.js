/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //分数条组件
    sheetElement.ScoreBar = function(score, sub, mode){
        this.mode = mode;        //normal    ,   sub
        this.sub = sub;
        this.score = score;
        this.height = 4.5;
    };
    sheetElement.ScoreBar.prototype = {
        init: function(){
            this.$dom = $('<div class="score-bar">').css({
                width: '100%',
                height: this.height + 'mm',
                position: 'relative'
            });
            if(this.mode === 'sub' && this.sub && this.sub.length > 0) {
                var $main = this.createMain(this.sub[0].score);
                $main.appendTo(this.$dom);
                var leftSubs = this.sub.slice(1);
                var minScore = Math.max.apply(this, _.pluck(leftSubs, 'score'));
                var minColNum = (minScore + 1) < 4 ? 4 : (minScore + 1);
                for(var i = 0; i < leftSubs.length; i++) {
                    var score = leftSubs[i].score;
                    if(score < minColNum - 1) {
                        score = minColNum - 1;
                    }
                    var widthPercent = (score + 1) * 100 / 22;
                    var $subTable = $('<div class="sub-score-box">').css({
                        width: widthPercent + '%',
                        height: this.height + 'mm',
                        'font-size': '12px',
                        position: 'absolute',
                        top: this.height * (i+1) + 'mm',
                        right: '1px',
                        'z-index': '10',
                        overflow: 'hidden',
                        'border-bottom': '1px solid #000'
                    });
                    var tdWP = 100 / (score + 1);
                    for(var j = score; j >= 0; j--) {
                        var borderTop = '';
                        if(i === 0 || j <= leftSubs[i - 1].score || j <= minColNum) {
                            borderTop = 'none';
                        }else {
                            borderTop = '1px solid #000';
                        }
                        $('<div>').css({
                            height: this.height + 'mm',
                            float: 'left',
                            width: tdWP + '%',
                            'text-align': 'center',
                            'border-top': borderTop,
                            'border-left': '1px solid #000',
                            'border-right': 'none',
                            'border-bottom': 'none'
                        }).text(j > leftSubs[i].score ? '-' : j).appendTo($subTable);
                    }
                    $subTable.appendTo(this.$dom);
                }
            }else {
                var $table = this.createMain(this.score);
                $table.appendTo(this.$dom);
            }
            return this;
        },
        createMain: function(score){
            var $table = $('<div>').css({
                width: '100%',
                height: this.height + 'mm',
                'font-size': '12px',
                // 'border-bottom': '1px solid #000',
                overflow: 'hidden'
            });
            var $col = $('<div>').css({
                width: 100/22 + '%',
                height: this.height + 'mm',
                'text-align': 'left',
                'text-indent': '2px',
                'border-right': '1px solid #000',
                'border-bottom': '1px solid #000',
                float: 'left'
            }).appendTo($table);
            var blackBlock = new sheetElement.BlackBlock();
            blackBlock.init().$dom.css({
                'margin-top': '0.75mm'
            }).appendTo($col);
            if(score > 18.5) {
                if(!sheetUtil.showHalfScore) {
                    $('<div>').css({
                        height: this.height + 'mm',
                        width: 100/22 + '%',
                        float: 'left',
                        'border-right': '1px solid #000',
                        'border-bottom': '1px solid #000',
                    }).text('').appendTo($table);
                }else {
                    $('<div>').css({
                        height: this.height + 'mm',
                        width: 100/22 + '%',
                        float: 'left',
                        'position': 'relative',
                        'border-right': '1px solid #000',
                        'border-bottom': '1px solid #000',
                    })
                        // .html('<img src="http://www.51jyfw.com/dist/img/half-score.png" style="position: absolute;top: 3px;left: 8px;">')
                        .text('')
                        .appendTo($table);
                }

            }else {
                var td = $('<div>').css({
                    height: this.height + 'mm',
                    width: 100/22 + '%',
                    float: 'left',
                    'border-right': '1px solid #000',
                    'border-bottom': '1px solid #000',
                    'text-align': 'center'
                });
                $('<div>').css({
                    border: '6px solid #000',
                    'border-radius': '10px',
                    'top': '1.5px',
                    'position': 'relative',
                    'display': 'inline-block'
                }).appendTo(td);
                td.appendTo($table);
            }
            var scoreArray = [];
            if(score > 18.5) {
                var num = Math.floor(score / 10);
                for(var i = 1; i <= 10; i++) {
                    if(i <= num) {
                        scoreArray.push(i);
                    }else {
                        scoreArray.push('-');
                    }
                }
                for(var j = 9; j >=0; j--) {
                    scoreArray.push(j);
                }
            }else {
                if(!sheetUtil.showHalfScore) {
                    scoreArray.push('');
                }else {
                    scoreArray.push('<img src="http://www.51jyfw.com/dist/img/half-score.png" style="position: absolute;top: 3px;left: 8px;">');
                }
                for(var i = 18; i >= 0 ;i--) {
                    if(i <= Math.floor(score)) {
                        scoreArray.push(i);
                    }else {
                        scoreArray.push('-');
                    }
                }
            }
            console.log(scoreArray);
            for(var i = 0; i < scoreArray.length; i++) {
                $('<div>').css({
                    height: this.height + 'mm',
                    width: 100/22 + '%',
                    float: 'left',
                    'text-align': 'center',
                    'border-right': (i === scoreArray.length - 1) ? 'none' : '1px solid #000',
                    'border-bottom': '1px solid #000',
                    position: 'relative'
                }).html(scoreArray[i]).appendTo($table);
            }
            return $table;
        },
        render: function(node){
            this.init().$dom.appendTo(node);
        }
    };
})();