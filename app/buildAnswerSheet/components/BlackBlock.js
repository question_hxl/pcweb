/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //黑块组件
    sheetElement.BlackBlock = function(){
        this.height = 3;
        this.width = 5;
    };
    sheetElement.BlackBlock.prototype = {
        init: function(){
            this.$dom = $('<div>').addClass('black-block').css({
                display: 'inline-block',
                'border-left': this.width + 'mm solid #000',
                'border-top': this.height + 'mm solid #000'
            });
            return this;
        },
        render: function(node){
            this.init().$dom.appendTo(node);
            return this;
        }
    };
})();