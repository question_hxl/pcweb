/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    sheetElement.XZTComp = Class.extend({
        construct: function(orders){
            this.orders = orders.split(',');
        },
        getDom: function(){
            var $dom = $('<div class="xzt">');
            $('<span>').text('我选做的题目是：').appendTo($dom);
            _.each(this.orders, function(o){
                var $box = $('<div>').css({
                    display: 'inline-block'
                });
                $('<span>').text(o).appendTo($box);
                $('<div>').css({
                    display: 'inline-block',
                    width: '6mm',
                    height: '3mm',
                    border: '1px solid #000',
                    'margin-left': '5px'
                }).appendTo($box);
                $box.appendTo($dom);
            });
            $dom.append('&nbsp;');
            return $dom;
        }
    });
})();