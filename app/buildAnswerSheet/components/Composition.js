/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    sheetElement.CompositionWithScore = sheetElement.SheetComp.extend({
        construct: function(score, num, fillHeight, rowHeight, spaceHeight, count){
            this._super(fillHeight, 'COMPOSITION_WITH_SCORE', false, true);
            this.score = score;
            this.num = num;
            this.rows = Math.ceil(this.num / 20);
            this.rowHeight = rowHeight;
            this.spaceHeight = spaceHeight;
            this.contentheight = this.rows * (this.rowHeight + this.spaceHeight);
            this.count = count || 0;
        },
        init: function(){
            this.$dom = $('<div class="composition sheet-ele">').css({
                width: '100%',
                height: this.height + 'mm',
                position: 'relative',
            });
            if(!sheetUtil.isMarkOnLine) {
                var scoreBar = new sheetElement.ScoreBar(this.score, this.sub, this.mode);
                var $scoreBar = $('<div class="score-bar-box">').css({
                    width: '100%',
                    height: scoreBar.height + 'mm',
                    position: 'relative',
                    border: '1px solid #000',
                    'border-bottom': 'none'
                }).appendTo(this.$dom);
                scoreBar.render($scoreBar);
            }
            var $compositionArea = $('<div class="composition-area">').css({
                width: '100%',
                height: this.contentheight + 'mm'
            });
            var $table = $('<table width="100%" border="0" cellspacing="0" cellpadding="0">').css({
                'border-top': sheetUtil.isMarkOnLine ? '1px solid #000' : 'none'
            });
            $('<tr><td colspan="20"></td></tr>').css({
                height: this.spaceHeight + 'mm',
                'text-align': 'right',
                padding: 0,
                'line-height': '0.5'
            }).appendTo($table);
            for(var i = 0; i < this.rows; i++) {
                var $tr = $('<tr>').css({
                    height: this.rowHeight + 'mm',
                    overflow: 'hidden'
                });
                for(var j = 0; j < 20; j++) {
                    this.count++;
                    var $td = $('<td>').html('&nbsp;').appendTo($tr);
                }
                $tr.appendTo($table);
                var $space = $('<tr><td colspan="20"></td></tr>').css({
                    height: this.spaceHeight + 'mm',
                    'text-align': 'right',
                    padding: 0,
                    'line-height': '0.5'
                }).appendTo($table);
                if(this.count % 100 === 0) {
                    var $spaceTd = $space.find('td');
                    $spaceTd.css({
                        height: this.spaceHeight + 'mm',
                        position: 'relative'
                    });
                    $("<span>").text(this.count + '字').css({
                        'text-align': 'right',
                        'font-size': '12px',
                        'line-height': '0.5',
                        display: 'inline-block',
                        'transform-origin': '0',
                        '-webkit-transform': 'scale(0.8)',
                        '-o-transform': 'scale(0.8)',
                        '-moz-transform': 'scale(0.8)',
                        '-ms-transform': 'scale(0.8)',
                        'transform': 'scale(0.8)',
                        position: 'absolute',
                        right: '2px',
                        top: '1px'
                    }).appendTo($spaceTd);
                }
            }
            $table.appendTo($compositionArea);
            $compositionArea.appendTo(this.$dom);
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$dom.off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('PAGE_ELE_DEACTIVE');
                self.active();
            });
            this.$dom.off('dblclick').on('dblclick', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('MODIFY_SOLVE_SCORE', self);
            });
        }
    });

    sheetElement.CompositionHalf = sheetElement.SheetComp.extend({
        construct: function(num, leftHeight, rowHeight, spaceHeight, count){
            this.num = num;
            this.rowHeight = rowHeight;
            this.spaceHeight = spaceHeight;
            this.count = count || 0;
            this.rows = Math.ceil(this.num / 20);
            this._super(this.rows * (this.rowHeight + this.spaceHeight), 'COMPOSITION_HALF', false, true);
        },
        init: function(){
            this.height = (this.rowHeight + this.spaceHeight) * this.rows;
            var $compositionArea = $('<div class="composition-area-half sheet-ele">').css({
                width: '100%',
                height: this.height + 'mm',
            });
            var $table = $('<table width="100%" border="0" cellspacing="0" cellpadding="0">');
            for(var i = 0; i < this.rows; i++) {
                var $tr = $('<tr>').css({
                    height: this.rowHeight + 'mm',
                    overflow: 'hidden'
                });
                for(var j = 0; j < 20; j++) {
                    this.count++;
                    var $td = $('<td>').html('&nbsp;').appendTo($tr);
                }
                $tr.appendTo($table);
                var $space = $('<tr><td colspan="20"></td></tr>').css({
                    height: this.spaceHeight + 'mm',
                    'text-align': 'right',
                    padding: 0,
                    'line-height': '0.5'
                }).appendTo($table);
                if(this.count % 100 === 0) {
                    var $spaceTd = $space.find('td');
                    $spaceTd.css({
                        height: this.spaceHeight + 'mm',
                        position: 'relative'
                    });
                    $("<span>").text(this.count + '字').css({
                        'text-align': 'right',
                        'font-size': '12px',
                        'line-height': '0.5',
                        display: 'inline-block',
                        'transform-origin': '0',
                        '-webkit-transform': 'scale(0.8)',
                        '-o-transform': 'scale(0.8)',
                        '-moz-transform': 'scale(0.8)',
                        '-ms-transform': 'scale(0.8)',
                        'transform': 'scale(0.8)',
                        position: 'absolute',
                        right: '2px',
                        top: '1px'
                    }).appendTo($spaceTd);
                }
            }
            $table.appendTo($compositionArea);
            this.$dom = $compositionArea;
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$dom.off('click').on('click', function(e){
                Event.trigger('PAGE_ELE_DEACTIVE');
                self.active();
            });
        }
    });

    sheetElement.Composition = function(score, num){
        this.score = score;
        this.num = num;
        this.comps = [];
        this.rows = Math.ceil(this.num / 20);
        this.rowHeight = 7;
        this.spaceHeight = 2;
        this.bigSpaceHeight = 0.4;
        this.type = 'COMPOSITION';
        this.scoreBarHeight = sheetUtil.isMarkOnLine ? 0 : 4.5;
        this.height = this.minHeight = this.rows * (this.rowHeight + this.spaceHeight) + this.scoreBarHeight + 2;
    };
    sheetElement.Composition.prototype = {
        init: function(leftHeight, boxHeight){
            var self = this;
            boxHeight = boxHeight - 2;
            this.comps = [];
            if(leftHeight > 15) {
                if(this.minHeight > leftHeight) {
                    var num = Math.floor((leftHeight - this.scoreBarHeight - 2) / (this.rowHeight + this.spaceHeight)) * 20;
                    this.comps.push(new sheetElement.CompositionWithScore(this.score, num, leftHeight, this.rowHeight, this.spaceHeight, 0));
                    var otherNum = this.num - num;
                    this.fillOther(otherNum, boxHeight);
                }else {
                    this.comps.push(new sheetElement.CompositionWithScore(this.score, this.num, this.minHeight, this.rowHeight, this.spaceHeight, 0));
                }
            }else {
                if(this.minHeight > boxHeight) {
                    var num = Math.floor((boxHeight - this.scoreBarHeight - 2) / (this.rowHeight + this.spaceHeight)) * 20;
                    this.comps.push(new sheetElement.CompositionWithScore(this.score, num, boxHeight, this.rowHeight, this.spaceHeight, 0));
                    var otherNum = this.num - num;
                    this.fillOther(otherNum, boxHeight);
                }else {
                    this.comps.push(new sheetElement.CompositionWithScore(this.score, this.num, this.minHeight, this.rowHeight, this.spaceHeight, 0));
                }
            }
            _.each(this.comps, function(comp){
                comp.orderInPage = self.orderInPage;
            });
            return this;
        },
        fillOther: function(num, boxHeight){
            var minHeight = num / 20 *(this.rowHeight + this.spaceHeight);
            if(minHeight > boxHeight) {
                var numLeft = Math.floor((boxHeight) / (this.rowHeight + this.spaceHeight)) * 20;
                this.comps.push(new sheetElement.CompositionHalf(numLeft, boxHeight, this.rowHeight, this.spaceHeight, this.num - num));
                var otherNum = num - numLeft;
                this.fillOther(otherNum, boxHeight);
            }else {
                this.comps.push(new sheetElement.CompositionHalf(num, boxHeight, this.rowHeight, this.spaceHeight, this.num - num));
            }
        },
        render: function(node){
            this.init();
            _.each(this.comps, function(comp){
                comp.render(node);
            });
        },
        active: function(){
            _.each(this.comps, function(c){
                c.active();
            });
        },
        deactive: function(){
            _.each(this.comps, function(c){
                c.deactive();
            });
        },
        setScore: function(score){
            this.score = score || this.score;
        },
        setNum: function(num){
            this.num = num || this.num;
            this.rows = Math.ceil(this.num / 20);
            this.height = this.minHeight = this.rows * (this.rowHeight + this.spaceHeight) + this.scoreBarHeight + 2;
        }
    };

})();