/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    sheetElement.Page = function(components, config, $ele, isInit, startOrder, startPage, totalPage, schoolName, opt){
        var defaults = {
            secretWidth: 23,
            leftMargin: 6,
            rightMargin: 14,
            topMargin: 16,
            pageSize: '8K',
            contentWidth: sheetUtil.PAGE_SIZE_A3,
            edgeHeight: 1.5,
            pagePadding: 4
        };
        this.components = _.map(components || [], function(item, index){
            item.orderInPage = item.orderInPage || index;
            return item;
        });
        this.componentCount = this.components.length;
        this.config = _.extend(defaults, config);
        this.isInit = !!isInit;
        this.boxHeight = sheetUtil.PAGE_SIZE[this.config.pageSize].height - (this.config.topMargin) * 2;
        this.contentHeight = 0;
        this.$ele = $ele;
        this.startOrder = startOrder || 1;
        this.startPage = startPage || 1;
        this.totalPage = totalPage || 1;
        this.schoolName = schoolName || '点击编辑';
        this.option = opt;
    };
    sheetElement.Page.prototype = {
        init: function(){
            this.$dom = $('<div id="page-box" class="page-box">').addClass('page-' + this.config.pageSize).css({
                position: 'relative',
                width: sheetUtil.PAGE_SIZE[this.config.pageSize].width + 'mm',
                height: sheetUtil.PAGE_SIZE[this.config.pageSize].height + 'mm',
                background: '#fff'
            });
            $('.buildAnswerCard ').addClass('page-' + this.config.pageSize);
            this.boxHeight = sheetUtil.PAGE_SIZE[this.config.pageSize].height - (this.config.topMargin) * 2;
            return this;
        },
        buildSecretBar: function(){
            var secretBox = new sheetElement.SecretBox(this.config, this.option.secretMode);
            secretBox.render(this.$dom);
        },
        build: function(isRebuild){
            this.contentHeight = 0;
            var isDoublePage = this.config.pageSize === 'A3' || this.config.pageSize === '8K';
            var self = this;
            if(isRebuild) {
                this.startOrder = 1;
                this.startPage = 1;
                this.boxHeight = sheetUtil.PAGE_SIZE[this.config.pageSize].height - (this.config.topMargin) * 2;
                this.totalPage = this.calcTotalPages();
                var countPageBreak = isDoublePage ? this.totalPage / 2 : this.totalPage;
                this.$ele.css({
                    width: sheetUtil.PAGE_SIZE[this.config.pageSize].width + 'mm',
                    height: countPageBreak * sheetUtil.PAGE_SIZE[this.config.pageSize].height + 'mm'
                });
                this.$ele.empty();
                this.init().$dom.appendTo(this.$ele);
                $.each(this.components, function(index, c) {
                    if (c.type === 'OPTION_GROUP') {
                        // c.startOrder = self.startOrder;
                        self.startOrder += c.num;
                    } else if (c.type === 'FILL_BLANK') {
                        // c.orders = _.range(self.startOrder, self.startOrder + c.orders.length);
                        self.startOrder += c.orders.length;
                    } else if (c.type === 'SOLVE') {
                        // c.order = self.startOrder;
                        self.startOrder += 1;
                    }
                });
            }else {
                this.init().$dom.appendTo(this.$ele);
            }
            if(!this.isInit || (isDoublePage && (this.startPage - 1) % 4 === 0) || (!isDoublePage && (this.startPage - 1) % 2 === 0)) {
                this.buildSecretBar();
            }
            console.log(this);
            var $leftPage = $('<div class="page-left"></div>').css({
                    padding: this.pagePadding + 'mm 0'
                }),
                $rightPage = $('<div class="page-right"></div>').css({
                    padding: this.pagePadding + 'mm 0'
                });
            var $fullPage = $('<div class="page-full">').css({
                padding: this.pagePadding + 'mm 0'
            });

            if(isDoublePage) {
                $leftPage.appendTo(this.$dom).css({
                    'left': ((!!this.isInit && (this.startPage - 1) % 4 !== 0) ? this.config.rightMargin : (this.config.secretWidth + this.config.leftMargin)) + 'mm',
                    width: this.config.contentWidth + 'mm',
                    height: this.boxHeight + 'mm',
                    background: '#fff',
                    position: 'absolute',
                    // top: (this.isInit ? (this.config.topMargin + 4) : (this.config.topMargin)) + 'mm',
                    top: this.config.topMargin + 'mm',
                    padding: this.config.pagePadding + 'mm 0'
                });
                $rightPage.appendTo(this.$dom).css({
                    right: ((!this.isInit || (this.startPage - 1) % 4 === 0) ? this.config.rightMargin : (this.config.secretWidth + this.config.leftMargin)) + 'mm',
                    width: this.config.contentWidth + 'mm',
                    height: this.boxHeight + 'mm',
                    background: '#fff',
                    position: 'absolute',
                    // top: (this.isInit ? (this.config.topMargin + 4) : (this.config.topMargin)) + 'mm',
                    top: this.config.topMargin + 'mm',
                    padding: this.config.pagePadding + 'mm 0'
                });
                $('<div class="edge edge-top">').css({
                    width: this.config.contentWidth + 'mm',
                    'border-bottom': this.config.edgeHeight + 'mm solid #000',
                    position: 'absolute',
                    top: 0
                }).appendTo($leftPage);
                $('<div class="edge edge-bottom">').css({
                    width: this.config.contentWidth + 'mm',
                    'border-bottom': this.config.edgeHeight + 'mm solid #000',
                    position: 'absolute',
                    bottom: 0
                }).appendTo($leftPage);
                $('<div class="edge edge-top">').css({
                    width: this.config.contentWidth + 'mm',
                    'border-bottom': this.config.edgeHeight + 'mm solid #000',
                    position: 'absolute',
                    top: 0
                }).appendTo($rightPage);
                $('<div class="edge edge-bottom">').css({
                    width: this.config.contentWidth + 'mm',
                    'border-bottom': this.config.edgeHeight + 'mm solid #000',
                    position: 'absolute',
                    bottom: 0
                }).appendTo($rightPage);
                if(this.option.isPagination) {
                    this.drawPagination($leftPage);
                    this.drawPagination($rightPage);
                }else {
                    this.startPage++;
                    this.startPage++
                }
                if(this.option.isSchool) {
                    this.drawSchoolName($leftPage);
                    this.drawSchoolName($rightPage);
                }
            }else {
                $fullPage.appendTo(this.$dom).css({
                    'margin-left': ((this.isInit && (this.startPage - 1) % 2 !== 0) ? this.config.rightMargin : (this.config.secretWidth + this.config.leftMargin)) + 'mm',
                    width: this.config.contentWidth + 'mm',
                    height: this.boxHeight + 'mm',
                    background: '#fff',
                    position: 'relative',
                    // top: (this.isInit ? (this.config.topMargin + 2) : (this.config.topMargin) ) + 'mm',
                    top: this.config.topMargin + 'mm',
                    padding: this.config.pagePadding + 'mm 0'
                });
                $('<div class="edge edge-top">').css({
                    width: this.config.contentWidth + 'mm',
                    'border-bottom': this.config.edgeHeight + 'mm solid #000',
                    position: 'absolute',
                    top: 0
                }).appendTo($fullPage);
                $('<div class="edge edge-bottom">').css({
                    width: this.config.contentWidth + 'mm',
                    'border-bottom': this.config.edgeHeight + 'mm solid #000',
                    position: 'absolute',
                    bottom: 0
                }).appendTo($fullPage);
                if(this.option.isPagination) {
                    this.drawPagination($fullPage);
                }else {
                    this.startPage++
                }
                if(this.option.isSchool) {
                    this.drawSchoolName($fullPage);
                }
            }
            var currentPage = 'LEFT';
            $.each(this.components, function(index, c){
                if(isDoublePage) {
                    //todo  渲染到2个页面上
                    if(self.getPageLeftHeight() >= c.height) {
                        if(c.type === 'COMPOSITION') {
                            var compositionObj = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            _.each(compositionObj.comps, function(comp){
                                comp.orderInPage = c.orderInPage;
                                self.contentHeight += c.height;
                                comp.render(currentPage === 'LEFT' ? $leftPage : $rightPage);
                            });
                        }else if(c.type === 'SOLVE') {
                            var comps = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            if(_.isArray(comps)) {
                                comps[0].orderInPage = c.orderInPage;
                                comps[0].render(currentPage === 'LEFT' ? $leftPage : $rightPage);
                            }else {
                                comps.orderInPage = c.orderInPage;
                                comps.render(currentPage === 'LEFT' ? $leftPage : $rightPage);
                            }
                            self.contentHeight += c.height;
                        }else {
                            self.contentHeight += c.height;
                            c.render(currentPage === 'LEFT' ? $leftPage : $rightPage);
                        }
                    }else {
                        if(c.type === 'COMPOSITION') {
                            var compObj = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            if(self.getPageLeftHeight() > 15) {
                                _.each(compObj.comps, function(item){
                                    item.orderInPage = c.orderInPage;
                                });
                                compObj.comps[0].render(currentPage === 'LEFT' ? $leftPage : $rightPage);
                                self.contentHeight += compObj.comps[0].height;
                                if(currentPage === 'LEFT') {
                                    currentPage = 'RIGHT';
                                    self.contentHeight = 0;
                                    for(var i = 1; i < compObj.comps.length; i++) {
                                        if(self.getPageLeftHeight() >= compObj.comps[i].height) {
                                            compObj.comps[i].render($rightPage);
                                            self.contentHeight += compObj.comps[i].height;
                                        }else {
                                            var leftComps = self.components.slice();
                                            leftComps.splice(index, 1, compObj.comps.slice(i));
                                            leftComps = _.flatten(leftComps);
                                            var newPage = new sheetElement.Page(leftComps.slice(index), self.config, self.$ele, true, self.startOrder,
                                                self.startPage, self.totalPage, self.schoolName, self.option);
                                            newPage.build();
                                            return false;
                                            break;
                                        }
                                    }
                                }else {
                                    var leftComps = self.components.slice();
                                    leftComps.splice(index, 1, compObj.comps.slice(1));
                                    leftComps = _.flatten(leftComps);
                                    var newPage = new sheetElement.Page(leftComps.slice(index), self.config, self.$ele, true, self.startOrder,
                                        self.startPage, self.totalPage, self.schoolName, self.option);
                                    newPage.build();
                                    return false;
                                }
                            }else {
                                _.each(compObj.comps, function(item){
                                    item.orderInPage = c.orderInPage;
                                });
                                if(currentPage === 'LEFT') {
                                    currentPage = 'RIGHT';
                                    self.contentHeight = 0;
                                    compObj.comps[0].render($rightPage);
                                    self.contentHeight += compObj.comps[0].height;
                                    for(var i = compObj.comps.length > 1 ? 1 : 0; i < compObj.comps.length; i++) {
                                        if(self.getPageLeftHeight() >= compObj.comps[i].height) {
                                            compObj.comps[i].render($rightPage);
                                            self.contentHeight += compObj.comps[i].height;
                                        }else {
                                            var leftComps = self.components.slice();
                                            leftComps.splice(index, 1, compObj.comps.slice(i));
                                            leftComps = _.flatten(leftComps);
                                            var newPage = new sheetElement.Page(leftComps.slice(index + 1), self.config, self.$ele, true, self.startOrder,
                                                self.startPage, self.totalPage, self.schoolName, self.option);
                                            newPage.build();
                                            return false;
                                            break;
                                        }
                                    }
                                }else {
                                    var leftComps = self.components.slice();
                                    leftComps.splice(index, 1, compObj.comps.slice(0));
                                    leftComps = _.flatten(leftComps);
                                    var newPage = new sheetElement.Page(leftComps.slice(index), self.config, self.$ele, true, self.startOrder,
                                        self.startPage, self.totalPage, self.schoolName, self.option);
                                    newPage.build();
                                    return false;
                                }
                            }
                        }else if(c.type === 'SOLVE') {
                            var comp = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            if(self.getPageLeftHeight() > 15) {
                                _.each(comp, function(item){
                                    item.orderInPage = c.orderInPage;
                                });
                                comp[0].render(currentPage === 'LEFT' ? $leftPage : $rightPage);
                                self.contentHeight += comp[0].height;
                                if(currentPage === 'LEFT') {
                                    //如果是跨页，继续填充右边页面，否则跳转下一页
                                    currentPage = 'RIGHT';
                                    self.contentHeight = 0;
									for(var i = 1; i < comp.length; i++) {
										if(self.getPageLeftHeight() >= comp[i].height) {
											comp[i].render($rightPage);
											self.contentHeight += comp[i].height;
										}else {
											var leftComps = self.components.slice();
											leftComps.splice(index, 1, comp.slice(i));
											leftComps = _.flatten(leftComps);
											var newPage = new sheetElement.Page(leftComps.slice(index), self.config, self.$ele, true, self.startOrder,
												self.startPage, self.totalPage, self.schoolName, self.option);
											newPage.build();
											return false;
											break;
										}
									}
                                }else {
                                    var leftComps = self.components.slice();
                                    leftComps.splice(index, 1, comp);
									leftComps = _.flatten(leftComps);
                                    var toRenderComps = leftComps.slice(index + 1);
                                    var page = new sheetElement.Page(toRenderComps, self.config, self.$ele, true, self.startOrder,
                                        self.startPage, self.totalPage, self.schoolName, self.option);
                                    page.build();
                                    return false;
                                }
                            }else {
								// _.each(comp, function(item){
								// 	item.orderInPage = c.orderInPage;
								// });
                                if(currentPage === 'LEFT') {
                                    currentPage = 'RIGHT';
                                    self.contentHeight = 0;
                                    if(_.isArray(comp)) {
                                        comp[0].orderInPage = c.orderInPage;
                                        comp[0].render($rightPage);
										self.contentHeight += comp[0].height;
										for(var i = 1; i < comp.length; i++) {
											if(self.getPageLeftHeight() >= comp[i].height) {
												comp[i].render($rightPage);
												self.contentHeight += comp[i].height;
											}else {
												var leftComps = self.components.slice();
												leftComps.splice(index, 1, comp.slice(i));
												leftComps = _.flatten(leftComps);
												var newPage = new sheetElement.Page(leftComps.slice(index + 1), self.config, self.$ele, true, self.startOrder,
													self.startPage, self.totalPage, self.schoolName, self.option);
												newPage.build();
												return false;
												break;
											}
										}
                                    }else {
                                        comp.orderInPage = c.orderInPage;
                                        comp.render($rightPage);
										self.contentHeight += c.height;
                                    }
                                }else {
                                    var page = new sheetElement.Page(self.components.slice(index), self.config, self.$ele, true, self.startOrder,
                                        self.startPage, self.totalPage, self.schoolName, self.option);
                                    page.build();
                                    return false;
                                }
                            }
                        }else {
                            if(currentPage === 'LEFT') {
                                currentPage = 'RIGHT';
                                self.contentHeight = 0;
                                c.render($rightPage);
                                self.contentHeight += c.height;
                            }else {
                                var page = new sheetElement.Page(self.components.slice(index), self.config, self.$ele, true, self.startOrder,
                                    self.startPage, self.totalPage, self.schoolName, self.option);
                                page.build();
                                return false;
                            }
                        }
                    }
                }else {
                    if(self.getPageLeftHeight() >= c.height) {
                        if(c.type === 'COMPOSITION') {
                            var compObj = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            for(var k = 0; k < compObj.comps.length; k++) {
                                compObj.comps[k].orderInPage = c.orderInPage;
                                compObj.comps[k].render($fullPage);
                                self.contentHeight += c.height;
                            }
                        }else if(c.type === 'SOLVE') {
                            var comps = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            if(_.isArray(comps)) {
                                comps[0].orderInPage = c.orderInPage;
                                comps[0].render($fullPage);
                            }else {
                                comps.orderInPage = c.orderInPage;
                                comps.render($fullPage);
                            }
                            self.contentHeight += c.height;
                        }else {
                            self.contentHeight += c.height;
                            c.render($fullPage);
                        }
                    }else {
                        if(c.type === 'COMPOSITION') {
                            var compObj = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            if(self.getPageLeftHeight() > 15) {
                                _.each(compObj.comps, function(item){
                                    item.orderInPage = c.orderInPage;
                                });
                                compObj.comps[0].render($fullPage);
                                self.contentHeight += compObj.comps[0].height;
                                var leftComps = self.components.slice();
                                leftComps.splice(index, 1, compObj.comps.slice());
                                leftComps = _.flatten(leftComps);
                                var toRenderComps = leftComps.slice(index + 1);
                                var page = new sheetElement.Page(toRenderComps, self.config, self.$ele, true, self.startOrder,
                                    self.startPage, self.totalPage, self.schoolName, self.option);
                            }else {
                                var page = new sheetElement.Page(self.components.slice(index), self.config, self.$ele, true, self.startOrder,
                                    self.startPage, self.totalPage, self.schoolName, self.option);
                            }
                            page.build();
                            return false;
                        }else if(c.type === 'SOLVE') {
                            var comp = c.init(self.getPageLeftHeight(), self.boxHeight - 2 * self.config.pagePadding);
                            if(self.getPageLeftHeight() > 15) {
                                _.each(comp, function(item){
                                    item.orderInPage = c.orderInPage;
                                });
                                comp[0].render($fullPage);
                                self.contentHeight += comp[0].height;
                                var leftComps = self.components.slice();
                                leftComps.splice(index, 1, comp);
								leftComps = _.flatten(leftComps);
                                var toRenderComps = leftComps.slice(index + 1);
                                var page = new sheetElement.Page(toRenderComps, self.config, self.$ele, true, self.startOrder,
                                    self.startPage, self.totalPage, self.schoolName, self.option);
                            }else {
                                var page = new sheetElement.Page(self.components.slice(index), self.config, self.$ele, true, self.startOrder,
                                    self.startPage, self.totalPage, self.schoolName, self.option);
                            }
                            page.build();
                            return false;
                        }else {
                            var page = new sheetElement.Page(self.components.slice(index), self.config, self.$ele, true, self.startOrder,
                                self.startPage, self.totalPage, self.schoolName, self.option);
                            page.build();
                            return false;
                        }
                    }
                }
            });
        },
        pushComponent: function(component){
            // var count = this.getTotalComponents();
            component.orderInPage = this.componentCount;
            this.components.push(component);
            this.componentCount += 1;
        },
        removeComponent: function(orderInPage){
            var index = _.findIndex(this.components, function(c){
                return c.orderInPage === orderInPage;
            });
            this.components.splice(index, 1);
        },
        insertComponent: function(order, component){
            var index = _.findIndex(this.components, function(c){
                return c.orderInPage === order;
            });
            component.orderInPage = this.componentCount;
            this.components.splice(index, 0, component);
            this.componentCount += 1;
        },
        insertAfterComp: function(order, component){
            var index = _.findIndex(this.components, function(c){
                return c.orderInPage === order;
            });
            component.orderInPage = this.componentCount;
            this.components.splice(index + 1, 0, component);
            this.componentCount += 1;
        },
        replaceComp: function(order, comp){
            var index = _.findIndex(this.components, function(c){
                return c.orderInPage === order;
            });
            comp.orderInPage = order;
            this.components[index] = comp;
        },
        pop: function(){
            this.components.pop();
        },
        getTotalComponents: function(){
            return this.components.length;
        },
        getPageLeftHeight: function(){
            return this.boxHeight - this.contentHeight - 2 * this.config.pagePadding;
        },
        setPageSize: function(size){
            this.config.pageSize = size;
            if(size === '8K' || size === '16K') {
                this.config.topMargin = 14;
                this.config.contentWidth = sheetUtil.PAGE_SIZE_8K;
            }else {
                this.config.topMargin = 18;
                this.config.contentWidth = sheetUtil.PAGE_SIZE_A3;
            }
            _.each(this.components, function(item){
                item.setPageSize && item.setPageSize(size);
            });
        },
        findComponentByOrder: function(order){
            return _.find(this.components, function(item){
                return item.orderInPage === order;
            });
        },
        calcTotalPages: function(){
            var pageHeight = this.boxHeight - 2 * this.config.pagePadding;
            var totalCompsHeight = 0;
            _.each(this.components, function(comp){
                totalCompsHeight += comp.height;
            });
            var length = Math.ceil(totalCompsHeight / pageHeight);
            if(this.config.pageSize === 'A3' || this.config.pageSize === '8K') {
                if(length % 2 !== 0) {
                    length += 1;
                }
            }
            return length;
        },
        activeCurrentComp: function(compOrder){
            var currentComp = _.find(this.components, function(c){
                return c.orderInPage === compOrder;
            });
            if(currentComp && currentComp.active && _.isFunction(currentComp.active)) {
                currentComp.active();
            }
        },
        setSchoolName: function(name){
            this.schoolName = name || '点击编辑';
        },
        drawSchoolName: function(node){
            var self = this;
            $('<div class="footer-school-name" contenteditable="true">').css({
                width: 'auto',
                'display': 'inline-block',
                position: 'absolute',
                bottom: '-6mm',
                right: 0,
                'min-width': '20px'
            }).appendTo(node).text(this.schoolName || '点击编辑').off('blur').on('blur', function(){
                self.schoolName = $(this).text();
            });
        },
        drawPagination: function(node){
            $('<div class="page-footer" contenteditable="true">').css({
                width: 'auto',
                'display': 'inline-block',
                position: 'absolute',
                bottom: '-6mm',
                left: '50%',
                'transform': 'translateX(-50%)'
            }).appendTo(node).text('第' + this.startPage++ + '页, 共' + this.totalPage + '页');
        },
        setOption: function(opt){
            this.option = opt;
        }
    };
})();