/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //空行组件
    sheetElement.Blank = sheetElement.SheetComp.extend({
        construct: function(height, disableDel){
            this._super(height || 2, 'BLANK', !!disableDel, true);
        },
        init: function(){
            this.$dom = $('<div class="sheet-ele">').css({
                width: '100%',
                height: this.height + 'mm'
            });
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$dom.off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('PAGE_ELE_DEACTIVE');
                self.active();
            });
        }
    });
})();