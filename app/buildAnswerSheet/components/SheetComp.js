/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function($, _){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //答题卡组件父类
    sheetElement.SheetComp = Class.extend({
        construct: function(height, type, disableDel, disableAdd) {
            this.height = height || 0;
            this.type = type || 'SHEET_COMP';
            this.disableDel = !!disableDel;
            this.disableAdd = !!disableAdd;
        },
        init: function() {
            return this;
        },
        render: function(node) {
            this.init().$dom.appendTo(node);
            this.$dom.css({
                position: 'relative'
            });
            this.listenEvent();
            this.supportHover();
            if(!this.disableDel) {
                this.supportDelete();
            }
            if(!this.disableAdd) {
                this.supportAddComp();
            }
            if(this.reCalcHeight && !this.reCalced) {
                this.reCalced = true;
                this.reCalcHeight();
            }
            return this;
        },
        active: function() {
            this.$dom.addClass('active');
            Event.trigger('UPDATE_CURRENT_COMP', this.orderInPage);
        },
        deactive: function() {
            this.$dom.removeClass('active');
            Event.trigger('UPDATE_CURRENT_COMP', -1);
        },
        listenEvent: function() {

        },
        addHeight: function(h) {
            this.height += h;
			this.$dom.css({
				height: this.height + 'mm'
			});
        },
        supportHover: function(){
            // var domHasBorder = this.$dom.css('border-style') !== 'none';
            // this.$dom.hover(function(){
            //     $(this).find('.operation-btn').show();
            //     if(!domHasBorder) {
            //         $(this).css({
            //             border: '1px solid #ccc'
            //         });
            //     }
            // }, function(){
            //     $(this).find('.operation-btn').hide();
            //     if(!domHasBorder) {
            //         $(this).css({
            //             border: 'none'
            //         });
            //     }
            // });
        },
        supportDelete: function(){
            var self = this;
            // var $del = $('<div class="comp-del operation-btn" title="删除"></div>').css({
            //     width: '20px',
            //     height: '20px',
            //     position: 'absolute',
            //     right: '0',
            //     top: '-20px',
            //     background: 'url(dist/img/btn_mimax_a.png) 127px -29px',
            //     'background-color': '#fff',
            //     color: '#ccc',
            //     display: 'none',
            //     'z-index': '15',
            //     cursor: 'pointer',
            //     border: '1px solid #ccc'
            // }).appendTo(this.$dom);
            var $del = $('<div class="comp-del operation-btn">').text('删除').css({
                width: '50px',
                height: '20px',
                lineHeight: '20px',
                position: 'absolute',
                right: '0',
                top: '-20px',
                background: '#FF7101',
                textAlign: 'center',
                color: '#fff',
                display: 'none',
                'z-index': '15',
                cursor: 'pointer',
                border: '1px solid #ccc'
            }).appendTo(this.$dom);
            $del.off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('DEL_COMP', self.orderInPage);
            });
        },
        supportAddComp: function(){
            var self = this;
            // var $del = $('<div class="comp-add operation-btn" title="添加此组件"></div>').css({
            //     width: '20px',
            //     height: '20px',
            //     position: 'absolute',
            //     right: '20px',
            //     top: '-20px',
            //     background: 'url(dist/img/btn_mimax_a.png) 76px -29px',
            //     'background-color': '#fff',
            //     color: '#ccc',
            //     display: 'none',
            //     'z-index': '15',
            //     cursor: 'pointer',
            //     border: '1px solid #ccc',
            //     'border-right': 'none'
            // }).appendTo(this.$dom);
            var $del = $('<div class="comp-add operation-btn">').text('复制').css({
                width: '50px',
                height: '20px',
                lineHeight: '20px',
                position: 'absolute',
                right: '50px',
                top: '-20px',
                background: '#347AB8',
                textAlign: 'center',
                color: '#fff',
                display: 'none',
                'z-index': '15',
                cursor: 'pointer',
                border: '1px solid #ccc'
            }).appendTo(this.$dom);
            $del.off('click').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                Event.trigger('ADD_COMP', self);
            });
        }
    });
})(jQuery, _);