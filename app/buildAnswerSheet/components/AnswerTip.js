/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
    var sheetElement = window.sheetElement = window.sheetElement || {};
    //答题卡题号及说明
    sheetElement.AnswerTip = sheetElement.SheetComp.extend({
        construct: function(paperNo, height){
            var HEIGHT = 21;
            this._super(HEIGHT, 'ANSWER_TIP', true, true);
            this.paperNo = paperNo || '00000000';
        },
        init: function(){
            this.$dom = $('<div class="answer-tip clear">').css({
                height: this.height + 'mm',
                'overflow': 'hidden'
            });
            // $('<div><span>试卷编号：</span><span class="paper-no" style="padding: 0 15px;border: 1px solid #000;' +
            //     'height:6mm;line-height: 6mm;font-size: 4.9mm;display: inline-block;" contenteditable="true">'
            //     + this.paperNo + '</span></div>').appendTo(this.$dom).css({
            //     display:'inline-block',
            //     float: 'left',
            //     'margin-top': '1px'
            // });
            $('<div class="paper-no">').css({
                display:'inline-block',
                float: 'left',
                'margin-top': '1px',
                position: 'relative',
                left: '-10px',
                // minWidth: '30%',
                // maxWidth: '30%',
                height: this.height + 'mm'
            }).appendTo(this.$dom).barcode(this.paperNo, "int25",{barWidth:2, barHeight:50, fontSize: 18});
            $('<div class="clear"><div style="float:left">考生须知：</div><div style="float:left">' +
                '<div>1、请在规定区域内答题，密封线以及评分区域严禁作答</div>' +
                '<div>2、学号、选择题等方框请务必填涂完整</div>' +
                '<div>3、选择题请使用2B铅笔填涂正确答案</div>' +
                '<div>4、解答题请在黑框内答题，严禁超出黑框区域</div>' +
                '</div></div>').appendTo(this.$dom).css({
                display:'inline-block',
                'font-size': '12px',
                'margin-left': '20px',
                float: 'left'
            });
            return this;
        },
        listenEvent: function(){
            var self = this;
            this.$dom.find('.paper-no').off('dblclick').on('dblclick', function(e){
                Event.trigger('RESET_PAPER_NO', self);
            });
        },
        setPaperNo: function(paperno){
            this.paperNo = paperno;
            this.$dom.find('.paper-no').empty().barcode(this.paperNo, "int25",{barWidth:2, barHeight:50, fontSize: 18});
        }
    });
})();