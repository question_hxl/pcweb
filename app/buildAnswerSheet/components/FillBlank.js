/**
 * Created by 贺小雷 on 2017/1/4.
 */
(function(){
	var sheetElement = window.sheetElement = window.sheetElement || {};
	//填空题组组件
	sheetElement.FillBlankQ = sheetElement.SheetComp.extend({
		construct: function(orders, count, pageSize){
			var HEIGHT = 8;
			this._super(HEIGHT, 'FILL_BLANK');
			this.orders = orders;
			this.count = orders.length > 1 ? 1 : (count || 1);
			this.pageSize = pageSize;
			this.marginBottom = 2;
		},
		init: function(){
			this.$dom = $('<div>').addClass('fill-blank-g sheet-ele').css({
				width: '100%',
				height: this.height + 'mm',
				'padding-left': '3px',
				position: 'relative'
			});
			if(!sheetUtil.isMarkOnLine) {
				var blackBlock = new sheetElement.BlackBlock();
				blackBlock.init().$dom.css({
					position: 'absolute',
					left: (-1 * blackBlock.width * sheetUtil.MM2PX_RATIO + 1) + 'px',
					bottom: this.marginBottom + 'mm',
					display: 'block'
				}).appendTo(this.$dom);
				blackBlock.init().$dom.css({
					position: 'absolute',
					bottom: this.marginBottom + 'mm'
				}).appendTo(this.$dom);
			}
			var length = this.orders.length;
			var self = this;
			var itemBox = $('<div>').addClass('fill-blank-item-box').css({
				width: this.calcItemBoxWidth() + 'mm',
				height: this.height + 'mm',
				display: 'inline-block',
				position: 'absolute',
				bottom: '0',
				right: '0'
			}).appendTo(this.$dom);
			var line = new Line(this.calcItemBoxWidth(), this.height, this.marginBottom, this.orders, this.count, this);
			line.render(itemBox);
			if(!sheetUtil.isMarkOnLine) {
				var scoreBox = new ScoreBoxForFillBlank(this.orders.length, this.count, this.marginBottom);
				scoreBox.render(itemBox);
			}
			// this.supportMerge();
			// this.supportChangeSpace();
			return this;
		},
		listenEvent: function(){
			var self = this;
			this.$dom.off('click').on('click', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('PAGE_ELE_DEACTIVE');
				self.active();
			});
			this.$dom.off('dblclick').on('dblclick', function(e){
				e.preventDefault();
				e.stopPropagation();
				Event.trigger('MODIFY_SOLVE_SCORE', self);
			});
		},
		setPageSize: function(size){
			this.pageSize = size;
		},
		calcItemBoxWidth: function(){
			if(sheetUtil.isMarkOnLine) {
				return (sheetUtil.isPageSizeLarge(this.pageSize) ? sheetUtil.PAGE_SIZE_A3 : sheetUtil.PAGE_SIZE_8K);
			}else {
				var blackBlock = new sheetElement.BlackBlock();
				return (sheetUtil.isPageSizeLarge(this.pageSize) ? sheetUtil.PAGE_SIZE_A3 : sheetUtil.PAGE_SIZE_8K) - (+blackBlock.width + 2);
			}
		},
		supportMerge: function(){
			var self = this;
			var $del = $('<div class="comp-merge operation-btn" title="' + (this.orders.length > 1 ? "分散": "合并") +'"></div>').css({
				width: '20px',
				height: '20px',
				position: 'absolute',
				right: '40px',
				top: '-20px',
				background: 'url(dist/img/btn_mimax_a.png) 127px -29px',
				'background-color': '#fff',
				color: '#ccc',
				display: 'none',
				'z-index': '15',
				cursor: 'pointer',
				border: '1px solid #ccc'
			}).appendTo(this.$dom);
			$del.off('click').on('click', function(){
				Event.trigger('MERGE_COMP', self);
			});
		},
		supportChangeSpace: function(){
			if(this.orders.length === 1) {
				var self = this;
				var $del = $('<div class="comp-merge operation-btn" title="切换一题多空"></div>').css({
					width: '20px',
					height: '20px',
					position: 'absolute',
					right: '60px',
					top: '-20px',
					background: 'url(dist/img/btn_mimax_a.png) 127px -29px',
					'background-color': '#fff',
					color: '#ccc',
					display: 'none',
					'z-index': '15',
					cursor: 'pointer',
					border: '1px solid #ccc'
				}).appendTo(this.$dom);
				$del.off('click').on('click', function(){
					Event.trigger('CHANGE_FB_SPACE', self);
				});
			}
		},
		setOrder: function(order){
			this.orders = order;
		},
		setCount: function(count){
			this.count = this.orders.length > 1 ? 1 : count || 1;
		},
		_updateOrder: function(order){
			this.orders = order;
			Event.trigger('REBUILD_PAGE');
		},
		autoSetOrder: function(order){
			if(!this.orders || this.orders.length === 0) {
				this.orders = order;
			}
		}
	});

	//给分框组件
	var ScoreBoxForFillBlank = function(qstCount, spaceCount, marginBottom){
		this.qstCount = qstCount;
		this.spaceCount = spaceCount;
		this.boxItemWidth = 6.5;
		this.boxItemHeight = 4.65;
		this.marginBottom = marginBottom;
		if(this.qstCount > 1) {
			this.width = 2 * this.boxItemWidth + 2;
		}else {
			this.width = this.spaceCount * this.boxItemWidth;
		}
	};
	ScoreBoxForFillBlank.prototype = {
		init: function(){
			this.$dom = $('<div class="score-fb-box">').css({
				position: 'absolute',
				width: this.width + 'mm',
				height: this.boxItemHeight + 'mm',
				right: '0',
				bottom: this.marginBottom + 'mm'
			});
			var $boxItem = $('<span>').css({
				display: 'block',
				width: this.boxItemWidth + 'mm',
				height: this.boxItemHeight + 'mm',
				border: '1px solid #000',
				position: 'absolute',
				bottom: '-0.5mm'
			});
			if(this.qstCount > 1) {
				$boxItem.clone().css({
					left: '0'
				}).appendTo(this.$dom);
				$boxItem.clone().css({
					right: '0'
				}).appendTo(this.$dom);
			}else {
				for(var i = 0; i < this.spaceCount; i++) {
					$boxItem.clone().css({
						left: i * this.boxItemWidth + 'mm',
						borderRight: i === (this.spaceCount - 1) ? '1px solid #000' : 'none'
					}).appendTo(this.$dom);
				}
			}
			return this;
		},
		render: function(node){
			this.init().$dom.appendTo(node);
			return this;
		}
	};
	//填空题横线组件
	var Line = function(width, height, marginBottom, orders, spaceCount, parent){
		this.orders = orders;
		this.spaceCount = spaceCount;
		var scoreBox = new ScoreBoxForFillBlank(orders.length, spaceCount);
		this.width = width - (sheetUtil.isMarkOnLine ? 0 : scoreBox.width);
		this.height = height;
		this.marginBottom = marginBottom;
		this.spaceWidth = 3;
		this.parent = parent;
	};
	Line.prototype = {
		init: function(){
			var self = this;
			this.$dom = $('<div class="fb-line">').css({
				position: 'absolute',
				width: this.width + 'mm',
				height: this.height + 'px',
				left: 0,
				bottom: this.marginBottom + 'mm'
			});
			var $order = $('<div class="fb-order" contenteditable="true">').css({
				width: '6mm',
				display: 'block',
				'text-align': 'center',
				position: 'absolute',
				'line-height': '1',
				left: 0,
				bottom: '0'
			});
			var $line = $('<div>').css({
				height: this.height + 'mm',
				'border-bottom': '1px solid #000',
				left: '6mm',
				bottom: 0,
				position: 'absolute'
			});
			var $box = $('<div class="fn-line-single">').css({
				width: this.width + 'mm',
				height: '100%',
				position: 'absolute'
			});
			if(this.orders.length === 2) {
				var width = this.width / 2;
				for(var i = 0; i < this.orders.length; i++) {
					var $boxItem = $box.clone();
					$boxItem.css({
						width: width + 'mm',
						left: i * width + 'mm'
					}).appendTo(this.$dom);
					$order.clone().text(this.orders[i]).appendTo($boxItem).off('blur').on('blur', (function(sOrder){
						return function(){
							var text = $(this).text();
							if(text && +text  && _.isNumber(+text) && +text !== self.orders[sOrder]) {
								self.orders[sOrder] = +text;
								var other = sOrder === 1 ? -1 : 1;
								self.orders[sOrder + other] = +text + other;
								self.parent._updateOrder(self.orders);
							}
						};
					})(i));
					$line.clone().css({
						width: (width - 6 - this.spaceWidth) + 'mm'
					}).appendTo($boxItem);
				}
			}else {
				var $boxItem = $box.clone();
				$boxItem.css({
					left: 0
				}).appendTo(this.$dom);
				$order.clone().text(this.orders[0]).appendTo($boxItem).off('blur').on('blur', function(){
					var text = $(this).text();
					if(text && +text  && _.isNumber(+text) && +text !== self.orders[0]) {
						self.orders[0] = +text;
						self.parent._updateOrder(self.orders);
					}
				});
				var lineWidth = (this.width - 6 - this.spaceWidth * this.spaceCount) / this.spaceCount;
				for(var j = 0; j < this.spaceCount; j++) {
					$line.clone().css({
						width: lineWidth + 'mm',
						left: j * (lineWidth + this.spaceWidth) + 6 + 'mm'
					}).appendTo($boxItem);
				}
			}
			return this;
		},
		render: function(node){
			this.init().$dom.appendTo(node);
			return this;
		}
	};
})();