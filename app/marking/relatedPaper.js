/**
 * Created by clc on 2016/11/2.
 */
define(['underscore-min', 'jquerymedia'], function(){
    return ['$http', '$scope', '$rootScope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', 'constantService',
        function($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce, constantService){
            $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
            var searchfrom = $location.search();
            $scope.examid = searchfrom.ExamFlnkID;
            $scope.clasid = searchfrom.ClassFlnkid;
            $scope.turnto = searchfrom.turnto;//0:overMarking(快速批阅);1:markingfast(线上批阅);
            $scope.TaskFId = searchfrom.TaskFId;//跳转至overMarking时需要
            $scope.isAllowMark = searchfrom.allowMark;//跳转至overMarking时需要
            $scope.ExamName = searchfrom.ExamName;//跳转至overMarking时需要
            $scope.isShowAll = searchfrom.isShowAll;
            $scope.UserFLnkID = searchfrom.UserFLnkID;
            var UnifiedItemFid = searchfrom.UnifiedItemFid;//跳转至overMarking时需要
            $scope.userFId = '';
            $scope.exafId = '';
            $scope.currentIndex = 0;
            $scope.currentIndex1 = 0;
            $scope.ClassName = '';
            $scope.NoRelated = [];
            $scope.YesRelated = [];
            $scope.AddRelated = [];
            //班级选择
            $http.post($rootScope.baseUrl + '/Interface0192.ashx', {
                schoolFLnkId : $scope.currentUser.schoolFId
            }).success(function(data){
                if(data && data.msg && _.isArray(data.msg)){
                    $scope.classData = data.msg;
                    $scope.currentClass =_.find($scope.classData ,function(Class){
                        return $scope.clasid  === Class.ClassID;
                    });
                        $scope.ClassName = $scope.currentClass.ClassID;
                }
            });

            //获取学生列表
            $scope.getStudentList = function(){
                $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    classFId : $scope.ClassName,
                    examFid : $scope.examid,
                    Proc_name : 'GetStudentUnbindPaper'
                }).success(function(data){
                    if(data && data.msg && _.isArray(data.msg)){
                        $scope.studentData = data.msg;
                        $scope.userFId = $scope.studentData.userFId;
                        $scope.studentUnbind = [];
                        $scope.studentNo=[];
                        $scope.studentAll = [];
                        _.filter($scope.studentData,function(item){
                            if(item.isExit === 0){
                                $scope.studentNo.push(item);
                                if($scope.studentNo.length > 0){
                                    studentSort($scope.studentNo)
                                }
                            }else{
                                $scope.studentAll.push(item);
                                if($scope.studentAll.length > 0){
                                    studentSort($scope.studentAll)
                                }
                            }
                        });
                        $scope.studentUnbind = $scope.studentNo.concat($scope.studentAll);
                        $scope.currentStu = _.find($scope.studentUnbind,function(user){
                            return $scope.UserFLnkID === user.userFId
                        });
                    }
                });
            };
            //学生按学号排序
            function studentSort(stu) {
                stu.sort(function (a, b) {
                    return +a.userNo - +b.userNo;
                });
            }

            //监听班级联动学生
            $scope.$watch('ClassName', function(oldValue, newValue){
                if(newValue == oldValue){
                    return;
                }
                $scope.getStudentList();
            });

            // 获取失效试卷
            function getUnbindPaper(){
               $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                   taskFid :$scope.TaskFId
               }).then(function(res){
                   if(_.isArray(res.data.msg)){
                       $scope.fileList = res.data.msg;
                       $scope.nweFile = [];
                       $scope.NoRelated = [];
                       $scope.YesRelated = [];
                       $scope.AddRelated = [];
                       _.each($scope.fileList,function (resq) {
                           if(resq.isNoRelated === 1){
                               $scope.NoRelated.push(resq);
                           }else if(resq.isNoRelated === 2){
                               $scope.AddRelated.push(resq);
                           }else if(resq.isNoRelated === 0){
                               $scope.YesRelated.push(resq);
                           }
                       });
                       $scope.nweFile = $scope.YesRelated.concat($scope.AddRelated);
                       $scope.fileList = $scope.nweFile.concat($scope.NoRelated);
                       $scope.currentFile = $scope.fileList && $scope.fileList[0];
                       // if($scope.YesRelated.length === 0){
                       //     if($scope.isShowAll) {
                       //         $scope.autoNextMarking();
                       //     }
                       // }
                   } else {
                       $scope.fileList = [];
                       if($scope.isShowAll) {
                           $scope.autoNextMarking();
                       }
                   }
               });
           }
            getUnbindPaper();
            function sortByKey(array, key) {
                return array.sort(function(a, b) {
                    var x = a[key]; var y = b[key];
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                });
            }
            //显示全部
            function getAllPaper(){
                $http.post('BootStrap/Interface/generalQuery.ashx',{
                    classFId:$scope.clasid,
                    examid:$scope.examid,
                    Proc_name: 'GetMyClassBindPaper'
                }).then(function(res){
                    if(_.isArray(res.data.msg)){
                        $scope.BindPaperList = res.data.msg;
                        if($scope.UserFLnkID) {
                           var currentBindPaperIndex = _.findIndex($scope.BindPaperList, function(item){
                                return item.uFlnkID === $scope.UserFLnkID;
                            });
                            if(currentBindPaperIndex<0){
                                $scope.currentBindPaper = $scope.BindPaperList[0];
                            }else {
                                $scope.clickfile1(currentBindPaperIndex);
                            }
                        }else {
                            $scope.currentBindPaper = $scope.BindPaperList[0];
                        }
                    } else {
                        $scope.BindPaperList = [];
                    }
                });
            }
            getAllPaper();
            if($scope.isShowAll === true){
                getUnbindPaper();
                $scope.isShowAll = !$scope.isShowAll;
            }else {
                getAllPaper();
                $scope.isShowAll = !$scope.isShowAll;
            }

            //点击切换
            //$scope.isShowAll = false;
            $scope.showAll = function(){
                if($scope.isShowAll === true){
                    getAllPaper();
                    $scope.isShowAll = !$scope.isShowAll;
                }else {
                    getUnbindPaper();
                    $scope.isShowAll = !$scope.isShowAll;
                }
                $scope.currentBindPaper = $scope.BindPaperList && $scope.BindPaperList[0];
                $scope.currentFile = $scope.fileList && $scope.fileList[0];
            };

            //点击获取焦点
            $scope.clickfile = function(index){
                $scope.currentIndex = index;
                $scope.currentFile = $scope.fileList[$scope.currentIndex];
                $scope.currentBindPaper = $scope.BindPaperList[$scope.currentIndex];
            };
            $scope.clickfile1 = function(index){
                $scope.currentIndex1 = index;
                $scope.currentBindPaper = $scope.BindPaperList[$scope.currentIndex1];
            };
            //试卷关联
            $scope.relatedPaper = function(){
                if($scope.ClassName != null && $scope.currentUserFId){
                    $http.post('BootStrap/EncryptUnified/UPDATEExAnswerUserFId.ashx', {
                        userFId : $scope.currentUserFId,
                        examFId : $scope.examid,
                        exaFId : $scope.currentFile.exaFId
                    }).then(function(res){
                        if(res.data.code === 0){
                            $scope.getStudentList();
                            $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                                taskFid :$scope.TaskFId
                            }).then(function(res){
                                if(_.isArray(res.data.msg)){
                                    $scope.fileList = res.data.msg;
                                    $scope.nweFile = [];
                                    $scope.NoRelated = [];
                                    $scope.YesRelated = [];
                                    $scope.AddRelated = [];
                                    _.each($scope.fileList,function (resq) {
                                        if(resq.isNoRelated === 1){
                                            $scope.NoRelated.push(resq);
                                        }else if(resq.isNoRelated === 2){
                                            $scope.AddRelated.push(resq);
                                        }else if(resq.isNoRelated === 0){
                                            $scope.YesRelated.push(resq);
                                        }
                                    });
                                    $scope.nweFile  = $scope.YesRelated.concat($scope.AddRelated);
                                    $scope.fileList = $scope.nweFile .concat($scope.NoRelated);
                                    // if($scope.YesRelated.length === 0){
                                    //     $scope.autoNextMarking();
                                    // }
                                    if($scope.fileList.length<=$scope.currentIndex){
                                        $scope.currentFile = $scope.fileList[$scope.currentIndex-1];
                                        $scope.currentIndex = $scope.currentIndex-1;
                                    }else if($scope.fileList.length>$scope.currentIndex){
                                        $scope.currentFile = $scope.fileList[$scope.currentIndex];
                                    }
                                    else{
                                        $scope.currentFile = $scope.fileList[0];
                                    }
                                    constantService.alert('绑定成功！');
                                    $scope.currentUserFId = '';
                                } else {
                                    $scope.fileList = [];
                                    // $scope.autoNextMarking();
                                }
                            }, function(res){
                                constantService.alert('数据刷新失败！');
                            });
                        }else {
                            constantService.alert('绑定失败！');
                        }
                    }, function(res){
                        constantService.alert('绑定失败！');
                    });
                } else {
                    constantService.alert('请选择需要关联的班级和学生！');
                }
            };
            //删除试卷
            // $scope.removePaper = function(){
            //     ngDialog.open({
            //         template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
            //         '<div class="nextMarking-title">请确认是否删除该答题卡，删除后将无法找回</div>'+
            //         '<div class="nextMarking-btn" style="margin:0 40px" ng-click="sure()">确定</div>'+
            //         '<div class="nextMarking-btn" ng-click="closeThisDialog()">取消</div>'
            //         +'</div>'+'</div>',
            //         className: 'ngdialog-theme-default ngdialog-related',
            //         scope: $scope,
            //         closeByDocument: false,
            //         plain: true
            //     });
            //     $scope.sure = function(){
            //         ngDialog.close();
            //         $http.post('BootStrap/Interface/deleteUnBindPaper.ashx', {
            //             exaFId : $scope.currentFile.exaFId
            //         }).then(function(res){
            //             $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
            //                 examFId : $scope.examid
            //             }).then(function(res){
            //                 if(_.isArray(res.data.msg)){
            //                     $scope.fileList = res.data.msg;
            //                     if($scope.fileList.length<=$scope.currentIndex){
            //                         $scope.currentFile = $scope.fileList[$scope.currentIndex-1];
            //                         $scope.currentIndex = $scope.currentIndex-1;
            //                     }else if($scope.fileList.length>$scope.currentIndex){
            //                         $scope.currentFile = $scope.fileList[$scope.currentIndex];
            //                     }
            //                     else{
            //                         $scope.currentFile = $scope.fileList[0];
            //                     }
            //                     constantService.alert('删除成功！');
            //                 } else {
            //                     $scope.fileList = [];
            //                     $scope.autoNextMarking();
            //                 }
            //             }, function(res){
            //                 constantService.alert('数据删除失败！');
            //             }, function(res){
            //                 constantService.alert('删除失败！');
            //             })
            //         })
            //     }
            // };
            //无法关联
            $scope.unRelated = function () {
                ngDialog.open({
                    template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
                    '<div class="nextMarking-title">请确认该无主卷无法被关联</div>'+
                    '<div class="nextMarking-btn" style="margin:0 40px" ng-click="yes()">确定</div>'+
                    '<div class="nextMarking-btn" ng-click="closeThisDialog()">取消</div>'
                    +'</div>'+'</div>',
                    className: 'ngdialog-theme-default ngdialog-related',
                    scope: $scope,
                    closeByDocument: false,
                    plain: true
                });
                $scope.yes = function () {
                    ngDialog.close();
                    $scope.nweFile = [];
                    $scope.NoRelated = [];
                    $scope.YesRelated = [];
                    $scope.AddRelated = [];
                    $http.post('BootStrap/EncryptUnified/RemarkAnswerNobody.ashx',{
                        answerFid:$scope.currentFile.exaFId
                    }).then(function (resq) {
                        if(resq.data.code === 0){
                            $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                                taskFid :$scope.TaskFId
                            }).then(function(res){
                                if(_.isArray(res.data.msg)){
                                    $scope.fileList = res.data.msg;
                                    _.each($scope.fileList,function (res) {
                                        if(res.exaFId === $scope.currentFile.exaFId){
                                            res.isNoRelated = 1
                                        }
                                        if(res.isNoRelated === 1){
                                            $scope.NoRelated.push(res);
                                        }else if(res.isNoRelated === 2){
                                            $scope.AddRelated.push(res);
                                        }else if(res.isNoRelated === 0){
                                            $scope.YesRelated.push(res);
                                        }
                                    });
                                    $scope.nweFile  = $scope.YesRelated.concat($scope.AddRelated);
                                    $scope.fileList = $scope.nweFile .concat($scope.NoRelated);
                                    // if($scope.YesRelated.length === 0){
                                    //     $scope.autoNextMarking();
                                    // }
                                    if($scope.fileList.length<=$scope.currentIndex){
                                        $scope.currentFile = $scope.fileList[$scope.currentIndex-1];
                                        $scope.currentIndex = $scope.currentIndex-1;
                                    }else if($scope.fileList.length>$scope.currentIndex){
                                        $scope.currentFile = $scope.fileList[$scope.currentIndex];
                                    }
                                    else{
                                        $scope.currentFile = $scope.fileList[0];
                                    }
                                    constantService.alert('修改成功！');
                                } else {
                                    $scope.fileList = [];
                                    //$scope.autoNextMarking();
                                }
                            })
                        }else{
                            constantService.alert('修改失败！');
                        }
                    },function () {
                        constantService.alert('修改失败！');
                    });
                }
            };
            //全部忽略
            $scope.unRelatedAll = function () {
                ngDialog.open({
                    template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
                    '<div class="nextMarking-title">确定是否取消所有未关联的试卷</div>'+
                    '<div class="nextMarking-btn" style="margin:0 40px" ng-click="yes()">确定</div>'+
                    '<div class="nextMarking-btn" ng-click="closeThisDialog()">取消</div>'
                    +'</div>'+'</div>',
                    className: 'ngdialog-theme-default ngdialog-related',
                    scope: $scope,
                    closeByDocument: false,
                    plain: true
                });
                function setRemarkAnswerNobody(params) {
                    var defer = $q.defer();
                    $http.post('BootStrap/EncryptUnified/RemarkAnswerNobody.ashx',{
                        answerFid:params
                    }).then(function (res) {
                        defer.resolve(res);
                    },function (res) {
                        defer.reject();
                    });
                    return defer.promise;
                }
                $scope.yes = function () {
                    ngDialog.close();
                    $scope.NoRelated = [];
                    $scope.YesRelated = [];
                    $scope.AddRelated = [];
                    var newFidList = [];
                    _.each($scope.fileList,function (item) {
                        newFidList.push(setRemarkAnswerNobody(item.exaFId));
                    });
                    if(newFidList.length>0){
                        $q.all(newFidList).then(function (resq) {
                            $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                                taskFid :$scope.TaskFId
                            }).then(function(res){
                                if(_.isArray(res.data.msg)){
                                    $scope.fileList = res.data.msg;
                                    _.each($scope.fileList,function (res) {
                                        res.isNoRelated = 1;
                                    });
                                    $scope.NoRelated = $scope.fileList;
                                    //$scope.autoNextMarking();
                                    constantService.alert('修改成功！');
                                } else {
                                    $scope.fileList = [];
                                    //$scope.autoNextMarking();
                                }
                            },function () {
                                constantService.alert('修改失败！');
                            })
                        });
                    }
                }
            };
            //解除绑定
            $scope.unbindPaper = function(){
                $http.post('BootStrap/Interface/unbindAnswer.ashx',{
                    exanFid:$scope.currentBindPaper.exaFId
                    }).then(function(res){
                        if(res.data.code === 0){
                            $scope.getStudentList();
                                 $http.post('BootStrap/Interface/generalQuery.ashx',{
                                        classFId : $scope.clasid,
                                        examid : $scope.examid,
                                        Proc_name : 'GetMyClassBindPaper'
                                    }).then(function(res){
                                     if(_.isArray(res.data.msg)){
                                         $scope.BindPaperList = res.data.msg;
                                         if($scope.BindPaperList.length<=$scope.currentIndex1){
                                             $scope.currentBindPaper = $scope.BindPaperList[$scope.currentIndex1-1];
                                             $scope.currentIndex1 = $scope.currentIndex1-1;
                                         }else if($scope.BindPaperList.length>$scope.currentIndex1){
                                             $scope.currentBindPaper = $scope.BindPaperList[$scope.currentIndex1];
                                         }
                                         else {
                                             $scope.currentBindPaper = $scope.BindPaperList[0];
                                         }
                                         constantService.alert('解绑成功！');
                                         $scope.currentUserFId = '';
                                         $scope.UserFLnkID = '';
                                     } else {
                                         $scope.BindPaperList = [];
                                     }
                                    },function(res){
                                        constantService.alert('数据解绑失败！');
                                    })
                                }else{
                                    constantService.alert('解绑失败！');
                        }
                })
            };

            //自动下一步
            $scope.autoNextMarking = function(){
                    ngDialog.open({
                        template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
                            '<div class="nextMarking-title">暂无待关联的试卷，请直接点击开始批阅</div>'+
                        '<div class="nextMarking-btn" style="margin:0 40px" ng-click="jump2Mark()">开始批阅</div>'+
                        '<div class="nextMarking-btn" ng-click="closeThisDialog()">取消</div>'
                        +'</div>'+'</div>',
                        className: 'ngdialog-theme-default ngdialog-related',
                        scope: $scope,
                        closeByDocument: false,
                        plain: true
                    })
            };

            //下一步
            $scope.nextMarking = function(){
                if($scope.YesRelated.length > 0){
                    ngDialog.open({
                        template : 'nextMarking',
                        className : 'ngdialog-theme-default ngdialog-related',
                        scope : $scope
                    });
                }else {
                    $scope.jump2Mark();
                }
            };
            $scope.taskFId = searchfrom.TaskFId;

            $scope.jump2Mark = function(){
                ngDialog.close();
                if($scope.turnto == '1'){
                    var wyWindow = window.open('_blank');
                }
                $http.post('BootStrap/Interface/finishMarkCorrecting.ashx', {
                    taskFLnkId : $scope.TaskFId
                }).then(function(resp){
                    if(resp.data.code == '0'){
                        if($scope.turnto == '0'){
                            $state.go('overallMarking',{
                                ExamFlnkID:$scope.examid,
                                ClassFlnkID:$scope.clasid,
                                TaskFId:$scope.TaskFId,
                                allowMark:$scope.isAllowMark,
                                ExamName:$scope.ExamName,
                                UnifiedItemFid:UnifiedItemFid
                            });
                        }
                        if($scope.turnto == '1'){
                            wyWindow.location = '#/fastmarking?ExamFlnkID=' + $scope.examid + '&ClassFlnkID=' + $scope.clasid+'&allowMark='+$scope.isAllowMark+ '&isOpenNewWin=' + 1;
                        }
                    } else {
                        constantService.alert(resp.data.msg);
                    }
                }, function(resp){
                    constantService.alert('更新任务状态失败！');
                });
            };
        }]
});