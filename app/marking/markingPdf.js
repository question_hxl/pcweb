define(['underscore-min', 'jquerymedia'], function() {
	return ['$http', '$scope', '$rootScope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', function($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce) {

		var searchfrom = $location.search();

		$scope.examid = searchfrom.ExamFlnkID;
		$scope.clasid = searchfrom.ClassFlnkid;
		$scope.taskFId = searchfrom.TaskFId;
		$scope.currentIndex = 0;

		$scope.$watch('currentIndex', function() {
			if($scope.currentStudent) {
				getpdfList();
			}
		});
		var width = $(window).width()* 0.87 + 'px';
		$('.markingpdf .con .marking .left').css('min-width',width);
		$('.markingpdf .con .marking .left .media').css('min-width',width);
		

		$http.post($rootScope.baseUrl + '/Interface0045A.ashx', {
			classId: searchfrom.ClassFlnkid,
			examFLnkId: searchfrom.ExamFlnkID
		}).then(function(resp) {
			$scope.studentList = resp.data.studentList;
			$scope.studentList.sort(function(a, b){
				return +a.type - +b.type;
			});

			for (var i = 0; i < $scope.studentList.length; i++) {
				if ($scope.studentList[0].type == -1) {
					$scope.currentIndex = i;
					break;
				}
			}

			$scope.currentStudent = $scope.studentList[$scope.currentIndex];

			getpdfList();
		});
		$scope.nodata = true;

		$scope.clickstudent = function(index) {
			if (typeof $scope.studentList[index].type == 'string' && $scope.studentList[index].type != 2) {
				$scope.currentIndex = index;
				$scope.currentStudent = $scope.studentList[$scope.currentIndex];
			}

		};

		function getpdfList() {
			$http.post($rootScope.baseUrl + '/Interface0187.ashx', {
				ExamFLnkID: searchfrom.ExamFlnkID, //'DB121CEA-26EE-4DF0-8E2F-BD75F82BCB31'
				UserFLnkID: $scope.currentStudent.studentId
			}).success(function(data) {
				if (data.Accessory == null) {
					$scope.nodata = true;
					return false;
				} else {
					$scope.ExName = data.ExName;
					$scope.nodata = false;
					$scope.Accessory = data.Accessory;

					viewPdf($scope.Accessory)
					var pdfpaper = data.msg;
					var pdfpaperList = [];
					for (var i = 0; i < pdfpaper.length; i++) {
						pdfpaperList.push({
							'scores': pdfpaper[i].scores,
							'paperallscores': pdfpaper[i].allscores,
                            'isRight': pdfpaper[i].isRight
						})
					}
					$scope.pdfpaperList = pdfpaperList;

					var totalScore = 0;
					var paperallscores = 0;
					_.each($scope.pdfpaperList, function(item) {
						totalScore += +item.isRight == -1 ? 0 : +item.scores;
						paperallscores += +item.paperallscores
					});
					$scope.totalScore = totalScore;
					$scope.paperallscores = paperallscores

					var questionBox = _.groupBy(data.msg, 'qType');
					var question = _.pairs(questionBox);
					question = _.map(question, function(item, index) {
						return {
							typeName: item[0],
							question: _.values(item[1])
						}
					});

					question = _.map(question, function(group, i) {
						var groupScore = 0;
						var questions = _.map(group.question, function(item, index) {
							if (item.qType == '填空填' || item.qType == '填空题' || item.qType == '解答题') {
								isType = true;
								isTypeChooes = false;
								typeClassName = "总分" //答案
							} else {
								isType = false;
								isTypeChooes = true;
								typeClassName = "总分"
							}
							groupScore += +item.isRight == -1 ? 0 : +item.scores;
							var errorType = getIsMarkError(item);
							return {
								'allscores': item.allscores,
								'answer': $sce.trustAsHtml(item.answer),
								'order': item.order,
								'qFlnkId': item.qFlnkId,
								'scores': +item.isRight == -1 ? '' : item.scores,
								'isType': isType,
								'isTypeChooes': isTypeChooes,
								'errorType': errorType
							}
						})
						return {
							typeName: group.typeName,
							question: questions,
							typeClassName: typeClassName,
							groupScore: groupScore
						}
					})
					$scope.questionBox = question;
					return false;
				}

			});
		}

		function getIsMarkError(qst){
			var type = 0;
			if(+qst.scores > +qst.allscores || +qst.scores < 0 || qst.scores === '' || +qst.isRight < 0) {
				//默认无分数，为错误
				type = 1;
			}else if(!isScoreIllegal(qst)) {
				type = 2;
			}else if(qst.isObjective === '1' && (+qst.scores !== 0 && +qst.scores !== +qst.allscores && qst.qType === '选择题')) {
				type = 1;
			}
			return type;
		}

		function isScoreIllegal (qst){
			if(+qst.scores * 10 % 5 !== 0 || +qst.scores * 10 % 1 !== 0) {
				return false;
			}else {
				return true;
			}
		}

		function viewPdf(url) {
			$('#paperPdf').attr('href', url).media({
				'width': width,
				'height': 770,
				'margin-top': '0'
			});
		}

		//点击选中
		$scope.inputclick = function ($event) {
			var tag = $event.target;
			$('#' + tag.id + '').select();
		};

		//提交批阅
		$scope.postscore = function() {

			postscore();
		};

		// 监听文本框输入值
		$scope.keyUp = function(dates, $event, question) {
			var tag = $event.target;
			//if ($(tag).val() > dates) {
			//	$(tag).val(dates);
			//	return false;
			//}
			$(tag).css({
				'color': 'black'
			})
			if ($(tag).val() != '') {
				if (+$(tag).val() > +dates) {
					ngDialog.open({
						template: '<p>分值不能大于本题最高分</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					$(tag).val('' + dates + '');
					question.scores = dates;
				}
			} else {
				ngDialog.open({
					template: '<p>分数框不能为空</p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					plain: true,
					className: 'ngdialog-theme-plain'
				});

				$(tag).val('' + dates + '');
				question.scores = dates;
				return false;
			}
			var totalScore = 0;

			var nowscores = _.map($scope.questionBox, function(group, i) {
				var groupScore = 0;
				_.each(group.question, function(item) {
					groupScore += +item.scores;
					totalScore += +item.scores;
				});
				group.groupScore = groupScore;
			})
			$scope.totalScore = totalScore;
		}

		function postscore() {
			var questionListArry = [];
			var errOrders = [];
			var newPage = _.map($scope.questionBox, function(group, i) {
				var questionsL = _.map(group.question, function(item, index) {
					if(+item.scores > +item.allscores || +item.scores < 0){
						errOrders.push(item.order);
					}
					return {
						'qFlnkId': item.qFlnkId,
						'scores': item.scores
					}
				});
				return {
					quesitonBox: questionsL
				}
			});
			if(errOrders.length){
				var orders = errOrders.join(',');
				ngDialog.openConfirm({
					template: '<p>'+'第'+orders+'题的得分超过总分，请核对!'+'</p>' +
					'<div class="ngdialog-buttons">' +
					'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button>',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					plain: true
				});
				return;
			}
			var greatObj = function() {
				angular.forEach(newPage, function(item) {
					angular.forEach(item.quesitonBox, function(item) {
						questionListArry.push(item)
					})
				});
				return questionListArry;
			};

			var msgDate = greatObj();

			$http({
				method: 'POST',
				url: $rootScope.baseUrl + '/Interface0188.ashx',
				data: {
					examFlnkId: searchfrom.ExamFlnkID,
					userFlnkId: $scope.currentStudent.studentId,
					msg: msgDate
				}
			}).success(function(data) {
				$scope.currentStudent.type = '0';
				var index = _.findIndex($scope.errPersonFid, function(o){
					return $scope.currentStudent.studentId === o.userfid;
				});
				if(index >= 0) {
					$scope.errPersonFid.splice(index, 1);
				}
				//移除错误题目
				_.each($scope.questionBox, function(g){
					_.each(g.question, function(q){
						q.errorType = 0;
					});
				});
				ngDialog.openConfirm({
					template: '<p> ' + data.msg + ' </p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button>',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					plain: true
				});
				// $http.post($rootScope.baseUrl + '/Interface0207.ashx', {
				// 	classFlnkID: searchfrom.ClassFlnkid,
				// 	examFlnkID: searchfrom.ExamFlnkID
				// }).success(function(data) {
				// 	console.log(data.code + '----' + data.msg);
					//if (data.code == 0) {
					//	ngDialog.open({
					//		template:
					//				'<p>' + data.msg + '</p>' +
					//				'<div class="ngdialog-buttons">' +
					//				'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					//		plain: true,
					//		scope: $scope,
					//		className: 'ngdialog-theme-plain'
					//	});
					//	return false;
					//} else {
					//	ngDialog.open({
					//		template:
					//				'<p>' + data.msg + '</p>' +
					//				'<div class="ngdialog-buttons">' +
					//				'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					//		plain: true,
					//		className: 'ngdialog-theme-plain',
					//		closeByEscape: false,
					//		closeByDocument: false,
					//		preCloseCallback: function () {
					//			$scope.canMarking = true;
					//		}
					//	});
					//	return false;
					//}
				// })
			})
		}
		/*后纠错*/
		$http.post('/BootStrap/EncryptUnified/AiTaskAssessment.ashx', {//数据有问题的学生
			taskFid:searchfrom.TaskFId
		}).success(function(data) {
			$scope.errPersonFid = data.msg;
			$scope.isErrStudentRelated = function (stu) {
				return _.findIndex($scope.errPersonFid,function (item) {
						return (item.userfid === stu.studentId)&&(stu.type != '-1');
					})!== -1;
			};
			$scope.isErrOrder = function (stu,order) {//当前学生题目数据错误详细
				var obj = _.find($scope.errPersonFid,function (item) {
						return item.userfid === stu.studentId;
					});
				if(obj && obj.qOrder){
					return	_.findIndex(obj.qOrder,function (o) {
						return o.order === order;
					}) !== -1 ? {'border':'1px solid red'} :{}
				}
			}
		});
	}]
})