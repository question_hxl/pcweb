define([], function () {
	return ['$http', '$scope', '$rootScope', '$state','constantService','ngDialog','$q','managerUrl',function ($http, $scope, $rootScope, $state,constantService,ngDialog,$q,managerUrl) {

		var user = angular.fromJson(sessionStorage.getItem('currentUser') || undefined);
		$scope.goMorede = 0;
		$http.get($rootScope.baseUrl + '/Interface0027.ashx').success(function (data) {
			$scope.classes = data.msg;
			$scope.classes.push({
				classId: '',
				name: '统一阅卷'
			});

			$scope.currentClass = $scope.classes[0];
			$scope.subject = $scope.currentClass.name;
		});

		//$scope.coursesList = [];

		$scope.$watch('currentClass', function () {
			if ($scope.currentClass == null) {
				$scope.coursesList = [];
				$scope.selectedCourse = null;
			} else {
				getCoursess();
			}
		}, true);

		function getCoursess() {
			if($scope.currentClass.classId === '') {
				$http.post('/BootStrap/EncryptUnified/GetEncryptUnifiedList.ashx').then(function(res){
					if(_.isArray(res.data.msg)) {
						_.each(res.data.msg, function(item){
							item.examName = item.examName;
							item.exType = item.exType;
							item.examNum = item.noFinishCount;
							item.classNum = item.allCount;
							item.examFlnkID = item.examFId;
						});
						$scope.unifiedIFId = false;
						$scope.testpaper = res.data.msg;
					}else {
						$scope.testpaper = [];
					}
				}, function(){
					$scope.testpaper = [];
				});
			}else {
				$http({
					method: 'post',
					url: '/BootStrap/Interface/getPaperListForMark.ashx',
					data: {
						classFlnkID : $scope.currentClass.classId
					}
				}).success(function (res) {
					if(_.isArray(res.msg)) {
						_.each(res.msg, function(item){
							item.examDate = item.examDate.split(" ");
						});
						_.each(res.msg, function(item){
							item.examDate = item.examDate[0];
						});
						$scope.unifiedIFId = true;
						$scope.testpaper = res.msg;
					}else {
						$scope.testpaper = [];
					}
					$scope.testPaper = $scope.testpaper
				});
			}
		}
		
		$scope.goEncryptMarking = function(paper){
			window.open('#/encryptmarking?examId='+paper.examFId+'&unifiedId='+paper.unifiedIFId + '&isOpenNewWin=' + 1);
		};

		$scope.gocomposing = function (goExamFlnkID) {
			location.href = "#/composing?ExamFlnkID=" + goExamFlnkID + "&ClassFlnkid=" + $scope.currentClass.classId;
		};
        $scope.gocomPoseRate = function (unifiedId) {
			$scope.curExamName = unifiedId.examName;
            $http.post('/BootStrap/EncryptUnified/WJFGetEncryptQList.ashx', {
                unifid : unifiedId.unifiedIFId
            }).success(function(res){
				ngDialog.open({
					template: 'template/examprocessing.html',
					className: 'ngdialog-theme-plain',
					appendClassName: 'examprocessing-confirm-to-setting',
					scope: $scope
				});
                if(_.isArray(res.msg)&&res.msg.length) {
                    $scope.examprocesslist = filterShowData(res.msg);
                    // var min = +$scope.examprocesslist[0].noFinishCount;
                    // var max = +$scope.examprocesslist[0].noFinishCount;
                    // _.each($scope.examprocesslist,function (item) {
                    //     if (max >= +item.noFinishCount) {
                    //         $scope.maxCount = +item.noFinishCount;
                    //         max = $scope.maxCount;
                    //     }
                    //     if (min <= +item.noFinishCount) {
                    //         $scope.minCount = +item.noFinishCount;
                    //         min = $scope.minCount;
                    //     }
                    // });
                }else {
                    $scope.examprocesslist = [];
                }
            })

        };
        $scope.widther = function (val) {
            var wid = (+val.allCount - +val.noFinishCount) * 100 / (+val.allCount);
            return {width: wid + '%'}
        };

		$scope.selectSubject = function (klass) {
			$scope.subject = klass.name;
			$scope.currentClass = klass;
		};
		$scope.analysis = function(paper){
				$http.post($rootScope.baseUrl + '/Interface0207.ashx', {
					classFlnkID: $scope.currentClass.classId,
					examFlnkID:paper.examFLnkId
				}).then(function(res){
					if(res.data.code ===0) {
						constantService.alert('提交分析成功！');
						paper.status = '4';
					}else {
					    constantService.alert(res.data.msg);
					}
				}, function(res){
				    constantService.alert(res.data.msg);
				});
		};
		$scope.goMore = function (val) {
			$scope.goMorede = val;
		};
		$scope.goMoreClose = function () {
			$scope.goMorede = '';
		};
		$scope.setDetail = function (val) {
			if(val.status == '4'){
				return;
			}
			$scope.currentOCRScoreModel = val.OCRScoreModel;
			$scope.currentOCRMissedHandler = val.OCRMissedHandler;
			ngDialog.open({
				template: '<div class="goToWeChat clearfix"> ' +
				'<div style="background: #41B39C ; height: 35px;border-radius: 10px 10px 0 0;"><p></p> </div> ' +
				'<div class="clearfix" style="padding: 20px 40px;"><div><big>配置试卷</big></div>' +
				'<div style="text-align: left"><div class="clearfix">分值计算方式：</div>' +
				'<div class="setDetailMode" ng-click="changeOCRScoreModel(\'1\')" style="width: 60%"><span ng-show="currentOCRScoreModel == 2"><img src="../img/weixuan.png"></span> <span ng-show="currentOCRScoreModel == 1"><img src="../img/yixuan.png"></span>  加分模式</div>' +
				'<div class="setDetailMode" ng-click="changeOCRScoreModel(\'2\')" style="width: 40%"><span ng-show="currentOCRScoreModel == 1"><img src="../img/weixuan.png"></span> <span ng-show="currentOCRScoreModel == 2"><img src="../img/yixuan.png"></span>  扣分模式</div></div>' +
				'<div style="text-align: left;margin-top: 45px;"><div class="clearfix">未批阅处理方式：</div>' +
				'<div class="setDetailMode" ng-click="changeOCRMissedHandler(\'1\')" style="width: 60%"><span ng-show="currentOCRMissedHandler == 2"><img src="../img/weixuan.png"></span> <span ng-show="currentOCRMissedHandler == 1"><img src="../img/yixuan.png"></span>  0分</div>' +
				'<div class="setDetailMode" ng-click="changeOCRMissedHandler(\'2\')" style="width: 40%"><span ng-show="currentOCRMissedHandler == 1"><img src="../img/weixuan.png"></span> <span ng-show="currentOCRMissedHandler == 2"><img src="../img/yixuan.png"></span>  保持</div></div></div>' +
				'<button class="smBtn" ng-click="done()">确认</button></div>',
				className: 'ngdialog-theme-plain',
				appendClassName: 'setDetail-confirm-to-setting',
				scope: $scope,
				closeByDocument: false,
				plain: true,
				controller: function ($scope) {
					return function () {
						$scope.done = function () {
							ngDialog.close();
							$http({
								method: 'post',
								url: '/BootStrap/Interface/modifyMarkConfig.ashx',
								data: {
									taskFLnkId : val.taskFLnkId,
									OCRScoreModel:$scope.currentOCRScoreModel,
									OCRMissedHandler:$scope.currentOCRMissedHandler
								}
							}).success(function (res) {
								if(!res.code) {
									constantService.alert('配置成功！');
									val.OCRScoreModel = $scope.currentOCRScoreModel;
									val.OCRMissedHandler = $scope.currentOCRMissedHandler;
								}
							});

						};
						$scope.changeOCRScoreModel = function (val) {
							if(+$scope.currentOCRScoreModel === val){
								return
							}else {
								$scope.currentOCRScoreModel = val ;
							}
						};
						$scope.changeOCRMissedHandler = function (val) {
							if(+$scope.currentOCRMissedHandler === val){
								return
							}else {
								$scope.currentOCRMissedHandler = val;
							}
						};
					}
				}
			});

		};
		$scope.toWeChat = function (val,index) {
			if(val.isSend === '0' && val.status == '4'){
				ngDialog.open({
					template: '<div class="goToWeChat clearfix"> ' +
					'<div class="mainNar">编辑微信通知模板</div> ' +
					'<div class="mainBorder" contenteditable="true">尊敬的家长您好：<br><span contenteditable="false" style="color: #FF0000;display: inline-block">XXX</span>同学，<span contenteditable="false" style="color: #FF0000;display: inline-block">《' + val.examName+'》</span>的成绩报告已生成，请点击查看！</div>' +
					'<div style="font-size: 12px;text-align: right;color: #ff0000;margin-right:20px;">红色字为系统关键字不可编辑</div>' +
					'<button class="smBtn" ng-click="done()">发送</button></div>',
					className: 'ngdialog-theme-plain',
					appendClassName: 'goToWeChat-confirm-to-setting',
					scope: $scope,
					closeByDocument: false,
					plain: true,
					controller: function ($scope) {
					return function () {
						$scope.done = function () {
							ngDialog.close();
							$http({
								method: 'post',
								url: '/BootStrap/Interface/sendWXMessage.ashx',
								data: {
									taskFlnkId : val.taskFLnkId,
									schoolFlnkId:user.schoolFId,
									examFLnkId:val.examFLnkId,
									msg:$('.mainBorder').text()
								}
							}).then(function (res) {
								if(!!(res.data.code+'')) {
									if(res.data.code == '0'){
										$scope.testpaper[index].isSend = '1';
									}
									constantService.alert(res.data.code == '0'?'发送成功!':res.data.msg)
								}
							},function (res) {
								constantService.alert('发送失败!');
							});
						};
					}
				}
				});
			}
		};
		var papaerList = [];
		var noRelatedList = [];
		$scope.gomarkingfastY = function (ExamFlnkID, status,TaskFId,isAllowMark,examName,UnifiedItemFid) {
            UnifiedItemFid = (UnifiedItemFid === null || UnifiedItemFid === undefined?0:UnifiedItemFid);//当为null或者undefined时候为0
			$scope.goExamFId = ExamFlnkID;
			getUnbindPapger(TaskFId).then(function(resp){
				papaerList = resp.data.msg;
				_.each(papaerList,function (item) {
					if(item.isNoRelated === 0 || item.isNoRelated === 2){
                        noRelatedList.push(item);
					}
                });
				if(status !== '4') {//2.待批阅
					chooseNgDialog(status);
				}else {
					constantService.alert('该试卷分析已经形成，如果需要再次批阅，请重新提交分析！', function(){
						chooseNgDialog(status);
					});
				}
			});
			function chooseNgDialog(status) {
				ngDialog.open({
					template: '<div class="goToWeChat clearfix"> ' +
					'<p class="mainNar"></p> ' +
					'<div style="padding: 20px 40px;"><big>请选择批阅方式</big></div>' +
					'<button class="smBtn" ng-click="goOverAllMarking()">快速批阅(誊分)</button><button class="smBtn" ng-click="goFastMarking()">线上批阅</button></div>',
					className: 'ngdialog-theme-plain',
					appendClassName: 'goToWeChat-confirm-to-setting',
					scope: $scope,
					closeByDocument: false,
					plain: true,
					controller: function ($scope) {
						return function () {
							$scope.goOverAllMarking = function () {
								ngDialog.close();
								if(status == '1'){
									if(_.isArray(noRelatedList)&&noRelatedList.length>0){
										location.href ='#/related?ExamFlnkID='+ $scope.goExamFId+'&ClassFlnkid='
											+ $scope.currentClass.classId + '&turnto=0'+ '&TaskFId=' + TaskFId
											+ '&allowMark=' + isAllowMark + '&ExamName=' + examName + '&UnifiedItemFid='
											+ UnifiedItemFid;
									}else {
										goJumpOverallMarking(status,TaskFId,isAllowMark,examName,UnifiedItemFid);
									}
								}else{
									//location.href = "#/pdf?ExamFlnkID=" + $scope.goExamFId + "&ClassFlnkID=" + $scope.currentClass.classId + "&TaskFId=" +TaskFId+ "&isAllowMark=" +isAllowMark;
									goJumpOverallMarking(status,TaskFId,isAllowMark,examName,UnifiedItemFid);
								}
							};
							$scope.goFastMarking = function () {
								ngDialog.close();
								if(status == '1'){
									if(_.isArray(noRelatedList)&&noRelatedList.length>0){
										location.href ='#/related?ExamFlnkID='+ $scope.goExamFId+'&ClassFlnkid=' + $scope.currentClass.classId+ '&turnto=1' + '&TaskFId=' + TaskFId + '&allowMark=' + isAllowMark + '&ExamName=' + examName;
									}else {
										goJumpFastMarking(status,TaskFId,isAllowMark,examName)									}
								}else {
									goJumpFastMarking(status,TaskFId,isAllowMark,examName)								}
							};
						}
					}
				});
			}
		};
		//按钮报表
		$scope.scoreprint = function (val) {
			var paper = _.find($scope.testpaper, function (paper) {
				return paper.examFLnkId === val.examFLnkId
			});
			if(paper.status === '4'){
				window.open(managerUrl + '/class-grid/index/index_new.html#' + '/?schoolId=' + user.schoolFId + '&paperId=' +
					paper.examFLnkId + '&session=' + paper.period + '&classId=' + $scope.currentClass.classId);
			}
		};
		//试卷纠错
		$scope.resetTaskStatus = function (paper,status,isAllowMark,examName) {
			if(paper.status == '4' || paper.status == '1'){
				return;
			}
			$http({
				method: 'post',
				url: '/BootStrap/Interface/resetTaskStatus.ashx',
				data: {
					taskFLnkId : paper.taskFLnkId
				}
			}).then(function (resp) {
				if(resp.data.code == '0'){
					resp.data.status = '1';
					location.href ='#/related?ExamFlnkID='+ paper.examFLnkId+'&ClassFlnkid=' + $scope.currentClass.classId+ '&turnto=0' + '&TaskFId=' + paper.taskFLnkId+ '&allowMark=' + paper.isAllowMark + '&ExamName=' + paper.examName+'&isShowAll='+'true';
				}
			},function (resp) {
				constantService.alert('重置失败!');
			});
		};
		/**
		 * 获取带纠错考卷
		 */
		function getUnbindPapger(TaskFId){
			var defer = $q.defer();
			$http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                taskFid :TaskFId
			}).then(function(res){
				defer.resolve(res);
			}, function(res){
				defer.reject(res)
			});
			return defer.promise;
		}
		function goJumpOverallMarking(status,TaskFId,isAllowMark,examName,UnifiedItemFid){
			if(status != '1'){
				$state.go('overallMarking',{
					ExamFlnkID:$scope.goExamFId,
					ClassFlnkID:$scope.currentClass.classId,
					TaskFId:TaskFId,
					allowMark:isAllowMark,
					ExamName:examName,
                    UnifiedItemFid:UnifiedItemFid
				});
			}else {
			$http.post('BootStrap/Interface/finishMarkCorrecting.ashx', {
				taskFLnkId : TaskFId
			}).then(function(resp){
				if(resp.data.code == '0'){
					$state.go('overallMarking',{
						ExamFlnkID:$scope.goExamFId,
						ClassFlnkID:$scope.currentClass.classId,
						TaskFId:TaskFId,
						allowMark:isAllowMark,
						ExamName:examName,
                        UnifiedItemFid:UnifiedItemFid
					});
				}else {
					constantService.alert(resp.data.msg);
				}
			},function(resp){
				constantService.alert('更新任务状态失败！');
			});
			}
		}
		function goJumpFastMarking(status,TaskFId,isAllowMark,examName){
			if(status != '1'){
				window.open('#/fastmarking?ExamFlnkID='+ $scope.goExamFId+'&ClassFlnkID=' + $scope.currentClass.classId+'&allowMark=' + isAllowMark + '&isOpenNewWin=' + 1);
			}else{
				$http.post('BootStrap/Interface/finishMarkCorrecting.ashx', {
					taskFLnkId : TaskFId
				}).then(function(resp){
					if(resp.data.code == '0'){
						window.open('#/fastmarking?ExamFlnkID='+ $scope.goExamFId+'&ClassFlnkID=' + $scope.currentClass.classId+'&allowMark=' + isAllowMark + '&isOpenNewWin=' + 1);
					}else {
						constantService.alert(resp.data.msg);
					}
				},function(resp){
					constantService.alert('更新任务状态失败！');
				});
			}
			getCoursess();
		}
		function filterShowData(list) {
			var repeatindex = [];
			var norepeat = [];
			_.each(list,function (item,index) {
				_.each(list,function (i,j) {
					if(index === j){
						return true;
					}else if((item.showDis === i.showDis) && j > index){
						repeatindex.push(j);
					}
				});
			});
			_.each(list,function (m,n) {
				var p = _.find(repeatindex,function (r) {
					return n === r;
				});
				if(p === 0 || !!p){
					return true;
				}else{
					norepeat.push(m);
				}
			});
			norepeat = _.filter(norepeat,function (t) {
				return t.isEncrypt === '1';
			});
			var groupnameList = [];
			_.each(norepeat,function (f) {
				groupnameList.push(f.groupname);
			});
			groupnameList = _.uniq(groupnameList);
			var qstlist = [];
		_.each(groupnameList,function (g) {
			var tempqstlist = _.filter(norepeat,function (h) {
				return h.groupname === g;
			});
			if(_.isArray(tempqstlist) && tempqstlist.length>0){
				qstlist.push({finish:tempqstlist[0].finish,qstlist:tempqstlist});
			}
		});
			return qstlist;
		}
	}]
});