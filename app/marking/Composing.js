define(["underscore-min", "FileSaver", 'jquery.datetimepicker', 'star-rating', 'pagination', 'dragable','jqfly',
	'requestAnimationFrame', 'TransferPaper2Html'], function () {
	return ['$http', '$scope', '$rootScope', '$state', 'ngDialog', '$location', '$sce', "paperService", 'constantService', 'cartService',
		function ($http, $scope, $rootScope, $state, ngDialog, $location, $sce, paperService, constantService, cartService) {
			var user = JSON.parse(sessionStorage.getItem('currentUser'));
			var GradeId = user.GradeId;
			var role = $scope.role = user.role;
			var searchfrom = $location.search();//地址栏参数
			$scope.isExamCenter = searchfrom.isExamCenter;
			var PAPER_SMALL_TITLE = '编辑小标题';

			var pathss = searchfrom.pathss;//系统路径来源
			if (pathss == '/paperBankmy') {
				$scope.ismypaper = true;
			} else {
				$scope.ismypaper = false;
			}
			$scope.isbjzy = false;

			$scope.testpaperId = searchfrom.ExamFlnkID;
			paperService.getPaperDataById(searchfrom.ExamFlnkID).then(function (res) {
				var data = res.data;
				$scope.isbjzy = data.examType == '班级作业';
				$scope.testpaperName = data.examName;
				$scope.papaerList = data;
				$scope.smalltitle = data.SmallTitle;
				$scope.times = data.spendtime;
				$scope.paperName = data.examName;
				$scope.QuestionListBox = data.msg;
				$scope.exType = data.examType;
				$scope.queList = data.msg;
				$scope.queList = _setExplain($scope.queList);
				$scope.paperGradeId = data.GradeId;
				$scope.paperGradeName = data.GradeName;

				$scope.groupList = paperService.getQuestionByGroup(data.msg);
				//获取试题篮数据，遍历之后设置试题篮
				getQuestionCart();
			});

			function _setExplain(list){
				_.map(list,function(item,index){
					item.explain =  item.explain ? item.explain : (item.QTypeName + '（共' + item.QNumber + '小题, 共' + item.QTypeScores + '分）');
				});

				return list;
			}

			function getQuestionCart(){
				cartService.getCart().then(function(res){
					if(_.isArray(res)) {
						_.each($scope.QuestionListBox, function(group){
							_.each(group.question, function(question){
								var qInCart = _.find(res, function(item){
									return item.questionFId.toUpperCase() === question.FLnkID.toUpperCase();
								});
								question.isInCart = !!qInCart;
							});
						})
					}
				});
			}

			//// 布置试卷
			//$scope.sendPaper = function () {
			//	if ($scope.testpaperName) {
			//		// $('.paperComposing .sendPaperButton').css({
			//		//     'background': '#6C6FC0'
			//		// })
			//		$http({
			//			method: 'post',
			//			url: $rootScope.baseUrl + '/Interface0150.ashx',
			//			data: {}
			//		}).success(function (res) {
			//			var tempArray = res.msg;

			//			if (tempArray.length > 0) {
			//				var classStr = '<div style="height:100px;width:100%;">';
			//				classStr += '<div class="selectValues"><div style="display:inline-block;margin-right: 10px;">布置对象: </div>';
			//				for (var i = 0; i < tempArray.length; i++) {
			//					classStr += '<input data-id="' + tempArray[i].classId + '" type="checkbox" class="addPointer" id="popUpDialog_' + i + '" /><lable class="addPointer inplable" for="popUpDialog_' + i + '">' + tempArray[i].name + '</lable>';
			//				}
			//				classStr += '</div>';
			//				classStr += '<div><div style="display:inline-block;padding-left: 30px;margin-top: 20px;"><div class="popUpDialog_beginTime">测试时间:</div><input id="popUpDialog_beginTime" readonly="readonly" placeholder="开始时间"/></div>'
			//				classStr += '<div style="display:inline-block"><div class="popUpDialog_endTime">至:</div><input id="popUpDialog_endTime" readonly="readonly" placeholder="结束时间"/></div></div></div>';
			//				ngDialog.open({
			//					template: classStr + '<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">布置试卷</button></div>',
			//					plain: true,
			//					className: 'ngdialog-theme-plain bigger',
			//					preCloseCallback: function () {
			//						var domList = $('.selectValues input');
			//						var classFLnkID = '';
			//						for (var i = 0; i < domList.length; i++) {
			//							if (domList[i].checked) {
			//								if (i === domList.length - 1)
			//									classFLnkID += domList[i].getAttribute('data-id');
			//								else
			//									classFLnkID += domList[i].getAttribute('data-id') + ',';
			//							}
			//						}
			//						var param = {
			//							examName: $scope.testpaperName,
			//							examFLnkID: $scope.testpaperId,
			//							classFLnkID: classFLnkID,
			//							startTime: $('#popUpDialog_beginTime').val(),
			//							stopTime: $('#popUpDialog_endTime').val()
			//						}
			//						//弹框提示
			//						var msg = '';
			//						if (!param.examFLnkID) {
			//							msg = '非法的试卷ID';
			//						} else if (!param.classFLnkID) {
			//							msg = '请选择班级';
			//						} else if (!param.startTime) {
			//							msg = '请选择开始时间';
			//						} else if (!param.stopTime) {
			//							msg = '请选择结束时间';
			//						}

			//						if (msg !== '') {

			//							ngDialog.open({
			//								template: '<p>' + msg + '</p>' +
			//								'<div class="ngdialog-buttons">' +
			//								'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
			//								plain: true,
			//								className: 'ngdialog-theme-plain'
			//							});

			//							return false;
			//						} else {
			//							$http({
			//								method: 'post',
			//								url: $rootScope.baseUrl + '/Interface0218.ashx',
			//								data: param
			//							}).success(function (res) {
			//								$('.sendPaperButton').hide();
			//								ngDialog.open({
			//									template: '<p>布置试卷成功</p>' +
			//									'<div class="ngdialog-buttons">' +
			//									'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
			//									plain: true,
			//									className: 'ngdialog-theme-plain'
			//								});
			//							})
			//						}
			//					}
			//				});
			//				addCalendarClick();
			//			} else {
			//				ngDialog.open({
			//					template: '<p>没有查询到班级</p>' +
			//					'<div class="ngdialog-buttons">' +
			//					'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
			//					plain: true,
			//					className: 'ngdialog-theme-plain'
			//				});
			//			}
			//		})
			//	}

			//}

			function addCalendarClick() {
				console.log('tset3');
				if ($('#popUpDialog_beginTime').length > 0) {
					console.log('tset2');
					$('#popUpDialog_beginTime').datetimepicker({
						lang: "ch",
						timepicker: false,
						format: "Y-m-d",
						todayButton: false,
						start: new Date(),
						scrollInput:false,
						onChangeDateTime: function (dp, $input) {

							$('#popUpDialog_beginTime').datetimepicker('hide');
						}
					});

					$('#popUpDialog_endTime').datetimepicker({
						lang: "ch",
						timepicker: false,
						format: "Y-m-d",
						todayButton: false,
						start: new Date(),
						scrollInput:false,
						onChangeDateTime: function (dp, $input) {
							// if ($input.val() < $('#popUpDialog_beginTime').val()) {
							//     ngDialog.open({
							//         template: '<p>结束时间需大于开始时间</p>' +
							//             '<div class="ngdialog-buttons">' +
							//             '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							//         plain: true,
							//         className: 'ngdialog-theme-plain'
							//     });
							// } else {

							$('#popUpDialog_endTime').datetimepicker('hide');
							// }
						}
					});
				} else {
					console.log('tset');
					setTimeout(function () {
						addCalendarClick();
					}, 100);
				}
			}

			$scope.gomao = function (orders) {
				var topL1 = $("#" + orders + "").offset().top;
				$(document).scrollTop(topL1)
			}

			//另存为试卷
			$scope.saveNewPager = function () {
				var dialog = ngDialog.open({
					template: 'template/saveAsTpl.html',
					className: 'ngdialog-theme-default',
					appendClassName: 'save-dialog',
					scope: $scope,
					controller: function($scope, $http, $rootScope){
						return function(){
							$scope.saveName = $scope.paperName;
							$http.post($rootScope.baseUrl + '/Interface0290A.ashx').then(function(res){
								if(_.isArray(res.data.course)) {
									$scope.gradeList = res.data.course;
									$scope.currentGrade = _.find($scope.gradeList, function(item){
										return item.gradeId === $scope.paperGradeId;
									});
									if(!$scope.currentGrade) {
										$scope.gradeList.unshift({
											gradeId: $scope.paperGradeId,
											gradeName: $scope.paperGradeName
										});
										$scope.currentGrade = $scope.gradeList[0];
									}
								}
							});
							$scope.setChosedGrade = function(grade){
								$scope.currentGrade = grade;
							};
							$scope.cancel = function(){
								dialog.close();
							};
							$scope.ok = function(){
								$scope.saveNewPager_($scope.saveName, $scope.currentGrade);
							}
						}
					}
				});
			};

			$scope.removeFromCart = function(qst){
				cartService.removeCart(qst.FLnkID).then(function(){
					qst.isInCart = false;
					$scope.$emit('baseketList_number_change');
				});
			};

			$scope.addToBasket = function(qst, e){
				cartService.putCart(qst.FLnkID, qst.qtypeName, qst.qtypeId).then(function(){
					var offset = $("#side-bar .cart").offset(),
						flyer = $('<img src="../img/paper.png" class="u-flyer">');
					flyer.fly({
						start: {
							left: e.clientX,
							top: e.clientY
						},
						end: {
							left: offset.left,
							top: offset.top - $(window).scrollTop(),
							width: 80,
							height: 80
						},
						speed: 1,
						onEnd: function(){
							flyer.remove();
							qst.isInCart = true;
							$scope.$emit('baseketList_number_change');
						}
					});
				});
			};

			$scope.saveNewPager_ = function (testpaperName, grade) {
				var action = searchfrom.action;
				$scope.gradeId = $location.search().gradeid; // 年级ID
				if (action == 0) {//标志为另存examFLnkIDnew 重置0
					var examFLnkIDnew = searchfrom.ExamFlnkID;
					if (!testpaperName) {
					    constantService.alert('请设置试卷名称');
						return false;
					}
				}
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0248.ashx',
					data: {
						examName: testpaperName,
						examFLnkID: examFLnkIDnew,
						gradeId: grade.gradeId
					}
				}).success(function (res) {
					if (res.code == 0) {
						$scope.msgs = res.msg;
						localStorage.removeItem('askList'); // 清除缓存
						localStorage.removeItem('answerList'); //清除补偿存储
						constantService.alert('保存成功');
						return false;
					} else if (res.code == 5 || res.code === 4) {
						constantService.alert(res.msg);
					}
				});
				ngDialog.close();
			};


			//下载试卷
			$scope.downloadpaper = function (Examname) {
				$scope.Examid = searchfrom.ExamFlnkID;
				$scope.Examname = Examname;
				ngDialog.open({
					template: '<div> <p>请选择导出方式：</p><div style="padding:10px">' +
					'<button class="btn btn-info" ng-click="downWord(Examid,Examname,1)">导出试卷</button> ' +
					'<button  class="btn btn-info" ng-click="downWord(Examid,Examname,2)">导出卷卡合一</button> ' +
					// '<button class="btn btn-info" ng-click="downWord(Examid,Examname,3)">导出作业</button>' +
					'</div></div>',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					plain: true
				})
			}

			// 下载
			$scope.downWord = function (id, name, type) {
				ngDialog.close();
				if (type == 1) {
					// $http.post($rootScope.baseUrl + '/Interface0118A.ashx', {
					// 	examId: id,
					// 	examName: name
					// }).success(function (res) {
					// 	var blob = new Blob([res.msg], {
					// 		type: "text/plain;charset=utf-8"
					// 	});
					// 	saveAs(blob, name + ".doc");
					// 	ngDialog.close();
					// })
					$http.post($rootScope.baseUrl + '/Interface0181.ashx', {
						examId: id
					}).then(function(res){
						var paperTransfer = new $.PaperTransfer(res.data);
						paperTransfer.exportToDoc();
					});
				} else if (type == 2) {
					//获取下载地址下载
					var win = window.open();
					$http.post($rootScope.baseUrl + '/Interface0118.ashx', {
						examId: id,
						examName: name
					}).success(function (res) {
						win.location = res.msg;
						setTimeout(function(){
							win.close();
						}, 500);
					});
				} else if (type == 3) {
					$http.post($rootScope.baseUrl + '/Interface0118B.ashx', {
						examId: id,
						examName: name
					}).success(function (res) {
						var blob = new Blob([res.msg], {
							type: "text/plain;charset=utf-8"
						});
						saveAs(blob, name + ".doc");
						ngDialog.close();
					})
				}
			}


			//收藏状态
			$scope.isfav = false;

			//收藏试卷
			$scope.favorpaper = function () {
				var ExamFlnkIDfav = $location.search().ExamFlnkID;
				$http.post($rootScope.baseUrl + '/Interface0204.ashx', {
					type: 1,
					resourceFlnkID: ExamFlnkIDfav
				}).success(function (data) {
					if (data.code != 2) {
						$scope.isfav = true;
						ngDialog.open({
							template: '<p>' + data.msg + '</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-plain'
						});
						return false;
					} else {
						$scope.isfav = false;
						ngDialog.open({
							template: '<p>' + data.msg + '</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-plain'
						});
						return false;
					}
				})
			}


			//取消收藏
			$scope.favorpaperundo = function () {
				var ExamFlnkIDfav = $location.search().ExamFlnkID;
				$http.post($rootScope.baseUrl + '/Interface0205.ashx', {
					type: 1,
					resourceFlnkID: ExamFlnkIDfav
				}).success(function (data) {
					if (data.code != 2) {
						$scope.isfav = false;
						ngDialog.open({
							template: '<p>' + data.msg + '</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-default'
						});
						return false;
					} else {
						$scope.isfav = true;
						ngDialog.open({
							template: '<p>' + data.msg + '</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-default'
						});
						return false;
					}
				})
			};

			//编辑试卷
			$scope.editpaper = function () {

				$location.path('/paperComposing').search({
					'examId': searchfrom.ExamFlnkID,
					'action': 3,
					'gradeid': GradeId
				});
			}


			//跳转编辑题目界面FLnkID
			$scope.goeditque = function (FLnkID, diff, qtname) {
				if (role == "管理员") {
					var gradeid = $location.search().gradeid;
					$location.path('/Editquestion').search({
						qFlnkID: FLnkID,
						DifficultLevel: diff,
						QTypeName: qtname,
						GradeId: gradeid,
						state: 1
					});
				} else {
				    constantService.alert('非管理员不可编辑题目');
					return false;
				}
			}
		}]
});