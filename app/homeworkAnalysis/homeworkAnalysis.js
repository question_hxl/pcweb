/**
 * Created by 贺小雷 on 2016/11/11.
 */
define([], function(){
    return['$rootScope','$scope','$http','$q','constantService',
        function($rootScope, $scope, $http, $q, constantService){
        var user = angular.fromJson(sessionStorage.getItem('currentUser'));
        var schoolId = user.schoolFId;
        $scope.name = '年级';
        $http.get($rootScope.baseUrl + '/Interface0150.ashx').then(function(res){
            if(res.data && _.isArray(res.data.msg)) {
                $scope.classList = res.data.msg;
                $scope.classList.unshift({classId: "",gradeNo: "",name: $scope.name,smallNo: ""});
                $scope.currentClass = $scope.classList[0];
            }else {
                $scope.classList = [];
                $scope.currentClass = {};
            }
            getPaperList();
        });

        function getPaperList(){
            $scope.paperList = [];
            $('.box ul').stop(false, true).animate({left: 0});
            $scope.showNoPaper = false;
            if(!_.isEmpty($scope.currentClass)) {
                if($scope.currentClass.name === $scope.name){
                    $scope.lowScore = '40';
                    $scope.highScore = '80';
                    return $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                        Proc_name:'Proc_GradJobList',
                        schoolFLnkID: user.schoolFId,
                        grad: +user.GradeNo,
                        subjectId: user.subjectId
                    }).then(function(res){
                        if(res.data && _.isArray(res.data.msg)) {
                            $scope.paperList = res.data.msg;
                            _.each($scope.paperList, function (item) {
                                item.ExamDate = (item.ExamDate.split(" "))[0];
                                item.ExamFlnkID = item.ExExamFLnkID;
                            });
                            $scope.currentPaper = $scope.paperList[0];
                            if(!$scope.paperList || $scope.paperList.length === 0) {
                                $scope.showNoPaper = true;
                            }else {
                                $scope.showNoPaper = false;
                            }
                            getGradeData();
                        }else {
                            $scope.paperList = [];
                            $scope.currentPaper = {};
                            $scope.showNoPaper = true;
                        }
                    });
                }else {
                    return $http.post($rootScope.baseUrl + '/Interface0156.ashx', {
                        ClassFlnkid: $scope.currentClass.classId
                    }).then(function(res){
                        if(res.data && _.isArray(res.data.msg)) {
                            $scope.paperList = _.filter(res.data.msg, function(paper){
                                return paper.ExType === '作业';
                            });
                            $scope.currentPaper = $scope.paperList[0];
                            if(!$scope.paperList || $scope.paperList.length === 0) {
                                $scope.showNoPaper = true;
                            }else {
                                $scope.showNoPaper = false;
                            }
                            getAchievementData();
                        }else {
                            $scope.paperList = [];
                            $scope.currentPaper = {};
                            $scope.showNoPaper = true;
                        }
                    });
                }
            }else {
                $scope.paperList = [];
                $scope.currentPaper = {};
                $scope.showNoPaper = true;
            }
        }
        function getGradeData() {
            if(!$scope.currentPaper || _.isEmpty($scope.currentPaper)) {
                return;
            }
            var url = $rootScope.baseUrl + '/generalQuery.ashx';
            var params = {
                Proc_name: 'Proc_GradJobDetail',
                schoolFid: schoolId,
                period: $scope.currentPaper.Period,
                examFid: $scope.currentPaper.ExExamFLnkID,
                highScore: +$scope.highScore,
                lowScore: +$scope.lowScore
            };
            $http.post(url, params).then(function(res){
                if(_.isArray(res.data.msg)){
                    $scope.lowScorer =  $scope.lowScore;
                    $scope.highScorer = $scope.highScore;
                    _.each(res.data.msg, function (value) {
                        value.studentName += '，';
                    });
                    $scope.classInFroList = _.groupBy(res.data.msg, 'ClassName');
                }
            });

        }
        function getAchievementData(){
            if(!$scope.currentPaper || _.isEmpty($scope.currentPaper)) {
                return;
            }
            var url = '/BootStrap/Report/getExamDetailByGridTypeAndClassId.ashx';
            var params = {
                schoolId: schoolId,
                paperId: $scope.currentPaper.ExamFlnkID,
                girdType: 1,
                session: $scope.currentPaper.Period,
                classId: $scope.currentClass.classId
            };
            $http.post(url, params).then(function(res){
                if(res.data.data.length) {
                    var data = res.data.data[0].subjectList;
                    var orderData = _.filter(data, function(item){
                        return !_.isNaN(+item.subjectNumber) && _.isNumber(+item.subjectNumber);
                    });
                    var classAchievementData = {};
                    classAchievementData.orders = _.pluck(orderData, 'subjectNumber');
                    classAchievementData.studentList = [];
                    _.each(orderData[0].studentList, function(stu){
                        classAchievementData.studentList.push({
                            studentName: stu.studentName,
                            scoreList: [],
                            goal: 0
                        });
                    });
                    for(var i = 0; i < classAchievementData.studentList.length; i++) {
                        _.each(orderData, function(item){
                            classAchievementData.studentList[i].goal += Math.floor(+(item.studentList[i] && item.studentList[i].goal || 0));
                            classAchievementData.studentList[i].scoreList.push(Math.floor(item.studentList[i] && item.studentList[i].goal || 0));
                        });
                    }
                    $scope.classData = classAchievementData;
                }
            });
        }

        $scope.selectSubject = function(subject){
            if(subject.classId !== $scope.currentClass.classId) {
                $scope.currentClass = subject;
                getPaperList();
            }
        };
        $scope.showStudent = function (item , status) {
            var students = '', studentList = [];
            item = _.sortBy(item, 'studentId');
            studentList = _.filter(item, function (value) {
                if(status === 1 || status === 2) {
                    return value.status === status;
                }else  if(status === 3){
                    return +value.studentSc >= +$scope.highScore && value.status === 1;
                }else  if(status === 4){
                    return +value.studentSc < +$scope.lowScore && value.status === 1;
                }
            });
            _.each(studentList, function (value, index) {
                students = students + value.studentName;
            });
            if(students === ''){
                return
            }else {
                var lastStr = students.substring(students.length - 1);
                students = ( lastStr === '，') ? students.substring(0, students.length - 1) : students;
            }
            constantService.alert(students);
        };
        $scope.selectCur = function(paper){
            $scope.currentPaper = paper;
            if($scope.currentClass.name === $scope.name){
                getGradeData();
                return
            }
            getAchievementData();
        };

        $scope.goPaperExplain = function(){
            location.href = "#/ViewAllQuestions?ClassFlnkID=" + $scope.currentClass.classId + "&ExamFlnkID=" + $scope.currentPaper.ExamFlnkID;
        };

        $scope.goPaperCheck = function(){
            window.open("#/composing?ExamFlnkID=" + $scope.currentPaper.ExamFlnkID + "&ClassFlnkID=" + $scope.currentClass.classId+'&isOpenNewWin=1');
        };
        $scope.scrollLeft = function(){
            if(!$('.moveBox ul').is(":animated")) {
                if(parseInt($('.moveBox ul').css('left')) < 0) {
                    $('.moveBox ul').stop(false, true).animate({
                        'left': '+=824px'
                    });
                }
            }
        };
        $scope.scrollRight = function(){
            if(!$('.moveBox ul').is(":animated")) {
                if(Math.abs(parseInt($('.moveBox ul').css('left'))) < 206 * ($scope.paperList.length - 4) && $('.moveBox ul').width() > 824) {
                    $('.moveBox ul').stop(false, true).animate({
                        'left': '-=824px'
                    });
                }
            }
        };
    }];
});