/**
 * Created by 贺小雷 on 2016/8/24.
 */
define(function(require){
   return['$rootScope', '$state', '$location', 'ngDialog', '$cookieStore', 'baseUrl', 'apiCommon',
       function ($rootScope, $state, $location, ngDialog, $cookieStore, baseUrl, apiCommon) {
       var assignRouter = require('../service/assignRouter').assignInject;
       $rootScope.goBack = function () {
           var param = $location.search();
           if (param.from) {
               location.hash = param.from;
           } else {
               window.history.go(-1);
           }
       };
       var param = $location.search();
       var tempName = decodeURIComponent(param.n),
           tempPsw = decodeURIComponent(param.p),
           isEmulate = param.isEm === '1';
       var appkey = param.appkey,
           userId = param.userid;
       if(appkey) {
           thirdPlatformLogin(appkey, userId, registEvent);
       }else {
           if(isEmulate) {
               emulateLogin(tempName, tempPsw, registEvent);
           }else {
               var user = angular.fromJson(sessionStorage.getItem('currentUser') || undefined);
               //如果在新窗口打开某些页面，本流程会将页面跳转到用户对应首页，导致用户无法正常浏览页面。
               //故在新打开窗口时，增加传参isOpenNewWin，接受到该参数时，不自动分配路由，跳往指定地址
               var isOpenNewWin = $location.search().isOpenNewWin === '1';
               if(user && user.SelfName === $cookieStore.get('userId')) {
                   if(!isOpenNewWin) {
                       assignRouter($state);
                   }
                   registEvent();
               }else {
                   apiCommon.checkLogin().then(function(res){
                       if(res.data.code === 0) {
                           //获取到用户信息
                           $cookieStore.put('userId', res.data.SelfName);
                           sessionStorage.setItem('currentUser', JSON.stringify(res.data));
                           if(!isOpenNewWin) {
                               assignRouter($state);
                           }
                       }else {
                           //未获取到用户信息，用户未登陆
                           window.sessionStorage.removeItem('currentUser');
                           $rootScope.$broadcast('CLEAR_CART_CACHE');
                           $state.go('login');
                       }
                   }, function(){
                       //未获取到用户信息，用户未登陆
                       window.sessionStorage.removeItem('currentUser');
                       $rootScope.$broadcast('CLEAR_CART_CACHE');
                       $state.go('login');
                   }).finally(registEvent);
               }
           }
       }

       $rootScope.baseUrl = baseUrl;

       function emulateLogin (tempName, tempPsw, cb){
           var loginParam = {
               userFId: tempName,
               tempPwd: tempPsw
           };
           apiCommon.emulateLogin(loginParam).success(function (data) {
               window.sessionStorage.setItem('currentUser', angular.toJson(data));
               $cookieStore.put('userId', data.SelfName);
               assignRouter($state);
               $rootScope.$broadcast('INIT_CART');
               $rootScope.$broadcast('UPDATE_ASSOCIATE_ACCOUNT');
           }).error(function(){
               $state.go('login');
           }).finally(cb);
       }

       function thirdPlatformLogin(appkey, userid, cb){
           apiCommon.JLLogin({
               appkey: appkey,
               userid: userid
           }).success(function (data) {
               window.sessionStorage.setItem('currentUser', angular.toJson(data));
               $cookieStore.put('userId', data.SelfName);
               assignRouter($state);
               $rootScope.$broadcast('INIT_CART');
               $rootScope.$broadcast('UPDATE_ASSOCIATE_ACCOUNT');
           }).error(function(){
               $state.go('login');
           }).finally(cb);
       }

       function registEvent(){
           $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
               var pathss = toState.url;
               if(pathss === '/login') {
                   // window.sessionStorage.removeItem('currentUser');
                   var param = $location.search();
                   var tempName = decodeURIComponent(param.n),
                       tempPsw = decodeURIComponent(param.p),
                       isEmulate = param.isEm === '1';
                   var appkey = param.appkey,
                       userId = param.userid;
                   if(appkey) {
                       event.preventDefault();
                       thirdPlatformLogin(appkey, userId, function(){});
                   }else if(isEmulate) {
                       event.preventDefault();
                       emulateLogin(tempName, tempPsw, function(){});
                   }
               }else if (pathss != '/test_allknolpoint') {

               } else {
                   $('body').css("background", "none");
                   $('.footer').css("display", "none");
               }

               var excludeValidate = toState.data.excludeValidate;
               var currentUser = angular.fromJson(sessionStorage.getItem('currentUser') || undefined);
               if (excludeValidate === false && typeof currentUser === 'undefined') {
                   event.preventDefault();
                   ngDialog.closeAll();
                   $rootScope.$broadcast('CLEAR_CART_CACHE');
                   $state.go('login');
               }
               if(currentUser && $cookieStore.get('userId') !== currentUser.SelfName) {
                   apiCommon.checkLogin().then(function(res){
                       if(res.data.code === 0) {
                           //获取到用户信息
                           sessionStorage.setItem('currentUser', JSON.stringify(res.data));
                           $rootScope.$broadcast('userChange');
                       }else {
                           //未获取到用户信息，用户未登陆
                           window.sessionStorage.removeItem('currentUser');
                           $rootScope.$broadcast('CLEAR_CART_CACHE');
                           $state.go('login');
                       }
                   });
               }
           });

           $rootScope.$on('SESSION_TIMEOUT', function () {
               console.error('SESSION_TIME_OUT!');
               ngDialog.closeAll();
               $rootScope.$broadcast('CLEAR_CART_CACHE');
               $state.go('login');
           });
       }
   }]
});