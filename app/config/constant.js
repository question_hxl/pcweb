/**
 * Created by 贺小雷 on 2017-07-31.
 */
define([], function(){
	return {
		ROLE: {
			TEACHER: '老师',
			STUDENT: '学生',
			GRADE_MASTER: '年级组长',
			SCHOOL_MASTER: '校长',
			CLASS_MASTER: '班主任',
			SCHOOL_MANAGER: '校管理员',
			MANAGER: '管理员'
		}
	}
});