/**
 * Created by 贺小雷 on 2016/8/24.
 */
define(['angular', 'angularAMD','../service/assignRouter'], function (angular, angularAMD, assignRouter) {
    return ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$logProvider', 'cfpLoadingBarProvider', '$sceDelegateProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider, $logProvider, cfpLoadingBarProvider, $sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist(['**']);
            // cfpLoadingBarProvider.includeSpinner = true;
            var loadingLogo = '../assets/skin/' + window.config.theme + '/img/loading-logo.png';
            cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div><img src=' + loadingLogo + ' width="195px">' +
                '</div><span class="fa fa-spinner fa-pulse"></span></div>';
            $stateProvider
                .state("login", angularAMD.route({
                    url: "/login",
                    templateUrl: loadHtml("/assets/skin/" + config.theme + "/template/loginBox.html"),
                    controllerUrl: "loginBox/loginBox.js",
                    data: {
                        excludeValidate: true
                    }
                }))
                .state("fastmarking",angularAMD.route({
                    url:"/fastmarking?UnifiedItemFid",
                    templateUrl:loadHtml("fastMarking/fastMarking.html"),
                    controllerUrl:"fastMarking/fastMarking.js",
                    data:{
                        excludeValidata:false
                    }
                }))
                .state("encryptmarking",angularAMD.route({
                    url:"/encryptmarking?examId&unifiedId",
                    templateUrl:loadHtml("encryptMarking/encryptMarking.html"),
                    controllerUrl:"encryptMarking/encryptMarking.js",
                    data:{
                        excludeValidata:false
                    }
                }))
                .state("startevaluating",angularAMD.route({
                    url:"/startevaluating",
                    templateUrl:loadHtml("evaluate/startEvaluating.html"),
                    controllerUrl:"evaluate/startEvaluating.js",
                    data:{
                        excludeValidata:false
                    }
                }))
                .state('overallMarking',angularAMD.route({
                    url:'/overallMarking?ExamFlnkID&ClassFlnkID&TaskFId&allowMark&ExamName&UnifiedItemFid',
                    templateUrl: loadHtml('overallMarking/overallMarking.html'),
                    controllerUrl:'overallMarking/overallMarking.js',
                    data:{
                        excludeValidata:false
                    }
                }))
                .state('answerSheet',angularAMD.route({
                    url:'/answerSheet',
                    templateUrl: loadHtml('answerSheet/answerSheet.html'),
                    controllerUrl:'answerSheet/answerSheet.js',
                    data:{
                    }
                }))
                .state('myApp', angularAMD.route({
                    abstract: true,
                    url: '',
                    templateUrl: loadHtml('mainView.html'),
                    controllerUrl: 'mainView.js',
                    data: {
                        excludeValidate: false
                    }
                }))
                .state('myApp.home', angularAMD.route({
                    url: '/homeView',
                    templateUrl: loadHtml('homeView/homeView.html'),
                    controllerUrl: 'homeView/homeView.js',
                    data: {
                        belong: 'home'
                    }
                }))
                .state('myApp.analysis', angularAMD.route({ //分析
                    url: '/analysis',
                    templateUrl: loadHtml('test_analysis/analysis.html'),
                    controllerUrl: 'test_analysis/analysis.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.analysis.test_analysis', angularAMD.route({ //班级分析
                    url: '/test_analysis',
                    templateUrl: loadHtml('test_analysis/test_analysis.html'),
                    controllerUrl: 'test_analysis/test_analysis.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.analysis.test_someknolpoint', angularAMD.route({ //部分知识点分析
                    url: '/test_someknolpoint',
                    templateUrl: loadHtml('test_analysis/test_someknolpoint.html'),
                    controllerUrl: 'test_analysis/test_someknolpoint.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.test_allknolpoint', angularAMD.route({ //所有知识点分析
                    url: '/test_allknolpoint',
                    templateUrl: loadHtml('test_analysis/test_allknolpoint.html'),
                    controllerUrl: 'test_analysis/test_allknolpoint.js',
                    data: {
                        belong: 'analysis',
                        hideShare: true
                    }
                }))
                .state('myApp.analysis.stu_analysis', angularAMD.route({ //学生分析
                    url: '/stu_analysis',
                    templateUrl: loadHtml('test_analysis/stu_analysis.html'),
                    controllerUrl: 'test_analysis/stu_analysis.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.analysis.classErrorBook', angularAMD.route({ //班级错题本
                    url: '/classErrorBook',
                    templateUrl: loadHtml('test_analysis/classErrorBook.html'),
                    controllerUrl: 'test_analysis/classErrorBook.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.analysis.testpaper', angularAMD.route({ //试卷分析
                    url: '/papertest?ClassFlnkID&ExamFlnkID',
                    templateUrl: loadHtml('testpaper/papertest.html'),
                    controllerUrl: 'testpaper/papertest.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.ViewAllQuestions', angularAMD.route({ //查看所有题目
                    url: '/ViewAllQuestions',
                    templateUrl: loadHtml('ViewAllQuestions/ViewAllQuestions.html'),
                    controllerUrl: 'ViewAllQuestions/ViewAllQuestions.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.EveryQuestion', angularAMD.route({ //逐题查看
                    url: '/EveryQuestion?ExAnswerFlnkID&ClassFlnkID&ExamFlnkID',
                    templateUrl: loadHtml('EveryQuestion/EveryQuestion.html'),
                    controllerUrl: 'EveryQuestion/EveryQuestion.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.scorereport', angularAMD.route({ //试卷分析-成绩报告
                    url: '/scorereport?ClassFlnkID',
                    templateUrl: loadHtml('scorereport/scorereport.html'),
                    controllerUrl: 'scorereport/scorereport.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.analyClass', angularAMD.route({ //战斗力分析
                    url: '/analyClass?ClassFlnkid',
                    templateUrl: loadHtml('analyClass/analyClass.html'),
                    controllerUrl: 'analyClass/analyClass.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.capacity', angularAMD.route({ //个人的 综合能力分析
                    url: '/capacity?Sfid&&ClassFlnkid&className',
                    templateUrl: loadHtml('capacity/capacity.html'),
                    controllerUrl: 'capacity/capacity.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.Perforsis', angularAMD.route({ //03试卷分析 - 成绩报告-成绩分析
                    url: '/Perforsis?ExAnswerFlnkID&ClassFlnkid&ExamFlnkID&MyName&userFlnkid&subjectId',
                    templateUrl: loadHtml('scorereport/Perforsis.html'),
                    controllerUrl: 'scorereport/Perforsis.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                //.state('myApp.assembly', angularAMD.route({
                //	url: '',
                //	templateUrl: 'question/questionAssembly.html'
                //}))
                .state('myApp.assembly.all', angularAMD.route({ // 全部组卷
                    url: '/assembly/all',
                    templateUrl: loadHtml('question/assemblyAll.html'),
                    controllerUrl: 'question/assemblyAll.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.assembly.class', angularAMD.route({ // 按照班级组卷
                    url: '/assembly/class',
                    templateUrl: loadHtml('question/assemblyClass.html'),
                    controllerUrl: 'question/assemblyClass.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.assembly.classRote', angularAMD.route({ // 按照分类组卷
                    url: '/assembly/classRote',
                    templateUrl: loadHtml('question/assemblyRote.html'),
                    controllerUrl: 'question/assemblyRote.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.assembly.classSet', angularAMD.route({ // 定制组卷
                    url: '/assembly/classSet',
                    templateUrl: loadHtml('question/assemblySet.html'),
                    controllerUrl: 'question/assemblySet.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.questionIndex', angularAMD.route({ // 出题首页
                    url: '/questionIndex',
                    templateUrl: loadHtml('question/questionIndex.html'),
                    controllerUrl: 'question/questionIndex.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.chapters', angularAMD.route({ // 章节出题
                    url: '/chapters',
                    templateUrl: loadHtml('chapters/chapters.html'),
                    controllerUrl: 'chapters/chapters.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.klgquestion', angularAMD.route({ // 知识点出题
                    url: '/klgquestion',
                    templateUrl: loadHtml('klgquestion/klgquestion.html'),
                    controllerUrl: 'klgquestion/klgquestion.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.questionBankAll', angularAMD.route({ // 系统题库
                    url: '/questionBankAll/:bankType',
                    templateUrl: loadHtml('questionBankView/questionBankView.html'),
                    controllerUrl: 'questionBankView/questionBankView.js',
                    data: {
                        belong: 'bank'
                    }
                }))
                .state('myApp.paperBank', angularAMD.route({ // 系统卷库
                    url: '/paperBank',
                    templateUrl: loadHtml('paperBank/paperBank.html'),
                    controllerUrl: 'paperBank/paperBank.js',
                    data: {
                        belong: 'bank'
                    }
                }))
                .state('myApp.paperBankSheet', angularAMD.route({ // 系统卷库
                    url: '/paperBankSheet?FID&FLnkID&unifiedId&action&MarkMode&CreateDate&ExName&examname',
                    templateUrl: loadHtml('paperBankSheet/paperBankSheet.html'),
                    controllerUrl: 'paperBankSheet/paperBankSheet.js',
                    data: {
                        belong: 'bank'
                    }
                }))
                .state('myApp.paperBankschool', angularAMD.route({ // 校本卷库
                    url: '/paperBankschool',
                    templateUrl: loadHtml('paperBankschool/paperBankschool.html'),
                    controllerUrl: 'paperBankschool/paperBankschool.js',
                    data: {
                        belong: 'bank'
                    }
                }))
                .state('myApp.paperBankmy', angularAMD.route({ // 我的卷库
                    url: '/paperBankmy',
                    templateUrl: loadHtml('paperBankmy/paperBankmy.html'),
                    controllerUrl: 'paperBankmy/paperBankmy.js',
                    data: {
                        belong: 'bank'
                    }
                }))
                .state('myApp.marking', angularAMD.route({ // 批阅
                    url: '/marking',
                    templateUrl: loadHtml('marking/marking.html'),
                    controllerUrl: 'marking/marking.js',
                    data: {
                        belong: 'marking'
                    }
                }))
                .state('myApp.markingfast', angularAMD.route({ // 批阅快速
                    url: '/fast?ExamFlnkID&ClassFlnkid&TaskFId',
                    templateUrl: loadHtml('marking/markingFast.html'),
                    controllerUrl: 'marking/markingFast.js',
                    data: {
                        belong: 'marking'
                    }
                }))
                .state('myApp.markingpdf', angularAMD.route({ // 批阅PDF
                    url: '/pdf?ExamFlnkID&ClassFlnkid&TaskFId',
                    templateUrl: loadHtml('marking/markingPdf.html'),
                    controllerUrl: 'marking/markingPdf.js',
                    data: {
                        belong: 'marking',
                        hideShare: true
                    }
                }))
                .state('myApp.relatedpaper', angularAMD.route({ // 关联试卷
                    url: '/related?ExamFlnkID&ClassFlnkid&TaskFId&allowMark&ExamName&UserFLnkID&isShowAll&turnto&UnifiedItemFid',
                    templateUrl: loadHtml('marking/relatedPaper.html'),
                    controllerUrl: 'marking/relatedPaper.js',
                    data: {
                        belong: 'marking',
                        hideShare: true
                    }
                }))
                .state('myApp.paperComposing', angularAMD.route({ // 出题
                    url: '/paperComposing?examId&action',
                    templateUrl: loadHtml('paperComposing/paperComposingNew.html'),
                    controllerUrl: 'paperComposing/paperComposingNew.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.composing', angularAMD.route({ // 批阅查看试卷
                    url: '/composing?ExamFlnkID&ClassFlnkid',
                    templateUrl: loadHtml('marking/Composing.html'),
                    controllerUrl: 'marking/Composing.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.manage', angularAMD.route({
                    url: '/manage',
                    templateUrl: loadHtml('manageEntry/manageEntry.html'),
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.manage.userInfo', angularAMD.route({ // 服务中心
                    url: '/userInfo',
                    templateUrl: loadHtml('userInfo/userInfo.html'),
                    controllerUrl: 'userInfo/userInfo.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.questionBasket', angularAMD.route({ // 进入试题篮
                    url: '/questionBasket',
                    templateUrl: loadHtml('questionBasket/questionBasket.html'),
                    controllerUrl: 'questionBasket/questionBasket.js',
                    data: {
                        belong: 'cart'
                    }
                }))
                .state('myApp.manage.userInfoClass', angularAMD.route({ // 班级管理
                    url: '/userInfoClass',
                    templateUrl: loadHtml('userInfo/userInfoClass.html'),
                    controllerUrl: 'userInfo/userInfoClass.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.manage.changePsw', angularAMD.route({ // 修改密码
                    url: '/changePsw',
                    templateUrl: loadHtml('userInfo/changePsw.html'),
                    controllerUrl: 'userInfo/changePsw.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.manage.changehead', angularAMD.route({ //图像管理
                    url: '/changehead',
                    templateUrl: loadHtml('userInfo/changehead.html'),
                    controllerUrl: 'userInfo/changehead.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.manage.stuCtrl', angularAMD.route({ //学生管理
                    url: '/stuCtrl',
                    templateUrl: loadHtml('userInfo/stuCtrl.html'),
                    controllerUrl: 'userInfo/stuCtrl.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.compensate', angularAMD.route({ // 补偿
                    url: '/compensate',
                    templateUrl: loadHtml('compensate/compensate.html'),
                    controllerUrl: 'compensate/compensate.js',
                    data: {
                        belong: 'composing'
                    }
                }))
                .state('myApp.klgpoint', angularAMD.route({ // 知识点分析
                    url: '/klgpoint',
                    templateUrl: loadHtml('klgpoint/klgpoint.html'),
                    controllerUrl: 'klgpoint/klgpoint.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.scoreprint', angularAMD.route({ // 分数打印
                    url: '/scoreprint',
                    templateUrl: loadHtml('scoreprint/scoreprint.html'),
                    controllerUrl: 'scoreprint/scoreprint.js',
                    data: {
                        belong: 'analysis',
                        hideShare: true
                    }
                }))
                .state('myApp.stuAllHis', angularAMD.route({ // 历史成绩
                    url: '/stuAllHis',
                    templateUrl: loadHtml('stuAllHis/stuAllHis.html'),
                    controllerUrl: 'stuAllHis/stuAllHis.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.UserViewAllQuestions', angularAMD.route({ // 知识点分析
                    url: '/UserViewAllQuestions?ClassFlnkID',
                    templateUrl: loadHtml('UserViewAllQuestions/UserViewAllQuestions.html'),
                    controllerUrl: 'UserViewAllQuestions/UserViewAllQuestions.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.Editquestion', angularAMD.route({ // 编辑题目
                    url: '/Editquestion',
                    templateUrl: loadHtml('Editquestion/Editquestion.html'),
                    controllerUrl: 'Editquestion/Editquestion.js'
                }))
                .state('myApp.Addquestion', angularAMD.route({ // 新增题目
                    url: '/Addquestion',
                    templateUrl: loadHtml('Addquestion/Addquestion.html'),
                    controllerUrl: 'Addquestion/Addquestion.js'
                }))
                .state('myApp.WrongAllQuestions', angularAMD.route({ //班级错题查看
                    url: '/WrongAllQuestions',
                    templateUrl: loadHtml('WrongAllQuestions/WrongAllQuestions.html'),
                    controllerUrl: 'WrongAllQuestions/WrongAllQuestions.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                .state('myApp.analysis.homeworkAnalysis', angularAMD.route({
                    url: '/homeworkAnalysis',
                    templateUrl: loadHtml('homeworkAnalysis/homeworkAnalysis.html'),
                    controllerUrl: 'homeworkAnalysis/homeworkAnalysis.js',
                    data: {
                        belong: 'analysis'
                    }
                }))
                //------------------学生端--------------------
                .state('myApp.stuIndex', angularAMD.route({ //学生首页
                    url: '/stuIndex',
                    templateUrl: loadHtml('stuIndex/stuIndex.html'),
                    controllerUrl: 'stuIndex/stuIndex.js',
                    data: {
                        belong: 'stuHome'
                    }
                }))
                .state('myApp.onlineStudy', angularAMD.route({
                    url: '/online-study',
                    templateUrl: loadHtml('onlineStudy/onlineStudy.html'),
                    controllerUrl: 'onlineStudy/onlineStudy.js',
                    data: {
                        belong: 'onlineStudy'
                    }
                }))
                .state('myApp.onlineSolution', angularAMD.route({
                    url: '/online-solution?ExamId&action&TaskId',
                    templateUrl: loadHtml('onlineStudy/onlineSolution.html'),
                    controllerUrl: 'onlineStudy/onlineSolution.js',
                    data: {
                        belong: 'onlineStudy'
                    }
                }))
                .state('myApp.stuPerforsis', angularAMD.route({
                    url: '/stuPerforsis',
                    templateUrl: loadHtml('stuPerforsis/stuPerforsis.html'),
                    controllerUrl: 'stuPerforsis/stuPerforsis.js',
                    data: {
                        belong: 'stuPerforsis'
                    }
                }))
                .state('myApp.evaluate', angularAMD.route({
                    url: '/evaluate',
                    templateUrl: loadHtml('evaluate/evaluate.html'),
                    controllerUrl: 'evaluate/evaluate.js',
                    data: {
                        belong: 'evaluate'
                    }
                }))
                .state('myApp.tRoomEvaluate', angularAMD.route({
                    url: '/tRoomEvaluate?ExamFlnkID&exType',
                    templateUrl: loadHtml('evaluate/startComputerRoomEvaluating.html'),
                    controllerUrl: 'evaluate/startComputerRoomEvaluating.js',
                    data: {
                        belong: 'evaluate'
                    }
                }))
                .state('myApp.roomEvaluateAnalysis', angularAMD.route({
                    url: '/roomEvaluateAnalysis?ExamFlnkID&ClassFlnkID',
                    templateUrl: loadHtml('evaluate/roomEvaluatingAnalysis.html'),
                    controllerUrl: 'evaluate/roomEvaluatingAnalysis.js',
                    data: {
                        belong: 'evaluate'
                    }
                }))
                .state('myApp.machineEvaluate', angularAMD.route({ //作业评测
                    url: '/machineEvaluate',
                    templateUrl: loadHtml('machineEvaluate/machineEvaluate.html'),
                    controllerUrl: 'machineEvaluate/machineEvaluate.js',
                    data: {
                        belong: 'stuEvaluate'
                    }
                }))
                .state('myApp.homeworkEvaluate', angularAMD.route({   //机房评测
                    url: '/homeworkEvaluate',
                    templateUrl: loadHtml('machineEvaluate/homeworkEvaluate.html'),
                    controllerUrl: 'machineEvaluate/homeworkEvaluate.js',
                    data: {
                        belong: 'stuHomeEvaluate'
                    }
                }))

                .state('myApp.manage.teacherHelpSelf', angularAMD.route({
                    url: '/teacherHelpSelf',
                    templateUrl: loadHtml('teacherHelpSelf/teacherHelpSelf.html'),
                    controllerUrl: 'teacherHelpSelf/teacherHelpSelf.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('outExamCode2', angularAMD.route({
                    url: '/outExamCode2',
                    templateUrl: loadHtml('userInfo/outExamCode2.html'),
                    controllerUrl: 'userInfo/outExamCode2.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('myApp.manage.outExamCode', angularAMD.route({
                    url: '/outExamCode',
                    templateUrl: loadHtml('userInfo/outExamCode.html'),
                    controllerUrl: 'userInfo/outExamCode.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('buildAnswerSheet', angularAMD.route({
                    url: '/buildAnswerSheet?examId&examFID&sheetId&isClear&isUnit&unifiedId',
                    templateUrl: loadHtml('buildAnswerSheet/buildAnswerSheet.html'),
                    controllerUrl: 'buildAnswerSheet/answerSheetNew.js',
                    data: {
                        belong: 'bank'
                    }
                }))
                .state('myApp.manage.sheetManage', angularAMD.route({
                    url: '/sheetManage',
                    templateUrl: loadHtml('sheetManage/sheetManage.html'),
                    controllerUrl: 'sheetManage/sheetManage.js',
                    data: {
                        belong: 'manage'
                    }
                }))
                .state('stuErrorBook', angularAMD.route({
                    url: '/stu-error-book?studentId',
                    templateUrl: loadHtml('errorBookDownload/errorBookDownload.html'),
                    controllerUrl: 'errorBookDownload/errorBookDownload.js',
                    data: {}
                }))
            //校长路由
                .state('myApp.masterHome', angularAMD.route({
                    url: '/master/home',
                    templateUrl: loadHtml('headmasterPort/new/home.html'),
                    controllerUrl: 'headmasterPort/new/home.js',
                    data: {
                        belong: 'home'
                    }
                }))
                .state('myApp.examCenter', angularAMD.route({
                    url: '/master/exam-center',
                    templateUrl: loadHtml('exam-center/examCenter.html'),
                    controllerUrl: 'exam-center/examCenter.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('myApp.newBlankPaper',angularAMD.route({
                    url:'/master/newblankpaper?unifiedId&unifiedName',
                    templateUrl:loadHtml('newBlankPaper/newBlankPaper.html'),
                    controllerUrl:'newBlankPaper/newBlankPaperController.js',
                    data:{
                        belong:'examCenter'
                    }
                }))
                .state('myApp.markPaperRecord',angularAMD.route({
                    url:'/master/markPaperRecord?unifiedid',
                    templateUrl:loadHtml('markPaperRecord/markPaperRecord.html'),
                    controllerUrl:'markPaperRecord/markPaperRecordController.js',
                    data:{
                        belong:'examCenter'
                    }
                }))
                .state('myApp.setValue', angularAMD.route({
                    url: '/master/setValue?unifiedId&status&action&Checked&marktype&examname',
                    templateUrl: loadHtml('setValue/setValue.html'),
                    controllerUrl: 'setValue/setValue.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('myApp.setPaper', angularAMD.route({
                    url: '/setPaper?unifiedId&isParent&action&submitNum&parenUnifiedId',
                    templateUrl: loadHtml('setPaper/setPaper.html'),
                    controllerUrl: 'setPaper/setPaper.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('myApp.submitRefer', angularAMD.route({
                    url: '/submitRefer?unifiedId&isParent&exName&subjectName&efid&submitNum',
                    templateUrl: loadHtml('submitRefer/submitRefer.html'),
                    controllerUrl: 'submitRefer/submitRefer.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('myApp.setReady', angularAMD.route({
                    url: '/setReady?unifiedId&examfid&marktype&gradeNum&status&action',
                    templateUrl: loadHtml('setRelated/setReady.html'),
                    controllerUrl: 'setRelated/setReady.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('myApp.setRelated', angularAMD.route({
                    url: '/setRelated?unifiedId&taskFid&marktype&examfid&gradeNum&status',
                    templateUrl: loadHtml('setRelated/setRelated.html'),
                    controllerUrl: 'setRelated/setRelated.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('abnormal', angularAMD.route({
                    url: '/abnormal?ExamFlnkID&TaskFId&UnifiedItemFid&gradeNum&marktype&status',
                    templateUrl: loadHtml('setRelated/abnormal.html'),
                    controllerUrl: 'setRelated/abnormal.js',
                    data: {
                        belong: 'examCenter'
                    }
                }))
                .state('myApp.encryptMarkConfig',angularAMD.route({
                    url:'/master/encryptMarkConfig?unifiedid&examfid&examname&time&marktype&subject&examid&gradenum&subjectid&submitnum&action&unifedname',
                    templateUrl:loadHtml('encryptMarkConfig/encryptMarkConfig.html'),
                    controllerUrl:'encryptMarkConfig/encryptMarkConfig.js',
                    data:{
                        belong:'examCenter'
                    }
                }))
                .state('myApp.doPaperCut',angularAMD.route({
                    url:'/master/doPaperCut?taskId&examId&action',
                    templateUrl:loadHtml('doPaperCut/doPaperCut.html'),
                    controllerUrl:'doPaperCut/doPaperCut.js',
                    data:{
                        belongL:'examCenter'
                    }
                }))
                .state('paperCutUnified',angularAMD.route({
                    url:'/paperCutUnified?taskId&examId&action&unifiedId',
                    templateUrl:loadHtml('doPaperCut/doPaperCutUnified.html'),
                    controllerUrl:'doPaperCut/doPaperCutUnified.js',
                    data:{
                        belongL:'examCenter'
                    }
                }))
                .state('tableManage_View', angularAMD.route({ // 统考报表
                    url: '/tableManage_View',
                    templateUrl: loadHtml('tableManage/tableManage_View.html'),
                    controllerUrl: 'tableManage/tableManage_View.js',
                    data:{
                        belong:'examCenter'
                    }
                }))
                .state('myApp.mainViewTool', angularAMD.route({ // 阅卷工具
                    url: '/mainViewTool?examFLnkId',
                    templateUrl: loadHtml('markingTools/mainViewTool/mainViewTool.html'),
                    controllerUrl: 'markingTools/mainViewTool/mainViewTool.js',
                    data:{
                        belong:'examCenter'
                    }
                }))
                .state('myApp.tableHomeworkAnalysis', angularAMD.route({ // 作业报表
                    url: '/tableHomeworkAnalysis',
                    templateUrl: loadHtml('tableManage/tableHomeworkAnalysis.html'),
                    controllerUrl: 'tableManage/tableHomeworkAnalysis.js',
                    data:{
                        belong:'tableHomeworkAnalysis'
                    }
                }))
                .state('myApp.t_s_manage', angularAMD.route({
                    url: '/s_manage',
                    templateUrl: loadHtml('manage/manage.html'),
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.t_s_manage.s_manageHome', angularAMD.route({
                    url: '/manageHome',
                    templateUrl: loadHtml('manage/manageHome.html'),
                    controllerUrl: 'manage/manageHome.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.t_s_manage.masterStuCtrl', angularAMD.route({
                    url: '/studentCtrl',
                    templateUrl: loadHtml('manage/student/studentCtrl.html'),
                    controllerUrl: 'manage/student/studentCtrl.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.t_s_manage.masterTeacherCtrl', angularAMD.route({
                    url: '/teacherCtrl',
                    templateUrl: loadHtml('manage/teacher/teacherCtrl.html'),
                    controllerUrl: 'manage/teacher/teacherCtrl.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.t_s_manage.masterExamQrCode', angularAMD.route({
                    url: '/examQrCode',
                    templateUrl: loadHtml('manage/examCode/examQrCode.html'),
                    controllerUrl: 'manage/examCode/examQrCode.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.t_s_manage.masterWxQrCode', angularAMD.route({
                    url: '/wxQrCode',
                    templateUrl: loadHtml('manage/wechatCode/wxQrCode.html'),
                    controllerUrl: 'manage/wechatCode/wxQrCode.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))

                .state('masterExamOutCode', angularAMD.route({
                    url: '/examOutCode',
                    templateUrl: loadHtml('manage/examCode/examOutCode.html'),
                    controllerUrl: 'manage/examCode/examOutCode.js',
                    data: {
                        belong: ''
                    }
                }))
                .state('myApp.masterWxOutCode', angularAMD.route({
                    url: '/wxOutCode',
                    templateUrl: loadHtml('manage/wechatCode/wxOutCode.html'),
                    controllerUrl: 'manage/wechatCode/wxOutCode.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.edit', angularAMD.route({
                    url: '/editTeacher',
                    templateUrl: loadHtml('manage/edit/editTeacher.html'),
                    controllerUrl: 'manage/edit/editTeacher.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.editStudent', angularAMD.route({
                    url: '/editStudent',
                    templateUrl: loadHtml('manage/edit/editStudent.html'),
                    controllerUrl: 'manage/edit/editStudent.js',
                    data: {
                        belong: 't_s_manage'
                    }
                }))
                .state('myApp.reportSheet', angularAMD.route({
                    url: '/reportSheet',
                    templateUrl: loadHtml('report/report.html'),
					controllerUrl: 'report/report.js',
					data: {
						belong: 'report'
					}
                }))
                .state('myApp.testPage', angularAMD.route({
                    url: '/app-test',
                    templateUrl: loadHtml('test-page/test-page.html'),
                    controllerUrl: 'test-page/test-page.js'
                }))
            ;
            $urlRouterProvider.when('', assignRouter.assignInject).when('/', assignRouter.assignInject);
            $urlRouterProvider.otherwise('/login');
            $httpProvider.defaults.withCredentials = true;
            // 拦截 用户 session 失效的响应，跳转到登陆页面
            $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {
                return {
                    response: function (response) {
                        var url = response.config.url;
                        var data = response.data;

                        // 排除登陆接口返回 code = 1 的场景
                        if ((url.indexOf('Interface0001.ashx') === -1 && data.code === 1)) {
                            window.sessionStorage.removeItem('currentUser');
                            $rootScope.$broadcast('SESSION_TIMEOUT');
                            return response;
                        } else {
                            return response;
                        }
                    }
                };
            }]);
            $logProvider.debugEnabled(true);
        }
    ];
});