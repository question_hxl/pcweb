/**
 * 部分配置与业务分离；
 * 固定配置，本地开发调试不需要上传代码库
 * 将作为angular constant服务引入
 */
define(function(){
	return {
		'baseUrl': '/BootStrap/Interface',
		'resourceUrl': 'http://api.51jyfw.com/api',
		'managerUrl':'http://51admin.lyclass.com',
		'transferServer': 'http://api10.51jyfw.com'
	};
});