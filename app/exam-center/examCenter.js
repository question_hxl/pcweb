/**
 * Created by 贺小雷 on 2017/2/22.
 */
define(['jquery.datetimepicker'],function () {
    var STEPS = [{
        stepId: 1,
        stepName: '设置分值'
    },{
        stepId: 2,
        stepName: '生成答题卡'
    },{
        stepId: 3,
        stepName: '阅卷管理'
    },{
        stepId: 4,
        stepName: '试卷纠错'
    },{
        stepId: 5,
        stepName: '提交分析'
    },{
        stepId: 6,
        stepName: '查看报表'
    },{
        stepId: 7,
        stepName: '微信通知'
    }];
    return ['$scope', '$state', '$q', '$cookieStore', 'bes','ngDialog', 'constantService', 'resourceUrl', '$cookies', 'managerUrl', 'Upload', 'apiCommon',
        function ($scope, $state, $q,$cookieStore, bes, ngDialog, constantService, resourceUrl, $cookies, managerUrl, Upload, apiCommon) {
        var user = JSON.parse(sessionStorage.getItem('currentUser'));
        var userClassId;
        if(user.classs && user.classs[0]) {
            userClassId = user.classs[0].FLnkID;
        }
        $scope.steps = STEPS;
        $scope.isSearch = false;
        $scope.checkedExams = [];
        var originalExamList = [];
        function getMyUnifiedList() {
            bes.send('/BootStrap/schoolmanager/getMyUnifieds.ashx',{}, {
                method: 'post'
            }).then(function (res) {
                if (_.isArray(res.data.msg)) {
                    $scope.examList = res.data.msg;
                    originalExamList = res.data.msg;
                    getMyGradeList();
                    //线下誊分模式，无阅卷管理，强行将状态转换成4
                    _.each($scope.examList,function (items) {
                        _.each(items.unitTasks, function (item) {
                            if(item.MarkMode !== 1 && item.status === 3){
                                apiCommon.updateUnifiedItemStep({//设置子统考
                                    unifiedItemId: item.FLnkId,
                                    step: 4
                                }).then(function (res) {
                                    if (res.data.code === 0) {
                                        item.status = 4;
                                    } else {
                                        constantService.alert(res.data.msg);
                                    }
                                });
                            }
                            if(item.GoodNum === null){
                                item.GoodNum = 0;
                            }
                            if(items.unitTasks.length === 1){
                                if(item.SubmitNum > 0 && ((+item.MarkMode === 1 &&item.status >= 3) ||(+item.MarkMode !== 1 &&item.status >= 4))){
                                    getDaijiucuoPaper(item);
                                }
                            }
                        })
                    });
                } else {
                    $scope.examList = [];
                }
            }, function () {
                $scope.examList = [];
            });
        }
        //获取带纠错试卷
        function getDaijiucuoPaper(untask) {
            var papaerList = [];
            var noRelatedList = [];
            apiCommon.generalQuery({
                UnifiedItemFId:untask.FLnkId,
                Proc_name :'Proc_GetUnifiedTasks'
            }).success(function (res) {
                if(res.msg.length === 0){
                    untask.noRelatedPaper = 0;
                    untask.currentStuList =[];
                }else{
                    $scope.classData = res.msg;
                    $scope.TaskFId = res.msg[0].FLnkID;
                    if(untask.MarkMode != 1){
                        getAbnormalPaper(untask);
                    }else{
                        untask.currentStuList =[];
                    }
                    getUnbindPapger( $scope.TaskFId).then(function (resq) {
                        papaerList = resq.data.msg;
                        if(_.isArray(papaerList)){
                            _.each(papaerList,function (item) {
                                if(item.isNoRelated === 0 || item.isNoRelated === 2){
                                    noRelatedList.push(item);
                                }
                            });
                            untask.noRelatedPaper = noRelatedList.length;
                        }else {
                            untask.noRelatedPaper = 0;
                        }

                    });
                }
            });
        }
        //获取题目异常试卷
        function getAbnormalPaper(examfid) {
            var newFidList = [];
            _.each($scope.classData, function (item) {
                newFidList.push(getAbnormalPapers(item,examfid.ExamFId));
            });
            if(newFidList.length> 0){
                examfid.currentStuList =[];
                $q.all(newFidList).then(function (resq) {
                    _.each(resq,function (res) {
                        _.each(res.data.studentList,function (data) {
                            if(data.type == -1){
                                examfid.currentStuList.push(data);
                            }
                        })
                    });
                });
            }
        }
        //根据学段初始化年级
         function getMyGradeList() {
             $scope.gradeList = [];
             $scope.currentGradeId = [];
             apiCommon.generalQuery({
                 Proc_name: 'getSchoolGrade',
                 schoolfId: user.schoolFId
             }).then(function (res) {
                 if(_.isArray(res.data.msg) && res.data.msg.length> 0){
                     var allGrade = {'GradeNum':'','gradeName':'全部','gradeId':0};
                     $scope.gradeList = res.data.msg;
                     $scope.gradeList.unshift(allGrade);
                     $scope.currentGradeId = $scope.gradeList[0].GradeNum;
                 }
             },function () {
                 constantService.alert('年级获取失败')
             })
         }
        getMyUnifiedList();

        //时间选择
        $('#start_datetimepicker').datetimepicker({
            lang: "ch",
            timepicker: false,
            format: "Y-m-d",
            todayButton: false,
            scrollInput: false,
            onChangeDateTime: function (dp, $input) {
                $('#start_datetimepicker').datetimepicker('hide');
            }
        });

        $('#end_datetimepicker').datetimepicker({
            lang: "ch",
            timepicker: false,
            format: "Y-m-d",
            todayButton: false,
            scrollInput: false,
            onChangeDateTime: function (dp, $input) {
                $('#end_datetimepicker').datetimepicker('hide');
            }
        });

        function getTodayDate() {
            var now = new Date();
            var year = now.getFullYear(),
                month = now.getMonth() + 1,
                day = now.getDate();
            month = month < 10 ? ('0' + month) : month;
            day = day < 10 ? ('0' + day) : day;
            return year + '-' + month + '-' + day;
        }
        // 点击搜索
        $scope.search = function () {
            var endTime = $('#end_datetimepicker').val(),
                startTime = $('#start_datetimepicker').val();
            if (startTime && endTime && endTime < startTime) {
                constantService.alert('结束时间需大于开始时间');
            }else if(startTime ==='' && endTime === ''){
                if($scope.currentGradeId === ''){
                    $scope.examList = originalExamList
                }else{
                    $scope.examList = _.filter(originalExamList,function (item) {
                        return item.GradeNum == $scope.currentGradeId;
                    })
                }
                if($scope.examList.length === 0){
                    $scope.isSearch = true;
                }else{
                    $scope.isSearch = false;
                }
            }else if(startTime ==='' && endTime){
                if($scope.currentGradeId === ''){
                    $scope.examList = _.filter(originalExamList,function (item) {
                        var dataTime = item.unifiedDate.slice(0,10);
                        return dataTime<= endTime;
                    })
                }else {
                    $scope.examList = _.filter(originalExamList,function (item) {
                        var dataTime = item.unifiedDate.slice(0,10);
                        return item.GradeNum == $scope.currentGradeId && dataTime<= endTime;
                    })
                }
                if($scope.examList.length === 0){
                    $scope.isSearch = true;
                }else{
                    $scope.isSearch = false;
                }
            }else{
                if(endTime === ''){
                    endTime = getTodayDate();
                }
                if($scope.currentGradeId === ''){
                    $scope.examList = _.filter(originalExamList,function (item) {
                        var dataTime = item.unifiedDate.slice(0,10);
                        return dataTime<= endTime && dataTime>= startTime;
                    })
                }else {
                    $scope.examList = _.filter(originalExamList,function (item) {
                        var dataTime = item.unifiedDate.slice(0,10);
                        return item.GradeNum == $scope.currentGradeId && dataTime<= endTime && dataTime >= startTime;
                    })
                }
                if($scope.examList.length === 0){
                    $scope.isSearch = true;
                }else{
                    $scope.isSearch = false;
                }
            }
        };

        $scope.getUnitTask = function (task,exam) {
            task.isActive = !task.isActive;
            if(task.SubmitNum > 0 && task.status >= 4 && task.isActive){
                getDaijiucuoPaper(task);
            }
        };
        $scope.checkExam = function(exam){
            var index = _.findIndex($scope.checkedExams, function(item){
                return item.unifiedId === exam.unifiedId;
            });
            if(index !== -1) {
                $scope.checkedExams.splice(index, 1);
            } else {
                $scope.checkedExams.push(exam);
            }
        };

        $scope.isExamChecked = function (exam) {
            return !!_.find($scope.checkedExams, function(item){
                return item.unifiedId === exam.unifiedId;
            });
        };

        $scope.cancelUnified = function (exam) {//取消统考
                $scope.cancelUnifiedList = [];
                $scope.cancelUnifiedData = [];
                constantService.confirm('温馨提示', '确定取消本次统考', ['确定', '取消'], function () {
                    $scope.period = exam.unitTasks[0].Period;
                    _.each(exam.unitTasks, function (item) {
                        $scope.cancelUnifiedList = {
                            Name: item.Name,
                            SchoolFId: user.schoolFId,
                            CreateuUserFId: user.fid,
                            ExamTime: item.ExamTime,
                            Period: $scope.period,
                            GradeId: item.GradeNum,
                            Diff: item.Diff,
                            ExamType: item.ExamType,
                            Status: 1,
                            kmUnifiedItemParam: [{FLnkId: item.FLnkId}]
                        };
                        $scope.cancelUnifiedData.push($scope.cancelUnifiedList)
                    });
                    var cancelUnifiedLists = {
                        kmUnifiedBaseParam: $scope.cancelUnifiedData,
                        oldParentFLnkIds: [exam.unifiedId]
                    };
                    apiCommon.SplitPaKmUnifiedItem(cancelUnifiedLists).then(function (res) {
                        if (res.data.Flag) {
                            constantService.alert(res.data.Message);
                            getMyUnifiedList()
                        } else {
                            constantService.alert(res.data.Message);
                        }
                        ngDialog.close();
                        $scope.checkedExams = [];
                    })
                });
            };

        $scope.createExam = function(){
            $state.go('myApp.setPaper');
        };

        $scope.addExam = function(e, exam, isParent){
            e.stopPropagation();
            $state.go('myApp.setPaper', {
                unifiedId: exam.unifiedId,
                isParent: isParent,
                action: 'ADD'
            });
        };

        $scope.setEncryptMark = function(){
            //todo 对选中的考试设置加密批阅
        };

        $scope.setUnified = function(){
            $scope.checkSubjectId = [];
            $scope.checkGradeNum = [];
            var isSubjectIdTrue = false,
            isGradeNumTrue = false;
            _.each($scope.checkedExams,function (item) {
                _.each(item.unitTasks,function (items) {
                    $scope.checkSubjectId.push(items.SubjectId);
                    $scope.checkGradeNum.push(items.GradeNum);
                });
            });
            // var s = $scope.checkSubjectId.join(",")+",";
            var s = _.uniq($scope.checkSubjectId);
            var ss = $scope.checkGradeNum.join(",")+",";
            // for(var i=0;i<$scope.checkSubjectId.length;i++) {
            //     if(s.replace($scope.checkSubjectId[i]+",","").indexOf($scope.checkSubjectId[i]+",")>-1) {
            //         isSubjectIdTrue = true;
            //         break;
            //     }
            // }
            if(s.length !== $scope.checkSubjectId.length){
                isSubjectIdTrue = true;
            }
            for(var j=0;j<$scope.checkGradeNum.length;j++) {
                if(ss.replace($scope.checkGradeNum[j]+",","").indexOf($scope.checkGradeNum[j]+",") === -1) {
                    isGradeNumTrue = true;
                    break;
                }
            }
            if($scope.checkedExams && $scope.checkedExams.length>1){
                if(isSubjectIdTrue){
                    constantService.alert("所选考试学科不可以重复！");
                }else{
                    if(isGradeNumTrue){
                        constantService.alert("请勿跨年级合并！");
                    }else{
                        $scope.examName = '';
                        conbineUnified();
                    }
                }
            }else{
                constantService.alert("请勾选两个及两个以上的考试！");
            }
        };

        function conbineUnified() {
                ngDialog.open({
                    template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div class="nextMarking-choess">'+
                    '<div style="width: 100%;text-align: center;height: 50px;line-height: 50px">确认选中考试为需要合并的考试信息</div>'+
                    '<div style="width: 100%;text-align: center;height: 50px;line-height: 50px">设置统考名称：<input type="text" ng-model="$parent.examName"></div>'+
                    '<div class="nextMarking-btn" style="margin:0 40px;color: #02bb99;background-color: #fff;border: 1px solid #02bb99;" ng-click="closeThisDialog()">取消</div>'+
                    '<div class="nextMarking-btn" ng-click="setUnifiedSure()">确认</div>'+
                    '</div>'+'</div>',
                    showClose: false,
                    className: 'ngdialog-theme-default ngdialog-related',
                    scope: $scope,
                    closeByDocument: false,
                    plain: true
                });
                $scope.period = $scope.checkedExams[0].Period;

                $scope.setUnifiedSure = function () {
                    if(!$scope.examName){
                        constantService.alert('请输入合并后的统考名称!')
                    }else{
                        var setUnifiedList = {
                            kmUnifiedBaseParam:[{
                                Name:$scope.examName,
                                SchoolFId:user.schoolFId,
                                CreateuUserFId:user.fid,
                                ExamTime:$scope.checkedExams[0].unitTasks[0].ExamTime,
                                Period: $scope.period,
                                GradeId:$scope.checkedExams[0].unitTasks[0].GradeNum,
                                Diff:$scope.checkedExams[0].unitTasks[0].Diff,
                                ExamType:$scope.checkedExams[0].ExamType,
                                Status:1,
                                kmUnifiedItemParam:[]
                            }],
                            oldParentFLnkIds:[]
                        };
                        _.each($scope.checkedExams,function (res) {
                            _.each(res.unitTasks,function (resq) {
                                var FLnkId = {FLnkId:resq.FLnkId};
                                setUnifiedList.kmUnifiedBaseParam[0].kmUnifiedItemParam.push(FLnkId);
                                setUnifiedList.oldParentFLnkIds.push(resq.UnifiedFId)
                            });
                        });
                        apiCommon.MergePaKmUnifiedItem(setUnifiedList).then(function (res) {
                            if(res.data.Flag){
                                $scope.checkedExams = [];
                                constantService.alert(res.data.Message);
                                getMyUnifiedList()
                            }else{
                                constantService.alert(res.data.Message);
                            }
                            ngDialog.close();
                        })
                    }
                }
            }

        $scope.viewUnifiedReports = function(exam){
            var unifWindow = window.open('_blank');
            $scope.subjectNames = [];
            var examId = [];
            _.each(exam.unitTasks, function (item) {
                $scope.subjectNames.push(item.subjectName);
                examId.push(item.ExamFId);
            });
			examId = examId.join(',');
            var keys = getSelectPaper();
            var period = exam.Period;
            unifWindow.location = '#/tableManage_View?paperId=' + examId + '&schoolId=' + user.schoolFId + '&period=' + period +  '&unifiedId=' + exam.unifiedId +
                + '&pharseId=' + user.pharseId + '&nianjiId=' + exam.GradeNum + '&subjectNames=' + (keys.join(',')) + '&isOpenNewWin=1&examName='
                + encodeURIComponent(exam.unifiedName);
            //setStepRequest(exam.FLnkId,'7');
            //getMyUnifiedList();
        };

        $scope.editExam = function(e, exam, isParent){
            // 跳转编辑考试页面
            e.stopPropagation();
            if((!!exam.unitTasks ? exam.unitTasks.length === 1 : true)){
                $state.go('myApp.setPaper', {
                    unifiedId: (!!exam.unitTasks ? exam.unitTasks[0].FLnkId : exam.FLnkId),
                    isParent: isParent,
                    action: 'MODIFY',
                    submitNum: (!!exam.unitTasks ? exam.unitTasks[0].SubmitNum : exam.SubmitNum) || '',
                    parenUnifiedId:exam.UnifiedFId
                });
            }else{
                getExamTypes(exam.ExamType);
                $scope.unifedname =  exam.unifiedName;
                constantService.modal({
                    title: '编辑综合统考名称',
                    scope: $scope,
                    type: 'confirm',
                    template: '<div class="unifedname_edit">' +
                    '<div class="unifedname_input" style="padding: 10px;">' +
                    '<span style="margin-right: 1rem;">综合统考名称:</span>' +
                    '<input style="width: 380px;height: 40px;text-indent: 5px;" ng-model="unifedname" type="text">' +
                    '</div>' +
                    '<div class="unifedname_input" style="padding: 10px;">' +
                    '<span style="margin-right: 1rem;">考&nbsp;&nbsp;试&nbsp;&nbsp;&nbsp;类&nbsp;&nbsp;型:</span>' +
                    '<select style="width: 12rem;height: 40px;text-indent: 5px;" ng-model="examType" ng-options="examType.ExType for examType in examTypeList"></select>' +
                    '</div></div>',
                    okFn: function(){
                        $scope.checkIllegal = function() {
                            $scope.unifedname = trimStr($scope.$$childTail.unifedname);
                            if(!($scope.unifedname.length >0 && $scope.unifedname.length < 50)){
                                constantService.alert('请输入合理的名称，且长度不能超过50个字!');
                                return false;
                            }else{
                                return true;
                            }
                        };
                        function trimStr(str){return str.replace(/(^\s*)|(\s*$)/g,"");}
                        if($scope.checkIllegal()){
                            apiCommon.modifyIntegrateUnifedName({
                                unifiedId:exam.unifiedId,
                                unifedName:$scope.unifedname,
                                examType:$scope.$$childTail.examType.ExType
                            }).then(function (res) {
                                if(+res.data.code === 0){
                                    _.each($scope.examList,function (item) {
                                        if(item.unifiedId === exam.unifiedId && item.unitTasks.length > 1){
                                            item.unifiedName = $scope.unifedname;
                                            item.ExamType = $scope.$$childTail.examType.ExType;
                                        }
                                    });
                                    ngDialog.closeAll();
                                }
                            },function (res) {
                                constantService.alert('修改失败，请稍后再试!')
                            });
                        }
                    }
                });
            }
        };
        //初始化试卷类型
         function getExamTypes(type) {
             $scope.examType = '';
             return apiCommon.examType().then(function (res) {
                 if (_.isArray(res.data.msg)) {
                     $scope.examTypeList = res.data.msg;
                     if (type) {
                         $scope.examType = _.find($scope.examTypeList, function (res) {
                             return res.ExType === type;
                         });
                     } else {
                         $scope.examType = $scope.examTypeList[4];
                     }
                 } else {
                     $scope.examTypeList = [];
                     $scope.examType = null;
                 }
             });
         }

        $scope.submitRefer = function(e, exam, isParent){
            e.stopPropagation();
            $state.go('myApp.submitRefer', {
                unifiedId: exam.FLnkId,
                exName : exam.ExName,
                isParent: isParent,
                subjectName : exam.subjectName,
                efid : exam.efid,
                submitNum : exam.SubmitNum || ''
            });
        };

        $scope.checkTools = function(e, exam){
            //todo 跳转批阅工具箱页面
            window.open("#/mainViewTool?examFLnkId=" + exam.FLnkId + '&isOpenNewWin=' + 1);
        };

        $scope.delExam = function (e, exam,group,index) {
            $scope.isDel = true;
            if(exam.unitTasks){
                for(var i = 0;i<exam.unitTasks.length;i++){
                    if(exam.unitTasks[i].SubmitNum >0){
                        $scope.isDel = true;
                        break;
                    }else{
                        $scope.isDel = false;
                    }
                }
            }else{
                if(exam.SubmitNum > 0){
                    $scope.isDel = true;
                }else{
                    $scope.isDel = false;
                }
            }
            if($scope.isDel){
                constantService.confirm('温馨提示', '有提交数据无法删除', ['确定', '取消'], function () {
                    delSure(e, exam,group,index);
                },function () {
                    ngDialog.closeAll();
                });
            }else{
                constantService.confirm('温馨提示', '是否删除该考试记录', ['确定', '取消'], function () {
                    delSure(e, exam,group,index);
                },function () {
                    ngDialog.closeAll();
                });
            }
        };
        function delSure(e, exam,group,index) {
            if(!$scope.isDel){
                if (exam.unitTasks){  //删除父统考及所有子统考信息
                   apiCommon.DeleteKmUnifiedByFLnkId([exam.unifiedId]).then(function (resp) {
                        if (resp.data.Flag){
                            constantService.alert(resp.data.Message);
                            $scope.examList.splice(index,1);
                            ngDialog.close();
                        }else {
                            constantService.alert(resp.data.Message);
                        }
                    });
                }else {   //删除子统考信息
                    apiCommon.DeleteKmUnifiedItemByFLnkId([exam.FLnkId]).then(function (resp) {
                        if (resp.data.Flag){
                            constantService.alert(resp.data.Message);
                            group.unitTasks.splice(index,1);
                            ngDialog.close();
                        }else {
                            constantService.alert(resp.data.Message);
                        }
                    });
                }
            }else{
                ngDialog.close()
            }
        }
        $scope.goMarking = function(exam){
            // if(exam.MarkMode === 1){//线上批阅
            //     window.open('#/encryptmarking?examId='+exam.ExamFId+'&unifiedId='+exam.FLnkId);
            // }
        };

        $scope.goStep = function(step,exam){
            if(step.stepId === 6 && +exam.status > 5){
                var wyWindow = window.open('_blank');
            }
            // 处理对应的步骤
            //action 1 正常跳步 2 从其他步骤跳转进入
            checkStatus(step.stepId, exam).then(function(res){
                if(exam.status === null){
                    exam.status = 1;
                }
                switch (step.stepId){
                    case 1:
                        $state.go('myApp.setValue',{
                            unifiedId:exam.FLnkId,
                            status:exam.status,
                            action:!!res?res.action:'',
                            Checked:exam.Checked,
                            marktype:exam.MarkMode,
                            examname:exam.ExName
                        });
                        break;
                    case 2:
                        $state.go('myApp.paperBankSheet', {
                            ExName: exam.Name || exam.ExName,
                            FID: exam.efid,
                            FLnkID: exam.ExamFId,
                            CreateDate: exam.CreateDate,
                            unifiedId: exam.FLnkId,
                            action:!!res?res.action:'1',
                            MarkMode: exam.MarkMode,
                            examname:exam.ExName
                        });
                        break;
                    case 3:
                        $state.go('myApp.encryptMarkConfig',{
                            unifiedid:exam.FLnkId,
                            examfid:exam.ExamFId,
                            examname:exam.ExName,
                            time:exam.ExamTime,
                            marktype:exam.MarkMode,
                            subject:exam.subjectName,
                            examid:exam.efid,
                            gradenum:exam.GradeNum,
                            subjectid:exam.SubjectId,
                            submitnum:exam.SubmitNum || '',
                            action:!!res?res.action:'',
                            unifedname:exam.Name || ''
                        });
                        break;
                    case 4:
                        if(+exam.status < 6) {
                            if( +exam.status !== 5 && (exam.noRelatedPaper || (exam.currentStuList && exam.currentStuList.length))){//带纠错试卷大于0，进纠错页面
                                apiCommon.generalQuery({
                                    UnifiedItemFId: exam.FLnkId,
                                    Proc_name: 'Proc_GetUnifiedTasks'
                                }).success(function (res) {
                                    if(res.msg.length){
                                        var TaskFId = res.msg[0].FLnkID;
                                        $state.go('myApp.setRelated', {
                                            unifiedId: exam.FLnkId,
                                            taskFid: TaskFId,
                                            marktype: exam.MarkMode,
                                            examfid: exam.ExamFId,
                                            gradeNum: exam.GradeNum,
                                            status: exam.status
                                        });
                                    }
                                });
                            }else{
                                $scope.currentPaper = exam;
                                if(exam.MarkMode == 1){//线上批阅直接给提示
                                    goNextStep(exam);
                                }else{//线下批阅，先获取题目异常的试卷
                                    var newFidList = [];
                                    _.each($scope.classData, function (item) {
                                        newFidList.push(getAbnormalPapers(item,exam.ExamFId));
                                    });
                                    if(newFidList.length> 0){
                                        exam.currentStuList =[];
                                        $q.all(newFidList).then(function (resq) {
                                            _.each(resq,function (res) {
                                                _.each(res.data.studentList,function (data) {
                                                    if(data.type == -1){
                                                        exam.currentStuList.push(data);
                                                    }
                                                })
                                            });
                                            if(exam.currentStuList.length > 0){//有题目异常的，跳转
                                                $state.go('abnormal',{
                                                    ExamFlnkID:exam.ExamFId,
                                                    TaskFId:$scope.TaskFId,
                                                    UnifiedItemFid:exam.FLnkId,
                                                    gradeNum:exam.GradeNum,
                                                    marktype:exam.MarkMode,
                                                    status:exam.status
                                                });
                                            }else{//没有给提示
                                                goNextStep(exam);
                                            }
                                        });
                                    }
                                }
                            }
                        } else {
                            constantService.alert('请先取消分析!');
                        }
                    /**
                     * 获取带纠错试卷及判断题目异常卷
                     */
                        function goNextStep(exam){

                            ngDialog.open({
                                template:'<div class="nextMarking">'+' <div class="nextMarking-nav"></div>'+' <div style="padding: 30px 20px 10px;text-align: center;">'+
                                '<div class="my-data" style="padding-bottom: 30px;">暂无需要关联<span ng-show="+exam.MarkMode !== 1">及题目异常</span>的试卷，请直接点击下一步 <span ng-show="+exam.status >= 6">重新进行纠错可能会取消分析!</span></div>'+
                                '<div class="btn" style="margin:0 15px;color: #02bb99;background-color: #fff;border: 1px solid #02bb99;width: 100px;" ng-click="cancelFn()">'+ (+exam.status < 5 ? '取消' : '继续上传') +'</div>'+
                                '<div class="btn" style="margin:0 15px;color: #fff;background-color: #02bb99;border: 1px solid #02bb99;width: 100px;" ng-click="okFnForAll()">进入纠错</div>'+
                                '<div class="btn" style="margin:0 15px;color: #fff;background-color: #02bb99;border: 1px solid #02bb99;width: 100px;" ng-click="okFn()">下一步</div>'+
                                '</div>'+'</div>',
                                showClose: true,
                                className: 'ngdialog-theme-default ngdialog-related',
                                scope: $scope,
                                plain: true
                            });
                            $scope.cancelFn = function () {
                                if(+exam.status < 5) {
                                    ngDialog.closeAll();
                                } else {
                                    $scope.goNext(4);
                                }
                            };
                            $scope.okFn = function () {
                                $scope.goNext(5);
                            };
                            $scope.okFnForAll = function () {
                                ngDialog.closeAll();
                                apiCommon.generalQuery({
                                    UnifiedItemFId: exam.FLnkId,
                                    Proc_name: 'Proc_GetUnifiedTasks'
                                }).success(function (res) {
                                    if(res.msg.length){
                                        var TaskFId = res.msg[0].FLnkID;
                                        $state.go('myApp.setRelated', {
                                            unifiedId: exam.FLnkId,
                                            taskFid: TaskFId,
                                            marktype: exam.MarkMode,
                                            examfid: exam.ExamFId,
                                            gradeNum: exam.GradeNum,
                                            status: exam.status
                                        });
                                    }
                                });
                            };
                        }
                        $scope.goNext = function (step) {
                            apiCommon.updateUnifiedItemStep({//设置子统考
                                unifiedItemId: exam.FLnkId,
                                step: step
                            }).then(function (res) {
                                if (res.data.code === 0) {
                                    var message = +step === 5 ? '纠错完成!': '可以上传了!';
                                    constantService.showSimpleToast(message);
                                    $scope.currentPaper.status = +step;
                                    setPaperStatus($scope.currentPaper);
                                } else {
                                    constantService.alert(res.data.msg);
                                }
                            });
                            ngDialog.close();
                        };
                        break;
                    case 5:
                        if(+exam.status > 5){
                            setStepRequest(exam.FLnkId,'5').then(function (res) {
                                exam.status = 5;
                                setPaperStatus(exam);
                            },function (res) {
                                constantService.alert('取消分析失败，请稍后再试!');
                            });
                            break;
                        }else{
                            getClassList5(exam,exam.Period);
                            break;
                        }
                    case 6:
                        wyWindow.location = managerUrl + '/class-grid/index/index_new.html#' + '/?schoolId=' + user.schoolFId + '&paperId=' +
                            exam.ExamFId + '&session=' + exam.Period + '&isOpenNewWin=1' + '&unifiedItemId=' + exam.FLnkId + '&isHalf=' + exam.ExamFactor
                            + (userClassId ? '&classId=' + userClassId : '');
                        if(+exam.status === 6){
                            setStepRequest(exam.FLnkId,'7').then(function (res) {
                                exam.status = 7;
                                setPaperStatus(exam);
                            });
                        }
                        //getMyUnifiedList();
                        break;
                    case 7:
                        getClassList6(exam, exam.Period);
                        break;
                    default:
                        break;
                }
            });

            /*if(step.stepId === 1){
                $state.go('myApp.setValue',{
                    unifiedId:exam.FLnkId
                });
            }else if(step.stepId === 2) {
                $state.go('myApp.paperBankSheet', {
                    ExName: exam.Name,
                    FID: exam.efid,
                    FLnkID: exam.FLnkId,
                    CreateDate: exam.CreateDate
                });
            }*/
        };
        //点击某一步骤时，检查是否可以执行该操作
        function checkStatus(step, exam){
            var defer = $q.defer();
            getSonUnifiedItem(exam.FLnkId).then(function (res) {
                var tempUnifiedItem = _.find(res.data.msg,function (item) {
                    return exam.FLnkId === item.FLnkId;
                });
                var isNeedUpdateState = false;
                if((exam.status === 3 && step === 4) || (exam.status === 6 && (step === 7 || step === 6))){
                    isNeedUpdateState = true;
                }
                exam.status = tempUnifiedItem.Status;
                if(isNeedUpdateState){
                    setPaperStatus(exam);
                }
                if(exam.status === null){
                    exam.status = 1;
                }
                switch (step){
                    /**  设置分值,需要检查
                     1.当前答题卡是否生成,如果生成，提示设置可能要重新生成答题卡
                     2.可能要重新分配批阅人
                     3.如果已分析，则不可以进行操作
                     */
                    case 1:
                        if(+exam.status > 2 && +exam.status <= 5 && +exam.MarkMode === 1 && exam.SubmitNum) {
                            constantService.confirm('提示', '修改试卷可能会清空批阅配置或者非选择题结果，是否继续?',function(){
                                defer.resolve();
                            }, function(){
                                defer.reject();
                            })
                        }else if(+exam.status > 5) {
                            constantService.confirm('提示', '试卷已形成分析，重新设置试卷请先取消分析！',function(){
                                defer.resolve();
                            }, function(){
                                defer.reject();
                            })
                        }else if(+exam.stat === 1){
                            defer.resolve({action:1});
                        }else{
                            defer.resolve();
                        }
                        break;
                    /** 生成答题卡，需要检查
                     * 1.是否已设置分值（进行第一步）,没有则不允许操作
                     * 2.是否提交分析，是则不允许操作
                     */
                    case 2:
                        /*if(+exam.status === 1) {
                            constantService.alert('您需要先设置分值才可以生成答题卡，请先设置分值！');
                            defer.reject();
                        }/!*else if(+exam.status >= 4 && +exam.status < 6) {
                            constantService.alert('本场考试在批阅中，无法继续操作！');
                            defer.reject();
                        }else if(+exam.status >= 6){
                            constantService.alert('本场考试已经提交分析，无法继续操作！');
                            defer.reject();
                        }*!/else {*/
                        //当考试状态为1的时候 可直接进入2 并且自动将状态置为2
                        /*if(+exam.status === 1){
                            setStepRequest(exam.FLnkId,'2').then(function (res) {
                                if(res.data.code === 0){
                                    exam.status = 2;
                                    setPaperStatus(exam);
                                    defer.resolve({action:1});
                                }else{
                                    constantService.alert('操作失败请进入设置分值保存后操作此步骤，或者稍后再试!');
                                    defer.reject();
                                }
                            },function () {
                                constantService.alert('操作失败请进入设置分值保存后操作此步骤，或者稍后再试!');
                                defer.reject();
                            });
                        }else */if(+exam.status === 2){
                            defer.resolve({action:1});
                        }else{
                            defer.resolve();
                        }
                        /*}*/
                        break;
                    /**  阅卷管理，需要检查
                     * 1.是否完成设置分值（第一步），否则不允许操作
                     * 2.是否提交分析，是则不允许操作
                     */
                    case 3:
                        if(+exam.status === 1) {
                            constantService.alert('您需要先设置分值才可以设置阅卷配置，请先设置分值！');
                            defer.reject();
                        }else if(+exam.status === 2) {
                            defer.reject();
                        }else /*if(+exam.status > 5) {
                            constantService.alert('本场考试已经提交分析，无法继续操作！');
                            defer.reject();
                        }else */if(+exam.status === 3 ){
                            defer.resolve({action:1});
                        }else{
                            defer.resolve();
                        }
                        break;
                    /**  试卷纠错
                     * 1.当前步骤是否超过答题卡生成，否则不允许操作
                     * 2.是否提交分析，是则不允许操作
                     */
                    case 4:
                        if(+exam.status <= 2) {
                            defer.reject();
                        // }else if(+exam.status > 5) {
                            // constantService.confirm('提示','重新进行纠错可能会取消分析！',function(){
                            //     defer.resolve();
                            // }, function(){
                            //     defer.reject();
                            // });
                        }else if(+exam.status === 4 && +exam.MarkMode === 1){
                            defer.resolve({action:1});
                        }else if((+exam.status === 3 || +exam.status === 4) && !exam.SubmitNum){
                            constantService.alert('请先上传学生答卷!');
                            defer.reject();
                        }else{
                            defer.resolve();
                        }
                        break;
                    /** 提交分析
                     * 1.是否当前步骤为4，并且错误为0，否则如果步骤<3，或者错误>0，提示是否强行提交
                     * 2.已提交分析的不允许再次提交
                     */
                    case 5:
                        if(+exam.status === 5) {
                            defer.resolve({action:1});
                        }else /*if(+exam.status === 3) {
                            constantService.confirm('提示', '检查到您还没有设置阅卷配置，是否需要提交分析？', function(){
                                defer.resolve();
                            }, function(){
                                defer.reject();
                            });
                        }else*/ if(+exam.errorCount > 0) {
                            constantService.confirm('提示', '检查到您还有试卷存在错误，是否需要提交分析？', function(){
                                defer.resolve();
                            }, function(){
                                defer.reject();
                            });
                        }else if(+exam.status > 5) {
                            constantService.confirm('提示','是否要取消分析!',function () {
                                defer.resolve();
                            },function () {
                                defer.reject();
                            });
                        }else {
                            defer.reject();
                        }
                        break;
                    /**  查看报表
                     * 1.是否已提交分析
                     */
                    case 6:
                        if(+exam.status === 6){
                            defer.resolve({action:1});
                        }else if(+exam.status > 5) {
                            defer.resolve();
                        }else {
                            defer.reject();
                        }
                        break;
                    /**  发送微信
                     * 1.是否提交分析
                     */
                    case 7:
                        if(+exam.status === 7){
                            defer.resolve({action:1});
                        }else if(+exam.status > 5) {
                            defer.resolve();
                        }else {
                            defer.reject();
                        }
                        break;
                    default:
                        break;
                }
            });
            return defer.promise;
        }
        /**
         * 跳转至新建空白卷页面
         * @param exam 统考信息
         */
        $scope.goAddBlankPaper = function (exam) {
            $state.go('myApp.newBlankPaper',{
                unifiedId:exam.FLnkId,
                unifiedName:exam.unifiedName
            });
        };
        /**
         * 获取带纠错考卷
         */
        function getUnbindPapger(TaskFId){
            var defer = $q.defer();
            apiCommon.GetNoBodyExAnswer({
                taskFid :TaskFId
            }).then(function(res){
                defer.resolve(res);
            }, function(res){
                defer.reject(res)
            });
            return defer.promise;
        }

         //获取题目异常的试卷
        function getAbnormalPapers(classId,examfid) {
                var defer = $q.defer();
                apiCommon.getAbnormalPapers({
                    classId: classId.ClassFLnkID,
                    examFLnkId: examfid
                }).then(function (res) {
                    defer.resolve(res);
                }, function (res) {
                    defer.reject();
                });
                return defer.promise;
         }
        function getClassList5(exam,Period) {
            $scope.currentPaper = exam;
            apiCommon.generalQuery({
                    Proc_name: 'ProcQueryClassExams ',
                    ShoolFlnkId: user.schoolFId,
                    ExamFLnkID: exam.ExamFId,
                    Period: Period
            }).success(function (res) {
                $scope.ff = res.msg;
                $scope.isStatueTrue = [];
                $scope.sendAnayList = [];
                _.filter($scope.ff, function (item) {
                    if (item.status === 1) {
                        $scope.isStatueTrue.push(item);
                    } else {
                        $scope.sendAnayList.push(item);
                    }
                });

                $scope.curExamUnifiedFId = exam.FLnkId;
                if (_.isArray($scope.sendAnayList) && $scope.sendAnayList.length > 0) {
                    ngDialog.open({
                        template:'<div class="nextMarking-nav" style="text-align: center;color: #ffffff">班级提交情况(请核实人数!)</div>'+
                        '<div class="article-content"><ul class="sub-task-container">' +
                        '<li class="sub-task" ng-repeat="classNan in sendAnayList">' +
                        '<div class="clearfix task-des" ng-click="getStuActive(classNan,$index)"  ng-class="{active: classNan.isActive}">' +
                        '<label for={{classNan.SmallNum}}><span style="font-weight: 600">{{classNan.ClassName}}</span>({{classNan.ExamNum}}/{{classNan.nums}})' +
                        '<span ng-show="(classNan.nums - classNan.finishnum) > 0" style="margin-left: 20px;font-size: 14px">' +
                        '(<span style="color: #237462">{{classNan.nums - classNan.finishnum}}</span>人未批阅或无数据)</span></label></div>' +
                        '<div class="actions-container row" ng-show="classNan.isActive">' +
                        '<div ng-repeat="stu in currentNoStuMsg" class="col-md-4 actions-stuMsg"><span ng-bind="stu.smallNum + stu.studentName"></span><span ng-class="{noData:stu.type == 2}">({{stu.type == 2 ? "无数据":"未批阅"}})</span></div>'+
                        '</div></li></ul></div>'+
                        '<div class="ngdialog-btn">' +
                        '<button type="button" class="ngdialog-but" ng-click="doSendAna()">提交分析</button></div>',
                        plain: true,
                        scope: $scope,
                        className: 'ngdialog-theme-default ngdialog-tableManage'
                    });
                    $scope.getStuActive = function (classNan ,index) {
                        _.each($scope.sendAnayList,function (item) {
                            if(item.FLnkID === classNan.FLnkID){
                                item.isActive =  !item.isActive;
                                if(item.isActive){
                                    getStuMsg(classNan);
                                }
                            }else{
                                item.isActive = false;
                            }
                        });
                    };
                    $scope.doSendAna = function(){
                        $scope.sendAna(exam);
                    }
                } else {
                    constantService.showSimpleToast("暂无数据！");
                }
            });
        }
        //获取每个班的详细数据
        function getStuMsg(classNan) {
            apiCommon.generalQuery({
                UnifiedItemFId: $scope.curExamUnifiedFId,
                Proc_name :'Proc_GetUnifiedTasks'
            }).then(function (res) {
                var classID = _.find(res.data.msg,function (item) {
                    return item.ClassName === classNan.ClassName;
                });
                $scope.currentNoStuMsg = [];
                var currentW = [],currentN = [];
                apiCommon.getAbnormalPapers({
                    classId: classID.ClassFLnkID,
                    examFLnkId: $scope.currentPaper.ExamFId
                }).then(function (resq) {
                    _.each(resq.data.studentList,function (stu) {
                        if(stu.type == -1){
                            currentW.push(stu);
                            studentSort(currentW);
                        }else if(stu.type == 2){
                            currentN.push(stu);
                            studentSort(currentN);
                        }
                    });
                    $scope.currentNoStuMsg = currentW.concat(currentN);
                })
            });
        }
        //学生按学号排序
         function studentSort(stu) {
             stu.sort(function (a, b) {
                 return +a.smallNum - +b.smallNum;
             });
         }
        //提交分析接口
        $scope.sendAna = function (exam) {
            var isEndStr = [], isNoEnd = [];
            _.filter($scope.sendAnayList, function (item) {
                if (item.isend === 1) {
                    isEndStr.push(item.FLnkID);
                } else {
                    isNoEnd.push(item.FLnkID);
                }
            });
            if (isNoEnd.length > 0) {
                constantService.alert('还有班级未批阅完成，请去阅卷管理查看批阅记录!');
                ngDialog.close();
            } else {
                var isEnd = isEndStr.join(',');
                ngDialog.close();
                constantService.modal({
                    title: '提示',
                    scope: $scope,
                    template: '<div style="text-align: center;margin-top: 20px;">' +
                        '<p style="font-size: 16px;">是否需要在统考分析中将本门考试总分计为一半？</p>' +
                    '<div style="margin-top: 10px;"><label style="color: #ccc;font-weight: normal;">' +
                    '<input type="checkbox" ng-model="isHalf" style="width:20px;height:20px;vertical-align: text-bottom">统考总分修改成一半' +
                    '</label></div>' +
                    '</div>',
                    type: 'confirm',
                    cancelText: '取消',
                    okText: '确定',
                    okFn: function(){
                        this.doSubmitAnalysis(isEnd, exam);
                    }
                });
            }
        };

        $scope.doSubmitAnalysis = function(taskIds, exam){
            var examFactor = this.isHalf ? 2 : 1;
            apiCommon.tasksAnalysis({
                tasksFLnkId: taskIds,
                isForce: 0,
                examFactor: examFactor
            }).success(function (res) {
                if (res.code === 0) {
                    exam.ExamFactor = examFactor;
                    //constantService.showSimpleToast('提交成功');
                    //if($scope.submittedList.length){
                    apiCommon.updateUnifiedItemStep({//设置子统考
                        unifiedItemId: $scope.curExamUnifiedFId,
                        step: '6'
                    }).then(function (res) {
                        if (res.data.code === 0) {
                            constantService.showSimpleToast('提交分析成功!');
                            $scope.currentPaper.status = 6;
                            setPaperStatus($scope.currentPaper)
                        } else {
                            constantService.alert(res.data.msg);
                        }
                    });
                    //}
                    ngDialog.close();
                } else {
                    ngDialog.close();
                    constantService.showSimpleToast('提交分析失败!');
                }
            })
        };
        //所选科目试卷
        function getSelectPaper() {
            var klys = [];
            var subjectNoMap = {
				'语文': 'A',
				'数学': 'B',
				'英语': 'C',
				'历史': 'D',
				'地理': 'E',
				'政治': 'F',
				'生物': 'G',
				'物理': 'H',
				'化学': 'I',
                '信息技术': 'J',
                '科学': 'K',
                '思社': 'L',
                '英语复习': 'M'
            };
            _.each($scope.subjectNames, function (item) {
                klys.push(subjectNoMap[item]);
            });
            return klys;
        }
        function getClassList6(exam, Period) {
            apiCommon.generalQuery({
                    Proc_name:'GetTaskSendList',
                    SchoolFlnkID:user.schoolFId,
                    ExExamFLnkID:exam.ExamFId ,
                    Period: Period
            }).success(function(res){
                $scope.gg = res.msg;
                $scope.isSendTrue = [];
                $scope.isSendFalse = [];
                _.filter($scope.gg,function(item){
                    if(item.IsSended === 1){
                        $scope.isSendTrue.push(item);
                    }else {
                        $scope.isSendFalse.push(item);
                    }
                });
                $scope.sendClassList = $scope.isSendFalse.concat($scope.isSendTrue);
                $scope.isAll1 = false;
                $scope.ttt= _.filter($scope.sendClassList,function(item){
                    return item.IsSended != 1
                });
                if(_.isArray($scope.sendClassList)&& $scope.sendClassList.length>0){
                    $scope.selectedTags = [];
                    ngDialog.open({
                        template : '<div class="nextMarking-nav"></div>'+
                        '<div style="text-align: center;font-size: initial;padding:20px 0 10px 0">班级发送情况</div>' +
                        '<div class="row" style="margin-left:10px"><div style="margin-top:10px" class="col-md-4">' +
                        '<input type="checkbox" id="all"  ng-click="all1()" ng-checked="isAll1" ng-disabled="tt.length===0"/>' +
                        '<label for="all" style="margin-left:5px;cursor:pointer"  ng-class="{checkBoxCol:tt.length===0}">全选</label></div>'+
                        '<div style="margin-top:10px" ng-repeat="classNa in sendClassList" class="col-md-4">' +
                        '<input type="checkbox" id={{classNa.SmallNum}} name="{{classNa.taskFid}}" ng-click="updateSelection1($event,classNa.SmallNum)" ng-checked="classNa.IsSended === 1||isSelected(classNa.SmallNum)" ng-disabled="classNa.IsSended === 1">' +
                        '<label for={{classNa.SmallNum}} style="margin-left:5px;cursor:pointer" ng-class="{checkBoxCol:classNa.IsSended===1}">{{classNa.ClassName}}</label></div></div>' +
                        '<div class="ngdialog-btn">' +
                        '<button type="button" class="ngdialog-but"  ng-click="sendMessage()">发送微信通知</button></div>',
                        plain : true,
                        scope : $scope,
                        className : 'ngdialog-theme-default ngdialog-tableManage',
                        controller:function($scope){
                            return function(){
                                clickCheck();
                                $scope.tt= _.filter($scope.sendClassList,function(item){
                                    return item.IsSended != 1
                                });
                            }
                        }
                    })
                }else{
                    constantService.showSimpleToast("暂无数据！");
                }
            });
        }
        //发送微信接口
        $scope.sendMessage = function(){
            $scope.getTaskFid = $scope.selectedTags.join(',');
            if( $scope.getTaskFid !== ''){
                apiCommon.sendWXMessage({
                        taskFlnkId : $scope.getTaskFid
                }).success(function(res){
                    if(res.code === 0){
                        ngDialog.close();
                        constantService.showSimpleToast('发送成功');
                    } else {
                        ngDialog.close();
                        constantService.showSimpleToast('发送失败');
                    }
                })
            }else{
                constantService.showSimpleToast("暂无数据！");
            }
        };
        $scope.uploadPaper = function(e,task, exam){
            e.stopPropagation();
            //var sessionId = $cookieStore.get('sessionId') || '';
            var sessionId = $cookies['sessionId'] || '';
            window.open('/exportPaperFromWord/paperFromWord.html?isOpenNewWin=1&sessionId=' + sessionId + '&subjectId=' + task.SubjectId
                + '&gradeNo=' + task.GradeNum + '&unifiedId=' + task.FLnkId + (task.efid ? ('&examNo=' + task.efid) : '') + (!!task.ExamFId ? ('&examId=' + task.ExamFId) : ''));
            constantService.confirm('提示', '请在新窗口中完成上传试卷操作，是否成功上传试卷？',['导入成功', '导入失败'], function(){
                //todo 上传成功之后重新加载该统考答卷
                bes.send('/BootStrap/schoolmanager/getMyUnifieds.ashx',{
                    targetId: task.FLnkId,
                    isParent: '0'
                }, {
                    method: 'post'
                }).then(function(res){
                    if(res.data.code === 0) {
                        var unitTasks = res.data.msg[0].unitTasks;
                        _.each(unitTasks, function(item){
                            var oldTask = _.find(exam.unitTasks, function(t){
                                return t.FLnkId === item.FLnkId;
                            });
                            item.isActive = oldTask.isActive;
                        });
                        exam.unitTasks = unitTasks;
                    }
                });
            });
        };
        $scope.uploadAnswer = function (e,task, exam) {
            e.stopPropagation();
            var sessionId = $cookies['sessionId'] || '';
            var _myWindow = window.open('_blank');
            _myWindow.location = '/exportPaperFromWord/answerForPaper.html?sessionId='+ (sessionId || '') +'&examId='
                + task.ExamFId + '&unifiedId=' + task.FLnkId;
        };
        function setXY() {
            if ($('.exam-center').offset().left > 160) {
                $('#map').css({'left': ($('.exam-center').offset().left - 160) + 'px'});
            } else {
                $('#map').css({'left': 0})
            }
        }
        setXY();
        $(window).resize(function () {
            if ($state.current.name === 'myApp.examCenter') {
                setXY();
            }
        });
        //复选框点击事件
        function clickCheck () {
            $scope.showType = 1;
            $scope.selectedTags = [];
            $scope.selected = [];
            $scope.all1 = function () {
                $scope.isAll = !$scope.isAll;
                if ($scope.selected.length !== $scope.ttt.length) {
                    _.each($scope.ttt, function (item, index) {
                        $scope.selected[index] = item.SmallNum;
                        $scope.selectedTags[index] = item.taskFid;
                    });
                } else if ($scope.selected.length === $scope.ttt.length) {
                    $scope.selected = [];
                    $scope.selectedTags = [];
                }
            };
            $scope.updateSelected = function (action, SmallNum, name) {
                if (action == 'add' && $scope.selected.indexOf(SmallNum) == -1) {
                    $scope.selected.push(SmallNum);
                    $scope.selectedTags.push(name);
                }
                if (action == 'remove' && $scope.selected.indexOf(SmallNum) != -1) {
                    var idx = $scope.selected.indexOf(SmallNum);
                    $scope.selected.splice(idx, 1);
                    $scope.selectedTags.splice(idx, 1);
                }
            };
            $scope.updateSelection1 = function ($event, SmallNum) {
                var checkbox = $event.target;
                var action = (checkbox.checked ? 'add' : 'remove');
                $scope.updateSelected(action, SmallNum, checkbox.name);
                if ($scope.selected.length !== $scope.ttt.length) {
                    $scope.isAll1 = false;
                } else {
                    $scope.isAll1 = true;
                }
            };
            $scope.isSelected = function (SmallNum) {
                return $scope.selected.indexOf(SmallNum) >= 0;
            };
        }
        function getSonUnifiedItem(unifieditem) {
            var defer = $q.defer();
            apiCommon.generalQuery({
                    Proc_name: 'getUnifiedItem',
                    UnifiedItemFid: unifieditem//子统考Id
            }).then(function (res) {
                defer.resolve(res);
            },function (res) {
                defer.reject(res);
            });
            return defer.promise;
        }
        function setPaperStatus(exam) {
            _.each($scope.examList,function (a) {
                _.each(a.unitTasks,function (b) {
                    if(exam.FLnkId === b.FLnkId){
                        exam.status = b.status;
                    }
                })
            })
        }
        function setStepRequest(unifieditemid,step) {
            var defer = $q.defer();
            apiCommon.updateUnifiedItemStep({//设置子统考
                unifiedItemId: unifieditemid,
                step: step
            }).then(function (res) {
                if (res.data.code === 0) {
                    constantService.showSimpleToast('设置成功!');
                    defer.resolve(res);
                } else {
                    constantService.alert(res.data.msg);
                    defer.reject(res);
                }
            },function (res) {
                defer.reject(res);
            });
            return defer.promise;
        }
        $scope.showMsgNo = function (exam) {
                $scope.classMsg = [];
                if(exam.SubmitNum > 0){
                    apiCommon.generalQuery({
                            Proc_name: 'ProcQueryClassExams ',
                            ShoolFlnkId: user.schoolFId,
                            ExamFLnkID: exam.ExamFId,
                            Period: exam.Period
                    }).success(function (resq) {
                        if(resq.msg.length === 0){
                            $scope.classMsg = [];
                        }else{
                            $scope.classMsg = resq.msg;
                        }
                    });
                    ngDialog.open({
                        template :'<div class="nextMarking-nav"></div>'+
                        '<div style="text-align: center;font-size: initial;padding:20px 0 10px 0">班级提交情况!</div>'+
                        '<div class="row" style="margin-left:10px">' +
                        '<div style="margin-top:15px" ng-repeat="classNo in classMsg" class="col-md-4"><span ng-bind="classNo.ClassName"></span>' +
                        '(<span ng-bind="classNo.ExamNum" class="green"></span>/<span ng-bind="classNo.nums"></span>)</div></div>'+
                        '<div class="row" style="height: 35px"></div>',
                        plain : true,
                        scope : $scope,
                        className : 'ngdialog-theme-default ngdialog-tableManage'
                    });
                }else{
                    constantService.alert('暂无数据!')
                }
            };
        $scope.importExam = function (e, exam, isParent) {
            e.stopPropagation();
            ngDialog.open({
                template: 'manage/importData.html',
                className: 'ngdialog-theme-plain',
                appendClassName: 'importData-confirm-to-setting',
                scope: $scope,
                closeByDocument: false,
                controller: function ($scope) {
                    return function () {
                        $scope.userName = '成绩';
                        $scope.exam = exam;
                    }
                }
            });
        };
        $scope.uploadDataCheck = function (picFile, name, exam) {
            if (!picFile) {
                constantService.alert('请选择文件！');
                return;
            } else {
                if (picFile.name) {
                    var fileType = picFile.name.substr(picFile.name.lastIndexOf(".")).toLowerCase();
                    if (!((fileType === '.xls') || (fileType === '.xlsx'))) {
                        constantService.alert('文件格式不对！');
                        return;
                    }
                } else {
                    constantService.alert("请命名文件后重新上传！");
                    return;
                }
            }
            ngDialog.close();
            $scope.userName = name;
            $scope.allowUpLoad = true;
            Upload.upload({
                url: resourceUrl + '/FileScore/UploadFile',
                data: {
                    file: picFile,
                    schoolId: user.schoolFId,
                    sjo: exam.efid,
                    examId: exam.ExamFId,
                    unifiedExamId: exam.UnifiedFId
                }
            }).then(function (resp) {
                if(resp.data.Flag) {
                    apiCommon.updateUnifiedItemStep({//设置子统考
                        unifiedItemId: exam.FLnkId,
                        step: 5
                    }).then(function (res) {
                        if (res.data.code === 0) {
                            exam.status = 5;
                            constantService.alert(resp.data.Message);
                        } else {
                            constantService.alert(res.data.msg);
                        }
                    });
                } else {
                    constantService.alert(resp.data.ResultObj);
                }
            });
        };
        $scope.goTop = function () {
            $(document).scrollTop(0)
        };
    }];
});