define(['fullPage','snow','demo2','particles','../service/assignRouter', 'constant'], function (fullpage, snow,demo2,particles, assignRouter, constant) {
	return ['$http', '$scope', '$rootScope', '$state', '$anchorScroll', '$location', '$timeout', 'ngDialog','$cookieStore','apiCommon',
		function ($http, $scope, $rootScope, $state, $anchorScroll, $location, $timeout, ngDialog, $cookieStore, apiCommon) {
			window.sessionStorage.removeItem('currentUser');
			$scope.login = function () {
				var master = angular.copy($scope.user);
				master.auto = master.auto ? 1 : 0;
				master.ipaddress = '192.168.1.1';
				apiCommon.login(master).success(function (data) {
					if (data.code == 1) {
						$scope.loginError = true;
						$timeout(function () {
							$scope.loginError = false;
						}, 1200);
						$scope.errorMsg = data.msg;
						$scope.user = {};
					} else if (data.username) {
						//data.role = 0;
                        $cookieStore.put('userId', data.SelfName);
						window.sessionStorage.setItem('currentUser', angular.toJson(data));
						if (master.auto) {
							window.localStorage.setItem('username', master.username);
							window.localStorage.setItem('psw', master.psw);
							window.localStorage.setItem('auto', master.auto);
							window.localStorage.setItem('ipaddress', master.ipaddress);
							window.localStorage.setItem('schoolName', master.schoolName);
							window.localStorage.setItem('SelfName', master.SelfName);

                        } else {
                            window.localStorage.removeItem('username');
                            window.localStorage.removeItem('psw');
                            window.localStorage.removeItem('auto');
                            window.localStorage.removeItem('ipaddress');
                            window.localStorage.removeItem('schoolName');
                            window.localStorage.removeItem('SelfName');
                        }
                        assignRouter.assign($state, data, function(){
                            if(data.role === constant.ROLE.TEACHER) {
                                if (data.isComplete === '0') {
                                    dialog(function () {
                                        $state.go('myApp.manage.teacherHelpSelf');
                                    }, $scope);
                                }
                            }
                        });
                    }
                }).error(function (resp) {
                    if (resp.code == 1) {
                        $scope.loginError = true;
                        $scope.errorMsg = resp.msg;
                        $scope.user = {};
                    }
                });
            };
            var userName = window.localStorage.getItem('username');
            var psw = window.localStorage.getItem('psw');
            var auto = window.localStorage.getItem('auto');
            var ipaddress = window.localStorage.getItem('ipaddress');
            if (userName && auto) {
                $scope.user = {
                    username: userName,
                    psw: psw,
                    auto: true,
                    ipaddress: ipaddress
                };
            }

			function dialog(doneCb, failCb) {
				var dialogObj = ngDialog.open({
					template: '<div class="jugdeDataFull clearfix"> ' +
					'<div style="background: #41B39C ; height: 35px;border-radius: 10px 10px 0 0;"> ' +
					'<p></p> </div> <div style="padding: 40px 15px;">您的部分资料还需要完善，为了不影响您的使用，请立即前往设置。' +
					'</div> <button class="smBtn" ng-click="done()">去设置</button>' +
					' <button class="smBtn" ng-click="cancel()">下次提醒我</button> </div>',
					className: 'ngdialog-theme-default',
					appendClassName: 'confirm-to-setting',
					closeByDocument: false,
					plain: true,
					controller: function ($scope) {
						return function () {
							$scope.cancel = function () {
								dialogObj.close();
								if (failCb && _.isFunction(failCb)) {
									failCb();
								}
							};
							$scope.done = function () {
								dialogObj.close();
								if (doneCb && _.isFunction(doneCb)) {
									doneCb();
								}
							};
						}
					}
				})
			}

            if (window.config.theme === '51') {
                $scope.$watch('$DOMContentLoad', function () {
                    firstPage();
                });
                function firstPage() {
                    preLoadNextBg(0);
                }

                particlesJS('particles-js', {
                    "particles": {
                        "number": {
                            "value": 60,
                            "density": {
                                "enable": true,
                                "value_area": 500
                            }
                        },
                        "color": {
                            "value": "#ffffff"
                        },
                        "shape": {
                            "type": "polygon",
                            "stroke": {
                                "width": 0,
                                "color": "#000000"
                            },
                            "polygon": {
                                "nb_sides": 5
                            },
                            "image": {
                                "src": "img/github.svg",
                                "width": 100,
                                "height": 100
                            }
                        },
                        "opacity": {
                            "value": 0.5,
                            "random": false,
                            "anim": {
                                "enable": false,
                                "speed": 1,
                                "opacity_min": 0.1,
                                "sync": false
                            }
                        },
                        "size": {
                            "value": 3,
                            "random": true,
                            "anim": {
                                "enable": false,
                                "speed": 40,
                                "size_min": 0.1,
                                "sync": false
                            }
                        },
                        "line_linked": {
                            "enable": true,
                            "distance": 150,
                            "color": "#ffffff",
                            "opacity": 0,
                            "width": 1
                        },
                        "move": {
                            "enable": true,
                            "speed": 6,
                            "direction": "none",
                            "random": false,
                            "straight": false,
                            "out_mode": "out",
                            "bounce": false,
                            "attract": {
                                "enable": false,
                                "rotateX": 600,
                                "rotateY": 1200
                            }
                        }
                    },
                    "interactivity": {
                        "detect_on": "canvas",
                        "events": {
                            "onhover": {
                                "enable": true,
                                "mode": "grab"
                            },
                            "onclick": {
                                "enable": true,
                                "mode": "grab"
                            },
                            "resize": true
                        },
                        "modes": {
                            "grab": {
                                "distance": 150,
                                "line_linked": {
                                    "opacity": 1
                                }
                            },
                            "bubble": {
                                "distance": 400,
                                "size": 40,
                                "duration": 2,
                                "opacity": 8,
                                "speed": 3
                            },
                            "repulse": {
                                "distance": 200,
                                "duration": 0.4
                            },
                            "push": {
                                "particles_nb": 4
                            },
                            "remove": {
                                "particles_nb": 2
                            }
                        }
                    },
                    "retina_detect": false
                });


                var temp = 0;
                var Swidth = 1000;
                var timer = null;
                var len = $(".Div1_title span a").length;

                function NextPage() {
                    if (temp > 2) {
                        temp = 0;
                    }
                    $(".Div1_title span a").removeClass("Div1_title_a1").eq(temp).addClass("Div1_title_a1");
                    $(".Div1_main").css({
                        left: -temp * Swidth + "px"
                    })
                }

                function PrevPage() {
                    if (temp < 0) {
                        temp = 2;
                    }
                    $(".Div1_title span a").removeClass("Div1_title_a1").eq(temp).addClass("Div1_title_a1");
                    $(".Div1_main").css({
                        left: -temp * Swidth + "px"
                    })
                }

                //下一页
                $(".Div1_next").click(function () {
                    temp++;
                    NextPage();
                });
                //上一页
                $(".Div1_prev").click(function () {
                    temp--;
                    PrevPage();
                });
                //自动滚动
                var r = [0, 0];
                var runPage;
                runPage = new FullPage({
                    id: 'pageContain',
                    slideTime: 200,
                    continuous: true,
                    effect: {
                        transform: {
                            translate: 'Y',
                            scale: [1, 1],
                            rotate: [r[Math.floor(Math.random() * 2)], 0]
                        },
                        opacity: [0.7, 1]
                    },
                    mode: 'wheel,touch,nav:navBar',
                    easing: 'ease',
                    onSwipeStart: function (index, thisPage) {
                    },
                    beforeChange: function (index, thisPage) {

                    },
                    callback: function (index, thisPage) {
                        loadImg(index);
                        preLoadNextBg(index);
                        if (index == 4) {
                            var timer = setInterval(function () {
                                temp++;
                                NextPage();
                            }, 10000);
                        }
                        if (index == 0) {
                            firstPage();
                        }
                        if (index == 5) {
                            snow.initSnow();
                        }
                        if (index == 4) {
                            demo2.initPage2();
                        }
                        if (index == 3) {
                            $("#pageContain .page7 .page7-index").css({
                                "animation": "indexZoom 8s linear forwards",
                                "-moz-animation": "indexZoom 8s linear forwards",
                                "-webkit-animation": "indexZoom 8s linear forwards",
                                "-o-animation": "indexZoom 8s linear forwards"
                            })
                        }
                    }
                });

                $('.arrow').click(function () {
                    runPage.next();
                });

                $('.page5 .arrow').click(function () {
                    runPage.go(0);
                });
                //性能优化，初始只加载第一页图片，其余页面的图片在切换页面时动态加载
                function loadImg(index) {
                    var $page = $('#page' + (index + 1));
                    var $imgs = $page.find('img');
                    $imgs.each(function () {
                        var imgSrc = $(this).attr('src');
                        if (!imgSrc) {
                            $(this).attr('src', $(this).data('src'));
                        }
                    });
                    var bgImg = $page.data('bgimg');
                    if (bgImg) {
                        $page.css({
                            'background-image': 'url(' + bgImg + ')',
                            'background-repeat': 'no-repeat'
                        });
                        $page.removeAttr('data-bgimg');
                    }
                }

                //为获取更好的用户体验，预加载下一页背景图片
                function preLoadNextBg(index) {
                    var $page = $('#page' + (index + 2));
                    var bgImg = $page.data('bgimg');
                    bgImg && $('<img>').attr('src', bgImg);
                }
                $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
                    if (window.config.theme !== 'jdy') {
                        document.removeEventListener('DOMMouseScroll', runPage.wheelScroll, true);
                        document.onmousewheel = window.onmousewheel = null;
                    }
                });
            }
        }
    ]
});