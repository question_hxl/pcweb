define(['jquery.datetimepicker', 'pagination', 'FileSaver', 'dateFomart', 'TransferPaper2Html'], function () {
	return ['$rootScope', '$http', '$scope', '$state', '$q', '$location', '$timeout', 'ngDialog', 'constantService',function ($rootScope, $http, $scope, $state, $q, $location, $timeout, ngDialog, constantService) {

		$scope.nodata = false;
        var user = JSON.parse(sessionStorage.getItem('currentUser'));
		var GradeId = $scope.GradeId = user.GradeId;
		$scope.gradeNum = user.GradeNo;
		console.log(GradeId + "--GradeId");
		var role = $scope.role = user.role;
		if (role == "管理员") {
			$scope.isadmin = true;
		} else {
			$scope.isadmin = false;
		}

		$scope.thispath = $location.path();

		// 获取年级
		$scope.currentprovince = '全部';
		$scope.currentyear = '全部';
		$scope.currentClasstype = '全部';
		$scope.currentlevel = '全部';
		$scope.currentTimes = '全部';
		$scope.currentgradeName = GradeId;
		$http.get($rootScope.baseUrl + '/Interface0290C.ashx').success(function (res) {
			$scope.gradeList = res.course;
			//$scope.currentgradeName = res.course[0].gradeId
			if($scope.gradeNum && +$scope.gradeNum >= 10) {
				//如果是高中
				var grade = _.find($scope.gradeList, function(g){
					return g.gradeNum === $scope.gradeNum;
				});
				$scope.currentgradeName = grade && grade.gradeId || GradeId;
				GetAllEmployee();
			}
		})
		//试卷类型
		$http.get($rootScope.baseUrl + '/Interface0196.ashx').success(function (res) {
			$scope.ClasstypeList = res.msg;
		})
		// 省份
		$http.get($rootScope.baseUrl + '/Interface0195.ashx').success(function (res) {
			$scope.provinceList = res.msg;
		})
		// selectCur
		$scope.selectCur = function ($event, properties) {
			var tag = $event.target;
			var txt = $(tag).text();
			switch (properties) {
				case 'province':
					$scope.currentprovince = txt; // 省份
					searchAll();
					break;
				case 'year':
					$scope.currentyear = txt; // 年份
					searchAll();
					break;
				case 'level':
					$scope.currentlevel = txt; // 级别
					searchAll();
					break;
				case 'Classtype':
					$scope.currentClasstype = txt; // 类型
					searchAll();
					break;
				case 'Times':
					$scope.currentTimes = txt; // 时间
					timeGo();
					searchAll();
					break;
				case 'gradeName':
					$('#searchpaper').val("");
					$scope.name = '';
					$scope.currentgradeName = $(tag).attr('data-id'); // 时间   
					searchAll();
					break;
			}
		}
		$('#start_datetimepicker').datetimepicker({
			lang: "ch",
			timepicker: false,
			format: "Y-m-d",
			todayButton: false,
			scrollInput: false,
			onChangeDateTime: function (dp, $input) {

				$('#start_datetimepicker').datetimepicker('hide');
			}
		});
		$('#end_datetimepicker').datetimepicker({
			lang: "ch",
			timepicker: false,
			format: "Y-m-d",
			todayButton: false,
			scrollInput: false,
			onChangeDateTime: function (dp, $input) {
				$('#end_datetimepicker').datetimepicker('hide');
			}
		});

		// 最近时间	
		var timeGo = function () {
			var date = new Date();
			// 判断时间
			if ($scope.currentTimes == '最近一周') {
				$('#start_datetimepicker').val(date.dateChange("lastsevendays")[0]);
				$('#end_datetimepicker').val(date.dateChange("lastsevendays")[1]);
			} else if ($scope.currentTimes == '最近一月') {
				$('#start_datetimepicker').val(date.dateChange("recentmonth")[0]);
				$('#end_datetimepicker').val(date.dateChange("recentmonth")[1]);
			} else {
				$('#start_datetimepicker').val('');
				$('#end_datetimepicker').val('');
			};
		};


		// 点击搜索
		$scope.search = function () { searchAll() };
		var searchAll = function () {
            var endTime = $('#end_datetimepicker').val(),
                startTime = $('#start_datetimepicker').val();
			if (startTime && endTime && endTime < startTime) {
				ngDialog.open({
					template: '<p>结束时间需大于开始时间</p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					plain: true,
					className: 'ngdialog-theme-plain'
				});
				return false;

			} else {
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0182.ashx',
					data: {
						province: $scope.currentprovince,
						year: $scope.currentyear,
						level: $scope.currentlevel,
						type: $scope.currentClasstype,
						gradeId: $scope.currentgradeName,
						startTime: startTime,
						stoptime: endTime,
						sourceType: 3,
						curPage: 1,
						pageSize: 10,
						name: $scope.name || ''
					}
				}).success(function (res) {
					if (res.code == 3) {
						$scope.nodata = true;
					} else {
						$scope.nodata = false;
						$scope.tkList = res.msg;
						$scope.Count = res.Count
						$scope.paginationConf.totalItems = res.Count,
                        $scope.paginationConf.currentPage = 1
					}
				})
			}
		}

		// 组卷
		$scope.printClass = function (id) {
			$location.path('/paperComposing').search({ 'examId': id, 'action': 3, 'gradeid': $scope.currentgradeName });
		}

		/*分页*/
		$scope.paginationConf = {
			currentPage: 1,
			itemsPerPage: 10
		};
		$scope.data = {
			province: $scope.currentprovince,
			year: $scope.currentyear,
			level: $scope.currentlevel,
			type: $scope.currentClasstype,
			gradeId: $scope.currentgradeName || 201,
			startTime: $('#start_datetimepicker').val(),
			stoptime: $('#end_datetimepicker').val(),
			sourceType: 3,
			curPage: 1,
			pageSize: 10,
			name: $scope.name || ''
		};
		var GetAllEmployee = function () {
			var req = angular.copy($scope.data);//copy  保存搜索状态
			req.curPage = $scope.paginationConf.currentPage,
			req.pageSize = $scope.paginationConf.itemsPerPage,
			req.province = $scope.currentprovince,
			req.year = $scope.currentyear,
			req.level = $scope.currentlevel,
			req.type = $scope.currentClasstype,
			req.gradeId = $scope.currentgradeName || 201,
			req.startTime = $('#start_datetimepicker').val(),
			req.stoptime = $('#end_datetimepicker').val(),
			req.sourceType = 3,
			req.name = $scope.name || ''
			$http.post($rootScope.baseUrl + '/Interface0182.ashx', req).success(function (resp) {
				if (resp.code == 3) {
					$scope.nodata = true;
				} else {
                    $scope.nodata = false;
					$scope.tkList = resp.msg;
					$scope.paginationConf.totalItems = resp.Count;
					$scope.Count = resp.Count
				}

			});
		}
		$scope.$watch('paginationConf.currentPage', GetAllEmployee);

		//查看试卷
		$scope.goScanPaper = function (id) {
			$location.path('/composing').search({ 'pathss': $scope.thispath, 'ExamFlnkID': id, 'action': 0, 'gradeid': $scope.currentgradeName });
		}

/*		// 点击下载
		$scope.chooseDown = function (Examid, Examname) {
			$scope.Examid = Examid;
			$scope.Examname = Examname
			ngDialog.open({
				template: '<div> <p>请选择导出方式：</p><center><div style="padding:10px">' +
				'<button  class="btn btn-info" ng-click="downWord(Examid,Examname,1)">导出试卷</button> ' +
				'<button  class="btn btn-info" ng-click="downWord(Examid,Examname,2)">导出卷卡合一</button> ' +
				// '<button class="btn btn-info" ng-click="downWord(Examid,Examname,3)">导出作业</button>' +
				'</div></center></div>',
				className: 'ngdialog-theme-plain',
				scope: $scope,
				plain: true
			})
		}

		//下载
		$scope.downWord = function (id, name, type) {
			ngDialog.close();
			if (type == 1) {
			   // $http.post($rootScope.baseUrl + '/Interface0118A.ashx', {
			   //     examId: id,
			   //     examName: name
			   // }).success(function (res) {
			   //     var blob = new Blob([res.msg], {
			   //         type: "text/plain;charset=utf-8"
			   //     });
			   //     saveAs(blob, name + ".doc");
			   //     ngDialog.close();
			   // })
				$http.post($rootScope.baseUrl + '/Interface0181.ashx', {
					examId: id
				}).then(function(res){
					var paperTransfer = new $.PaperTransfer(res.data);
					paperTransfer.exportToDoc();
				});
			} else if (type == 2) {
				//获取下载地址下载
				var win = window.open();
				$http.post($rootScope.baseUrl + '/Interface0118.ashx', {
					examId: id,
					examName: name
				}).success(function (res) {
					win.location = res.msg;
					setTimeout(function(){
						win.close();
					}, 500);
				});
			} else if (type == 3) {
				$http.post($rootScope.baseUrl + '/Interface0118B.ashx', {
					examId: id,
					examName: name
				}).success(function (res) {
					var blob = new Blob([res.msg], {
						type: "text/plain;charset=utf-8"
					});
					saveAs(blob, name + ".doc");
					ngDialog.close();
				})
			}

		}*/

		// 点击下载
		$scope.chooseDown = function (Examid, Examname) {
			$scope.Examid = Examid;
			$scope.Examname = Examname;
			$scope.downId = 0;
			$scope.isLoad = false;
			$scope.modifyData = {
				isModify:false,
				qIndex:0
			};
			/*ngDialog.open({
				template: '/downLoad.html',
				className: 'ngdialog-theme-plain',
				scope: $scope
			})*/
			$scope.downWord($scope.Examid,$scope.Examname,1);
		};

		$scope.editPaper = function(tk){
			$state.go('myApp.paperComposing', {
				examId: tk.FLnkID,
				action: 3
			});
		};

		$scope.showSheetHistory = function(paper){
			$state.go('myApp.paperBankSheet',{
				ExName:paper.ExName,
				FID:paper.FID,
				FLnkID:paper.FLnkID,
				CreateDate:paper.CreateDate
			});
            // $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
             //    Proc_name: 'GetAnswerSheet',
             //    examId: paper.FLnkID,
             //    SheetName: '',
             //    sheetId: ""
            // }).then(function(res){
             //    if(res.data.code === 0 && _.isArray(res.data.msg) && res.data.msg.length > 0) {
             //        $scope.sheetList = res.data.msg;
             //        ngDialog.open({
             //            template: 'checkAnswerSheet',
             //            className: 'ngdialog-theme-default',
             //            appendClassName: 'task-apply-dialog',
             //            closeByDocument: true,
             //            scope: $scope,
             //            controller: function ($scope, $http, $state) {
             //                return function () {
             //                    $scope.editAnswerSheet = function(sheet){
             //                        ngDialog.close();
             //                        window.open('#/buildAnswerSheet?sheetId='+sheet.sheetId);
             //                    };
             //                    $scope.rebuildSheet = function(){
             //                        ngDialog.close();
             //                        window.open('#/buildAnswerSheet?examId=' + paper.FLnkID + '&examFID=' + paper.FID);
             //                    };
             //                }
             //            }
             //        })
             //    }else {
             //        $scope.sheetList = [];
             //        constantService.alert('该试卷暂无答题卡!');
             //    }
            // }, function(){
             //    constantService.alert('查询答题卡失败，请稍后重试!');
			// });
		};

		$scope.buildAnswerSheet = function(paper){
            window.open('#/buildAnswerSheet?examId=' + paper.FLnkID + '&examFID=' + paper.FID + '&isOpenNewWin=' + 1);
		};



		// 下载
		$scope.downWord = function (id, name, type) {
			if (type == 1) {
				$scope.downId = 1;
				ngDialog.closeAll();
				$http.post($rootScope.baseUrl + '/Interface0181.ashx', {
					examId: id
				}).then(function(res){
					var paperTransfer = new $.PaperTransfer(res.data);
					paperTransfer.exportToDoc();
				});
			}else if(type === 2) {
				$scope.downId = 2;
				ngDialog.closeAll();
				var win = window.open();//获取下载地址下载
				$http.post($rootScope.baseUrl + '/Interface0118.ashx', {
					examId: id,
					examName: name
				}).success(function (res) {
					win.location = res.msg;
					setTimeout(function(){
						win.close();
					}, 500);
				});
			} else if (type == 3) {
				$scope.downId = 3;
				ngDialog.closeAll();
				$http.post($rootScope.baseUrl + '/Interface0118B.ashx', {
					examId: id,
					examName: name
				}).success(function (res) {
					var blob = new Blob([res.msg], {
						type: "text/plain;charset=utf-8"
					});
					saveAs(blob, name + ".doc");
					ngDialog.close();
				})
			}else if(type == 4){//自定义导出
				if($scope.downId === 4){
					$scope.downId = 0;
					$scope.isLoad = true;
					return;
				}
				$scope.downId = 4;
				if(!$scope.isLoad){
					$scope.QTYPES = [];//本卷题目类型
					$scope.QstDTYPES = [];
					$scope.DTYPES = [{id:1,dName:'判断题'},{id:2,dName:'给分框'}];//导出方式

					//获取试卷里面的题型
					$http.post($rootScope.baseUrl + '/Interface0181.ashx', {
						examId: id
					}).success(function(res){
						$scope.QTYPES = res.msg.map(function (item,index) {
							return {
								id:+index+1,
								qName:item.QTypeName,
								dName:item.QTypeName === '选择题' ? '判断题' : '给分框',//默认导出模式
								dType:item.QTypeName === '选择题' ? '1' : '2'
							}
						});
						$scope.QstDTYPES =	$scope.QTYPES;
						$scope.qstMsg = $scope.QstDTYPES.map(function (item) {//接口数据
							return {
								type :item.dType,
								qtypeName :item.qName
							}
						});
					});
				}
			}

			$scope.modifySlot = function (index) {
				$scope.modifyData.isModify = true;
				$scope.modifyData.qIndex = index;
				$scope.bottomtype = {'margin-top':(35+index*42)+'px'};
			};
			$scope.modifyLeave = function (index) {
				$scope.modifyData.isModify = false;
			};
			$scope.doModify = function (item) {//修改导出方式
				$scope.modifyData.isModify = false;
				if($scope.qstMsg[$scope.modifyData.qIndex].type === item.id){//没改变
					return;
				}
				$scope.QstDTYPES[$scope.modifyData.qIndex].dName = item.dName;
				$scope.qstMsg[$scope.modifyData.qIndex].type = item.id;
			};
			$scope.doSure = function () {//确认自定义导出
				ngDialog.closeAll();
				var win = window.open();//获取下载地址下载
				$http.post($rootScope.baseUrl + '/Interface0118D.ashx', {
					examId: $scope.Examid,
					examName: $scope.Examname,
					msg:$scope.qstMsg
				}).success(function (res) {
					win.location = res.msg;
					setTimeout(function(){
						win.close();
					}, 500);
				});
			};
		}

        //编辑
        $scope.editpaperBank = function (question) {
            $scope.ExName = question.ExName;
			$scope.examTypeList = [{type:'2',name:'校本卷库'},{type:'3',name:'本人卷库'}];
            ngDialog.open({
                template: 'editpaperBankmy',
                className: 'ngdialog-theme-plain',
				appendClassName: 'editpaperBankmy-confirm-to-setting',
                scope: $scope,
				closeByDocument: true,
				controller: function ($scope) {
					return function () {
						$scope.currentExamType = $scope.examTypeList[1];
						$scope.selectTeacher ={userName:user.username};
						//teaList();
						$scope.changeGrade = function (Grade) {
							if($scope.currentgrade !== Grade){
								$scope.currentgrade = Grade;
							}
						};
						$scope.changeType = function (Type) {
							if($scope.currentClass !== Type){
								$scope.currentClass = Type;
							}
						};
						$scope.changeExamType = function (ExamType) {
							if($scope.currentExamType !== ExamType){
								$scope.currentExamType = ExamType;
							}
							// if($scope.currentExamType.type === '3'){
							// 	//teaList();
							// 	$scope.selectTeacher ={userName:user.username}
							// }
						};
						// function teaList() {
						// 	$http.post($rootScope.baseUrl + '/Interface0295.ashx', {
						// 		schoolFId: user.schoolFId
						// 	}).success(function (res) {
						// 		$scope.teaList = res.msg;
						// 		$scope.teaList.unshift({userName:'请选择'});
						// 		$scope.selectTeacher = $scope.teaList[0];
						// 	})
						// }
						$scope.editPaperBankMySave = function () {
							if($scope.ExName === ''|| $scope.currentClass === undefined || $scope.currentExamType === undefined || $scope.currentgrade === undefined){
								
							}else {
								console.log($scope.ExName);
								console.log($scope.currentClass.ExType);
								console.log($scope.currentExamType.type);
								console.log($scope.currentgrade.gradeId);
							}
						};
					}}
            });
        };
        $scope.hoverQst = function (index) {
            $scope.qstIndex = index;
        };
        $scope.leaveQst = function (index) {
            $scope.qstIndex = '';
        };
        /**
		 * 个人卷库转为校本
         */
        $scope.changeBank = function (item,index) {
			$http.post('BootStrap/Interface/updatePaperStore.ashx',{
                examFLnkId:item.FLnkID,
                bankType:2
			}).then(function (res) {
				constantService.alert('操作成功!');
				$scope.tkList.splice(index,1);
            },function (res) {
				constantService.alert('操作失败，请稍后!')
            });
        };
        $scope.link2ImportPaper = function () {
			window.open('/exportPaperFromWord/paperFromWord.html');
        }
    }]
})