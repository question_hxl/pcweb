define(function(){
	return {
		initSnow: function(){
			var COUNT = 300;
			var canvas = document.getElementById('snow');
			var ctx = canvas.getContext('2d');
			var width = window.innerWidth;
			var height = window.innerHeight;
			canvas.width = width;
			canvas.height = height;
			var i = 0;

			function onResize() {
				width = window.innerWidth;
				height = window.innerHeight;
				canvas.width = width;
				canvas.height = height;
			}
			var Snowflake = function() {
				this.x = 0;
				this.y = 0;
				this.vy = 0;
				this.vx = 0;
				this.r = 0;
				this.reset();
			}
			Snowflake.prototype.reset = function() {
				this.x = Math.random() * width;
				this.y = Math.random() * -height;
				this.vy = 1 + Math.random() * 3;
				this.vx = 0.5 - Math.random();
				this.r = 1 + Math.random() * 2;
				this.o = 0.5 + Math.random() * 0.5;
			}
			canvas.style.position = 'absolute';
			canvas.style.left = canvas.style.top = '0';
			var snowflakes = [],
				snowflake;
			for (i = 0; i < COUNT; i++) {
				snowflake = new Snowflake();
				snowflakes.push(snowflake);
			}

			function update() {
				ctx.clearRect(0, 0, width, height);
				for (i = 0; i < COUNT; i++) {
					snowflake = snowflakes[i];
					snowflake.y += snowflake.vy;
					snowflake.x += snowflake.vx;
					ctx.globalAlpha = snowflake.o;
					ctx.beginPath();
					ctx.fillStyle = '#FFF';
					ctx.arc(snowflake.x, snowflake.y, snowflake.r, 0, Math.PI * 2, false);
					ctx.closePath();
					ctx.fill();
					if (snowflake.y > height) {
						snowflake.reset();
					}
				}
				requestAnimationFrame(update);
			}
			update();
			window.addEventListener('resize', onResize, false);
		}
	}
});