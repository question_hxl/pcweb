/**
 * Created by 赵文东 on 2017/9/4.
 */
angular.module('urlServer',[])
    .service('apiUrlServer',function () {
        return {
            /**
             * 配置URL地址
             */
            baseUrl: '/BootStrap/Interface',
            resourceUrl: 'http://api.51jyfw.com:9080',
            gotoUrl:'http://www.51jyfw.com'
        }
    });
