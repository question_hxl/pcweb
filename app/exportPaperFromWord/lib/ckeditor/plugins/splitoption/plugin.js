/**
 * Created by 赵文东 on 2017/6/21.
 */
(function () {
    var pluginName = 'splitoption';
    var count = 1;
    var map = {};
    var ranges = [];
    var isInserted = false;
    CKEDITOR.plugins.add(pluginName, {
        icons: 'splitoption',
        init: function (editor) {
            editor.addCommand( 'splitoption', {
                exec: function( editor ) {
                    var self = this;
                    editor.on('selectionChange', function(e){
                        if(self.state === CKEDITOR.TRISTATE_ON && !isInserted) {
                            isInserted = true;
                            var $ele = CKEDITOR.dom.element.createFromHtml(createSplitor());
                            editor.insertElement($ele);
                            setTimeout(function() {
                                isInserted = false;
                            }, 200);
                        }
                    });
                    if(this.state === CKEDITOR.TRISTATE_OFF) {
                        this.setState(CKEDITOR.TRISTATE_ON);
                        // var gSplitorIns = editor.ui.instances.groupsplitor;
                        // if(gSplitorIns.getState() === CKEDITOR.TRISTATE_ON) {
                        //     gSplitorIns.setState(CKEDITOR.TRISTATE_OFF);
                        //     editor.ui.instances.groupsplitor._.state = CKEDITOR.TRISTATE_OFF;
                        // }
                    }else {
                        this.setState(CKEDITOR.TRISTATE_OFF);
                    }
                }
            });
            editor.ui.addButton( 'optionsplitor', {
                label: '切割选项',
                command: 'splitoption',
                toolbar: 'split',
                icon: this.path + '/icons/option-splitor.png'
            });
        }
    });
    function createSplitor(editor, count){
        return '<img class="qst-splitor" src="/dist/img/option-splitor.png">';
    }
    function createRange(editor, isStart){
        var range;
        var selection = editor.getSelection().getNative();
        if(isStart) {
            range = editor.createRange();
            range.setStart(new CKEDITOR.dom.element(selection.focusNode.parentElement), selection.focusOffset);
            ranges.push(range);
        }else {
            range = ranges[ranges.length - 1];
            range.setEnd(new CKEDITOR.dom.element(selection.focusNode.parentElement), selection.focusOffset);
        }
    }
    CKEDITOR.splitor = CKEDITOR.splitor || {
            getRanges: function(){
                return ranges;
            }
        };
})();