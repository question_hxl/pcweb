/**
 * Created by 贺小雷 on 2017-06-01.
 */
(function () {
    var pluginName = 'splitgroup';
    var count = 1;
    var map = {};
    var ranges = [];
    var isInserted = false;
	var self;
    CKEDITOR.plugins.add(pluginName, {
        icons: 'splitgroup',
        init: function (editor) {
			var mouseupListener = function(e){
				var key = e.data.$.button;
				var isLeftClick = key === 0;
				if(self.state === CKEDITOR.TRISTATE_ON && !isInserted && isLeftClick) {
					isInserted = true;
					var $ele = CKEDITOR.dom.element.createFromHtml(createSplitor());
					editor.insertElement($ele);
					setTimeout(function() {
						isInserted = false;
					}, 200);
				}
			};
            editor.addCommand( 'groupsplit', {
                exec: function( editor ) {
                    self = this;
                    if(this.state === CKEDITOR.TRISTATE_OFF) {
                        this.setState(CKEDITOR.TRISTATE_ON);
						editor.document.removeListener('mouseup', mouseupListener);
						editor.document.on('mouseup', mouseupListener);
                        var splitorIns = editor.ui.instances.splitor;
                        if(!!splitorIns && splitorIns.getState() === CKEDITOR.TRISTATE_ON) {
                            splitorIns.setState(CKEDITOR.TRISTATE_OFF);
							splitorIns._.feature.setState(CKEDITOR.TRISTATE_OFF);
                        }
                    }else {
                        this.setState(CKEDITOR.TRISTATE_OFF);
						editor.document.removeListener('mouseup', mouseupListener);
                    }
                }
            });
            editor.ui.addButton( 'groupsplitor', {
                label: '切割分组',
                command: 'groupsplit',
                toolbar: 'split',
                icon: this.path + '/icons/group-splitor.png'
            });
        }
    });
    function createSplitor(editor, count){
        return '<img class="group-splitor" src="/dist/img/group-splitor.png">';
    }
    function createRange(editor, isStart){
        var range;
        var selection = editor.getSelection().getNative();
        if(isStart) {
            range = editor.createRange();
            range.setStart(new CKEDITOR.dom.element(selection.focusNode.parentElement), selection.focusOffset);
            ranges.push(range);
        }else {
            range = ranges[ranges.length - 1];
            range.setEnd(new CKEDITOR.dom.element(selection.focusNode.parentElement), selection.focusOffset);
        }
    }
})();