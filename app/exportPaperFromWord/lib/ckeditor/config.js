/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'split'}
	];

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	// config.removeDialogTabs = 'image:advanced;link:advanced';
	config.pasteFromWordKeepsStructure = false;
	config.pasteFromWordRemoveStyle = false;
	config.pasteFromWordRemoveFontStyles = false;

    config.image_previewText = ' ';
	config.entities_greek = true;
	//是否转换一些拉丁字符为HTML
	config.entities_latin = true;
	//是否转换一些特殊字符为ASCII字符
	config.entities_processNumerical = false;
    CKEDITOR.dtd.$removeEmpty.span = 0;
    config.allowedContent = {
        $1: {
            elements: CKEDITOR.dtd,
            attributes: true,
            styles: false,
            classes: false
        }
    };

	config.filebrowserUploadUrl = '/BootStrap/Interface' + '/Interface0001A.ashx';
	config.extraPlugins = 'splitrichtext,splitgroup';
};
