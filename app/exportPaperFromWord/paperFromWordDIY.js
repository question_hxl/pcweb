/**
 * Created by 贺小雷 on 2017-06-01.
 */
angular.module('customService', ['ngDialog'])
    .service('constantService', function (ngDialog) {
        return {
            EMPTY_QFLNKID: "00000000-0000-0000-0000-000000000000",
            NoMap: {
                0: '',
                1: '一',
                2: '二',
                3: '三',
                4: '四',
                5: '五',
                6: '六',
                7: '七',
                8: '八',
                9: '九',
                10: '十'
            },
            PROVINCE: [
                '安徽', '澳门', '北京', '重庆', '福建', '甘肃', '广东', '广西', '贵州', '海南',
                '河北', '河南', '黑龙江', '湖北', '湖南', '吉林', '江苏', '江西', '辽宁', '内蒙古',
                '宁夏', '青海', '山东', '山西', '陕西', '上海', '四川', '台湾', '天津', '西藏', '香港',
                '新疆', '云南', '浙江'
            ],
            PUBLISH_CLUB: [
                {
                    id: '1',
                    name: '苏教版'
                }, {
                    id: '2',
                    name: '人教版'
                }, {
                    id: '3',
                    name: '苏科版'
                }, {
                    id: '4',
                    name: '牛津版'
                }
            ],
            getYearList: function () {
                var yearList = [];
                var year = new Date().getFullYear();
                for (var i = 0; i < 20; i++) {
                    yearList.push(year - i);
                }
                return yearList;
            },
            GRADE_LIST: [
                {
                    id: '1',
                    name: '一年级'
                }, {
                    id: '2',
                    name: '二年级'
                }, {
                    id: '3',
                    name: '三年级'
                }, {
                    id: '4',
                    name: '四年级'
                }, {
                    id: '5',
                    name: '五年级'
                }, {
                    id: '6',
                    name: '六年级'
                }, {
                    id: '7',
                    name: '七年级'
                }, {
                    id: '8',
                    name: '八年级'
                }, {
                    id: '9',
                    name: '九年级'
                }, {
                    id: '10',
                    name: '十年级'
                }, {
                    id: '11',
                    name: '十一年级'
                }, {
                    id: '12',
                    name: '十二年级'
                }
            ],
            XD_LIST: [{
                id: '1',
                name: '小学'
            }, {
                id: '2',
                name: '初中'
            }, {
                id: '3',
                name: '高中'
            }
            ],
            showTypeList: [{
                id: '2',
                name: '一行四列'
            }, {
                id: '1',
                name: '两行两列'
            }, {
                id: '0',
                name: '四行一列'
            }],
            getCNNoByIndex: function (index) {
                if (!index) {
                    return '';
                }
                var shang = Math.floor(index / 10),
                    yu = index % 10;
                var cnStr = '';
                if (shang > 1) {
                    cnStr = this.NoMap[shang] + this.NoMap[10] + this.NoMap[yu];
                } else if (shang === 1) {
                    cnStr = this.NoMap[10] + this.NoMap[yu];
                } else {
                    cnStr = this.NoMap[yu];
                }
                return cnStr;
            },
            showSimpleToast: (function () {
                return function (text, autoHideTime) {
                    autoHideTime = autoHideTime || 2000;
                    var dialogObj = ngDialog.open({
                        template: '<div class="toast-title">' + text + '</div>',
                        className: 'ngdialog-theme-default simple-toast',
                        closeByDocument: true,
                        plain: true
                    });
                    (function (obj) {
                        setTimeout(function () {
                            obj.close();
                        }, autoHideTime);
                    })(dialogObj);
                };
            })(),
            /**
             * confirm提示框service
             * @param {String} text     提示文本
             * @param {Array} [btns]   按钮名称，可选，第一个为确认按钮，第二个为取消按钮
             * @param {Function} doneCb   确认方法回调
             * @param {Function} [failCb] 可选，取消方法回调
             */
            showConfirm: function (text, btns, doneCb, failCb) {
                var okBtn = '删除', cancelBtn = '取消';
                if (_.isArray(btns)) {
                    if (btns[0]) {
                        okBtn = btns[0];
                    }
                    if (btns[1]) {
                        cancelBtn = btns[1];
                    }
                } else {
                    failCb = doneCb;
                    doneCb = btns;
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-default btn-cancel" ng-click="cancel()">' + cancelBtn + '</div>',
                        '<div class="btn btn-primary btn-ok" ng-click="done()">' + okBtn + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default simple-toast',
                    closeByDocument: false,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.cancel = function () {
                                dialogObj.close();
                                if (failCb && _.isFunction(failCb)) {
                                    failCb();
                                }
                            };
                            $scope.done = function () {
                                dialogObj.close();
                                if (doneCb && _.isFunction(doneCb)) {
                                    doneCb();
                                }
                            };
                        }
                    }
                });
            },

            /**
             * showMakeSure提示框service
             * @param {String} text 提示文本
             * @param   closeByDocument  是否点击关闭 默认false
             * @param {String} btnName  按钮名称，默认"确定"
             */
            showMakeSure: function (text, closeByDocument, btnName) {
                if (!$.trim(btnName)) {
                    btnName = "确定"
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-primary btn-ok" style="margin-right: -6%;" ng-click="sure()">' + btnName + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default simple-toast',
                    closeByDocument: closeByDocument || false,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.sure = function () {
                                dialogObj.close();
                            };
                        }
                    }
                });
            }
        }
    });
angular.module('myApp',['ngDialog','customService','urlServer'])
	.config(['$httpProvider', function($httpProvider){
		$httpProvider.defaults.withCredentials = true;
	}])
    .controller('paperDIYCtrl', function($scope, $http, ngDialog, constantService,$location,apiUrlServer){
    var search = formatUrlAttr($location.absUrl());
    window.wordContent = JSON.parse(window.localStorage.getItem('diyPaperData'));
    if(!!window.wordContent.content){
        $scope.htmlContent = window.wordContent.content;
    }else{
        //当没有内容时自动跳转回导入界面
        //$state.go('exportPaperFromWord');
        window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/paperFromWord.html?' +
            'isOpenNewWin=1&sessionId=' + isParamsEmpty(search.sessionId) + '&subjectId=' + isParamsEmpty(search.subjectId) + '&gradeNo=' +
            isParamsEmpty(search.gradeNo) + '&unifiedId=' + isParamsEmpty(search.unifiedId) + (!!isParamsEmpty(search.examNo) ? ('&examNo=' + isParamsEmpty(search.examNo)) : '') + (!!isParamsEmpty(search.examId) ? ('&examId='+isParamsEmpty(search.examId)) : '');
        return;
    }
    //初始化富文本编辑器
    CKEDITOR.replace('htmlEditor', {
        height: window.innerHeight - 106
    });
    var ins = CKEDITOR.instances["htmlEditor"];
    //分步状态 1 切分分组 2 切分题目 3 输入试卷名
    $scope.stepNum = 1;
    //是否需要弹出提示框 0 步骤1提示框 1 步骤2提示框
    $scope.needAlert = [true,true];

    /**
     * 切割完成后的结构
     * group 类型list
     * group |--- groupname 分组名
     *       |--- groupnamestr 原始分组名字符串
     *       |--- groupquestionstr 原始分组题目字符串
     *       |--- questionlist
     *            questionlist 类型 list
     *            questionlist |--- questiontitle
     *                         |--- questionoption
     */

    /**
     * 切割分组 <img class="group-splitor" src="/dist/img/group-splitor.png" />
     * 切割父子题 <img class="qst-splitor" src="/dist/img/ab-splitor.png" />
     * 切割题干  <img class="qst-splitor" src="/dist/img/qst-splitor.png" />
     * 切割选项 <img class="qst-splitor" src="/dist/img/option-splitor.png" />
     */
    $scope.rockAndRoll = function () {
        var grouplist = [];
        var firstsplitlist = [];
        //切割分组
        firstsplitlist = ins.getData().split(/<img[^>]*src=\"\/dist\/img\/group-splitor\.png\"[^>]*>/);
        if(firstsplitlist.length > 1){
            grouplist = getGruopList(firstsplitlist);
            //切割题目
            for(var i = 0; i<grouplist.length;i++){
                var tempqstlist = grouplist[i].groupquestionstr.split(/<img[^>]*src=\"\/dist\/img\/qst-splitor\.png\"[^>]*>/);
                grouplist[i].questionlist = getQuestionList(tempqstlist);
            }
        }else{
            /**
             * 直接切出所有题目
             */
            firstsplitlist = ins.getData().split(/<img[^>]*src=\"\/dist\/img\/qst-splitor\.png\"[^>]*>/);
            if(firstsplitlist.length > 1){
                for(var i = 0; i < firstsplitlist.length; i++){
                    firstsplitlist[i] = markTagIntact(firstsplitlist[i]);
                }
                /*$scope.questionlist = [];
                for(var i = 0; i < firstsplitlist.length; i++){
                    if(i % 2 !== 0){
                        $scope.questionlist.push(markTagIntact(firstsplitlist[i]));
                    }
                }*/
                grouplist.push({groupname:''});
                grouplist[0].questionlist = getQuestionList(firstsplitlist);
            }else{
                constantService.showSimpleToast('你划题的姿势不正确╮(╯▽╰)╭！');
                return;
            }
        }
        /*//切割每题选项
        for(var i = 0; i < $scope.grouplist.length; i++){
            var tempoptionlist = [];
            for(var j=0;j < $scope.grouplist[i].questionlist.length;j++){
                tempoptionlist = getQstOption($scope.grouplist[i].questionlist[j].optionstr);
                $scope.grouplist[i].questionlist[j].optionlist = tempoptionlist;
            }
        }*/
        //完成划题 跳转页面前让用户输入试卷名
        /*ngDialog.open({
            template: '<div class="editname">' +
            '<div class="editname_title">请输入试卷名</div>' +
            '<div class="input_area"><div>试卷名：<input class="input_name" ng-model="examname" ng-blur="checkIllagl()"></div></div>' +
            '<div class="btn_area"><button class="confirm_btn" ng-click="confirm()">确认</button><button class="cancel_btn" ng-click="cancel()">取消</button></div>' +
            '</div>',
            className: 'ngdialog-theme-default edite_examname',
            plain: true,
            closeByDocument: false,
            scope: $scope,
            controller: function($scope){
                return function () {
                    $scope.checkIllgel = function () {
                        $scope.examname = $.trim($scope.examname);
                        if($scope.examname.length >50){
                            constantService.showSimpleToast('试卷名称不能超过50个字符!');
                            return false;
                        }else{
                            return true;
                        }
                    };
                    $scope.confirm = function () {
                        if($scope.checkIllgel()){
                            var saveData = {Qlist:organizData(grouplist),content:ins.getData()};
                            if(isGroupHasQst(saveData.Qlist)){
                                constantService.showSimpleToast('存在无题目分组，请检查!');
                                return;
                            }
                            saveData.Qlist.unshift({dis:0,type:0,value:$scope.examname});
                            window.localStorage.setItem('diyPaperData', JSON.stringify(saveData));
                            window.location.href = 'http://jc1ki6uktqnucmf:3001/exportPaperFromWord/paperFromWord.html?' +
                                'isOpenNewWin=1&sessionId=' + search.sessionId + '&subjectId=' + search.subjectId + '&gradeNo=' +
                                search.gradeNo + '&unifiedId=' + search.unifiedId + '&examNo=' + search.examNo + '&action=2';
                            ngDialog.closeAll();
                        }
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll();
                    }
                }
            }
        });*/
        if($scope.checkIllagl()){
            var saveData = {Qlist:organizData(grouplist),content:ins.getData()};
            if(isGroupHasQst(saveData.Qlist)){
                constantService.showSimpleToast('存在无题目分组，请检查!');
                return;
            }
            saveData.Qlist.unshift({dis:0,type:0,value:$scope.examname});
            window.localStorage.setItem('diyPaperData', JSON.stringify(saveData));
            window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/paperFromWord.html?' +
                'isOpenNewWin=1&sessionId=' + isParamsEmpty(search.sessionId) + '&subjectId=' + isParamsEmpty(search.subjectId) + '&gradeNo=' +
                isParamsEmpty(search.gradeNo) + '&unifiedId=' + isParamsEmpty(search.unifiedId) + (!!isParamsEmpty(search.examNo) ? ('&examNo=' + isParamsEmpty(search.examNo)) : '') + (!!isParamsEmpty(search.examId) ? ('&examId='+isParamsEmpty(search.examId)) : '') + '&action=2';
        }
    };
    function markTagIntact(str) {
        var split_1 = [];//split('</')
        var split_2 = [];//标签名
        split_1 = str.split('</');
        _.each(split_1,function (item,index) {
            if(index>0){
                var tempindex = item.indexOf('>');
                split_2.push(item.slice(0,tempindex));
            }
        });
        split_2 = _.uniq(split_2);
        _.each(split_2,function (item) {
            var beforre = new RegExp('<[^>]*' + item + '[^>]*>');
            var lastre = new RegExp('<\/' + item + '[^>]*>');
            var tempsplit1 = str.split(lastre);
            if(tempsplit1.length>1){
                if(!beforre.test(tempsplit1[0])){
                    tempsplit1[0] = '<' + item + '>' + tempsplit1[0];
                }
                if(beforre.test(tempsplit1[tempsplit1.length-1])){
                    tempsplit1[tempsplit1.length-1] = tempsplit1[tempsplit1.length-1] + '</' + item + '>';
                }
            }
            /*if(tempsplit1.length>2){
                startAndEndCombin(1,tempsplit1,item,false);
            }
            startAndEndCombin(tempsplit1.length-1,tempsplit1,item,true);*/
            //首尾校验 首 需要校验第一个元素的最后一位字符是否是'<'
            //尾校验 需要校验倒数第二个元素的最后一位字符是不是'/' 并且还要校验最后一个元素的第一位字符是不是'<'
            /*if(tempsplit1.length>1){
                if(!(tempsplit1[0].charAt(tempsplit1[0].length-1) === '<')){
                    tempsplit1[0] = '>' + tempsplit1[0];
                    tempsplit1.unshift('<');
                }
                if(!((tempsplit1[tempsplit1.length-2].charAt(tempsplit1[tempsplit1.length-2].length-1) === '/') && (tempsplit1[tempsplit1.length-1].charAt(0) === '>'))){
                    tempsplit1[tempsplit1.length-1] = tempsplit1[tempsplit1.length-1] + '</';
                    tempsplit1.push('>');
                }
            }*/
            str = tempsplit1.join('</' + item + '>');
        });
        return str;
    }

    //头部合并 尾部合并
    function startAndEndCombin(scale,tempsplit1,key,fromend) {
        for(;tempsplit1.length>2;){
            if(!checkTagSplitLegality(tempsplit1[scale-1],tempsplit1[scale])){
                tempsplit1[scale] = tempsplit1[scale-1] + key + tempsplit1[scale];
                tempsplit1.splice(scale-1,scale);
                if(fromend){
                    scale = tempsplit1.length-1
                }
            }else{
                break;
            }
        }
    }

    //解析选项
    function getQstOption(str) {
        var orginOption = [];//第一次切割
        orginOption = str.split(/<img[^>]*src=\"\/dist\/img\/option-splitor\.png\"[^>]*>/);
        var templist = [];
        for(var i = 0; i < orginOption.length; i++){
            if(i % 2 != 0){
                templist.push(orginOption[i]);
            }
        }
        orginOption = templist;
        for(var i = 0; i < orginOption.length; i++){
            orginOption[i] = markTagIntact(orginOption[i]);
        }
        return orginOption;
    }
    //切割分组
    function getGruopList(list) {
        var tempgrouplist = [];
        var tempgroupname = '';
        var tempquestion = '';
        for(var i=0; i < list.length;i++){
            if(i > 0){
                if(i%2 !== 0){
                    tempgroupname = markTagIntact(list[i]);
                }else{
                    tempquestion = markTagIntact(list[i]);
                    tempgrouplist.push({groupname:tempgroupname,groupquestionstr:tempquestion});
                    tempgroupname = '';
                    tempquestion = '';
                }
            }
        }
        return tempgrouplist;
    }
    //切割题目
    function getQuestionList(list) {
        var afterfilter = [];
        var titlestr = '';
        var optionstr = '';
        for(var k = 0; k < list.length; k++){
            if(k>0){
                //双图片分割
                /*if(k%2 !== 0){
                    titlestr = markTagIntact(list[k]);
                }else{
                    optionstr = markTagIntact(list[k]);
                    afterfilter.push({questiontitle:titlestr,optionstr:optionstr});
                    titlestr = '';
                    optionstr = '';
                }*/
                //单图片分割
                titlestr = markTagIntact(list[k]);
                afterfilter.push({questiontitle:titlestr});
                titlestr = '';
            }
        }
        return afterfilter;
    }
    //组织数据
    function organizData(list) {
        var aimlist = [];
        for(var i = 0; i <list.length; i++){
            if(!!list[i].groupname){
                aimlist.push({dis:i+1,notes:'',type:1,value:list[i].groupname});
            }
            if(!!list[i].questionlist){
                for(var j = 0; j < list[i].questionlist.length; j++){
                    aimlist.push({dis:1+getQuestionDis(list,list[i].questionlist[j]),type:2,value:list[i].questionlist[j].questiontitle})
                }
            }
        }
        return aimlist;
    }
    function getQuestionDis(list,ele) {
        var qstlist = [];
        for(var i = 0; i<list.length;i++){
            qstlist = qstlist.concat(list[i].questionlist);
        }
        return _.findIndex(qstlist,ele);
    }

    /**
     * 如果分组下面没有题目 则需要阻止用户
     */
    function isGroupHasQst(list) {
        var indexlist = [];
        _.each(list,function (item,index) {
            if(item.type === 1){
                indexlist.push(index);
            }
        });
        if(indexlist.length>0){
            for(var i = 0; i < indexlist.length; i++){
                if(list[indexlist[i]+1].type === 1){
                    return true;
                }else{
                    continue;
                }
            }
            return false;
        }else{
            return false;
        }
    }

    /**
     * Check the html tag to split the legitimacy
     */
    function  checkTagSplitLegality(brele,ele) {
        //标签有两种 <></> </>
        //标签拆分后 前一个元素结尾应为'<' 后一个元素开头应为'>'或者开头前两位为'/>'
        if(brele.charAt(brele.length-1) === '<' || (ele.charAt(0) === '>' || (ele.length>2?ele.slice(0,2) === '/>':false))){
            return true;
        }else {
            return false;
        }
    }
    /**
     * 解析URL
     */
    function formatUrlAttr(url) {
        var paremersObj = new Object();
        var intsplit = url.split('?');
        var paramerslist = new Array();
        if(intsplit.length >= 2){
            paramerslist = intsplit[1].split('&');
        }else{
            return {};
        }
        if(paramerslist.length >= 2){
            for(var i = 0; i < paramerslist.length; i++){
                paramerslist[i] = paramerslist[i].split('=');
            }
            return _.object(paramerslist);
        }else{
            return {};
        }
    }
    $scope.$watch('stepNum',function (newVal,oldVal) {
        /*if(newVal === oldVal){
            return;
        }*/
        if(newVal === 3){
            return;
        }
        showStepAlert(newVal);
    });
    $scope.goStep = function (num) {
        if(!!num){
            $scope.stepNum = num;
        }else{
            $scope.stepNum = ($scope.stepNum < 3 ? ($scope.stepNum + 1) : 3);
        }
    };
    function showStepAlert(num) {
        if($scope.needAlert[num-1]){
            ngDialog.open({
                template: '<div class="tip_alert_area">' +
                '<div class="tip_alert_title" ng-bind="num === 1?\'添加分组标记\':\'添加题目标记\'">添加分组标记</div>' +
                '<div class="tip_alert_text">如下示意图，<span ng-show="num === 1">在每组<span style="color:#f90507">分组前和后</span>的位置各添加一个标记</span><span ng-show="num === 2">在<span style="color:#f90507;">每题头部</span>的位置添加<span style="color:#f90507;">1个标记</span></span></div>' +
                '<div style="width: 100%;text-align: center;margin-bottom: 10px;"><img class="tip_alert_img" ng-src="{{num === 1?\'../dist/img/splitgroup_tip_alert.png\':\'../dist/img/splitqst_tip_alert.png\'}}"/></div>' +
                '<div class="btn_area"><button class="confirm_btn" ng-click="start()">开 始</button></div>' +
                '</div>',
                className: 'ngdialog-theme-default split_alert_tip',
                plain: true,
                showClose: false,
                closeByEscape: false,
                closeByDocument: false,
                scope: $scope,
                controller: function($scope){
                    return function () {
                        $scope.num = num;
                        $scope.start = function () {
                            $scope.needAlert[num-1] = false;
                            ngDialog.closeAll();
                        }
                    }
                }
            });
        }else{
            return;
        }
    }
    $scope.checkIllagl = function () {
        $scope.examname = $.trim($scope.examname);
        if(!$scope.examname){
            constantService.showSimpleToast('请输入试卷名!');
            return false;
        }
        if($scope.examname.length >50){
            constantService.showSimpleToast('试卷名称不能超过50个字符!');
            return false;
        }else{
            return true;
        }
    };
    $scope.goBack = function () {
        window.history.go(-1);
    };
    /**
     * url为保护
     */
    function isParamsEmpty(params) {
        if(!!params && params != 'undefined' && params != 'null'){
            return params;
        }else{
            return ''
        }
    }
}).filter('trustHtml', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
});