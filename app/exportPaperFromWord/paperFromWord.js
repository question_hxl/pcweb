/**
 * Created by 贺小雷 on 2017-05-05.
 */
angular.module('customService', ['ngDialog'])
    .service('constantService', function (ngDialog) {
        return {
            EMPTY_QFLNKID: "00000000-0000-0000-0000-000000000000",
            NoMap: {
                0: '',
                1: '一',
                2: '二',
                3: '三',
                4: '四',
                5: '五',
                6: '六',
                7: '七',
                8: '八',
                9: '九',
                10: '十'
            },
            PROVINCE: [
                '安徽', '澳门', '北京', '重庆', '福建', '甘肃', '广东', '广西', '贵州', '海南',
                '河北', '河南', '黑龙江', '湖北', '湖南', '吉林', '江苏', '江西', '辽宁', '内蒙古',
                '宁夏', '青海', '山东', '山西', '陕西', '上海', '四川', '台湾', '天津', '西藏', '香港',
                '新疆', '云南', '浙江'
            ],
            PUBLISH_CLUB: [
                {
                    id: '1',
                    name: '苏教版'
                }, {
                    id: '2',
                    name: '人教版'
                }, {
                    id: '3',
                    name: '苏科版'
                }, {
                    id: '4',
                    name: '牛津版'
                }
            ],
            getYearList: function () {
                var yearList = [];
                var year = new Date().getFullYear();
                for (var i = 0; i < 20; i++) {
                    yearList.push(year - i);
                }
                return yearList;
            },
            GRADE_LIST: [
                {
                    id: '1',
                    name: '一年级'
                }, {
                    id: '2',
                    name: '二年级'
                }, {
                    id: '3',
                    name: '三年级'
                }, {
                    id: '4',
                    name: '四年级'
                }, {
                    id: '5',
                    name: '五年级'
                }, {
                    id: '6',
                    name: '六年级'
                }, {
                    id: '7',
                    name: '七年级'
                }, {
                    id: '8',
                    name: '八年级'
                }, {
                    id: '9',
                    name: '九年级'
                }, {
                    id: '10',
                    name: '十年级'
                }, {
                    id: '11',
                    name: '十一年级'
                }, {
                    id: '12',
                    name: '十二年级'
                }
            ],
            XD_LIST: [{
                id: '1',
                name: '小学'
            }, {
                id: '2',
                name: '初中'
            }, {
                id: '3',
                name: '高中'
            }
            ],
            showTypeList: [{
                id: '2',
                name: '一行四列'
            }, {
                id: '1',
                name: '两行两列'
            }, {
                id: '0',
                name: '四行一列'
            }],
            getCNNoByIndex: function (index) {
                if (!index) {
                    return '';
                }
                var shang = Math.floor(index / 10),
                    yu = index % 10;
                var cnStr = '';
                if (shang > 1) {
                    cnStr = this.NoMap[shang] + this.NoMap[10] + this.NoMap[yu];
                } else if (shang === 1) {
                    cnStr = this.NoMap[10] + this.NoMap[yu];
                } else {
                    cnStr = this.NoMap[yu];
                }
                return cnStr;
            },
            showSimpleToast: (function () {
                return function (text, autoHideTime) {
                    autoHideTime = autoHideTime || 2000;
                    var dialogObj = ngDialog.open({
                        template: '<div class="toast-title">' + text + '</div>',
                        className: 'ngdialog-theme-default simple-toast',
                        closeByDocument: true,
                        plain: true
                    });
                    (function (obj) {
                        setTimeout(function () {
                            obj.close();
                        }, autoHideTime);
                    })(dialogObj);
                };
            })(),
            /**
             * confirm提示框service
             * @param {String} text     提示文本
             * @param {Array} [btns]   按钮名称，可选，第一个为确认按钮，第二个为取消按钮
             * @param {Function} doneCb   确认方法回调
             * @param {Function} [failCb] 可选，取消方法回调
             */
            showConfirm: function (text, btns, doneCb, failCb) {
                var okBtn = '删除', cancelBtn = '取消';
                if (_.isArray(btns)) {
                    if (btns[0]) {
                        okBtn = btns[0];
                    }
                    if (btns[1]) {
                        cancelBtn = btns[1];
                    }
                } else {
                    failCb = doneCb;
                    doneCb = btns;
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-default btn-cancel" ng-click="cancel()">' + cancelBtn + '</div>',
                        '<div class="btn btn-primary btn-ok" ng-click="done()">' + okBtn + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default simple-toast',
                    closeByDocument: false,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.cancel = function () {
                                dialogObj.close();
                                if (failCb && _.isFunction(failCb)) {
                                    failCb();
                                }
                            };
                            $scope.done = function () {
                                dialogObj.close();
                                if (doneCb && _.isFunction(doneCb)) {
                                    doneCb();
                                }
                            };
                        }
                    }
                });
            },

            /**
             * showMakeSure提示框service
             * @param {String} text 提示文本
             * @param   closeByDocument  是否点击关闭 默认false
             * @param {String} btnName  按钮名称，默认"确定"
             */
            showMakeSure: function (text, closeByDocument, btnName) {
                if (!$.trim(btnName)) {
                    btnName = "确定"
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-primary btn-ok" style="margin-right: -6%;" ng-click="sure()">' + btnName + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default simple-toast',
                    closeByDocument: closeByDocument || false,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.sure = function () {
                                dialogObj.close();
                            };
                        }
                    }
                });
            }
        }
    });
angular.module('myApp',['ngCookies','ngDialog','customService','ngFileUpload','angular-loading-bar','urlServer'])
    .config(['$httpProvider','cfpLoadingBarProvider', function($httpProvider,cfpLoadingBarProvider){
        $httpProvider.defaults.withCredentials = true;
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div><span class="fa fa-spinner fa-pulse"></span></div>';
    }])
    .controller('paperFromWordCtrl', function($scope, $http,$location,$cookies, ngDialog, constantService, Upload,$q,apiUrlServer){
    $scope.isUploaded = false;
    $scope.importStatus = 0;//1 上传完成 2 上传失败 3 正在上传 0 未开始上传
    var ins;
    var search = formatUrlAttr($location.absUrl());
    //var search = $location.search();
    var action = isParamsEmpty(search.action);
    $scope.showIndex = false;

    //注册scope
    $scope.paperData = {
        paperName: '',
        datas: []
    };
    $scope.selectedQsts = [];
    $scope.groups = [];
    $scope.isAutoParse = true;
    $scope.selected = '';
    $scope.currentQst = '';
    $scope.currentGroup = '';
    $scope.currentSub = '';
    $scope.currentOrder = 1;
    var isNotifiedBeforeSetQtype = false;
    //原始数据状态
    var originDataObj = [];
    //操作数据堆栈
    var handleHisArr = [];

    $scope.importStatusTip = '上传进度';

    //存贮已有空白卷结构
    var blankPaper = [];

    //从手动话题跳转过来
    if(!!action && action === '2'){
        var diyPaperdata = window.localStorage.getItem('diyPaperData');
        if(!!diyPaperdata){
            $scope.isUploaded = true;
        }else{
            window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/paperFromWord.html?' +
                'isOpenNewWin=1&sessionId=' + isParamsEmpty(search.sessionId) + '&subjectId=' + isParamsEmpty(search.subjectId) + '&gradeNo=' +
                isParamsEmpty(search.gradeNo) + '&unifiedId=' + isParamsEmpty(search.unifiedId) + (!!isParamsEmpty(search.examNo) ? ('&examNo=' + isParamsEmpty(search.examNo)) : '')  + '&examId='+isParamsEmpty(search.examId);
        }
        window.wordContent = JSON.parse(diyPaperdata);
        parsePaperData(window.wordContent);
        blankPaper = [];
        if(!!search.examId && search.examId != 'null' && search.examId != 'undefined'){
            getPaperConfig(search.examId).then(function (res) {
                blankPaper = handleSubQst(res.data.msg);
            });
        }
    }
    function init(){
        var doc = ins.document;
        var groups = [{
            question: [{
                orders: '1',
                title: 'sss',
                startTagId: 'g_start_1',
                endTagId: 'g_end_1'
            },{
                orders: '2',
                title: 'sss',
                startTagId: 'g_start_2',
                endTagId: 'g_end_2'
            }]
        }];
        _.each(groups[0].question, function(q){
            var $start = doc.getById(q.startTagId), $end = doc.getById(q.endTagId);
            var range = ins.createRange();
            range.setStart($start, 0);
            range.setEnd($end, 0);
            var bookmark = range.createBookmark2();
            q.selection = [bookmark];
        });
        ins.getSelection().selectBookmarks(groups[0].question[0].selection).scrollIntoView();
    }
    //获取题型列表
    $http.post(apiUrlServer.baseUrl + '/getKmQuestionsTypeLimit.ashx').then(function(res){
        res.data.msg = handelQtype(res.data.msg);
        $scope.qtypes = res.data.msg;
        $scope.currentQtype = $scope.qtypes[0];
    });

    $scope.uploadDoc = function(fileDoc,errFiles){
        if (!fileDoc) {
            //constantService.showMakeSure("请选择一份word文档！");
            return;
        } else {
            if (fileDoc.name) {
                var scoFiletype = fileDoc.name.substr(fileDoc.name.lastIndexOf(".")).toLowerCase();
                if (!((scoFiletype === '.doc') || (scoFiletype === '.docx'))) {
                    constantService.showMakeSure("文档格式错误！");
                    return;
                }
            } else {
                constantService.showSimpleToast("文档格式错误！");
                return;
            }
        }
        fileDoc.upload = Upload.upload({
            url: apiUrlServer.resourceUrl + '/AgentDataSync/UploadServlet',
            data: {file: fileDoc, 'sessionId': isParamsEmpty(search.sessionId) || $cookies['sessionId']},
            ignoreLoadingBar: true
        });
        $scope.importStatus = 3;

        fileDoc.upload.then(function (response) {
            if(response.data.Code === '00') {
                if(!!response.data.Data){
                    $scope.importStatus = 1;
                    $scope.importStatusTip = '上传成功';
                    window.wordContent = JSON.parse(response.data.Data);
                    window.localStorage.setItem('diyPaperData', response.data.Data);
                    if($scope.isAutoParse && (!!window.wordContent.Qlist && window.wordContent.Qlist.length>0)) {
                        $scope.isUploaded = true;
                        $scope.importStatus = 0;
                        parsePaperData(JSON.parse(response.data.Data));
                        blankPaper = [];
                        if(!!search.examId && search.examId != 'null' && search.examId != 'undefined'){
                            getPaperConfig(search.examId).then(function (res) {
                                blankPaper = handleSubQst(res.data.msg);
                            });
                        }
                    }else {
                        ///window.wordContent = JSON.parse(response.data.Data);
                        //$state.go('exportPaperFromWordDIY');
                        window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/paperFromWordDIY.html?' +
                            'isOpenNewWin=1&sessionId=' + isParamsEmpty(search.sessionId) + '&subjectId=' + isParamsEmpty(search.subjectId) + '&gradeNo=' +
                            isParamsEmpty(search.gradeNo) + '&unifiedId=' + isParamsEmpty(search.unifiedId) + (!!isParamsEmpty(search.examNo) ? ('&examNo=' + isParamsEmpty(search.examNo)) : '')  + '&examId='+isParamsEmpty(search.examId);
                    }
                    storeOriginData();
                }else{
                    //$scope.importStatusTip = '请重新选择文件上传';
                    $scope.importStatus = 2;
                    $scope.importStatusTip = '转换失败';
                }
            }else {
                //constantService.showMakeSure(response.data.Msg);
                //$scope.importStatusTip = response.data.Msg;
                $scope.importStatus = 2;
                $scope.importStatusTip = '转换失败';
            }
        }, function (response) {
            $scope.importStatus = 2;
            $scope.importStatusTip = '上传失败';
            //constantService.showMakeSure('上传失败，请重试！');
        }, function (evt) {
            $scope.currentUploadProgress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total)) + '%';
            if(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)) === 100){
                $scope.importStatusTip = '正在转换...';
            }
            containerProg.animate(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)));
        });
    };
    $scope.createGroup = function(){
        ngDialog.open({
            template: 'addGroup',
            className: 'ngdialog-theme-default dialog-create-group',
            closeByDocument: false,
            scope: $scope,
            controller: function($scope){
                return function () {
                    $scope.doCreate = function(){
                        var group = {
                            Dtype: $scope.groupName,
                            qtype: $scope.groupType || '',
                            Explain: $scope.groupExplain,
                            title: '',
                            orders: '',
                            sub: [],
                            mode: '',
                            type: '1'
                        };
                        $scope.paperData.datas.push(group);
                        $scope.setCurrentData(group, $scope.paperData.datas.length - 1);
                        ngDialog.close();
                    };
                }
            }
        });
    };
    $scope.createQst = function(){
        var title = ins.getSelectedHtml().getHtml();
        //记录选中的ranges，以备核对使用
        var section = ins.getSelection();
        var bookmarks = section.createBookmarks2();
        var qst = {
            title: title,
            orders: $scope.currentOrder++,
            sub: [],
            mode: '',
            selection: bookmarks,
            type: '2'
        };
        $scope.paperData.datas.push(qst);
        $scope.setCurrentData(qst, $scope.paperData.datas.length - 1);
    };
    $scope.setCurrentData = function(data, index){
        $scope.selected = {
            data: data,
            index: index
        };
        if(data.type === '2' || data.type === '3') {
            $scope.currentQst = data;
            $scope.currentGroup = '';
            $scope.currentSub = '';
            //编辑器获取焦点，并且选中当前选中的文本，滚到到视口内
            ins.focus();
            ins.getSelection().selectBookmarks(data.selection).scrollIntoView();
        }else if(data.type === '1') {
            $scope.currentQst = '';
            $scope.currentSub = '';
            $scope.currentGroup = data;
        }
    };
    $scope.selectQst = function(q,e){
        e.stopPropagation();
        e.preventDefault();
        /*var index = _.findIndex($scope.selectedQsts, function(item){
            return item.title === q.title;
        });*/
        var index = _.findIndex($scope.selectedQsts,function (item) {
            return _.isEqual(item,q)
        });
        if(index >= 0) {
            var qst = $scope.selectedQsts[index];
            if($scope.selectedGroup && !$scope.selectedGroup.isFinish) {
                //如果当前分组处于编辑状态，移除选中题目时需要删除qtypeInfo属性
                delete qst.qtypeInfo;
            }
            $scope.selectedQsts.splice(index, 1);
            //如果选中的为父子
            if(!!q.sub && q.sub.length>0){
                addAllSub(q.sub,2);
            }
        }else {
            $scope.selectedQsts.push(q);
            if($scope.selectedGroup && !$scope.selectedGroup.isFinish) {
                //如果当前分组处于编辑状态，移除选中题目时需要删除qtypeInfo属性
                q.qtypeInfo = $scope.selectedGroup.qtypeInfo;
            }
            if(!!q.sub && q.sub.length>0){
                addAllSub(q.sub,1);
            }
        }
        $scope.selectedQsts = _.uniq($scope.selectedQsts);
    };
    $scope.isQstSelected = function(q){
        return !!_.find($scope.selectedQsts, function(item){
            return item.title === q.title && item.order === q.order;
        });
    };
    $scope.addQtype4Qst = function(){
        if($scope.selectedQsts.length === 0) {
            constantService.showSimpleToast('请先选择题目！');
            return;
        }
        if(!$scope.currentQtype){
            constantService.showSimpleToast('请选择题目类型!');
            return;
        }
        if(checkDuplicateQuestion()){
            constantService.showSimpleToast('存在已设置过类型的题目，请修改后再设置!');
            return;
        }
        if(!isNotifiedBeforeSetQtype) {
            ngDialog.open({
                template: '<div class="confirmQtype">' +
                '<div class="confirmQtype_title">设置题型</div>' +
                '<div class="confirmQtype_message">设置题型前，请确定试卷题目处理完毕，否则可能导致题型与题目不对应！</div>' +
                '<div class="btn_area"><button class="cancel_btn" ng-click="cancel()">取消</button><button class="confirm_btn" ng-click="confirm()">我已处理完毕</button></div>' +
                '</div>',
                className: 'ngdialog-theme-default paperfromword_confirm_Qtype',
                plain: true,
                closeByDocument: false,
                scope: $scope,
                controller: function($scope){
                    return function () {
                        $scope.cancel = function () {
                            ngDialog.closeAll();
                        };
                        $scope.confirm = function () {
                            ngDialog.closeAll();
                            //记录操作前数据状态
                            saveDataState();
                            isNotifiedBeforeSetQtype = true;
                            doAdd();
                        }
                    }
                }
            });
            /*constantService.showConfirm('设置题型前，请确定试卷题目处理完毕，否则可能导致题型与题目不对应！', ['我已处理完毕', '取消'], function(){
                //记录操作前数据状态
                saveDataState();
                $scope.isNotifiedBeforeSetQtype = true;
                doAdd();
            });*/
        }else {
            //记录操作前数据状态
            saveDataState();
            doAdd();
        }
        function doAdd(){
            //老王要求 不要将新分组合并到已有分组中 直接添加
            /*var existGroup = _.find($scope.groups, function(item){
                return item.qtypeInfo.QtypeId === $scope.currentQtype.QtypeId;
            });*/
            //添加检查
            if(blankPaper.length>0 && !checkPaperStructure(blankPaper,$scope.groups.length,$scope.selectedQsts,$scope.currentQtype)){
                return;
            }
            var existGroup = undefined;
            _.each($scope.selectedQsts, function(item){
                item.qtypeInfo = $scope.currentQtype;
            });
            if(existGroup) {
                existGroup.questions = existGroup.questions.concat(_.map($scope.selectedQsts, function(item){
                    return item;
                }));
            }else {
                var group = {
                    qtypeInfo: $scope.currentQtype,
                    questions: _.map($scope.selectedQsts, function(item){
                        return item;
                    }),
                    isFinish: true
                };
                $scope.groups.push(group);
            }
            $scope.selectedQsts = [];
        }
    };
    $scope.editGroup = function(g, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        g.isFinish = false;
        $scope.selectedGroup = g;
        $scope.selectedQsts = g.questions;
    };
    $scope.saveGroup = function(g, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        g.isFinish = true;
        g.questions = _.map($scope.selectedQsts, function(item){
            return item;
        });
        //sorting
        g.questions = _.sortBy(g.questions,'order');
        $scope.selectedGroup = null;
        $scope.selectedQsts = [];
    };
    $scope.selectGroup = function(g){
        if($scope.selectedGroup && $scope.selectedGroup.qtypeInfo.QtypeId === g.qtypeInfo.QtypeId && $scope.isSelecteGroupAndGroupsEqual($scope.selectedGroup,g)) {
            $scope.selectedGroup = null;
            $scope.selectedQsts = [];
        }else {
            $scope.selectedGroup = g;
            $scope.selectedQsts = _.map(g.questions, function(item){
                return item;
            });
        }
    };
    $scope.deleteGroup = function(e, g, index){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        _.each(g.questions, function(item){
            delete item.qtypeInfo;
        });
        $scope.groups.splice(index, 1);
    };
    $scope.selectGroupQsts = function(question, index,e){
        e.stopPropagation();
        e.preventDefault();
        var dataAfter = $scope.paperData.datas.slice(index + 1);
        var nextGroupIndex = _.findIndex(dataAfter, function(item){
            return item.type === '1'
        });
        if(nextGroupIndex === 0) {
            constantService.showSimpleToast('分组内无题目，请删除该分组，或者在分组内添加题目！');
            return ;
        }
        var groupQsts;
        if(nextGroupIndex > 0) {
            groupQsts = dataAfter.slice(0, nextGroupIndex);
        }else {
            groupQsts = dataAfter.slice(0);
        }
        if($scope.isQstSelected(groupQsts[0])) {
            $scope.selectedQsts = [];
        }else {
            $scope.selectedQsts = groupQsts;
        }
    };
    $scope.setToGroup = function(q, index, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        q.Dtype = q.origindtype || q.title;
        q.type = '1';
        setQstOrder($scope.paperData.datas);
    };
    $scope.setToQst = function(q, index, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        q.title = q.Dtype + (q.Explain || '');
        q.type = '2';
        setQstOrder($scope.paperData.datas);
    };
    $scope.setToFather = function(q, index, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        q.type = '2';
        q.mode = '';
        setQstOrder($scope.paperData.datas);
    };
    $scope.setToSub = function(q, index, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        q.type = '3';
        q.mode = '';
        setQstOrder($scope.paperData.datas);
    };
    $scope.addOptions = function(){
        var txt = ins.getSelectedHtml().getHtml();
        var ranges = ins.getSelection().createBookmarks2();
        if($scope.currentQst) {
            //如果当前题目有子题，则优先为子题添加选项
            if($scope.currentSub) {
                if(!$scope.currentSub.OptionOne) {
                    $scope.currentSub.OptionOne = txt;
                }else if(!$scope.currentSub.OptionTwo) {
                    $scope.currentSub.OptionTwo = txt;
                }else if(!$scope.currentSub.OptionThree) {
                    $scope.currentSub.OptionThree = txt;
                }else {
                    $scope.currentSub.OptionFour = txt;
                }
                $scope.currentSub.selection[0].end = ranges[0].end;
                $scope.currentSub.selection[0].endOffset  = ranges[0].endOffset ;
            }else {
                if(!$scope.currentQst.OptionOne) {
                    $scope.currentQst.OptionOne = txt;
                }else if(!$scope.currentQst.OptionTwo) {
                    $scope.currentQst.OptionTwo = txt;
                }else if(!$scope.currentQst.OptionThree) {
                    $scope.currentQst.OptionThree = txt;
                }else {
                    $scope.currentQst.OptionFour = txt;
                }
                $scope.currentQst.selection[0].end = ranges[0].end;
                $scope.currentQst.selection[0].endOffset  = ranges[0].endOffset ;
            }
        }else {
            constantService.showSimpleToast('请先创建题目！');
        }
    };
    $scope.addSubs = function(){
        var txt = ins.getSelectedHtml().getHtml();
        var section = ins.getSelection();
        var bookmarks = section.createBookmarks2();
        if($scope.currentQst) {
            var currentQstSubs = $scope.currentQst.sub;
            var sub = {
                title: txt,
                orders: '',
                selection: bookmarks
            };
            if(currentQstSubs) {
                sub.orders = currentQstSubs.length + 1
            }else {
                sub.orders = 1;
                $scope.currentQst.sub = [];
            }
            $scope.currentQst.mode = 'B';
            $scope.currentQst.sub.push(sub);
            $scope.currentSub = sub;
            //更新当前题目的区域
            $scope.currentSub.selection[0].end = bookmarks[0].end;
            $scope.currentSub.selection[0].endOffset  = bookmarks[0].endOffset ;
        }else {
            constantService.showSimpleToast('请先创建题目！');
        }
    };
    $scope.calcGroupOrder = function(data){
        var groups = _.filter($scope.paperData.datas, function(item){
            return item.type === '1';
        });
        var index = _.findIndex(groups, function(item){
            return item.Dtype === data.Dtype;
        });
        return constantService.getCNNoByIndex(index + 1);
    };
    $scope.setToModifySub = function(data){
        console.log(data);
    };
    $scope.splitContent = function(question, index,e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        $scope.toSplitContent = question.title;
        ngDialog.open({
            template: 'splitContentTemp',
            className: 'ngdialog-theme-default dialog-split-qst',
            closeByDocument: false,
            scope: $scope,
            onOpenCallback: function(){
                CKEDITOR.replace('splitBox');
            },
            controller: function($scope){
                return function () {
                    setTimeout(function(){
                        CKEDITOR.replace('splitBox',{extraPlugins : 'splitrichtext'});
                    }, 200);
                    $scope.doSplit = function(){
                        var splitIns = CKEDITOR.instances.splitBox;
                        var htmlStr = splitIns.getData();
                        /*var splitor = '<img class="qst-splitor" src="/dist/img/qst-splitor.png" />';
                        var qstsStrArr = htmlStr.split(splitor);*/
                        var qstsStrArr = htmlStr.split(/<img[^src>]*?class=\"qst-splitor\"[^src>]*?src=\"\/dist\/img\/qst-splitor.png\"[^src>]*?\>/);
                        if(qstsStrArr.length > 2 && !qstsStrArr[0]){
                            qstsStrArr.splice(0,1);
                        }
                        var qsts = _.map(qstsStrArr, function(item){
                            return {
                                title: item,
                                orders: '',
                                sub: [],
                                mode: (question.mode === 'A'?'A':''),
                                selection: '',
                                type: (question.type === '3' ? '3' : '2')
                            };
                        });
                        //
                        var tempindex = _.indexOf($scope.selectedQsts,question);
                        if(tempindex > 0){
                            $scope.selectedQsts.splice(tempindex,1);
                        }
                        Array.prototype.splice.apply($scope.paperData.datas, _.flatten([index, 1, qsts]));
                        setQstOrder($scope.paperData.datas);
                        ngDialog.close();
                    };
                }
            }
        });
    };
    $scope.delete = function(question, index, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        paperFromWardConfirm('删除提示','是否确认删除!',['删除','取消'],function () {
            $scope.paperData.datas.splice(index,1);
            //删除题干后其下子题自动变回父题
            if(_.isArray(question.sub) && question.sub.length > 0){
                turnToParent(question.sub);
            }
            setQstOrder($scope.paperData.datas);
            var thisIndex = _.findIndex($scope.selectedQsts, function(item){
                return item.title === question.title;
            });
            if(thisIndex >= 0) {
                $scope.selectedQsts.splice(thisIndex, 1);
            }
        });
        /*constantService.showConfirm('是否确认删除!',['删除','取消'],function () {
            $scope.paperData.datas.splice(index,1);
            //删除题干后其下子题自动变回父题
            if(_.isArray(question.sub) && question.sub.length > 0){
                turnToParent(question.sub);
            }
            setQstOrder($scope.paperData.datas);
            var thisIndex = _.findIndex($scope.selectedQsts, function(item){
                return item.title === question.title;
            });
            if(thisIndex >= 0) {
                $scope.selectedQsts.splice(thisIndex, 1);
            }
        });*/
    };
    $scope.mergeUp = function(question, index, e){
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        if(index > 0) {
            var preQst = $scope.paperData.datas[index - 1];
            var currentQst = $scope.paperData.datas[index];
            //查找出在选中题目中的索引
            var selePre = _.findIndex($scope.selectedQsts,function (item) {
                return _.isEqual(item,preQst);
            });
            var seleCurr = _.findIndex($scope.selectedQsts,function (item) {
                return _.isEqual(item,currentQst);
            });
            preQst.title = preQst.title + currentQst.title;
            $scope.paperData.datas.splice(index,1);
            setQstOrder($scope.paperData.datas);
            //处理选selectQsts数组
            if(!!selePre){
                $scope.selectedQsts[selePre] = preQst;
            }
            if(!!seleCurr){
                $scope.selectedQsts.splice(seleCurr,1);
            }
        }
    };
    $scope.savePaper = function(){
        if($scope.groups.length <= 0){
            constantService.showSimpleToast('请设置题目题型后再保存!');
            return;
        }
        //判断是否存在试卷名 如果不存在则弹出提示
        var temNameObj = _.find($scope.paperData.datas,function (item) {
            return (item.type+'') === '0';
        });
        if(!temNameObj){
            constantService.showSimpleToast('请输入试卷名后保存！');
            return;
        }
        if(!!temNameObj && ($.trim(temNameObj.title) === '请输入试卷名')){
            constantService.showSimpleToast('请输入试卷名后保存!');
            return;
        }
        /*var noQtypeInfoQst = _.find($scope.paperData.datas, function(item){
            return (item.type === '2' || item.type === '3') && !item.qtypeInfo;
        });*/
        $scope.noQtypeInfoQst = [];
       /*$scope.noQtypeInfoQst = _.filter($scope.paperData.datas,function (item) {
            return (item.type === '2' || item.type === '3') && !item.qtypeInfo;
        });*/
        for(var i = 0; i < $scope.paperData.datas.length; i++){
            if(($scope.paperData.datas[i].type === '2' || $scope.paperData.datas[i].type === '3') && !$scope.paperData.datas[i].qtypeInfo){
                $scope.noQtypeInfoQst.push(i);
            }
        }
        if($scope.noQtypeInfoQst.length > 0) {
            ngDialog.open({
                template: '<div class="confirm_save_paper">' +
                '<div class="confirm_save_paper_title">保存试卷</div>' +
                '<div class="confirm_save_paper_tip"><div>试卷上还有部分题目未设置题型,是否要删除这些未标记题目并保存试卷?</div><div style="font-size: 12px;line-height: 36px;" ng-bind="\'已标记\'+ ($parent.paperData.datas.length - $parent.noQtypeInfoQst.length) + \'题,未标记\' + $parent.noQtypeInfoQst.length + \'题\'"></div></div>' +
                '<div class="btn_area"><button class="cancel_btn" ng-click="cancel()">返回修改</button><button class="confirm_btn" ng-click="confirm()">删除提交</button></div>' +
                '</div>',
                className: 'ngdialog-theme-default paperfromword_confirm_savepaper',
                plain: true,
                closeByDocument: false,
                scope: $scope,
                controller: function($scope){
                    return function () {
                        $scope.confirm = function () {
                            ngDialog.closeAll();
                            findAndDelet($scope.noQtypeInfoQst);
                            handleSavePaper();
                        };
                        $scope.cancel = function () {
                            ngDialog.closeAll();
                        };
                    }
                }
            });
            /*constantService.showSimpleToast('存在部分题目未设置题型，请先全部设置题型后再保存试卷！');
            return;*/
        }else{
            handleSavePaper();
        }
    };

    //处理试卷保存
    function handleSavePaper() {
        var sessionId = isParamsEmpty(search.sessionId) || $cookies['sessionId'];
        var subjectId = isParamsEmpty(search.subjectId), gradeNo = isParamsEmpty(search.gradeNo), examNo = isParamsEmpty(search.examNo), unifiedId = isParamsEmpty(search.unifiedId);
        var examName = _.find($scope.paperData.datas, function(d){
            return d.type === '0'
        });
        var groups = [];
        var groupCount = 0;
        var lastGroup;
        var datas = parseMetaToPaperData($scope.paperData.datas);
        _.each(datas, function(g, index){
            var qList = [];
            _.each(g.qList, function(q){
                if(q.sub && q.sub.length > 0) {
                    _.each(q.sub, function(s, index){
                        var subQ = {
                            mode: q.mode === 'A'?'外部编号题':'內编编号题',
                            qDis: s.order + '',
                            title: s.title,
                            qType: s.qtypeInfo.Name
                        };
                        if(index === 0) {
                            subQ.tigan = q.title;
                        }
                        qList.push(subQ);
                    });
                }else {
                    qList.push({
                        mode: '独立题',
                        qDis: q.order + '',
                        title: q.title,
                        qType: q.qtypeInfo.Name
                    });
                }
            });
            groups.push({
                groupDis: (index + 1) + '',
                groupName: $.trim($('<div>' + g.Dtype + '</div>').text()),
                groupNote: $.trim($('<div>' + (g.Explain || '') + '</div>').text()),
                qList: qList
            });
        });
        // _.each($scope.paperData.datas, function(q){
        //     if(q.type === '1') {
        //         groupCount ++;
        //         groups.push({
        //             groupDis: groupCount + '',
        //             groupName: $.trim($('<div>' + q.Dtype + '</div>').text()),
        //             groupNote: $.trim($('<div>' + q.Explain + '</div>').text()),
        //             qList: []
        //         });
        //     }else if(q.type === '2') {
        //         lastGroup = groups[groups.length - 1];
        //         if(!lastGroup) {
        //             throw new Error('试卷内容异常，存在部分题目没有分组！');
        //         }else {
        //             lastGroup.qList.push({
        //                 mode: '独立题',
        //                 qDis: q.order + '',
        //                 title: q.title,
        //                 qType: q.qtypeInfo.Name
        //             });
        //         }
        //     }
        // });
        if(subjectId && subjectId != 'undefined' && subjectId != 'null') {
            $http.post('/BootStrap/schoolmanager/getMyUnifieds.ashx', {
                isParent: '0',
                targetId: unifiedId
            }).then(function (res) {
                var examType = res.data.msg[0].ExamType;
                //如果有学科信息，则直接调接口
                $http.post(apiUrlServer.resourceUrl + '/AgentDataSync/ExamImport2'+(!!sessionId?('?sessionId='+sessionId):''),{
                    sessionId: sessionId,
                    examName: $.trim($('<div>' + examName.title + '</div>').text()),
                    subjectId: subjectId,
                    examType: examType,
                    gradNo: gradeNo,
                    examNo: examNo,
                    groups: groups
                }).then(function(res){
                    //根据试卷号判断是否需要与统考关联
                    if(res.data.Code === '00') {
                        if(!examNo && !!unifiedId) {
                            //todo 关联统考
                            $http.post('/BootStrap/Interface/paperAssociateUnified.ashx', {
                                unifiedId: unifiedId,
                                examFLnkID: res.data.Data
                            }).then(function(resp){
                                if(resp.data.code === 0) {
                                    paperFromWardConfirm('导入成功提示','恭喜您，试卷导入成功！是否继续导入试卷答案？',['导入答案', '以后再说'], function(){
                                        window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/answerForPaper.html?sessionId='+ (isParamsEmpty(search.sessionId) || $cookies.sessionId || '') +'&examId='
                                            + res.data.Data + '&unifiedId=' + unifiedId;
                                    }, function(){
                                        window.close();
                                    });
                                }else {
                                    constantService.showSimpleToast('试卷与统考关联失败！');
                                }
                            });
                        }else {
                            paperFromWardConfirm('导入成功提示','恭喜您，试卷导入成功！是否继续导入试卷答案？',['导入答案', '以后再说'], function(){
                                window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/answerForPaper.html?sessionId='+ (isParamsEmpty(search.sessionId) || $cookies.sessionId || '') +'&examId='
                                    + res.data.Data + '&unifiedId=' + unifiedId;
                            }, function(){
                                window.close();
                            });
                        }
                    }else {
                        constantService.showSimpleToast('试卷提交失败！请检查试卷或重试！');
                    }
                });
            });
        }else {
            ngDialog.open({
                template: 'customize-paper-info',
                className: 'ngdialog-theme-default dialog-saveAs',
                closeByDocument: true,
                scope: $scope,
                controller: function ($scope) {
                    //弹出窗口用户自定义输入学科年级等相关信息
                    return function(){
                        getSaveInfo();
                        function getSaveInfo() {
                            $scope.pharseList = constantService.XD_LIST;
                            $scope.pharse = $scope.pharseList[0];
                            $scope.subjectList = [];
                            $scope.bankType = '1';

                            //初始化试卷类型
                            $http.get(apiUrlServer.baseUrl + '/Interface0196.ashx').then(function (res) {
                                if (_.isArray(res.data.msg)) {
                                    $scope.PaperTypeList = res.data.msg;
                                    $scope.paperType = $scope.PaperTypeList[0];
                                }
                            });
                            //初始化学校
                            $http.get(apiUrlServer.baseUrl + '/Interface0222.ashx').then(function (res) {
                                if (_.isArray(res.data.msg)) {
                                    $scope.schoolList = res.data.msg;
                                    $scope.chosenSchool = $scope.schoolList[0];
                                }
                            });
                            $scope.$watch('pharse', function(val){
                                if(val) {
                                    $http.post(apiUrlServer.baseUrl + '/Interface0220B.ashx', {
                                        pharseId: val.id
                                    }).then(function(res){
                                        if(_.isArray(res.data.msg)) {
                                            $scope.subjectList = res.data.msg;
                                            $scope.subject = $scope.subjectList[0];
                                            getgradeList();
                                        }else {
                                            $scope.subjectList = [];
                                            $scope.subject = $scope.subjectList[0];
                                        }
                                    });
                                }
                            });
                            /*$scope.$watch('subject', function(val){
                                if(val) {
                                    $http.post($rootScope.baseUrl + '/Interface0290.ashx', {
                                        pharseId: $scope.pharse.id,
                                        subjectId: val.subjectId
                                    }).then(function(res){
                                        if(_.isArray(res.data.course)) {
                                            $scope.gradeList = _.filter(res.data.course, function(g){
                                                return g.gradeNum !== '0';
                                            });
                                            $scope.grade = $scope.gradeList[0];
                                        }else {
                                            $scope.gradeList = [];
                                            $scope.grade = undefined;
                                        }
                                    });
                                    setTeacherList();
                                }
                            }, true);*/
                            function getgradeList() {
                                $http.post(apiUrlServer.baseUrl + '/Interface0290.ashx', {
                                    pharseId: $scope.pharse.id,
                                    subjectId: $scope.subject.subjectId
                                }).then(function(res){
                                    if(_.isArray(res.data.course)) {
                                        $scope.gradeList = _.filter(res.data.course, function(g){
                                            return g.gradeNum !== '0';
                                        });
                                        $scope.grade = $scope.gradeList[0];
                                    }else {
                                        $scope.gradeList = [];
                                        $scope.grade = undefined;
                                    }
                                });
                                setTeacherList();
                            }
                            $scope.$watch('chosenSchool', function () {
                                setTeacherList();
                            }, true);
                            $scope.$watch('grade', setTeacherList, true);
                            function setTeacherList() {
                                //初始化老师
                                if ($scope.chosenSchool && $scope.subject && $scope.grade) {
                                    $http.post(apiUrlServer.baseUrl + '/Interface0223.ashx', {
                                        schoolFLnkId: $scope.chosenSchool.schoolFLnkId,
                                        subjectFLnkID: $scope.subject.subjectFLnkId,
                                        gradeId: $scope.grade.gradeId
                                    }).success(function (res) {
                                        if (_.isArray(res.msg)) {
                                            $scope.teacherList = res.msg;
                                            $scope.chosenTeacher = $scope.teacherList[0];
                                        } else {
                                            $scope.teacherList = [];
                                            $scope.chosenTeacher = {};
                                        }
                                    });
                                }
                            }
                        }
                        $scope.modifyPharse = function(pharse){
                            $scope.pharse = pharse;
                        };
                        $scope.modifySub = function(sub){
                            $scope.subject = sub;
                        };
                        $scope.modifyGrade = function(grade){
                            $scope.grade = grade;
                        };
                        $scope.modifyType = function(type) {
                            $scope.paperType = type;
                        };
                        $scope.doSave = function(){
                            $http.post(apiUrlServer.resourceUrl + '/AgentDataSync/ExamImport2'+(!!sessionId?('?sessionId='+sessionId):''),{
                                sessionId: sessionId,
                                examName: $.trim($('<div>' + examName.title + '</div>').text()),
                                subjectId: $scope.subject.subjectId,
                                examType: $scope.paperType.ExType,
                                gradNo: $scope.grade.gradeNum,
                                examNo: examNo,
                                groups: groups
                            }).then(function(res){
                                if(res.data.Code === '00') {
                                    ngDialog.close();
                                    paperFromWardConfirm('导入成功提示','恭喜您，试卷导入成功！是否继续导入试卷答案？',['导入答案', '以后再说'], function(){
                                        window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/answerForPaper.html?sessionId='+ (isParamsEmpty(search.sessionId) || $cookies.sessionId || '') +'&examId='
                                            + res.data.Data;
                                    }, function(){
                                        window.close();
                                    });
                                    /*constantService.showConfirm('恭喜您，试卷导入成功！是否继续导入试卷答案？',['导入答案', '以后再说'], function(){
                                        window.location.href = '#/answerForPaper?sessionId='+ (search.sessionId || $cookies.sessionId || '') +'&examId='
                                            + res.data.Data;
                                    }, function(){
                                        window.close();
                                    });*/
                                }else {
                                    constantService.showSimpleToast(res.data.Msg);
                                }
                            }, function(){
                                constantService.showSimpleToast('保存试卷失败，请重试！');
                            });
                        };
                    };
                }
            });
        }
    }

    $scope.reExport = function(){
        window.location.href = apiUrlServer.gotoUrl+ '/exportPaperFromWord/paperFromWord.html?' +
            'isOpenNewWin=1&sessionId=' + isParamsEmpty(search.sessionId) + '&subjectId=' + isParamsEmpty(search.subjectId) + '&gradeNo=' +
            isParamsEmpty(search.gradeNo) + '&unifiedId=' + isParamsEmpty(search.unifiedId) + (!!isParamsEmpty(search.examNo) ? ('&examNo=' + isParamsEmpty(search.examNo)) : '')  + '&examId='+isParamsEmpty(search.examId);
        $scope.isUploaded = false;
        isNotifiedBeforeSetQtype = false;
        //清空右侧组数据
        $scope.groups = [];
        //清空历史操作数据
        handleHisArr = [];
        //滚动条回到最上
        $('.paper-from-word .paper-view').scrollTop(0);
    };
    $scope.$watch('currentQst.sub', function(val){
        $scope.toModifySub = _.find(val, function(item){
            return !!item.isModifying;
        });
    }, true);
    $scope.updateDtype = function(e, question){
        var target = $(e.target);
        if(target.text().length > 30){
            constantService.showSimpleToast('输入内容大于限制长度，请重新输入!');
            target.html('');
            return;
        }
        question.Dtype = target.html();
        question.origindtype = target.html();
    };
    $scope.updateExplain = function(e, question){
        var target = $(e.target);
        if(target.text().length > 30){
            constantService.showSimpleToast('输入内容大于限制长度，请重新输入!');
            target.html('');
            return;
        }
        question.Explain = target.html();
    };
    $scope.updateExamName = function(e, question){
        var target = $(e.target);
        question.title = target.html();
    };
    $scope.checkQst = function(e, q){
        e.stopPropagation();
        //页面滚动到当前题目位置；
        var $container = $('.paper-parser-auto .paper-view'), $ele = $container.find('#q_' + q.order);
        if($scope.toCheckQst && $scope.toCheckQst.order === q.order) {
            $scope.toCheckQst = null;
        }else {
            $scope.toCheckQst = q;
            if($ele.length > 0) {
                var containerOffsetTop = $container.offset().top,
                    containerScrollTop = $container.scrollTop(),
                    eleOffsetTop = $ele.offset().top + containerScrollTop;
                $container.scrollTop(eleOffsetTop - containerOffsetTop);
            }
        }

    };
    function parsePaperData(data){
        makeUpPaperName(data.Qlist);
        var meta = _.filter(data.Qlist, function(item){
            return item.value !== '';
        });
        $scope.paperData.datas = _.map(meta, function(c){
            var res;
            if(+c.type === 0) {
                res = {
                    title: c.value,
                    type: '0'
                }
            }else if(+c.type === 1) {
                res = {
                    Dtype: c.value,
                    Explain: c.notes,
                    orders: c.dis,
                    sub: [],
                    mode: '',
                    selection: '',
                    type: '1',
                    origindtype:c.value
                }
            }else if(+c.type === 2 || +c.type === 3) {
                res = {
                    title: c.value,
                    orders: c.dis,
                    sub: [],
                    mode: '',
                    selection: '',
                    type: c.type + ''
                }
            }
            return res;
        });
        setQstOrder($scope.paperData.datas);
        $scope.htmlContent = data.content;
        //初始化富文本编辑器
        CKEDITOR.replace('htmlEditor', {
            height: window.innerHeight - 106
        });
        ins = CKEDITOR.instances["htmlEditor"];
        // ins.on('instanceReady', init);
    }
    function setQstOrder(data){
        // var qsts = _.filter(data, function(item){
        //     return item.type === '2';
        // });
        // _.each(qsts, function(item, index){
        //     item.order = index + 1;
        // });
        var qsts = [];
        _.each(data, function(item, index){
            item.sub = [];
            if(item.type === '2') {
                qsts.push(item);
            }else if(item.type === '3') {
                var qstBefore = qsts[qsts.length - 1];
                qstBefore.sub = qstBefore.sub || [];
                qstBefore.sub.push(item);
            }
        });
        var _index = 0;
        _.each(qsts, function(q, index){
            q.order = index + 1 + _index;
            if(q.sub && q.sub.length > 0) {
                _.each(q.sub, function(s, subIndex){
                    if(q.mode !== 'A'){
                        s.order = q.order + '.' + (subIndex + 1);
                    }else{
                        s.order = q.order + subIndex;
                    }
                });
                if(q.mode === 'A'){
                    _index = _index + q.sub.length - 1;
                }
            }
        });
    }
    function parseMetaToPaperData(list){
        var result = [];
        var groupBefore, qBefore;
        _.each(list, function(item){
            item.sub = [];
            item.qList = null;
            if(item.type === '1') {
                result.push(item)
            }else if(item.type === '2') {
                groupBefore = result[result.length - 1];
                if(groupBefore) {
                    groupBefore.qList = groupBefore.qList || [];
                    groupBefore.qList.push(item);
                }
            }else if(item.type === '3') {
                groupBefore = result[result.length - 1];
                qBefore = groupBefore.qList[groupBefore.qList.length - 1];
                qBefore.sub = qBefore.sub || [];
                qBefore.sub.push(item);
            }
        });
        return result;
    }
    $scope.goDIYPage = function () {
        //$state.go('exportPaperFromWordDIY');
        window.location.href = apiUrlServer.gotoUrl + '/exportPaperFromWord/paperFromWordDIY.html?' +
            'isOpenNewWin=1&sessionId=' + isParamsEmpty(search.sessionId) + '&subjectId=' + isParamsEmpty(search.subjectId) + '&gradeNo=' +
            isParamsEmpty(search.gradeNo) + '&unifiedId=' + isParamsEmpty(search.unifiedId) + (!!isParamsEmpty(search.examNo) ? ('&examNo=' + isParamsEmpty(search.examNo)) : '') + (!!isParamsEmpty(search.examId) ? ('&examId='+isParamsEmpty(search.examId)) : '');
    };

    //进行操作的时候保存前十次操作数据信息
    function saveDataState() {
        if(handleHisArr.length === 10){
            handleHisArr.splice(0,1);
        }
        //深度复制数据存储在originDataObj中
        var temp = {
            /*_currentQtype:copyObject(true,{},$scope.currentQtype),*/
            _toModifySub:copyObject(true,{},$scope.toModifySub),
            _currentSub:copyObject(true,{},$scope.currentSub),
            _currentGroup:copyObject('',$scope.currentGroup),
            _currentQst:copyObject(true,{},$scope.currentQst),
            _selected:copyObject(true,{},$scope.selected),
            _groups:copyObject(true,[],$scope.groups),
            _selectedQsts:copyObject(true,[],$scope.selectedQsts),
            _paperData:copyObject(true,{},$scope.paperData)
        };
        handleHisArr.push(temp);
    }

    //回退操作
    $scope.undoHandle = function() {
        if(handleHisArr.length > 0){
            var temp = handleHisArr[handleHisArr.length-1];
            /*$scope.currentQtype = temp._currentQtype;*/
            $scope.toModifySub = temp._toModifySub;
            $scope.currentSub = temp._currentSub;
            $scope.currentGroup = temp._currentGroup;
            $scope.currentQst = temp._currentQst;
            $scope.selected = temp._selected;
            $scope.groups = temp._groups;
            $scope.selectedQsts = temp._selectedQsts;
            $scope.paperData = temp._paperData;
            handleHisArr.pop();
        }
    };
    function storeOriginData() {
        /*originDataObj._currentQtype = copyObject(true,{},$scope.currentQtype);*/
        originDataObj._toModifySub = copyObject(true,{},$scope.toModifySub);
        originDataObj._currentSub = copyObject(true,{},$scope.currentSub);
        originDataObj._currentGroup = copyObject('',$scope.currentGroup);
        originDataObj._currentQst = copyObject(true,{},$scope.currentQst);
        originDataObj._selected = copyObject(true,{},$scope.selected);
        originDataObj._groups = copyObject(true,[],$scope.groups);
        originDataObj._selectedQsts = copyObject(true,[],$scope.selectedQsts);
        originDataObj._paperData = copyObject(true,{},$scope.paperData);
    }
    //还原初始状态
    $scope.resetData = function () {
        /*$scope.currentQtype = originDataObj._currentQtype;*/
        $scope.toModifySub = originDataObj._toModifySub;
        $scope.currentSub = originDataObj._currentSub;
        $scope.currentGroup = originDataObj._currentGroup;
        $scope.currentQst = originDataObj._currentQst;
        $scope.selected = originDataObj._selected;
        $scope.groups = originDataObj._groups;
        $scope.selectedQsts = originDataObj._selectedQsts;
        $scope.paperData = originDataObj._paperData;
    };

    //深度复制Object
    function copyObject () {
        var i = 1,
            target = arguments[0] || {},
            deep = false,
            length = arguments.length,
            name, options, src, copy,
            copyIsArray, clone;

        // 如果第一个参数的数据类型是Boolean类型
        // target往后取第二个参数
        if (typeof target === 'boolean') {
            deep = target;
            // 使用||运算符，排除隐式强制类型转换为false的数据类型
            // 如'', 0, undefined, null, false等
            // 如果target为以上的值，则设置target = {}
            target = arguments[1] || {};
            i++;
        }

        // 如果target不是一个对象或数组或函数
        if (typeof target !== 'object' && !(typeof target === 'function')) {
            target = {};
        }

        // 如果arguments.length === 1 或
        // typeof arguments[0] === 'boolean',
        // 且存在arguments[1]，则直接返回target对象
        if (i === length) {
            return target;
        }

        // 循环每个源对象
        for (; i < length; i++) {
            // 如果传入的源对象是null或undefined
            // 则循环下一个源对象
            if (typeof (options = arguments[i]) != null) {
                // 遍历所有[[emuerable]] === true的源对象
                // 包括Object, Array, String
                // 如果遇到源对象的数据类型为Boolean, Number
                // for in循环会被跳过，不执行for in循环
                for (name in options) {
                    // src用于判断target对象是否存在name属性
                    src = target[name];
                    // copy用于复制
                    copy = options[name];
                    // 判断copy是否是数组
                    copyIsArray = Array.isArray(copy);
                    if (deep && copy && (typeof copy === 'object' || copyIsArray)) {
                        if (copyIsArray) {
                            copyIsArray = false;
                            // 如果目标对象存在name属性且是一个数组
                            // 则使用目标对象的name属性，否则重新创建一个数组，用于复制
                            clone = src && Array.isArray(src) ? src : [];
                        } else {
                            // 如果目标对象存在name属性且是一个对象
                            // 则使用目标对象的name属性，否则重新创建一个对象，用于复制
                            clone = src && typeof src === 'object' ? src : {};
                        }
                        // 深复制，所以递归调用copyObject函数
                        // 返回值为target对象，即clone对象
                        // copy是一个源对象
                        target[name] = copyObject(deep, clone, copy);
                    } else if (copy !== undefined){
                        // 浅复制，直接复制到target对象上
                        target[name] = copy;
                    }
                }
            }
        }
        // 返回目标对象
        return target;
    }

    /**
     * 百分比进度条
     */
    var containerProg = $('#indicatorContainer').radialIndicator({
        radius : 65,
        percentage :true,
        barColor : "#108fe9",
        initValue : 0,
        fontColor:"#919191",
        barWidth:7,
        roundCorner : true
    }).data('radialIndicator');
    $scope.$watch('importStatus',function(newVal,oldVal)  {
        if(newVal === oldVal){
            return;
        }
        var timer1=window.setInterval(function(){
            window.clearInterval(timer1);
            switch (newVal){
                case 1:
                    containerProg.option('barColor','#00a954');
                    containerProg.option('displayNumber',false);
                    containerProg.animate(100);
                    break;
                case 2:
                    containerProg.option('barColor','#f14034');
                    containerProg.option('displayNumber',false);
                    containerProg.animate(70);
                    break;
                case 3:
                    containerProg.option('barColor','#108fe9');
                    containerProg.option('displayNumber',true);
                    containerProg.animate(0);
                    break;
                case 0:
                    containerProg.option('barColor','#108fe9');
                    containerProg.option('displayNumber',true);
                    containerProg.animate(0);
                    break;
            }
        },50);
    });
    //Modify the type of question
    $scope.modifyQtype = function (g,e) {
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        ngDialog.open({
            template: '<div class="modifytype">' +
            '<div class="modifytype_title">请选择题型</div>' +
            '<div class="input_area"><div>试卷名：<select ng-options="qtype.showTypeName for qtype in qtypes" ng-model="selectMQtype"></select></div></div>' +
            '<div class="btn_area"><button class="confirm_btn" ng-click="confirm()">确认</button><button class="cancel_btn" ng-click="cancel()">取消</button></div>' +
            '</div>',
            className: 'ngdialog-theme-default paperfromword_modify_qtype',
            plain: true,
            closeByDocument: false,
            scope: $scope,
            controller: function($scope){
                return function () {
                    for(var i = 0; i < $scope.qtypes.length; i++){
                        if($scope.qtypes[i].QtypeId === g.qtypeInfo.QtypeId){
                            $scope.selectMQtype = $scope.qtypes[i];
                            break;
                        }
                    }
                    $scope.confirm = function () {
                        var _index = _.findIndex($scope.groups,function (item) {
                            return _.isEqual(item,g)
                        });
                        if(_index > -1){
                            $scope.groups[_index].qtypeInfo = $scope.selectMQtype;
                            ngDialog.closeAll();
                        }else{
                            constantService.showSimpleToast('设置失败');
                        }
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll();
                    };
                }
            }
        });
    };
    /**
     * 将获取到的题目类型中 选择题展示为 单选题 非选择题展示为解答题及其他
     * 修改后仅作为展示使用 不影响平台交互及功能 故增加字段以示区分
     */
    function handelQtype(list) {
        _.each(list,function (item) {
            if(item.QtypeId === 338){
                item.showTypeName = '单选题';
            }else if(item.QtypeId === 307){
                item.showTypeName = '解答题等其他';
            }else{
                item.showTypeName = item.Name;
            }
        });
        return list;
    }

    /**
     * 删除题干后其下字体自动变回父题
     */
    function turnToParent(list) {
        for(var i = 0; i < $scope.paperData.datas.length; i++){
            for(var j = 0; j < list.length; j++){
                if(_.isEqual($scope.paperData.datas[i], list[j])){
                    $scope.paperData.datas[i].type = '2';
                    setQstOrder($scope.paperData.datas);
                }
            }
        }
    }

    /**
     * 如果存在未设置类型的题目，用户选择删除并保存后，删除未设置类型题目
     */
    function findAndDelet(list) {
        for(var i = list.length - 1; i >= 0; i--){
            $scope.paperData.datas.splice(list[i],1);
        }
        //删除没有题目分组
        var indexlist = [];
        _.each($scope.paperData.datas.length,function (item,index) {
            if(item.type === 1){
                indexlist.push(index);
            }
        });
        if(indexlist.length>0){
            for(var j = indexlist.length-1; j >= 0; j--){
                if(indexlist[j] === $scope.paperData.datas.length-1){
                    $scope.paperData.datas.splice(indexlist[j],1);
                }else if($scope.paperData.datas[indexlist[j]-1].type === 1){
                    $scope.paperData.datas.splice(indexlist[j]-1,1);
                }else{
                    continue;
                }
            }
        }
    }

    /**
     * 更改父子题类型 A B两种类型
     */
    $scope.changeMode = function (q,index,e) {
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        q.mode = ((q.mode === 'B' || q.mode === '')?'A':'B');
        setQstOrder($scope.paperData.datas);
    };
    /**
     * 标题向上合并
     */
    $scope.titleMergeUp = function (q,index,e) {
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        if(index > 0) {
            var preQst = $scope.paperData.datas[index - 1];
            var currentQst = $scope.paperData.datas[index];
            preQst.Dtype = preQst.Dtype + currentQst.Dtype;
            $scope.paperData.datas.splice(index,1);
            setQstOrder($scope.paperData.datas);
        }
    };
    /**
     * 去除所有子题
     */
    $scope.removeAllSub = function (q,index,e) {
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        for(;q.sub.length>0;){
            var i = q.sub.length-1;
            if(i>=0){
                q.sub[i].type = '2';
                q.sub[i].mode = '';
                setQstOrder($scope.paperData.datas);
            }
        }
    };
    /**
     * 合并所有子题
     */
    $scope.mergeUpAllSub = function (q,index,e) {
        //记录操作前数据状态
        saveDataState();
        e.stopPropagation();
        e.preventDefault();
        for(;q.sub.length>0;){
            var _index = _.findIndex($scope.paperData.datas,function (item) {
                return _.isEqual(item,q.sub[q.sub.length-1])
            });
            var preQst = $scope.paperData.datas[_index - 1];
            var currentQst = $scope.paperData.datas[_index];
            preQst.title = preQst.title + currentQst.title;
            $scope.paperData.datas.splice(_index,1);
            setQstOrder($scope.paperData.datas);
        }
    };
    /**
     * 选择父题后默认全选其下所有子题
     * flag: 1 add 2 remove
     */
    function addAllSub(list,flag) {
        if(flag === 1){
            for(var j = 0; j < list.length; j++){
                var tep = _.find($scope.selectedQsts,list[j]);
                if(!!tep){
                    continue;
                }else{
                    $scope.selectedQsts.push(list[j]);
                }
            }
        }else{
            for(var i = list.length-1;i>=0;i--){
                var _index = _.findIndex($scope.selectedQsts,function (item) {
                    return _.isEqual(item.list[i])
                });
                if(_index>=0){
                    $scope.selectedQsts.splice(_index,1);
                }
            }
        }

    }

    /**
     * confirmAlert
     */
    function paperFromWardConfirm (title,text, btns, doneCb, failCb) {
        var okBtn = '删除', cancelBtn = '取消';
        if (_.isArray(btns)) {
            if (btns[0]) {
                okBtn = btns[0];
            }
            if (btns[1]) {
                cancelBtn = btns[1];
            }
        } else {
            failCb = doneCb;
            doneCb = btns;
        }
        var dialogObj = ngDialog.open({
            template:'<div class="toast-title" ng-bind="title" ng-if="!!title"></div>'+
                '<div class="toast-tip" ng-bind="text"></div>'+
                '<div class="button-group">'+
                '<button class="btn btn-default btn-cancel" ng-click="cancel()">' + cancelBtn + '</button>'+
                '<button class="btn btn-primary btn-ok" ng-click="done()">' + okBtn + '</button>'+
                '</div>',
            className: 'ngdialog-theme-default paperfromword_confirmdialog',
            closeByDocument: false,
            plain: true,
            controller: function ($scope) {
                return function () {
                    $scope.title = title;
                    $scope.text = text;
                    $scope.cancel = function () {
                        dialogObj.close();
                        if (failCb && _.isFunction(failCb)) {
                            failCb();
                        }
                    };
                    $scope.done = function () {
                        dialogObj.close();
                        if (doneCb && _.isFunction(doneCb)) {
                            doneCb();
                        }
                    };
                }
            }
        });
    }

    /**
     * Check whether there is a duplicate title in the selected title
     * set question type
     */
    function checkDuplicateQuestion() {
        var flag = false;
        if($scope.selectedQsts.length>0){
            for(var i = 0; i < $scope.selectedQsts.length; i++){
                if(!!$scope.selectedQsts[i].qtypeInfo){
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 支持百位以内的正整数转换
     * @param index 序号
     */
    $scope.characterEn_ChConversion = function(index) {
        var chlist = ['','一', '二', '三', '四', '五', '六', '七', '八', '九', '十'];
        var _index = index + 1;
        var hundreds_en = Math.floor(_index / 100);
        var tens_en = Math.floor(_index / 10);
        var bit_en = _index - (hundreds_en * 100 + tens_en * 10);
        return (chlist[hundreds_en] + chlist[tens_en] + chlist[bit_en] + '、');
    };
    /*$(window).resize(function () {
        if(document.body.clientWidth < 1600){
            $('.paper-from-word .paper-parser-auto .infor_user').hide();
        }else{
            $('.paper-from-word .paper-parser-auto .infor_user').show();
        }
    });*/
    /**
     * 解析URL
     */
    function formatUrlAttr(url) {
        var paremersObj = new Object();
        var intsplit = url.split('?');
        var paramerslist = new Array();
        if(intsplit.length >= 2){
            paramerslist = intsplit[1].split('&');
        }else{
            return {};
        }
        if(paramerslist.length >= 2){
            for(var i = 0; i < paramerslist.length; i++){
                paramerslist[i] = paramerslist[i].split('=');
            }
            return _.object(paramerslist);
        }else{
            return {};
        }
    }
    $scope.isSelecteGroupAndGroupsEqual = function (g,s) {
        return _.isEqual(g,s);
    };
    $scope.changeShowIndex = function () {
        $scope.showIndex = !$scope.showIndex;
    };
    /**
     *检查是否存在试卷名 如果不存在补齐
     */
    function makeUpPaperName(list) {
        var ele = _.find(list,function (item) {
            return item.type === 0;
        });
        if(!ele){
            list.unshift({dis: 0, type: 0, value: "请输入试卷名"});
        }
        return list;
    }
    /**
     *获取试卷结构
     */
    function getPaperConfig(examNo) {
        var defer = $q.defer();
        $http.post(apiUrlServer.baseUrl + '/Interface0181.ashx',{
            examId: examNo
        }).then(function (res) {
            if(res.data && res.data.msg){
                defer.resolve(res);
            }else{
                defer.reject(res);
            }
        },function (res) {
            defer.reject(res);
        });
        return defer.promise;
    }
    /**
     * 处理181返回数据 将子题全部拿出来 与父题平行深度
     */
    function handleSubQst(list) {
        for(var i = 0; i < list.length; i++){
            if(!!list[i].question && list[i].question.length > 0){
                var temlist = [];
                for(var k = 0; k < list[i].question.length; k++){
                    if(!!list[i].question[k].sub && list[i].question[k].sub.length > 0){
                        temlist.push({index:i,sub:list[i].question[k].sub});
                    }
                }
                if(temlist.length> 0){
                    for(var j = 0; j < temlist.length; j++){
                        list[i].question.splice(temlist[j].index,0,temlist[j].sub);
                    }
                }
            }
        }
        return list;
    }
    /**
     *若已存在空白卷 则需要检查所添加试题 是否符合空白卷结构 若不符合则直接拒绝
     * */
    function checkPaperStructure(orginlist,index,addele,typeObj) {
        if(index >= 0 && index < orginlist.length){
            if(orginlist[index].question.length === addele.length){
                var temlist = [];
                for(var i = 0; i < orginlist[index].question.length; i++){
                    if(orginlist[index].question[i].QTypeId !== (typeObj.QtypeId+'')){
                        temlist.push(orginlist[index].question[i]);
                    }
                }
                if(temlist.length>0){
                    constantService.showSimpleToast('添加题型与原卷题型不相符，请重新选择后添加!');
                    return false;
                }else{
                    return true;
                }
            }else{
                constantService.showSimpleToast('添加题目数量与原卷不一致，请重新选择后添加!');
                return false;
            }
        }else{
            constantService.showSimpleToast('添加分数与原卷不一致，请重新整理!');
            return false;
        }
    }
    /**
     * url为保护
     */
    function isParamsEmpty(params) {
        if(!!params && params != 'undefined' && params != 'null'){
            return params;
        }else{
            return ''
        }
    }
    }).filter('trustHtml', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
});