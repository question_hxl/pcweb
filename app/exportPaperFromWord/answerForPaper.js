/**
 * Created by 贺小雷 on 2017-06-09.
 */
angular.module('customService', ['ngDialog'])
    .service('constantService', function (ngDialog) {
        return {
            EMPTY_QFLNKID: "00000000-0000-0000-0000-000000000000",
            NoMap: {
                0: '',
                1: '一',
                2: '二',
                3: '三',
                4: '四',
                5: '五',
                6: '六',
                7: '七',
                8: '八',
                9: '九',
                10: '十'
            },
            PROVINCE: [
                '安徽', '澳门', '北京', '重庆', '福建', '甘肃', '广东', '广西', '贵州', '海南',
                '河北', '河南', '黑龙江', '湖北', '湖南', '吉林', '江苏', '江西', '辽宁', '内蒙古',
                '宁夏', '青海', '山东', '山西', '陕西', '上海', '四川', '台湾', '天津', '西藏', '香港',
                '新疆', '云南', '浙江'
            ],
            PUBLISH_CLUB: [
                {
                    id: '1',
                    name: '苏教版'
                }, {
                    id: '2',
                    name: '人教版'
                }, {
                    id: '3',
                    name: '苏科版'
                }, {
                    id: '4',
                    name: '牛津版'
                }
            ],
            getYearList: function () {
                var yearList = [];
                var year = new Date().getFullYear();
                for (var i = 0; i < 20; i++) {
                    yearList.push(year - i);
                }
                return yearList;
            },
            GRADE_LIST: [
                {
                    id: '1',
                    name: '一年级'
                }, {
                    id: '2',
                    name: '二年级'
                }, {
                    id: '3',
                    name: '三年级'
                }, {
                    id: '4',
                    name: '四年级'
                }, {
                    id: '5',
                    name: '五年级'
                }, {
                    id: '6',
                    name: '六年级'
                }, {
                    id: '7',
                    name: '七年级'
                }, {
                    id: '8',
                    name: '八年级'
                }, {
                    id: '9',
                    name: '九年级'
                }, {
                    id: '10',
                    name: '十年级'
                }, {
                    id: '11',
                    name: '十一年级'
                }, {
                    id: '12',
                    name: '十二年级'
                }
            ],
            XD_LIST: [{
                id: '1',
                name: '小学'
            }, {
                id: '2',
                name: '初中'
            }, {
                id: '3',
                name: '高中'
            }
            ],
            showTypeList: [{
                id: '2',
                name: '一行四列'
            }, {
                id: '1',
                name: '两行两列'
            }, {
                id: '0',
                name: '四行一列'
            }],
            getCNNoByIndex: function (index) {
                if (!index) {
                    return '';
                }
                var shang = Math.floor(index / 10),
                    yu = index % 10;
                var cnStr = '';
                if (shang > 1) {
                    cnStr = this.NoMap[shang] + this.NoMap[10] + this.NoMap[yu];
                } else if (shang === 1) {
                    cnStr = this.NoMap[10] + this.NoMap[yu];
                } else {
                    cnStr = this.NoMap[yu];
                }
                return cnStr;
            },
            showSimpleToast: (function () {
                return function (text, autoHideTime) {
                    autoHideTime = autoHideTime || 2000;
                    var dialogObj = ngDialog.open({
                        template: '<div class="toast-title">' + text + '</div>',
                        className: 'ngdialog-theme-default simple-toast',
                        closeByDocument: true,
                        plain: true
                    });
                    (function (obj) {
                        setTimeout(function () {
                            obj.close();
                        }, autoHideTime);
                    })(dialogObj);
                };
            })(),
            /**
             * confirm提示框service
             * @param {String} text     提示文本
             * @param {Array} [btns]   按钮名称，可选，第一个为确认按钮，第二个为取消按钮
             * @param {Function} doneCb   确认方法回调
             * @param {Function} [failCb] 可选，取消方法回调
             */
            showConfirm: function (text, btns, doneCb, failCb) {
                var okBtn = '删除', cancelBtn = '取消';
                if (_.isArray(btns)) {
                    if (btns[0]) {
                        okBtn = btns[0];
                    }
                    if (btns[1]) {
                        cancelBtn = btns[1];
                    }
                } else {
                    failCb = doneCb;
                    doneCb = btns;
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-default btn-cancel" ng-click="cancel()">' + cancelBtn + '</div>',
                        '<div class="btn btn-primary btn-ok" ng-click="done()">' + okBtn + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default simple-toast',
                    closeByDocument: false,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.cancel = function () {
                                dialogObj.close();
                                if (failCb && _.isFunction(failCb)) {
                                    failCb();
                                }
                            };
                            $scope.done = function () {
                                dialogObj.close();
                                if (doneCb && _.isFunction(doneCb)) {
                                    doneCb();
                                }
                            };
                        }
                    }
                });
            },

            /**
             * showMakeSure提示框service
             * @param {String} text 提示文本
             * @param   closeByDocument  是否点击关闭 默认false
             * @param {String} btnName  按钮名称，默认"确定"
             */
            showMakeSure: function (text, closeByDocument, btnName) {
                if (!$.trim(btnName)) {
                    btnName = "确定"
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-primary btn-ok" style="margin-right: -6%;" ng-click="sure()">' + btnName + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default simple-toast',
                    closeByDocument: closeByDocument || false,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.sure = function () {
                                dialogObj.close();
                            };
                        }
                    }
                });
            }
        }
    });
angular.module('myApp',['ngCookies','ngDialog','customService','ngFileUpload','angular-loading-bar','urlServer'])
	.config(['$httpProvider', 'cfpLoadingBarProvider',function($httpProvider,cfpLoadingBarProvider){
		$httpProvider.defaults.withCredentials = true;
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"><div><span class="fa fa-spinner fa-pulse"></span></div>';
	}])
    .controller('answerForPaperCtrl', function($scope, $http, $location, $cookies, ngDialog, constantService, Upload,apiUrlServer){
    var search =formatUrlAttr($location.absUrl());
    //var search = $location.search();
    var unifiedId = isParamsEmpty(search.unifiedId);
    $scope.isUploadDone = false;
    $scope.totalCount = 0;
    $scope.initCount = 0;

    $scope.importStatus = 0;//1 上传完成 2 上传失败 3 正在上传 0 未开始上传
    $scope.importStatusTip = '上传进度';

    $scope.uploadAnswer = function(fileDoc,errFiles){
        if (!fileDoc) {
            constantService.showMakeSure("请选择一份word文档！");
            return;
        } else {
            if (fileDoc.name) {
                var scoFiletype = fileDoc.name.substr(fileDoc.name.lastIndexOf(".")).toLowerCase();
                if (!((scoFiletype === '.doc') || (scoFiletype === '.docx'))) {
                    return;
                }
            } else {
                constantService.showMakeSure("文档格式错误！");
                return;
            }
        }
        fileDoc.upload = Upload.upload({
            url: apiUrlServer.resourceUrl + '/AgentDataSync/UploadServlet',
            data: {
                file: fileDoc,
                'sessionId': isParamsEmpty(search.sessionId) || $cookies['sessionId'],
                isExam : '0',
                examFlnkid: isParamsEmpty(search.examId)
            },
            ignoreLoadingBar: true
        });

        $scope.importStatus = 3;

        fileDoc.upload.then(function (response) {
            if(response.data.Code === '00') {

                $scope.importStatus = 1;
                $scope.importStatusTip = '上传成功';

                $scope.isUploadDone = true;
                $scope.importStatus = 0;
                parseAnswerData(JSON.parse(response.data.Data));
            }else {
                constantService.showMakeSure(response.data.Msg);
                $scope.importStatus = 2;
                $scope.importStatusTip = '转换失败';
            }
        }, function (response) {
            /*constantService.showMakeSure('上传失败，请重试！');*/
            $scope.importStatus = 2;
            $scope.importStatusTip = '上传失败';
        }, function (evt) {
            $scope.currentUploadProgress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total)) + '%';
            if(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)) === 100){
                $scope.importStatusTip = '正在转换请稍后...';
            }
            containerProg.animate(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)));
        });
    };

    function parseAnswerData (data){
        $scope.missedAnswers = _.filter(data, function(item){
            return +item.Dis === -1;
        });
        $http.post(apiUrlServer.baseUrl + '/Interface0181.ashx', {
            examId: isParamsEmpty(search.examId)
        }).then(function(res){
            if(res.data && res.data.msg) {
                $scope.paperName = res.data.examName;
                var answerDatas = getAllSingleQsts(res.data.msg);
                _.each(answerDatas, function(item){
                    _.each(item.question, function(q){
                        $scope.totalCount++;
                        var qstInParsedData = _.find(data, function(qa){
                            return +qa.Dis === +q.orders;
                        });
                        if(qstInParsedData) {
                            q.initAnswer = q.isObjective==='1'?testChoiceAnswer(qstInParsedData.Answer):qstInParsedData.Answer;
                            $scope.initCount++;
                        }else {
                            q.initAnswer = null;
                        }
                        q.realAnswer = q.initAnswer;
                    });
                });
                $scope.groups = answerDatas;
            }
        });
    }

    function getAllSingleQsts (groups){
        return _.map(groups, function(g){
            var singleQsts = [];
            _.each(g.question, function(item){
                if(item.mode === 'B' || item.Mode === 'B') {
                    var subs = item.sub;
                    _.each(subs, function(s){
                        s.orders = item.orders + '.' + s.SubOrder;
                    });
                    singleQsts = singleQsts.concat(subs);
                } else if(item.mode === 'A' || item.Mode === 'A'){
                    singleQsts = singleQsts.concat(item.sub);
                }else {
                    singleQsts.push(item);
                }
            });
            g.question = singleQsts;
            return g;
        })
    }

    $scope.remark = function(q, e){
        e.preventDefault();
        $scope.lastSelectedHtml = '';
        if($scope.currentMarkQuestion) {
            $scope.currentMarkQuestion.isMarking = false;
        }
        q.isMarking = true;
        $scope.currentMarkQuestion = q;
    };
    $scope.saveMark = function(q, e){
		e.preventDefault();
        q.isMarking = false;
        if($scope.lastSelectedHtml.length > 0) {
            getRemainAnswer(q.initAnswer,$scope.lastSelectedHtml);
            q.initAnswer = $scope.lastSelectedHtml;
        }
        $scope.currentMarkQuestion = null;
        //更新已标记题目数量
        updataInitCount();
    };
    $scope.startMark = function(q, e){
		e.preventDefault();
        $scope.lastSelectedHtml = '';
        if($scope.currentMarkQuestion) {
            $scope.currentMarkQuestion.isMarking = false;
        }
        q.isMarking = true;
        $scope.currentMarkQuestion = q;
    };
    $scope.prevent = function(e){
        e.preventDefault();
    };
    $scope.isAnswer = function(q, opt){
        var answer = q.initAnswer || '';
        return answer.indexOf(opt) >= 0;
    };
    $scope.setAnswer = function(q, opt){
        var answer = q.initAnswer || '', answerList = answer.split('');
        var index = _.findIndex(answerList, function(a){
            return a === opt;
        });
        if(index >= 0) {
            answerList.splice(index, 1);
            q.initAnswer = answerList.join('');
        }else {
            answerList.push(opt);
            q.initAnswer = answerList.join('');
        }
        q.initAnswer = $.trim(q.initAnswer);
        //更新已标记题目数量
        updataInitCount();
    };
    $scope.checkCurrentQst = function(q){
        var $container = $('.answer-edit-section .answer-container'), $ele = $container.find('#a_' + q.orders);
        if($scope.selectedQst && $scope.selectedQst.orders === q.orders) {
            $scope.selectedQst = null;
        }else {
            $scope.selectedQst = q;
            if($ele.length > 0) {
                var containerOffsetTop = $container.offset().top,
                    containerScrollTop = $container.scrollTop(),
                    eleOffsetTop = $ele.offset().top + containerScrollTop;
                $container.scrollTop(eleOffsetTop - containerOffsetTop);
            }
        }
    };
    $scope.goBack = function(){
        $scope.isUploadDone = false;
    };
    $scope.savePaperAnswer = function(){
        var answerData = [];
        _.each($scope.groups, function(g){
            _.each(g.question, function(q){
                var item = {
                    qFlnkid: q.FLnkID,
                    answer: q.initAnswer ? (q.isObjective === '1' ? q.initAnswer.split('').join(',') : q.initAnswer) : '',
                    isObjective: q.isObjective
                };
                answerData.push(item);
            });
        });
        var existNoAnswer = [];
        if(answerData.length > 0){
            for(var i = 0; i < answerData.length; i++){
                if(!answerData[i].answer){
                    existNoAnswer.push({index:i,obj:answerData[i]});
                }
            }
        }else{
            constantService.showSimpleToast('请重新导入答案');
            return;
        }

        if(existNoAnswer.length === answerData.length){
            constantService.showSimpleToast('请标记答案后保存!');
            return;
        }

        /*var existNoAnswer = _.find(answerData, function(item){
            return !item.answer;
        });*/
        if(existNoAnswer.length>0) {
            constantService.showConfirm('存在部分题目未设置答案，是否继续提交？',['继续'], function(){
                $http.post('/BootStrap/schoolmanager/saveExamAnswers.ashx', {
                    unifiedItemId: unifiedId ? unifiedId : undefined,
                    examFlnkid: unifiedId ? undefined : isParamsEmpty(search.examId),
                    questionList: answerData
                }).then(function(res){
                    if(res.data.code === 0) {
                        constantService.showSimpleToast('恭喜您，答案导入成功！');
                    }else {
                        constantService.showSimpleToast('答案导入失败，请重试！');
                    }
                });
            });
        }else {
            $http.post('/BootStrap/schoolmanager/saveExamAnswers.ashx', {
                unifiedItemId: unifiedId ? unifiedId : undefined,
                examFlnkid: unifiedId ? undefined : isParamsEmpty(search.examId),
                questionList: answerData
            }).then(function(res){
                if(res.data.code === 0) {
                    constantService.showSimpleToast('恭喜您，答案导入成功！');
                }else {
                    constantService.showSimpleToast('答案导入失败，请重试！');
                }
            });
        }
    };
    $(document).on('selectstart', function(e){
        $(document).off('mouseup.markAnswer').on('mouseup.markAnswer', function(){
            var selection = document.getSelection();
            //首尾不重叠，说明有选中区域，记录选中selection
            if(!selection.isCollapsed) {
                $scope.lastSelectedHtml = getSelectionHtml(selection);
            }
        });
    });
    function getSelectionHtml(selection){
        var range = selection.getRangeAt(0);
        var frag = range.cloneContents();
        var div = $('<div>');
        div.append($(frag));
        return div.html();
    }

    /**
     * 更新已标记答案题目数量
     */
    function updataInitCount() {
        var _initCount = 0;
        _.each($scope.groups, function(item){
            _.each(item.question, function(q){
                if(!!q.initAnswer){
                    _initCount++;
                }
            });
        });
        $scope.initCount = _initCount;
    }

    /**
     * 百分比进度条
     */
    var containerProg = $('#indicatorContainer_answer').radialIndicator({
        radius : 65,
        percentage :true,
        barColor : "#108fe9",
        initValue : 0,
        fontColor:"#919191",
        barWidth:7,
        roundCorner : true
    }).data('radialIndicator');
    $scope.$watch('importStatus',function(newVal,oldVal)  {
        if(newVal === oldVal){
            return;
        }
        switch (newVal){
            case 1:
                containerProg.option('barColor','#00a954');
                containerProg.option('displayNumber',false);
                containerProg.animate(100);
                break;
            case 2:
                containerProg.option('barColor','#f14034');
                containerProg.option('displayNumber',false);
                containerProg.animate(90);
                break;
            case 3:
                containerProg.option('barColor','#108fe9');
                containerProg.option('displayNumber',true);
                break;
            case 0:
                containerProg.option('barColor','#108fe9');
                containerProg.option('displayNumber',true);
                containerProg.animate(0);
                break;
        }
    });
    /**
     * 解析URL
     */
    function formatUrlAttr(url) {
        var paremersObj = new Object();
        var intsplit = url.split('?');
        var paramerslist = new Array();
        if(intsplit.length >= 2){
            paramerslist = intsplit[1].split('&');
        }else{
            return {};
        }
        if(paramerslist.length >= 2){
            for(var i = 0; i < paramerslist.length; i++){
                paramerslist[i] = paramerslist[i].split('=');
            }
            return _.object(paramerslist);
        }else{
            return {};
        }
    }
    /**
     * 将剪切剩下的答案信息归入未标记答案
     */
    function getRemainAnswer(str,splitstr) {
        if(!!str){
            $scope.missedAnswers.unshift({Answer:str.split(splitstr).join(''),Dis:'-1'});
        }
    }
    $scope.gotoMissedAnswer = function () {
        var $container = $('.answer-edit-section .answer-container'), $ele = $container.find('#missedAnswersArea');
        var containerOffsetTop = $container.offset().top,
            containerScrollTop = $container.scrollTop(),
            eleOffsetTop = $ele.offset().top + containerScrollTop;
        $container.scrollTop(eleOffsetTop - containerOffsetTop);
    };
    /**
     * url为保护
     */
    function isParamsEmpty(params) {
        if(!!params && params != 'undefined' && params != 'null'){
            return params;
        }else{
            return ''
        }
    }
    /**
     *正则表达式 去除选择题答案中非ABCDEFG以外的字符串
     */
    function testChoiceAnswer(answer) {
        if(!!(answer+'')){
            var t = (answer+'').split('');
            if(t.length>0){
                for(var i = t.length-1; i >= 0; i--){
                    if(!(new RegExp(/^[ABCDEFGabcefg]+$/)).test(t[i])){
                        t.splice(i,1);
                    }
                }
                answer = t.join('');
            }
        }
        return answer;
    }
    }).filter('trustHtml', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
})
    .filter('fillBlank', function(){
        return function (input) {
            return input || '暂无内容';
        }
    })
    .filter('filterByKey', function(){
        return function(source, key, prop){
            if(!key) {
                var result = _.filter(source, function(item){
                    return source;
                });
            }else {
                var result = _.filter(source, function(item){
                    return prop ? item[prop].indexOf(key) >= 0 : item.indexOf(key) >= 0;
                });
            }
            return result;
        };
    })
    .filter('numToCN', function(constantService){
        return function(input){
            return constantService.getCNNoByIndex(input);
        };
    });