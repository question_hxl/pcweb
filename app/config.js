/**
 * Created by 贺小雷 on 2016/9/2.
 */
var config = {
    useFor: '51',           //51, jdy
    version: '2.7.0',
    cssToMinify: [],
    hostThemeMap: {
        'localhost': '51',                //睿越
        'www.51jyfw.com': '51',          //51教育服务
        'www.wxy100.com': 'jdy',          //精斗云
        'ry.51jyfw.com': 'ry'
    },
    copyInfo: {
        '51': '南京伍壹教育科技有限公司',
        'jdy': '江苏飚众教育科技有限公司',
        'ry': '江苏睿越竟智信息技术有限公司'
    },
    themeCompanyNameMap: {
        '51': '51教育服务',
        'jdy': '飚众教育',
        'ry': '睿越竞智'
    },
    homePageMap: {
        '51': 'index.html',
        'jdy': 'wxyindex.html',
        'ry': 'ryindex.html'
    }
};
config.theme = config.hostThemeMap[location.hostname];
function loadHtml(url){
    return url + '?ver=' + config.version;
}

function loadCss(cssHref, mediaAttr){
    var header = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');
    link.setAttribute('charset', 'UTF-8');
    link.setAttribute('href', cssHref + '?ver=' + config.version);
    if(mediaAttr) {
        link.setAttribute('media', mediaAttr);
    }
    header.appendChild(link);
}

function loadJs(src, attr){
    var header = document.getElementsByTagName('head')[0];
    var link = document.createElement('script');
    link.setAttribute('type', 'text/javascript');
    link.setAttribute('charset', 'UTF-8');
    link.setAttribute('src', src + '?ver=' + config.version);
    if(attr && typeof attr === 'object') {
        for(var key in attr) {
            if(key === 'data-main') {
                link.setAttribute(key, attr[key] + '?ver=' + config.version);
            }else {
                link.setAttribute(key, attr[key]);
            }
        }
    }
    header.appendChild(link);
}