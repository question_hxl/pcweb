/**
 * Created by 贺小雷 on 2016/8/10.
 */
define(['jquery.datetimepicker', 'pagination', 'dateFomart', 'underscore-min', 'jqfly', 'requestAnimationFrame', 'ckeditor'], function () {
    return ['$rootScope', '$http', '$scope', '$q', '$location', 'ngDialog', '$state', '$timeout','constantService','cartService','utilService',
        function ($rootScope, $http, $scope, $q, $location, ngDialog, $state, $timeout, constantService, cartService, utilService) {
        var user = JSON.parse(sessionStorage.getItem('currentUser'));
            var GradeId = $scope.GradeId = user.GradeId; //当前学科ID
            var role = $scope.role = user.role; //用户角色
            $scope.isUserManager = user.isSystemSchool === '1';

            //获取年份
            $scope.yearList = constantService.getYearList();
            $scope.yearList.unshift({
                id: '全部',
                name: '全部'
            });
            $scope.currentYear = $scope.yearList[0];
            $scope.currentTime = 0;
            $scope.currentQType = {};
            $scope.currentTab = +$state.params.bankType || 1;
            $scope.currentChapter = '';
            $scope.currentKlg = '';
            $scope.currentTreeType = 1;
            $scope.pagConf = {
                currentPage: 1,
                totalItems: 0,
                itemsPerPage: 10
            };
            $scope.questionList = [];
            $scope.initChapter = false;
            $scope.initKlg = false;
            $scope.tiKuTrue = false;

            //初始化时间选择器插件
            $('#start_datetimepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: "Y-m-d",
                todayButton: true,
                scrollInput: false,
                onChangeDateTime: function(dp, $input) {
                    $('#start_datetimepicker').datetimepicker('hide');
                }
            });
            $('#end_datetimepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: "Y-m-d",
                todayButton: true,
                scrollInput: false,
                onChangeDateTime: function(dp, $input) {
                    $('#end_datetimepicker').datetimepicker('hide');
                }
            });

            //获取省份
            $http.post($rootScope.baseUrl + '/Interface0195.ashx').success(function (res) {
                if(res.msg && _.isArray(res.msg)) {
                    $scope.provinceList = res.msg;
                    var jsIndex = _.findIndex($scope.provinceList, function(item){
                        return item.Province === '江苏省';
                    });
                    $scope.provinceList.splice(jsIndex, 1);
                    $scope.provinceList.unshift({Province: '江苏省'});
                }else {
                    $scope.provinceList = [];
                }
                $scope.provinceList.unshift({Province: '全部'});
                $scope.currentProvince = $scope.provinceList[0];
            }).finally(function(){
                //获取学段
                $http.post($rootScope.baseUrl + '/Interface0290A.ashx').success(function(res){
                    if(res.course && _.isArray(res.course)){
                        $scope.gradeList = res.course;
                        $scope.currentGrade = _.find($scope.gradeList, function(item){
                            return item.gradeId === GradeId;
                        }) || $scope.gradeList[0];
                    }else {
                        $scope.gradeList = [];
                        $scope.currentGrade = {
                            gradeId: GradeId
                        };
                    }
                }).finally(function(){
                    //获取题型
                    getQTypes($scope.currentGrade.gradeId);
                })
            });

            $scope.setTabActive = function(tabId){
                if($scope.currentTab !== tabId) {
                    $scope.currentTab = +tabId;
                    if($scope.pagConf.currentPage === 1) {
                        $scope.name = '';
                        $scope.searchQst();
                    } else {
                        $scope.pagConf.currentPage = 1;
                    }
                }
            };

            $scope.selectProvince = function(province){
                if($scope.currentProvince.Province === province.Province) {
                    return;
                }
                $scope.currentProvince = province;
                if($scope.pagConf.currentPage === 1) {
                    $scope.name = '';
                    $scope.searchQst();
                }else {
                    $scope.pagConf.currentPage = 1;
                }
            };

            $scope.selectYear = function(year){
                if($scope.currentYear === year) {
                    return;
                }
                $scope.currentYear = year;
                if($scope.pagConf.currentPage === 1) {
                    $scope.name = '';
                    $scope.searchQst();
                } else {
                    $scope.pagConf.currentPage = 1;
                }
            };

            $scope.selectGrade = function(grade){
                $scope.currentGrade = grade;
            };

            $scope.selectType = function(type){
                if($scope.currentQType.qtypeId === type.qtypeId) {
                    return;
                }
                $scope.currentQType = type;
                if($scope.pagConf.currentPage === 1) {
                    $scope.name = '';
                    $scope.searchQst();
                } else {
                    $scope.pagConf.currentPage = 1;
                }
            };

            $scope.selectTime = function(time){
                if($scope.currentTime === time) {
                    return;
                }
                $scope.currentTime = time;
                var date = new Date();
                // 判断时间
                if ($scope.currentTime == 1) {
                    $('#start_datetimepicker').val(date.dateChange("lastsevendays")[0]);
                    $('#end_datetimepicker').val(date.dateChange("lastsevendays")[1]);
                } else if ($scope.currentTime == 2) {
                    $('#start_datetimepicker').val(date.dateChange("recentmonth")[0]);
                    $('#end_datetimepicker').val(date.dateChange("recentmonth")[1]);
                } else {
                    $('#start_datetimepicker').val('');
                    $('#end_datetimepicker').val('');
                }
                if($scope.pagConf.currentPage === 1) {
                    $scope.name = '';
                    $scope.searchQst();
                } else {
                    $scope.pagConf.currentPage = 1;
                }
            };

            $scope.setTreeType = function(type){
                $scope.currentTreeType = type;
                $scope.initChapter = type === 1;
                $scope.initKlg = type === 2;
            };

            $scope.searchQst = function(unitId){
                if (new Date($('#end_datetimepicker').val()) < new Date($('#start_datetimepicker').val())) {
                    constantService.alert('结束时间不能小于开始时间！');
                    return false;
                }
                var currentPage = $scope.pagConf.currentPage || 1;
                var param = {
                    province: $scope.currentProvince.Province,
                    year: $scope.currentYear.id,
                    type: $scope.currentQType.qtypeId || '全部',
                    gradeId: $scope.currentGrade.gradeId,
                    startTime: $('#start_datetimepicker').val(),
                    stoptime: $('#end_datetimepicker').val(),
                    sourceType: $scope.currentTab,
                    curPage: currentPage,
                    name: $scope.name || ''
                };
                var defer = $q.defer();
                if($scope.currentTreeType === 1) {
                    if($scope.currentTab ===3 && !$scope.tiKuTrue) {
                        param.chapterId = '';
                    } else{
                        param.chapterId = unitId || $scope.currentChapter;
                    }
                    defer = $http.post($rootScope.baseUrl + '/Interface0194.ashx', param);
                }else if($scope.currentTreeType === 2) {
                    if($scope.currentTab ===3 && !$scope.tiKuTrue ) {
                        param.knowledgeId = '';
                    }else{
                        param.knowledgeId = unitId || $scope.currentKlg;
                    }
                    defer = $http.post($rootScope.baseUrl + '/Interface0194A.ashx', param);
                }
                defer.then(function(res){
                    if(_.isArray(res.data.msg)) {
                        $scope.pagConf.totalItems = +res.data.Count;
                        $scope.questionList = res.data.msg;
                        _.each($scope.questionList, function(qst){
                            qst.ShowType = _.find(utilService.showTypeList, function(type){
                                return type.id === qst.ShowType;
                            });
                            _.each(qst.sub, function(s, index){
                                s.orders = index + 1;
                                s.OptionOne = s.OptionA;
                                s.OptionTwo = s.OptionB;
                                s.OptionThree = s.OptionC;
                                s.OptionFour = s.OptionD;
                                s.ShowType = _.find(utilService.showTypeList, function(type){
                                    return type.id === s.ShowType;
                                });
                            });
                        })
                    }else {
                        $scope.pagConf.totalItems = 0;
                        $scope.questionList = [];
                    }
                    getQuestionCart();
                    getOtherInfo();
                }, function(res){
                    $scope.pagConf.totalItems = 0;
                    $scope.questionList = [];
                });
            };

            $scope.$watch('currentGrade', function(val, oldVal){
                //年级变化，获取知识点和章节
                if(val) {
                    if(!oldVal || (oldVal && val.gradeId !== oldVal.gradeId)) {
                        getQTypes(val.gradeId);
                        getChaptersAndKnowledges(val.gradeId);
                    }
                }
            });

            $scope.$watch('pagConf.currentPage', function(val, oldVal){
                if(val && val > 0 && val !== oldVal) {
                    $scope.searchQst();
                }
            });

            $scope.tiTrue = function (tiKuTrue) {
                $scope.tiKuTrue = !tiKuTrue;
                $scope.searchQst();
            };
            
            $scope.treeClick = function(unitId, force){
                if(force) {
                    if($scope.pagConf.currentPage === 1) {
                        $scope.name = '';
                        $scope.searchQst(unitId);
                    } else {
                        if($scope.currentTreeType === 1) {
                            $scope.currentChapter = unitId;
                        }else {
                            $scope.currentKlg = unitId;
                        }
                        $scope.pagConf.currentPage = 1;
                    }
                }else {
                    if($scope.currentTreeType === 1 && $scope.currentChapter === unitId) {
                        return;
                    }
                    if($scope.currentTreeType === 2 && $scope.currentKlg === unitId) {
                        return;
                    }
                    if($scope.pagConf.currentPage === 1) {
                        $scope.name = '';
                        $scope.searchQst(unitId);
                    } else {
                        if($scope.currentTreeType === 1) {
                            $scope.currentChapter = unitId;
                        }else {
                            $scope.currentKlg = unitId;
                        }
                        $scope.pagConf.currentPage = 1;
                    }
                }
            };

            $scope.removeFav = function(question) {
                $http.post($rootScope.baseUrl + '/Interface0205.ashx', {
                    resourceFlnkID: question.FLnkID
                }).then(function(res){
                    question.IsCollection = '0';
                });
            };

            $scope.addFav = function(question){
                $http.post($rootScope.baseUrl + '/Interface0204.ashx', {
                    resourceFlnkID: question.FLnkID,
                    type: 2
                }).then(function(res){
                    question.IsCollection = '1';
                });
            };

            $scope.checkAnalysis = function(question){
                $scope.values = question.Analysis;
                $scope.qAnswer = question.Answer;
                ngDialog.open({
                    template: 'template/viewAnalysis.html',
                    className: 'ngdialog-theme-plain',
                    scope: $scope
                });
            };

            $scope.addToCart = function(question, e){
                cartService.putCart(question.FLnkID, question.QTypeName, question.QTypeId).then(function(){
                    question.isInCart = true;
                    var offset = $("#side-bar .cart").offset(),
                        flyer = $('<img src="../img/paper.png" class="u-flyer">');
                    flyer.fly({
                        start: {
                            left: e.clientX,
                            top: e.clientY
                        },
                        end: {
                            left: offset.left,
                            top: offset.top - $(window).scrollTop(),
                            width: 80,
                            height: 80
                        },
                        speed: 1.5,
                        onEnd: function(){
                            flyer.remove();
                            $scope.$emit('baseketList_number_change');
                        }
                    });
                });
            };

            $scope.removeFromCart = function(question){
                cartService.removeCart(question.FLnkID).then(function(res){
                    question.isInCart = false;
                    $scope.$emit('baseketList_number_change');
                });
            };

            $scope.onKeyUp = function(e){
                if(e.keyCode === 13) {
                    $scope.searchQst();
                }
            };

            //为便于资源管理，对系统的管理学校开发编辑试题功能
            $scope.editQuestion = function(q){
                if(user.isSystemSchool === '1') {
                    $scope.toEditQuestion = q;
                    ngDialog.open({
                        template : 'editQuestionBank',
                        plain : false,
                        scope : $scope,
                        className : 'ngdialog-theme-default',
                        appendClassName : 'edit-q-dialog',
                        preCloseCallback : function(){
                            $scope.$broadcast('$destory-ck');
                        }
                    });
                }
            };

            $scope.saveEditQst = function(data){
                _.extend($scope.toEditQuestion, {
                    title: data.title,
                    OptionOne: data.OptionOne,
                    OptionTwo: data.OptionTwo,
                    OptionThree: data.OptionThree,
                    OptionFour: data.OptionFour,
                    Answer: data.answer,
                    Analysis: data.analysis
                });
                $http.post($rootScope.baseUrl + '/Interface0206.ashx', {
                    qFlnkId: data.FLnkID,
                    title: data.title,
                    optionA: data.OptionOne,
                    optionB: data.OptionTwo,
                    optionC: data.OptionThree,
                    optionD: data.OptionFour,
                    answer: data.answer,
                    analysis: data.analysis
                }).then(function(res){
                    if(res.data.code === 0) {
                        ngDialog.close();
                        constantService.alert('修改成功！');
                    }else {
                        constantService.alert('修改失败，请检查输入内容！');
                    }
                }, function(res){
                    constantService.alert('修改失败，服务器异常！');
                });
            };

            $scope.saveDiff = function(data, q){
                $http.post($rootScope.baseUrl + '/Interface0206.ashx', {
                    qFlnkId: q.FLnkID,
                    diff: data
                }).then(function(res){
                    if(res.data.code === 0) {
                        ngDialog.close();
                        constantService.alert('修改成功！');
                    }else {
                        constantService.alert('修改失败，请检查输入内容！');
                    }
                }, function(res){
                    constantService.alert('修改失败，服务器异常！');
                });
            };

            $scope.sendError = function(question, index){
                $scope.tk = {
                    fLnkId: '',
                    orders: index + 1,
                    title: question.title,
                    ShowType: { id: question.ShowType },
                    OptionOne: question.OptionOne,
                    OptionTwo: question.OptionTwo,
                    OptionThree: question.OptionThree,
                    OptionFour: question.OptionFour
                };

                ngDialog.open({
                    template: 'template/checkQuestion.html',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'ngdialog-theme-zxz',
                    scope: $scope,
                    controller: function($scope){
                        return function(){
                            $scope.goclose = function() { //关闭
                                ngDialog.close();
                            };

                            $scope.goconfirm = function(errorReason) { //提交题目，错误原因
                                if (!$.trim(errorReason)) {
                                    $scope.errMsg = "请填写错误原因！";
                                    $timeout(function() { $scope.errMsg = ""; }, 1000);
                                } else {
                                    $http({ //提交接口
                                        method: 'post',
                                        url: $rootScope.baseUrl + '/Interface0253.ashx',
                                        data: {
                                            qFLnkId: question.FLnkID,
                                            comment: errorReason
                                        }
                                    }).success(function(data) {
                                        if (!!data && data.code === 0) {
                                            $scope.sucMsg = "提交纠错成功！";
                                            $timeout(function() {
                                                $scope.sucMsg = "";
                                                ngDialog.close();
                                            }, 1000);
                                        } else {
                                            $scope.sucMsg = "提交纠错失败，请重试！";
                                            $timeout(function() { $scope.sucMsg = ""; }, 1000);
                                        }
                                    })
                                }
                            };
                        }
                    }
                });
            };

            function getOtherInfo(){
                var ids = _.pluck($scope.questionList, 'FLnkID').join(',');
                if(ids.length > 0) {
                    $http.post($rootScope.baseUrl + '/Interface0294.ashx', {
                        strQFId: ids
                    }).then(function(res){
                        var result = res.data.msg;
                        if(_.isArray(result)) {
                            _.each($scope.questionList, function(qst){
                                var info = _.find(result, function(item){
                                    return qst.FLnkID.toUpperCase() === item.fId.toUpperCase();
                                });
                                if(info) {
                                    qst.IsCollection = info.isCollection;
                                    qst.UseNum = info.useNum;
                                    qst.SucRate = info.sucRate;
                                }
                            });
                        }
                    });
                }
                // var subQsts = _.filter($scope.questionList, function (q) {
                //     return q.IsSub === '1';
                // });
                // var subIds = _.pluck(subQsts, 'FLnkID').join(',');
                // if(!subIds) {
                //     return;
                // }
                // $http.post($rootScope.baseUrl + '/Interface0208A.ashx', {
                //     qFlnkID: subIds
                // }).then(function(res){
                //     var qsts = res.data.msg;
                //     if(_.isArray(qsts)) {
                //         _.each($scope.questionList, function(qst){
                //             var info = _.find(qsts, function(item){
                //                 return qst.FLnkID.toUpperCase() === item.QFLnkID.toUpperCase();
                //             });
                //             if(info) {
                //                 qst.mode = 'B';
                //                 qst.sub = info.sub;
                //                 _.each(qst.sub, function(s, index){
                //                     s.orders = index + 1;
                //                     s.OptionOne = s.OptionA;
                //                     s.OptionTwo = s.OptionB;
                //                     s.OptionThree = s.OptionC;
                //                     s.OptionFour = s.OptionD;
                //                 });
                //             }
                //         });
                //     }
                // });
            }

            function getQuestionCart(){
                cartService.getCart().then(function(res){
                    relateQstCart(res);
                }, function(){
                    relateQstCart([]);
                })
            }

            function relateQstCart(cart){
                _.each($scope.questionList, function(item){
                    var qInCart = _.find(cart, function(c){
                        return c.questionFId.toUpperCase() === item.FLnkID.toUpperCase();
                    });
                    item.isInCart = !!qInCart;
                });
            }

            function getChaptersAndKnowledges(gradeId){
                $scope.initChapter = false;
                $scope.initKlg = false;
                $http.post($rootScope.baseUrl + '/Interface0175.ashx', {
                    gradeId: gradeId
                }).success(function(res){
                    if(_.isArray(res.msg)) {
                        $scope.chapterList = res.msg;
                        //判断当前是章节/知识点TAB，是章节才触发章节树初始化
                        if($scope.currentTreeType === 1) {
                            $scope.initChapter = true;
                            $scope.initKlg = false;
                        }
                    }else {
                        $scope.chapterList = [];
                    }
                }).finally(function(){
                    if($scope.klgList && !_.isEmpty($scope.klgList)) {
                        if($scope.currentTreeType === 2) {
                            $scope.initChapter = false;
                            $scope.initKlg = true;
                        }
                    }else {
                        $http.post($rootScope.baseUrl + '/Interface0213.ashx', {
                            gradeId: gradeId
                        }).success(function(res){
                            if(_.isArray(res.msg)) {
                                $scope.klgList = res.msg;
                                //判断当前是章节/知识点TAB，是知识点才触发知识点树初始化
                                if($scope.currentTreeType === 2) {
                                    $scope.initChapter = false;
                                    $scope.initKlg = true;
                                }
                            }else {
                                $scope.klgList = [];
                            }
                        });
                    }
                });
            }

            function getQTypes(gradeId){
                $scope.currentQType = {};
                return $http.post($rootScope.baseUrl + '/Interface0197.ashx', {
                    gradeId: gradeId
                }).success(function(res){
                    if(res.msg && _.isArray(res.msg)) {
                        $scope.qTypeList = res.msg;
                    }else {
                        $scope.qTypeList = [];
                    }
                    $scope.qTypeList.unshift({
                        qtypeId: '全部',
                        qtypeName: '全部'
                    });
                    $scope.currentQType = $scope.qTypeList[0];
                });
            }
    }];
});