﻿define(["underscore-min"], function () {
	return ['$http', '$scope', '$rootScope', '$state', '$location', '$sce', 'ngDialog', function ($http, $scope, $rootScope, $state, $location, $sce, ngDialog) {
		var searchfrom = $location.search();
		$scope.username = searchfrom.MyName;
		//$scope.ExAnswerFlnkID = searchfrom.ExAnswerFlnkID;
		//$scope.ClassFlnkID = searchfrom.ClassFlnkid;
		//$scope.ExamFlnkID = searchfrom.ExamFlnkID;

		//加载切图位置列表
		$http.post($rootScope.baseUrl + '/Interface0251A.ashx', {
			examFId: searchfrom.ExamFlnkID ,
			classFId: searchfrom.ClassFlnkid
		}).then(function (resp) {
			if(!_.isEmpty(resp.data.msg) && _.isArray(resp.data.msg)) {
				$scope.cutDetail = resp.data.msg[0];
			}else {
				$scope.cutDetail = null;
			}
			//获取题目
			//type=1全部
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0170.ashx',
				data: {
					ExAnswerFlnkID: searchfrom.ExAnswerFlnkID,
					type:1
				}
			}).success(function (data) {
				//$scope.allquenum = data.msg.length;
				var sub1 = [];
				var useranswer = '';
				for (var i = 0 ; i < data.msg.length ; i++) {
					if (data.msg[i].UserAnswer.indexOf("OCR/ocrftp/") >= 0) {
						useranswer = '<img src=' + data.msg[i].UserAnswer + '>';
					}
					else {
						useranswer = data.msg[i].UserAnswer;
					}
					sub1.push({
						'Serial': +data.msg[i].Serial,
						'Title': data.msg[i].Title,
						'Diff': data.msg[i].Diff,
						'Score': data.msg[i].Score,
						'LostSc': data.msg[i].LostSc,
						'Qtype':data.msg[i].Qtype,
						'ClassErrorRate': data.msg[i].ClassErrorRate,
						'Parse': data.msg[i].Parse,
						'UserAnswer': useranswer,
						'ClassRightNum': data.msg[i].ClassRightNum,
						'ClassErrorNum': data.msg[i].ClassErrorNum,
						'GradeErrorRate': data.msg[i].GradeErrorRate,
						'Source': data.msg[i].Source,
						'Option_a': data.msg[i].Option_a,
						'Option_b': data.msg[i].Option_b,
						'Option_c': data.msg[i].Option_c,
						'Option_d': data.msg[i].Option_d,
						'Knowledges': data.msg[i].Knowledges,
						'QFlnkID': data.msg[i].QFlnkID,
						'Answer': data.msg[i].Answer,
						'startDiff': viewStar(data.msg[i].Diff),
						'Option': data.msg[i].Option_a + data.msg[i].Option_b + data.msg[i].Option_c + data.msg[i].Option_d,
						'Image':  data.msg[i].Image
					})
				}
				//$scope.sub1List = sub1;
				$scope.sub1List = _.sortBy(sub1, 'Serial')
				//console.log(_.sortBy(sub1, 'Serial'));
			});


			//type=0错题
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0170.ashx',
				data: {
					ExAnswerFlnkID: searchfrom.ExAnswerFlnkID,
					type: 0
				}
			}).success(function (data) {
				//$scope.allquenum = data.msg.length;
				var sub2 = [];
				var useranswer2 = '';
				for (var i = 0 ; i < data.msg.length ; i++) {
					if (data.msg[i].UserAnswer.indexOf("OCR/ocrftp/") >= 0) {
						useranswer2 = '<img src=' + data.msg[i].UserAnswer + '>';
					}
					else {
						useranswer2 = data.msg[i].UserAnswer;
					}
					sub2.push({
						'Serial': +data.msg[i].Serial,
						'Title': data.msg[i].Title,
						'Diff': data.msg[i].Diff,
						'Score': data.msg[i].Score,
						'LostSc': data.msg[i].LostSc,
						'Qtype': data.msg[i].Qtype,
						'ClassErrorRate': data.msg[i].ClassErrorRate,
						'Parse': data.msg[i].Parse,
						'UserAnswer2': useranswer2,
						'ClassRightNum': data.msg[i].ClassRightNum,
						'ClassErrorNum': data.msg[i].ClassErrorNum,
						'GradeErrorRate': data.msg[i].GradeErrorRate,
						'Source': data.msg[i].Source,
						'Option_a': data.msg[i].Option_a,
						'Option_b': data.msg[i].Option_b,
						'Option_c': data.msg[i].Option_c,
						'Option_d': data.msg[i].Option_d,
						'Knowledges': data.msg[i].Knowledges,
						'QFlnkID': data.msg[i].QFlnkID,
						'Answer': data.msg[i].Answer,
						'startDiff': viewStar(data.msg[i].Diff),
						'Option': data.msg[i].Option_a + data.msg[i].Option_b + data.msg[i].Option_c + data.msg[i].Option_d,
						'Image': data.msg[i].Image
					})
				}
				//$scope.sub1List = sub1;
				$scope.sub2List = _.sortBy(sub2, 'Serial')
				//console.log(_.sortBy(sub1, 'Serial'));
			});
		});

		//获取试卷头部
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0169.ashx',
			data: {
				ClassFlnkID: searchfrom.ClassFlnkid,
				ExamFlnkID: searchfrom.ExamFlnkID
			}
		}).success(function (res) {
			$scope.topinfoList = res.msg;
		});

		//查看解析
		$scope.viewAnalysis = function (values) {
			$scope.values = values;
			ngDialog.open({
				template: 'template/viewAnalysis.html',
				className: 'ngdialog-theme-default',
				scope: $scope,
				preCloseCallback: function () { //回调

				}
			})
		};

		//逐题讲解
		//$scope.EveryQuestion = function () {
		//	$location.path('/EveryQuestion').search({ ExAnswerFlnkID: searchfrom.ExAnswerFlnkID, ClassFlnkID: searchfrom.ClassFlnkid, ExamFlnkID: searchfrom.ExamFlnkID });
		//}

		//查看做题信息
		$scope.answerAnalysis = function (QFlnkID) {
			$scope.answerArea = '<div>&nbsp;</div>';
			$http.post($rootScope.baseUrl + '/Interface0186A.ashx', {
				ClassFlnkID: searchfrom.ClassFlnkid,
				ExamFlnkID: searchfrom.ExamFlnkID,
				QFlnkID: QFlnkID
			}).success(function (data) {
				var answerAnalysisList = $scope.answerAnalysisList = data.msg;

				var analysisListChoseRight = [];
				var analysisListChoseWrong = [];
				for (var i = 0; i < answerAnalysisList.length; i++) {
					if (+answerAnalysisList[i].fIsRight == 1) {
						analysisListChoseRight.push({
							'Accessory': answerAnalysisList[i].Accessory,
							'Answer': answerAnalysisList[i].Answer,
							'IsObjective': answerAnalysisList[i].IsObjective,//0非选择，1选择
							'UserAnswer': answerAnalysisList[i].UserAnswer,
							'UserName': answerAnalysisList[i].UserName,
							'fIsRight': answerAnalysisList[i].fIsRight,
							'fScores': answerAnalysisList[i].fScores
						})
					}

					if (+answerAnalysisList[i].fIsRight < 1) {
						analysisListChoseWrong.push({
							'Accessory': answerAnalysisList[i].Accessory,
							'Answer': answerAnalysisList[i].Answer,
							'IsObjective': answerAnalysisList[i].IsObjective,//0非选择，1选择
							'UserAnswer': answerAnalysisList[i].UserAnswer,
							'UserName': answerAnalysisList[i].UserName,
							'fIsRight': answerAnalysisList[i].fIsRight,
							'fScores': answerAnalysisList[i].fScores
						})
					}
				}
				$scope.analysisListChoseRight = analysisListChoseRight;
				$scope.analysisListChoseWrong = analysisListChoseWrong;
				$scope.rightren = analysisListChoseRight.length;
				$scope.wrongren = analysisListChoseWrong.length;

				$scope.stuListA = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('A') >= 0;
				});
				$scope.stuListB = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('B') >= 0;
				});
				$scope.stuListC = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('C') >= 0;
				});
				$scope.stuListD = _.filter(answerAnalysisList, function (item) {
					return $.trim(item.UserAnswer).indexOf('D') >= 0;
				});
			});
			ngDialog.open({
				template: 'template/answerAnalysis.html',
				className: 'ngdialog-theme-default',
				scope: $scope,
				preCloseCallback: function () { //回调

				}
			})
		};



		// 根据当前数字显示五角星
		function viewStar(num) {
			var str = '';
			for (var i = 0; i < num; i++) {
				str = str + '<span class="fa fa-star orange"></span>'
			}
			return str;
			console.log(str)
		}
	}]
})
