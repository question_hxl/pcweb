﻿/// <reference path="js/tableExport.js" />
/// <reference path="js/jquery.treeview.js" />
/// <reference path="js/jquery.treeview.js" />
require.config({
    //配置angular的路径
    paths: {
        "angular": "bower_components/angular/angular.min",
        "jquery": "js/jquery.min",
        "angular-route": "bower_components/angular-ui-router/release/angular-ui-router.min",
        "angular-cookies": "bower_components/angular-cookies/angular-cookies.min",
        "bootstrap": "js/bootstrap.min",
        "highcharts": "js/highcharts",
        "exporting": "js/exporting",
        "ngDialog": "bower_components/ngDialog/ngDialog.min",
        "icheck": "bower_components/iCheck/icheck.min",
        "jquery.datetimepicker": "bower_components/datetimepicker/jquery.datetimepicker",
        "jquery.dataTables": "bower_components/datatables/media/jquery.dataTables",
        "dataTables.bootstrap": "bower_components/datatables/media/dataTables.bootstrap",
        "ng-file-upload": "js/lib/ng-file-upload",
        "ng-file-upload-shim": "js/lib/ng-file-upload-shim",
        // angularAMD
        "angularAMD": "bower_components/angularAMD/angularAMD",
        "ngload": "bower_components/angularAMD/ngload",
        // end
        "ckeditor": "bower_components/ckeditor/ckeditor",
        "underscore-min": "bower_components/underscore/underscore-min",
        "keyPointMarkView": "keyPointManager/keyPointMarkView",
        "KPMInterfacesService": "keyPointManager/KPMInterfacesService",
        "keyPointManager": "keyPointManager/keyPointManageView",
        "sourceManageView": "keyPointManager/sourceManageView",
        "outexcel": "scoreprint/outexcel",
        "star-rating": "bower_components/rating/jquery.raty",
        "pagination": "bower_components/pagination/tm.pagination",
        "download": "assets/script/lib/download",
        "FileSaver": "assets/script/lib/FileSaver",
        "jquery.wordexport": "assets/script/lib/jquery.wordexport",
        "dateFomart": "js/dateFomart",
        "jquerymedia": "marking/jquery.media",
        "angular-loading-bar": "bower_components/angular-loading-bar/build/loading-bar",
        "dragable": 'assets/script/lib/jquery.dragable',
        "sitelogo": 'bower_components/sitelogo/sitelogo',
        "cropper": 'bower_components/cropper/cropper',
        "directive": "directive-extend/directive",
        "richDirective": "directive-extend/richDirective",
        "lazyTree": "directive-extend/lazyTree",
        "filter": "filter-extend/filter",
        "ba-throttle-debounce": "js/jquery.ba-throttle-debounce.min",
        "jquery-stickyheader": "js/jquery.stickyheader",
        "backEndService": "service/backEndService",
        "customService": "service/ConstantService",
        "utilService": "service/utilService",
        "paperService": "service/paperService",
        "cartService": "service/questionCartService",
        "evaluate": "evaluate/evaluate",
        "jqprint": "js/jquery.jqprint-0.3",
        "jqrcode":"js/qrcode.min",
        "jqfly": 'js/jquery.fly.min',
        "requestAnimationFrame": 'js/requestAnimationFrame',
        "TransferPaper2Html": "js/TransferPaper2Html",
        "wyCarousel":"js/jquery.wyCarousel",
        "echarts": "js/echarts.min",
        "Class": "js/inherit",
        "fullPage":"newLogin/js/fullPage.min",
        "snow":"newLogin/js/snow",
        "TweenLite":"newLogin/js/TweenLite.min",
        "EasePack":"newLogin/js/EasePack.min",
        "demo1":"newLogin/js/demo1",
        "demo2":"newLogin/js/demo2",
        "prefixfree":"newLogin/js/prefixfree.min",
        "particles":"newLogin/js/particles.min",
        'EventBus': 'buildAnswerSheet/EventBus',
        'jquery-barcode': 'js/jquery-barcode',
        "sheetUtil": "buildAnswerSheet/sheetUtil",
        "AnswerTip": "buildAnswerSheet/components/AnswerTip",
        "BlackBlock": "buildAnswerSheet/components/BlackBlock",
        "Blank": "buildAnswerSheet/components/Blank",
        "Composition": "buildAnswerSheet/components/Composition",
        "ExamName": "buildAnswerSheet/components/ExamName",
        "FillBlank": "buildAnswerSheet/components/FillBlank",
        "OptionGroup": "buildAnswerSheet/components/OptionGroup",
        "Page": "buildAnswerSheet/components/Page",
        "ScoreBar": "buildAnswerSheet/components/ScoreBar",
        "SecretBox": "buildAnswerSheet/components/SecretBox",
        "SheetComp": "buildAnswerSheet/components/SheetComp",
        "Solve": "buildAnswerSheet/components/Solve",
        "Title": "buildAnswerSheet/components/Title",
        "XZTComp": "buildAnswerSheet/components/XZTComp",
        "content4Unit": "buildAnswerSheet/unit-component/content4Unit",
        "Paragraph": "buildAnswerSheet/unit-component/Paragraph",
        "UnitFillBlank": "buildAnswerSheet/unit-component/UnitFillBlank",
        "constant": "config/constant",
        "d3": "assets/script/lib/d3.min",
		"tableau-2": "assets/script/lib/tableau-2",
		"tableau-2.1": "assets/script/lib/tableau-2.1.0",
		"bi": "assets/script/lib/jquery.bi",
        "apiCommon": "service/api/api.common",
        'transData2XML': "tableManage/transData2XML"
    },
    //这个配置是你在引入依赖的时候的包名
    shim: {
        "angular": {
            exports: "angular"
        },
        "angular-route": {
            deps: ["angular"],
            exports: "angular-route"
        },
        "angular-cookies": {
            deps: ["angular"], // 依赖主框架
            exports: "angular-cookies"
        },
        "ngDialog": {
            deps: ["angular"],
            exports: "ngDialog"
        },
        "icheck": {
            deps: ["jquery"],
            exports: "icheck"
        },
        "jquery.datetimepicker": {

            exports: "jquery.datetimepicker"
        },
        "jquery.dataTables": {
            deps: ["jquery"],
            exports: "jquery.dataTables"
        },
        "dataTables.bootstrap": {
            deps: ["bootstrap"],
            exports: "dataTables.bootstrap"
        },
        "ckeditor": {
            exports: "ckeditor"
        },
        "underscore-min": {
            exports: "underscore-min"
        },
        "keyPointMarkView": {
            deps: ["angular"],
            exports: "keyPointMarkView"
        },
        "KPMInterfacesService": {
            deps: ["angular"],
            exports: "KPMInterfacesService"
        },
        "keyPointManager": {
            deps: ["angular"],
            exports: "keyPointManager"
        },
        "sourceManageView": {
            deps: ["angular"],
            exports: "sourceManageView"
        },
        "highcharts": {
            deps: ["jquery"],
            exports: "highcharts"

        },
        "exporting": {
            exports: "exporting"
        },
        "angularAMD": {
            deps: ["angular"],
            exports: "angularAMD"
        },
        "ngload": {
            deps: ["angularAMD"],
            exports: "ngload"
        },
        "bootstrap": {
            deps: ["jquery"],
            exports: "bootstrap"
        },
        "star-rating": {
            deps: ["jquery"],
            exports: "star-rating"
        },
        "pagination": {
            exports: "pagination"
        },
        "download": {
            exports: "download"
        },
        "FileSaver": {
            exports: "FileSaver"
        },
        "jquery.wordexport": {
            exports: "jquery.wordexport"
        },
        "dateFomart": {
            exports: "dateFomart"
        },
        "jquerymedia": {
            exports: "jquerymedia"
        },
        "angular-loading-bar": {
            deps: ["angular"],
            exports: "angular-loading-bar"
        },
        "dragable": {
            deps: ["jquery"],
            exports: "dragable"
        },
        "sitelogo": {
            deps: ["jquery"],
            exports: "sitelogo"
        },
        "cropper": {
            deps: ["jquery"],
            exports: "cropper"
        },
        "ng-file-upload": {
            deps: ["angular"],
            exports: "ng-file-upload"
        },
        "directive": {
            deps: ["angular", "jquery"],
            exports: "directive"
        },
        "richDirective": {
            deps: ["angular", "jquery"],
            exports: "richDirective"
        },
        "lazyTree": {
            deps: ["angular", "jquery"],
            exports: "lazyTree"
        },
        "filter": {
            deps: ["angular"],
            exports: "filter"
        },
        "ba-throttle-debounce": {
            deps: ["jquery"],
            exports: "ba-throttle-debounce"
        },
        "jquery-stickyheader": {
            deps: ["jquery"],
            exports: "jquery-stickyheader"
        },
        "backEndService": {
            deps: ["angular"],
            exports: "backEndService"
        },
        "utilService": {
            deps: ["angular", "underscore-min"],
            exports: "utilService"
        },
        "paperService": {
            deps: ["angular", "utilService", "underscore-min"],
            exports: "paperService"
        },
        "cartService": {
            deps: ["angular", "underscore-min"],
            exports: "cartService"
        },
        "customService": {
            deps: ["angular"],
            exports: "customService"
        },
        "evaluate": {
            deps: ["angular"],
            exports: "evaluate"
        },
        "jqprint": {
            deps: ["jquery"],
            exports: "jqprint"
        },
        "jqrcode": {
            deps: ["jquery"],
            exports: "jqrcode"
        },
        "echarts": {
            deps: ["jquery"],
            exports: "echarts"
        },
        'requestAnimationFrame': {
            deps: ["jquery"],
            exports: "requestAnimationFrame"
        },
        "jqfly": {
            deps: ["jquery"],
            exports: "jqfly"
        },
        "TransferPaper2Html": {
            deps: ["jquery", "underscore", "FileSaver"],
            exports: "TransferPaper2Html"
        },
        "wyCarousel":{
            deps:["jquery"],
            exports:"wyCarousel"
        },
        "Class": {
            exports: "Class"
        },
        "jquery-barcode": {
            deps: ["jquery"]
        },
        "AnswerTip": {
            deps: ["jquery", "underscore", "jquery-barcode", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "BlackBlock": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "Blank": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "Composition": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "ExamName": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "FillBlank": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "OptionGroup": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "Page": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class','SheetComp']
        },
        "ScoreBar": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "SecretBox": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "SheetComp": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class']
        },
        "Solve": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'SheetComp']
        },
        "Title": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "XZTComp": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "content4Unit": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp', 'Paragraph']
        },
        "Paragraph": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
        "UnitFillBlank": {
            deps: ["jquery", "underscore", "sheetUtil", "EventBus", 'Class', 'SheetComp']
        },
		"tableau-2": {
            deps: ["tableau-2.1", "d3"]
        },
		"tableau-2.1": {
			deps: [ "d3"]
        },
        "bi": {
            deps: ["jquery", "tableau-2", "tableau-2.1", "d3"]
        },
        "apiCommon": {
            deps: [ "backEndService"]
        }
    },
    waitSeconds: 0,
    //增量更新版本号
    urlArgs: "ver=" + config.version
});

define(["angular", "angularAMD", './config/routerConfig', './config/runController', './config/apiBaseUrlConfig', "angular-route", "bootstrap","angular-cookies",
    "ngDialog", "ng-file-upload", 'angular-loading-bar', "directive", "richDirective", "lazyTree", "filter", "backEndService", "customService",
    "utilService", "paperService", 'cartService', 'apiCommon'
], function (angular, angularAMD, routerConfig, runController, baseUrlCfg) {
    var app = angular.module('myApp', [
        'ui.router',
        'ngDialog',
        'ngFileUpload',
        'ngCookies',
        "angular-loading-bar",
        "customizeDirective",
        "customizeFilter",
        "backEndService",
        "richDirective",
        "customService",
        "paperService",
        "Util",
        "lazyTree",
        'cartService',
        'api.common'
    ]).config(routerConfig)
        .run(runController);

    for(var key in baseUrlCfg) {
        app.constant(key, baseUrlCfg[key]);
    }

    return angularAMD.bootstrap(app);
});
