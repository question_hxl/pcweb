define(['jquery.dataTables', 'highcharts'], function () {
	return ['$http', '$scope', '$rootScope', '$state', '$location', function ($http, $scope, $rootScope, $state, $location) {
		var Sfid = $location.search().Sfid;
		var ClassFlnkid = $location.search().ClassFlnkid; // 班级ID
		$scope.ClassFlnkid = ClassFlnkid;
		$scope.UserId = Sfid;

		// 页面初始数据
		var init = function (Sfid) {
			// 用户战斗力变化
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0166A.ashx',
				data: {
					//查看所有限制100条
					Limit: 10,
					UserFlnkID: Sfid
				}
			}).success(function (data) {
				$scope.Combat = data.msg;
				$scope.ClassName = data.msg[0].ClassName;
				$scope.MyName = data.msg[0].MyName;
				// 班级战斗力
				var ClassPow = [], MyPow = [], Day = [];
				for (var i = 0 ; i < data.msg.length ; i++) {
					ClassPow.push(Number(data.msg[i].ClassPow))
				}
				// 自身战斗力
				for (var i = 0 ; i < data.msg.length ; i++) {
					MyPow.push(Number(data.msg[i].MyPow))
				}
				// 日期
				for (var i = 0 ; i < data.msg.length ; i++) {
					Day.push(data.msg[i].Day)
				}
				highchartsline(ClassPow, MyPow, Day);
			}).then(function () {
				//用户战斗力信息
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0165.ashx',
					data: {
						UserFlnkID: Sfid
					}
				}).success(function (data) {
					$scope.UserCombat = data.msg[0];
				})
			}).then(function () {
				// 成绩数据
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0167.ashx',
					data: {
						Limit: 100,
						UserFlnkID: Sfid
					}
				}).success(function (data) {
					var StuAnalysis = data.msg;
					$scope.StuAnalysis.fnAddData(StuAnalysis);
				});
				//成绩数据table绘制
				$scope.StuAnalysis = $('#allStuAnalysis').dataTable({
					"aoColumns": [
					{ "mDataProp": "Order", "sWidth": "8%" },
					{ "mDataProp": "ExamName", "sWidth": "18.5%" },
					{ "mDataProp": "ExamTime", "sWidth": "18.5%" },
					{ "mDataProp": "AllSc", "sWidth": "10%" },
					{ "mDataProp": "Sc", "sWidth": "10%" },
					{ "mDataProp": "ClassAvgSc", "sWidth": "15%" },
					{ "mDataProp": "Rank", "sWidth": "10%" },
					{ "mDataProp": "ExamFlnkID", "sWidth": "10%" }
					],
					"aoColumnDefs": [
						{
							"aTargets": [7], "mRender": function (data, type, full) {
								return '<a class="homeviewbtnana" href="#/Perforsis?ExamFlnkID=' + data + '&ClassFlnkid=' + ClassFlnkid + '&ExAnswerFlnkID=' + full.ExAnswerFlnkID + '">查看详细</a>';
							}
						},
						{
							"aTargets": [4], "mRender": function (data, type, full) {
								return '<div class="orange">' + data + '</div>';
							}
						},
						{
							"aTargets": [1], "mRender": function (data, type, full) {
								return '<div data-toggle="tooltip" data-placement="right"  ng-mouseover="moveOver()" class="t_overflow" title="' + data + '">' + data + '</div>';
							}
						}
					],
					"oLanguage": {
						"sProcessing": "处理中...",
						"sLengthMenu": "显示 _MENU_ 项结果",
						"sZeroRecords": "没有匹配结果",
						"sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
						"sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
						"sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
						"sInfoPostFix": "",
						"sSearch": "搜索：",
						"sUrl": "",
						"sEmptyTable": "表中数据为空",
						"sLoadingRecords": "载入中...",
						"sInfoThousands": ",",
						"oPaginate": {
							"sFirst": "首页",
							"sPrevious": "上页",
							"sNext": "下页",
							"sLast": "末页"
						}
					},
					"bDestroy": true,//允许重新加载表格对象数据
					"bRetrieve": true,
					"bProcessing": false,
					"bLengthChange": false,
					"bPaginate": false,
					"bAutoWidth": true,
					"bFilter": false,//禁用搜索
					"bLengthChange": false,//禁用分页显示
				});
				$('#allStuAnalysis').dataTable().fnClearTable();
			})
		}
		init(Sfid);
		// 战斗力提升
		var highchartsline = function (ClassPow, MyPow, Day) {
			$('#containers').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: false
				},
				xAxis: {
					categories: Day
				},
				credits: {
					enabled: false,
				},
				yAxis: {
					title: false
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					floating: true
				},

				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						}
					}
				},
				series: [{
					name: '个人成绩变化',
					color: '#ff4c79',
					data: MyPow
				}, {
					name: '班级平均成绩',
					color: '#2dc0e8',
					data: ClassPow
				}]
			});
		}

		// tips
		$scope.moveOver = function () {
			$("[data-toggle='tooltip']").tooltip({
				html: true
			});
		}
	}]
})


