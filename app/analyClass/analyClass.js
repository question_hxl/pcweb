define(['jquery.dataTables', 'underscore-min', 'highcharts'], function () {
	return ['$http', '$scope', '$rootScope', '$state', 'ngDialog', '$location', function ($http, $scope, $rootScope, $state, ngDialog, $location) {
		// 获取班级Id
		var ClassFlnkid = $location.search().ClassFlnkid;
		var cfid = [];
        $http.get($rootScope.baseUrl + '/Interface0150.ashx').then(function(res){
            if(_.isArray(res.data.msg)) {
                cfid = _.pluck(res.data.msg, 'classId')
			}else {
                cfid = [];
			}
		}, function(){
            cfid = [];
		});
		var getArrSet = function () { //  返回当前班级的位置
			var getArrClassSet = cfid.indexOf(ClassFlnkid, 0);
			return getArrClassSet;
		}
		$scope.next = function () {
			if (getArrSet() == cfid.length - 1) {
				$('.next').addClass('f_gray');
				return false;
			} else {
				var i = getArrSet() + 1;
				window.location.href = "#/analyClass?ClassFlnkid=" + cfid[i];
				window.location.reload();
			}
		}
		if (getArrSet() == 0) {
			$('.prev').addClass('f_gray');
			//return false;
		}
		if (getArrSet() == cfid.length - 1) {
			$('.next').addClass('f_gray');
		}
		$scope.prev = function () {
			if (getArrSet() == 0) {
				$('.prev').addClass('f_gray');
				return false;
			} else {
				var i = getArrSet() - 1;
				window.location.href = "#/analyClass?ClassFlnkid=" + cfid[i];
				window.location.reload();
			}
		}



		// 班级详细信息
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0162.ashx',
			data: {
				ClassFlnkid: ClassFlnkid,
			}
		}).success(function (data) {
			$scope.ClassFlnkList = data.msg[0];
		})
		// 战斗力变化
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0163.ashx',
			data: {
				ClassFlnkid: ClassFlnkid,
			}
		}).success(function (data) {
			var ClassStudentDate = [],
				ClassStudent = [];
			$scope.ClassStudent = data.msg;

			for (var i = 0; i < $scope.ClassStudent.length; i++) {
				ClassStudentDate.push($scope.ClassStudent[i].Day)
			}
			for (var i = 0; i < $scope.ClassStudent.length; i++) {
				ClassStudent.push(
					Number($scope.ClassStudent[i].Pow)
				)
			}
			highchartsLine(ClassStudentDate, ClassStudent);
		})
		// 学生战斗力
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0153.ashx',
			data: {
				ClassFlnkid: ClassFlnkid
			}
		}).success(function (data) {
			var Proportion = [];
			for (var i = 0; i < data.msg.length; i++) {
				//build
				var r = data.msg[i];
				Proportion.push([r.PartName, Number(r.Proportion), r.Section]);
			}
			// 学生战斗力
			$('#container').highcharts({
				title: {
					text: null
				},
				tooltip: {
					pointFormat: '<b>{point.percentage:.1f}%</b>'
				},
				credits: {
					enabled: false,
				},
				legend: {
					enabled: false
				},
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				plotOptions: {
					pie: {
						size: '75%',
						borderWidth: 0,
						showInLegend: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							color: '#000000',
							connectorColor: '#000000',
							format: '{point.percentage:.1f} %'
							//distance: -50, //通过设置这个属性，将每个小饼图的显示名称和每个饼图重叠  
							//style: {
							//	fontSize: '14px',
							//	lineHeight: '14px'
							//},
							//formatter: function (index) {
							//	return '<span style="color:#fff;text-shadow:none">' + this.point.y + '%</span>';
							//}
						},
					}

				},
				exporting: {
					enabled: false
				},
				series: [{
					type: 'pie',
					data: Proportion
				}]
			});
		})
		// 战斗力提升
		var highchartsLine = function (ClassStudentDate, ClassStudent) {
			$('#containers').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: null
				},
				xAxis: {
					categories: ClassStudentDate
				},
				credits: {
					enabled: false,
				},
				yAxis: {
					title: false
				},
				legend: {
					enabled: false
				},
				tooltip: {
					enabled: true,
					formatter: function () {
						return '' + this.x + ': ' + this.y ;
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				exporting: {
					enabled: false
				},
				series: [{
					data: ClassStudent
				}]
			});
		}
		// 学生信息列表
		//调用学生信息接口
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0164.ashx',
			data: {
				ClassFlnkid: ClassFlnkid,
			}
		}).success(function (data) {
			var tablesort = data.msg;
			$scope.table.fnAddData(tablesort);
		})
		//绘制学生信息表格
		$scope.table = $('#stuAnalysis').dataTable({
			"aoColumns": [{
				"mDataProp": "Order",
				"sWidth": "14%"
			}, {
				"mDataProp": "Name",
				"sWidth": "15%"
			}, {
				"mDataProp": "Pow",
				"sWidth": "14%"
			}, {
				"mDataProp": "LastExamSc",
				"sWidth": "14%"
			}, {
				"mDataProp": "ActiveDegree",
				"sWidth": "14%"
			}, {
				"mDataProp": "CompensateNum",
				"sWidth": "14%"
			}, {
				"mDataProp": "UserFLnkID",
				"sWidth": "15%"
			}],
			"aoColumnDefs": [
				{
					"aTargets": [6], "mRender": function (data, type, full) {
						return '<a class="homeviewbtnana" href="#/capacity?Sfid='+data+'&ClassFlnkid='+ClassFlnkid+'">查看详细</a>'
					}
				},
				{
					"aTargets": [5], "mRender": function (data, type, full) {
						return data;
					}
				},
				{
					"aTargets": [4], "mRender": function (data, type, full) {
						return data;
					}
				},
				{
					"aTargets": [3], "mRender": function (data, type, full) {
						
					    return data;
					}
				},
				{
					"aTargets": [2], "mRender": function (data, type, full) {
					    return data;
					}
				},
				{
					"aTargets": [1], "mRender": function (data, type, full) {
					    return '<a href="#/capacity?Sfid=' + full.UserFLnkID + '&ClassFlnkid=' + ClassFlnkid + '">' + data + '</a>';
					}
				},
				{
					"aTargets": [0], "mRender": function (data, type, full) {
					    return data;
					}
				}
			],
			"oLanguage": {
				"sProcessing": "处理中...",
				"sLengthMenu": "显示 _MENU_ 项结果",
				"sZeroRecords": "没有匹配结果",
				"sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
				"sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
				"sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
				"sInfoPostFix": "",
				"sSearch": "搜索：",
				"sUrl": "",
				"sEmptyTable": "表中数据为空",
				"sLoadingRecords": "载入中...",
				"sInfoThousands": ",",
				"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "上页",
					"sNext": "下页",
					"sLast": "末页"
				}
			},
			"bDestroy": true,//允许重新加载表格对象数据
			"bRetrieve": true,
			"bProcessing": false,
			"bLengthChange": false,
			"bPaginate": false,
			"bSort": true //排序功能
		});
		$('#stuAnalysis').dataTable().fnClearTable();

		// tips
		$scope.moveOver = function () {
			$("[data-toggle='tooltip']").tooltip({
				html: true
			});
		}
	}]
})