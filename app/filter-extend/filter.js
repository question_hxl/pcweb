/**
 * Created by Hexl on 2016/4/21.
 */
angular.module('customizeFilter', [])
    .filter('trustHtml', function ($sce) {
        return function (input) {
            return $sce.trustAsHtml(input);
        }
    })
    .filter('answerFilter', function () {
        return function (input, option) {
            var filterd = _.filter(input, function (item) {
                return item.UserAnswer === option;
            });
            return _.pluck(filterd, 'UserName').join(',');
        }
    })
    .filter('secondToFull', function () {
        return function (input, param) {
            input = input || 0;
            var dest = '';
            var hour = Math.floor(input / 3600),
                minute = Math.floor(input % 3600 / 60),
                second = (input % 3600 % 60).toFixed(0);
            switch (param) {
                case '时分秒':
                    dest = '<span>' + hour + '</span>时<span>' + minute + '</span>分<span>' + second + '</span>秒';
                    break;
                default:
                    dest = hour + ':' + minute + ':' + second;
                    break;
            }
            return dest;
        }
    })
    .filter('secondToMinute', function () {
        return function (input) {
            input = input || 0;
            var minute = Math.floor(input / 60);
            if (minute < 10) {
                minute = '0' + minute;
            }
            var second = input % 60;
            if (second < 10) {
                second = '0' + second;
            }
            return minute + ' : ' + second;
        };
    })
    .filter('fillZero', function () {
        return function (input) {
            input = +input || 0;
            if (input < 10) {
                return '0' + input;
            } else {
                return input + '';
            }
        }
    })
    .filter('stuByAnswer', function () {
        return function (input) {
            return _.pluck(input, 'UserName').join(',');
        }
    })
    .filter('formatDate', function () {
        return function(input, param){
            var year = input.slice(0, 4),
                month = input.slice(4, 6),
                day = input.slice(6, 8);
            var hour = input.slice(11,13),
                min = input.slice(14,16),
                sec = input.slice(17,19);
            var reg = /[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/;
            if(reg.test(input)) {
                var date = new Date(input);
                year = date.getFullYear();
                month = date.getMonth() + 1;
                month = month < 10 ? ('0' + month) : month;
                day = date.getDate();
                day = day < 10 ? ('0' + day) : day;
                hour = date.getHours();
                hour = hour < 10 ? ('0' + hour) : hour;
                min = date.getMinutes();
                min = min < 10 ? ('0' + min) : min;
                sec = date.getSeconds();
                sec = sec < 10 ? ('0' + sec) : sec;
            }
            var result = input;
            switch(param) {
                case 'yyyy-MM-DD':
                    result = year + '-' + month + '-' + day;
                    break;
                case 'MM月DD':
                    result = month + '月' + day;
                    break;
                case 'MM-DD':
                    result = month + '-' + day;
                    break;
                case 'MM-DD HH:mm':
                    result = month + '-' + day + ' ' + hour + ':' + min;
                    break;
                default:
                    break;
            }
            return result;
        }
    })
    .filter('filterForMarkRecord', function(){
        return function(source, key){
            if(!key){
                var result = _.filter(source, function(item){
                    return source;
                });
            }else{
                var result = _.filter(source,function (item) {
                    return (item.stuname.indexOf(key) >= 0) || (item.stuno.indexOf(key) >= 0);
                });
            }
            return result;
        };
    })
    .filter('arrayToString', function(){
        return function(input, key){
            if(!input) {
                return '暂无内容';
            }
            if(_.isArray(input)) {
				if(key) {
                    return _.pluck(input, key).join(',');
				}else {
					return input.join(',');
				}
            }else {
                throw new Error('数据类型必须为数组！');
            }
        }
    })
    .filter('optionalFilter', function(){
        return function(input, opt){
            return _.filter(input, function(item){
                return !!item[opt];
            });
        };
    });