/**
 * Created by 贺小雷 on 2017-04-28.
 */
define(['jquery.datetimepicker'], function () {
    return ['$scope', '$http', '$location', '$rootScope', '$q', 'constantService', 'paperService', 'Upload', 'ngDialog', 'resourceUrl','apiCommon',
        function ($scope, $http, $location, $rootScope, $q, constantService, paperService, Upload, ngDialog, resourceUrl, apiCommon) {
            $scope.logo = '/assets/skin/' + window.config.theme + '/img/loading-logo.png';
            $scope.qstList = [];
            $scope.isShowAnswer = false;
            $scope.isShowStuAnswer = false;
            $scope.isStudent = $location.search().isStudent;
            $scope.studentId = $location.search().studentId;
            var user = JSON.parse(sessionStorage.getItem('currentUser'));

            $scope.paperList = [];
            $('#start_datetimepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: "Y-m-d",
                todayButton: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    $('#start_datetimepicker').datetimepicker('hide');
                }
            });
            $('#end_datetimepicker').datetimepicker({
                lang: "ch",
                timepicker: false,
                format: "Y-m-d",
                todayButton: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    $('#end_datetimepicker').datetimepicker('hide');
                }
            });
            $http.get($rootScope.baseUrl + '/Interface0196.ashx').success(function (res) {
                $scope.ClassTypeList = res.msg;
                $scope.ClassTypeList.unshift({ExType: '全部'});
                $scope.curClassType = $scope.ClassTypeList[0];
                if ($scope.isStudent) {
                    if(+user.GradeNo < 10){
                        $http.get(resourceUrl + '/Subject/GetSubjectNameByGradeNum?gradeNum=' + user.GradeNo
                        ).success(function (res) {
                            $scope.subjectList = res.ResultObj;
                            //$scope.curSubject = $scope.subjectList[0];
                            //paperList();
                        });
                    }else {
                        $http.post($rootScope.baseUrl + '/Interface0220B.ashx', {pharseId: user.pharseId}).then(function (res) {
                            $scope.subjectList = [];
                            if (_.isArray(res.data.msg)) {
                                _.each(res.data.msg, function (itemSub) {
                                    $scope.subjectList.push({
                                        subjectName: itemSub.subjectName,
                                        subjectid: itemSub.subjectId
                                    })
                                });
                            }
                        });
                    }
                } else {
                    $scope.curSubject = {subjectid: user.subjectId};
                    //paperList();
                }
            });
            $http.post('/BootStrap/Interface/generalQuery.ashx', {
                Proc_name: 'Proc_GetStudentInfo',
                userFid: $scope.studentId
            }).then(function (res) {
                if (!!res.data.msg && res.data.msg.length > 0) {
                    $scope.studentName = res.data.msg[0].UserName;
                    $scope.schoolName = res.data.msg[0].schoolname;
                    $scope.className = res.data.msg[0].classname;
                }
            });
            function paperList() {
                if($scope.curSubject === undefined || $scope.curSubject === null) {
                    $scope.curSubject = undefined;
                    return
                }
                $http.post('/BootStrap/Interface/generalQuery.ashx', {
                    Proc_name: 'Proc_GetStudentExams',
                    userFid: $scope.studentId,
                    subjectId: $scope.curSubject.subjectid
                }).then(function (res) {
                    if (!!res.data.msg && res.data.msg.length > 0) {
                        $scope.paperList = res.data.msg;
                        $scope.paperList.unshift({ExExamFLnkID: '1', TaskName: '显示全部错题', extype: ''});
                        $scope.curPaperList = $scope.paperList;
                        $scope.selectedPaper = $scope.curPaperList[0];
                    }else {
                        $scope.paperList = [];
                        $scope.curPaperList = $scope.paperList;
                    }
                });
            }

            $scope.$watch('curSubject', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === undefined) {
                    return;
                }
                $scope.paperList = [];
                $scope.qstListAll = [];
                $scope.curPaperList = [];
                $scope.isShow = false;
                $scope.errorNum = null;
                curSearchPaperList = [];
                paperList();
            });
            $scope.$watch('s_date', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === undefined) {
                    return;
                }
                $scope.searchErrorBook();
            });
            $scope.$watch('e_date', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === undefined) {
                    return;
                }
                $scope.searchErrorBook();
            });
            $scope.$watch('curClassType', function (newValue, oldValue) {
                if (newValue === oldValue || newValue === undefined || oldValue === undefined) {
                    return;
                }
                $scope.searchErrorBook();
            });
            var curSearchPaperList = [];
            $scope.$watch('selectedPaper', function (newValue, oldValue) {
                if (!newValue || newValue === oldValue || newValue === undefined || newValue === '') {
                    return;
                }
                if (newValue.ExExamFLnkID === '1' && $scope.curPaperList.length > 1) {
                    $scope.searchErrorBook();
                } else if (newValue.ExExamFLnkID !== '1') {
                    curSearchPaperList = [];
                    curSearchPaperList.push(newValue);
                }
            });
            //TODO 获取错题 支持分页获取 如果获取全部则不传试卷id，只传页数
            $scope.getErrorQuestion = function() {
                if(curSearchPaperList.length === 0) {
                    return
                }
                $scope.errorNum = 0;
                $scope.qstListAll = [];
                $scope.isShow = true;
                var deferList = [], deferList2 = [];
                _.each(curSearchPaperList, function (val) {
                    if (val.ExExamFLnkID !== '1') {
                        deferList.push(paperService.getPaperDataById(val.ExExamFLnkID));
                        deferList2.push($http.post('/BootStrap/Interface/UserExamErrorQ.ashx', {exanswerFid: val.aFlnkid}))
                    }
                });
                $q.all(deferList).then(function (res) {
                    $scope.examList = [];
                    _.each(res, function (item) {
                        if (item.data) {
                            $scope.examList.push(item);
                        }
                    });
                    $q.all(deferList2).then(function (resp) {
                        $scope.errorList = [];
                        _.each(resp, function (item) {
                            if (item.data) {
                                $scope.errorList.push(item);
                            }
                        });
                        _.each($scope.errorList, function (error, errorIndex) {
                            _.each($scope.examList, function (exam, examIndex) {
                                if (errorIndex === examIndex) {
                                    $scope.qstList = [];
                                    paperService.setQuestionsOrder(exam.data.msg);
                                    var qstAllList = paperService.getAllQuestions(exam.data.msg);
                                    _.each(error.data.qlist, function (item) {
                                        var qstDetail = _.find(qstAllList, function (q) {
                                            return item.qflnkid === q.FLnkID
                                        });
                                        if (qstDetail) {
                                            if (qstDetail.isMain) {
                                                //如果错题为父题或者普通题，直接放入试题列表中
                                                qstDetail.getRate = item.getRate;
                                                if (error.data.cutPosition && error.data.Accessory) {
                                                    qstDetail.stuAnswer = '见答题卡';
                                                } else {
                                                    if (qstDetail.isObjective == '1') {
                                                        qstDetail.stuAnswer = item.answer;
                                                    } else {
                                                        qstDetail.stuAnswer = '';
                                                    }
                                                }
                                                qstDetail.qAccessory = item.qAccessory;
                                                $scope.qstList = $scope.qstList.concat(qstDetail);
                                            } else if (qstDetail.isSub) {
                                                //否则，如果为子题，则获取其父题，并过滤
                                                var mainQst = JSON.parse(JSON.stringify(qstDetail.parent));
                                                //多道子题在一个父题中展示
                                                var isParentExist = _.find($scope.qstList, function (data) {
                                                    return data.FLnkID === mainQst.FLnkID;
                                                });
                                                //如果父题已存在在错题列表中，说明已经过滤过该父题的错题，不需要再次处理
                                                if (!isParentExist) {
                                                    if (mainQst.mode === 'B' || mainQst.Mode === 'B') {
                                                        _.each(mainQst.sub, function (item) {
                                                            item.orders = item.SubOrder;
                                                        });
                                                    }
                                                    //过滤父题的子题列表，如果存在错题才显示
                                                    mainQst.sub = _.filter(mainQst.sub, function (sub) {
                                                        var isSubError = _.find(error.data.qlist, function (errorQst) {
                                                            return errorQst.qflnkid === sub.FLnkID;
                                                        });
                                                        if (isSubError) {
                                                            sub.getRate = isSubError.getRate;
                                                            if (error.data.cutPosition && error.data.Accessory) {
                                                                sub.stuAnswer = '见答题卡';
                                                            } else {
                                                                if (qstDetail.isObjective == '1') {
                                                                    sub.stuAnswer = isSubError.answer;
                                                                } else {
                                                                    sub.stuAnswer = '';
                                                                }
                                                            }
                                                            sub.qAccessory = isSubError.qAccessory;
                                                            sub.qAccessory = isSubError.qAccessory;
                                                        }
                                                        return !!isSubError;
                                                    });
                                                    mainQst.mode = mainQst.Mode || mainQst.mode || '';
                                                    $scope.qstList = $scope.qstList.concat(mainQst);
                                                }
                                                //子题独立显示
                                                // mainQst.sub = _.filter(mainQst.sub, function(s){
                                                //     return s.FLnkID === qstDetail.FLnkID;
                                                // });
                                                // _.each(mainQst.sub, function (value) {
                                                //     value.getRate = item.getRate;
                                                //     if($scope.qst.cutPosition && $scope.qst.Accessory){
                                                //         value.stuAnswer = '见答题卡';
                                                //     }else {
                                                //         value.stuAnswer = item.answer;
                                                //     }
                                                //     value.qAccessory = item.qAccessory;
                                                //     value.mode = value.Mode || value.mode || '';
                                                // });
                                                // mainQst.getRate = item.getRate;
                                                // if($scope.qst.cutPosition && $scope.qst.Accessory){
                                                //     mainQst.stuAnswer = '见答题卡';
                                                // }else {
                                                //     mainQst.stuAnswer = item.answer;
                                                // }
                                                // mainQst.qAccessory = item.qAccessory;
                                                // mainQst.mode = mainQst.Mode || mainQst.mode || '';
                                                // $scope.qstList =$scope.qstList.concat(mainQst);
                                            }
                                        }
                                        error.data.qstList = $scope.qstList;
                                    });

                                    // $scope.qstList.sort(function (a, b) {
                                    //     if(a.orders === b.orders) {
                                    //         return a.sub[0].orders - b.sub[0].orders;
                                    //     }else {
                                    //         return a.orders - b.orders;
                                    //     }
                                    // });
                                    $scope.qstListAll.push(error.data);
                                }
                            });
                        });
                        _.each($scope.qstListAll, function (item) {
                            if (item.qstList) {
                                $scope.errorNum += item.qstList.length;
                            }
                            if (item.cutPosition && item.Accessory) {
                                isImgLoaded(item.Accessory).then(function ($el) {
                                    $($el).appendTo($('body'));
                                    item.AccessoryWidth = $($el).width();
                                    item.AccessoryHeight = $($el).height();
                                    if (item.AccessoryWidth > 900) {
                                        var width = item.AccessoryWidth;
                                        item.AccessoryWidth = 900;
                                        item.AccessoryHeight = 900 * item.AccessoryHeight / width;
                                    }
                                    $($el).remove();
                                })
                            }
                            _.each(item.qstList, function (value) {
                                if (!item.cutPosition || !value.Answer) {
                                    isImgLoaded(value.qAccessory).then(function ($el) {
                                        $($el).appendTo($('body'));
                                        value.qAccessoryWidth = $($el).width();
                                        value.qAccessoryHeight = $($el).height();
                                        if (value.qAccessoryWidth > 800) {
                                            var width = value.qAccessoryWidth;
                                            value.qAccessoryWidth = 800;
                                            value.qAccessoryHeight = 800 * value.qAccessoryHeight / width;
                                        }
                                        $($el).remove();
                                    })
                                }
                            })
                        });
                    });
                });
            };

            $scope.searchErrorBook = function () {
                $scope.isShow = false;
                var afterDate;
                var endTime = $('#end_datetimepicker').val(),
                    startTime = $('#start_datetimepicker').val();
                $http.get('/BootStrap/Interface/getSysTime.ashx').then(function (res) {
                    if(+res.status === 200){
                        afterDate = res.data;
                        // var myDate = new Date();
                        // var afterDate = myDate.getFullYear() + '-' + ['0'+(myDate.getMonth()+1)].slice(-2) + '-'+ ['0'+(myDate.getDate())].slice(-2);
                        if (endTime === '') {
                            endTime = afterDate;
                        }
                        if (endTime && startTime && endTime < startTime) {
                            if(afterDate < startTime) {
                                constantService.alert('开始时间需小于今天！');
                            } else {
                                constantService.alert('结束时间需大于开始时间！');
                            }
                            return false;
                        }
                        $scope.curPaperList = $scope.paperList;
                        if ($scope.curClassType) {
                            if ($scope.curClassType.ExType === '全部') {
                                if (startTime !== '') {
                                    $scope.curPaperList = _.filter($scope.curPaperList, function (item) {
                                        return (item.ExamTime >= startTime && item.ExamTime <= endTime) || item.extype === ""
                                    });
                                } else {
                                    $scope.curPaperList = _.filter($scope.curPaperList, function (item) {
                                        return  item.ExamTime <= endTime || item.extype === ""
                                    });
                                }
                            } else {
                                if (startTime === '') {
                                    $scope.curPaperList = _.filter($scope.curPaperList, function (item) {
                                        return ((item.extype === $scope.curClassType.ExType)) || item.extype === ""
                                    });
                                } else{
                                    $scope.curPaperList = _.filter($scope.curPaperList, function (item) {
                                        return ((item.ExamTime >= startTime && item.ExamTime <= endTime) && (item.extype === $scope.curClassType.ExType)) || item.extype === ""
                                    });
                                }
                            }
                            if($scope.curPaperList.length === 1 && $scope.curPaperList[0].ExExamFLnkID === '1'){
                                $scope.curPaperList = [];
                                $scope.qstListAll = [];
                                $scope.selectedPaper = '';
                            }else {
                                $scope.selectedPaper = $scope.curPaperList[0];
                            }
                            curSearchPaperList = $scope.curPaperList;
                        }
                    }
                });
            };

            $scope.reverse = function (index) {
                if (index === 1) {
                    $scope.isShowAnswer = !$scope.isShowAnswer;
                } else {
                    $scope.isShowStuAnswer = !$scope.isShowStuAnswer
                }
            };
            $scope.goPicture = function (pic) {
                window.open(pic);
            };
            $scope.downloadErrorBook = function () {
                if ($scope.paperList.length === 0 || $scope.paperList === undefined) {
                    return
                }
                var area = $('.main-container ul.error-qst-list').clone();
                var str = '<!DOCTYPE html><html>';
                str += '<head><meta charset="utf-8">';
                str += "<link type='text/css' rel='stylesheet' href='" + location.origin + "/errorBookDownload/exportErrorBookStyle.css'/>";
                str += "<link type='text/css' rel='stylesheet' href='" + location.origin + "/directive-extend/css/directive.css'/>";
                str += '</head>';
                str += '<body>';
                str += area.get(0).outerHTML;
                str += '</body></html>';
                apiCommon.exportHtml2Pdf({
                    content: str,
                    type: 'A4',
                    name: '学生错题本'
                }).then(function (res) {
                    if (res.data.code === 0) {
                        var link = $('#download-link');
                        link.attr('href', res.data.msg);
                        link.attr('download', '学生错题本.pdf');
                        link.find('#downloadlabel').click();
                    } else {
                        constantService.alert('下载错题本失败！');
                    }
                });
            };

            function isImgLoaded(url) {
                var defer = $q.defer();
                var $img = new Image();
                $img.src = url;
                $img.onload = function () {
                    setTimeout(function () {
                        defer.resolve($img);
                    }, 100);
                };
                $img.onerror = function () {
                    setTimeout(function () {
                        defer.reject(1);
                    }, 100);
                };
                if ($img.complete) {
                    setTimeout(function () {
                        defer.resolve($img);
                    }, 100);
                }
                return defer.promise;
            }
        }];
});