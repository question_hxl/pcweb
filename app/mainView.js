define(["jquery", "constant"], function ($, constant) {
    return ['$scope', '$state', '$rootScope', '$http', '$location', '$cookieStore', 'cartService','constantService', 'apiCommon', '$timeout', 'ngDialog',
        function ($scope, $state, $rootScope, $http, $location,$cookieStore, cartService, constantService, apiCommon, $timeout, ngDialog) {
            $scope.user = angular.fromJson(window.sessionStorage.getItem('currentUser'));
            $scope.isShow = false;
            $scope.notteach = false;
            $scope.associatedAccount = [];
            $scope.ROLE = constant.ROLE;
            //2016.9.5 zwd 用于判断是否展示右侧微博 微信
            if ($scope.user.role === $scope.ROLE.TEACHER || $scope.user.role === $scope.ROLE.MANAGER) {
                $scope.notteach = true;
            }
            $scope.logo = 'assets/skin/' + window.config.theme + '/img/logo.png';
            $scope.copy = window.config.copyInfo[window.config.theme];
            $scope.version = config.version;

            //是否获取关联账号状态位
            var isAssociateLoaded = false;
            var isLoadingAssociate = true;
            //获取关联账号
            function getAssociateAccount(){
                apiCommon.getAssociateAccounts().then(function (resp) {
					isAssociateLoaded = true;
					isLoadingAssociate = false;
					if(_.isArray(resp.data.msg)){
						$scope.associatedAccount = resp.data.msg;
					}else{
						$scope.associatedAccount = [];
					}
				},function () {
					isAssociateLoaded = true;
					isLoadingAssociate = false;
					$scope.associatedAccount = [];
				});
            }
			getAssociateAccount();

            //用于设置当前菜单
            $scope.activeNav = 'home';
            $scope.$on('$stateChangeSuccess', function (val, state) {
                $scope.activeNav = state.data.belong;
                if(state.data.hideShare) {
                    $("#shares").hide();
                } else {
                    $("#shares").show();
                }
                $scope.user = angular.fromJson(window.sessionStorage.getItem('currentUser'));
                if ($scope.user.role === $scope.ROLE.TEACHER || $scope.user.role === $scope.ROLE.MANAGER) {
                    $scope.notteach = true;
                    $('.main-sidebar').show();
                }
            });

            $rootScope.$on('CLEAR_CART_CACHE', function(){
                cartService.clearLocalCart();
            });

            $rootScope.$on('INIT_CART', function(){
                getBasketLength();
            });

            getBasketLength();

            $rootScope.$on('baseketList_number_change', function () {
                getBasketLength();
            });

            function getBasketLength() {
                cartService.getCart().then(function(res){
                    $scope.buycarNum = res.length;
                }, function(){
                    $scope.buycarNum = 0;
                });
            }

            $scope.logolink = function () {
                $location.path('');
            };

            $(function () {
                //2017-1-6 王建峰要求右侧购物车上面箭头始终展示
                /*$(window).scroll(function () {
                    if ($(window).scrollTop() > 100) {
                        $("#side-bar .gotop").fadeIn();
                    } else {
                        $("#side-bar .gotop").hide();
                    }
                });*/
                //$("#side-bar .gotop").click(function () {
                //	$('html,body').animate({ 'scrollTop': 0 }, 500);
                //});
                $(".gotop").click(function () {
                    $('body,html').animate({scrollTop: 0}, 500);
                    return false;
                });
            });

            //当前菜单
            $scope.gotonav = function (nav) {
                $scope.activeNav = nav;
            };

            //成绩报告
            $scope.gocjbg = function () {
                var answersList = localStorage.getItem('userPaperinfo');
                var infos = JSON.parse(answersList);
                console.log(infos);
                var examid = !!infos[0]?infos[0].ExamFlnkID : '';
                var classsid = $scope.user.scid;
                var eflnkid = !!infos[0]?infos[0].ExAnswerFlnkID : '';
                $location.path('/Perforsis').search({
                    ExamFlnkID: examid,
                    ClassFlnkid: classsid,
                    ExAnswerFlnkID: eflnkid
                });
            };

            $scope.goErrorBook = function () {
                window.open("#/stu-error-book?studentId=" + $scope.user.fid + '&isStudent=' + true + '&isOpenNewWin=' +1);
            };

            $scope.logout = function () {
				isAssociateLoaded = false;
				isLoadingAssociate = false;
                $http.post($rootScope.baseUrl + '/Interface0114.ashx').then(function (resp) {
                    sessionStorage.removeItem('currentUser');
                    $cookieStore.remove('currentUser');
                    $cookieStore.remove('userId');
                    cartService.clearLocalCart();
                    $state.go('login');
                });
            };

            // 导航菜单
            $scope.toggleMenu = function (index) {
                $scope.isShow = !$scope.isShow;
            };

            $rootScope.$on('userChange', function () {
                $scope.user = angular.fromJson(sessionStorage.getItem('currentUser'));
            });

            $rootScope.$on('UPDATE_ASSOCIATE_ACCOUNT', function(){
                if(!isAssociateLoaded && !isLoadingAssociate) {
					$scope.user = angular.fromJson(sessionStorage.getItem('currentUser'));
					getAssociateAccount();
                }
            });

            var curDate = new Date();
            $scope.curTime = curDate.getHours();
            if ($scope.curTime >= 7 && $scope.curTime <= 9) {
                $scope.sayHello = '早上好';
            }else if ($scope.curTime > 9 && $scope.curTime <= 12) {
                $scope.sayHello = '上午好';
            }else if ($scope.curTime > 12 && $scope.curTime <= 18) {
                $scope.sayHello = '下午好';
            } else {
                $scope.sayHello = '晚上好';
            }

            //多账户关联模拟登陆
            $scope.simulationLanding = function (item) {
                var fId = item.userFId;
				isAssociateLoaded = false;
				isLoadingAssociate = false;
                $http.post($rootScope.baseUrl + '/Interface0231A.ashx', {
                    userFId: fId
                }).success(function (data) {
                    if(data.code === 0) {
                        //模拟登录获取临时密码成功 在切换新账户登陆之前先将原来用户注销
                        var tempPwd = data.msg;
                        $http.post($rootScope.baseUrl + '/Interface0114.ashx').then(function (resp) {
                            sessionStorage.removeItem('currentUser');
                            cartService.clearLocalCart();
                            window.location.href = location.origin + '/index.html#/login?n=' + fId + '&p=' + tempPwd + '&isEm=1';
                            // window.location.reload();
                        });
                    }else {
                        constantService.alert('切换登录失败！');
                    }
                });
            };

            $scope.$on('$viewContentLoaded', function(){
                $('.main-view ').css({
                    'min-height': $(window).height() + 'px'
                });
            });

            $scope.ModifyPassword = function(){
                $scope.PassWord = {
                    old: '',
                    new1: '',
                    new2: ''
                };
                constantService.modal({
                    title: '修改密码',
                    scope: $scope,
                    template:
                    '<div style="margin: 2% 10%;">' +
                        '<label>登 &nbsp;录&nbsp; 名：<span style="font-weight: 200;">'+ $scope.user.SelfName +'</span></label></div>' +
                    '<div style="margin: 2% 10%;">' +
                        '<label>原 &nbsp;密&nbsp; 码：</label>' +
                        '<input type="passWord" id="Text1" ng-model="PassWord.old" placeholder="请输入原密码" style="border-radius: 4px;border: 1px solid #adadad;height: 34px;padding-left: 2%;" required/>' +
                        '<span class="text-error" style="color: red" ng-show="errorShow">原密码为空!</span>' +
                        '<span class="text-error" style="color: red" ng-show="PWDserrorShow">原密码错误!</span></div>' +
                    '<div style="margin: 2% 10%;">' +
                        '<label>新 &nbsp;密&nbsp; 码：</label>' +
                        '<input type="passWord" id="Text2" ng-model="PassWord.new1" placeholder="请输入新密码" style="border-radius: 4px;border: 1px solid #adadad;height: 34px;padding-left: 2%;" required/>' +
                        '<span class="text-error" style="color: red" ng-show="n1errorShow">新密码为空!</span></div>' +
                    '<div style="margin: 2% 10%;">' +
                        '<label>确认密码：</label>' +
                        '<input type="passWord" id="Text3" ng-model="PassWord.new2" placeholder="请确认新密码" style="border-radius: 4px;border: 1px solid #adadad;height: 34px;padding-left: 2%;" required/>' +
                        '<span class="text-error" style="color: red" ng-show="perrorShow">两次密码不一致!</span>' +
                        '<span class="text-error" style="color: red" ng-show="lerrorShow">密码长度低于六位!</span>' +
                        '<span class="text-error" style="color: red" ng-show="n2errorShow">确认密码为空!</span></div>',
                    type: 'confirm',
                    cancelText: '取消',
                    okText: '确认修改',
                    okFn: function(){
                        if($scope.PassWord.old === "" || $scope.PassWord.old === null){
                            $scope.PWDserrorShow = true;
                            $timeout(function(){
                                $scope.PWDserrorShow = false;
                            }, 900);
                        }
                        else {
                            $scope.PWDserrorShow = false;
                            if($scope.PassWord.new1 === "" || $scope.PassWord.new1 === null){
                                $scope.n1errorShow = true;
                                $timeout(function(){
                                    $scope.n1errorShow = false;
                                }, 900);
                            }
                            else {
                                $scope.n1errorShow = false;
                                if($scope.PassWord.new2 === "" || $scope.PassWord.new2 === null){
                                    $scope.n2errorShow = true;
                                    $timeout(function(){
                                        $scope.n2errorShow = false;
                                    }, 900);
                                }
                                else {
                                    $scope.n2errorShow = false;
                                    var NullValue = "";//如果未改变，赋值""
                                    var ChangePwd = $scope.PassWord.new1 === $scope.PassWord.new2 ? $scope.PassWord.new1 : NullValue;
                                    if(ChangePwd === "" || ChangePwd === null){
                                        $scope.perrorShow = true;
                                        $timeout(function(){
                                            $scope.perrorShow = false;
                                        }, 1000);
                                    }
                                    else {
                                        $scope.perrorShow = false;
                                        if(ChangePwd !== null && ChangePwd.length < 6){
                                            $scope.lerrorShow = true;
                                            $timeout(function(){
                                                $scope.lerrorShow = false;
                                            }, 800);
                                        } else {
                                            $scope.lerrorShow = false;
                                            $http({
                                                method : 'post',
                                                url : $rootScope.baseUrl + '/Interface0190.ashx',
                                                data : {
                                                    SelfName : NullValue,
                                                    Sex : NullValue,
                                                    Birthday : NullValue,
                                                    QQ : NullValue,
                                                    Mobile : NullValue,
                                                    Email : NullValue,
                                                    PassWord : $scope.PassWord.old,
                                                    NewPassWord : ChangePwd
                                                }
                                            }).success(function(data){
                                                if(+data.code === 0){
                                                    $scope.PWDsuccessShow = true;
                                                    sessionStorage.removeItem('currentUser');
                                                    ngDialog.close();
                                                    constantService.alert('密码已修改，请重新登录！', function () {
                                                        $state.go('login');
                                                    });
                                                } else {
                                                    $scope.errorMsg = data.msg;
                                                    $scope.PWDserrorShow = true;
                                                    $timeout(function(){
                                                        $scope.PWDserrorShow = false;
                                                    }, 1000);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            };
        }];
});