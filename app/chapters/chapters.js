define(['star-rating', 'underscore-min', 'icheck'], function () {
    return ['$rootScope', '$http', '$scope', '$q', '$location', '$state', 'ngDialog', 'constantService', function ($rootScope, $http, $scope, $q, $location, $state, ngDialog, constantService) {

        var GradeId = $scope.GradeId = JSON.parse(window.sessionStorage.getItem('currentUser')).GradeId;
        var USER = angular.fromJson(sessionStorage.getItem('currentUser'));
        //保存题型信息
        $scope.questionsTypeArray = [];
        // 获取年级
        $http.get($rootScope.baseUrl + '/Interface0290A.ashx').success(function (res) {
            $scope.gradeList = res.course;
            console.log(GradeId);
            $scope.currentClassName = GradeId;
            $scope.currentId = GradeId;//res.course[0].gradeId
            $scope.numbers = 0;
            $scope.Difficult = 3;
            $scope.isNumbers = false;
            $scope.QBelong = 1;
            $scope.ChooseA = $scope.ChooseB = $scope.ChooseC = $scope.ChooseD = $scope.ChooseE = $scope.ChooseF = false;
            $scope.qbank = 3;
        });
        $http.get($rootScope.baseUrl + '/Interface0196.ashx').success(function (res) {
            if (_.isArray(res.msg)) {
                $scope.PaperTypeList = res.msg;
                $scope.paperType = _.find($scope.PaperTypeList, function(item){
                    return item.ExType === '周测';
                }) || $scope.PaperTypeList[0];
            }
        });
        // $watch
        $scope.$watch('currentClassName', function () {
            if ($scope.gradeList !== undefined) {
                var grade = _.find($scope.gradeList, function(item){
                    return item.gradeId === $scope.currentId;
                });
                $scope.currentGrade = grade;
                if(grade && grade.isChooese === '1') {
                    $http.post($rootScope.baseUrl + '/Interface0290B.ashx', {
                        gradeId: $scope.currentId
                    }).then(function(res){
                        if(res.data.code === 0) {
                            if(_.isArray(res.data.msg)) {
                                $scope.NJList = res.data.msg;
                                var findGrade = _.find($scope.NJList, function(item){
                                    return item.gradeId === $scope.currentId;
                                });
                                $scope.currentNJ = findGrade;
                            }
                        }else {
                            $scope.NJList = [];
                            $scope.currentNJ = {};
                        }
                    })
                }else {
                    $scope.NJList = [];
                    $scope.currentNJ = {};
                }
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0175.ashx',
                    data: {
                        gradeId: $scope.currentId
                    }
                }).success(function (res) {
                    if (res.msg == '' || res.msg.length == 0) {
                        $scope.havechapter = true;
                    } else {
                        $scope.havechapter = false;
                        var chapterData = res.msg;
                        $scope.treeData = res.msg;
                        _.each($scope.treeData, function(item){
                            if(!item.unit || item.unit.length === 0) {
                                var copyUnit = [];
                                copyUnit.push({
                                    unitId: item.unitId,
                                    unitName: item.unitName
                                });
                                item.unit = copyUnit;
                            }
                        });
                        //$scope.sectionsDates = res.msg[0].unit[0].section; // 默认第一个数据
                        $scope.currentUnit = $scope.treeData[0];
                        //如果只有一层
                        if (res.msg[0].unit == '' || res.msg[0].unit == []) {
                            $scope.currentSection = $scope.currentUnit || [];
                            console.log($scope.currentSection.unitId);
                            console.log($scope.currentSection.unitName);
                        } else {
                            $scope.currentSection = $scope.currentUnit.unit;
                            $scope.currentSubSection = $scope.currentSection.unit || [];
                        }
                    }
                })
            }

        });
        // 切换年级
        $scope.selectCur = function (data) {
            $scope.currentClassName = data;
            $scope.currentId = data;
            $scope.selected = [];
            $scope.selectedTags = [];
            $scope.currentTag = [];
            $scope.selectedID = '';
            $scope.numbers = 0;
        };
        $scope.choseNianji = function(data){
            $scope.currentNJ = data;
        };
        // 点击获取复选框
        $scope.selected = [];
        $scope.selectedTags = [];
        $scope.currentTag = [];
        var doAddOrRemove = function (action, id, name) {
            if (action == 'add' && $scope.selected.indexOf(id) == -1) {
                $scope.selected.push(id);
                $scope.selectedTags.push(name);
                $scope.currentTag.push({
                    'id': id,
                    'name': name
                });
                $scope.selectedID = $scope.selected.join();
                $scope.isNumbers = true;
            }
            if (action == 'remove' && $scope.selected.indexOf(id) != -1) {
                var idx = $scope.selected.indexOf(id);
                $scope.selected.splice(idx, 1);
                $scope.selectedTags.splice(idx, 1);
                $scope.currentTag.splice(idx, 1);
                $scope.selectedID = $scope.selected.join();

            }
            $scope.numbers = $scope.currentTag.length;
        };
        var updateSelected = function (action, section, name, isLast) {
            if (isLast) {
                doAddOrRemove(action, section.unitId, name);
            } else {

            }
        };
        $scope.updateSelection = function ($event, section, stopProp) {
            $event.stopPropagation();
            var checkboxs = $event.target;
            var action = (!$scope.isSelected(section) ? 'add' : 'remove');
            if (!stopProp) {
                //$scope.choseSection($event, section);
                if ($.isArray(section.unit) && !$.isEmptyObject(section.unit)) {
                    $scope.currentSubSection = section.unit;
                } else {
                    $scope.currentSubSection = [];
                }
            }
            if (section.unit && section.unit.length > 0) {
                $.each(section.unit, function (index, unit) {
                    updateSelected(action, unit, unit.unitName, true);
                });
                updateSelected(action, section, section.unitName, false);
            } else {
                updateSelected(action, section, section.unitName, true);
            }

        };

        $scope.isSelected = function (section) {
            if (section.unit && section.unit.length > 0) {
                var notSelectedUnit = _.find(section.unit, function (unit) {
                    return $scope.selected.indexOf(unit.unitId) === -1;
                });
                return !notSelectedUnit;
            } else {
                return $scope.selected.indexOf(section.unitId) >= 0;
            }
        };
        //  删除
        $scope.delmsg = function (id, name) {
            //updateSelected('remove', id, name);
            doAddOrRemove('remove', id, name);
        };
        // form

        $('.start').raty({ // 等级
            score: function () { 
                return $(this).attr('data-score');
            },
            path: function () {
                return this.getAttribute('data-path');
            },
            click: function (score, evt) {
                $scope.Difficult = score;
                if ($scope.selectedID != undefined) {
                    $http({
                        method: 'post',
                        url: $rootScope.baseUrl + '/Interface0178.ashx',
                        data: {
                            ContentId: $scope.selectedID,
                            QBelong: $scope.QBelong,
                            Difficult: $scope.Difficult
                        }
                    }).success(function (res) {
                        if (_.isArray(res.msg)) {
                            _.each (res.msg, function (item) {
                                item.initNumber = Math.min(5, +item.qtypeNum);
                            });
                            $scope.questionsTypeArray = res.msg;
                            if($scope.paperType.ExType === '课堂评测' || $scope.paperType.ExType === '机房评测'){
                                $scope.typeList = seekMultipleChoice(res.msg);
                            }else{
                                $scope.typeList = res.msg;
                            }
                        } else {
                            $scope.typeList = [];
                            $scope.questionsTypeArray = [];
                        }
                    })
                } else {
                    constantService.alert('请选择章节');
                    //ngDialog.open({
                    //	template:
                    //		'<p>请选择章节</p>' +
                    //		'<div class="ngdialog-buttons">' +
                    //		'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                    //	plain: true,
                    //	className: 'ngdialog-theme-default'
                    //});
                }

            }
        });
        var TypeId = [];  // 题目来源
        /*$scope.selectCurs = function (val, $event) {
            var checkboxs = $event.target;
            var index = _.indexOf($scope.QBelong, val);
            if (index >= 0) {
                $scope.QBelong.splice(index, 1);
            } else {
                $scope.QBelong.push(val);
            }
        };*/

        /*function getQBelong() {
            var qBelong = [];
            var checkboxs = $('.setting-white').find('input[type="checkbox"]');
            _.each(checkboxs, function (checkbox) {
                if (checkbox.checked) {
                    qBelong.push($(checkbox).val());
                }
            });
            return qBelong.join(',');
        }*/
        // 监听题目数量
        $scope.$watch('numbers', function () {


            if ($scope.selectedID != undefined) {
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0178.ashx',
                    data: {
                        ContentId: $scope.selectedID,
                        QBelong: $scope.QBelong,
                        Difficult: $scope.Difficult
                    }
                }).success(function (res) {
                    if (_.isArray(res.msg)) {
                        _.each(res.msg, function (item) {
                            item.initNumber = Math.min(5, +item.qtypeNum);
                        });
                        $scope.questionsTypeArray = res.msg;
                        if($scope.paperType.ExType === '课堂评测' || $scope.paperType.ExType === '机房评测'){
                            $scope.typeList = seekMultipleChoice(res.msg);
                        }else{
                            $scope.typeList = res.msg;
                        }
                    } else {
                        $scope.typeList = [];
                        $scope.questionsTypeArray = [];
                    }
                })
            }


        });
        // 监听题目类型
        $scope.$watch('QBelong', function () {
            if ($scope.selectedID != undefined) {
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0178.ashx',
                    data: {
                        ContentId: $scope.selectedID,
                        QBelong: $scope.QBelong,
                        Difficult: $scope.Difficult
                    }
                }).success(function (res) {
                    if (_.isArray(res.msg)) {
                        _.each(res.msg, function (item) {
                            item.initNumber = Math.min(5, +item.qtypeNum);
                        });
                        $scope.questionsTypeArray = res.msg;
                        if($scope.paperType.ExType === '课堂评测' || $scope.paperType.ExType === '机房评测'){
                            $scope.typeList = seekMultipleChoice(res.msg);
                        }else{
                            $scope.typeList = res.msg;
                        }
                    } else {
                        $scope.typeList = [];
                        $scope.questionsTypeArray = [];
                    }
                })
            }
        }, true);
        // 加减少数量
        $scope.add = function ($event, typeBox) {
            var maxnum = +typeBox.qtypeNum;
            if(typeBox.initNumber < maxnum) {
                typeBox.initNumber++;
            }
        };
        $scope.cutDown = function ($event, typeBox) {
            if(typeBox.initNumber > 0) {
                typeBox.initNumber--;
            }
        };
        // 文本框失去光标
        $scope.noblur = function ($event, typeObj) {
            var addbox = $event.target;
            var strs = +$(addbox).val();
            typeObj.initNumber = Math.min(strs, +typeObj.qtypeNum) || 0;
        };
        // 组卷方式

        // 出题按钮 //ui-sref="myApp.paperComposing" QTypeId
        $scope.sendMsg = function () {
            // 用户所选数目
            var userNum = [], qtypeId = [], QTypeName = [];
            var saveDate = [];
            if (!$scope.numbers) {
                //ngDialog.open({
                //	template:
                //		'<p>请选择题目章节</p>' +
                //		'<div class="ngdialog-buttons">' +
                //		'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                //	plain: true,
                //	className: 'ngdialog-theme-default'
                //});
                constantService.alert('请选择题目章节');
                return false;
            }
            if ($scope.typeList.length == 0) {
                //ngDialog.open({
                //	template:
                //		'<p>请选择题目章节</p>' +
                //		'<div class="ngdialog-buttons">' +
                //		'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                //	plain: true,
                //	className: 'ngdialog-theme-default'
                //});
                constantService.alert('请选择题目章节');
                return false;
            } else {
                angular.forEach($scope.typeList, function (item, index) {
                    // userNum +=  $('#'+ $scope.typeList[index].qtypeId).val() + ','; //用户选题数量
                    //  qtypeId += $scope.typeList[index].qtypeId + ',' ; // 用户选题类型
                    //userNum.push($('#' + $scope.typeList[index].qtypeId).val())

                    userNum.push(item.initNumber);
                    qtypeId.push($scope.typeList[index].qtypeId);
                    QTypeName.push($scope.typeList[index].qtypeName);
                });
            }

            if (!$scope.testpaperName) {
                //ngDialog.open({
                //	template:
                //		'<p>请输入试卷名称</p>' +
                //		'<div class="ngdialog-buttons">' +
                //		'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                //	plain: true,
                //	className: 'ngdialog-theme-default'
                //});
                constantService.alert('请输入试卷名称');
                return false;
            }
            var userChooesNum = 0;
            for (var i = 0; i < userNum.length; i++) {

                userChooesNum += Number(userNum[i])

            }
            if (userChooesNum == 0) {
                //ngDialog.open({
                //	template:
                //		'<p>请输入题目的数量</p>' +
                //		'<div class="ngdialog-buttons">' +
                //		'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                //	plain: true,
                //	className: 'ngdialog-theme-default'
                //});
                constantService.alert('请输入题目的数量');
                return false;
            }
            for (var i = 0; i < $scope.typeList.length; i++) {
                if (userNum[i] != 0) {
                    saveDate.push({
                        'ContentId': $scope.selectedID,
                        'QBelong': $scope.QBelong,
                        'exType': $scope.paperType.ExType,
                        'Difficult': $scope.Difficult.toString(),
                        'testpaperName': $scope.testpaperName,
                        'QNumber': userNum[i], // 用户所选题目数量
                        'QTypeId': qtypeId[i], // 题目类型ID
                        'QTypeName': QTypeName[i], // 题目名称
                        'ChooseA': $scope.ChooseA.toString(),
                        'ChooseB': $scope.ChooseB.toString(),
                        'ChooseC': $scope.ChooseC.toString(),
                        'ChooseD': $scope.ChooseD.toString(),
                        'ChooseE': $scope.ChooseE.toString(),
                        'ChooseF': $scope.ChooseF.toString(),
                        'gradeId': ($scope.currentNJ && $scope.currentNJ.gradeId) || $scope.currentId // 年级ID
                    })
                }
            }
            $http.post($rootScope.baseUrl + '/Interface0179.ashx', {
				msg: saveDate
            }).then(function(res){
                if(_.isArray(res.data.msg) && res.data.msg.length > 0) {
                    localStorage.setItem('answerList', JSON.stringify(res.data.msg));
					location.hash = '#/paperComposing?bankPosition=' + $scope.qbank + '&examType=' + $scope.paperType.ExType
                        + '&gradeid=' + (($scope.currentNJ && $scope.currentNJ.gradeId) || $scope.currentId) + '&examTitle=' + encodeURIComponent($scope.testpaperName);
                }else {
                    constantService.alert('出题错误，请重试！');
                }
            }, function(){
				constantService.alert('服务器异常，请稍后重试！');
            });
            // saveDate = JSON.stringify(saveDate);
            // console.log(saveDate);
            // console.log("开始存储");
            // localStorage.setItem('askList', saveDate);
            // console.log('存储完毕');
            // console.log(localStorage.getItem('askList') + '----save1');
            // localStorage.setItem('qbank',$scope.qbank);
            //
            // return false;
        };
        $scope.goback = function () {
            $state.go('myApp.questionIndex')
        };
        $scope.subSectionsDates = [];
        $scope.choseChapter = function (unit) {
            $scope.currentUnit = unit;
            if (unit.unit == '' || unit.unit == []) {
                $scope.currentSection = unit;
                console.log($scope.currentSection.unitId);
                console.log($scope.currentSection.unitName);
            } else {
                $scope.currentSection = unit.unit;
            }
        };
        $scope.choseSection = function ($event, unit) {
            if ($.isArray(unit.unit) && !$.isEmptyObject(unit.unit)) {
                $scope.currentSubSection = unit.unit;
            } else {
                $scope.currentSubSection = [];
                $scope.updateSelection($event, unit);
            }
        };
        $scope.isEmptySubSection = function () {
            return $.isEmptyObject($scope.currentSubSection);
        };
        $scope.isCurrentUnit = function (unit) {
            return $scope.currentUnit.unitName === unit.unitName;
        };

        /**
         * 当试卷类型为机房评测或者课堂评测时 试题类型只能为选择题
         * @param list
         * @returns {Array}
         */
        function seekMultipleChoiceAndBlank(list) {
            var temp = [];
            _.each(list,function (item) {
                if(item.qtypeName === '选择题' || item.qtypeName === '填空题'){
                    temp.push(item);
                }
            });
            return temp;
        }
        function seekMultipleChoice(list) {
            var temp = [];
            _.each(list,function (item) {
                if(item.qtypeName === '选择题'){
                    temp.push(item);
                }
            });
            return temp;
        }

        /**
         * 监听试卷类型变化
         */
        $scope.$watch('paperType',function (val) {
            if(!_.isEmpty(val)){
                if($scope.paperType.ExType === '课堂评测'){
                    $scope.typeList = seekMultipleChoice($scope.questionsTypeArray);
                }else if($scope.paperType.ExType === '机房评测' || '作业'){
                    if(USER.subjectId === 3) {
                        $scope.typeList = $scope.questionsTypeArray;
                    }else {
                        $scope.typeList = seekMultipleChoiceAndBlank($scope.questionsTypeArray);
                    }
                }else {
                    $scope.typeList = $scope.questionsTypeArray;
                }
            }
        });
        $scope.moveOver = function () {
            $("[data-toggle='tooltip']").tooltip({
                html: true
            });
        };
        //$('.inputs input').iCheck({
        //    checkboxClass: 'icheckbox_flat-blue'
        //});
    }]
});



angular.module('app', []).directive('treeView', [function () {
    return {
        restrict: 'EA',
        templateUrl: '/treeView.html',
        scope: {
            treeData: '=',
            textField: '@',
            itemTemplateUrl: '@'
        },
        controller: ['$scope', function ($scope) {
            $scope.isLeaf = function (item) {
                return !item.unit || !item.unit.length;
            };
            $scope.itemExpended = function (item, $event) {
                item.isExpend = !item.isExpend;
                $event.stopPropagation();
            };
            $scope.itemSelectCur = function (item, $event) {
                if (item.unitName == ' ') {
                    $scope.$parent.sectionsDates = item.unit;
                    console.log(item.unit)
                } else {
                    var cur = [];
                    cur.push({
                        'unitId': item.unitId,
                        'unitName': item.unitName
                    });
                    $scope.$parent.sectionsDates = cur;
                    return false
                }
            }

        }]

    }

}]);