define(['jquery.datetimepicker', 'pagination', 'FileSaver', 'dateFomart', 'underscore-min'], function() {
    return ['$rootScope', '$http', '$scope', '$q', '$location', '$sce', 'ngDialog', '$state', '$timeout', 'constantService', 'utilService', 'cartService',function($rootScope, $http, $scope, $q, $location, $sce, ngDialog, $state, $timeout, constantService, utilService, cartService) {

            $scope.conuntList = [];
            var user = JSON.parse(sessionStorage.getItem('currentUser'));
            var GradeId = $scope.GradeId = user.GradeId;
            var ids = '';
            var questionIdList = [];
            $scope.selectedArray = []; //待删除问题
            var questionCache = [];
            /*分页*/
            $scope.paginationConf = {
                currentPage: 1,
                itemsPerPage: 10,
                totalItems: 0
            };
            //试卷保存位置 默认为3 个人卷库 2校本卷库
            $scope.qbank = 3;

            $scope.$watch('paginationConf.currentPage + paginationConf.itemsPerPage', getQuestionDetail);

            $http.post($rootScope.baseUrl + '/Interface0290.ashx').then(function(res){
                if(_.isArray(res.data.course)) {
                    $scope.gradeList = res.data.course;
                    $scope.currentGrade = _.find($scope.gradeList, function(item){
                        return item.gradeId === GradeId;
                    }) || $scope.gradeList[0];
                }else {
                    $scope.gradeList = [];
                }
            });

            function getLists() {
                cartService.getCart().then(function(res){
                    questionIdList = res;
                    if (questionIdList && questionIdList.length > 0) {
                        questionIdList = _.uniq(questionIdList, function(question) {
                            return question.questionFId.toUpperCase();
                        });

                        $scope.totalNumber = questionIdList.length;
                        $scope.paginationConf.totalItems = $scope.totalNumber;
                        setCountList();
                        var ids =  _.pluck(questionIdList, 'questionFId');
                        var idSplitor = [];
                        var deferArray = [];
                        for(var i = 0; i < ids.length; i++) {
                            idSplitor.push(ids[i]);
                            if((i > 0 && (i+1) % 100 === 0) || (i === ids.length - 1)) {
                                deferArray.push($http.post($rootScope.baseUrl + '/Interface0208A.ashx', {
                                    qFlnkID: idSplitor.join(',')
                                }));
                                idSplitor = [];
                            }
                        }
                        $q.all(deferArray).then(function(res){
                            var result = [];
                            _.each(res, function(item){
                                if(_.isArray(item.data.msg)) {
                                    result = result.concat(item.data.msg);
                                }
                            });
                            questionCache = _.map(result, function(item, index) {
                                var qtype = _.find(questionIdList, function(id) {
                                    return id.questionFId.toUpperCase() === item.QFLnkID.toUpperCase();
                                }) || {};
                                var sub = _.map(item.sub, function(s, index){
                                    return {
                                        FLnkID: s.QFLnkID,
                                        title: s.title,
                                        OptionOne: s.OptionA,
                                        OptionTwo: s.OptionB,
                                        OptionThree: s.OptionC,
                                        OptionFour: s.OptionD,
                                        DifficultLevel: s.DifficultLevel,
                                        knowledges: s.knowledges,
                                        Answer: s.Answer,
                                        QTypeName: qtype.qtypeName,
                                        QTypeId: qtype.qtypeId,
                                        source: s.source,
                                        UseNum: s.UseNum,
                                        SucRate: s.SucRate,
                                        Analysis: s.Analysis,
                                        IsCollection: s.IsCollection ? item.IsCollection : 0,
                                        ShowType: _.find(utilService.showTypeList, function (type) {
                                            return type.id === s.ShowType
                                        }),
                                        orders: index + 1,
                                        isObjective:s.isObjective
                                    }
                                });
                                return {
                                    FLnkID: item.QFLnkID,
                                    title: item.title,
                                    OptionOne: item.OptionA,
                                    OptionTwo: item.OptionB,
                                    OptionThree: item.OptionC,
                                    OptionFour: item.OptionD,
                                    DifficultLevel: item.DifficultLevel,
                                    knowledges: item.knowledges,
                                    Answer: item.Answer,
                                    QTypeName: qtype.qtypeName,
                                    QTypeId: qtype.qtypeId,
                                    source: item.source,
                                    UseNum: item.UseNum,
                                    SucRate: item.SucRate,
                                    Analysis: item.Analysis,
                                    IsCollection: item.IsCollection ? item.IsCollection : 0,
                                    ShowType: _.find(utilService.showTypeList, function (type) {
                                        return type.id === item.ShowType
                                    }),
                                    mode: item.mode || (item.sub && item.sub.length > 0 ? 'B' : '') ,
                                    sub: sub,
                                    isObjective:item.isObjective
                                };
                            });
                            getQuestionDetail();
                        });

                        // $http.post($rootScope.baseUrl + '/Interface0208A.ashx', {
                        //     qFlnkID: ids.join(',')
                        // }).then(function(res){
                        //     if(_.isArray(res.data.msg)) {
                        //         questionCache = _.map(res.data.msg, function(item, index) {
                        //             var qtype = _.find(questionIdList, function(id) {
                        //                     return id.questionFId.toUpperCase() === item.QFLnkID.toUpperCase();
                        //                 }) || {};
                        //                 var sub = _.map(item.sub, function(s, index){
                        //                     return {
                        //                         FLnkID: s.QFLnkID,
                        //                         title: s.title,
                        //                         OptionOne: s.OptionA,
                        //                         OptionTwo: s.OptionB,
                        //                         OptionThree: s.OptionC,
                        //                         OptionFour: s.OptionD,
                        //                         DifficultLevel: s.DifficultLevel,
                        //                         knowledges: s.knowledges,
                        //                         Answer: s.Answer,
                        //                         QTypeName: qtype.qtypeName,
                        //                         QTypeId: qtype.qtypeId,
                        //                         source: s.source,
                        //                         UseNum: s.UseNum,
                        //                         SucRate: s.SucRate,
                        //                         Analysis: s.Analysis,
                        //                         IsCollection: s.IsCollection ? item.IsCollection : 0,
                        //                         ShowType: _.find(utilService.showTypeList, function (type) {
                        //                             return type.id === s.ShowType
                        //                         }),
                        //                         orders: index + 1,
                        //                         isObjective:s.isObjective
                        //                     }
                        //                 });
                        //             return {
                        //                 FLnkID: item.QFLnkID,
                        //                 title: item.title,
                        //                 OptionOne: item.OptionA,
                        //                 OptionTwo: item.OptionB,
                        //                 OptionThree: item.OptionC,
                        //                 OptionFour: item.OptionD,
                        //                 DifficultLevel: item.DifficultLevel,
                        //                 knowledges: item.knowledges,
                        //                 Answer: item.Answer,
                        //                 QTypeName: qtype.qtypeName,
                        //                 QTypeId: qtype.qtypeId,
                        //                 source: item.source,
                        //                 UseNum: item.UseNum,
                        //                 SucRate: item.SucRate,
                        //                 Analysis: item.Analysis,
                        //                 IsCollection: item.IsCollection ? item.IsCollection : 0,
                        //                 ShowType: _.find(utilService.showTypeList, function (type) {
                        //                     return type.id === item.ShowType
                        //                 }),
                        //                 mode: item.mode || (item.sub && item.sub.length > 0 ? 'B' : '') ,
                        //                 sub: sub,
                        //                 isObjective:item.isObjective
                        //             };
                        //         });
                        //     }else {
                        //         questionCache = [];
                        //     }
                        //     getQuestionDetail();
                        // });
                    }else {
                        questionIdList = [];
                        $scope.tkList = [];
                    }
                }, function(){
                    questionIdList = [];
                    $scope.tkList = [];
                    constantService.alert('获取试题篮题目列表失败，请重试！');
                });
            }
            getLists();

            function setCountList() {
                $scope.conuntList = [];
                var typeList = _.pluck(questionIdList, 'qtypeName');
                typeList = _.uniq(typeList);
                for (var i = 0; i < typeList.length; i++) {
                    $scope.conuntList.push({ 'type': typeList[i] });
                }

                for (var i = 0; i < questionIdList.length; i++) {
                    for (var j = 0; j < $scope.conuntList.length; j++) {
                        if ($scope.conuntList[j].type == questionIdList[i].qtypeName) {
                            if ($scope.conuntList[j].count) {
                                $scope.conuntList[j].count++;
                            } else {
                                $scope.conuntList[j].count = 1;
                            }
                        }
                    }
                }
            }

            function getQuestionDetail() {
                ids = '';
                var startIndex = ($scope.paginationConf.currentPage - 1) * $scope.paginationConf.itemsPerPage;
                var endIndex = $scope.paginationConf.currentPage * $scope.paginationConf.itemsPerPage;
                if (questionIdList.length < endIndex) {
                    endIndex = questionIdList.length;
                }
                $scope.tkList = questionCache.slice(startIndex, endIndex);
            }

            $scope.deleteFromBasketComifrm = function(index) {
                constantService.showConfirm('确定将该题目移出试题篮', ['确定'], function() {
                    $scope.deleteFromBasket(index);
                });
            };

            $scope.deleteFromBasket = function(index) {
                cartService.removeCart($scope.tkList[index].FLnkID).then(function(res){
                    if(res.data.code === 0) {
                        for (var i = questionIdList.length - 1; i >= 0; i--) {
                            if (questionIdList[i].questionFId.toUpperCase() === $scope.tkList[index].FLnkID.toUpperCase()) {
                                questionIdList.splice(i, 1);
                                questionCache.splice(i,1);
                                break;
                            }
                        }
                        $scope.tkList.splice(index, 1);
                        $scope.listss = _.difference($scope.selectedArray,$scope.tkList);
                        if ($scope.listss.length!==0){
                            $scope.selectedArray = _.difference($scope.selectedArray,$scope.listss);
                        }
                        $scope.paginationConf.totalItems = questionIdList.length;
                        $scope.totalNumber = questionIdList.length;
                        setCountList();
                        $scope.$emit('baseketList_number_change');
                        //getQuestionDetail();
                    }
                });
            };

            $scope.viewAnalysis = function(values) {
                $scope.values = $sce.trustAsHtml(values);
                ngDialog.open({
                    template: 'template/viewAnalysis.html',
                    className: 'ngdialog-theme-default',
                    scope: $scope

                })
            };
            $scope.questionSelected = function(element, qst) {
                qst.isContentChecked = !qst.isContentChecked;
                if(!qst.isContentChecked) {
                    var index = _.findIndex($scope.selectedArray, function (qstitem) {
                        return qstitem.FLnkID === qst.FLnkID;
                    });
                    $scope.selectedArray.splice(index,1);
                }else{
                    $scope.selectedArray.push(qst);

                }
                // if (element.currentTarget.checked) {
                //     $scope.selectedArray.push(qst); //待删除问题
                // } else {
                //     $scope.selectedArray = _.without($scope.selectedArray, qst);
                // }
            };
            $scope.deleteSomeFromBasket = function() {
                if($scope.selectedArray.length == 0){
                    return;
                }
                constantService.showConfirm('确定将这些题目移出试题篮？', ['确定'], function() {
                    deleteFromCart(_.pluck($scope.selectedArray, 'FLnkID'));
                });
            };

            function clearCart(){
                deleteFromCart(_.pluck(questionCache, 'FLnkID'));
            }

            function deleteFromCart(qIds){
                cartService.removeCart(qIds.join(',')).then(function(res){
                    if(res.data.code === 0) {
                        /*for (var i = 0; i < qIds.length; i++) {
                            var indexInIdList = _.findIndex(questionIdList, function(item){
                                return item.questionFId.toUpperCase() === qIds[i].toUpperCase();
                            });
                            var indexInTkList = _.findIndex($scope.tkList, function(item){
                                return item.FLnkID.toUpperCase() === qIds[i].toUpperCase();
                            });

                            indexInIdList >=0 && questionIdList.splice(indexInIdList, 1);
                            indexInIdList >=0 && questionCache.splice(indexInIdList, 1);
                            indexInTkList >= 0 && $scope.tkList.splice(indexInTkList, 1);
                        }*/
                        getLists();
                        $scope.paginationConf.totalItems = questionIdList.length;
                        $scope.totalNumber = questionIdList.length;
                        setCountList();
                        $scope.selectedArray = [];
                        $scope.$emit('baseketList_number_change');
                        getQuestionDetail();
                    }
                });
            }

            $scope.startComposing = function(paperName, paperType, paperDiff, gradeId) {
                if(!paperName || paperName.trim() === '') {
                    constantService.alert('请输入试卷名称！');
                    return;
                }
                var grouppedQst = _.groupBy(questionCache, function(item) {
                    return item.QTypeId;
                });
                var allQsts = _.values(grouppedQst);

                if(paperType.ExType === '课堂评测'){
                    if(checkIsObjective(allQsts)){
                        constantService.showSimpleToast('课堂评测中不能加入非选择题!');
                        return;
                    }
                } else if(paperType.ExType === '机房评测' || paperType.ExType === '作业') {
                    if(checkIsObjectiveBLank(allQsts)){
                        constantService.showSimpleToast(paperType.ExType + '中不能加入非填空题的非客观题!');
                        return;
                    }
                }
                //设置order
                var allQuestions = [];
                _.each(allQsts, function (item, index) {
                    _.each(item, function (q) {
                        if (q.mode === 'A') {
                            allQuestions = allQuestions.concat(q.sub);
                            q.orders = '';
                        } else if (q.mode === 'B') {
                            allQuestions.push(q);
                            _.each(q.sub, function (s, index) {
                                s.orders = index + 1;
                            });
                        } else {
                            allQuestions.push(q);
                        }
                    });
                });
                _.each(allQuestions, function (item, index) {
                    item.orders = index + 1;
                });
                // var toSubmitQst = [];
                // _.each(allQsts, function(qArr, i) {
                //     _.each(qArr, function(question) {
                //         if (question.mode === 'A') {
                //             //是父子题A模式
                //             var parentOrder = question.sub[0] && question.sub[0].orders;
                //             var subList = _.map(question.sub, function (item, index) {
                //                 return {
                //                     qFLnkID: item.QFLnkID || item.fLnkId || item.FLnkID || '',
                //                     qtypeName: question.QTypeName,
                //                     Dtype: question.QTypeName,
                //                     qScores: item.score || 0,
                //                     Order: item.orders + '',
                //                     GAP: '0',
                //                     ShowType: item.ShowType && item.ShowType.id || '0',
                //                     mode: 'A',
                //                     sOrder: index + 1,
                //                     parentOrder: parentOrder,
                //                     isSub: 1
                //                 }
                //             });
                //             toSubmitQst.push({
                //                 qFLnkID: question.QFLnkID || question.fLnkId || question.FLnkID || '',
                //                 qtypeName: question.QTypeName,
                //                 Dtype: question.QTypeName,
                //                 qScores: question.score || 0,
                //                 Order: parentOrder + '',
                //                 GAP: '0',
                //                 ShowType: question.ShowType && question.ShowType.id || '0',
                //                 mode: 'A',
                //                 sOrder: 0,
                //                 parentOrder: 0,
                //                 explain: '',
                //                 isSub: 1
                //             });
                //             toSubmitQst = toSubmitQst.concat(subList);
                //         } else if (question.mode === 'B') {
                //             //是父子题B模式
                //             var parentOrderB = question.orders;
                //             var subListB = _.map(question.sub, function (item, index) {
                //                 return {
                //                     qFLnkID: item.QFLnkID || item.fLnkId || item.FLnkID ||'',
                //                     qtypeName: question.QTypeName,
                //                     Dtype: question.QTypeName,
                //                     qScores: item.score || 0,
                //                     Order: parentOrderB + '',
                //                     GAP: '0',
                //                     ShowType: item.ShowType && item.ShowType.id || '0',
                //                     mode: 'B',
                //                     sOrder: index + 1,
                //                     parentOrder: parentOrderB,
                //                     isSub: 1
                //                 }
                //             });
                //             toSubmitQst.push({
                //                 qFLnkID: question.QFLnkID || question.fLnkId || question.FLnkID || '',
                //                 qtypeName: question.QTypeName,
                //                 Dtype: question.QTypeName,
                //                 qScores: question.score || 0,
                //                 Order: parentOrderB + '',
                //                 GAP: '0',
                //                 ShowType: question.ShowType && question.ShowType.id || '0',
                //                 mode: 'B',
                //                 sOrder: 0,
                //                 parentOrder: 0,
                //                 explain: '',
                //                 isSub: 1
                //             });
                //             toSubmitQst = toSubmitQst.concat(subListB);
                //         } else {
                //             //是普通题型
                //             toSubmitQst.push({
                //                 qFLnkID: question.QFLnkID || question.fLnkId || question.FLnkID || '',
                //                 qtypeName: question.QTypeName,
                //                 Dtype: question.QTypeName,
                //                 qScores: question.score || 0,
                //                 Order: question.orders + '',
                //                 GAP: '0',
                //                 ShowType: question.ShowType && question.ShowType.id || '0',
                //                 sOrder: 0,
                //                 parentOrder: 0,
                //                 isSub: 0,
                //                 explain: '',
                //                 mode: ''
                //             });
                //         }
                //     });
                // });
                //组卷
                var list = [];
                _.each(allQsts, function (item) {
                    if(item.length) {
                        list.push({
                            QNumber: item.length,
                            QTypeName: item[0].QTypeName,
                            question: item
                        })
                    }
                });
                ngDialog.closeAll();
                constantService.alert('到试卷编辑页面设置试卷分值，设置完成记得保存试卷！',function() {
                    localStorage.setItem('answerList', JSON.stringify(list));
                    location.hash = '#/paperComposing?bankPosition=' + +$scope.qbank + '&examType=' + paperType.ExType
                        + '&gradeid=' + gradeId + '&examTitle=' + encodeURIComponent(paperName) + '&type=' + 1;
                });

                // orgaPaper(paperDiff,paperType,paperName,gradeId,toSubmitQst,$scope.qbank).then(function (res) {
                //     ngDialog.closeAll();
                //     constantService.confirm('提示','恭喜您，组卷成功！是否需要清空试题篮？', ['清空试题篮'], function(){
                //         clearCart();
                //         $scope.$emit('baseketList_number_change');
                //         $state.go('myApp.paperComposing', {
                //             examId: res.data.msg,
                //             action: 3
                //         });
                //     }, function(){
                //         $state.go('myApp.paperComposing', {
                //             examId: res.data.msg,
                //             action: 3
                //         });
                //     });
                    // clearCart();
                    // constantService.alert('恭喜您，组卷成功！');
                    // $scope.$emit('baseketList_number_change');
                    // $state.go('myApp.paperComposing', {
                    //     examId: res.data.msg,
                    //     action: 3
                    // });
                // },function (res) {
                //     ngDialog.closeAll();
                //     constantService.alert('组卷失败！');
                // });
            };
            //组卷方法
            // function orgaPaper(paperDiff,paperType,paperName,gradeId,toSubmitQst,qbank) {
            //     var defer = $q.defer();
            //     $http.post($rootScope.baseUrl + '/Interface0219.ashx', {
            //         diff: paperDiff + '',
            //         exType: paperType.ExType,
            //         examFLnkId: 0,
            //         examName: paperName,
            //         gradeId: gradeId,
            //         scores: 0,
            //         smallTitle: "",
            //         time: 0,
            //         isTeacher: '1',
            //         msg: toSubmitQst,
            //         fLnkID : (+qbank === 3 ? (JSON.parse(sessionStorage.getItem('currentUser')).fid):(+qbank === 2 ? JSON.parse(sessionStorage.getItem('currentUser')).schoolFId:''))
            //     }).then(function (res) {
            //         defer.resolve(res);
            //     },function (res) {
            //         defer.reject(res);
            //     });
            //     return defer.promise;
            // }

            $scope.startPaper = function() {
                //localStorage.removeItem('baseketList');
                //弹出dialog，提示用户输入基础信息
                var diffs = _.pluck($scope.tkList, 'DifficultLevel');
                var total = 0;
                _.each(diffs, function(item){
                    total += (+item || 0);
                });
                $scope.paperDiff = Math.round(total / diffs.length);
                ngDialog.open({
                    template: '/questionCartBegin.html',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'ngdialog-theme-quebasket',
                    scope: $scope,
                    controller: function($scope){
                        return function(){
                            $scope.choseNianji = function(grade){
                                $scope.currentGrade = grade;
                            };
                        };
                    }
                });
            };

            //初始化试卷类型
            $http.get($rootScope.baseUrl + '/Interface0196.ashx').then(function(res) {
                if (_.isArray(res.data.msg)) {
                    $scope.typeList = res.data.msg;
                    var index = _.findIndex($scope.typeList,function (value) {
                        return value.ExType == '周测';
                    });
                    var str = $scope.typeList.splice(index,1);
                    $scope.typeList.unshift(str[0]);
                    $scope.paperType = $scope.typeList[0];
                }
            });

        	//查看解析
            $scope.viewAnalysis = function (tk) {
            	$scope.values = tk.Analysis;
                $scope.qAnswer = tk.Answer;
            	ngDialog.open({
            		template: 'template/viewAnalysis.html',
            		className: 'ngdialog-theme-plain',
            		scope: $scope

            	})
            };

            // 增加收藏
            $scope.addAnalysis = function(tk, type, $event) { // type 1  试卷  2 题目
                var tag = $event.target;
                $http.post($rootScope.baseUrl + '/Interface0204.ashx', {
                    resourceFlnkID: tk.FLnkID,
                    type: type
                }).success(function(data) {
                    tk.IsCollection = 1;
                    constantService.alert('收藏成功！');
                });
            };

        	/*题目纠错 2016/6/7 新增功能*/
            $scope.goCheck = function (tk, index) {
            	$scope.tk = {
            		fLnkId: '',
            		orders: index + 1,
            		title: tk.title,
            		ShowType: { id: tk.ShowType },
            		OptionOne: tk.OptionOne,
            		OptionTwo: tk.OptionTwo,
            		OptionThree: tk.OptionThree,
            		OptionFour: tk.OptionFour,
            	};

            	ngDialog.open({
            		template: 'template/checkQuestion.html',
            		className: 'ngdialog-theme-default',
            		appendClassName: 'ngdialog-theme-zxz',
            		scope: $scope,
            		preCloseCallback: function () {

            		}
            	});

            	$scope.goclose = function () {//关闭
            		ngDialog.close();
            	};

            	$scope.goconfirm = function () {//提交题目，错误原因
            		var errText = $("#myText").val();
            		if (!errText) {
            			$scope.errMsg = "请填写错误原因！";
            			$timeout(function () { $scope.errMsg = ""; }, 1000);
            		} else {
            			$http({//提交接口
            				method: 'post',
            				url: $rootScope.baseUrl + '/Interface0253.ashx',
            				data: {
            					qFLnkId: tk.FLnkID,
            					comment: errText
            				}
            			}).success(function (data) {
            				if (!!data && data.code === 0) {
            					$scope.sucMsg = "成功纠错！";
            					$timeout(function () {
            						$scope.sucMsg = "";
            						ngDialog.close();
            					}, 1000);

            				} else {
            					$scope.sucMsg = "稍后纠错吧，服务繁忙！";
            					$timeout(function () { $scope.sucMsg = ""; }, 1000);
            				}
            			})
            		}
            	};
            };
            function checkIsObjective(qList) {
                var isObjective = false;
                _.each(qList,function (q) {
                    _.each(q,function (item) {
                        if(item.sub && item.sub.length>0){
                            _.each(item.sub,function (i) {
                                if(i.isObjective === '0'){
                                    isObjective = true;
                                    return false;
                                }
                            });
                        }else{
                            if(item.isObjective === '0'){
                                isObjective = true;
                                return false;
                            }
                        }
                    });
                });
                return isObjective;
            }
        function checkIsObjectiveBLank(qList) {
            var isObjective = false;
            _.each(qList,function (q) {
                _.each(q,function (item) {
                    if(item.sub && item.sub.length>0){
                        _.each(item.sub,function (i) {
                            if(i.isObjective === '0'){
                                isObjective = true;
                                return false;
                            }
                        });
                    }else{
                        if(item.isObjective === '0' && item.QTypeName !== '填空题'){
                            isObjective = true;
                            return false;
                        }
                    }
                });
            });
            return isObjective;
        }
        }];
});
