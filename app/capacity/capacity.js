define(['jquery.dataTables', 'highcharts', 'echarts'], function (dataTables, highcharts, echarts) {
    return ['$http', '$scope', '$rootScope', '$state', '$location', 'ngDialog', function ($http, $scope, $rootScope, $state, $location, ngDialog) {

        var Sfid = $location.search().Sfid;
        var ClassFlnkid = $location.search().ClassFlnkid; // 班级ID
        $scope.ClassFlnkid = ClassFlnkid;
        $scope.ClassName = $location.search().className;
        var studentFlnkIde = [];
        $http({
            method: 'post',
            url: $rootScope.baseUrl + '/Interface0164.ashx',
            data: {
                ClassFlnkid: ClassFlnkid
            }
        }).then(function (res) {
            if (_.isArray(res.data.msg)) {
                $scope.studentList = res.data.msg;
                studentFlnkIde = _.pluck(res.data.msg, 'UserFLnkID');
                _.each($scope.studentList,function (item) {
                    if(item.UserFLnkID === Sfid){
                        $scope.MyName = item.Name;
                    }
                });
            } else {
                studentFlnkIde = [];
            }
        });
        //studentFlnkIde = studentFlnkIde.split(","); // 字符 转数组
        var getArrSet = function () { //  返回当前学生的位置
            var getArrSet = studentFlnkIde.indexOf(Sfid, 0);
            return getArrSet;
        }
        console.log(getArrSet())
        $scope.next = function () {
            if (getArrSet() == studentFlnkIde.length - 1) {
                $('.next').addClass('f_gray');
                return false;
            } else {
                var i = getArrSet() + 1;
                Sfid = studentFlnkIde[i];
                $scope.MyName = $scope.studentList[i].Name;
                $scope.UserId = Sfid;
                init(Sfid);
               // window.location.href = "#/capacity?Sfid=" + studentFlnkIde[i] + "&ClassFlnkid=" + ClassFlnkid;
               // window.location.reload();
            }
        };
        if (getArrSet() == 0) {
            $('.prev').addClass('f_gray');
            //return false;
        }
        if (getArrSet() == studentFlnkIde.length - 1) {
            $('.next').addClass('f_gray');
        }
        $scope.prev = function () {
            if (getArrSet() == 0) {
                $('.prev').addClass('f_gray');
                return false;
            } else {
                var i = getArrSet() - 1;
                Sfid = studentFlnkIde[i];
                $scope.UserId = Sfid;
                $scope.MyName = $scope.studentList[i].Name;
                init(Sfid);
               // window.location.href = "#/capacity?Sfid=" + studentFlnkIde[i] + "&ClassFlnkid=" + ClassFlnkid;
               // window.location.reload();
            }
        };

        $scope.UserId = Sfid;
        $scope.noklgdata = false;
        // 页面初始数据
        var init = function (Sfid) {
            // 用户战斗力变化
            // $http({
            // 	method: 'post',
            // 	url: $rootScope.baseUrl + '/Interface0166.ashx',
            // 	data: {
            // 		Limit: 10,
            // 		UserFlnkID: Sfid
            // 	}
            // }).success(function (data) {
            // 	if (data.msg != '') {
            // 		$scope.Combat = data.msg;
            // 		$scope.ClassName = data.msg[0].ClassName;
            // 		$scope.MyName = data.msg[0].MyName;
            // 		sessionStorage.setItem('MyName', $scope.MyName);
            // 		// 班级战斗力
            // 		var ClassPow = [], MyPow = [], Day = [];
            // 		for (var i = 0 ; i < data.msg.length ; i++) {
            // 			if (Number(data.msg[i].ClassPow) <= 0) {
            // 				if (Number(data.msg[i - 1]) && Number(data.msg[i - 1].ClassPow) <= 0) {
            // 					if (Number(data.msg[i - 2]) && Number(data.msg[i - 2].ClassPow) <= 0) {
            // 						if (Number(data.msg[i - 3]) && Number(data.msg[i - 3].ClassPow) <= 0) {
            // 							if (Number(data.msg[i - 4]) && Number(data.msg[i - 4].ClassPow) <= 0) {
            // 								ClassPow.push(Number(data.msg[i - 5].ClassPow));
            // 							} else {
            // 								ClassPow.push(Number(data.msg[i - 4].ClassPow));
            // 							}
            // 						} else {
            // 							ClassPow.push(Number(data.msg[i - 3].ClassPow));
            // 						}
            // 					} else {
            // 						ClassPow.push(Number(data.msg[i - 2].ClassPow));
            // 					}
            // 				} else {
            // 					ClassPow.push(Number(data.msg[i - 1].ClassPow));
            // 				}
            // 			} else {
            // 				ClassPow.push(Number(data.msg[i].ClassPow))
            // 			}
            // 		}
            // 		// 自身战斗力
            // 		for (var i = 0 ; i < data.msg.length ; i++) {
            // 			MyPow.push(Number(data.msg[i].MyPow))
            // 		}
            // 		// 日期
            // 		for (var i = 0 ; i < data.msg.length ; i++) {
            // 			Day.push(data.msg[i].Day)
            // 		}
            // 		highchartsline(ClassPow, MyPow, Day)
            //
            // 	} else {
            // 		ngDialog.open({
            // 			template:
            // 					'<p>暂无数据</p>' +
            // 					'<div class="ngdialog-buttons">' +
            // 					'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
            // 			plain: true,
            // 			className: 'ngdialog-theme-plain'
            // 		});
            // 		return false;
            // 	}
            // })
            $http({
                method: 'post',
                url: $rootScope.baseUrl + '/Interface0166A.ashx',
                data: {
                    //查看所有限制100条
                    Limit: 10,
                    UserFlnkID: Sfid
                }
            }).success(function (data) {
                $scope.Combat = data.msg;
                if(_.isArray($scope.Combat)&&$scope.Combat.length !== 0){
                    $scope.datapie = true;
                    // 班级战斗力
                    var ClassPow = [], MyPow = [], Day = [];
                    for (var i = 0; i < data.msg.length; i++) {
                        ClassPow.push(Number(data.msg[i].ClassPow))
                    }
                    // 自身战斗力
                    for (var i = 0; i < data.msg.length; i++) {
                        MyPow.push(Number(data.msg[i].MyPow))
                    }
                    // 日期
                    for (var i = 0; i < data.msg.length; i++) {
                        Day.push(data.msg[i].Day)
                    }
                    // 绘制图表
                    echarts.init(document.getElementById('containers')).setOption({
                        tooltip: {
                            trigger: "item",
                            formatter: "{b} <br/>{a} : {c}"
                        },
                        legend: {
                            orient: 'vertical',
                            padding: [0,0,0,900],
                            x: 'left',
                            data: ["个人成绩变化", "班级平均成绩"]
                        },
                        xAxis: [
                            {
                                type: "category",
                                name: "x",
                                splitLine: {show: false},
                                data: Day
                            }
                        ],
                        yAxis: [
                            {
                                type: "value",
                                name: "y"
                            }
                        ],
                        calculable: true,
                        series: [
                            {
                                name: "班级平均成绩",
                                type: "line",
                                data: ClassPow,
                                itemStyle: {
                                    normal: {
                                        color: '#2dc0e8',
                                        lineStyle: {
                                            color: '#2dc0e8'
                                        }
                                    }
                                }

                            },
                            {
                                name: "个人成绩变化",
                                type: "line",
                                data: MyPow,
                                itemStyle: {
                                    normal: {
                                        color: '#ff4c79',
                                        lineStyle: {
                                            color: '#ff4c79'
                                        }
                                    }
                                }

                            }
                        ]
                    });
                }else {
                    $scope.datapie = false;
                }

            })
                .then(function () {
                    //用户战斗力信息
                    $http({
                        method: 'post',
                        url: $rootScope.baseUrl + '/Interface0165.ashx',
                        data: {
                            UserFlnkID: Sfid
                        }
                    }).success(function (data) {
                        $scope.UserCombat = data.msg[0];
                    })
                }).then(function () {
                //历史成绩数据接口调用
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0167.ashx',
                    data: {
                        Limit: 5,
                        UserFlnkID: Sfid
                    }
                }).success(function (data) {
                    var historytable = data.msg;
                    $scope.historyScoretable.fnAddData(historytable);
                });
                //历史成绩数据table绘制
                $scope.historyScoretable = $('#historyScore').dataTable({
                    "aoColumns": [
                        {"mDataProp": "Order", "sWidth": "8%"},
                        {"mDataProp": "ExamName", "sWidth": "18.5%"},
                        {"mDataProp": "ExamTime", "sWidth": "18.5%"},
                        {"mDataProp": "AllSc", "sWidth": "10%"},
                        {"mDataProp": "Sc", "sWidth": "10%"},
                        {"mDataProp": "ClassAvgSc", "sWidth": "15%"},
                        {"mDataProp": "Rank", "sWidth": "10%"},
                        {"mDataProp": "ExamFlnkID", "sWidth": "10%"}
                    ],
                    "aoColumnDefs": [
                        {
                            "aTargets": [7], "mRender": function (data, type, full) {
                            return '<a class="homeviewbtnana" href="#/Perforsis?ExamFlnkID=' + data + '&ClassFlnkid=' + ClassFlnkid + '&ExAnswerFlnkID=' + full.ExAnswerFlnkID + '">查看详细</a>';
                        }
                        },
                        {
                            "aTargets": [4], "mRender": function (data, type, full) {
                            return '<div class="orange">' + data + '</div>';
                        }
                        },
                        {
                            "aTargets": [1], "mRender": function (data, type, full) {
                            return '<div data-toggle="tooltip" data-placement="right"  ng-mouseover="moveOver()" class="t_overflow" title="' + data + '">' + data + '</div>';
                        }
                        }
                    ],
                    //"fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    //	$("td:first", nRow).html(iDisplayIndex + 1);//设置序号位于第一列，并顺次加一
                    //	return nRow;
                    //},
                    "oLanguage": {
                        "sProcessing": "处理中...",
                        "sLengthMenu": "显示 _MENU_ 项结果",
                        "sZeroRecords": "没有匹配结果",
                        "sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
                        "sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
                        "sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
                        "sInfoPostFix": "",
                        "sSearch": "搜索：",
                        "sUrl": "",
                        "sEmptyTable": "表中数据为空",
                        "sLoadingRecords": "载入中...",
                        "sInfoThousands": ",",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": "上页",
                            "sNext": "下页",
                            "sLast": "末页"
                        }
                    },
                    "bDestroy": true,//允许重新加载表格对象数据
                    "bRetrieve": true,
                    "bProcessing": false,
                    "bLengthChange": false,
                    "bPaginate": false,
                    "bAutoWidth": true,
                    "bFilter": false,//禁用搜索
                    "bLengthChange": false,//禁用分页显示
                });
                $('#historyScore').dataTable().fnClearTable();
            }).then(function () {
                // 知识点分析接口调用
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0168.ashx',
                    data: {
                        UserFlnkID: Sfid
                    }
                }).success(function (data) {
                    if (data.msg != '') {
                        var KnowledgeData = data.msg;
                        $scope.klgAnalysistable.fnAddData(KnowledgeData);
                    } else {
                        $scope.noklgdata = true;
                    }
                });
                //知识点分析table绘制
                $scope.klgAnalysistable = $('#klgAnalysistable').dataTable({
                    "aoColumns": [
                        {"mDataProp": "Order", "sWidth": "8%"},
                        {"mDataProp": "KnowledgeName", "sWidth": "20%"},
                        {"mDataProp": "LostNum", "sWidth": "15%"},
                        {"mDataProp": "RightRate", "sWidth": "15%"},
                        {"mDataProp": "ClassRightRate", "sWidth": "15%"},
                        {"mDataProp": "CompensateNum", "sWidth": "10%"}
                    ],
                    "aoColumnDefs": [
                        {
                            "aTargets": [4], "mRender": function (data, type, full) {
                            return data + '%';
                        }
                        },
                        {
                            "aTargets": [3], "mRender": function (data, type, full) {
                            return data + '%';
                        }
                        },
                        {
                            "aTargets": [1], "mRender": function (data, type, full) {
                            return '<div class="t_overflow" title="' + data + '">' + data + '</div>';
                        }
                        }
                    ],
                    //"fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    //	$("td:first", nRow).html(iDisplayIndex + 1);//设置序号位于第一列，并顺次加一
                    //	return nRow;
                    //},
                    "oLanguage": {
                        "sProcessing": "处理中...",
                        "sLengthMenu": "显示 _MENU_ 项结果",
                        "sZeroRecords": "没有匹配结果",
                        "sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
                        "sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
                        "sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
                        "sInfoPostFix": "",
                        "sSearch": "搜索：",
                        "sUrl": "",
                        "sEmptyTable": "表中数据为空",
                        "sLoadingRecords": "载入中...",
                        "sInfoThousands": ",",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": "上页",
                            "sNext": "下页",
                            "sLast": "末页"
                        }
                    },
                    "bDestroy": true,//允许重新加载表格对象数据
                    "bRetrieve": true,
                    "bProcessing": false,
                    "bLengthChange": false,
                    "bPaginate": false,
                    "bAutoWidth": true,
                    "bFilter": false,//禁用搜索
                    "bLengthChange": false,//禁用分页显示
                });
                $('#klgAnalysistable').dataTable().fnClearTable();
            })
        }
        init(Sfid);

        // 战斗力提升
        // var highchartsline = function (ClassPow, MyPow, Day) {
        // 	$('#containers').highcharts({
        // 		chart: {
        // 			type: 'line'
        // 		},
        // 		title: {
        // 			text: false
        // 		},
        // 		xAxis: {
        // 			categories: Day
        // 		},
        // 		credits: {
        // 			enabled: false,
        // 		},
        // 		yAxis: {
        // 			title: false
        // 		},
        // 		legend: {
        // 			layout: 'vertical',
        // 			align: 'right',
        // 			verticalAlign: 'top',
        // 			floating: true
        // 		},
        //
        // 		plotOptions: {
        // 			line: {
        // 				dataLabels: {
        // 					enabled: true
        // 				}
        // 			}
        // 		},
        // 		exporting: {
        // 			enabled: false
        // 		},
        // 		series: [{
        // 			name: '个人战斗力变化',
        // 			color: '#ff4c79',
        // 			data: MyPow
        // 		}, {
        // 			name: '班级平均战斗力',
        // 			color: '#2dc0e8',
        // 			data: ClassPow
        // 		}]
        // 	});
        // }
        //查看全部-历史成绩汇总
        $scope.gostuAllHis = function (UserId, ClassFlnkid) {
            console.log(UserId + ClassFlnkid);
            window.open("#/stuAllHis?Sfid=" + UserId + "&ClassFlnkid=" + ClassFlnkid + "&isOpenNewWin=1")
        }

        // tips
        $scope.moveOver = function () {
            $("[data-toggle='tooltip']").tooltip({
                html: true
            });
        }
    }]
})


