/**
 * Created by 贺小雷 on 2016/9/13.
 */
define(['wyCarousel'], function () {
    return ['$scope', '$http', '$location', '$q', 'api', 'constantService', 'paperService', 'ngDialog',
        function ($scope, $http, $location, $q, api, constantService, paperService, ngDialog) {
            $('#encryptmarking').css('height', $(window).height());
            $(window).off('resize').on('resize', function (e) {
                $('#encryptmarking').css('height', $(window).height());
            });
            var windowHeight = window.innerHeight;
            var windowWidth = window.innerWidth;
            $scope.img1 = {"top": windowHeight - 300 + 'px', "left": "20px", "position": "absolute"};
            $scope.img2 = {
                "top": windowHeight - 150 + 'px',
                "left": "320px",
                "position": "absolute",
                "cursor": "pointer"
            };
            $scope.img3 = {"top": "100px", "left": "20px", "position": "absolute"};
            $scope.img4 = {"top": "300px", "left": "385px", "position": "absolute", "cursor": "pointer"};
            $scope.img5 = {"top": "80px", "right": '200px', "position": "absolute"};
            $scope.img6 = {"top": "200px", "right": "200px", "position": "absolute", "cursor": "pointer"};
            $scope.img7 = {"top": "120px", "right": '10px', "position": "absolute"};
            $scope.img8 = {"top": "500px", "right": "200px", "position": "absolute", "cursor": "pointer"};
            $scope.newP = true;
            $scope.toggle = function () {
                $scope.newP = !$scope.newP;
                $('#step1').show();
                $('#step2').hide();
                $('#step3').hide();
                $('#step4').hide();
                $('.next').on('click', function () {
                    var obj = $(this).parents('.step');
                    var step = obj.attr('step');
                    obj.hide();
                    $('#step' + (parseInt(step) + 1)).show();
                });
                $('.over').on('click', function () {
                    $(this).parents('.step').hide();
                    $scope.$apply(function () {
                        $scope.newP = true;
                    });
                })
            };
            var search = $location.search(),
                unifiedId = search.unifiedId,
                examId = search.examId;
            var imgLoadingDialog = '';
            var cacheCanMarkSub = {};
            var list4SunQ = [];
            $scope.isShowLeft = 1;
            var afterfilter = [];
            $scope.data = {
                currentBlock: 1,
                PULL_LIMIT: 50,
                preload_img_count: 10,
                stuPaperImg: '',
                groupList:[],
                allQuestionList:[],
                inputlist:[],
                defaultScoreType:'1'
            };
            //defaultScoreType 1：填空题默认全对 2：填空题默认全错 初始默认全对
            $scope.haveMore = true;
            $scope.isAutoLoading = false;
            $scope.isReloadOver = false;
            $scope.logoSrc = '/assets/skin/' + window.config.theme + '/img/loading-logo.png';
            $scope.copy = window.config.themeCompanyNameMap[window.config.theme];
            //获取考试切图
            api.getPaperCut(unifiedId).then(function (res) {
                $scope.data.cutData = res.data.position && JSON.parse(res.data.position);
                //获取老师所需批阅题号
                api.getMarkingOrder(unifiedId).then(function (res) {
                    if (res.data.code === 0) {
                        // _.each(res.data.msg, function(m){
                        //     m.order = '36'
                        // });
                        var afterfilter = filterByShowDis(res.data.msg);
                        var datalist = sortListByGroupname(afterfilter);
                        //beforefilter = sortListByGroupname(beforefilter);
                        $scope.data.groupList = datalist;
                        $scope.data.idOrderMap = datalist;
                        $scope.data.qFIds = getlistBykey(datalist,'qFId');
                        $scope.data.orderList = getlistBykey(datalist,'dis');
                        $scope.data.currentGroup = $scope.data.groupList[0];
                        //获取题目和加载学生
                        getQuestions($scope.data.orderList, examId, $scope.data.idOrderMap);
                        getStudents($scope.data.currentGroup.groupname,$scope.data.currentGroup.qstList[0].dis, false);
                        setTimeout(function () {
                            $('header .viewport').wyCarousel();
                        }, 0)
                    } else {
                        constantService.alert('获取题号失败，您暂时没有批阅权限！');
                    }
                }, function () {
                    constantService.alert('获取题号失败，您暂时无法批阅！');
                });
            });

            $scope.changeOrder = function (group) {
                if ($scope.data.groupList && $scope.data.groupList.length > 0 && $scope.data.currentGroup !== group) {
                    $scope.data.currentGroup = group;
                    $scope.data.currentBlock = 1;
                    $scope.data.currentStu = undefined;
                    getStudents($scope.data.currentGroup.groupname,$scope.data.currentGroup.qstList[0].dis, false).then(function () {
                        var qst = _.find($scope.data.allQuestionList, function (qst) {
                            return qst.groupname + '' === $scope.data.currentGroup.groupname+'';
                        });
                        $scope.haveMore = true;
                        $scope.isReloadOver = false;
                        $scope.data.currentQst = qst.qstlist;
                        for (var i = 0;i<$scope.data.currentQst.length; i++){
                            if ($scope.data.currentQst[i].showinput === 2){
                                $scope.data.currentQst.splice(i,1);
                            }
                        }
                        /*var tempscore = calToalScore(qst.qstlist);
                        $scope.data.currentGroup.totalScore = tempscore.total;
                        $scope.data.currentGroup.studentScore = tempscore.stu;*/
                    });
                }
            };

            $scope.startMarking = function () {
                if ($scope.data.stus && $scope.data.stus.length > 0) {
                    $scope.hideLeftList();
                    $scope.data.currentBlock = 2;
                    if ($scope.data.unMarkStus.length > 0) {
                        $scope.data.currentStu = $scope.data.unMarkStus[0];
                    } else {
                        $scope.data.currentStu = $scope.data.stus[0];
                    }
                }
            };

            $scope.showQstAndAnswer = function (type) {
                if (type === 1) {
                    $scope.data.currentBlock = 1;
                } else {
                    ngDialog.open({
                        template: 'questionViewer',
                        className: 'ngdialog-theme-default',
                        appendClassName: 'qst-answer-box',
                        closeByDocument: true,
                        plain: false,
                        scope: $scope
                    });
                }
            };

            $scope.markStudent = function (stu) {
                $scope.data.currentStu = stu;
                $scope.data.currentBlock = 2;
            };

            $scope.loadNext = function () {
                var haveUnMarkedStu = _.find($scope.data.stus, function (stu) {
                    return stu.isMarked === '0';
                });
                if (haveUnMarkedStu) {
                    constantService.alert('您还没有全部批阅完成，请全部批阅后再试！');
                    return;
                }
                var qFId = findFid($scope.data.idOrderMap,$scope.data.currentGroup.groupname,$scope.data.currentGroup.qstList[0].dis);
                api.getStusByOrder(qFId, $scope.data.currentGroup.qstList[0].dis.split('.')[0], unifiedId, $scope.data.PULL_LIMIT, true).then(function (res) {
                    if (res.data.code === 0) {
                        $scope.data.stus = res.data.msg;
                        $scope.data.unMarkStus = _.filter($scope.data.stus, function (item) {
                            return item.isMarked === '0';
                        });
                        if ($scope.data.unMarkStus.length > 0) {
                            preLoadImg();
                            $scope.data.currentStu = $scope.data.unMarkStus[0];
                            $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                            $scope.data.markedNum = getMarkedStudents().length;
                            $scope.data.averageScore = getMarkedStusAverageScore();
                        } else {
                            constantService.alert('所有学生已全部批阅完成！');
                            $scope.data.markedNum = getMarkedStudents().length;
                            $scope.data.averageScore = getMarkedStusAverageScore();
                        }
                    } else {
                        $scope.data.stus = [];
                        $scope.data.unMarkStus = [];
                        $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                        $scope.data.markedNum = getMarkedStudents().length;
                        $scope.data.averageScore = getMarkedStusAverageScore();
                        constantService.alert('当前没有待批阅的学生！');
                    }
                }, function () {
                    $scope.data.stus = [];
                    $scope.data.unMarkStus = [];
                    $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                    $scope.data.markedNum = getMarkedStudents().length;
                    $scope.data.averageScore = getMarkedStusAverageScore();
                    constantService.alert('当前没有待批阅的学生！');
                });
            };

            $scope.autoLoad = function (showLoading) {
                var qFId = findFid($scope.data.idOrderMap,$scope.data.currentGroup.groupname,$scope.data.currentGroup.qstList[0].dis);
                $scope.isAutoLoading = true;
                api.getStusByOrder(qFId, $scope.data.currentGroup.qstList[0].dis.split('.')[0], unifiedId, $scope.data.PULL_LIMIT, true, !showLoading).then(function (res) {
                    $scope.isAutoLoading = false;
                    if (res.data.code === 0) {
                        var filterStus = _.filter(res.data.msg, function (item) {
                            var itemInStus = _.find($scope.data.stus, function (stu) {
                                return stu.id === item.id;
                            });
                            return !itemInStus;
                        });
                        if (filterStus.length === 0) {
                            $scope.haveMore = false;
                        }
                        // $scope.data.stus = $scope.data.stus.concat(res.data.msg);
                        Array.prototype.push.apply($scope.data.stus, filterStus);
                        $scope.data.unMarkStus = _.filter($scope.data.stus, function (item) {
                            return item.isMarked === '0';
                        });
                        if ($scope.data.unMarkStus.length > 0) {
                            preLoadImg(true);
                            $scope.data.currentStu = $scope.data.unMarkStus[0];
                            $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                            $scope.data.markedNum = getMarkedStudents().length;
                            $scope.data.averageScore = getMarkedStusAverageScore();
                        } else {
                            // constantService.alert('所有学生已全部批阅完成！');
                            $scope.haveMore = false;
                            $scope.data.markedNum = getMarkedStudents().length;
                            $scope.data.averageScore = getMarkedStusAverageScore();
                        }
                        constantService.notificate('已为您加载' + filterStus.length + '名待批阅学生');
                    } else {
                        $scope.haveMore = false;
                        // constantService.alert('当前没有待批阅的学生！');
                        constantService.notificate('当前没有更多待批阅的学生');
                    }
                }, function () {
                    $scope.isAutoLoading = false;
                    $scope.haveMore = false;
                    // $scope.haveMore = false;
                    // constantService.alert('当前没有待批阅的学生！');
                    constantService.notificate('当前没有更多待批阅的学生');
                });
            };

            $scope.scale = 1;

            $scope.showOriginal = function () {
                $scope.scale = 1;
                setCutImgHeight();
            };

            $scope.minus = function () {
                if ($scope.scale > 0.1) {
                    $scope.scale = $scope.scale - 0.1;
                }
                setCutImgHeight();
            };

            $scope.plus = function () {
                var width = $('.img-area img').width();
                if ($scope.scale < 2) {
                    $scope.scale = $scope.scale + 0.1;
                }
                setCutImgHeight();
            };
            function setCutImgHeight() {
                var $temp = $('.img-area').find('.cutImgContent');
                _.each($temp, function (item) {
                    var tempHeight = 0;
                    _.each(item.children, function (i) {
                        tempHeight += $(i).height();
                    });
                    $(item).height(Math.ceil($scope.scale >= 1 ? $scope.scale * tempHeight : tempHeight));
                });
            }

            $scope.fastMarking = function (score, e) {
                e.preventDefault();
                e.stopPropagation();
                updateMarking(score,0).then(function () {
                    if(!(!!$scope.data.currentQst[0].orderchange&&$scope.data.currentQst[0].orderchange===1)){
                        if (markNext()) {
                            // constantService.showConfirm('本批次已批阅完成，是否继续批阅下一批次的学生？', ['确定'], function () {
                            //     $scope.loadNext();
                            // });
                            $scope.autoLoad();
                        }
                    }
                });
            };

            $scope.listenEnter = function (e, score, max,index) {
                if (e.keyCode === 13) {
                    //回车批阅
                    if (closeDialog()) {
                        ngDialog.closeAll();
                        return;
                    }
                    if ($scope.checkScoreIllegal(score, max)) {
                        updateMarking(score,index).then(function () {
                            if(!(!!$scope.data.currentQst[index].orderchange&&$scope.data.currentQst[index].orderchange===1)){
                                if (markNext()) {
                                    // constantService.showConfirm('本批次已批阅完成，是否继续批阅下一批次的学生？', ['确定'], function () {
                                    //     $scope.loadNext();
                                    // });
                                    $scope.autoLoad();
                                }
                            }
                        });
                    }
                }
            };

            $scope.listenSubEnter = function (e, subQst, max, index, isLast) {
                if (e.keyCode === 13 || e.keyCode === 187 || e.keyCode === 107 || e.keyCode === 189 || e.keyCode === 109) {
                    if (closeDialog()) {
                        ngDialog.closeAll();
                        return;
                    }
                    if (e.keyCode === 187 || e.keyCode === 107) {
                        subQst.studentScore = +max;
                    } else if (e.keyCode === 189 || e.keyCode === 109) {
                        subQst.studentScore = 0;
                    }
                    if ($scope.checkScoreIllegal(+subQst.studentScore, max)) {
                        $scope.updateSubQstTotalScore();
                        if (isLast) {
                            //提交批阅，更新状态，切换学生
                            $scope.submitSubOnce();
                        } else {
                            //切换下一子题
                            $('.sub-input-part input:eq(' + (index + 1) + ')').focus().select();
                        }
                    }
                }
            };

            $scope.submitSub = function (e, subQst, max, index, isLast) {
                if (+subQst.studentScore >= 0 && subQst.studentScore !== '') {
                    if ($scope.checkScoreIllegal(+subQst.studentScore, max)) {
                        /*if($(e.target).hasClass('red-border')){
                            $(e.target).removeClass('red-border');
                        }*/
                        $scope.updateSubQstTotalScore();
                    }/*else{
                        if(!$(e.target).hasClass('red-border')){
                            $(e.target).addClass('red-border');
                        }
                    }*/
                }
            };
            $scope.isScoreIllegal4redborder = function (score,max) {
                if (+score < 0 || +score > max || +score * 10 % 5 !== 0) {
                    return false;
                } else {
                    return true;
                }
            };

            $scope.updateSubQstTotalScore = function () {
                var score = 0;
               /* _.each($scope.data.currentQst.sub, function (s) {
                    score += (+s.studentScore >= 0 ? +s.studentScore : 0);
                });
                $scope.data.currentQst.totalScore = score;*/
                _.each($scope.data.currentQst,function (s) {
                    score += (+s.studentScore >= 0 ? +s.studentScore : 0);
                });
                $scope.data.currentGroup.studentScore = score;
            };

            $scope.submitSubOnce = function () {
                if (!$scope.data.currentStu.isSubmiting) {
                    $scope.data.currentStu.isSubmiting = true;
                    var examItemIds = [];
                    var orderchangeQlist = [];
                    var grouporderchangelist= [];
                    var checkscoreilllist = [];
                    _.each($scope.data.currentQst,function (b) {
                        if(!!b.orderchange&&b.orderchange===1){
                            orderchangeQlist.push(b.showDis);
                        }
                    });
                    orderchangeQlist = _.uniq(orderchangeQlist);
                    _.each(orderchangeQlist,function (c) {
                        var templist = _.filter($scope.data.currentQst,function (d) {
                            return d.showDis === c;
                        });
                        grouporderchangelist.push(templist);
                    });
                    grouporderchangelist = handleCombineQstScore(grouporderchangelist);
                    _.each(grouporderchangelist,function (s) {
                        _.each(s,function (t) {
                            var v = _.find($scope.data.currentQst,function (v) {
                                return v.QFLnkID === t.QFLnkID;
                            });
                            v.studentScore = t.studentScore;
                        });
                    });
                    _.each($scope.data.currentQst, function (subQst) {
                        var exaItem = _.find($scope.data.currentStu.question, function (item) {
                            return item.qFId === subQst.FLnkID;
                        });
                        examItemIds.push({
                            exaItemId: exaItem.exaItemId,
                            score:+subQst.studentScore
                        });
                        checkscoreilllist.push({score:+subQst.studentScore,max:+subQst.score});
                    });
                    for(var w=0;w<checkscoreilllist.length;w++){
                        if(!$scope.checkScoreIllegal(checkscoreilllist[w].score,checkscoreilllist[w].max)){
                            $scope.data.currentStu.isSubmiting = false;
                            return;
                        }
                    }
                    var defer = $q.defer();
                    (function(stu){
                        api.submitSubMarking(examItemIds, stu.exaId).then(function (res) {
                            stu.isSubmiting = false;
                            if (res.data.code === 0) {
                                updateSubMarking();
                                if (markNext()) {
                                    $scope.autoLoad();
                                }
                            } else if(res.data.code === 3){
                                constantService.alert('您的批阅数据已失效，请点击确定重新获取学生！', function(){
                                    location.reload();
                                });
                            }else {
                                constantService.alert('批阅失败，请重试！');
                            }
                            defer.resolve(res);
                        }, function () {
                            stu.isSubmiting = false;
                            constantService.alert('批阅失败，请重试！');
                            defer.reject();
                        })
                    })($scope.data.currentStu);
                    return defer.promise;
                }
            };

            $scope.checkScoreIllegal = function (score, max) {
                if (+score < 0 || +score > max || +score * 10 % 5 !== 0) {
                    constantService.alert('分数必须为0.5的整数倍，且不能大于本题总分值，请重新批阅！');
                    return false;
                } else {
                    return true;
                }
            };

            $scope.checkStuAnswer = function () {
                api.getCheckAnswerRights(unifiedId, examId, $scope.data.currentStu.id, $scope.data.currentStu.exaId).then(function (res) {
                    if (res.data.code === 0 && +res.data.msg > 0) {
                        ngDialog.open({
                            template: 'check-stu-answer-tmp',
                            className: 'ngdialog-theme-default',
                            appendClassName: 'check-answer-box',
                            closeByDocument: false,
                            plain: false,
                            scope: $scope
                        });
                    } else {
                        constantService.alert('抱歉，您暂时没有权限查看该学生的答卷！');
                    }
                }, function (res) {
                    constantService.alert('抱歉，您暂时没有权限查看该学生的答卷！');
                });
            };

            $scope.onInputFocus = function (e) {
                $(e.currentTarget).select();
            };

            $scope.setImgMarginTop = function (e) {
                $(e).css({
                    'margin-top': ($scope.scale - 1) * $(e).height() / 2 + 'px'
                });
            };

            $scope.preventDefault = function (e) {
                switch (e.keyCode) {
                    case 187:
                    case 107:
                    case 189:
                    case 109:
                        e.preventDefault();
                        e.stopPropagation();
                        break;
                    default:
                        break;
                }
            };

            $scope.setScore = function (type, forceUpdate, isHand) {
                var defer;
                if ($scope.data.stus && $scope.data.stus.length > 0 && $scope.data.currentStu && $scope.data.currentBlock === 2) {
                    if (type === 0) {
                        //零分
                        if ($scope.data.currentQst && $scope.data.currentQst.length > 1) {
                            //提交每个子题的批阅
                            if (isHand || (!!$scope.data.currentQst[0].orderchange&&$scope.data.currentQst[0].orderchange===1)) {
                                _.each($scope.data.currentQst, function (s) {
                                    s.studentScore = 0;
                                });
                                $scope.submitSubOnce();
                            }
                        } else {
                            if (forceUpdate) {
                                $scope.$apply(function () {
                                    defer = updateMarking(0,0);
                                });
                            } else {
                                defer = updateMarking(0,0);
                            }
                        }
                    } else {
                        //满分
                        if ($scope.data.currentQst && $scope.data.currentQst.length > 1) {
                            if (isHand || (!!$scope.data.currentQst[0].orderchange&&$scope.data.currentQst[0].orderchange===1)) {
                                _.each($scope.data.currentQst, function (s) {
                                    s.studentScore = (s.orderchange === 1 ? s.comScore:s.score);
                                });
                                $scope.submitSubOnce();
                            }
                        } else {
                            if (forceUpdate) {
                                $scope.$apply(function () {
                                    defer = updateMarking($scope.data.currentQst[0].score,0);
                                });
                            } else {
                                defer = updateMarking($scope.data.currentQst[0].score,0);
                            }
                        }
                    }
                    if (defer) {
                        defer.then(function () {
                            console.error('-------------------');
                            console.error('切换学生。。。');
                            if (markNext()) {
                                //TODO 批阅到最后一个
                                // constantService.showConfirm('本批次已批阅完成，是否继续批阅下一批次的学生？', ['确定'], function () {
                                //     $scope.loadNext();
                                // });
                                $scope.autoLoad();
                            }
                        });
                    }
                }
            };

            function updateMarking(score,index) {
                ngDialog.closeAll();
                console.log('提交的学生ID：' + $scope.data.currentStu.id);
                console.log('提交的学生答卷ID：' + $scope.data.currentStu.question[0].exaItemId);
                if(!!$scope.data.currentQst[index].orderchange&&$scope.data.currentQst[index].orderchange===1){
                    var defer = $q.defer();
                    $scope.submitSubOnce().then(function (res) {
                        defer.resolve(res);
                    },function () {
                        defer.reject();
                    });
                    return defer.promise;
                }else {
                    $scope.data.currentQst[index].studentScore = score;
                    if (!$scope.data.currentStu.isSubmiting) {
                        $scope.data.currentStu.isSubmiting = true;
                        var dialogObj = ngDialog.open({
                            template: '<div style="font-size: 59px;font-weight: bold;color: #ff0000;font-family: SimHei;">得分:' + score + '</div>',
                            className: 'ngdialog-theme-default',
                            appendClassName: 'scored-toast',
                            closeByDocument: true,
                            plain: true,
                            showClose: false,
                            disableAnimation: true,
                            scope: $scope,
                            controller: function ($scope, $rootScope) {
                                return function () {
                                    $scope.$on('ngDialog.opened', function (e, $dialog) {
                                        $('.ngdialog.ngdialog-theme-default.scored-toast .ngdialog-content').fadeOut(2000, function () {
                                            dialogObj.close();
                                        });
                                    });
                                }
                            }
                        });
                        var defer = $q.defer();
                        (function (stu,score,currentQstFid) {
                            api.submitMarking(score, stu.exaId, stu.question[0].exaItemId).then(function (res) {
                                stu.isSubmiting = false;
                                if (res.data.code === 0) {
                                    console.log('提交成功,学生ID：' + stu.id);
                                    console.log('提交成功，学生答卷ID：' + stu.question[0].exaItemId);
                                    stu.studentScore = [{
                                        id: currentQstFid,
                                        score: score
                                    }];
                                    stu.isMarked = '1';
                                    $scope.data.unMarkStus = _.filter($scope.data.stus, function (item) {
                                        return item.isMarked === '0';
                                    });
                                    $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                                    $scope.data.markedNum = getMarkedStudents().length;
                                    $scope.data.averageScore = getMarkedStusAverageScore();
                                    $scope.data.currentGroup.studentScore = score;
                                } else if(res.data.code === 3){
                                    constantService.alert('您的批阅数据已失效，请点击确定重新获取学生！', function(){
                                        location.reload();
                                    });
                                } else {
                                    constantService.alert('提交批阅失败，请重试！');
                                }
                                defer.resolve(res);
                            }, function () {
                                stu.isSubmiting = false;
                                constantService.alert('提交批阅失败，请重试！');
                                defer.reject();
                            })
                        })($scope.data.currentStu,score,$scope.data.currentQst[index].FLnkID);
                        return defer.promise;
                    }
                }
            }

            function updateSubMarking() {
                $scope.data.currentStu.studentScore = _.map($scope.data.currentQst, function (s) {
                    return {
                        id: s.FLnkID,
                        score: s.studentScore
                    };
                });
                $scope.data.currentStu.isMarked = '1';
                $scope.data.unMarkStus = _.filter($scope.data.stus, function (item) {
                    return item.isMarked === '0';
                });
                $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                $scope.data.markedNum = getMarkedStudents().length;
                $scope.data.averageScore = getMarkedStusAverageScore();
            }

            $scope.$watch('data.currentStu', onStuChange, true);

            $scope.$watch('data.currentGroup', updateStudentScore);

            $scope.$watch('data.currentBlock', setInputFocus);

            function onStuChange(val, oldVal) {
                if ($scope.data.currentStu && $scope.data.currentQst) {
                    $scope.isImgLoaded = false;
                    scrollToCurrentStu();
                    var qstCut = [];
                    _.each(filterCombinQst4ImgCut($scope.data.currentGroup.qstList),function (h) {
                        var tep = _.filter($scope.data.cutData, function (item) {
                            return item.order+'' === h.dis + '';
                        });
                        if(!tep.length>0){
                            tep = _.filter($scope.data.cutData, function (item) {
                                return item.order.split('.')[0] === h.dis.split('.')[0] + '';
                            });
                        }

                        qstCut = qstCut.concat(tep);
                    });
                    qstCut = _.uniq(qstCut);
                    if ($scope.data.currentStu.paperImg && $scope.data.cutData && qstCut && qstCut.length > 0) {
                        $scope.data.stuPaperImg = $scope.data.currentStu.paperImg;
                        $scope.data.currentCut = qstCut;
                        $scope.data.ocrImgList = [];
                        isImgLoaded($scope.data.stuPaperImg).then(function(res){
                            $scope.isImgLoaded = true;
                        }, function(){
                            $scope.isImgLoaded = true;
                        });
                    } else {
                        $scope.data.stuPaperImg = '';
                        $scope.data.currentCut = '';
                        //$scope.data.ocrImgList = $scope.data.currentStu.ocrImg.split(',');
                        $scope.data.ocrImgList = getOcrImgFromQstList($scope.data.currentStu.question);
                        isImgLoaded($scope.data.ocrImgList[0]).then(function(res){
                            $scope.isImgLoaded = true;
                        }, function(){
                            $scope.isImgLoaded = true;
                        });
                    }
                    //计算合并题目分值
                    /*_.each($scope.data.currentQst,function (h) {
                        if(!!h.orderchange && h.orderchange === 1){
                            var tempscore = 0;
                            _.each($scope.data.currentQst,function (v) {
                                if(v.showDis === h.showDis){
                                    var tems = +(_.find($scope.data.currentStu.studentScore, function (w) {
                                        return w.id === v.FLnkID;
                                    })).score;
                                    tempscore = tempscore + tems;
                                }
                            });
                            h.studentScore = +tempscore;
                        }
                    });*/
                } else {
                    $scope.data.stuPaperImg = '';
                    $scope.data.currentCut = '';
                    $scope.data.ocrImgList = [];
                }
                if (val && oldVal && val.id !== oldVal.id) {
                    updateStudentScore();
                    setInputFocus();
                    loadAroundImg();
                } else if (val && !oldVal) {
                    updateStudentScore();
                    setInputFocus();
                }
                ngDialog.closeAll();
            }

            function updateStudentScore() {
                if ($scope.data.currentStu && $scope.data.currentGroup) {
                    scrollToCurrentStu();
                    var qst = _.find($scope.data.allQuestionList, function (qst) {
                        return (qst.groupname + '') === ($scope.data.currentGroup.groupname + '');
                    });
                    $scope.data.currentQst = qst.qstlist;
                    if ($scope.data.currentStu.isMarked === '0') {
                        $scope.data.currentGroup.studentScore = -1;
                        $scope.data.currentGroup.originalScore = -1;
                        _.each($scope.data.currentQst,function (item) {
                            item.studentScore = -1;
                            item.originalScore = -1;
                        })
                    } else {
                        _.each($scope.data.currentQst,function (h) {
                            if(!!h.orderchange && h.orderchange === 1){
                                var tempscore = 0;
                                _.each($scope.data.currentQst,function (v) {
                                    if(v.showDis === h.showDis){
                                        var tems = +(_.find($scope.data.currentStu.studentScore, function (w) {
                                            return w.id === v.FLnkID;
                                        })).score;
                                        tempscore = tempscore + tems;
                                    }
                                });
                                h.studentScore = +tempscore;
                            }else{
                                h.studentScore = +(_.find($scope.data.currentStu.studentScore, function (item) {
                                    return item.id === h.FLnkID;
                                })).score;
                            }
                            h.originalScore = h.studentScore;
                        });
                        if($scope.data.currentGroup.markingShowType == '1'){
                            $scope.data.currentGroup.studentScore = $scope.data.currentQst[0].studentScore;
                        }else{
                            $scope.updateSubQstTotalScore();
                        }
                    }
                    //如果为填空题 根据默认全对 全错 提前置分 如果原来有分则保持原来分数
                    _.each($scope.data.currentQst,function (item) {
                        if(item.qtypeId === '327' && $scope.data.currentGroup.markingShowType === '21'){
                            item.studentScore = item.studentScore === -1?($scope.data.defaultScoreType === '1'?(item.orderchange===1?+item.comScore:+item.score):0):item.studentScore;
                        }
                    });
                    $scope.data.currentGroup.totalScore = calToalScore($scope.data.currentQst).total;
                }
            }

            function setInputFocus() {
                if ($scope.data.currentBlock === 2) {
                    if ($scope.data.currentQst && $scope.data.currentQst.length > 1) {
                        setTimeout(function () {
                            if($scope.data.currentGroup.markingShowType !== '21'){
                                $($('input').get(0)).focus().select();
                            }else{
                                $($('.sub-input-part input').get(0)).focus().select();
                            }
                        }, 0);
                    } else {
                        setTimeout(function () {
                            if($scope.data.currentGroup.markingShowType === '21'){
                                $($('.for-blank-single input').get(0)).focus().select();
                            }else{
                                $($('input').get(0)).focus().select();
                            }
                        }, 0);
                    }
                }
            }

            function scrollToCurrentStu() {
                var $keyBoards = $('.keyboards');
                var boxOffset = $keyBoards.offset().top,
                    boxHeight = $keyBoards.height(),
                    listHeight = $keyBoards.find('ul').height();
                setTimeout(function () {
                    $('.stu-answer-box').scrollTop(0);
                    if ($('#' + $scope.data.currentStu.id).offset()) {
                        var keyOffset = $('#' + $scope.data.currentStu.id).offset().top + $keyBoards.scrollTop();
                        $keyBoards.scrollTop(keyOffset - boxHeight / 2 - boxOffset);
                    }
                }, 100);
            }

            function isImgLoaded(url){
                var defer = $q.defer();
                var $img = new Image();
                $img.src = url;
                $img.onload = function(){
                    setTimeout(function(){
                        defer.resolve();
                    }, 200);
                };
                $img.onerror = function(){
                    setTimeout(function(){
                        defer.reject(1);
                    }, 200);
                };
                if($img.complete) {
                    setTimeout(function(){
                        defer.resolve();
                    }, 200);
                }
                return defer.promise;
            }

            function getCanMarkSub(qst) {
                var defer = $q.defer();
                if (cacheCanMarkSub[qst.FLnkID]) {
                    defer.resolve(cacheCanMarkSub[qst.FLnkID]);
                } else {
                    $http.post('/BootStrap/EncryptUnified/GetEncryptSunQ.ashx', {
                        unifiedItemFId: unifiedId,
                        pQId: qst.FLnkID
                    }).then(function (res) {
                        cacheCanMarkSub[qst.FLnkID] = res;
                        defer.resolve(res);
                    }, function (res) {
                        defer.reject();
                    });
                }
                return defer.promise;
            }

            $(window).on('keyup', function (e) {
                e.preventDefault();
                e.stopPropagation();
                switch (e.keyCode) {
                    case 13:
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        }
                        break;
                    case 187:
                    case 107:
                        //+号
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        } else {
                            $scope.setScore(1, true);
                        }
                        break;
                    case 189:
                    case 109:
                        //-号
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        } else {
                            $scope.setScore(0, true);
                        }
                        break;
                    case 39:
                        //右箭头
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        } else {
                            next(true);
                        }
                        break;
                    case 37:
                        //左箭头
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        } else {
                            pre(true);
                        }
                        break;
                    default:
                        break;
                }
            });

            function pre(forceUpdate) {
                if ($scope.data.stus && $scope.data.stus.length > 0 && $scope.data.currentStu && $scope.data.currentBlock === 2) {
                    var currentIndex = _.findIndex($scope.data.stus, function (item) {
                        return $scope.data.currentStu.id === item.id;
                    });
                    if (typeof currentIndex !== 'undefined' && currentIndex > 0) {
                        if (forceUpdate) {
                            $scope.$apply(function () {
                                $scope.data.currentStu = $scope.data.stus[currentIndex - 1];
                            });
                        } else {
                            $scope.data.currentStu = $scope.data.stus[currentIndex - 1];
                        }
                    } else {
                        if (forceUpdate) {
                            if (currentIndex === 0) {
                                return true;
                            } else {
                                $scope.$apply(function () {
                                    $scope.data.currentStu = $scope.data.stus[$scope.data.stus.length - 1];
                                });
                            }
                        } else {
                            if (currentIndex === 0) {
                                return true;
                            } else {
                                $scope.data.currentStu = $scope.data.stus[$scope.data.stus.length - 1];
                            }
                        }
                    }
                }
            }

            function next(forceUpdate) {
                if ($scope.data.stus && $scope.data.stus.length > 0 && $scope.data.currentStu && $scope.data.currentBlock === 2) {
                    var currentIndex = _.findIndex($scope.data.stus, function (item) {
                        return $scope.data.currentStu.id === item.id;
                    });
                    if (typeof currentIndex !== 'undefined' && currentIndex < $scope.data.stus.length - 1) {
                        if (forceUpdate) {
                            $scope.$apply(function () {
                                $scope.data.currentStu = $scope.data.stus[currentIndex + 1];
                            });
                        } else {
                            $scope.data.currentStu = $scope.data.stus[currentIndex + 1];
                        }
                    } else {
                        if (forceUpdate) {
                            if (currentIndex >= $scope.data.stus.length - 1) {
                                return true;
                            } else {
                                $scope.$apply(function () {
                                    $scope.data.currentStu = $scope.data.stus[0];
                                });
                            }
                        } else {
                            if (currentIndex >= $scope.data.stus.length - 1) {
                                return true;
                            } else {
                                $scope.data.currentStu = $scope.data.stus[0];
                            }
                        }
                    }
                }
            }

            function markNext() {
                if ($scope.data.stus && $scope.data.stus.length > 0 && $scope.data.currentStu && $scope.data.currentBlock === 2) {
                    var currentIndex = _.findIndex($scope.data.stus, function (item) {
                        return $scope.data.currentStu.id === item.id;
                    });
                    if (typeof currentIndex !== 'undefined' && currentIndex < $scope.data.stus.length - 1) {
                        $scope.data.currentStu = $scope.data.stus[currentIndex + 1];
                        if (currentIndex >= $scope.data.stus.length - 1 - 10) {
                            if (!$scope.isAutoLoading && $scope.haveMore) {
                                return true;
                            }
                        }
                    } else if (currentIndex === $scope.data.stus.length - 1) {
                        // constantService.alert('本题已全部批阅完成！');
                        //批阅完成后刷新列表,规避部分数据被锁定导致不一致的问题
                        getStudents($scope.data.currentGroup.groupname,$scope.data.currentGroup.qstList[0].dis, false, true);
                    } else {
                        $scope.data.currentStu = $scope.data.stus[0];
                    }
                }
            }

            function getQuestions(order, examId, idOrderMap) {
                api.getQuestionsByOrders(order, examId, idOrderMap).then(function (res) {
                    if (res.data.code === 0) {
                        $scope.data.allQuestionList = res.data.msg;
                        _.each($scope.data.allQuestionList, function (h) {
                            _.each(h.qstlist,function (item) {
                                item.OptionOne = item.OptionOne || item.optionOne;
                                item.OptionTwo = item.OptionTwo || item.optionTwo;
                                item.OptionThree = item.OptionThree || item.optionThree;
                                item.OptionFour = item.OptionFour || item.optionFour;
                                // if (+item.score * 10 % 10 === 5 || +item.score <= 15) {
                                //     item.scoreList = _.range(0, +item.score + 0.5, 0.5);
                                // } else {
                                //老王要求将快捷打分0.5倍步长去掉 20170508
                                item.scoreList = _.range(0, +item.score + 1);
                                // }
                                _.each($scope.data.groupList,function (n) {
                                    _.each(n.qstList,function (r) {
                                        if(r.qFId === item.FLnkID){
                                            item.showDis = r.showDis;
                                            if(!!r.orderchange){
                                                item.orderchange = r.orderchange;
                                                item.showinput = r.showinput;
                                                if(item.orderchange === 1){
                                                    var tempComScore = calMergeQstScore(item.showDis);//计算合并题目分数
                                                    item.comScore = tempComScore.comScores;
                                                    item.comStuScore = tempComScore.comStuScore;
                                                    // if (+item.score * 10 % 10 === 5 || +item.score <= 15) {
                                                    //     item.scoreList = _.range(0, +item.score + 0.5, 0.5);
                                                    // } else {
                                                    item.comScoreList = _.range(0, +item.comScore + 1);
                                                    //}
                                                }
                                            }
                                            //向题目中设置自定义的快速批阅分数组
                                            if(!!r.fastInputStr || (r.fastInputStr === '0')){
                                                item.scoreList = r.fastInputStr.split(' ');
                                                if(item.orderchange === 1){
                                                    item.comScore = r.fastInputStr.split(' ');
                                                }
                                            }
                                        }
                                    });
                                });
                            });
                        });
                        var qst = _.find($scope.data.allQuestionList, function (qst) {
                            return (qst.groupname + '') === ($scope.data.currentGroup.groupname + '');
                        });
                        $scope.data.currentQst = qst.qstlist;
                    } else {
                        constantService.alert('获取试题失败，请检查试卷是否正常！');
                    }
                }, function () {
                    constantService.alert('获取试题失败！');
                });
            }

            function getStudents(groupname,order, isForce, isReload) {
                var qFId = findFid($scope.data.idOrderMap,groupname,order);
                if (!isReload || !$scope.isReloadOver) {
                    if (isReload) {
                        $scope.isReloadOver = true;
                    }
                    return api.getStusByOrder(qFId, order.split('.')[0], unifiedId, $scope.data.PULL_LIMIT, isForce).then(function (res) {
                        if (res.data.code === 0) {
                            $scope.data.stus = res.data.msg;
                            $scope.data.unMarkStus = _.filter($scope.data.stus, function (item) {
                                return item.isMarked === '0';
                            });
                            $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                            $scope.data.markedNum = getMarkedStudents().length;
                            $scope.data.averageScore = getMarkedStusAverageScore();
                            if (isReload) {
                                if ($scope.data.unMarkStus.length === 0) {
                                    constantService.alert('本题已全部批阅完成！');
                                    return;
                                }
                            }
                            preLoadImg();
                        } else {
                            $scope.data.stus = [];
                            $scope.data.unMarkStus = [];
                            $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                            $scope.data.markedNum = getMarkedStudents().length;
                            $scope.data.averageScore = getMarkedStusAverageScore();
                            constantService.alert('当前没有待批阅的学生！');
                        }
                    }, function (res) {
                        if(res === 'interface_error') {
                            $scope.isReloadOver = false;
                        }
                        $scope.data.stus = [];
                        $scope.data.unMarkStus = [];
                        $scope.data.unMarkNum = $scope.data.unMarkStus.length;
                        $scope.data.markedNum = getMarkedStudents().length;
                        $scope.data.averageScore = getMarkedStusAverageScore();
                        constantService.alert('当前没有待批阅的学生！');
                    });
                } else if (isReload && $scope.isReloadOver) {
                    if($scope.data.unMarkNum>0){
                        constantService.alert('本题还有答卷未批阅!')
                    }else{
                        constantService.alert('本题已全部批阅完成！');
                    }
                }
            }

            function preLoadImg(ignoreImgLoading) {
                var deferList = [];
                if (!ignoreImgLoading) {
                    showImgLoadingTip();
                }
                _.each($scope.data.unMarkStus.slice(0, $scope.data.preload_img_count), function (stu) {
                    if (stu.paperImg && $scope.data.cutData) {
                        // $('<img>').attr('src', stu.paperImg);
                        deferList.push(loadImg(stu.paperImg));
                    } else {
                        //var ocrImgList = stu.ocrImg.split(',');
                        var ocrImgList = getOcrImgFromQstList(stu.question);
                        _.each(ocrImgList, function (img) {
                            // $('<img>').attr('src', img);
                            deferList.push(loadImg(img));
                        });
                    }
                });
                $.when.apply(null, deferList).always(function () {
                    hideImgLoadingTip();
                });
            }

            function loadImg(src) {
                var defer = $.Deferred();
                var $img = new Image();
                $img.src = src;
                if ($img.complete) {
                    defer.resolve(true);
                    return defer;
                }
                $($img).on('load', function (e) {
                    defer.resolve(true);
                }).on('error', function () {
                    defer.resolve(true);
                });
                return defer;
            }

            function loadAroundImg() {
                if ($scope.data.stus && $scope.data.currentStu) {
                    var currentIndex = _.findIndex($scope.data.stus, function (item) {
                        return $scope.data.currentStu.id === item.id;
                    });
                    var start = (currentIndex - $scope.data.preload_img_count) < 0 ? 0 : (currentIndex - $scope.data.preload_img_count);
                    var end = (currentIndex + $scope.data.preload_img_count) > ($scope.data.stus.length - 1) ? ($scope.data.stus.length - 1) : (currentIndex + $scope.data.preload_img_count);
                    var deferList = [];
                    for (var i = start; i < end; i++) {
                        var stu = $scope.data.stus[i];
                        if (stu.paperImg && $scope.data.cutData) {
                            deferList.push(loadImg(stu.paperImg));
                        } else {
                            //var ocrImgList = stu.ocrImg.split(',');
                            var ocrImgList = getOcrImgFromQstList(stu.question);
                            _.each(ocrImgList, function (img) {
                                // $('<img>').attr('src', img);
                                deferList.push(loadImg(img));
                            });
                        }
                    }
                }
            }

            function showImgLoadingTip() {
                ngDialog.close();
                imgLoadingDialog = ngDialog.open({
                    template: '<div class="img-box"><div class="text">正在为您准备学生答卷，请稍后...</div><div class="icon">' +
                    '<span class="fa fa-spinner fa-pulse"></span></div></div>',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'img-loading-tip',
                    closeByDocument: false,
                    plain: true,
                    showClose: false,
                    scope: $scope
                });
            }

            function hideImgLoadingTip() {
                setTimeout(function () {
                    if (imgLoadingDialog) {
                        ngDialog.close(imgLoadingDialog.id);
                    }
                }, 200);
            }

            function getMarkedStudents() {
                return _.filter($scope.data.stus, function (item) {
                    return item.isMarked === '1';
                });
            }

            function getMarkedStusAverageScore() {
                var markedStus = getMarkedStudents();
                var totalScore = 0;
                for (var i = 0; i < markedStus.length; i++) {
                    var scoreArray = _.pluck(markedStus[i].studentScore, 'score');
                    _.each(scoreArray, function (item) {
                        totalScore += (+item >= 0 ? +item : 0);
                    });
                }
                return markedStus.length === 0 ? 0 : (totalScore / markedStus.length).toFixed(1);
            }

            function closeDialog() {
                var loglist = ngDialog.getOpenDialogs();
                if (_.isArray(loglist) && loglist.length > 0) {
                    return true
                } else {
                    return false;
                }
            }

            function getCutImgBySunQ(cutlist, qlist, currentOrder) {
                var temobj = null;
                var temlist = [];
                _.each(qlist, function (item) {
                    temobj = _.find(cutlist, function (i) {
                        return i.order + '' === (item.Distance + '.' + item.SubOrder);
                    });
                    if (!!temobj) {
                        temlist.push(temobj);
                    }
                });
                if (!(temlist.length > 0)) {
                    temlist = _.filter(cutlist, function (n) {
                        return n.order.split('.')[0] === currentOrder + '';
                    });
                }
                return temlist;
            }
            $scope.hideLeftList = function () {
                if ($scope.isShowLeft === 1) {
                    $('#encryptmarking .main-view').css({
                        width: '100%'
                    });
                    $scope.isShowLeft = 0;
                } else {
                    $('#encryptmarking .main-view').css({
                        width: 'calc(100% - 180px)'
                    });
                    $scope.isShowLeft = 1;
                }
            };
            function sortListByGroupname(list) {
                var groupnamelist = [];
                _.each(list,function (item) {
                    groupnamelist.push(item.groupName);
                });
                groupnamelist = _.uniq(groupnamelist);
                var filterGroupname = [];
                _.each(groupnamelist,function (i) {
                    var tep = _.filter(list,function (h) {
                        return h.groupName === i;
                    });
                    filterGroupname.push({groupname:i,markingShowType:tep[0].showType,qstList:tep});
                });
                return filterGroupname;
            }
            function getlistBykey(list,key) {
                var aimlist = [];
                _.each(list,function (item) {
                    aimlist.push({groupname:item.groupname,list:_.pluck(item.qstList, key)});
                });
                return aimlist;
            }
            function findFid(list,groupname,order) {
                var qFIditemList = _.find(list, function (item) {
                    return item.groupname === groupname;
                });
                /*var qFId = _.find(qFIditemList.qstList,function (i) {
                    return i.dis === order;
                }).qFId;*/
                var qFId = '';
                _.each(qFIditemList.qstList,function (h,index) {
                    if(index<qFIditemList.qstList.length-1){
                        qFId = qFId+h.qFId+',';
                    }else{
                        qFId = qFId+h.qFId;
                    }
                });
                return qFId;
            }
            function calToalScore(list) {
                var temtalscore = 0;
                var stuscore = 0;
                _.each(list,function (item) {
                    temtalscore = temtalscore + (item.score === undefined ? 0 : +item.score);
                    stuscore = stuscore + (item.studentScore === undefined ? 0 : +item.studentScore);
                });
                return {total:temtalscore,stu:stuscore};
            }
            function filterByShowDis(list) {
                var repeatindex = [];
                var norepeat = [];
                _.each(list,function (item,index) {
                    _.each(list,function (i,j) {
                        if(index === j){
                            return true;
                        }else if(item.showDis === i.showDis && item.groupName === i.groupName){
                            item.orderchange = 1;
                            if(j > index){
                                i.showinput = 1;
                            }else/* if(!!i.showinput){
                                return true;
                            } else */{
                                i.showinput = 2;
                            }
                        }
                    });
                });
                return list;
            }
            function handleCombineQstScore(qstlist) {
                _.each(qstlist,function (k,index) {
                    var tempqst = _.find(k,function (m) {
                        return !!m.showinput&&m.showinput===1;
                    });
                    if(!!tempqst){
                        var allscore = tempqst.studentScore;
                    }else{
                        return true;
                    }
                    var remainder = 0;
                    if(allscore*10%10 !== 0){
                        remainder = allscore*10%10;
                        remainder = remainder/10;
                        allscore = allscore - remainder;
                    }
                    var allqstnum = k.length;
                    _.each(k,function (q) {
                        q.studentScore = 0;
                    });
                    /*for(;Math.floor(allscore/allqstnum)>=0;){
                        var t = Math.floor(allscore/allqstnum);
                        if(t>0){
                            _.each(k,function (item) {
                                item.studentScore = (+item.studentScore)+t;
                            });
                        }else if(t==0&&(allscore%allqstnum>0)){
                            for(var i = 0;i<allscore%allqstnum;i++){
                                /!*_.each(k,function (h) {
                                    h.studentScore = (+h.studentScore)+1;
                                });*!/
                                k[i].studentScore = (+k[i].studentScore)+1;
                            }
                            break;
                        }else{
                            break;
                        }
                        allscore = allscore - allqstnum * t;
                    }
                    k[k.length-1].studentScore = k[k.length-1].studentScore + remainder;*/
                    var allfullscore = 0;
                    _.each(k,function (j) {
                        allfullscore = allfullscore + j.score;
                    });
                    var alreadyAssign = 0;
                    _.each(k,function (n) {
                        n.studentScore = Math.floor((n.score/allfullscore)*allscore);
                        alreadyAssign = alreadyAssign + n.studentScore;
                    });
                    allscore = allscore - alreadyAssign;
                    for(;allscore > 0;){
                        _.each(k,function (q) {
                            if(allscore > 0 && q.studentScore+1 <= q.score){
                                q.studentScore = q.studentScore + 1;
                                allscore = allscore - 1;
                            }else{
                                return true;
                            }
                        });
                    }
                    for(var s = 0; s<k.length; s++){
                        if(k[s].studentScore + remainder <= k[s].score){
                            k[s].studentScore = k[s].studentScore + remainder;
                            break;
                        }else{
                            continue
                        }
                    }
                });
                return qstlist;
            }
            function getOcrImgFromQstList(list) {
                var imglist = [];
                var templist = [];
                _.each($scope.data.currentGroup.qstList,function (b) {
                    var qstitem = _.find(list,function (c) {
                        return c.qFId === b.qFId
                    });
                    if(!!qstitem){
                        templist.push(qstitem);
                    }
                });
                _.each(templist,function (item) {
                    imglist.push(item.ocriImg);
                });
                imglist = _.uniq(imglist);
                return imglist;
            }
            $scope.showHelpPic = function () {
                var helpImgDialog = ngDialog.open({
                    template: '<img src="../assets/images/encryptMark-help.jpg" style="width: 100%;max-width: 100%;"/>',
                    className: 'ngdialog-theme-default',
                    appendClassName:'showEncryptMarkHelp',
                    closeByDocument: false,
                    plain: true,
                    showClose: true,
                    scope: $scope
                });
            };

            /**
             * 设置快捷打分分值
             */
            $scope.editFastInput = function (qstitem) {
                ngDialog.open({
                    template:'<div class="editFastInput-title">设置分数</div><div class="editFastInput-container">' +
                    '<div class="tip-input-infront">输入分数将用于快捷打分，输入的数字请用空格隔开，且每个分数仅能为<span style="color: #ac2925">0.5</span>的整数倍</div>' +
                    '<div class="tip-score-infront">本题分数<span style="color: #ac2925" ng-bind="qstScore"></span>分，请在合理范围内设置分值.</div>' +
                    '<div class="input-area"><input class="score-input" type="text" ng-model="scores"></div>' +
                    '<div class="btn-group"><div class="btn abolish-btn" ng-click="closeInput()">取消</div><div class="btn confirm-btn" ng-click="confirmInput()">确定</div></div>' +
                    '</div>',
                    className: 'ngdialog-theme-default',
                    appendClassName:'encryptmarkConfig-editFastInput',
                    plain: true,
                    scope:$scope,
                    closeByDocument:false,
                    controller:function ($scope) {
                        return function () {
                            $scope.scores = qstitem.fastInputStr;
                            $scope.qstScore = qstitem.score;
                            $scope.closeInput = function () {
                                ngDialog.closeAll();
                            };
                            $scope.confirmInput = function () {
                                if(!!$scope.scores || ($scope.scores == 0)){
                                    $scope.scores = dbc2sbc($scope.scores);
                                    $scope.scores = trimLAndR($scope.scores);
                                    var scorelist = $scope.scores.split(' ');
                                    var onlyNum = /^(\d+(\.\d{1,2})?)?$/;
                                    for(var i = 0; i < scorelist.length; i++){
                                        if(!onlyNum.test(scorelist[i])){
                                            constantService.showSimpleToast('仅能输入数字，小数点后仅能保留一位!');
                                            return;
                                        }
                                        if (+scorelist[i] < 0 || +scorelist[i] > +qstitem.score || +scorelist[i] * 10 % 5 !== 0) {
                                            constantService.showSimpleToast('分数必须为0.5的整数倍，且不能大于本题总分值！');
                                            return ;
                                        }
                                    }
                                }
                                qstitem.fastInputStr = scorelist.join(' ');
                                qstitem.scoreList = _.uniq(scorelist);//数据去重
                                ngDialog.closeAll();
                            }
                        }
                    }
                })
            };
            function dbc2sbc(str) {
                return str.replace(/[\uff01-\uff5e]/g,function(a){return String.fromCharCode(a.charCodeAt(0)-65248);}).replace(/\u3000/g," ");
            }
            function trimLAndR(s){
                return s.replace(/(^\s*)|(\s*$)/g, "");
            }
            $scope.setFastInputScore = function (score) {
                //$scope.data.currentQst[0].studentScore = score;
                _.each($scope.data.currentQst,function (item) {
                    item.studentScore = score;
                })
            };
            /**
             * 计算合并后题目总以及学生得分总分
             */
            function calMergeQstScore(showDis) {
                var comScores = 0;
                var comStuScores = 0;
                _.each($scope.data.allQuestionList,function (item) {
                    _.each(item.qstlist, function (h) {
                        _.each($scope.data.groupList,function (n) {
                            _.each(n.qstList,function (r) {
                                if(r.qFId === h.FLnkID && r.orderchange === 1 && r.showDis === showDis){
                                    comScores = comScores + h.score;
                                    comStuScores = comStuScores + ((!!h.studentScore || ((h.studentScore+'') === '0')) ? h.studentScore : 0);
                                }
                            });
                        });
                    });
                });
                return {comScores:comScores,comStuScore:comStuScores};
            }
            $scope.fastBlankMark = function (score,index) {
                $scope.data.currentQst[index].studentScore = score;
            };
            $scope.blankSub = function (subQst, max, index, isLast) {
                if (closeDialog()) {
                    ngDialog.closeAll();
                    return;
                }
                if ($scope.checkScoreIllegal(+subQst.studentScore, max)) {
                    $scope.updateSubQstTotalScore();
                    /*if (!isLast) {
                        //切换下一子题
                        $('.sub-input-part input:eq(' + (index + 1) + ')').focus().select();
                    }*/
                }
            };
            $scope.submitBlankSub = function () {
                if (closeDialog()) {
                    ngDialog.closeAll();
                    return;
                }
                for(var i = 0; i < $scope.data.currentQst; i++){
                    if($scope.checkScoreIllegal(+$scope.data.currentQst[i].studentScore,(scope.data.currentQst[i].orderchange===1?+scope.data.currentQst[i].comScore:+scope.data.currentQst[i].score))){
                        continue;
                    }else{
                        constantService.alert('存在不合理批阅分数!');
                        return;
                    }
                }
                $scope.updateSubQstTotalScore();
                //提交批阅，更新状态，切换学生
                $scope.submitSubOnce();
            };
            $scope.$watch('data.defaultScoreType',function (newVal,oldVal) {
                if(newVal === oldVal){
                    return;
                }
                _.each($scope.data.currentQst,function (item) {
                    if(item.qtypeId === '327' && $scope.data.currentGroup.markingShowType === '21'){
                        item.studentScore = item.studentScore === -1?($scope.data.defaultScoreType === '1'?(item.orderchange===1?+item.comScore:+item.score):0):item.studentScore;
                    }
                });
            });
            /**
             * 对于真合并的题目 后切图只需要取其中一题便可
             */
            function filterCombinQst4ImgCut(list) {
                var afterfilter = [];
                _.each(_.values(_.groupBy(list,'showDis')),function (item) {
                    afterfilter.push(item[0]);
                });
                return afterfilter;
            }
        }];
});
angular.module('app', []).service('api', function ($http, $q, paperService) {
    return {
        send: function (url, param) {
            return $http.post(url, param);
        },
        getMarkingOrder: function (unifiedIFId) {
            return this.send('/BootStrap/EncryptUnified/GetEncryptQList.ashx', {
                unifiedIFId: unifiedIFId
            });
        },
        getStusByOrder: function (qFId, order, examId, limit, isForce, ignoreLoading) {
            var param = {
                qFId: qFId,
                order: order,
                unifiedIFId: examId,
                limit: limit
            };
            isForce && (param.isForce = '1');
            var defer = $q.defer();
            $http({
                method: 'post',
                url: '/BootStrap/EncryptUnified/GetEncryptStuList.ashx',
                data: param,
                ignoreLoadingBar: !!ignoreLoading
            }).then(function (res) {
                if (_.isArray(res.data.msg)) {
                    res.data.code = 0;
                    _.each(res.data.msg, function (item) {
                        item.studentScore = _.map(item.question, function (q) {
                            return {
                                id: q.qFId,
                                score: +q.scores,
                                exaItemId: q.exaItemId
                            }
                        });
                    });
                    defer.resolve(res);
                } else {
                    defer.reject(res);
                }
            }, function (res) {
                defer.reject('interface_error');
            });
            return defer.promise;
        },
        getQuestionsByOrders: function (order, examId, idOrderMap) {
            var defer = $q.defer();
            var self = this;
            paperService.getPaperDataById(examId).then(function (res) {
                var questions = self.getPaperAllQsts(res.data.msg);
                // var needQuestion = _.filter(questions, function (item) {
                //     return $.inArray(item.orders + '', order) !== -1;
                // });
                var nQsts = [];
                _.each(order, function (o) {
                    var groupname = o.groupname;
                    nQsts.push({groupname:o.groupname,qstlist:[]});
                    _.each(o.list,function (t) {
                        var oArray = t.split('.');
                        var qst;
                        /*qst = _.find(questions, function (q) {
                            return q.dis + '' === t;
                        });
                        nQsts[nQsts.length-1].qstlist.push(qst);*/
                        if (oArray.length === 1) {
                            qst = _.find(questions, function (q) {
                                return q.dis + '' === t;
                            });
                            nQsts[nQsts.length-1].qstlist.push(qst);
                        } else {
                            var main = _.find(questions, function (q) {
                                return q.dis + '' === oArray[0];
                            });
                            qst = _.find(main.sub, function (s) {
                                return s.dis + '' === t;
                            });
                            nQsts[nQsts.length-1].qstlist.push(!!qst?qst:main);
                        }
                    });
                });
                // var nQsts = _.filter(questions, function(q){
                //     return $.inArray(q.QFLnkID + '', _.pluck(idOrderMap, 'qFId')) !== -1;
                // });

                defer.resolve({
                    data: {
                        code: 0,
                        msg: nQsts
                    }
                });
            });
            return defer.promise;
        },
        getPaperAllQsts: function (groups) {
            var allQuestions = [];
            _.each(groups, function (item, index) {
                _.each(item.question, function (q) {
                    if (q.mode === 'A') {
                        allQuestions = allQuestions.concat(q.sub);
                    } else {
                        if(q.mode === 'B'){
                            _.each(q.sub,function (h) {
                                h.parent.mode = q.mode;
                            })
                        }
                        allQuestions.push(q);
                    }
                });
            });
            return allQuestions;
        },
        submitMarking: function (score, exaId, exaItemId) {
            return $http({
                method: 'post',
                url: '/BootStrap/Interface/Interface0137.ashx',
                data: {
                    score: score,
                    exaId: exaId,
                    exaItemId: exaItemId
                },
                ignoreLoadingBar: true
            });
        },
        submitSubMarking: function (answerMap, exaId) {
            return $http({
                method: 'post',
                url: '/BootStrap/Interface/Interface0137A.ashx',
                data: {
                    exaId: exaId,
                    exaItemIds: answerMap
                },
                ignoreLoadingBar: true
            });
        },
        getPaperCut: function (unifiedId) {
            return this.send('/BootStrap/EncryptUnified/GetEncryptUnifiedCutP.ashx', {
                unifiedIFId: unifiedId
            });
        },
        getCheckAnswerRights: function (unifiedId, examId, studentId, exaId) {
            return this.send('/BootStrap/EncryptUnified/getCheckAnswerPaperRights.ashx', {
                unifiedIFId: unifiedId,
                examId: examId,
                studentId: studentId,
                exaId: exaId
            });
        }
    }
});