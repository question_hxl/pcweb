/**
 * Created by 赵文东 on 2017/2/27.
 */
define(['pagination'],function () {
    return ['$scope','$http','$location','constantService','$q',function ($scope,$http,$location,constantService,$q) {
        $('#markPaperRecord').css('min-height', $(window).height()*0.7);
        var unifiedid = $location.search().unifiedid;
        $scope.data = {
            currentQst:null,
            currentindex:0,
            allQuestionList : [],
            detailArr:[]
        };
        $http.post('BootStrap/EncryptUnified/getPaperConfig.ashx',{
            unifiedId:unifiedid
        }).then(function (resp) {
            var questionlist = [];
            _.each(resp.data.msg.meta,function (item) {
                questionlist = questionlist.concat(item.question);
            });
            $scope.data.allQuestionList = questionlist;
            $scope.data.currentQst = $scope.data.allQuestionList[0];
        },function (resp) {
            constantService.alert('获取数据失败!');
            $scope.data.allQuestionList = [];
        });
        $scope.prevPage = function () {
            $scope.data.currentindex = $scope.data.currentindex-1<0?$scope.data.currentindex:$scope.data.currentindex-1;
            $scope.data.currentQst = $scope.data.allQuestionList[$scope.data.currentindex];
        };
        $scope.nextPage = function () {
            $scope.data.currentindex = $scope.data.currentindex+1>$scope.data.allQuestionList.length-1?$scope.data.currentindex:$scope.data.currentindex+1;
            $scope.data.currentQst = $scope.data.allQuestionList[$scope.data.currentindex];
        };
        $scope.changeCurrentPage = function (item,index) {
            $scope.data.currentindex = index;
            $scope.data.currentQst = item;
        };
        $scope.goBack = function () {
            history.go(-1);
        };
        $scope.$watch('data.currentQst',function (newVal,oldVal) {
            if(newVal != oldVal){
                getQstMarkHistory({unifiedId:unifiedid,showDis:newVal.showDis,order:newVal.orders,MarkingUser:''}).then(function (resp) {
                    if(_.isArray(resp.data.teacherlist)&&(resp.data.teacherlist.length>0)){
                        $scope.data.detailArr = resp.data.teacherlist;
                    }else{
                        $scope.data.detailArr = [];
                        constantService.alert('暂无数据!');
                    }
                },function (resp) {
                    $scope.data.detailArr = [];
                    constantService.alert('获取数据失败!');
                })
            }
        });
        function getQstMarkHistory(param) {
            var defer = $q.defer();
            $http({
                method: 'post',
                url: '/BootStrap/schoolmanager/getEncryptionMarkHistory.ashx',
                data: param
            }).then(function (res) {
                defer.resolve(res);
            }, function (res) {
                defer.reject(res);
            });
            return defer.promise;
        }
        $scope.searchStu = function () {
            if(!$scope.searchKey|| !($scope.data.detailArr.length>0)){
                return;
            }else{
                _.each($scope.detailArr,function (item) {
                    _.each(item.classlist,function (i) {
                        i.stulist = _.filter(i.stulist,function (h) {
                            return h.stuname.indexOf($scope.searchKey) || h.stuno.indexOf($scope.searchKey);
                        })
                    })
                })
            }
        }
    }];
});