/**
 * Created by 赵文东 on 2016/11/9.
 */
define(['jquerymedia'], function () {
    return ['$rootScope', '$scope', '$http', '$location', '$q', 'constantService', 'paperService', 'utilService', 'ngDialog', '$state',
        function ($rootScope, $scope, $http, $location, $q, constantService, paperService, utilService, ngDialog, $state) {
            $('#overallMarking').css('height', $(window).height());
            $(window).off('resize').on('resize', function (e) {
                $('#overallMarking').css('height', $(window).height());
            });

            var search = $location.search();
            var examId = search.ExamFlnkID;
            var classId = search.ClassFlnkID;
            var taskId = search.TaskFId;
            var gradeNum = search.gradeNum;
            var marktype = search.marktype;
            $scope.status = search.status;
            $scope.examName = search.ExamName;
            var unifiedItemFid = search.UnifiedItemFid;
            $scope.logoSrc = '/assets/skin/' + window.config.theme + '/img/loading-logo.png';
            $scope.copy = window.config.themeCompanyNameMap[window.config.theme];
            $scope.no_data_img = '/assets/images/51jyfw-nopic.jpg';
            $scope.data = {
                currentStuList: [],
                currentQuestionsArr: [],
                currentPaperName: '',
                currentPaperImg: '',
                currentStu: null,
                currentPaperMyScore: 0,
                currentPaperAccessory: ''
            };
            //存储获取到的试卷配置数据
            var paperConfigData = null;
            //存储合并数据 在开始展示时找出合并的题目 在提交分数的时候拆开赋值的依据 每次切换学生的时候需要重新赋值 数据里面包含 showDis 展示题号
            //hiddenQstlist 被合并题目（不包含合并题目的第一题） showQst 被合并题目的第一题
            var combinDetailList = [];
            //标志位 是否展示所有学生
            $scope.isShowAll = false;
            //班级选择
            $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                UnifiedItemFId: unifiedItemFid,
                Proc_name: 'Proc_GetUnifiedTasks'
            }).success(function (res) {
                if (res.msg && _.isArray(res.msg)) {
                    $scope.classData = res.msg;
                    $scope.classIndex = 0;
                    $scope.currentClassID = $scope.classData[$scope.classIndex].ClassFLnkID;
                    getAbnormal();
                    getUnbindPaper();
                }
            });

            // 获取失效试卷
            function getUnbindPaper() {
                $scope.TaskFId =  $scope.classData[0].FLnkID;
                $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                    taskFid: $scope.TaskFId
                }).then(function (res) {
                    if (_.isArray(res.data.msg)) {
                        $scope.fileList = res.data.msg;
                        $scope.noRelatedList = [];
                        _.each($scope.fileList, function (resq) {
                            if (resq.isNoRelated === 0 || resq.isNoRelated === 2) {
                                $scope.noRelatedList.push(resq);
                            }
                        });
                    }else {
                        $scope.noRelatedList = [];
                    }
                });
            }

            //获取题目异常试卷
            function getAbnormal() {
                var newFidList = [];
                _.each($scope.classData, function (item) {
                    newFidList.push(getAbnormalPapers(item, examId));
                });
                if (newFidList.length > 0) {
                    $scope.loadStu = [];
                    $scope.stuData = [];
                    $q.all(newFidList).then(function (resq) {
                        _.each(resq, function (res) {
                            /*_.each(res.data.studentList, function (data) {
                                //if (data.type == -1) {
                                    $scope.loadStu.push(data);
                                //}
                            })*/
                            $scope.stuData.push(res.data.studentList);
                        });
                        /*for(var i=0,len=$scope.loadStu.length;i<len;i+=30){
                            $scope.stuData.push($scope.loadStu.slice(i,i+30));
                        }*/
                        $scope.classIndex = 0;
                        $scope.data.currentStuList = $scope.stuData[$scope.classIndex];
                        _.each($scope.data.currentStuList, function (i) {
                            i.type = +i.type;
                        });
                        $scope.data.currentStuList.sort(function (a,b) {
                            return a.type - b.type;
                        });
                        $scope.data.currentStu = $scope.data.currentStuList[0];
                        //获取错误题目信息
                        getErrQst();
                    }, function (res) {
                        constantService.alert('获取学生列表失败！');
                    });
                }
            }
            function getAbnormalPapers(classId, examfid) {
                var defer = $q.defer();
                /**
                 * type值：-1：未批改；0：已批改；2：无数据
                 */
                $http.post($rootScope.baseUrl + '/Interface0045A.ashx', {
                    classId: classId.ClassFLnkID,
                    examFLnkId: examfid
                }).then(function (res) {
                    defer.resolve(res);
                }, function (res) {
                    defer.reject();
                });
                return defer.promise;
            }
            $scope.abnormalLoad = function () {
                if($scope.classIndex ===  $scope.stuData.length-1 ){
                    constantService.alert('暂无需要加载的数据!')
                }else{
                    var lastIndex = $scope.data.currentStuList.length;
                    $scope.classIndex = $scope.classIndex+1;
                    $scope.data.currentStuList = $scope.data.currentStuList.concat($scope.stuData[$scope.classIndex]);
                    $scope.data.currentStu = $scope.data.currentStuList[lastIndex];
                }
            };
            $scope.$watch('data.currentStu', function (newValue, oldValue) {
                if (newValue === oldValue) {
                    return;
                }
                if (!newValue) {
                    return;
                }
                $http.post($rootScope.baseUrl + '/Interface0187.ashx', {
                    ExamFLnkID: examId,
                    UserFLnkID: $scope.data.currentStu.studentId
                }).then(function (resp) {
                    if (resp.data.img == null) {
                        $scope.data.currentPaperImg = $scope.no_data_img;
                    } else {
                        var tempimgsplit = resp.data.img.split('/');
                        tempimgsplit[tempimgsplit.length - 1] = '0' + tempimgsplit[tempimgsplit.length - 1];
                        $scope.data.currentPaperImg = tempimgsplit.join('/');
                        $scope.data.currentPaperAccessory = resp.data.Accessory;
                        $scope.data.currentPaperName = resp.data.ExName;
                        $scope.data.currentQuestionsArr = _.pairs(_.groupBy(resp.data.msg, 'dType'));
                        $scope.data.currentPaperTotalScore = 0;
                        _.each($scope.data.currentQuestionsArr, function (i) {
                            var everyGroupMyTotalScore = 0;//每组题目学生总得分
                            _.each(i[1], function (item) {
                                item.allscores = +item.allscores;
                                item.order = +item.order;
                                item.scores = !!item.scores ? +item.scores : item.score;
                                item.beChanged = false;
                                //计算试卷总分
                                $scope.data.currentPaperTotalScore = $scope.data.currentPaperTotalScore + item.allscores;
                                //判断题目列表中是否有后纠错题目
                                if (!!$scope.data.currentStu.errQstList && $scope.data.currentStu.errQstList.length > 0) {
                                    for (var j = 0; j < $scope.data.currentStu.errQstList.length; j++) {
                                        if (item.order === +$scope.data.currentStu.errQstList[j].order) {
                                            item.isError = true;
                                        }
                                    }
                                }
                                if (item.scores === '' || item.scores === null || item.scores < 0 || item.scores > item.allscores || item.scores * 10 % 5 !== 0) {
                                    item.isError = true;
                                } else {
                                    item.isError = false;
                                }
                                everyGroupMyTotalScore = everyGroupMyTotalScore + (!!item.scores ? +item.scores : 0);
                            });
                            i.push(i[0] === '选择题' ? false : true);
                            //查找该组中是否存在后纠错题目
                            i.push(!!_.find(i[1], function (k) {
                                return !!k.isError;
                            }));
                            i.push(everyGroupMyTotalScore);
                        });
                        getPaperConfig();
                    }
                    calStuScore();
                }, function (resp) {
                    constantService.alert('获取学生试卷信息失败！')
                });
                scrollToCurrentStu();
            });
            $scope.expandList = function (index) {
                $scope.data.currentQuestionsArr[index][2] = !$scope.data.currentQuestionsArr[index][2];
            };

            $scope.submitScore = function () {
                var questionListArry = [];
                var errOrders = [];
                var newPage = _.map($scope.data.currentQuestionsArr, function (group, i) {
                    group = handleCompletion(combinDetailList, group);
                    var questionsL = _.map(group[1], function (item, index) {
                        if (+item.scores > +item.allscores || +item.scores < 0 || +item.scores * 10 % 5 !== 0 || item.scores === '' || item.scores === undefined || +item.scores === null) {
                            errOrders.push(item.order);
                        }
                        return {
                            'qFlnkId': item.qFlnkId,
                            'scores': item.scores
                        }
                    });
                    return {
                        quesitonBox: questionsL
                    }
                });
                checkSetPaperList();
                if (errOrders.length) {
                    var orders = errOrders.join(',');
                    constantService.alert('第' + orders + '题的得分不合理，请核对!');
                    return;
                }
                var greatObj = function () {
                    angular.forEach(newPage, function (item) {
                        angular.forEach(item.quesitonBox, function (itemr) {
                            questionListArry.push(itemr)
                        })
                    });
                    return questionListArry;
                };

                var msgDate = greatObj();

                $http({
                    method: 'POST',
                    url: $rootScope.baseUrl + '/Interface0188.ashx',
                    data: {
                        examFlnkId: examId,
                        userFlnkId: $scope.data.currentStu.studentId,
                        msg: msgDate
                    }
                }).then(function (resp) {
                    $scope.data.currentStu.type = 0;
                    var newIndex = _.findIndex($scope.data.currentStuList, function (item) {
                        return item.studentId === $scope.data.currentStu.studentId;
                    });
                    constantService.alert(resp.data.msg);
                    var lastIndex = $scope.data.currentStuList.length;
                    if (($scope.classIndex + 1) === $scope.stuData.length) {
                        if ((newIndex + 1) !== $scope.data.currentStuList.length) {
                            $scope.data.currentStu = $scope.data.currentStuList[newIndex + 1];
                        } else {
                            $scope.data.currentStu = $scope.data.currentStuList[lastIndex - 1];
                        }
                    }else{
                        if((newIndex+1) === $scope.data.currentStuList.length){
                            $scope.classIndex = $scope.classIndex+1;
                            $scope.data.currentStuList = $scope.data.currentStuList.concat($scope.stuData[$scope.classIndex]);
                            $scope.data.currentStu = $scope.data.currentStuList[newIndex+1];
                        }else{
                            $scope.data.currentStu = $scope.data.currentStuList[newIndex+1];
                        }
                    }
                    calStuScore();
                }, function (resp) {
                    constantService.alert('分数提交失败！');
                    calStuScore();
                });
            };
            function getErrQst() {
                /*后纠错*/
                $http.post('/BootStrap/EncryptUnified/AiTaskAssessment.ashx', {//数据有问题的学生
                    taskFid: taskId
                }).then(function (resp) {
                    //如果没有错误数据则显示全部学生列表
                    if(resp.data.msg.length === 0){
                        $scope.isShowAll = true;
                    }
                    for (var i = 0; i < resp.data.msg.length; i++) {
                        for (var j = 0; j < $scope.data.currentStuList.length; j++) {
                            if (resp.data.msg[i].userfid === $scope.data.currentStuList[j].studentId) {
                                $scope.data.currentStuList[j].errQstList = resp.data.msg[i].qOrder;
                            }
                        }
                    }
                }, function (resp) {
                    console.log('请求纠错接口失败！');
                });
            }

            //处理输入的分数
            $scope.inputScore = function (question, index, group, e) {
                var tar = e.target;
                $(tar).css({
                    'color': '#000'
                });
                var errorflag = true;
                if (!checkScoreIllegal(question.scores, question.allscores, true)) {
                    errorflag = true;
                    // question.scores = 0;
                } else {
                    errorflag = false;
                }
                var groupindex = _.findIndex($scope.data.currentQuestionsArr, group);
                if (groupindex >= 0) {
                    $scope.data.currentQuestionsArr[groupindex][1][index].beChanged = true;
                    $scope.data.currentQuestionsArr[groupindex][1][index].isError = errorflag;
                }
                calStuScore();
                check4Error();
            };
            //计算当前学生得分
            function calStuScore() {
                var score = 0;
                _.each($scope.data.currentQuestionsArr, function (item) {
                    var everyGroupMyTotalScore = 0;
                    _.each(item[1], function (i) {
                        score = score + (!!i.scores ? +i.scores : 0);
                        everyGroupMyTotalScore = everyGroupMyTotalScore + (!!i.scores ? +i.scores : 0);
                    });
                    item[4] = everyGroupMyTotalScore;
                });
                $scope.data.currentPaperMyScore = score;
            }

            //修改分数后判断该组内是否还有错误题目
            function check4Error() {
                _.each($scope.data.currentQuestionsArr, function (item) {
                    var temflag = false;
                    _.each(item[1], function (i) {
                        if (i.isError) {
                            temflag = true;
                            return false;
                        }
                    });
                    item[3] = temflag;
                });
            }

            //设置默认图片展示比例
            $scope.imgWidth = $('.stu-answer-box').width() - 40;
            $scope.scale = 1;

            $scope.showOriginal = function () {
                $scope.imgWidth = $('.stu-answer-box').width() - 40;
                $scope.scale = 1;
            };

            $scope.minus = function () {
                $scope.imgWidth = (($scope.imgWidth - 80 > 500) ? ($scope.imgWidth - 80) : 500);
                if ($scope.scale > 0.2) {
                    $scope.scale = $scope.scale - 0.1;
                }
            };

            $scope.plus = function () {
                $scope.imgWidth = $scope.imgWidth + 80;
                if ($scope.scale < 2) {
                    $scope.scale = $scope.scale + 0.1;
                }
            };
            $(window).off('keyup.over').on('keyup.over', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $scope.$apply(handleSmallkeybord(e));

            });
            function handleSmallkeybord(e) {
                switch (e.keyCode) {
                    case 39:
                        //右箭头
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        } else {
                            next();
                        }
                        break;
                    case 37:
                        //左箭头
                        if (closeDialog()) {
                            ngDialog.closeAll();
                        } else {
                            pre();
                        }
                        break;
                    default:
                        break;
                }
            }

            $scope.markStudent = function (stu) {
                if (stu.type+'' === '2') {
                    return;
                }
                $scope.data.currentStu = stu;
            };

            function next() {
                //var index = $scope.data.currentStu.index;
                var index = _.findIndex($scope.data.currentStuList, $scope.data.currentStu);
                for (; index < $scope.data.currentStuList.length;) {
                    if (index === $scope.data.currentStuList.length - 1) {
                        constantService.alert('已经到最后了，不能再向后了！');
                        return;
                    } else {
                        index = index + 1;
                    }
                    if ($scope.data.currentStuList[index].type === 2) {
                        continue;
                    } else {
                        break;
                    }
                }
                $scope.data.currentStu = $scope.data.currentStuList[index];
            }

            function pre() {
                var index = _.findIndex($scope.data.currentStuList, $scope.data.currentStu);
                if (index === 0) {
                    constantService.alert('已经是第一个了，不能再向前了！');
                } else {
                    $scope.data.currentStu = $scope.data.currentStuList[index - 1];
                }
            }


            $scope.preventDefault = function (e) {
                switch (e.keyCode) {
                    case 187:
                    case 107:
                    case 189:
                    case 109:
                        e.preventDefault();
                        e.stopPropagation();
                        break;
                    default:
                        break;
                }
            };

            function closeDialog() {
                var loglist = ngDialog.getOpenDialogs();
                if (_.isArray(loglist) && loglist.length > 0) {
                    return true
                } else {
                    return false;
                }
            }

            function scrollToCurrentStu() {
                var $keyBoards = $('.keyboards');
                var boxOffset = $keyBoards.offset().top,
                    boxHeight = $keyBoards.height(),
                    listHeight = $keyBoards.find('ul').height();
                var index = _.findIndex($scope.data.currentStuList, $scope.data.currentStu);
                setTimeout(function () {
                    if ($('#' + index).offset()) {
                        var keyOffset = $('#' + index).offset().top + $keyBoards.scrollTop();
                        $keyBoards.scrollTop(keyOffset - boxHeight / 2 - boxOffset);
                    }
                }, 100);
            }

            function checkScoreIllegal(score, max, isShowAlert) {
                if (+score < 0 || +score > max || +score * 10 % 5 !== 0 || score === '' || score === null) {
                    // if (isShowAlert) {
                    //     constantService.alert('分数必须为0.5的整数倍，且不能大于本题总分值，请重新批阅！');
                    // }
                    return false;
                } else {
                    return true;
                }
            }

            $scope.goBackButton = function () {
                //回到列表页面
                $scope.noFinish = [];
                _.each($scope.data.currentStuList,function (item) {
                    if(item.type == -1){
                        $scope.noFinish.push(item);
                    }
                });
                $scope.allPaper = $scope.noRelatedList.length +  $scope.noFinish.length;
                if ( $scope.allPaper > 0) {
                    $state.go('myApp.setRelated', {
                        unifiedId: unifiedItemFid,
                        taskFid: taskId,
                        marktype: marktype,
                        examfid: examId,
                        gradeNum: gradeNum,
                        status: status
                    })
                } else {
                    $http.post('' + '/BootStrap/schoolmanager/updateUnifiedItemStep.ashx', {//设置子统考
                        unifiedItemId:unifiedItemFid,
                        step: '5'
                    }).then(function (res) {
                        if (res.data.code === 0) {
                            constantService.alert('暂无待纠错试卷，即将前往考试中心', function(){
                                $state.go('myApp.examCenter');
                            });
                        } else {
                            constantService.alert(res.data.msg);
                        }
                    });
                    ngDialog.close();
                }
            };
            $scope.inputSelect = function (e) {
                e.target.select();
            };

            /**
             * 题号合并改造
             * 支持题号后批阅
             */
            //获取配置列表
            function getPaperConfig(currentQstArr) {
                if ((unifiedItemFid + '') === '0' || unifiedItemFid === undefined || unifiedItemFid === null) {
                    return;
                } else {
                    rquestPaperConfig(unifiedItemFid).then(function (res) {
                        paperConfigData = res.data.msg.meta;
                        insertShowDis($scope.data.currentQuestionsArr, paperConfigData);
                        checkSetPaperList();
                    });
                }
            }

            function rquestPaperConfig(unifiedFid) {
                var defer = $q.defer();
                $http.post('' + '/BootStrap/EncryptUnified/getPaperConfig.ashx', {
                    unifiedId: unifiedFid
                }).then(function (res) {
                    defer.resolve(res);
                }, function (res) {
                    defer.reject(res)
                });
                return defer.promise;
            }

            /**
             * 对照配置表合并题目
             */
            function checkSetPaperList() {
                combinDetailList = [];
                _.each($scope.data.currentQuestionsArr, function (item) {
                    var combinlist = filterCombin(item[1]);
                    if (combinlist.length > 0) {
                        _.each(combinlist, function (h) {
                            _.each(item[1], function (n) {
                                if (n.qFlnkId === h.showQst.qFlnkId) {
                                    n = h.showQst;
                                }
                            });
                            _.each(h.hiddenQstlist, function (m) {
                                item[1] = _.without(item[1], m);
                            })
                        });
                        combinDetailList.push({dtype: item[0], combin: combinlist});
                    }
                })
            }

            function filterCombin(list) {
                var tempShowDisList = [];
                _.each(list, function (item) {
                    tempShowDisList.push(item.showDis);
                });
                tempShowDisList = _.uniq(tempShowDisList);
                var showdisGroup = [];
                _.each(tempShowDisList, function (item) {
                    var tempQst = _.filter(list, function (h) {
                        return h.showDis === item;
                    });
                    if (tempQst.length > 1) {
                        var tempbeChanged = _.find(tempQst, function (e) {
                            return e.beChanged === true;
                        });
                        tempQst[0].beChanged = (tempbeChanged === undefined ? false : true);
                        var tempisError = _.find(tempQst, function (f) {
                            return f.isError === true;
                        });
                        tempQst[0].isError = (tempisError === undefined ? false : true);
                        var tempallscores = 0;
                        _.each(tempQst, function (g) {
                            tempallscores = g.allscores + tempallscores;
                        });
                        tempQst[0].allscores = tempallscores;
                        var undefinedscore = _.filter(tempQst, function (f) {
                            return f.scores === undefined || f.scores === null;
                        });
                        if (undefinedscore.length === tempQst.length) {
                            tempQst[0].scores = undefined;
                        } else {
                            var temnumscores = 0;
                            _.each(tempQst, function (t) {
                                if (t.scores !== undefined && t.scores !== null) {
                                    temnumscores = t.scores + temnumscores;
                                }
                            });
                            tempQst[0].scores = temnumscores;
                        }
                        showdisGroup.push({
                            showDis: item,
                            hiddenQstlist: _.without(tempQst, tempQst[0]),
                            showQst: tempQst[0]
                        });
                    }
                });
                return showdisGroup;
            }

            function insertShowDis(orginlist, configlist) {
                for (var i = 0; i < orginlist.length; i++) {
                    _.each(orginlist[i][1], function (item) {
                        _.each(configlist, function (h) {
                            var tempQst = _.find(h.question, function (n) {
                                return n.qFlnkId === item.qFlnkId;
                            });
                            if (tempQst !== undefined) {
                                item.showDis = tempQst.showDis;
                            }
                        });
                    });
                }
            }

            //提交分数的时候 要把合并的题目补全
            function handleCompletion(combinlist, group) {
                _.each(combinlist, function (item) {
                    if (item.dtype === group[0]) {
                        _.each(item.combin, function (s) {
                            _.each(group[1], function (h, num) {
                                if (h.qFlnkId === s.showQst.qFlnkId) {
                                    var thandlelist = [];
                                    s.hiddenQstlist.unshift(s.showQst);
                                    thandlelist = handleCombineQstScore(s.hiddenQstlist, h.scores);
                                    _.each(thandlelist, function (n, index) {
                                        if (index === 0) {
                                            h = n;
                                        } else {
                                            group[1].splice(num + index, 0, n);
                                        }
                                    });
                                }
                            });
                        });
                    }
                });
                return group;
            }

            //处理分数 将合并的分数分给其他题目
            function handleCombineQstScore(qstlist, allscore) {
                var allscore = allscore;
                var allqstnum = qstlist.length;
                _.each(qstlist, function (q) {
                    q.scores = 0;
                });
                for (; Math.floor(allscore / allqstnum) >= 0;) {
                    var t = Math.floor(allscore / allqstnum);
                    if (t > 0) {
                        _.each(qstlist, function (item) {
                            item.scores = (+item.scores) + t;
                        });
                    } else if (t == 0 && (allscore % allqstnum > 0)) {
                        for (var i = 0; i < allscore % allqstnum; i++) {
                            qstlist[i].scores = (+qstlist[i].scores) + 1;
                        }
                        break;
                    } else {
                        break;
                    }
                    allscore = allscore - allqstnum * t;
                }
                return qstlist;
            }

            $scope.goExamCenter = function() {
                $state.go('myApp.examCenter');
            };
            $scope.changeAllOrErr = function (flag) {
                if(flag+'' === '1'){
                    $scope.isShowAll = false;
                }else{
                    $scope.isShowAll = true;
                }
            };
            /**
             * 监听班级下拉列表
             */
            $scope.$watch('selectClass',function (newVal,oldVal) {
                if(newVal != oldVal && !!newVal){
                    var selectIndex = _.findIndex($scope.classData,function (item) {
                        return _.isEqual(item,newVal);
                    });
                    var aimIndex = setLoactionByClassIndex($scope.stuData,$scope.stuData[selectIndex][0]);
                    if(aimIndex >= 0){
                        if(aimIndex > $scope.data.currentStuList.length-1){
                            for(;aimIndex > $scope.data.currentStuList.length-1;){
                                $scope.abnormalLoad();
                            }
                        }
                        scrollToStu(aimIndex);
                        $scope.data.currentStu = $scope.data.currentStuList[aimIndex];
                        constantService.alert('当前位置：' + newVal.ClassName);
                    }else{
                        constantService.alert('对不起，这个班可能没数据哦!')
                    }
                }
            });

            /**
             * 找到所选学生在整个学生数组中的索引
             * @param twoDArr
             * @param goalItem
             * @returns {*}
             */
            function setLoactionByClassIndex(twoDArr,goalItem) {
                $scope.isShowAll = true;
                var tempArr = [];
                for(var i = 0; i < twoDArr.length; i++){
                    for(var j = 0; j < twoDArr[i].length; j++){
                        tempArr.push(twoDArr[i][j]);
                        if(typeof goalItem === 'string'){
                            if(twoDArr[i][j].studentName === goalItem){
                                return tempArr.length - 1;
                            }
                        }else{
                            if(_.isEqual(twoDArr[i][j],goalItem)){
                                return tempArr.length-1;
                            }
                        }
                    }
                }
                return '';
            }

            /**
             * 滚动到指定学生位置
             * @param index
             */
            function scrollToStu(index) {
                var $keyBoards = $('.keyboards');
                var boxOffset = $keyBoards.offset().top,
                    boxHeight = $keyBoards.height();
                setTimeout(function () {
                    $('.stu-answer-box').scrollTop(0);
                    if ($('#' + index).offset()) {
                        var keyOffset = $('#' + index).offset().top + $keyBoards.scrollTop();
                        $keyBoards.scrollTop(keyOffset - boxHeight / 2 - boxOffset);
                    }
                }, 100);
            }

            $scope.startSearch = function () {
                if(verifyKey($scope.searchKey)){
                    var aimIndex = setLoactionByClassIndex($scope.stuData,$scope.searchKey);
                    if(aimIndex >= 0){
                        if(aimIndex > $scope.data.currentStuList.length-1){
                            for(;aimIndex > $scope.data.currentStuList.length-1;){
                                $scope.abnormalLoad();
                            }
                        }
                        $scope.data.currentStu = $scope.data.currentStuList[aimIndex];
                        scrollToStu(aimIndex);
                    }else{
                        constantService.alert('对不起，已经拼命在搜索了但还是没找到!');
                    }
                }else{
                    constantService.alert('请重新输入合适的关键字进行搜索！')
                }
            };

            /**
             * 判断搜索关键字合法性
             * @param arg
             */
            function verifyKey(arg){
                var str = $.trim(arg);
                if(str.length!=0 && str.length <= 20){
                    reg=/^[\u0391-\uFFE5]+$/;
                    if(!reg.test(str)){
                        return false;
                    }else{
                        return true;
                    }
                }else{
                    return false;
                }
            }

        }]
});