define(['underscore-min', 'jquerymedia'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', 'constantService',
        function ($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce, constantService) {
            var searchfrom = $location.search();
            var unifiedId = searchfrom.unifiedId,
                examfid = searchfrom.examfid,
                marktype = searchfrom.marktype,
                gradeNum = searchfrom.gradeNum,
                status = searchfrom.status;
            // 获取失效试卷
            function getUnbindPaper(){
                $http.post($rootScope.baseUrl + '/generalQuery.ashx',{
                    UnifiedItemFId:unifiedId,
                    Proc_name :'Proc_GetUnifiedTasks'
                }).success(function (res) {
                    if(res.msg.length === 0){
                        $scope.YesRelated = [];
                    }else{
                        $scope.TaskFId = res.msg[0].FLnkID;
                        $scope.classData = res.msg;
                        getAbnormalPaper();
                        $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                            taskFid :$scope.TaskFId
                        }).then(function(res){
                            if(_.isArray(res.data.msg)){
                                $scope.fileList = res.data.msg;
                                $scope.noRelatedList = [];
                                _.each($scope.fileList,function (resq) {
                                    if(resq.isNoRelated === 0 || resq.isNoRelated === 2){
                                        $scope.noRelatedList.push(resq);
                                    }
                                });
                            }
                        });
                    }
                });
            }
            getUnbindPaper();

            //获取题目异常试卷
            function getAbnormalPaper() {
                if(marktype == 1){
                    $scope.currentStuList =[];
                }else{
                    var newFidList = [];
                    _.each($scope.classData, function (item) {
                        newFidList.push(getAbnormalPapers(item,examfid));
                    });
                    if(newFidList.length> 0){
                        $scope.currentStuList =[];
                        $q.all(newFidList).then(function (resq) {
                            _.each(resq,function (res) {
                                _.each(res.data.studentList,function (data) {
                                    if(data.type == -1){
                                        $scope.currentStuList.push(data);
                                    }
                                })
                            });
                        });
                    }
                }
            }
            function getAbnormalPapers(classId,examfid) {
                var defer = $q.defer();
                $http.post($rootScope.baseUrl + '/Interface0045A.ashx', {
                    classId: classId.ClassFLnkID,
                    examFLnkId: examfid
                }).then(function (res) {
                    defer.resolve(res);
                }, function (res) {
                    defer.reject();
                });
                return defer.promise;
            }

            //批阅
            $scope.goMarking = function () {
              //todo  批阅跳转
                //判断是线上还是线下
                // if(marktype == 1){
                //     window.open('#/encryptmarking?examId='+examfid+'&unifiedId='+unifiedId + '&isOpenNewWin=' + 1);
                // }else {
                //     if($scope.currentStuList.length === 0){
                //         constantService.alert('暂无需要批阅的数据')
                //     }else{
                //         $state.go('abnormal',{
                //             ExamFlnkID:examfid,
                //             TaskFId:$scope.TaskFId,
                //             UnifiedItemFid:unifiedId,
                //             gradeNum:gradeNum,
                //             marktype:marktype,
                //             status:status
                //         });
                //     }
                // }
            };
            //试卷关联
            $scope.goRelated = function () {
                $state.go('myApp.setRelated', {
                    unifiedId: unifiedId,
                    taskFid :$scope.TaskFId,
                    marktype:marktype,
                    examfid: examfid,
                    gradeNum:gradeNum,
                    status:status
                })
            }
        }]
});