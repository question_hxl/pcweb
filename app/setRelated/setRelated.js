/**
 * Created by clc on 2016/11/2.
 */
define(['underscore-min', 'jquerymedia'], function(){
    return ['$http', '$scope', '$rootScope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', 'constantService',
        function($http, $scope, $rootScope, $state, $location, $q, ngDialog, $timeout, $sce, constantService){
            $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
            var searchfrom = $location.search();
            var unifiedId = searchfrom.unifiedId,
                taskFid = searchfrom.taskFid,
                examfid = searchfrom.examfid,
                marktype = searchfrom.marktype,
                gradeNum = searchfrom.gradeNum;
            $scope.status = searchfrom.status;
            $scope.clasid = searchfrom.ClassFlnkid;
            $scope.UserFLnkID = searchfrom.UserFLnkID;
            $scope.userFId = '';
            $scope.exafId = '';
            $scope.currentIndex = 0;
            $scope.ClassName = '';
            $scope.currentShowType = 1;
            $scope.NoRelated = [];
            $scope.YesRelated = [];
            $scope.AddRelated = [];
            $scope.currentClassId = '';
            $scope.queueList = [
                {name: '首页',url:'myApp.masterHome'},
                {name: '考试中心',url:'myApp.examCenter'},
                {name: '试卷纠错',url:' '}];
            var oldPaperList = [],
                newPaperList = [];
            //返回
            $scope.back = function () {
                $state.go('myApp.examCenter');
            };
            //试卷异常
            $scope.paperError = function () {
                $scope.currentShowType =1;
            };

            //获取题目异常的试卷
            $scope.quesError = function () {
                if(marktype == 1){
                    constantService.alert('线上阅卷无需题目异常纠错!')
                }else{
                    //if($scope.currentStuList.length > 0){//有题目异常的，跳转
                        $state.go('abnormal',{
                            ExamFlnkID:examfid,
                            TaskFId:taskFid,
                            UnifiedItemFid:unifiedId,
                            gradeNum:gradeNum,
                            marktype:marktype,
                            status:$scope.status
                        });
                        ngDialog.close();
                    /*}else{//没有给提示
                        constantService.alert('暂无题目异常的试卷需要处理，请直接点击下一步!')
                    }*/
                }
                ngDialog.close();
            };
            //获取题目异常试卷
            function getAbnormalPaper() {
                if(marktype == 1){
                    $scope.currentStuList =[];
                }else{
                    var newFidList = [];
                    _.each($scope.classData, function (item) {
                        newFidList.push(getAbnormalPapers(item,examfid));
                    });
                    if(newFidList.length> 0){
                        $scope.currentStuList =[];
                        $q.all(newFidList).then(function (resq) {
                            _.each(resq,function (res) {
                                _.each(res.data.studentList,function (data) {
                                    if(data.type == -1){
                                        $scope.currentStuList.push(data);
                                    }
                                })
                            });
                        });
                    }
                }
            }
            function getAbnormalPapers(classId,examfid) {
                var defer = $q.defer();
                $http.post($rootScope.baseUrl + '/Interface0045A.ashx', {
                    classId: classId.ClassFLnkID,
                    examFLnkId: examfid
                }).then(function (res) {
                    defer.resolve(res);
                }, function (res) {
                    defer.reject();
                });
                return defer.promise;
            }

            //班级选择
            $http.post($rootScope.baseUrl + '/generalQuery.ashx',{
                UnifiedItemFId:unifiedId,
                Proc_name :'Proc_GetUnifiedTasks'
            }).success(function (res) {
                if(res.msg && _.isArray(res.msg)){
                    $scope.classData = res.msg;
                    $scope.currentClass = res.msg[0];
                    $scope.currentClassId = $scope.currentClass.ClassFLnkID;
                    getAbnormalPaper();
                    getAllStudentList();
                }
            });

            //监听班级联动学生
            $scope.$watch('currentClassId', function(newValue,oldValue){
                if(!newValue || newValue === oldValue){
                    return;
                }
                $scope.getStudentList();
            });
            $scope.$watch('currentClassIdForStudent', function(newValue,oldValue){
                if(!newValue || newValue === oldValue){
                    return;
                }
                $http.post('BootStrap/Interface/generalQuery.ashx',{
                    classFId: $scope.currentClassIdForStudent,
                    examid: examfid,
                    Proc_name: 'GetMyClassBindPaper'
                }).then(function(res){
                    if(_.isArray(res.data.msg)){
                        $scope.BindPaperList = res.data.msg;
                        $scope.currentFile = $scope.BindPaperList[0];
                    } else {
                        $scope.BindPaperList = [];
                    }
                });
            });
            $scope.changeRelated = function () {
                $scope.StatusRelated = !$scope.StatusRelated;
                $scope.currentClassIdForStudent = $scope.classData[0].ClassFLnkID;
                if($scope.StatusRelated) {
                    $scope.currentFile = $scope.BindPaperList ? $scope.BindPaperList[0] : {};
                } else {
                    getUnbindPaper()
                }
            };
            $scope.unbindPaper = function(){
                $http.post('BootStrap/Interface/unbindAnswer.ashx',{
                    exanFid: $scope.currentFile.exaFId
                }).then(function(res){
                    if(res.data.code === 0){
                        var r = _.findIndex($scope.BindPaperList, {exaFId: $scope.currentFile.exaFId});
                        $scope.BindPaperList.splice(r, 1);
                        $scope.currentFile = $scope.BindPaperList[(r === $scope.BindPaperList.length ? (r - 1) : r)];
                        //只是为了增加$scope.nweFile长度；'add'无任何意义。
                        $scope.nweFile.push({
                            add: '1'
                        });
                        constantService.alert('解绑成功！', function () {
                            if(+$scope.status > 4) {
                                $http.post('' + '/BootStrap/schoolmanager/updateUnifiedItemStep.ashx', {//设置子统考
                                    unifiedItemId:unifiedId,
                                    step: '4'
                                }).then(function (res) {
                                    if (res.data.code === 0) {
                                        $scope.status = 4;
                                    } else {
                                        constantService.alert(res.data.msg);
                                    }
                                });
                            }
                        });
                    } else {
                        $scope.BindPaperList = [];
                    }
                })
            };

            //获取学生列表
            $scope.getStudentList = function(){
                $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    classFId : $scope.currentClassId,
                    examFid : examfid,
                    Proc_name : 'GetStudentUnbindPaper'
                }).success(function(data){
                    if(data && data.msg && _.isArray(data.msg)){
                        $scope.studentData = data.msg;
                        $scope.userFId = $scope.studentData.userFId;
                        $scope.studentUnbind = [];
                        $scope.studentNo=[];
                        $scope.studentAll = [];
                        _.filter($scope.studentData,function(item){
                            if(item.isExit === 0){
                                $scope.studentNo.push(item);
                                if($scope.studentNo.length > 0){
                                    studentSort($scope.studentNo)
                                }
                            }else{
                                $scope.studentAll.push(item);
                                if($scope.studentAll.length > 0){
                                    studentSort($scope.studentAll)
                                }
                            }
                        });
                        $scope.studentUnbind = $scope.studentNo.concat($scope.studentAll);
                        $scope.currentStu = _.find($scope.studentUnbind,function(user){
                            return $scope.UserFLnkID === user.userFId
                        });
                    }
                });
            };
            //学生按学号排序
            function studentSort(stu) {
                stu.sort(function (a, b) {
                    return +a.userNo - +b.userNo;
                });
            }

            //统一获取所有学生列表
            function getAllStudentList() {
                var newFidList = [];
                var classIdList = [];
                $scope.allStudentList = [];
                _.each($scope.classData, function (item) {
                    newFidList.push(getAllStu(item.ClassFLnkID,examfid));
                    classIdList.push(item.ClassName);
                });
                if(newFidList.length> 0){
                    $q.all(newFidList).then(function (resq) {
                        _.each(resq,function (item, index) {
                            var classId = classIdList[index];
                            studentSort(item.data.msg);
                            _.each(item.data.msg,function (stu) {
                                if(stu.isExit === 0){
                                    stu.classId = classId;
                                    $scope.allStudentList.push(stu);
                                }
                            })
                        });
                    })
                }
            }
            function getAllStu(classId,examfid) {
                var defer = $q.defer();
                $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    classFId : classId,
                    examFid : examfid,
                    Proc_name : 'GetStudentUnbindPaper'
                }).then(function (res) {
                    defer.resolve(res);
                }, function (res) {
                    defer.reject();
                });
                return defer.promise;
            }

            // 获取失效试卷
            function getUnbindPaper(){
                if(taskFid === unifiedId){
                    $scope.YesRelated = [];
                }else{
                    $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                        taskFid :taskFid
                    }).then(function(res){
                        if(_.isArray(res.data.msg)){
                            $scope.fileList = res.data.msg;
                            $scope.nweFile = [];
                            $scope.NoRelated = [];
                            $scope.YesRelated = [];
                            $scope.AddRelated = [];
                            _.each($scope.fileList,function (resq) {
                                if(resq.isNoRelated === 1){
                                    $scope.NoRelated.push(resq);
                                }else if(resq.isNoRelated === 2){
                                    $scope.AddRelated.push(resq);
                                }else if(resq.isNoRelated === 0){
                                    $scope.YesRelated.push(resq);
                                }
                            });
                            $scope.nweFile = $scope.YesRelated.concat($scope.AddRelated);
                            $scope.fileList = $scope.nweFile.concat($scope.NoRelated);
                            $scope.currentFile = $scope.fileList && $scope.fileList[0];
                            getWidth($scope.currentFile);
                            newPaperList = $scope.nweFile;
                            for(i=0;i<newPaperList.length;i++){
                                oldPaperList[i] = {
                                    exaFId:newPaperList[i].exaFId,
                                    fileNo:newPaperList[i].fileNo,
                                    jpg:newPaperList[i].jpg,
                                    isNoRelated:newPaperList[i].isNoRelated
                                }
                            }
                        } else {
                            $scope.fileList = [];
                            $scope.nweFile = [];
                        }
                    });
                }
            }
            getUnbindPaper();
            function getWidth(item) {
                if (item.jpg) {
                    isImgLoaded(item.jpg).then(function ($el) {
                        $($el).appendTo($('body'));
                        item.jpgWidth = $($el).width();
                        item.jpgHeight = $($el).height();
                        $('.relatedPaper-pdf').css('height', $('.relatedPaper-peple').height() + $($el).height() + 20);
                        $($el).remove();
                    })
                }
            }
            function isImgLoaded(url) {
                var defer = $q.defer();
                var $img = new Image();
                $img.src = url;
                $img.onload = function () {
                    setTimeout(function () {
                        defer.resolve($img);
                    }, 100);
                };
                $img.onerror = function () {
                    setTimeout(function () {
                        defer.reject(1);
                    }, 100);
                };
                if ($img.complete) {
                    setTimeout(function () {
                        defer.resolve($img);
                    }, 100);
                }
                return defer.promise;
            }
            //点击获取焦点
            $scope.clickfile = function(index, type){
                $scope.currentIndex = index;
                if(type === 1){
                    $scope.currentFile = $scope.fileList[$scope.currentIndex];
                } else {
                    $scope.currentFile = $scope.BindPaperList[$scope.currentIndex];
                }
                getWidth($scope.currentFile);
            };

            //试卷关联
            $scope.relatedPaper = function(){
                if($scope.stu === undefined){//所有学生没有选择按之前步骤走
                    if($scope.ClassName != null && $scope.currentUserFId){
                        getUPDATEExAnswerUserFId($scope.currentUserFId,examfid,$scope.currentFile.exaFId);
                    } else {
                        constantService.alert('请选择需要关联的班级和学生！');
                    }
                }else{//所有学生选择
                    getUPDATEExAnswerUserFId($scope.stu,examfid,$scope.currentFile.exaFId);
                }
            };
            function getUPDATEExAnswerUserFId(userFId,examFId,exaFId) {
                $http.post('BootStrap/EncryptUnified/UPDATEExAnswerUserFId.ashx', {
                    userFId : userFId,
                    examFId : examFId,
                    exaFId : exaFId
                }).then(function(res){
                    if(res.data.code === 0){
                        $scope.getStudentList();
                        $scope.stu = undefined;
                        getAllStudentList();
                        $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                            taskFid :taskFid
                        }).then(function(res){
                            if(_.isArray(res.data.msg)){
                                $scope.fileList = res.data.msg;
                                $scope.nweFile = [];
                                $scope.NoRelated = [];
                                $scope.YesRelated = [];
                                $scope.AddRelated = [];
                                _.each($scope.fileList,function (resq) {
                                    if(resq.isNoRelated === 1){
                                        $scope.NoRelated.push(resq);
                                    }else if(resq.isNoRelated === 2){
                                        $scope.AddRelated.push(resq);
                                    }else if(resq.isNoRelated === 0){
                                        $scope.YesRelated.push(resq);
                                    }
                                });
                                $scope.nweFile  = $scope.YesRelated.concat($scope.AddRelated);
                                $scope.fileList = $scope.nweFile .concat($scope.NoRelated);
                                newPaperList = $scope.nweFile;
                                if($scope.fileList.length<=$scope.currentIndex){
                                    $scope.currentFile = $scope.fileList[$scope.currentIndex-1];
                                    $scope.currentIndex = $scope.currentIndex-1;
                                }else if($scope.fileList.length>$scope.currentIndex){
                                    $scope.currentFile = $scope.fileList[$scope.currentIndex];
                                }
                                else{
                                    $scope.currentFile = $scope.fileList[0];
                                }
                                getWidth($scope.currentFile);
                                constantService.alert('绑定成功！',function () {
                                    if($scope.nweFile.length === 0){
                                        $scope.autoNextMarking();
                                    }
                                });
                                $scope.currentUserFId = '';
                            } else {
                                $scope.fileList = [];
                                $scope.nweFile = [];
                                if($scope.nweFile.length === 0){
                                    $scope.autoNextMarking();
                                }
                            }
                        }, function(res){
                            constantService.alert('数据刷新失败！');
                        });
                    }else {
                        constantService.alert('绑定失败！');
                    }
                }, function(res){
                    constantService.alert('绑定失败！');
                });
            }
            //无法关联
            $scope.unRelated = function () {
                constantService.confirm('提示', '确定是否为无法关联的试卷', ['确定', '取消'], function () {
                    $scope.yes();
                }, function () {
                    ngDialog.closeAll();
                });
                $scope.yes = function () {
                    ngDialog.close();
                    $scope.nweFile = [];
                    $scope.NoRelated = [];
                    $scope.YesRelated = [];
                    $scope.AddRelated = [];
                    $http.post('BootStrap/EncryptUnified/RemarkAnswerNobody.ashx',{
                        answerFid:$scope.currentFile.exaFId
                    }).then(function (resq) {
                        if(resq.data.code === 0){
                            $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                                taskFid :taskFid
                            }).then(function(res){
                                if(_.isArray(res.data.msg)){
                                    $scope.fileList = res.data.msg;
                                    _.each($scope.fileList,function (res) {
                                        if(res.exaFId === $scope.currentFile.exaFId){
                                            res.isNoRelated = 1
                                        }
                                        if(res.isNoRelated === 1){
                                            $scope.NoRelated.push(res);
                                        }else if(res.isNoRelated === 2){
                                            $scope.AddRelated.push(res);
                                        }else if(res.isNoRelated === 0){
                                            $scope.YesRelated.push(res);
                                        }
                                    });
                                    $scope.nweFile  = $scope.YesRelated.concat($scope.AddRelated);
                                    $scope.fileList = $scope.nweFile .concat($scope.NoRelated);
                                    newPaperList = $scope.nweFile;
                                    if($scope.fileList.length<=$scope.currentIndex){
                                        $scope.currentFile = $scope.fileList[$scope.currentIndex-1];
                                        $scope.currentIndex = $scope.currentIndex-1;
                                    }else if($scope.fileList.length>$scope.currentIndex){
                                        $scope.currentFile = $scope.fileList[$scope.currentIndex];
                                    }
                                    else{
                                        $scope.currentFile = $scope.fileList[0];
                                    }
                                    getWidth($scope.currentFile);
                                    constantService.alert('修改成功！',function () {
                                        if($scope.nweFile.length === 0){
                                            $scope.autoNextMarking();
                                        }
                                    });
                                } else {
                                    $scope.fileList = [];
                                    $scope.nweFile = [];
                                    if($scope.nweFile.length === 0){
                                        $scope.autoNextMarking();
                                    }
                                }
                            })
                        }else{
                            constantService.alert('修改失败！');
                        }
                    },function () {
                        constantService.alert('修改失败！');
                    });
                }
            };
            //全部忽略
            $scope.unRelatedAll = function () {
                constantService.confirm('提示', '确定是否取消所有未关联的试卷', ['确定', '取消'], function () {
                    $scope.yes();
                }, function () {
                    ngDialog.closeAll();
                });
                function setRemarkAnswerNobody(params) {
                    var defer = $q.defer();
                    $http.post('BootStrap/EncryptUnified/RemarkAnswerNobody.ashx',{
                        answerFid:params
                    }).then(function (res) {
                        defer.resolve(res);
                    },function (res) {
                        defer.reject();
                    });
                    return defer.promise;
                }
                $scope.yes = function () {
                    ngDialog.close();
                    $scope.NoRelated = [];
                    $scope.YesRelated = [];
                    $scope.AddRelated = [];
                    var newFidList = [];
                    _.each($scope.fileList,function (item) {
                        newFidList.push(setRemarkAnswerNobody(item.exaFId));
                    });
                    if(newFidList.length>0){
                        $q.all(newFidList).then(function (resq) {
                            $http.post('BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx', {
                                taskFid :taskFid
                            }).then(function(res){
                                if(_.isArray(res.data.msg)){
                                    $scope.fileList = res.data.msg;
                                    _.each($scope.fileList,function (res) {
                                        res.isNoRelated = 1;
                                    });
                                    $scope.NoRelated = $scope.fileList;
                                    $scope.nweFile = [];
                                    newPaperList = $scope.nweFile;
                                    constantService.alert('修改成功！',function () {
                                        if($scope.nweFile.length === 0){
                                            $scope.autoNextMarking();
                                        }
                                    });
                                } else {
                                    $scope.fileList = [];
                                    $scope.nweFile = [];
                                    if($scope.nweFile.length === 0){
                                        $scope.autoNextMarking();
                                    }
                                }
                            },function () {
                                constantService.alert('修改失败！');
                            })
                        });
                    }
                }
            };

            //下一步
            $scope.nextMarking = function () {
                $scope.allPaper = [];
                if(+marktype === 1){
                    constantService.confirm('提示', ($scope.nweFile.length ? '还有' + $scope.nweFile.length + '份待纠错试卷，请确认是否下一步' : '暂无需要关联的试卷，请直接点击下一步'), ['下一步', '取消'], function () {
                        $scope.goNext();
                    }, function () {
                        ngDialog.closeAll();
                    });
                }else{
                    $scope.allPaper = $scope.nweFile.length + $scope.currentStuList.length;
                    if($scope.currentStuList.length === 0){
                        constantService.confirm('提示', (+$scope.allPaper === 0 ? '暂无待题目异常的试卷，请直接点击下一步' : '')+($scope.nweFile.length ? '还有' + $scope.nweFile.length + '份待纠错试卷，请确认是否下一步' : ''), ['下一步', '取消'], function () {
                            $scope.goNext();
                        }, function () {
                            ngDialog.closeAll();
                        });
                        // ngDialog.open({
                        //     template:'<div class="batchScore">'+' <div class="batchScore-nav"></div>'+
                        //     '<div class="batchScore-title" ng-show="allPaper === 0">暂无待题目异常的试卷，请直接点击下一步</div>'+
                        //     '<div class="batchScore-title" ng-show="nweFile.length > 0 ">还有{{nweFile.length}}份待纠错试卷，请确认是否下一步</div>'+
                        //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin:20px 20px 0 116px" >取消</div>'+
                        //     '<div class="batchScore-btn" ng-click="goNext()" style="margin-top:20px">下一步</div>'
                        //     +'</div>'+'</div>',
                        //     className: 'ngdialog-theme-default ngdialog-setValue',
                        //     scope: $scope,
                        //     closeByDocument: false,
                        //     plain: true
                        // });
                    }else{
                        $scope.quesError();
                        // ngDialog.open({
                        //     template:'<div class="batchScore">'+' <div class="batchScore-nav"></div>'+
                        //     '<div class="batchScore-title" ng-show="currentStuList.length > 0 ">还有{{currentStuList.length}}份待纠错试卷，是否处理</div>'+
                        //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin:20px 20px 0 116px" >取消</div>'+
                        //     '<div class="batchScore-btn" ng-click="quesError()" style="margin-top:20px">确定</div>'
                        //     +'</div>'+'</div>',
                        //     className: 'ngdialog-theme-default  ngdialog-setValue',
                        //     scope: $scope,
                        //     closeByDocument: false,
                        //     plain: true
                        // });
                    }
                }

                $scope.goNext = function () {
                    $http.post('' + '/BootStrap/schoolmanager/updateUnifiedItemStep.ashx', {//设置子统考
                        unifiedItemId:unifiedId,
                        step: '5'
                    }).then(function (res) {
                        if (res.data.code === 0) {
                            constantService.alert('试卷纠错完成，即将前往考试中心', function(){
                                $state.go('myApp.examCenter');
                            });
                        } else {
                            constantService.alert(res.data.msg);
                        }
                    });
                    ngDialog.close();
                }
            };
            //自动下一步
            $scope.autoNextMarking = function(){
                if(+marktype === 1){
                    constantService.confirm('提示','暂无待关联的试卷，请直接点击下一步', ['下一步', '取消'], function () {
                        $scope.autoGoNext();
                    }, function () {
                        ngDialog.closeAll();
                    });
                }else{
                    constantService.confirm('提示',($scope.currentStuList.length ? '还有' + $scope.currentStuList.length + '份题目异常的试卷，是否处理': '暂无待关联及题目异常的试卷，请直接点击下一步'),
                        [($scope.currentStuList.length ? '确定' : '下一步'), '取消'], function () {
                        if($scope.currentStuList.length) {
                            $scope.quesError();
                        }else {
                            $scope.autoGoNext();
                        }
                    }, function () {
                        ngDialog.closeAll();
                    });
                    // ngDialog.open({
                    //     template:'<div class="batchScore">'+' <div class="batchScore-nav"></div>'+
                    //     '<div class="batchScore-title" ng-show="currentStuList.length == 0">暂无待关联及题目异常的试卷，请直接点击下一步</div>'+
                    //     '<div class="batchScore-title" ng-show="currentStuList.length > 0">还有{{currentStuList.length}}份题目异常的试卷，是否处理</div>'+
                    //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin:20px 20px 0 116px" >取消</div>'+
                    //     '<div class="nextMarking-btn" style="margin-top:20px" ng-click="autoGoNext()" ng-show="currentStuList.length === 0">下一步</div>'+
                    //     '<div class="batchScore-btn" ng-click="quesError()" style="margin-top:20px" ng-show="currentStuList.length > 0">确定</div>'
                    //     +'</div>'+'</div>',
                    //     className: 'ngdialog-theme-default ngdialog-setValue',
                    //     scope: $scope,
                    //     closeByDocument: false,
                    //     plain: true
                    // });
                }

                $scope.autoGoNext = function () {
                    $http.post('' + '/BootStrap/schoolmanager/updateUnifiedItemStep.ashx', {//设置子统考
                        unifiedItemId:unifiedId,
                        step: '5'
                    }).then(function (res) {
                        if (res.data.code === 0) {
                            $state.go('myApp.examCenter');
                        } else {
                            constantService.alert(res.data.msg);
                        }
                    });
                    ngDialog.close();
                }
            };
        }]
});