define(['../../config/apiBaseUrlConfig'], function(urlBase){
    var apiUrlMap = {
        'common': urlBase.baseUrl,
        'resource': urlBase.resourceUrl,
        'transfer': urlBase.transferServer
    };
    function getUrl(url, type){
        type = type || 'common';
        return apiUrlMap[type] + url;
    }
    return {
        //登录接口地址
        LOGIN: getUrl('/Interface0001.ashx'),
        //ckeditor文件上传接口地址
        FILE_UPLOAD_4_CK: getUrl('/Interface0001A.ashx'),
        //通用图片上传接口地址
        IMG_UPLOAD_COMMON: getUrl('/Interface0001C.ashx'),
        //检测当前用户登录信息
        CHECK_LOGIN: getUrl('/Interface0001E.ashx'),
        //九龙中学接入登录
        JL_LOGIN: getUrl('/Interface0001JL.ashx'),
        //注销登录
        LOGOUT: getUrl('/Interface0114.ashx'),
        //获取用户关联帐号接口
        GET_ASSOCIATE_ACCOUNT: getUrl('/Interface0299.ashx'),
        //导出HTML内容到PDF文件接口
        EXPORT_HTML_TO_PDF: getUrl('/api/File/Html2PDF', 'transfer'),
        //通用查询
        GENERAL_QUERY: getUrl('/generalQuery.ashx'),
        // 获取题目异常的试卷
        GET_ABNORMAL_PAPERS: getUrl('/Interface0045A.ashx'),
        //初始化班级
        CLASS_SIMPLE_DETAIL: getUrl('/Interface0150.ashx'),
        //试卷列表
        EXAM_LIST: getUrl('/Interface0156.ashx'),
        //班级年级对比
        CLASS_GRADE_CONTRAST: getUrl('/Interface0158.ashx'),
        //得分排名
        SCORE_RANKING: getUrl('/Interface0159.ashx'),
        //成绩分布
        SCORE_DISTRIBUTION: getUrl('/Interface0160.ashx'),
        //错题列表
        ERROR_QUESTION: getUrl('/Interface0161B.ashx'),
        //试卷详细信息
        EXAM_DETAIL: getUrl('/Interface0169.ashx'),
        // 试卷类型列表
        EXAM_TYPE: getUrl('/Interface0196.ashx'),
        // 修改客观题答案
        RESET_ANSWER_MARK_RECORD: getUrl('/Interface0206.ashx'),
        //获取知识点列表
        KNOWLEDGE_LIST: getUrl('/Interface0213.ashx'),
        //获取学科列表
        SUBJECT_LIST: getUrl('/Interface0220B.ashx'),
        //试卷审核
        PAPER_CHECK: getUrl('/Interface0224A.ashx'),
        //获取出版社与教材列表
        PUBLISH_GRADE_LIST: getUrl('/Interface0281.ashx'),
        //获取学生列表
        STUDENT_LIST: getUrl('/Interface0233.ashx'),
        //获取班级列表
        CLASS_LIST: getUrl('/Interface0230.ashx'),
        //获取角色列表
        ROLE_LIST: getUrl('/BaseUser/GetBaseRoleList', 'resource'),
        //删除用户
        DELETE_USER: getUrl('//BaseUser/DeleteBaseUser', 'resource'),
        //重置密码
        RESET_PASSWORD: getUrl('/BaseUser/ResetUserPassword', 'resource'),
        //获取老师列表
        USER_LIST: getUrl('/BaseUser/GetUserListPageView', 'resource'),
        //获取除老师学生其他角色列表
        MASTER_LIST: getUrl('/BaseUser/GetMasterListPageView', 'resource'),
        //批量修改学生资料——改班级
        UPDATE_STUDENT_CLASS: getUrl('/BaseUser/UpdateStudentClass', 'resource'),
        //导入老师数据检测
        // UPLOAD_TEACHER_FILE: getUrl('/File/UploadTeacherFile', 'resource'),
        //老师数据导入——确认导入。
        SURE_IMPORT_TEACHER_LIST: getUrl('/File/SureImportTeacherList', 'resource'),
        //学生数据导入——确认导入。
        SURE_IMPORT_STUDENT_LIST: getUrl('/File/SureImportStudentList', 'resource'),
        //学生详细信息。
        GET_SINGL_EBASE_USER: getUrl('/BaseUser/GetSingleBaseUser', 'resource'),
        //修改学生资料——保存接口
        UPDATE_BASE_USER_BY_STUDENT: getUrl('/BaseUser/UpdateBaseUserByStudent', 'resource'),
        //新增学生资料——保存接口
        INSERT_BASE_USER_BY_STUDENT:getUrl('/BaseUser/InsertBaseUserByStudent', 'resource'),
        //获取班级详细信息
        CLASS_VIEW_DETAIL: getUrl('/BaseClass/GetSingerBaseClassListPageViewDetail', 'resource'),
        //删除班级
        DELETE_SURE_BASE_CLASS: getUrl('/BaseClass/DeleteSureBaseClass', 'resource'),

        USER_LIST_VIEW: getUrl('/BaseUser/GetPageUserListPageView', 'resource'),
        //班级类型：文、理
        CLASS_TYPE_LIST: getUrl('/BaseClass/GetBaseClassTypeList', 'resource'),
        //修改班级信息——保存接口
        UPDATE_CLASS_MASTER: getUrl('/BaseClass/UpdateBaseClassMaster', 'resource'),
        //新增班级——保存接口
        INSERT_CLASS_MASTER: getUrl('/BaseClass/InsertBaseClassMaster', 'resource'),
        //查询学校升级
        GET_SCHOOL_UP_STATUS: '/BootStrap/schoolmanager/getSchoolUpStatus.ashx',
        //学校升级
        SCHOOL_UP: '/BootStrap/schoolmanager/schoolUp.ashx',
        //获取批阅信息
        GET_ENCRYPT_QLIST: '/BootStrap/EncryptUnified/WJFGetEncryptQList.ashx',
        //重置批阅结果
        RESET_QUESTION_MARK_RECORD: '/BootStrap/schoolmanager/resetQuestionMarkRecord.ashx',
        //设置分值 保存页面修改。
        UPDATE_PAPER_CONFIG: '/BootStrap/schoolmanager/updatePaperConfig.ashx',
        //设置子统考
        UPDATE_UNIFIED_ITEM_STEP: '/BootStrap/schoolmanager/updateUnifiedItemStep.ashx',
        //获取试卷
        GET_PAPER_CONFIG: '/BootStrap/EncryptUnified/getPaperConfig.ashx',
        //获取老师详细信息
        GET_TEACHER_DETAIL_VIEW: getUrl('/BaseUser/GetTeacherDetailView', 'resource'),
        //根据教材获取年级
        GET_GRADE_NUM: getUrl('/BaseUser/GetGradeNum', 'resource'),
        //修改老师资料。——保存接口
        UPDATE_BASE_TEACHER: getUrl('/BaseUser/UpdateBaseUserByTeacher', 'resource'),
        // 年级过滤学科
        SUBJECT_NAME_BY_GRADENUM: getUrl('/Subject/GetSubjectNameByGradeNum', 'resource'),
        //根据年级或学科查询老师
        GET_UNIFIED_ALLOW_MANAGER: '/BootStrap/schoolmanager/getUnifiedAllowManager.ashx',
        //修改父统考信息
        MODIFY_PARENT_UNIFIED_NAME: '/BootStrap/schoolmanager/modifyIntegrateUnifedName.ashx',
        //提交分析
        TASKS_ANALYSIS: getUrl('/tasksAnalysis.ashx'),
        //发送微信通知
        SEND_WX_MESSAGE: getUrl('/sendWXMessage.ashx'),
        //取消统考
        SPLIT_UNIFIED_ITEM: getUrl('/KmUnified/SplitPaKmUnifiedItem', 'resource'),
        //合并统考
        MERGE_UNIFIED_ITEM: getUrl('/KmUnified/MergePaKmUnifiedItem', 'resource'),
        //删除父统考
        DELETE_UNIFIED: getUrl('/KmUnified/DeleteKmUnifiedByFLnkId', 'resource'),
        //删除子统考
        DELETE_UNIFIED_ITEM: getUrl('/KmUnified/DeleteKmUnifiedItemByFLnkId', 'resource'),
        //获取带纠错考卷
        GET_NOBODY_EXANSWER: 'BootStrap/EncryptUnified/GetNoBodyExAnswer.ashx',




    };
});