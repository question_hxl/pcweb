/**
 * 统一接口调用处理
 * @module apiCommon
 */
define(['service/api/reqUrlMap'], function (urlMap) {
    angular.module('api.common', [
        'backEndService'
    ])
        .service('apiCommon', function ($q, $http, bes, baseUrl) {
            return {
                /**
                 * 主动登录接口
                 * @param params  登录参数，包括用户名，密码，是否自动登录
                 * @returns {*}
                 */
                login: function (params) {
                    return bes.post({
                        url: urlMap.LOGIN,
                        data: {
                            auto: params.auto,
                            ipaddress: '192.168.1.1',
                            psw: params.psw,
                            username: params.username
                        }
                    });
                },
                /**
                 * 模拟登陆接口
                 * @param param
                 * @returns {*}
                 */
                emulateLogin: function (param) {
                    return bes.post({
                        url: urlMap.LOGIN,
                        data: {
                            userFId: param.userFId,
                            tempPwd: param.tempPwd
                        }
                    });
                },
                /**
                 * 检测当前用户登录信息
                 * @returns {*}
                 */
                checkLogin: function () {
                    return bes.post({
                        url: urlMap.CHECK_LOGIN
                    });
                },
                /**
                 * 九龙中学接入登录接口
                 * @param param
                 * @returns {*}
                 */
                JLLogin: function (param) {
                    return bes.get({
                        url: urlMap.JL_LOGIN,
                        data: param
                    });
                },
                /**
                 * 注销登录
                 * @returns {*}
                 */
                logOut: function () {
                    return bes.get({
                        url: urlMap.LOGOUT
                    });
                },
                imgUpload: function () {

                },
                /**
                 * 获取当前用户关联帐号
                 * @returns {*}
                 */
                getAssociateAccounts: function () {
                    return bes.post({
                        url: urlMap.GET_ASSOCIATE_ACCOUNT
                    });
                },
                /**
                 * 通用查询。
                 *  Proc_name。'getSchoolGrade'：年级列表(学校开通的年级)；'Proc_getSchoolGrade'：年级列表(与学校无关)；'GetGradeClasss'：班级列表
                 *  'TeachValidChoiceClass': 老师能教的班级（不包括已有老师的班级）; 'Proc_ClassAvgOrder': 年级排名; 'getUnifiedItem'：获取学生答卷信息
                 *  'Proc_getUnifiedItemManager'; 'Proc_GetUnifiedTasks'; 'ProcQueryClassExams'; 'GetTaskSendList'
                 * @returns {*}
                 */
                generalQuery: function (param) {
                    return bes.post({
                        url: urlMap.GENERAL_QUERY,
                        data: param
                    });
                },
                /**
                 * 获取学科列表。
                 * @param param 学段
                 * @returns {*}
                 */
                getSubjectList: function (param) {
                    return bes.post({
                        url: urlMap.SUBJECT_LIST,
                        data: {
                            pharseId: param.pharseId
                        }
                    });
                },
                /**
                 * 获取出版社、教材列表
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                PublishGradeList: function (param) {
                    return bes.post({
                        url: urlMap.PUBLISH_GRADE_LIST,
                        data: {
                            pharseId: param.pharseId,
                            subjectId: param.subjectId
                        }
                    });
                },
                /**
                 * 获取学生列表。
                 * @param param 班级id 学校id
                 * @returns {*}
                 */
                getStudentList: function (param) {
                    return bes.post({
                        url: urlMap.STUDENT_LIST,
                        data: {
                            classFLnkId: param.classFLnkId,
                            schoolFLnkId: param.schoolFLnkId
                        }
                    });
                },
                /**
                 * 获取班级列表
                 * @param param
                 * @returns {*}
                 */
                getClassList: function (param) {
                    return bes.post({
                        url: urlMap.CLASS_LIST,
                        data: {
                            schoolFLnkId: param.schoolFLnkId
                        }
                    });
                },
                /**
                 * 获取角色列表。
                 * @returns {*}
                 */
                getBaseRoleList: function () {
                    return bes.get({
                        url: urlMap.ROLE_LIST
                    });
                },
                /**
                 * 删除用户。老师: '4'；学生：'5'
                 * @param param(Array) [{UserLnkID: 用户id, RoleID: 角色id},]
                 * @returns {*}
                 */
                deleteBaseUser: function (param) {
                    return bes.post({
                        url: urlMap.DELETE_USER,
                        data: param
                    });
                },
                /**
                 * 重置密码。老师: '4'；学生：'5'
                 * @param param(Array) [{UserLnkID: 用户id, RoleID: 角色id},]
                 * @returns {*}
                 */
                resetUserPassword: function (param) {
                    return bes.post({
                        url: urlMap.RESET_PASSWORD,
                        data: param
                    });
                },
                /**
                 * 获取老师列表。老师: '4'
                 * @param param
                 * @returns {*}
                 */
                getUserListPageView: function (param) {
                    return bes.post({
                        url: urlMap.USER_LIST,
                        data: {
                            SchoolFLnkId: param.SchoolFLnkId,
                            GradNo: param.GradNo,
                            RoleId: param.RoleId,
                            UserName: param.UserName,
                            SubjectName: param.SubjectName,
                            ClassName: param.ClassName,
                            Page: {
                                PageIndex: param.Page.PageIndex,
                                PageSize: param.Page.PageSize
                            }
                        }
                    });
                },
                /**
                 * 获取除老师、学生其他角色列表。老师: '4'；学生：'5'
                 * @param param
                 * @returns {*}
                 */
                getMasterListPageView: function (param) {
                    return bes.post({
                        url: urlMap.MASTER_LIST,
                        data: {
                            SchoolFLnkId: param.SchoolFLnkId,
                            GradNo: param.GradNo,
                            RoleId: param.RoleId,
                            UserName: param.UserName,
                            SubjectName: param.SubjectName,
                            ClassName: param.ClassName,
                            Page: {
                                PageIndex: param.Page.PageIndex,
                                PageSize: param.Page.PageSize
                            }
                        }
                    });
                },
                /**
                 * 批量修改学生资料——改班级。
                 * @param param
                 * @returns {*}
                 */
                updateStudentClass: function (param) {
                    return bes.post({
                        url: urlMap.UPDATE_STUDENT_CLASS,
                        data: {
                            StudentLnkID: param.StudentLnkID,
                            ClassLnkID: param.ClassLnkID,
                            GradeNum: param.GradeNum
                        }
                    });
                },
                /**
                 * 获取班级详细信息
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                GetSingerBaseClassListPageViewDetail: function (param) {
                    return bes.get({
                        url: urlMap.CLASS_VIEW_DETAIL + '?classFlnkId=' + param
                    });
                },
                /**
                 * 删除班级
                 * @param param
                 * @returns {*}
                 */
                deleteBaseClass: function (param) {
                    return bes.post({
                        url: urlMap.DELETE_SURE_BASE_CLASS,
                        data: {
                            FLnkID: param.FLnkID
                        }
                    });
                },
                /**
                 * 查询学校班主任列表 roleId: "3"
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                GetPageUserListPageView: function (param) {
                    return bes.post({
                        url: urlMap.USER_LIST_VIEW,
                        data: {
                            ClassFLnkID: param.ClassFLnkID,
                            SchoolFLnkId: param.SchoolFLnkId,
                            RoleId: param.RoleId,
                            Page: {
                                PageIndex: param.Page.PageIndex,
                                PageSize: param.Page.PageSize
                            }
                        }
                    });
                },
                /**
                 * 班级类型
                 * @returns {*}
                 * @constructor
                 */
                GetBaseClassTypeList: function () {
                    return bes.get({
                        url: urlMap.CLASS_TYPE_LIST
                    });
                },
                /**
                 * 修改班级信息
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                UpdateBaseClassMaster: function (param) {
                    return bes.post({
                        url: urlMap.UPDATE_CLASS_MASTER,
                       data: {
                           FLnkID: param.FLnkID,
                           ClassName: param.ClassName,
                           CreateByUserID: param.CreateByUserID,
                           SchoolFlnkID: param.SchoolFlnkID,
                           GradeNum: param.GradeNum,
                           Session: param.Session,
                           SmallNum: param.SmallNum,
                           UpOrDown: param.UpOrDown,
                           Period: param.Period,
                           Type: param.Type,
                           UserFLnkId: param.UserFLnkId,
                           RoleId: param.RoleId
                       }
                    })
                },
                /**
                 * 新增班级
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                InsertBaseClassMaster: function (param) {
                    return bes.post({
                        url: urlMap.INSERT_CLASS_MASTER,
                        data: {
                            FLnkID: param.FLnkID,
                            ClassName: param.ClassName,
                            CreateByUserID: param.CreateByUserID,
                            SchoolFlnkID: param.SchoolFlnkID,
                            GradeNum: param.GradeNum,
                            Session: param.Session,
                            SmallNum: param.SmallNum,
                            UpOrDown: param.UpOrDown,
                            Period: param.Period,
                            Type: param.Type,
                            UserFLnkId: param.UserFLnkId,
                            RoleId: param.RoleId
                        }
                    })
                },
                /**
                 * 查询学校升级
                 * @returns {*}
                 */
                getSchoolUpStatus: function () {
                    return bes.post({
                        url: urlMap.GET_SCHOOL_UP_STATUS
                    })
                },
                /**
                 * 学校升级
                 * @returns {*}
                 * @constructor
                 */
                SchoolUp: function () {
                    return bes.post({
                        url: urlMap.SCHOOL_UP
                    })
                },
                /**
                 * 获取老师详细信息
                 * @param teacherId
                 * @returns {*}
                 * @constructor
                 */
                GetTeacherDetailView: function (teacherId) {
                    return bes.get({
                        url: urlMap.GET_TEACHER_DETAIL_VIEW + '?teacherId=' + teacherId
                    })
                },
                /**
                 * 根据教材获取年级
                 * @param gradeId
                 * @returns {*}
                 */
                getGradeNum: function (gradeId) {
                    return bes.get({
                        url: urlMap.GET_GRADE_NUM + '?gradeId=' + gradeId
                    })
                },
                /**
                 * 修改老师资料。——保存接口
                 * @param param
                 * @returns {*}
                 */
                updateBaseTeacher: function (param) {
                    return bes.post({
                        url: urlMap.UPDATE_BASE_TEACHER,
                        data: param
                    })
                },
                //
                // uploadTeacherFile: function (param) {
                //     return bes.post({
                //         url: urlMap.UPLOAD_TEACHER_FILE,
                //         data: param
                //     });
                // },
                /**
                 * 老师数据导入——确认导入。老师: '4'
                 * @returns {*}
                 */
                sureImportTeacherList: function (param) {
                    return bes.post({
                        url: urlMap.SURE_IMPORT_TEACHER_LIST,
                        data: param
                    });
                },
                /**
                 * 学生数据导入——确认导入。学生：'5'
                 * @returns {*}
                 */
                sureImportStudentList: function (param) {
                    return bes.post({
                        url: urlMap.SURE_IMPORT_STUDENT_LIST,
                        data: param
                    });
                },
                /**
                 * 学生详细信息。
                 * @param studentFLnkId
                 * @returns {*}
                 * @constructor
                 */
                GetSingleBaseUser: function (studentFLnkId) {
                    return bes.get({
                        url: urlMap.GET_SINGL_EBASE_USER + '?id=' + studentFLnkId
                    })
                },
                /**
                 * 修改学生资料——保存接口
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                UpdateBaseUserByStudent: function (param) {
                    return bes.post({
                        url: urlMap.UPDATE_BASE_USER_BY_STUDENT,
                        data: {
                            FLnkID: param.FLnkID,
                            CreateByUserID: param.CreateByUserID,
                            SchoolFLnkId: param.SchoolFLnkId,
                            pharseId: param.pharseId,
                            UserName: param.UserName,
                            SelfName : param.SelfName,
                            LoginName: param.LoginName,
                            ClassFLnkID: param.ClassFLnkID,
                            Email: param.Email,
                            Telephone: param.Telephone,
                            Sex: param.Sex,
                            GradNo: param.GradNo,
                            Birthday: param.Birthday,
                            EmpID: param.EmpID,
                            SmallNum: param.SmallNum,
                            SFID: param.SFID,
                            Career: param.Career,
                            CarrerNew: param.CarrerNew,
                            RoleId: param.RoleId
                        }
                    });
                },
                /**
                 * 新增学生资料——保存接口
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                InsertBaseUserByStudent: function (param) {
                    return bes.post({
                        url: urlMap.INSERT_BASE_USER_BY_STUDENT,
                        data: {
                            FLnkID: param.FLnkID,
                            CreateByUserID: param.CreateByUserID,
                            SchoolFLnkId: param.SchoolFLnkId,
                            pharseId: param.pharseId,
                            UserName: param.UserName,
                            SelfName : param.SelfName,
                            LoginName: param.LoginName,
                            ClassFLnkID: param.ClassFLnkID,
                            Email: param.Email,
                            Telephone: param.Telephone,
                            Sex: param.Sex,
                            GradNo: param.GradNo,
                            Birthday: param.Birthday,
                            EmpID: param.EmpID,
                            SmallNum: param.SmallNum,
                            SFID: param.SFID,
                            Career: param.Career,
                            CarrerNew: param.CarrerNew,
                            RoleId: param.RoleId
                        }
                    });
                },
                /**
                 * 初始化班级
                 * @returns {*}
                 */
                classSimpleDetail: function () {
                    return bes.get({
                        url: urlMap.CLASS_SIMPLE_DETAIL
                    })
                },
                /**
                 * 试卷列表
                 * @param param
                 * @returns {*}
                 */
                examList: function (param) {
                    return bes.post({
                        url: urlMap.EXAM_LIST,
                        data: {
                            ClassFlnkid: param.ClassFlnkid
                        }
                    })
                },
                /**
                 * 试卷详细信息
                 * @param param
                 * @returns {*}
                 */
                examDetail: function (param) {
                    return bes.post({
                        url: urlMap.EXAM_DETAIL,
                        data: {
                            ClassFlnkid: param.ClassFlnkid,
                            ExamFlnkID: param.ExamFlnkID
                        }
                    })
                },
                /**
                 * 班级年级对比
                 * @param param
                 * @returns {*}
                 */
                classGradeContrast: function (param) {
                    return bes.post({
                        url: urlMap.CLASS_GRADE_CONTRAST,
                        data: {
                            ClassFlnkid: param.ClassFlnkid,
                            ExamFlnkID: param.ExamFlnkID
                        }
                    })
                },
                /**
                 * 得分排名
                 * @param param
                 * @returns {*}
                 */
                scoreRanking: function (param) {
                    return bes.post({
                        url: urlMap.SCORE_RANKING,
                        data: {
                            ClassFlnkid: param.ClassFlnkid,
                            ExamFlnkID: param.ExamFlnkID
                        }
                    })
                },
                /**
                 * 错题列表
                 * @param param
                 * @returns {*}
                 */
                errorQuestion: function (param) {
                    return bes.post({
                        url: urlMap.ERROR_QUESTION,
                        data: {
                            ClassFlnkid: param.ClassFlnkid,
                            ExamFlnkID: param.ExamFlnkID,
                            Type: param.Type
                        }
                    })
                },
                /**
                 * 成绩分布
                 * @param param
                 * @returns {*}
                 */
                scoreDistribution: function (param) {
                    return bes.post({
                        url: urlMap.SCORE_DISTRIBUTION,
                        data: {
                            ClassFlnkid: param.ClassFlnkid,
                            ExamFlnkID: param.ExamFlnkID
                        }
                    })
                },
                /**
                 * 获取批阅信息
                 * @param param
                 * unifid: 统考id
                 * @returns {*}
                 */
                getEncryptQList: function (param) {
                    return bes.post({
                        url: urlMap.GET_ENCRYPT_QLIST,
                        data: {
                            unifid: param.unifid,
                            getall: param.getall
                        }
                    })
                },
                /**
                 * 获取试卷
                 * @param param
                 * @returns {*}
                 */
                getPaperConfig: function (param) {
                    return bes.post({
                        url: urlMap.GET_PAPER_CONFIG,
                        data: {
                            unifiedId: param.unifiedId
                        }
                    })
                },
                /**
                 * 获取知识点列表
                 * @param param
                 * @returns {*}
                 */
                knowledgeList: function (param) {
                    return bes.post({
                        url: urlMap.KNOWLEDGE_LIST,
                        data: {
                            subjectId: param.subjectId,
                            pharseId: param.pharseId
                        }
                    })
                },
                /**
                 * 试卷审核
                 * @param param
                 * @returns {*}
                 */
                paperCheck: function (param) {
                    return bes.post({
                        url: urlMap.PAPER_CHECK,
                        data: {
                            examFLnkId: param.examFLnkId
                        }
                    })
                },
                /**
                 * 修改客观题答案
                 * @param param
                 * @returns {*}
                 */
                resetAnswerMarkRecord: function (param) {
                    return bes.post({
                        url: urlMap.RESET_ANSWER_MARK_RECORD,
                        data: {
                            qFlnkId: param.qFlnkId,
                            answer: param.answer,
                            forceFreshSc: param.forceFreshSc
                        }
                    })
                },
                /**
                 * 重置批阅结果
                 * @param param
                 * @returns {*}
                 */
                resetQuestionMarkRecord: function (param) {
                    return bes.post({
                        url: urlMap.RESET_QUESTION_MARK_RECORD,
                        data: {
                            unifiedId: param.unifiedId,
                            qFlnkId: param.qFlnkId
                        }
                    })
                },
                /**
                 * 设置分值 保存页面修改
                 * @param param
                 * @returns {*}
                 */
                updatePaperConfig: function (param) {
                    return bes.post({
                        url: urlMap.UPDATE_PAPER_CONFIG,
                        data: {
                            unifiedId: param.unifiedId,
                            meta: param.meta,
                            needResetEncrypt: param.needResetEncrypt
                        }
                    })
                },
                /**
                 * 设置子统考
                 * @param param
                 * @returns {*}
                 */
                updateUnifiedItemStep: function (param) {
                    return bes.post({
                        url: urlMap.UPDATE_UNIFIED_ITEM_STEP,
                        data: {
                            unifiedItemId: param.unifiedItemId,
                            step: param.step
                        }
                    })
                },
                /**
                 * 试卷类型列表
                 * @returns {*}
                 */
                examType: function () {
                    return bes.get({
                        url: urlMap.EXAM_TYPE
                    })
                },
                /**
                 * 年级过滤学科
                 * @param gradeNum
                 * @returns {*}
                 */
                getSubjectNameByGradeNum: function (gradeNum) {
                    return bes.get({
                        url: urlMap.SUBJECT_NAME_BY_GRADENUM + '?gradeNum=' + gradeNum
                    })
                },
                /**
                 * 根据年级或学科查询老师
                 * @param param
                 * @returns {*}
                 */
                getUnifiedAllowManager: function (param) {
                    return bes.post({
                        url: urlMap.GET_UNIFIED_ALLOW_MANAGER,
                        data: {
                            schoolId: param.schoolId,
                            grade: param.grade,
                            subjectId: param.subjectId
                        }
                    })
                },
                /**
                 * 修改父统考信息
                 * @param param
                 * @returns {*}
                 */
                modifyIntegrateUnifedName: function (param) {
                    return bes.post({
                        url: urlMap.MODIFY_PARENT_UNIFIED_NAME,
                        data: {
                            unifiedId: param.unifiedId,
                            unifedName: param.unifedName,
                            examType: param.examType
                        }
                    })
                },
                /**
                 * 获取题目异常的试卷
                 * @param param
                 * @returns {*}
                 */
                getAbnormalPapers: function (param) {
                    return bes.post({
                        url: urlMap.GET_ABNORMAL_PAPERS,
                        data: {
                            classId: param.classId,
                            examFLnkId: param.examFLnkId
                        }
                    })
                },
                /**
                 * 提交分析
                 * @param param
                 * @returns {*}
                 */
                tasksAnalysis: function (param) {
                    return bes.post({
                        url: urlMap.TASKS_ANALYSIS,
                        data: {
                            tasksFLnkId: param.tasksFLnkId,
                            isForce: param.isForce,
                            examFactor: param.examFactor || 1
                        }
                    })
                },
                /**
                 * 发送微信通知
                 * @param param
                 * @returns {*}
                 */
                sendWXMessage: function (param) {
                    return bes.post({
                        url: urlMap.SEND_WX_MESSAGE,
                        data: {
                            taskFlnkId : param.taskFlnkId
                        }
                    })
                },
                /**
                 * 取消统考
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                SplitPaKmUnifiedItem: function (param) {
                    return bes.post({
                        url: urlMap.SPLIT_UNIFIED_ITEM,
                        data: {
                            kmUnifiedBaseParam: param.kmUnifiedBaseParam,
                            oldParentFLnkIds: param.oldParentFLnkIds
                        }
                    })
                },
                /**
                 * 合并统考
                 * @param param
                 * @returns {*}
                 * @constructor
                 */
                MergePaKmUnifiedItem: function (param) {
                    return bes.post({
                        url: urlMap.MERGE_UNIFIED_ITEM,
                        data: param
                    })
                },
                //删除父统考
                DeleteKmUnifiedByFLnkId: function (param) {
                    return bes.post({
                        url: urlMap.DELETE_UNIFIED,
                        data: param
                    })
                },
                //删除子统考
                DeleteKmUnifiedItemByFLnkId: function (param) {
                    return bes.post({
                        url: urlMap.DELETE_UNIFIED_ITEM,
                        data: param
                    })
                },
                //获取带纠错考卷
                GetNoBodyExAnswer: function (param) {
                    return bes.post({
                        url: urlMap.GET_NOBODY_EXANSWER,
                        data: {
                            taskFid: param.taskFid
                        }
                    })
                },
                /**
                 * 导出html为pdf文件
                 * @param opt
                 *     content:  html内容字符串
                 *     type: 纸张类型，如‘A4’,'8K'等
                 *     name: 导出的文件名
                 * @returns {*}
                 */
                exportHtml2Pdf: function(opt){
                    return bes.post({
                        url: urlMap.EXPORT_HTML_TO_PDF,
                        data: {
                            content: opt.content,
                            type: opt.type || 'A4',
                            name: opt.name || '导出下载'
                        }
                    });
                }
            };
        });
});
