/**
 * Created by Hexl on 2016/4/30.
 */
angular.module('customService', [])
    .service('constantService', ['$q', '$http', 'ngDialog',function($q, $http, ngDialog) {
        return {
            NoMap: {
                0: '',
                1: '一',
                2: '二',
                3: '三',
                4: '四',
                5: '五',
                6: '六',
                7: '七',
                8: '八',
                9: '九',
                10: '十'
            },
            GRADE_LIST: [
                {
                    id: '1',
                    name: '一年级'
                }, {
                    id: '2',
                    name: '二年级'
                }, {
                    id: '3',
                    name: '三年级'
                }, {
                    id: '4',
                    name: '四年级'
                }, {
                    id: '5',
                    name: '五年级'
                }, {
                    id: '6',
                    name: '六年级'
                }, {
                    id: '7',
                    name: '七年级'
                }, {
                    id: '8',
                    name: '八年级'
                }, {
                    id: '9',
                    name: '九年级'
                }, {
                    id: '10',
                    name: '十年级'
                }, {
                    id: '11',
                    name: '十一年级'
                }, {
                    id: '12',
                    name: '十二年级'
                }
            ],
            /**
             *
             * @param text   文本内容
             * @param cb     确定回调
             */
            alert: function(text, cb){
                var dialog = ngDialog.open({
                    template: '<div ng-keypress="IsEnterKeyPress()" class="wy-alert-box"><p class="title t_center" style="background: #41B39C">提&nbsp;&nbsp;&nbsp;&nbsp;示</p><p class="text" ng-bind="text"></p>' +
                    '<div class="btn-group"><div class="btn btn-51 btn-51-green" ng-click="ok()" style="line-height: 35px">确定</div></div></div>',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'wy-alert',
                    closeByDocument: true,
                    plain: true,
                    controller: function($scope){
                        return function(){
                            $scope.text = text;
                            $scope.ok = function(){
                                dialog.close();
                                if(cb && _.isFunction(cb)) {
                                    cb();
                                }
                            };
                            $scope.IsEnterKeyPress = function () {
                                var lKeyCode = (navigator.appName=="Netscape")?event.which:event.keyCode;
                                if ( lKeyCode == 13 ){
                                    $scope.ok();
                                }
                            };
                        }
                    }
                })
            },
            confirm: function(cue, text, btns, doneCb, failCb){
                var dialog = ngDialog.open({
                    template: '<div class="wyt-alert-box"><p class="title t_center" ng-bind="cue"></p><p class="text" ng-bind="text"></p>' +
                    '<div class="btn-group"><div class="btn btnUsl" ng-click="fail()" style="border: 1px solid #CBCBCB" ng-bind="btns[1]"></div>' +
                    '<div class="btn btnUsl" ng-click="done()" style="background-color: #02C19F;color: #fff" ng-bind="btns[0]"></div></div></div>',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'wyt-alert',
                    closeByDocument: true,
                    plain: true,
                    controller: function($scope){
                        return function(){
                            $scope.cue = cue;
                            $scope.text = text;
                            $scope.btns = ['确定', '取消'];
                            if(_.isArray(btns)) {
                                $scope.btns[0] = btns[0] || '确定';
                                $scope.btns[1] = btns[1] || '取消';
                            }else if(_.isFunction(btns)) {
                                failCb = doneCb;
                                doneCb = btns;
                            }
                            $scope.fail = function(){
                                dialog.close();
                                if(failCb && _.isFunction(failCb)) {
                                    failCb();
                                }
                            };
                            $scope.done = function () {
                                dialog.close();
                                if (doneCb && _.isFunction(doneCb)) {
                                    doneCb();
                                }
                            };
                        }
                    }
                })
            },
            modal: function(opt){
                var modalDefer = $q.defer();
                function loadTemplate(){
                    var defer = $q.defer();
                    if(opt.template) {
                        defer.resolve(opt.template);
                    }else if(opt.templateUrl) {
                        $http.get(opt.templateUrl).then(function(res){
                            console.log(res.data);
                            defer.resolve(res.data);
                        }, defer.reject);
                    }else {
                        defer.reject();
                        throw new Error('NO_TEMPLATE_DEFINED!');
                    }
                    return defer.promise;
                }
                loadTemplate().then(function(template){
                    var $template = '<div class="wy-modal-box">' +
                        '<p class="title t_center" ng-bind="title"></p>' +
                        '<div class="modal-main">'+ template +'</div>' +
                        '<div class="btn-center-group" ng-show="isConfirm">' +
                        '<div class="btn btn-51-big btn-51-default" ng-click="cancel()" ng-bind="cancelBtnText"></div>' +
                        '<div class="btn btn-51-big btn-51-green" ng-click="ok()" ng-bind="okBtnText"></div>' +
                        '</div>' +
                        '</div>';
                    var modal = ngDialog.open({
                        template: $template,
                        className: 'ngdialog-theme-plain',
                        appendClassName: 'wy-modal',
                        closeByDocument: true,
                        scope: opt.scope,
                        plain: true,
                        controller: function($scope){
                            return function(){
                                $scope.title = opt.title;
                                $scope.isConfirm = opt.type === 'confirm';
                                var cancelBtn = opt.cancelText || '取消',
                                    okBtn = opt.okText || '确定',
                                    cancelFn = typeof opt.cancelFn === 'function' && opt.cancelFn || function(){},
                                    okFn = typeof opt.okFn === 'function' && opt.okFn || function(){};
                                $scope.cancelBtnText = cancelBtn;
                                $scope.okBtnText = okBtn;
                                $scope.cancel = function(){
                                    cancelFn.call($scope);
                                    ngDialog.close();
                                };
                                $scope.ok = function(){
                                    okFn.call($scope);
                                };
                            };
                        }
                    });
                    modalDefer.resolve(modal);
                });
                return modalDefer.promise;
            },
            getCNNoByIndex: function (index) {
                if (!index) {
                    return '';
                }
                var shang = Math.floor(index / 10),
                    yu = index % 10;
                var cnStr = '';
                if (shang > 1) {
                    cnStr = this.NoMap[shang] + this.NoMap[10] + this.NoMap[yu];
                } else if (shang === 1) {
                    cnStr = this.NoMap[10] + this.NoMap[yu];
                } else {
                    cnStr = this.NoMap[yu];
                }
                return cnStr;
            },
            showSimpleToast: (function() {
                return function(text, autoHideTime, callBack) {
                    autoHideTime = autoHideTime || 2000;
                    var dialogObj = ngDialog.open({
                        template: '<div class="toast-title">' + text + '</div>',
                        className: 'ngdialog-theme-default',
                        appendClassName: 'simple-toast',
                        closeByDocument: true,
                        plain: true,
                        preCloseCallback: function() {
                            if (callBack) {
                                callBack();
                            }
                        }
                    });
                    (function(obj) {
                        setTimeout(function() {
                            obj.close();
                        }, autoHideTime);
                    })(dialogObj);
                };
            })(),
            showSimpleText: (function() {
                return function(text) {
                    var dialogObj = ngDialog.open({
                        template: '<div class="toast-title">' + text + '</div>',
                        className: 'ngdialog-theme-default',
                        appendClassName: 'simple-toast',
                        closeByDocument: true,
                        plain: true
                    });
                };
            })(),
            showConfirm: function(text, btns, doneCb, failCb) {
                var okBtn = '删除',
                    cancelBtn = '取消';
                if (_.isArray(btns)) {
                    if (btns[0]) {
                        okBtn = btns[0];
                    }
                    if (btns[1]) {
                        cancelBtn = btns[1];
                    }
                } else {
                    failCb = doneCb;
                    doneCb = btns;
                }
                var dialogObj = ngDialog.open({
                    template: ['<div class="toast-title">' + text + '</div>',
                        '<div class="button-group">',
                        '<div class="btn btn-default btn-cancel" ng-click="cancel()">' + cancelBtn + '</div>',
                        '<div class="btn btn-primary btn-ok" ng-click="done()">' + okBtn + '</div>',
                        '</div>'
                    ].join(''),
                    className: 'ngdialog-theme-default',
                    appendClassName: 'simple-toast',
                    closeByDocument: false,
                    plain: true,
                    controller: function($scope) {
                        return function() {
                            $scope.cancel = function() {
                                dialogObj.close();
                                if (failCb && _.isFunction(failCb)) {
                                    failCb();
                                }
                            };
                            $scope.done = function() {
                                dialogObj.close();
                                if (doneCb && _.isFunction(doneCb)) {
                                    doneCb();
                                }
                            };
                        }
                    }
                });
            },
            getYearList: function(){
                var year = new Date().getFullYear();
                var yearList = [];
                for(var i = 0; i < 4; i++) {
                    yearList.push({
                        id: year - i,
                        name: year - i
                    });
                }
                return yearList;
            },
            notificate: function(text){
                var $template = $('<div id="notification">').css({
                    position: 'fixed',
                    right: '0',
                    bottom: '-50px',
                    width: 'auto',
                    'min-width': '100px',
                    height: '40px',
                    'line-height': '40px',
                    'background': '#41b29c',
                    'padding': '0 15px',
                    '-ms-transition': 'all ease-in .2s',
                    '-webkit-transition': 'all ease-in .2s',
                    '-o-transition': 'all ease-in .2s',
                    '-moz-transition': 'all ease-in .2s',
                    transition: 'all ease-in .2s',
                    'box-shadow': '1px 1px 5px 5px #ccc',
                    'color': '#fff'
                }).appendTo($('body'));
                var $icon = $('<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>').css({
                    display: 'inline-block',
                    'text-align': 'left',
                    'font-size': '18px'
                }).appendTo($template);
                var $text = $('<span>').text(text).css({
                    display: 'inline-block',
                    'text-align': 'center'
                }).appendTo($template);
                setTimeout(function(){
                    $template.css({
                        bottom: '0'
                    });
                }, 100);
                var timeout = setTimeout(function(){
                    $template.css({
                        bottom: '-50px'
                    });
                    setTimeout(function () {
                        $template.remove();
                    }, 200);
                }, 3000);
            },
            multiAnswerTip: function(cb){
                var dialog = ngDialog.open({
                    template: '<div ng-keypress="IsEnterKeyPress()" class="wy-alert-box">' +
                    '<p class="title t_center">提&nbsp;&nbsp;&nbsp;&nbsp;示</p>' +
                    '<div class="text">' +
                    '<div class="row-tip"><label>录入多答案</label><div><span class="red">使用"/"分割</span>.例如：如果答案有多个答案a,b,录入格式为"a/b"</div></div>' +
                    '<div class="row-tip"><label>题目有多空</label><div><span class="red">使用";"分割</span>.例如：如果题目有两空，答案分别为a,b,录入格式为"a;b"</div></div>' +
                    '<div class="row-tip"><label>多空题目，每空有多个答案</label><div><span class="red">使用"/"+";"组合分割</span>.例如：如果题目有两空，每空有两个答案a,b,录入格式为"a/b;a/b"</div></div>' +
                    '</div>' +
                    '<div class="btn-group"><div class="btn btn-51 btn-51-orange" ng-click="ok()" style="line-height: 35px">确定</div></div></div>',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'wy-alert',
                    closeByDocument: true,
                    plain: true,
                    controller: function($scope){
                        return function(){
                            $scope.ok = function(){
                                dialog.close();
                                if(cb && _.isFunction(cb)) {
                                    cb();
                                }
                            };
                            $scope.IsEnterKeyPress = function () {
                                var lKeyCode = (navigator.appName=="Netscape")?event.which:event.keyCode;
                                if ( lKeyCode == 13 ){
                                    $scope.ok();
                                }
                            };
                        }
                    }
                })
            }
        }
    }]);
