/**
 * Created by 贺小雷 on 2016/8/29.
 */
angular.module('cartService', [])
.service('cartService', ['$rootScope', '$http', '$q',function($rootScope, $http, $q){
    var cart;
    return {
        getCart: function(){
            var defer = $q.defer();
            if(typeof cart === 'undefined') {
                $http.post($rootScope.baseUrl + '/Interface0293.ashx').success(function(res){
                    if(_.isArray(res.msg)) {
                        cart = res.msg;
                    }else if(res.code !== 1){
                        cart = [];
                    }
                    defer.resolve(cart || []);
                }).error(function(){
                    defer.reject(cart);
                });
            }else {
                defer.resolve(cart);
            }
            return defer.promise;
        },
        putCart: function(id, qTypeName, qTypeId){
            var defer = $q.defer();
            var isExist = _.find(cart || [], function(item){
                return item.questionFId.toUpperCase() === id.toUpperCase();
            });
            if(isExist) {
                console.error(id + '试题已在试题篮中');
                defer.resolve();
            }else {
                $http.post($rootScope.baseUrl + '/Interface0291.ashx', {
                    questionFId: id
                }).then(function(res){
                    if(res.data.code === 0) {
                        cart.push({
                            questionFId: id,
                            qtypeName: qTypeName,
                            qtypeId: qTypeId
                        });
                        // $scope.$emit('cart_length_change', cart.length);
                        defer.resolve(res);
                    }else {
                        defer.reject();
                    }
                }, function(){
                    defer.reject();
                });
            }
            return defer.promise;
        },
        removeCart: function(id){
            var defer = $q.defer();
            $http.post($rootScope.baseUrl + '/Interface0292.ashx', {
                questionFId: id
            }).then(function(res){
                if(res.data.code === 0) {
                    var idArray = id.split(',');
                    var toDelIndex = [];
                    _.each(idArray, function(item, index){
                        var findIndex = _.findIndex(cart, function(cartItem){
                            return cartItem.questionFId.toUpperCase() === item.toUpperCase();
                        });
                        if(findIndex >= 0) {
                            toDelIndex.push(findIndex);
                        }
                    });
                    toDelIndex.sort(function(a, b){
                        return b - a;
                    });
                    _.each(toDelIndex, function(index){
                       cart.splice(index, 1);
                    });
                    defer.resolve(res);
                }else {
                    defer.reject();
                }
            }, function(){
                defer.reject();
            });
            return defer.promise;
        },
        clearLocalCart: function(){
            cart = undefined;
        },
        clearDatabaseCart: function(){
            var defer = $q.defer();
            if(cart) {
                var ids = _.pluck(cart, 'questionFId').join(',');
                this.removeCart(ids).then(defer.resolve, defer.reject);
            }else {
                defer.resolve();
            }
            return defer.promise;
        }
    }
}]);