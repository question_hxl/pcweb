/**
 * backEndService Module
 *
 * Description
 */
'use strict';
angular.module('backEndService', []).provider('bes', function() {
    var baseUrl = 'BootStrap/Interface';
    return {
        setBaseUrl: function(_baseUrl) {
            baseUrl = _baseUrl;
        },
        $get: ['$http','constantService', function($http, constantService) {
            return {
                baseUrl: baseUrl,
                /**
                 * 通用http请求
                 * @param path  请求路径
                 * @param data  请求参数
                 * @param option  相关配置
                 *        ---- method: String:请求方式    'get', 'post'
                 *        ---- ignoreLoading: Boolean:是否禁用进度条
                 *        ---- disableErrorHandler: Boolean:是否禁用默认错误处理
                 */
                send: function(path, data, option) {
                    option = option || {};
                    var method = option.method || 'get';
                    var def = {
                        url: path,
                        method: method,
                        ignoreLoadingBar: option.ignoreLoading
                    };
                    if(method === 'get') {
                        def.params = data;
                        def.cache = true;
                    }else {
                        def.data = data;
                    }
                    return $http(def).error(function(res){
                        if(!option.disableErrorHandler) {
                            constantService.alert('服务器异常，请重试！');
                        }
                    });
                },
                get: function(opt){
                    var url = opt.url,
                        data = opt.data,
                        config = opt.cfg || {};
                    config.method = 'get';
                    return this.send(url, data, config);
                },
                post: function(opt){
                    var url = opt.url,
                        data = opt.data,
                        config = opt.cfg || {};
                    config.method = 'post';
                    return this.send(url, data, config);
                }
            };
        }]
    };
});
