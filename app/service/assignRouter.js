/**
 * Created by 贺小雷 on 2017/2/13.
 */
define(function(require){
    var constant = require('constant');
    var checkAndAssign = function($state, data, fn){
        /**
         * 根据用户角色分配路由
         */
        var user = data || angular.fromJson(sessionStorage.getItem('currentUser') || undefined);
        if(user) {
            switch (user.role){
                case constant.ROLE.TEACHER:
                    $state.go('myApp.home');
                    fn && fn();
                    break;
                case constant.ROLE.CLASS_MASTER:
                case constant.ROLE.SCHOOL_MANAGER:
                case constant.ROLE.GRADE_MASTER:
                case constant.ROLE.SCHOOL_MASTER:
                    $state.go('myApp.masterHome');
                    // location.href = location.origin + '/headmasterPort/index.html#/';
                    // alert('校长平台正在升级中，暂时无法访问，请切换其他账号登录！');
                    break;
                case constant.ROLE.STUDENT:
                    $state.go('myApp.Perforsis');
                    break;
                default:
					$state.go('myApp.home');
                    break;
            }
        }else {
            $state.go('login');
        }
    };
    return {
        assign: function($state, data, fn){
            checkAndAssign($state, data, fn);
        },
        assignInject: function($state){
            checkAndAssign($state);
        }
    };
});