define(['ckeditor', 'underscore-min'], function () {
	return ['$rootScope', '$http', '$scope', '$q', '$location', '$sce', 'ngDialog',
	function ($rootScope, $http, $scope, $q, $location, $sce, ngDialog) {
		var questionlists = $location.search();
		var question = $scope.newQuestion = {};
		$scope.newtypename = '';
		$scope.newqtype = '';

		$scope.isAnswerA = false;
		$scope.isAnswerB = false;
		$scope.isAnswerC = false;
		$scope.isAnswerD = false;
		$scope.isChoice = false;

		initCKEditor();
		function initCKEditor() {
			var instances = _.values(CKEDITOR.instances);
			_.each(instances, function (ist) {
				try {
					ist && ist.destroy(true);
				} catch (e) {
					console.log(e);
				}
			});
			CKEDITOR.replaceAll();
		}


		$scope.currentgradeName = questionlists.gradeid;
		$http.get($rootScope.baseUrl + '/Interface0051A.ashx').success(function (res) {
			$scope.gradeList = res.course;
			$scope.currentgradeName = questionlists.gradeid;
			$scope.currentgradenames = $('.gradenames').data('gradename');
		})

		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0197B.ashx'
		}).success(function (data) {
			$scope.NewClasstypeList = data.msg;
		})


		//新的题型
		$scope.$watch('newqtype', function () {
			console.log($scope.newqtype);
			$scope.newqtype = $scope.newqtype;
		});


		//添加新题型
		$scope.savenewtype = function () {
			if ($scope.newqtype == '') {
				ngDialog.open({
					template:
							'<p>请选择题型类别</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					plain: true,
					className: 'ngdialog-theme-plain'
				});
				return false;
			} else {
				if ($scope.newtypename == '') {
					ngDialog.open({
						template:
								'<p>请输入知识点名称</p>' +
								'<div class="ngdialog-buttons">' +
								'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					return false;
				} else {
					$http({
						method: 'post',
						url: $rootScope.baseUrl + '/Interface0197C.ashx',
						data: {
							Name: $scope.newtypename,
							TypeId: $scope.newqtype //$scope.newtypename
						}
					}).success(function (data) {
						if (data.code == 0) {
							$http({
								method: 'post',
								url: $rootScope.baseUrl + '/Interface0197B.ashx'
							}).success(function (data) {
								$scope.NewClasstypeList = data.msg;
							})
						} else {
							ngDialog.open({
								template:
										'<p>' + data.msg + '</p>' +
										'<div class="ngdialog-buttons">' +
										'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
								plain: true,
								className: 'ngdialog-theme-plain'
							});
							return false;
						}
					})
				}
			}
		}

		//添加新类型至左边列表
		$scope.addnewtype = function (data) {
			$scope.NewClassList = data;
			var r = _.findIndex($scope.ClasstypeList, { qtypeId: data.qtypeId });
			console.log(r);
			if (r != -1) {
				ngDialog.openConfirm({
					template: '<p>已存在</p>' +
						'<div class="ngdialog-buttons">' +'</div>',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					plain: true,
				});
				return false;
			} else {
				ngDialog.openConfirm({
					template: '<p>确定加入？</p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closedia()">取消</button>' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(add())">确定</button></div>',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					plain: true,
				});

				//记入新类型
				$scope.add = function () {
					$http.post($rootScope.baseUrl + '/Interface0197D.ashx', {
						gradeId: $scope.currentgradeName,
						qtypeId: data.qtypeId
					}).success(function (resp) {
						console.log(resp.msg);
						$http({
							method: 'post',
							url: $rootScope.baseUrl + '/Interface0197.ashx',
							data: {
								gradeId: questionlists.gradeid,
							}
						}).success(function (data) {
							$scope.ClasstypeList = data.msg;
							console.log($scope.ClasstypeList);
						})
					});

				};

				//关闭ngDialog
				$scope.closedia = function () {
					console.log("取消加入");
					ngDialog.close();
				};
			}
		}


		$scope.levelList = [1, 2, 3, 4, 5];
		$scope.newQuestion.level = $scope.levelList[0];
		$scope.chapterId = questionlists.chapterId;
		$scope.orderList = [{
			id: 0,
			name: '一行四列'
		}, {
			id: 1,
			name: '两行两列'
		}, {
			id: 2,
			name: '四行一列'
		}];

		$scope.currentknowname = [];
		$scope.currentknowledgeid = [];

		$scope.isKnowledgeRelated = function (knowledge) {
			return _.contains($scope.currentknowledgeid, knowledge.knowledgeId);
		};

		$scope.newQuestion.order = $scope.orderList[0];
		$scope.choicesList = [{
			name: 'A',
			isAnswer: false
		}, {
			name: 'B',
			isAnswer: false
		}, {
			name: 'C',
			isAnswer: false
		}, {
			name: 'D',
			isAnswer: false
		}];
		$scope.isChoice = false;
		$scope.isFromExam = false;
		var answer = '';

		//监听复选框
		$scope.$watch('isSelected', function () {
			//alert($scope.isSelected);
		});

		$scope.isSelected = function (a) {
			if (answer == '') {
				answer = a
			} else {
				answer = answer + ',' + a
			}
		}
		if (questionlists.state == 2) {
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0213.ashx',
				data: {
					gradeId: questionlists.gradeid,
				}
			}).success(function (res) {
				var klglist = [];
				for (var i = 0; i < res.msg.length; i++) {//第一层
					for (var j = 0; j < res.msg[i].unit.length; j++) {//第二层
						if (!res.msg[i].unit[j].unit || !res.msg[i].unit[j].unit.length) {//如果标记0就只有两层
							klglist.push({
								'knowledgeId': res.msg[i].unit[j].unitId,
								'knowledgeName': res.msg[i].unit[j].unitName
							})
						} else {
							for (var k = 0; k < res.msg[i].unit[j].unit.length; k++) {//第三层
								klglist.push({
									'knowledgeId': res.msg[i].unit[j].unit[k].unitId,
									'knowledgeName': res.msg[i].unit[j].unit[k].unitName
								})
							}
						}
					}
				}
				$scope.knowledgeList = klglist;
			})

			$scope.strknowledgeId = questionlists.chapterId;
			//$http({
			//	method: 'post',
			//	url: $rootScope.baseUrl + '/Interface0210.ashx',
			//	data: {
			//		chapterId: $scope.chapterId,
			//	}
			//}).success(function (data) {
			//	$scope.knowledgeList = data.msg;
			//})
		}
		if (questionlists.state == 1) {
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0213.ashx',
				data: {
					gradeId: questionlists.gradeid,
				}
			}).success(function (res) {
				var klglist = [];
				for (var i = 0; i < res.msg.length; i++) {//第一层
					for (var j = 0; j < res.msg[i].unit.length; j++) {//第二层
						if (!res.msg[i].unit[j].unit || !res.msg[i].unit[j].unit.length) {//如果标记0就只有两层
							klglist.push({
								'knowledgeId': res.msg[i].unit[j].unitId,
								'knowledgeName': res.msg[i].unit[j].unitName
							})
						} else {
							for (var k = 0; k < res.msg[i].unit[j].unit.length; k++) {//第三层
								klglist.push({
									'knowledgeId': res.msg[i].unit[j].unit[k].unitId,
									'knowledgeName': res.msg[i].unit[j].unit[k].unitName
								})
							}
						}
					}
				}
				$scope.knowledgeList = klglist;
			})

			$scope.strknowledgeId = questionlists.chapterId;

			//$scope.$watch('knowledgeList + strknowledgeId', function () {
			//	if ($scope.strknowledgeId) {
			//		//var knowledgeIds = $scope.strknowledgeId.split(',');
			//		_.each($scope.knowledgeList, function (item) {
			//			item.knowledgeId = !!_.contains($scope.strknowledgeId, item.knowledgeId);
			//		});
			//	}

			//	console.log($scope.knowledgeList);
			//});

		}


		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0197.ashx',
			data: {
				gradeId: questionlists.gradeid,
			}
		}).success(function (data) {
			$scope.ClasstypeList = data.msg;
			console.log($scope.ClasstypeList);
		})





		// selectCur
		$scope.selectCur = function ($event, properties) {
			var tag = $event.target;
			var txt = $(tag).text();

			switch (properties) {
				case 'year':
					$scope.currentyear = txt; // 年份
					break;
				case 'Classtype':
					$scope.currentClasstype = $(tag).attr('data-id'); // 题型ID
					$scope.currenttypeid = $(tag).attr('data-typeid'); // 主观题客观题判断 
					$scope.currentqname = $(tag).attr('data-qname'); // 题型
					break;
				case 'knowledge':
					var name = $(tag).attr('data-klgName'),
						id = $(tag).attr('data-id');
					var nameIndex = _.indexOf($scope.currentknowname, name),
						idIndex = _.indexOf($scope.currentknowledgeid, id);
					if (nameIndex >= 0) {
						$scope.currentknowname.splice(nameIndex, 1);
					} else {
						$scope.currentknowname.push(name);
					}
					if (idIndex >= 0) {
						$scope.currentknowledgeid.splice(idIndex, 1);
					} else {
						$scope.currentknowledgeid.push(id);
					}
					break;
				case 'gradeName':
					$scope.currentgradeName = $(tag).attr('data-id');
					$scope.currentgradenames = $(tag).attr('data-gradename');
					console.log($scope.currentgradeName + "---" + $scope.currentgradenames);
					break;
			}
			//console.log($scope.currenttypeid);
			if ($scope.currenttypeid == 1) {
				$scope.isChoice = true;
			} else {
				$scope.isChoice = false;
			}
		}

		//if (answer == '') {
		//	CKEDITOR.document.getById('answerArea').getData();
		//}

		//无数据弹框
		function noindata(infos) {
			//$(document).scrollTop('#questionDescArea');
			if ($scope.currenttypeid == 1) {
				ngDialog.open({
					template:
							'<p><span style="color:red;font-weight:bold;">' + infos + '</span>不能为空</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					plain: true,
					className: 'ngdialog-theme-plain'
				});
				return false;
			}
			ngDialog.open({
				template:
						'<p><span style="color:red;font-weight:bold;">' + infos + '</span>不能为空</p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
				plain: true,
				className: 'ngdialog-theme-plain'
			});
			return false;
		}
		//新增题目提交
		$scope.addQuestion = function () {
			var title = CKEDITOR.instances.questionDescArea.getData();
			if (title == '') {
				noindata('问题描述');
				return false;
			}
			var optionA = CKEDITOR.instances.answerA.getData();
			if ($scope.currenttypeid == 1) {
				if (optionA == '') {
					noindata('选项A');
					return false;
				}
			}

			var optionB = CKEDITOR.instances.answerB.getData();
			if ($scope.currenttypeid == 1) {
				if (optionB == '') {
					noindata('选项B');
					return false;
				}
			}

			var optionC = CKEDITOR.instances.answerC.getData();
			if ($scope.currenttypeid == 1) {
				if (optionC == '') {
					noindata('选项C');
					return false;
				}
			}

			var optionD = CKEDITOR.instances.answerD.getData();
			if ($scope.currenttypeid == 1) {
				if (optionD == '') {
					noindata('选项D');
					return false;
				}
			}

			var answers = CKEDITOR.instances.answerArea.getData();

			if ($scope.currenttypeid != 1 && answers == '') {
				noindata('题目答案');
				return false;
			}

			var pares = CKEDITOR.instances.analysisArea.getData();
			if (pares == '') {
				noindata('题目解析');
				return false;
			}

			//alert(question.source);

			if (question.source === undefined) {
				question.source = '';
			}

			if ($scope.currentknowname == '' || $scope.currentknowledgeid == '' || $scope.currentClasstype == '' || $scope.currentqname == '' || $scope.currentyear == '' || question.source == 'undefined') {
				noindata('年份/类型/知识点信息');
				return false;
			} else {
				if ($scope.isChoice == false) {
					answer = answers;
				} else {
					var answerTemp = [];
					if ($scope.isAnswerA) {
						answerTemp.push('A');
					}
					if ($scope.isAnswerB) {
						answerTemp.push('B');
					}
					if ($scope.isAnswerC) {
						answerTemp.push('C');
					}
					if ($scope.isAnswerD) {
						answerTemp.push('D');
					}
					answer = answerTemp.join(',');
				}



				$http.post($rootScope.baseUrl + '/Interface0209.ashx', {
					answer: answer,
					knowledgeId: $scope.currentknowledgeid.join(','),
					qTypeNum: $scope.currenttypeid,
					title: title,
					optionA: optionA,
					optionB: optionB,
					optionC: optionC,
					optionD: optionD,
					pares: pares,
					diff: question.level,
					qtypeName: $scope.currentqname,
					qtypeId: $scope.currentClasstype,
					gradeId: questionlists.gradeId,
					knowledge: $scope.currentknowname.join(','),
					year: $scope.currentyear,
					source: question.source,
					showType: question.order.id
				}).success(function (data) {
					if (data.code == 0) {
						ngDialog.open({
							template:
									'<p>新增成功~！</p>' +
									'<div class="ngdialog-buttons">' +
									'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-plain'
						});
						return false;
					} else {
						ngDialog.open({
							template:
									'<p>' + data.msg + '</p>' +
									'<div class="ngdialog-buttons">' +
									'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
							plain: true,
							className: 'ngdialog-theme-plain'
						});
						return false;
					}
				});
			}


		};
	}]
})