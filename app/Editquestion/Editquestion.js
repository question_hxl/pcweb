define(['ckeditor', 'underscore-min'], function () {
	return ['$rootScope', '$http', '$scope', '$q', '$location', '$sce', 'ngDialog', '$stateParams',
	function ($rootScope, $http, $scope, $q, $location, $sce, ngDialog) {
		// 获取题目ID
		var questions = $location.search();
		console.log(questions);
		$scope.isAnswerA = false;
		$scope.isAnswerB = false;
		$scope.isAnswerC = false;
		$scope.isAnswerD = false;

		$scope.GradeId = questions.GradeId;
		$scope.QTypeName = questions.QTypeName;
		$scope.isChoice = false;

		//  根据题目的ID调取题目的详细信息
		var answer = '';
		$http.get($rootScope.baseUrl + '/Interface0051A.ashx').success(function (res) {
			$scope.grades = res.course;
			var r = _.findIndex(res.course, { gradeId: $scope.GradeId });
			$scope.gradeName = $scope.grades[r].gradeName;
		});

		$http.post($rootScope.baseUrl + '/Interface0208.ashx', {
			qFlnkID: questions.qFlnkID
		}).success(function (data) {
			var questionList = data.msg[0];
			console.log(questionList);
			if (questionList.OptionA != '') {
				$scope.isChoice = true;
				CKEDITOR.document.getById('answerA').setHtml(questionList.OptionA);
				CKEDITOR.document.getById('answerB').setHtml(questionList.OptionB);
				CKEDITOR.document.getById('answerC').setHtml(questionList.OptionC);
				CKEDITOR.document.getById('answerD').setHtml(questionList.OptionD);

				if (questionList.Answer.indexOf('A') >= 0 || questionList.Answer.indexOf('a') >= 0) {
					$scope.isAnswerA = true;
				}
				if (questionList.Answer.indexOf('B') >= 0 || questionList.Answer.indexOf('b') >= 0) {
					$scope.isAnswerB = true;
				}
				if (questionList.Answer.indexOf('C') >= 0 || questionList.Answer.indexOf('c') >= 0) {
					$scope.isAnswerC = true;
				}
				if (questionList.Answer.indexOf('D') >= 0 || questionList.Answer.indexOf('d') >= 0) {
					$scope.isAnswerD = true;
				}
			} else {
				$scope.isChoice = false;
			}

			CKEDITOR.document.getById('questionDescArea').setHtml(questionList.title);
			CKEDITOR.document.getById('analysisArea').setHtml(questionList.Analysis);
			CKEDITOR.document.getById('answerArea').setHtml(questionList.Answer);
			//设置难度级别和来源
			$scope.level = questionList.DifficultLevel;
			$scope.order = _.find($scope.orderList, function (item) {
				return item.id === questionList.ShowType;
			}) || $scope.orderList[0];
			$scope.source = questionList.source;
			$scope.strknowledgeId = questionList.strknowledgeId;//questionList.strknowledgeId
			initCKEditor();
		});

		function initCKEditor() {
			var instances = _.values(CKEDITOR.instances);
			_.each(instances, function (ist) {
				try {
					ist && ist.destroy(true);
				} catch (e) {
					console.log(e);
				}
			});
			CKEDITOR.replaceAll();
		}

		if (questions.state == 1) {
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0213.ashx',
				data: {
					gradeId: questions.GradeId,
				}
			}).success(function (res) {
				var klglist = [];
				for (var i = 0; i < res.msg.length; i++) {//第一层
					for (var j = 0; j < res.msg[i].unit.length; j++) {//第二层
						if (!res.msg[i].unit[j].unit || !res.msg[i].unit[j].unit.length) {//如果标记0就只有两层
							klglist.push({
								'knowledgeId': res.msg[i].unit[j].unitId,
								'knowledgeName': res.msg[i].unit[j].unitName
							})
						} else {
							for (var k = 0; k < res.msg[i].unit[j].unit.length; k++) {//第三层
								klglist.push({
									'knowledgeId': res.msg[i].unit[j].unit[k].unitId,
									'knowledgeName': res.msg[i].unit[j].unit[k].unitName
								})
							}
						}
					}
				}
				$scope.knowledgeList = klglist;
			})
		}

		if (questions.state == 2) {
			//  根据题章节ID调取知识点信息
			$http.post($rootScope.baseUrl + '/Interface0210.ashx', {
				chapterId: questions.chapterId
			}).success(function (data) {
				$scope.knowledgeList = data.msg;
			});
		}

		//$scope.currentknowledgeid = [];

		////知识点是否选中
		//$scope.isKnowledgeRelated = function (knowledge) {
		//	return _.contains($scope.currentknowledgeid, knowledge.knowledgeId);
		//};


		$scope.$watch('knowledgeList + strknowledgeId', function () {
			if (!_.isEmpty($scope.knowledgeList) && $scope.strknowledgeId) {
				var knowledgeIds = $scope.strknowledgeId.split(',');
				_.each($scope.knowledgeList, function (item) {
					item.isRelated = !!_.contains(knowledgeIds, item.knowledgeId);
				});
			} else {
				_.each($scope.knowledgeList, function (item) {
					item.isRelated = false;
				});
			}
		});


		var question = $scope.newQuestion = {};

		$scope.levelList = ['1', '2', '3', '4', '5'];

		$scope.orderList = [{
			id: '0',
			name: '一行四列'
		}, {
			id: '1',
			name: '两行两列'
		}, {
			id: '2',
			name: '四行一列'
		}];

		$scope.choicesList = [{
			name: 'A',
			isAnswer: false
		}, {
			name: 'B',
			isAnswer: false
		}, {
			name: 'C',
			isAnswer: false
		}, {
			name: 'D',
			isAnswer: false
		}];

		$scope.isChoice = false;

		$scope.isFromExam = false;



		// var initQuestion = function (odlQuestion) {
		// 	question.level = odlQuestion && +odlQuestion.level || $scope.levelList[0];
		// 	question.isExam = odlQuestion && odlQuestion.isExam || '0';
		// 	if (odlQuestion && odlQuestion.questionTypeid) {
		// 		var currentOrder = _.find($scope.orderList, function (item) {
		// 			return item.id === +odlQuestion.order;
		// 		});
		// 		question.order = currentOrder || $scope.orderList[0];
		// 	} else {
		// 		question.order = $scope.orderList[0];
		// 	}
		//
		// 	question.source = odlQuestion && odlQuestion.source || '';
		// };
		//
		// var param = $location.search();
		//
		// initQuestion(question);

		// selectCur
		$scope.selectCur = function ($event, properties) {
			var tag = $event.target;
			var txt = $(tag).text();
			switch (properties) {
				case 'knowledge':
					console.log($scope.currenttypeid);
					var id = $(tag).attr('data-id');
					var array = [];
					array = $scope.strknowledgeId.split(',');
					var idIndex = _.indexOf(array, id);

					if (idIndex >= 0) {
						array.splice(idIndex, 1);
						var arrast = '';
						for (var i = 0; i < array.length; i++) {
							if (arrast == '') {
								arrast += array[i];
							} else {
								arrast += ',' + array[i];
							}
						}
						$scope.strknowledgeId = arrast;
						console.log(arrast);
					} else {
						$scope.strknowledgeId += ',' + id;
						//$scope.currentknowledgeid.push(id);
						console.log(array);
					}
					break;
			}
			//if ($scope.currenttypeid == 1) {
			//	alert($scope.currenttypeid);
			//	$scope.isChoice = true;
			//} else {
			//	alert($scope.currenttypeid);
			//	$scope.isChoice = false;
			//}
		}

		$scope.isSelected = function (a) {
			console.log(answer);
			if (answer == '') {
				answer = a
			} else {
				answer = answer + ',' + a
			}
			//console.log(answer);
		}
		//无数据弹框
		function noindata(infos) {
			//$(document).scrollTop('#questionDescArea');
			if ($scope.currenttypeid == 1) {
				ngDialog.open({
					template:
							'<p><span style="color:red;font-weight:bold;">' + infos + '</span>不能为空</p>' +
							'<div class="ngdialog-buttons">' +
							'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
					plain: true,
					className: 'ngdialog-theme-plain'
				});
				return false;
			}
			ngDialog.open({
				template:
						'<p><span style="color:red;font-weight:bold;">' + infos + '</span>不能为空</p>' +
						'<div class="ngdialog-buttons">' +
						'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
				plain: true,
				className: 'ngdialog-theme-plain'
			});
			return false;
		}
		$scope.addQuestion = function () {

			var title = CKEDITOR.instances.questionDescArea.getData();
			if (title == '') {
				noindata('问题描述');
				return false;
			}

			if ($scope.isChoice == false) {
				answer = CKEDITOR.instances.answerArea.getData();
			} else {
				var answerTemp = [];
				if ($scope.isAnswerA) {
					answerTemp.push('A');
				}
				if ($scope.isAnswerB) {
					answerTemp.push('B');
				}
				if ($scope.isAnswerC) {
					answerTemp.push('C');
				}
				if ($scope.isAnswerD) {
					answerTemp.push('D');
				}
				answer = answerTemp.join(',');
			}
			$scope.newQuestion.analysis = CKEDITOR.instances.analysisArea.getData();

			var klglist = $scope.strknowledgeId.toString();
			console.log(klglist);
			//  根据题目的ID调取题目的详细信息
			if (CKEDITOR.instances.answerA.getData() == '' || CKEDITOR.instances.answerA.getData().length == 0 || typeof (CKEDITOR.instances.answerA.getData()) == undefined) {
				$http.post($rootScope.baseUrl + '/Interface0206.ashx', {
					qFlnkId: questions.qFlnkID,
					title: title,
					answer: answer,
					diff: $scope.level,
					showType: $scope.order.id,
					source: $scope.source,
					analysis: $scope.newQuestion.analysis,
					strKnowledgeId: klglist
				}).success(function (data) {
					ngDialog.open({
						template:
								'<p>' + data.msg + '</p>' +
								'<div class="ngdialog-buttons">' +
								'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					//$location.path('questionBank');
					return false;
				});
			} else {
				$http.post($rootScope.baseUrl + '/Interface0206.ashx', {
					qFlnkId: questions.qFlnkID,
					title: title,
					optionA: CKEDITOR.instances.answerA.getData(),
					optionB: CKEDITOR.instances.answerB.getData(),
					optionC: CKEDITOR.instances.answerC.getData(),
					optionD: CKEDITOR.instances.answerD.getData(),
					answer: answer,
					diff: $scope.level,
					showType: $scope.order.id,
					source: $scope.source,
					analysis: $scope.newQuestion.analysis,
					strKnowledgeId: klglist
				}).success(function (data) {
					ngDialog.open({
						template:
								'<p>' + data.msg + '</p>' +
								'<div class="ngdialog-buttons">' +
								'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					//$location.path('questionBank');
					return false;
				});
			}
			
		};
	}]
})

//angular.module('app', []).directive('treeView', [function () {
//	return {
//		restrict: 'E',
//		templateUrl: '/treeView.html',
//		scope: {
//			treeData: '=',
//			canChecked: '=',
//			textField: '@',
//			itemClicked: '&',
//			itemCheckedChanged: '&',
//			itemTemplateUrl: '@'
//		},
//		controller: ['$scope', function ($scope) {
//			$scope.itemExpended = function (item, $event) {
//				item.$$isExpend = !item.$$isExpend;
//				$event.stopPropagation();
//			};

//			$scope.getItemIcon = function (item) {
//				var isLeaf = $scope.isLeaf(item);
//				if (isLeaf) {
//					$scope.choLast = true;
//					return 'fa';
//				} else {
//					$scope.choLast = false;
//				}
//				return item.$$isExpend ? 'fa fa-minus-square' : 'fa fa-plus-square';
//			};

//			$scope.isLeaf = function (item) {
//				return !item.unit || !item.unit.length
//			};

//			$scope.warpCallback = function (item, $event) {
//				if (item.unitId == undefined || item.unitId == null) {
//					return false;
//				} else {
//					$scope.$parent.chapterId = item.unitId;
//				}

//			};
//		}]

//	}

//}])