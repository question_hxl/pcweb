/**
 * Created by zy on 2017/7/27 0027.
 * */
define(['underscore-min'], function () {
    return ['$http', '$scope', '$rootScope','$state','constantService',
        function ($http, $scope, $rootScope,$state,constantService ) {
            $scope.tablethKeys = ['examName', 'status'];
            $scope.currentMode = 1;
            $scope.papers = [];
            $http.post('/BootStrap/Interface/Interface0285.ashx',{exType: '机房评测'}).then(function(res){
                if(_.isArray(res.data.msg)){
                    $http.get('/BootStrap//Interface/getSysTime.ashx').then(function(resp){
                        var sysTime = resp.data;
                        $scope.paperList = _.map(res.data.msg, function (item) {
                            var isBegin = false, remaintime;
                            if(sysTime <= item.StopTime) {
                                if(sysTime >= item.ExamTime) {
                                    isBegin = true;
                                    if(item.aflnkid === undefined){
                                        if(item.SpendTime === 0) {
                                            var StopTime = new Date(item.StopTime),ExamTime = new Date(item.ExamTime);
                                            remaintime = Math.ceil(StopTime.getTime() - ExamTime.getTime())/1000/60;
                                        } else {
                                            remaintime = item.SpendTime;
                                        }
                                    }else if(item.isfinish){
                                        remaintime = '0';
                                    } else {
                                        remaintime = item.remaintime;
                                    }
                                }
                            }else {
                                item.status = 1;
                            }
                            return {
                                examFId : item.ExExamFLnkID,
                                FLnkID : item.FLnkID,
                                examName : item.TaskName,
                                status : item.status,
                                exAnswerFlnkID : item.aflnkid,
                                ExamTime : item.ExamTime,
                                StopTime : item.StopTime,
                                finishdate : item.finishdate,
                                remaintime : remaintime,
                                isBegin : isBegin,
                                isfinish : item.isfinish
                            }
                        });
                    });
                }
            });

            $scope.startTesting = function(paper){
                var win = window.open('/machineEvaluate/startMachineEva.html#/?examId=' + paper.examFId +'&taskFlnkid='+ paper.FLnkID + '&exType=机房评测&from=' + location.href+'&isOpenNewWin=1');
                $http.post('/BootStrap/student/StartExam.ashx',{
                    taskFLnkId: paper.FLnkID
                }).then(function(res){
                    if(res.data.code === 0){

                    }else {
                        win.close();
                        constantService.alert(res.data.msg);
                    }
                });

            };

            $scope.viewAnalysis = function(paper){
                $state.go('myApp.Perforsis', {
                    ExamFlnkID : paper.examFId,
                    ClassFlnkid : JSON.parse(sessionStorage.getItem('currentUser')).scid,
                    ExAnswerFlnkID : paper.exAnswerFlnkID
                });
            };
        }
    ]
});