/**
 * Created by Administrator on 2016/10/25 0025.
 */
define(["highcharts", 'echarts', '../service/assignRouter'], function (highcharts, echarts, assignRouter) {
    return ['$scope', '$state', '$location', '$sce', 'ngDialog', 'paperService', 'managerUrl', 'apiCommon',
        function ($scope, $state, $location, $sce, ngDialog, paperService, managerUrl, apiCommon) {
            $scope.knowledgedetails = false;
            $scope.titledetails = false;
            var ExamFlnkID = $location.search().ExamFlnkID,
                ClassFlnkID = $location.search().ClassFlnkID,
                user = angular.fromJson(sessionStorage.getItem('currentUser'));
            //初始化班级
            apiCommon.classSimpleDetail().success(function (data) {
                $scope.classList = data.msg;
                if (_.isArray($scope.classList)) {
                    if(ClassFlnkID) {
                        $scope.currentClassid = ClassFlnkID;
                    }else {
                        $scope.currentClassid = $scope.classList[0].classId;
                    }
                    getExam();
                }
            });
            // $scope.newArray = [{Title:'初一7',Orders:'2'},{Title:'初一6',Orders:'5'},
            //     {Title:'初一5',Orders:'3'},{Title:'初一4',Orders:'4'},{Title:'初一3',Orders:'1'},{Title:'初一1',Orders:'7'}];
            // $scope.kks = 'Orders';
            //获取更多班级排名
            $scope.clickMore = function () {
                getMoreClassOrder();
                ngDialog.open({
                    template:'<div class="row text-center clickMore-title"><div class="col-md-6">班级</div><div class="col-md-6">年级排名</div></div>'+
                    '<div><div class="row text-center clickMore-body" ng-repeat="order in newArray | orderBy:classOrder">' +
                    '<div class="col-md-6" ng-bind="order.classname"></div><div class="col-md-6" ng-bind="order.classorder"></div></div></div>',
                    className:'ngdialog-theme-default ngdialog-clickMore',
                    scope:$scope,
                    plain : true
                })
            };
            function getMoreClassOrder() {
                console.log($scope.currentTaskFid);
                apiCommon.generalQuery({
                    Proc_name : 'Proc_ClassAvgOrder',
                    taskflnkid:$scope.currentTaskFid
                }).success(function (data) {
                    $scope.newArray = data.msg;
                    $scope.classOrder = 'classorder'
                })
            }
            //试卷列表
            function getExam() {
                apiCommon.examList({
                    ClassFlnkid: $scope.currentClassid
                }).success(function (data) {
                    $scope.testList = data.msg;
                    if (_.isArray($scope.testList) && $scope.testList.length) {
                        $scope.noExam = true;
                        var curExam;
                        if(ExamFlnkID) {
                            _.each($scope.testList, function (item) {
                                if(item.ExamFlnkID === ExamFlnkID) {
                                    curExam = item;
                                }
                            });
                        }else {
                            curExam = $scope.testList[0];
                        }
                        if(curExam) {
                            $scope.currentExamFlnkId = curExam.ExamFlnkID;
                            $scope.currentExId = curExam.ExamFlnkID;
                            $scope.currentTaskFid = curExam.TaskFId;
                            getExamDetail();
                        }
                    } else {
                        $scope.noExam = false;
                    }
                })
            }

            //试卷详细信息
            function getExamDetail() {
                apiCommon.examDetail({
                    ClassFlnkid: $scope.currentClassid,
                    ExamFlnkID: $scope.currentExamFlnkId
                }).success(function (data) {
                    if (_.isArray(data.msg)) {
                        $scope.ExamDetail = data.msg[0];
                        getclassbat();
                        getclassrabk();
                        geterrorlist();
                        getpiepicture();
                    }
                })
            }

            //班级年级对比
            function getclassbat() {
                apiCommon.classGradeContrast({
                    ClassFlnkid: $scope.currentClassid,
                    ExamFlnkID: $scope.currentExamFlnkId
                }).success(function (data) {
                    if (_.isArray(data.msg)) {
                        $scope.nopaperList = false;
                        $scope.paperList = _.map(data.msg, function (val) {
                            return {
                                'AvgScores': val.AvgScores,
                                'ERate': val.ERate,
                                'MaxSc': val.MaxSc,
                                'Orders': val.Orders,
                                'QRate': val.QRate + '%',
                                'Title': val.Title,
                                'TypeID': val.TypeID
                            }
                        });
                    } else {
                        $scope.nopaperList = true;
                    }
                })
            }

            //得分排名
            function getclassrabk() {
                apiCommon.scoreRanking({
                    ClassFlnkid: $scope.currentClassid,
                    ExamFlnkID: $scope.currentExamFlnkId
                }).success(function (data) {
                    if (_.isArray(data.msg)) {
                        $scope.t1List = _.filter(data.msg, function (item) {
                            return item.TypeID === '1';
                        });
                        $scope.t2List = _.filter(data.msg, function (item) {
                            return item.TypeID === '2';
                        });
                        $scope.t3List = _.filter(data.msg, function (item) {
                            return item.TypeID === '3';
                        });
                        $scope.t4List = _.filter(data.msg, function (item) {
                            return item.TypeID === '4';
                        });
                    }
                })
            }

            //初始化错题
            function geterrorlist() {
                // $http.post($rootScope.baseUrl + '/Interface0161.ashx', {
                //     ClassFlnkid: $scope.currentClassid,
                //     ExamFlnkID: $scope.currentExamFlnkId,
                //     Type: 0
                // }).success(function (data) {
                //     if (_.isArray(data.msg)) {
                //         var groupByQstList = _.filter(data.msg, function (item) {
                //             if (item.Knowledges === '') {
                //                 item.Knowledges = '——';
                //             }
                //             if (item.Source === '') {
                //                 item.Source = '——';
                //             }
                //             return +item.LostSc !== 0;
                //         });
                //         if(_.isArray(groupByQstList)&&groupByQstList.length){
                //             $scope.nogroupByQstList =false;
                //             groupByQstList = groupByQstList.slice(0, 10);
                //             groupByQstList = _.groupBy(groupByQstList, 'Dtype');
                //             var Dtype = _.keys(groupByQstList);
                //             var questionCon = _.values(groupByQstList);
                //             $scope.questionList = _.map(Dtype, function (item, i) {
                //                 var questions = questionCon[i];
                //                 var childen = _.map(questions, function (val) {
                //                     return {
                //                         'Serial': val.Serial,
                //                         'Title': $sce.trustAsHtml(val.Title),
                //                         'ClassErrorRate': val.ClassErrorRate + '%',
                //                         'GradeErrorRate': val.GradeErrorRate + '%',
                //                         'Source': $sce.trustAsHtml(val.Source),
                //                         'LostSc': val.LostSc,
                //                         'Score': val.Score,
                //                         'Knowledges': $sce.trustAsHtml(val.Knowledges),
                //                         'Option_a': $sce.trustAsHtml(val.Option_a),
                //                         'Option_b': $sce.trustAsHtml(val.Option_b),
                //                         'Option_c': $sce.trustAsHtml(val.Option_c),
                //                         'Option_d': $sce.trustAsHtml(val.Option_d)
                //                     }
                //                 });
                //                 return {
                //                     'Dtype': item,
                //                     'questionBox': childen
                //                 }
                //             });
                //         }else {
                //             $scope.nogroupByQstList =true;
                //         }
                //
                //     }
                // })

                paperService.getPaperDataById($scope.currentExamFlnkId).then(function(res){
                    var paperMeta = res.data.msg;
					apiCommon.errorQuestion({
						ClassFlnkid: $scope.currentClassid,
						ExamFlnkID: $scope.currentExamFlnkId,
						Type: 0
					}).then(function(resp){
						var errorQuestions = [];
					    if(resp.data.code === 0) {
					        var allErrors = _.filter(resp.data.msg, function(item){
					            return +item.clostrate > 0;
                            });
					        allErrors = allErrors.slice(0, 10);
					        _.each(allErrors, function(errorItem){
					            var group = _.find(paperMeta, function(g){
					                return g.Dtype === errorItem.dtype;
                                });
					            var tempG = _.find(errorQuestions, function(item){
					                return item.Dtype === group.Dtype;
                                });
					            if(!tempG) {
									tempG = {Dtype: errorItem.dtype, question: []};
									errorQuestions.push(tempG);
                                }
					            var qst = _.find(group.question, function(q){
                                    if(q.isMain && q.sub.length){
                                        var isSub = false;
                                        _.each(q.sub, function (sub) {
                                            if(sub.QFLnkID === errorItem.qflnkid || sub.FLnkID === errorItem.qflnkid) {
                                                isSub = true;
                                                if(sub.Mode === 'a' || sub.Mode === 'A') {
                                                    errorItem.Serial = sub.orders ? sub.orders : sub.dis;
                                                } else {
                                                    errorItem.Serial = sub.dis ? sub.dis : (sub.orders + '.' + sub.SubOrder);
                                                }
                                                errorItem.Title = q.title + sub.title;
                                                errorItem.Knowledges = sub.knowledges;
                                                errorItem.Option_a = $sce.trustAsHtml(sub.OptionOne);
                                                errorItem.Option_b = $sce.trustAsHtml(sub.OptionTwo);
                                                errorItem.Option_c = $sce.trustAsHtml(sub.OptionThree);
                                                errorItem.Option_d = $sce.trustAsHtml(sub.OptionFour);
                                            }
                                        });
                                        return isSub;
                                    } else {
                                        if(q.QFLnkID === errorItem.qflnkid) {
                                            errorItem.Serial = q.orders;
                                            errorItem.Title = q.title;
                                            errorItem.Knowledges = q.knowledges;
                                            errorItem.Option_a = $sce.trustAsHtml(q.OptionOne);
                                            errorItem.Option_b = $sce.trustAsHtml(q.OptionTwo);
                                            errorItem.Option_c = $sce.trustAsHtml(q.OptionThree);
                                            errorItem.Option_d = $sce.trustAsHtml(q.OptionFour);
                                        }
                                        return q.QFLnkID === errorItem.qflnkid
                                    }
                                });
                                if(qst) {
                                    errorItem.Source = qst.source || res.data.examName;

                                    qst && tempG.question.push(errorItem);
                                }
                            });
                        }
                        $scope.questionList = errorQuestions;
                	})
            	});
			}

            //饼状图
            function getpiepicture() {
                apiCommon.scoreDistribution({
                    ClassFlnkid: $scope.currentClassid,
                    ExamFlnkID: $scope.currentExamFlnkId
                }).success(function (data) {
                    if (_.isArray(data.msg)) {
                        var datapie1 = [], datapie2 = [], datapie3 = [];
                        //学生成绩分布
                        $scope.pie1 = _.filter(data.msg, function (item) {
                            return item.TypeID === '1';
                        });
                        if (_.isArray($scope.pie1) && $scope.pie1.length) {
                            $scope.datapie1 = false;
                            _.each($scope.pie1, function (item) {
                                if (item.fScores !== "0.0") {
                                    datapie1.push({'value': '' + item.fScores + '', 'name': '' + item.Title + ''})
                                }
                            });
                            // 绘制图表
                            echarts.init(document.getElementById('container')).setOption({
                                tooltip: {
                                    trigger: 'item',
                                    formatter: "{a} <br/>{b}: {d}%"
                                },
                                legend: {
                                    x: 'left',
                                    y: 'center',
                                    orient: 'vertical',
                                    data: datapie1
                                },
                                calculable: true,
                                series: [{
                                    name: '学生成绩分布',
                                    type: 'pie',
                                    radius: '65%',
                                    center: ['65%', '50%'],
                                    color: ["#5DC7C7", "#989CFF", "#F95659", "#3F80C4", "#8CC43F"],
                                    // labelLine: {normal: {show: false}},
                                    itemStyle: {
                                        normal: {
                                            label: {
                                                show: true,
                                                formatter: function (d) {
                                                    return d.percent.toFixed(0) + '%';
                                                },
                                                position: 'inner',
                                                textStyle: {
                                                    fontSize: '10'
                                                }
                                            }
                                        }
                                    },
                                    data: datapie1
                                }]
                            });
                        } else {
                            $scope.datapie1 = true;
                        }
                        //成绩分析
                        $scope.pie2 = _.filter(data.msg, function (item) {
                            return item.TypeID === '2';
                        });
                        if (_.isArray($scope.pie2) && $scope.pie2.length) {
                            $scope.datapie2 = false;
                            _.each($scope.pie2, function (item) {
                                if (item.fScores !== "0.0") {
                                    datapie2.push({'value': '' + item.fScores + '', 'name': '' + item.Title + ''})
                                }
                            });
                            // 绘制图表
                            echarts.init(document.getElementById('containers')).setOption({
                                tooltip: {
                                    trigger: 'item',
                                    formatter: "{a} <br/>{b}: {d}%"
                                },
                                series: [
                                    {
                                        name: '成绩分析',
                                        type: 'pie',
                                        radius: '65%',
                                        color: ["#5DC7C7", "#989CFF", "#F95659", "#3F80C4", "#8CC43F"],
                                        // labelLine: {normal: {show: false}},
                                        itemStyle: {
                                            normal: {
                                                label: {
                                                    show: true,
                                                    position: 'inner',
                                                    formatter: "{b}: {c}\n占比：{d}%",
                                                    textStyle: {
                                                        fontSize: '10'
                                                    }
                                                }
                                            }
                                        },
                                        data: datapie2
                                    }
                                ]
                            });
                        } else {
                            $scope.datapie2 = true;
                        }
                        //丢失知识点分布
                        $scope.pie3 = _.filter(data.msg, function (item) {
                            return item.TypeID === '3';
                        });
                        if (_.isArray($scope.pie3) && $scope.pie3.length) {
                            $scope.datapie3 = false;
                            $scope.pie3.sort(function (a, b) {
                                return +a.Serial - +b.Serial
                            });

                            $scope.otners = '0';
                            for (var i = 4; i < $scope.pie3.length; i++) {
                                $scope.otners = +$scope.otners + +$scope.pie3[i].fScores;
                            }
                            if ($scope.pie3.length < 6) {
                                $scope.pie3 = $scope.pie3.slice(0, 5);
                            } else {
                                $scope.pie3 = $scope.pie3.slice(0, 4);
                                $scope.pie3.push({'fScores': '' + $scope.otners + '', 'Title': '' + '其他' + ''});
                            }

                            _.each($scope.pie3, function (item) {
                                if (item.fScores !== "0.0") {
                                    datapie3.push({'value': '' + item.fScores + '', 'name': '' + item.Title + ''})
                                }
                            });
                            datapie3 = _.sortBy(datapie3, function (item) {
                                return -item.name.length;
                            });
                            // 绘制图表
                            echarts.init(document.getElementById('knows')).setOption({
                                tooltip: {
                                    trigger: 'item',
                                    formatter: "{a} <br/>{b}: {d}%"
                                },
                                legend: {
                                    x: 'left',
                                    width: '30px',
                                    orient: 'vertical',
                                    data: datapie3
                                },
                                series: [{
                                    name: '丢分知识点分布',
                                    type: 'pie',
                                    center: ['65%', '50%'],
                                    radius: ['40%', '65%'],
                                    color: ["#5DC7C7", "#989CFF", "#F95659", "#3F80C4", "#8CC43F"],
                                    // labelLine: {normal: {show: false}},
                                    itemStyle: {
                                        normal: {
                                            label: {
                                                show: true,
                                                formatter: function (d) {
                                                    return d.percent.toFixed(0) + '%';
                                                },
                                                position: 'inner',
                                                textStyle: {
                                                    fontSize: '10'
                                                }
                                            }
                                        }
                                    },
                                    data: datapie3
                                }]
                            });
                        } else {
                            $scope.datapie3 = true;
                        }
                    }
                })
            }

            //切换班级
            $scope.selectclass = function (val) {
                if ($scope.currentClassid === val) {
                    return;
                }
                $scope.currentClassid = val;
                getExam();
            };
            //切换试卷
            $scope.selectpaper = function (val) {
                if ($scope.currentExamFlnkId === val.ExamFlnkID) {
                    return;
                }
                $scope.currentExamFlnkId = val.ExamFlnkID;
                $scope.currentTaskFid = val.TaskFId;
                getExamDetail();
            };
            //按钮成绩报告
            $scope.gotoscorereport = function () {
                location.href = "#/scorereport?ClassFlnkID=" + $scope.currentClassid + "&ExamFlnkID=" + $scope.currentExamFlnkId;
            };
            //按钮试题讲解
            $scope.gotoViewAllQuestions = function () {
                location.href = "#/ViewAllQuestions?ClassFlnkID=" + $scope.currentClassid + "&ExamFlnkID=" + $scope.currentExamFlnkId;
            };
            //按钮分析报表
            $scope.scoreprint = function () {
                var paper = _.find($scope.testList, function (paper) {
                    return paper.ExamFlnkID === $scope.currentExamFlnkId
                });
                window.open(managerUrl + '/class-grid/index/index_new.html#' + '/?schoolId=' + user.schoolFId +
                    '&paperId=' + $scope.currentExamFlnkId + '&session=' + paper.Period + '&classId=' + $scope.currentClassid);
            };
            //按钮查看试卷
            $scope.viewpaper = function () {
                window.open("#/composing?ExamFlnkID=" + $scope.currentExamFlnkId + "&ClassFlnkID=" + $scope.currentClassid + '&isOpenNewWin=' + 1);
            };
            //查看全部错题
            $scope.gotoViewAllWrongQuestions = function () {
                location.href = "#/WrongAllQuestions?ClassFlnkID=" + $scope.currentClassid + "&ExamFlnkID=" + $scope.currentExamFlnkId;
            };
            //跳转个人信息
            $scope.gouserinfos = function (AFLnkID, SName,StuFLnkID) {
                console.log(AFLnkID + '--' + 'SName');
                $state.go('myApp.Perforsis', {
                    ExAnswerFlnkID: AFLnkID,
                    ClassFlnkid: $scope.currentClassid,
                    ExamFlnkID: $scope.currentExamFlnkId,
                    MyName: SName,
                    userFlnkid:StuFLnkID,
                    subjectId:user.subjectId || ''
                })
            };
            //tips
            $scope.moveOver = function () {
                $("[data-toggle='tooltip']").tooltip({
                    html: true
                });
            };

            $scope.titletips = function (sul, sub) {
                if (sul.Option_a || sul.Option_b || sul.Option_c || sul.Option_d) {
                    return '<div class="divr">' + sul.Title +
                        '<div class="Optionbr"><p>A、</p>' + sul.Option_a + '</div>' +
                        '<div class="Optionbr"><p>B、</p>' + sul.Option_b + '</div>' +
                        '<div class="Optionbr"><p>C、</p>' + sul.Option_c + '</div>' +
                        '<div class="Optionbr"><p>D、</p>' + sul.Option_d + '</div></div>';
                } else {
                    return '<div class="divr">' + sul.Title + '</div>';
                }
            };
            $scope.ScoresFun = function (Scores) {
                if (Scores < 0) {
                    return -Scores;
                } else {
                    return Scores;
                }
            };
            // 滑动事件
            var $cur = 1;
            var $w = $('.moveBox').width();
            $scope.scrollToLeft = function () {
                var $len = $('.box ul li').length;
                var $pages = Math.ceil($len / 4);
                $('.box ul').css('width', $len * 207.5);
                if ($cur == 1) {
                    $('.box ul').stop(false, true).animate({left: '-=' + $w * ($pages - 1)});
                    $cur = $pages;
                } else {
                    $('.box ul').stop(false, true).animate({left: '+=' + $w});
                    $cur--;
                }
            };
            $scope.scrollToRight = function () {
                var $len = $('.box ul li').length;
                var $pages = Math.ceil($len / 4);
                $('.box ul').css('width', $len * 207.5);
                if ($cur == $pages) {
                    $('.box ul').stop(false, true).animate({left: 0});
                    $cur = 1;
                } else {
                    $('.box ul').stop(false, true).animate({left: '-=' + $w});
                    $cur++;
                }
            };
        }]
});