/**
 * Created by Administrator on 2016/6/14.
 */
define(["underscore-min", "star-rating", 'jquery.datetimepicker'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$sce', '$q', 'constantService', 'ngDialog', '$location', '$timeout',
        function ($http, $scope, $rootScope, $state, $sce, $q, constantService, ngDialog, $location, $timeout) {
            $scope.tablethKeys = ['examName', 'status'];
            $scope.currentTab = 3;
            $http.post($rootScope.baseUrl + '/Interface0150.ashx').then(function (res) {
                if (_.isArray(res.data.msg)) {
                    $scope.klassList = res.data.msg;
                    //初始化搜索一次
                    $scope.search();
                }
            });

            $scope.search = function () {
                var defer = $q.defer();
                if($scope.currentTab === 1) {
                    //查询课堂评测列表
                    defer = $http.post($rootScope.baseUrl + '/Interface0268.ashx', {
                        examName: $scope.searchKey || ''
                    });
                }else if($scope.currentTab === 3){
                    //查询机房评测列表
                    defer = $http.post($rootScope.baseUrl + '/Interface0268A.ashx', {
                        examName: $scope.searchKey || '',
                        exType: '机房评测'
                    });
                } else {
                    //查询作业列表
                    defer = $http.post($rootScope.baseUrl + '/Interface0268A.ashx', {
                        examName: $scope.searchKey || '',
                        exType: '作业'
                    });
                }
                defer.then(function (res) {
                    if (_.isArray(res.data.msg)) {
                        $scope.paperList = _.map(res.data.msg, function (item) {
                            /**
                             * 判断有没有班级没考过这份试卷，全考过则不展示评测按钮
                             * 考虑到可能异常情况中断考试，老师可能重新组织评测，暂时屏蔽
                             */
                            var testedClass = _.pluck(item.lstClass, 'classFlnkID');
                            var haveClassNotTest = !!_.find($scope.klassList, function (k) {
                                return $.inArray(k.classId, testedClass) === -1;
                            });
                            if(item.lstClass.length){
                                item.status = '1';
                            }else {
                                item.status = '0';
                            }
                            if(+item.status === 1){
                                var curStatus = '';
                                _.each(item.lstClass, function (itemClass) {
                                    if(+itemClass.status === 1){
                                        curStatus = 1;
                                    }
                                });
                                if(curStatus === 1){
                                    item.status = '1';
                                }else {
                                    item.status = '2';
                                }
                            }
                            return {
                                id: item.examFlnkID,
                                examName: item.examName,
                                classNames: _.pluck(item.lstClass, 'className').join(','),
                                exType: item.exType,
                                status: item.status,
                                modifyTime: item.updateDate,
                                lstClass: item.lstClass,
                                haveClassNotTest: true,
                                spendTime: item.spendTime
                            };
                        });
                    } else {
                        $scope.paperList = [];
                    }
                });
            };

            $scope.setTab = function (tab) {
                $scope.currentTab = +tab;
                $scope.search();
            };

            $scope.startTesting = function (paper) {
                if($scope.currentTab === 1) {
                    if ($scope.klassList.length > 1) {
                        ngDialog.open({
                            template: 'selectClass.html',
                            className: 'ngdialog-theme-default',
                            appendClassName: 'select-class-dialog',
                            closeByDocument: true,
                            scope: $scope,
                            controller: function ($scope) {
                                return function () {
                                    $scope.list = $scope.klassList;
                                    $scope.useType = 'EVALUATE';
                                    $scope.chosenClass = $scope.list[0];
                                    $scope.choseKlass = function (klass) {
                                        $scope.chosenClass = klass;
                                    };
                                    $scope.doStartTest = function (klass) {
                                        ngDialog.closeAll();
                                        window.open('#/startevaluating?examId=' + paper.id + '&classId=' + klass.classId +
                                            '&gradeNum=' + $scope.chosenClass.gradeNo + '&smallNum=' + $scope.chosenClass.smallNo + '&from=' + location.href + '&isOpenNewWin=' + 1);
                                    };
                                }
                            }
                        });
                    } else {
                        var currentClass = $scope.klassList[0];
                        window.open('#/startevaluating?examId=' + paper.id + '&classId=' + currentClass.classId +
                            '&gradeNum=' + currentClass.gradeNo + '&smallNum=' + currentClass.smallNo + '&from=' + location.href + '&isOpenNewWin=' + 1) ;
                    }
                }else {
                    //currentTab = 2开始机房评测
                    startComputerRoomTesting(paper);
                }
            };

            $scope.modifyPaper = function (paper) {
                $state.go('myApp.paperComposing', {
                    examId: paper.id,
                    action: 3
                });
            };

            $scope.viewAnalysis = function (paper) {
                if ($scope.klassList.length > 1) {
                    ngDialog.open({
                        template: 'selectClass.html',
                        className: 'ngdialog-theme-default',
                        appendClassName: 'select-class-dialog',
                        closeByDocument: true,
                        scope: $scope,
                        controller: function ($scope) {
                            return function () {
                                var testedClass = _.pluck(paper.lstClass, 'classFlnkID');
                                $scope.list = _.filter($scope.klassList, function (item) {
                                    return $.inArray(item.classId, testedClass) !== -1;
                                });
                                $scope.useType = 'ANALYSIS';
                                $scope.chosenClass = $scope.list[0];
                                $scope.choseKlass = function (klass) {
                                    $scope.chosenClass = klass;
                                };
                                $scope.doStartTest = function (klass) {
                                    ngDialog.closeAll();
                                    $state.go('myApp.analysis.testpaper', {
                                        ClassFlnkID: klass.classId,
                                        ExamFlnkID: paper.id
                                    });
                                };
                            }
                        }
                    });
                } else {
                    var currentClass = $scope.klassList[0];
                    $state.go('myApp.analysis.testpaper', {
                        ClassFlnkID: currentClass.classId,
                        ExamFlnkID: paper.id
                    });
                }
            };

            function startComputerRoomTesting(paper){
                //跳转机房评测老师页面
                $state.go('myApp.tRoomEvaluate', {
                    ExamFlnkID: paper.id,
                    exType: $scope.currentTab === 2 ? '作业' : '机房评测'
                });
            }
            $scope.setTesting = function(paper) {
                $scope.classeList = $scope.klassList;
                $scope.classListAll = _.filter($scope.klassList, function (item) {
                    return item.classId !== '1';
                });
                if($scope.classeList[0].classId !== '1' && $scope.klassList.length > 1){
                    $scope.classeList.unshift({
                        classId: '1',
                        name: '全部'
                    });
                }
                $scope.curClasses = $scope.classeList[0];
                //测试时间
                $scope.setTime = {
                    timeLength: +paper.spendTime,
                    s_date: '',
                    s_date_H: '',
                    s_date_M: '',
                    e_date: '',
                    e_date_H: '',
                    e_date_M: ''
                };
                ngDialog.open({
                    template: 'setTestTime.html',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'ngdialog-theme-setTestTime',
                    scope: $scope,
                    controller: function($scope){
                        return function(){
                            _.each($scope.classeList, function (respe) {
                                respe.isDown = false;
                            });
                            _.each(paper.lstClass, function (resp) {
                                _.each($scope.classeList, function (respe) {
                                    if(respe.classId === resp.classFlnkID) {
                                        respe.isDown = true;
                                    }
                                })
                            });
                            $scope.selectClass = function (value, index) {
                                //设置默认时间
                                if($scope.curClasses === value && index !== 1) {
                                    return
                                }
                                $scope.curClasses = value;
                                check();
                            };
                            $scope.selectClassAll = function (value) {

                            };
                            $scope.selectClass($scope.curClasses, 1);
                            function check() {
                                $scope.setTime.timeLength = +paper.spendTime;
                                if(($scope.curClasses && $scope.curClasses.classId !== '1') ||(paper.lstClass.length > 1 && (paper.lstClass.length === $scope.classeList.length - 1))){
                                    var taskFlnkidList = [];
                                    $http.get('/BootStrap//Interface/getSysTime.ashx').then(function(resp){
                                        if($scope.curClasses.classId === '1') {
                                            _.each(paper.lstClass, function (item, index) {
                                                taskFlnkidList.push(item.taskFlnkid);
                                            });
                                        }else {
                                            _.each(paper.lstClass, function (itemClass) {
                                                if(itemClass.classFlnkID === $scope.curClasses.classId){
                                                    taskFlnkidList.push(itemClass.taskFlnkid);
                                                }
                                            });
                                            if(taskFlnkidList.length === 0) {
                                                var sysTime = resp.data,
                                                    sysTimeMark = new Date(resp.data),
                                                    minLen = +sysTime.slice(14, 16),
                                                    s_date_ss, e_date_ss;

                                                if(minLen <= 30) {
                                                    s_date_ss = new Date((sysTimeMark.getTime() + (30 - minLen) *60 *1000));
                                                    s_date_ss = getTodayDate(s_date_ss, 1);
                                                } else {
                                                    s_date_ss = new Date((sysTimeMark.getTime() + (60 - minLen) *60 *1000));
                                                    s_date_ss = getTodayDate(s_date_ss, 1);
                                                }
                                                if($scope.setTime.timeLength) {
                                                    e_date_ss = new Date(new Date(s_date_ss).getTime() + $scope.setTime.timeLength *60 *1000);
                                                    e_date_ss = getTodayDate(e_date_ss, 1);
                                                    $scope.e_date = e_date_ss.slice(0, 10);
                                                    $scope.setTime.e_date_H = +e_date_ss.slice(11, 13);
                                                    $scope.setTime.e_date_M = +e_date_ss.slice(14, 16);
                                                }
                                                s_date_ss = getTodayDate(s_date_ss, 1);
                                                $scope.s_date = s_date_ss.slice(0, 10);
                                                $scope.setTime.s_date_H =  +s_date_ss.slice(11, 13);
                                                $scope.setTime.s_date_M = +s_date_ss.slice(14, 16);
                                                return
                                            }
                                        }
                                        $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                                            Proc_name:'Proc_QueryTask',
                                            FLnkID : taskFlnkidList.join(',')
                                        }).success(function (res) {
                                            if (_.isArray(res.msg)) {
                                                var searchResult = res.msg;
                                                if(searchResult.length){
                                                    var num = 0, len = searchResult.length;
                                                    for(var i =0; i < len - 1; i++) {
                                                        if (searchResult[i].ExamTime === searchResult[i + 1].ExamTime && searchResult[i].StopTime === searchResult[i + 1].StopTime && searchResult[i].SpendTime === searchResult[i + 1].SpendTime) {
                                                            num += 1;
                                                        }
                                                    }
                                                    if(num !== len - 1) {
                                                        $scope.setTime = {
                                                            timeLength: '',
                                                            s_date_H: '',
                                                            s_date_M: '',
                                                            e_date_H: '',
                                                            e_date_M: ''
                                                        };
                                                        $scope.e_date = '';
                                                        $scope.s_date = '';
                                                    } else {
                                                        var curTime = searchResult[0];
                                                        $scope.setTime.timeLength = +curTime.SpendTime;
                                                        if(curTime.ExamTime) {
                                                            $scope.s_date = curTime.ExamTime.slice(0, 10);
                                                            $scope.setTime.s_date_H = +curTime.ExamTime.slice(11, 13);
                                                            $scope.setTime.s_date_M = +curTime.ExamTime.slice(14, 16);
                                                        }
                                                        if(curTime.StopTime) {
                                                            $scope.e_date = curTime.StopTime.slice(0, 10);
                                                            $scope.setTime.e_date_H = +curTime.StopTime.slice(11, 13);
                                                            $scope.setTime.e_date_M = +curTime.StopTime.slice(14, 16);
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                    });
                                }
                            }
                            $scope.startSave = function () {
                                $scope.e_date = $('#end_datetimepicker').val();
                                $scope.s_date = $('#start_datetimepicker').val();
                                var today = getTodayDate(1),
                                    classIdList = [],
                                    examLengthCount;
                                if((!$scope.setTime.s_date_H && $scope.setTime.s_date_H !== 0) || (!$scope.setTime.s_date_M && $scope.setTime.s_date_M !== 0) || (!$scope.setTime.e_date_H && $scope.setTime.e_date_H !== 0) || (!$scope.setTime.e_date_M && $scope.setTime.e_date_M !== 0)) {
                                    constantService.alert('请设置时间！');
                                    return false;
                                }else{
                                    var start = $scope.s_date.slice(8, 10) *24 * 60 + $scope.setTime.s_date_H * 60 + $scope.setTime.s_date_M,
                                        end  = $scope.e_date.slice(8, 10) *24* 60 + $scope.setTime.e_date_H *60 + $scope.setTime.e_date_M;
                                    examLengthCount = end - start;
                                    $scope.setTime.startTime = $scope.s_date + ' ' + twoBoth($scope.setTime.s_date_H) + ':' + twoBoth($scope.setTime.s_date_M) + ':00';
                                    $scope.setTime.endTime = $scope.e_date + ' ' + twoBoth($scope.setTime.e_date_H) + ':' + twoBoth($scope.setTime.e_date_M) + ':00';
                                    var startTime = new Date($scope.setTime.startTime),endTime = new Date($scope.setTime.endTime);
                                    examLengthCount = Math.ceil(endTime.getTime() - startTime.getTime())/1000/60;
                                }
                                $http.get('/BootStrap//Interface/getSysTime.ashx').then(function(resp) {
                                    var SysTime = resp.data;
                                    if ($scope.setTime.endTime <= $scope.setTime.startTime) {
                                        $scope.falseOne = true;
                                        $timeout(function () {
                                            $scope.falseOne = false;
                                        }, 2000);
                                        return false;
                                    } else if (SysTime > $scope.setTime.startTime) {
                                        $scope.falseTwo = true;
                                        $timeout(function () {
                                            $scope.falseTwo = false;
                                        }, 2000);
                                        return false;
                                    } else if (examLengthCount < $scope.setTime.timeLength) {
                                        $scope.falseThree = true;
                                        $timeout(function () {
                                            $scope.falseThree = false;
                                        }, 2000);
                                        return false;
                                    }
                                    if ($scope.curClasses.classId === '1') {
                                        _.each($scope.classeList, function (item, index) {
                                            if (index) {
                                                classIdList.push(item.classId);
                                            }
                                        })
                                    } else {
                                        classIdList.push($scope.curClasses.classId);
                                    }
                                    if (classIdList.length >= 1) {
                                        var className = [], param;

                                        _.each($scope.paperList, function (item) {
                                            if (paper.id === item.id) {
                                                _.each(item.lstClass, function (itemClass) {
                                                    if (itemClass.status === '1' || itemClass.examTime < SysTime)
                                                        className.push(itemClass.className);
                                                })
                                            }
                                        });
                                        param = {
                                            classFLnkIds: classIdList.join(','),
                                            examFLnkId: paper.id,
                                            startExam: $scope.setTime.startTime,
                                            endExam: $scope.setTime.endTime,
                                            examDuration: $scope.setTime.timeLength
                                        };
                                        if (classIdList.length > 1 && className.length) {
                                            constantService.alert('提示信息：您要修改的全部班级中，' + className.join('、') + '班已经完成考试或者学生正在考试中，重新修改启动时间将会重置之前的成绩，请确认是否修改时间？', function () {
                                                startExam(param);
                                            });
                                        } else {
                                            startExam(param);
                                        }

                                    } else {
                                        var param = {
                                            classFLnkIds: classIdList.join(','),
                                            examFLnkId: paper.id,
                                            startExam: $scope.setTime.startTime,
                                            endExam: $scope.setTime.endTime,
                                            examDuration: $scope.setTime.timeLength
                                        };
                                        startExam(param);
                                    }
                                });
                            };
                        };
                    }
                });
                function startExam(param) {
                    $http.post($rootScope.baseUrl + '/Interface0282.ashx', param).then(function(res){
                        if(res.data.code === 0) {
                            ngDialog.close();
                            constantService.alert('考试已成功启动！',function () {
                                $scope.search();
                            });
                        }else {
                            constantService.alert('启动考试失败！');
                        }
                    });
                }
                function getTodayDate(date,index) {
                    var now = new Date(date),
                        year = now.getFullYear(),
                        month = now.getMonth(),
                        day = now.getDate(),
                        hours = now.getHours(),
                        minutes = now.getMinutes();
                    month = ('0' + (month + 1)).slice(-2);
                    if(index === 1) {
                        return year + '-' + month + '-' + twoBoth(day) + ' ' + twoBoth(hours) + ':' + twoBoth(minutes) + ':00';
                    } else {
                        return year + '-' + month + '-' + twoBoth(day);
                    }
                }
                function twoBoth(value) {
                    return ('0' + value).slice(-2);
                }
                $scope.$on('ngDialog.templateLoaded', function(){
                    setTimeout(function(){
                        $('#start_datetimepicker').datetimepicker({
                            lang: "ch",
                            timepicker: false,
                            format: "Y-m-d",
                            todayButton: false,
                            scrollInput: false,
                            onChangeDateTime: function (dp, $input) {
                                $('#start_datetimepicker').datetimepicker('hide');
                            }
                        });
                        $('#end_datetimepicker').datetimepicker({
                            lang: "ch",
                            timepicker: false,
                            format: "Y-m-d",
                            todayButton: false,
                            scrollInput: false,
                            onChangeDateTime: function (dp, $input) {
                                $('#end_datetimepicker').datetimepicker('hide');
                            }
                        });
                    }, 100)
                });
            };
        }
    ]
});
