/**
 * Created by Administrator on 2016/6/18.
 */
define([], function () {
    return ['$scope', '$location', '$http', '$q', 'paperService', 'utilService', 'ngDialog', 'config',
        function ($scope, $location, $http, $q, paperService, utilService, ngDialog, config) {
        var search = $location.search();
        var href = search.from.slice(0, search.from.indexOf('#/'));
        var examId = search.examId;
        var classFLnkId = search.classId;
        var gradeNum = search.gradeNum;
        var smallNum = search.smallNum;
        $('#startevaluating').css('height',$(window).height());
        $(window).off('resize').on('resize', function(e){
            $('#startevaluating').css('height',$(window).height());
        });


        /**
         * 设置状态常量，标识当前步骤
         */
        var SIGN = 'SIGN',                  //签到
            EVALUATING = 'EVALUATING',      //答题
            COUNT = 'COUNT',                //统计
            ANALYSIS = 'ANALYSIS';          //分析
        //是否自动翻页，默认false
        $scope.AUTO_PAGER = false;
        var DEFAULT_PAGER_TIMER = 30;
        $scope.autoPagerInterval = DEFAULT_PAGER_TIMER;
        //计时器
        $scope.totalTime = 0;
        var totalTimeTimer = null, qstTimer = null, signTimer = null, answerTimer = null;
        //初始化默认当前步骤为签到
        $scope.step = SIGN;
        //初始化状态标识，初始化完成后才可以开始评测
        $scope.isDataReady = false;
        //初始化班级和试卷信息，准备签到
        $q.all([$http.post('/BootStrap/Interface/Interface0181.ashx', {
            examId: examId
        }), $http.post('/BootStrap/Interface/Interface0233.ashx', {
            classFLnkId: classFLnkId
        })]).then(function (res) {
            var paperMeta = res[0].data;
            $scope.groups = paperMeta.msg;
            $scope.paperName = paperMeta.examName;
            $scope.paperDiff = +paperMeta.Diff || 1;
            $scope.questionList = getAllPaperQsts($scope.groups);
            $scope.total = $scope.questionList.length;
            if (_.isArray(res[1].data.msg)) {
                $scope.studentList = _.map(res[1].data.msg, function (item) {
                    return {
                        studentFLnkId: item.studentFLnkId,
                        studentName: item.studentName,
                        isSign: false
                    };
                });
            } else {
                $scope.studentList = [];
            }
            //加载成功（获取到试卷信息，获取到班级学生列表）之后，准备签到
            startSign();
        });

        $scope.startEvaluate = function () {
            if ($scope.isDataReady) {
                clearInterval(signTimer);
                $scope.step = EVALUATING;
                $scope.currentQstNo = 0;
                $scope.currentQst = $scope.questionList[$scope.currentQstNo];
                $scope.isFirst = true;
                $scope.isLast = false;
                //开始评测后，启动定时器，统计总用时
                startTotalTimeTimer();
                //开始考试
                beginTesting();
            }
        };

        $scope.next = function () {
            if ($scope.step === EVALUATING) {
                //如果是在评测页面
                if ($scope.currentQstNo === $scope.questionList.length - 1) {
                    //如果当前题目到最后一题，进入统计页面
                    getStuAnswerForQst($scope.currentQst).then(calcPaperScoreRate);
                    $scope.step = COUNT;
                    //到最后一题，结束考试
                    endTesting();
                    clearInterval(answerTimer);
                    clearInterval(qstTimer);
                    clearInterval(totalTimeTimer);
                    $scope.isFirst = false;
                    $scope.isLast = false;
                } else {
                    nextQuestion();
                    $scope.isFirst = false;
                    $scope.isLast = false;
                }
            } else if ($scope.step === COUNT) {
                //如果是在统计页面
                $scope.step = ANALYSIS;
                $scope.isLast = true;
            } else {
                $scope.isLast = true;
            }
        };

        $scope.pre = function () {
            if ($scope.step === EVALUATING) {
                //如果是在评测页面
                if ($scope.currentQstNo === 0) {
                    //如果当前题目到第一题
                    $scope.isFirst = true;
                    $scope.isLast = false;
                } else if ($scope.currentQstNo === 1) {
                    preQuestion();
                    $scope.isFirst = true;
                    $scope.isLast = false;
                } else {
                    preQuestion();
                    $scope.isFirst = false;
                    $scope.isLast = false;
                }
            } else if ($scope.step === COUNT) {
                //如果是在统计页面
                $scope.step = EVALUATING;
                $scope.isLast = false;
                $scope.isFirst = false;
            } else {
                $scope.step = COUNT;
                $scope.isLast = false;
                $scope.isFirst = false;
            }
        };

        $scope.autoPager = function () {
            if ($scope.AUTO_PAGER) {
                clearInterval(qstTimer);
                $scope.AUTO_PAGER = false;
            } else {
                ngDialog.open({
                    template: '<div class="auto-pager-box"><div class="container">' +
                    '自动翻页间隔时间：<input type="text" ng-model="autoPagerInterval">秒</div>' +
                    '<div class="btn btn-ok" ng-click="setAutoPager(autoPagerInterval)">确定</div></div>',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'evaluate-dialog',
                    closeByDocument: true,
                    plain: true,
                    scope: $scope
                });
            }
        };

        /**
         * 设置自动翻页，自动翻页后，启动答题倒计时
         */
        $scope.setAutoPager = function (interval) {
            ngDialog.closeAll();
            $scope.AUTO_PAGER = true;
            $scope.autoPagerInterval = interval;
            startQstTimer();
        };

        $scope.showSubmitStudents = function () {
            ngDialog.open({
                template: 'evaluate/submitStudents.tpl.html',
                className: 'ngdialog-theme-default',
                appendClassName: 'submit-dialog',
                closeByDocument: true,
                scope: $scope
            });
        };

        $scope.showAnswerDetail = function () {
            $http.post('/BootStrap/Interface/Interface0272.ashx', {
                examFId: examId,
                classFId: classFLnkId,
                examName: $scope.paperName,
                gradeNum: gradeNum,
                smallNum: smallNum,
                type: 'B'
            }).then(function () {
                console.log('【暂停任务】');
            });
            ngDialog.open({
                template: 'evaluate/answerDetail.tpl.html',
                className: 'ngdialog-theme-default',
                appendClassName: 'answer-dialog',
                closeByDocument: true,
                scope: $scope,
                controller: function ($scope, $http) {
                    return function () {
                        $http.post('/BootStrap/Interface/Interface0186A.ashx', {
                            ExamFlnkID: examId,
                            ClassFlnkID: classFLnkId,
                            QFlnkID: $scope.currentQst.FLnkID
                        }).then(function (res) {
                            if (_.isArray(res.data.msg)) {
                                $scope.currentQst.stuOptions = res.data.msg;
                                $scope.stuOptionA = _.filter($scope.currentQst.stuOptions, function (item) {
                                    return item.UserAnswer.indexOf('A') >= 0;
                                });
                                $scope.stuOptionB = _.filter($scope.currentQst.stuOptions, function (item) {
                                    return item.UserAnswer.indexOf('B') >= 0;
                                });
                                $scope.stuOptionC = _.filter($scope.currentQst.stuOptions, function (item) {
                                    return item.UserAnswer.indexOf('C') >= 0;
                                });
                                $scope.stuOptionD = _.filter($scope.currentQst.stuOptions, function (item) {
                                    return item.UserAnswer.indexOf('D') >= 0;
                                });
                                $scope.currentQst.correctStus = _.filter($scope.currentQst.stuOptions, function (item) {
                                    return item.UserAnswer !== '' && $scope.currentQst.Answer.indexOf(item.UserAnswer) >= 0;
                                });
                                $scope.currentQst.errorStus = _.filter($scope.currentQst.stuOptions, function (item) {
                                    return item.UserAnswer !== '' && $scope.currentQst.Answer.indexOf(item.UserAnswer) < 0;
                                });
                                $scope.currentQst.correctRate = ($scope.currentQst.correctStus.length / $scope.currentQst.stuOptions.length * 100).toFixed(1);
                            }
                        });

                        $scope.isAnswer = function (param) {
                            var answer = $scope.currentQst.Answer.toUpperCase();
                            var stuAnswer = param.toUpperCase();
                            return answer.indexOf(stuAnswer) >= 0;
                        }
                    };
                }
            });
        };

        $scope.goPaperAnalysis = function () {
            window.open(href + '#/analysis/papertest?ClassFlnkID=' + classFLnkId + '&ExamFlnkID=' + examId + '&isOpenNewWin=' + 1);
        };

        $scope.startTeaching = function () {
            window.open(href + '#/EveryQuestion?ClassFlnkID=' + classFLnkId + '&ExamFlnkID=' + examId + '&isOpenNewWin=' + 1);
        };

        $scope.$watch('studentList', function (newVal) {
            if (newVal) {
                $scope.signNum = _.filter(newVal, function (item) {
                    return item.isSign === true;
                }).length;
                if ($scope.signNum === $scope.studentList.length) {
                    clearInterval(signTimer);
                }
            }
        }, true);

        $scope.$watch('currentQst', function (val) {
            //切换题目时，把题目中的图片放大一倍
            setTimeout(function () {
                $('.qst-box .content img').each(function (item) {
                    var $ele = $(this);
                    if ($ele.width() > 0) {
                        $ele.css({
                            width: $ele.width() * 2 + 'px',
                            height: $ele.height() * 2 + 'px'
                        });
                    } else {
                        $ele.on('load', function () {
                            $ele.css({
                                width: $ele.width() * 2 + 'px',
                                height: $ele.height() * 2 + 'px'
                            });
                        });
                    }
                });
            }, 0);
        });

        function calcPaperScoreRate() {
            var totalRight = 0, totalStus = 0;
            _.each($scope.questionList, function (item) {
                var rlen = 0, tlen = 0;
                if (item.correctStus) {
                    rlen = item.correctStus.length;
                }
                if (item.stuOptions) {
                    tlen = item.stuOptions.length;
                }
                totalRight += rlen;
                totalStus += tlen;
            });
            if (totalStus === 0 && totalRight === 0) {
                $scope.paperScoreRate = 0;
            } else {
                $scope.paperScoreRate = ((totalRight / totalStus) * 100).toFixed(1);
            }
        }

        function startSign() {
            //初始化准备签到
            $http.post('/BootStrap/Interface/Interface0270.ashx', {
                examFId: examId,
                classFId: classFLnkId,
                examName: $scope.paperName,
                gradeNum: gradeNum,
                smallNum: smallNum,
                useNo: $scope.studentList.length,
                maxWait: config.maxWait
            }).then(function (res) {
                //初始化准备签到完成之后，启动定时器，监测签到人数
                if (+res.data.code === 0) {
                    //监测到设备在线才认为数据准备彻底完成，进行下一步签到状态监测
                    $scope.isDataReady = true;
                    clearInterval(signTimer);
                    signTimer = setInterval(function () {
                        getSignStatus();
                    }, config.signInterval);
                    //超过最大等待时间后，清空定时器
                    setTimeout(function () {
                        clearInterval(signTimer);
                    }, (config.maxWait + 5) * 1000);
                } else {
                    utilService.showSimpleText(res.data.msg, 'evaluate-toast');
                }
            }, function () {
                utilService.showSimpleText(res.data.msg, 'evaluate-toast');
            });
        }

        function getSignStatus() {
            return $http({
                method: 'post',
                url: '/BootStrap/Interface/Interface0271.ashx',
                data: {
                    examFId: examId,
                    classFId: classFLnkId,
                    examName: $scope.paperName,
                    gradeNum: gradeNum,
                    smallNum: smallNum
                },
                ignoreLoadingBar: true
            }).then(function (res) {
                if (_.isArray(res.data.msg)) {
                    _.each(res.data.msg, function (item) {
                        if (item.isSign === '1') {
                            var stu = _.find($scope.studentList, function (s) {
                                return s.studentFLnkId === item.userFLnkID;
                            });
                            stu.isSign = true;
                        }
                    });
                }
            });
        }

        function preQuestion() {
            clearInterval(qstTimer);
            $scope.currentQstNo--;
            $scope.currentQst = $scope.questionList[$scope.currentQstNo];
        }

        function nextQuestion() {
            clearInterval(qstTimer);
            //切换题目之前，查询上一题的答题情况，便于后面的分析页面展示
            getStuAnswerForQst($scope.currentQst);
            $scope.currentQst.isOver = true;
            $scope.currentQstNo++;
            $scope.currentQst = $scope.questionList[$scope.currentQstNo];

            //如果设置了自动翻页，则启动自动翻页定时器
            if ($scope.AUTO_PAGER) {
                startQstTimer();
            }
            //切换到下一题，启动统计提交数定时器
            if (!$scope.currentQst.isOver) {
                beginTesting();
            }
        }

        function getStuAnswerForQst(q) {
            //如果题目没有答题结束状态标记，则获取答题情况
            return $http.post('/BootStrap/Interface/Interface0186A.ashx', {
                ExamFlnkID: examId,
                ClassFlnkID: classFLnkId,
                QFlnkID: q.FLnkID
            }).then(function (res) {
                if (_.isArray(res.data.msg)) {
                    q.stuOptions = res.data.msg;
                    q.correctStus = _.filter(q.stuOptions, function (item) {
                        return item.UserAnswer !== '' && q.Answer.indexOf(item.UserAnswer) >= 0;
                    });
                    q.errorStus = _.filter(q.stuOptions, function (item) {
                        return item.UserAnswer !== '' && q.Answer.indexOf(item.UserAnswer) < 0;
                    });
                    q.correctRate = (q.correctStus.length / q.stuOptions.length * 100).toFixed(1);
                    _.each($scope.studentList, function (item) {

                    });
                }
            });
        }

        function beginTesting() {
            var qNo = $scope.currentQst.orders;
            var choiceSize = 4;
            var mayChoiceSize = 1;
            $http.post('/BootStrap/Interface/Interface0269.ashx', {
                examFId: examId,
                classFId: classFLnkId,
                examName: $scope.paperName,
                gradeNum: gradeNum,
                smallNum: smallNum,
                qNo: qNo,
                choiceSize: choiceSize,
                mayChoiceSize: mayChoiceSize,
                canModify: config.canModify
            }).then(function (res) {
                //开始之后，查询答题情况
                getAnswerDetail();
            });
        }

        function getAnswerDetail() {
            clearInterval(answerTimer);
            answerTimer = setInterval(function () {
                if ($scope.currentQst.submitNums && $scope.currentQst.stuAnswerDetail && $scope.currentQst.submitNums >= $scope.currentQst.stuAnswerDetail.length) {
                    clearInterval(answerTimer);
                } else {
                    $http({
                        method: 'post',
                        url: '/BootStrap/Interface/Interface0199.ashx',
                        data: {
                            ExamFLnkID: examId,
                            ClassFLnkID: classFLnkId,
                            qFLnkID: $scope.currentQst.FLnkID
                        },
                        ignoreLoadingBar: true
                    }).then(function (res) {
                        if (_.isArray(res.data.msg)) {
                            var partStudents = _.filter(res.data.msg, function (item) {
                                return +item.IsRight != 2;
                            });
                            $scope.currentQst.submitNums = _.filter(partStudents, function (item) {
                                return +item.IsRight != -1;
                            }).length;
                            $scope.currentQst.stuAnswerDetail = partStudents;
                        }
                    });
                }
            }, config.answerInterval);
        }

        function getAllPaperQsts(groups) {
            var allQsts = [];
            _.each(groups, function (item) {
                _.each(item.question, function (q) {
                    if (q.mode === 'A') {
                        _.each(q.sub, function (s) {
                            s.father = q;
                            s.isSub = true;
                        });
                        allQsts.concat(q.sub);
                    } else if (q.mode === 'B') {
                        allQsts.push(q);
                    } else {
                        allQsts.push(q);
                    }
                });
            });
            return allQsts;
        }

        function startTotalTimeTimer() {
            //清空定时器，清空当前统计时间
            clearInterval(totalTimeTimer);
            //时间单位：秒
            $scope.totalTime = 0;
            totalTimeTimer = setInterval(function () {
                $scope.$apply(function () {
                    $scope.totalTime += 1;
                });
            }, 1000);
        }

        function startQstTimer() {
            clearInterval(qstTimer);
            //如果题目没有测试过，则启动倒计时定时器
            if (!$scope.currentQst.isOver) {
                $scope.currentQst.timeCount = $scope.autoPagerInterval;
                qstTimer = setInterval(function () {
                    if ($scope.currentQst.timeCount > 0) {
                        $scope.currentQst.timeCount--;
                    } else {
                        if ($scope.currentQstNo === $scope.questionList.length - 1) {
                            getStuAnswerForQst($scope.currentQst).then(calcPaperScoreRate);
                            $scope.$apply(function () {
                                $scope.step = COUNT;
                            });
                            //到最后一题，结束考试
                            endTesting();
                            clearInterval(answerTimer);
                            clearInterval(qstTimer);
                            clearInterval(totalTimeTimer);
                        } else {
                            nextQuestion();
                        }
                    }
                }, 1000);
            }
        }

        /**
         * 结束考试
         */
        function endTesting() {
            //结束考试接口
            $http.post('/BootStrap/Interface/Interface0272.ashx', {
                examFId: examId,
                classFId: classFLnkId,
                examName: $scope.paperName,
                gradeNum: gradeNum,
                smallNum: smallNum,
                type: 'A'
            }).then(function () {
                console.log('【考试结束！】');
                //提交试卷分析接口
                $http.post('/BootStrap/Interface/Interface0207.ashx', {
                    classFlnkID: classFLnkId,
                    examFlnkID: examId
                }).then(function () {
                    console.log('提交分析成功');
                });
                //获取学生答题排名
                getStudentOrder();
            });
        }

        function getStudentOrder() {
            $http.post('/BootStrap/Interface/Interface0280.ashx', {
                classFId: classFLnkId,
                examFId: examId
            }).then(function (res) {
                var studentList = res.data.msg;
                $scope.bestStuList = studentList.slice(0, 3);
                var stus = studentList.slice(3);
                var len = stus.length, avgLen = Math.ceil(len / 4);
                $scope.otherStuGroups = [];
                _.each(stus, function (item, index) {
                    item.order = index + 3;
                });
                for (var i = 0; i < 4; i++) {
                    $scope.otherStuGroups.push(stus.slice(i * avgLen, i * avgLen + avgLen));
                }
            });
        }
    }];
});
angular.module('app', [])/*.directive('fontStar', function () {
 return {
 restrict: 'AE',
 replace: true,
 templateUrl: '../directive-extend/template/font-star.html',
 scope: {
 starVal: '=',
 clickCb: '&',
 disable: '=',
 clickable: '='
 },
 controller: function ($scope) {
 $scope.starVal = (+$scope.starVal || 1);
 $scope.starValList = [1, 2, 3, 4, 5];
 if ($scope.disable) {
 $scope.starValList = _.range(1, $scope.starVal + 1);
 }
 $scope.setStarVal = function (item) {
 if ($scope.disable || !$scope.clickable) {
 return;
 }
 $scope.starVal = item;
 }
 }
 }
 })*/.constant('config', {
    maxWait: 3600,                   //签到最大等待时间
    signInterval: 1000,             //签到轮询间隔，1s一次
    answerInterval: 1000,           //查询学生答题情况，1s查询一次
    canModify: 1,                    //能否修改答案
    roomEvaluateQueryInterval: 3000 //机房评测老师端查询答题情况， 3s查询一次
});