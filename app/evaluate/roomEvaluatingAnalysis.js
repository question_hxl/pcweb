/**
 * Created by Administrator on 2016/6/25.
 */
define(["underscore-min", "star-rating"], function () {
    return['$scope','$rootScope','$http','$location','$state',
        function($scope, $rootScope, $http, $location, $state){
        var ExamFlnkID = $location.search().ExamFlnkID,
            ClassFlnkID = $location.search().ClassFlnkID;

        $http.post('/BootStrap/Interface/Interface0280.ashx', {
            classFId: ClassFlnkID,
            examFId: ExamFlnkID
        }).then(function(res){
            var studentList = res.data.msg;
            $scope.bestStuList = studentList.slice(0, 3);
            var stus = studentList.slice(3);
            var len = stus.length, avgLen = Math.ceil(len / 3);
            $scope.otherStuGroups = [];
            _.each(stus, function(item, index){
                item.order = index + 3;
            });
            for(var i = 0 ;i < avgLen; i++) {
                var rowData = [];
                rowData.push(stus[i]);
                if(stus[i + avgLen]) {
                    rowData.push(stus[i + avgLen]);
                }
                if(stus[i + avgLen * 2]) {
                    rowData.push(stus[i + avgLen * 2]);
                }
                $scope.otherStuGroups.push(rowData);
            }
        });

        $http.post('/BootStrap/Interface/Interface0289.ashx', {
            classFLnkId : ClassFlnkID,
            examFLnkId : ExamFlnkID
        }).then(function(res){
            if(_.isArray(res.data.msg)) {
                $scope.questionList = res.data.msg;
                var totalRight = 0, totalError = 0;
                _.each($scope.questionList, function(item){
                    totalRight += +item.goodNum;
                    totalError += +item.lostNum;
                    if((+item.goodNum + +item.lostNum) === 0) {
                        item.correctRate = 0;
                    }else {
                        item.correctRate = Math.round(+item.goodNum / (+item.goodNum + +item.lostNum) * 100);
                    }
                });
                if((totalRight + totalError) === 0) {
                    $scope.paperScoreRate = 0;
                }else {
                    $scope.paperScoreRate = Math.round(totalRight / (totalRight + totalError) * 100);
                }
            }else {
                $scope.questionList = [];
                $scope.paperScoreRate = 0;
            }
        });

        $scope.goPaperAnalysis = function(){
            $state.go('myApp.analysis.testpaper', {
                ClassFlnkID: ClassFlnkID,
                ExamFlnkID: ExamFlnkID
            });
        };
        
        $scope.startTeaching = function(){
            window.open('#/EveryQuestion?ClassFlnkID=' + ClassFlnkID + '&ExamFlnkID=' + ExamFlnkID + '' + '&isOpenNewWin=' + 1);
        };
    }]
});