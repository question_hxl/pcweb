/**
 * Created by 贺小雷 on 2016/6/21.
 */
angular.module('config', [])
    .constant('config' ,{
        maxWait: 3600,                   //签到最大等待时间
        signInterval: 1000,             //签到轮询间隔，1s一次
        answerInterval: 1000,           //查询学生答题情况，1s查询一次
        canModify: 1,                    //能否修改答案
        roomEvaluateQueryInterval: 3000 //机房评测老师端查询答题情况， 3s查询一次
    });