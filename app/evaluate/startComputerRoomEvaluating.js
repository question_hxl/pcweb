/**
 * Created by Administrator on 2016/6/25.
 */
define(["underscore-min", "star-rating"], function () {
    return[ '$scope','$rootScope','$http','$state','$location','$q','constantService','paperService',
        function($scope, $rootScope, $http, $state, $location, $q, constantService, paperService){
        var ExamFlnkID = $location.search().ExamFlnkID,
            exType = $location.search().exType,
            taskFlnkidList,curTaskList, examTime, klassList;
        var queryAnswerStatusInterval = null, timeCountInterval = null;
        var config = {
            roomEvaluateQueryInterval: 60000
        };
        //获取班级列表
        var deferClass = $q.defer(), deferPaper = $q.defer();
        deferClass = $http.post($rootScope.baseUrl + '/Interface0150.ashx').then(function(res){
            klassList = res.data.msg;
            _.each(klassList, function (itemClass) {
                itemClass.status = 0;
            });
        });
        //获取试卷详情
        deferPaper = paperService.getPaperDataById(ExamFlnkID).then(function(res){
            examTime = +res.data.spendtime;
            $scope.paperTime = +res.data.spendtime;
            $scope.paperName = res.data.examName;
            $scope.paperQstNum = paperService.getAllQuestionInPaper(res.data.msg).length;
        });
        $q.all([deferClass, deferPaper]).then(function(res){
            checkStatus();
        }, function(){
            constantService.alert('考试初始化失败！');
        });
        function checkStatus() {
            //获取列表
            $http.post($rootScope.baseUrl + '/Interface0268A.ashx', {
                examName: $scope.searchKey || '',
                exType: exType
            }).success(function (resp) {
                if (_.isArray(resp.msg)) {
                    $scope.klassList = klassList;
                    _.each(resp.msg, function (item) {
                        if(item.examFlnkID === ExamFlnkID){
                            taskFlnkidList = [];
                            _.each(item.lstClass, function (itemTask) {
                                _.each($scope.klassList, function (itemClass) {
                                    if(itemClass.classId === itemTask.classFlnkID) {
                                        if(itemTask.examTime <= item.sysTime) {
                                            itemClass.status = 2;
                                            if(itemTask.status === '1'){
                                                itemClass.status = 1;
                                            }
                                        }
                                    }
                                });
                                taskFlnkidList.push(itemTask.taskFlnkid)
                            });
                            taskFlnkidList = taskFlnkidList.join(',');
                        }
                    });
                    if(!$scope.currentClass) {
                        _.each($scope.klassList, function (item) {
                            item.names = '';
                            if(item.name.indexOf('(未开始)') > 0) {
                                item.name = item.name.substring(0, item.name.indexOf('(未开始)'));
                            } else if(item.name.indexOf('(已测验)') > 0) {
                                item.name = item.name.substring(0, item.name.indexOf('(已测验)'));
                            } else if(item.name.indexOf('(测验中)') > 0) {
                                item.name = item.name.substring(0, item.name.indexOf('(测验中)'));
                            }
                            if(item.status === 0) {
                                item.names = item.name + '(未开始)';
                            } else if (item.status === 1) {
                                item.names = item.name + '(已测验)';
                            }else if (item.status === 2) {
                                item.names = item.name + '(测验中)';
                            }
                        });
                        $scope.currentClass = $scope.klassList[0];
                    } else {
                        _.each($scope.klassList, function (item) {
                            if(item.classId === $scope.currentClass.classId) {
                                if(item.name.indexOf('(未开始)') > 0) {
                                    item.name = item.name.substring(0, item.name.indexOf('(未开始)'));
                                }
                                $scope.currentClass.names = item.name + '(测验中)';
                            }
                        });
                    }
                    if(taskFlnkidList){
                        $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                            Proc_name:'Proc_QueryTask',
                            FLnkID : taskFlnkidList
                        }).success(function (res) {
                            if (_.isArray(res.msg)) {
                                curTaskList = res.msg;
                                getSpendTime(curTaskList);
                            }
                            initExam();
                        });
                    }
                }
            });
        }
        //监听班级变化，获取考试状态，如果开始考试，则展示考试界面
        $scope.$watch('currentClass', function(val){
            if(val && $scope.paperTime) {
                $scope.paperTime = examTime;
                getSpendTime(curTaskList);
                initExam();
            }
        }, true);
        var spendTimeTotal;
        function getSpendTime(curTaskList) {
            $scope.ExamTime = '';
            $scope.StopTime = '';
            _.each(curTaskList, function (item) {
                if($scope.currentClass.classId === item.ClassFLnkID) {
                    $scope.ExamTime = item.ExamTime.slice(0, 16);
                    $scope.StopTime = item.StopTime.slice(0, 16);
                    var FinishDate = new Date(item.StopTime),sysTime = new Date(item.ExamTime);
                    spendTimeTotal = Math.ceil(FinishDate.getTime() - sysTime.getTime())/1000/60;
                    if(!item.SpendTime) {
                        $scope.paperTime = spendTimeTotal;
                    } else {
                        $scope.paperTime = item.SpendTime;
                    }
                }
            });
        }
        //开始考试
        $scope.startTestTask = function(){
            //TODO 调启动考试接口
            $scope.paperTime = $('#paperTime').val();
            var param = {
                classFLnkIds: $scope.currentClass.classId,
                examFLnkId: ExamFlnkID,
                startExam: '',
                endExam: $scope.StopTime,
                examDuration: $scope.paperTime.toString()
            };
            $http.post($rootScope.baseUrl + '/Interface0282.ashx', param).then(function(res){
                if(res.data.code === 0) {
                    $scope.isBegin = true;
                    $scope.leftTime = $scope.paperTime * 60;
                    checkStatus();
                    //TODO 启动定时器，开始倒计时并查询学生答题情况
                    startTimeCounter();
                    //加载当前班级学生列表
                    queryAnswerStatus();
                    clearInterval(queryAnswerStatusInterval);
                    setInterval(queryAnswerStatus, config.roomEvaluateQueryInterval);
                    //获取列表
                    // $http.post($rootScope.baseUrl + '/Interface0268A.ashx', {
                    //     examName: $scope.searchKey || '',
                    //     exType: exType
                    // }).success(function (resp) {
                    //     if (_.isArray(resp.msg)) {
                    //         _.each(resp.msg, function (item) {
                    //             if(item.examFlnkID === ExamFlnkID){
                    //                 taskFlnkidList = [];
                    //                 _.each(item.lstClass, function (itemTask) {
                    //                     taskFlnkidList.push(itemTask.taskFlnkid)
                    //                 });
                    //                 taskFlnkidList = taskFlnkidList.join(',');
                    //             }
                    //         });
                    //         $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    //             Proc_name:'Proc_QueryTask',
                    //             FLnkID : taskFlnkidList
                    //         }).success(function (rese) {
                    //             if (_.isArray(rese.msg)) {
                    //                 curTaskList = rese.msg;
                    //                 getSpendTime(curTaskList);
                    //             }
                    //         });
                    //     }
                    // });

                }else {
                    constantService.alert('启动考试失败！');
                }
            });
        };

        /**
         * 老师收卷
         */
        $scope.forceSubmit = function(){
            constantService.showConfirm('收卷后将结束考试，是否确认收卷？', ['确认'], submitPaperAndAnalysis);
        };

        $scope.viewAnalysis = function(){
            clearInterval(timeCountInterval);
            clearInterval(queryAnswerStatusInterval);
            $state.go('myApp.roomEvaluateAnalysis', {
                ExamFlnkID: ExamFlnkID,
                ClassFlnkID: $scope.currentClass.classId
            });
        };

        function submitPaperAndAnalysis (){
            clearInterval(timeCountInterval);
            clearInterval(queryAnswerStatusInterval);
            $http.post($rootScope.baseUrl + '/Interface0283.ashx', {
                classFLnkId: $scope.currentClass.classId,
                examFLnkId: ExamFlnkID
            }).then(function(res){
                if(res.data.code === 0) {
                    checkStatus();
                    $http.post($rootScope.baseUrl + '/Interface0207.ashx', {
                        classFlnkID: $scope.currentClass.classId,
                        examFlnkID: ExamFlnkID
                    }).then(function(){
                        clearInterval(timeCountInterval);
                        clearInterval(queryAnswerStatusInterval);
                        $state.go('myApp.roomEvaluateAnalysis', {
                            ExamFlnkID: ExamFlnkID,
                            ClassFlnkID: $scope.currentClass.classId
                        });
                    }, function(){
                        constantService.showConfirm('试卷分析失败！');
                    });
                }else {
                    constantService.showConfirm('操作失败，请重试！');
                }
            }, function(res){
                constantService.showConfirm('操作失败，请重试！');
            });
        }

        function initExam(){
            if($scope.currentClass && $scope.paperTime) {
                getExamStatus($scope.currentClass.classId, ExamFlnkID).then(function(res){
                    var beginOffset = Math.ceil(+res.data.time);
                    var status = +res.data.code;
                    if(+res.data.time < 0){
                        status = 2;
                    }
                    if(typeof res.data.code === 'undefined' && typeof res.data.time !== 'undefined') {
                        //考试中
                        $scope.isOver = false;
                        if(exType === '机房评测') {
                            if(beginOffset >= $scope.paperTime * 60) {
                                //TODO 考试结束,如果未收卷，调结束考试接口收卷，否则跳转分析页面
                                $scope.isBegin = false;
                                $scope.leftTime = 0;
                                $scope.isOver = true;
                                // queryAnswerStatus();
                                // submitPaperAndAnalysis();
                            }else if(beginOffset > 0) {
                                //考试开始
                                var leftTime = $scope.paperTime * 60 - beginOffset;
                                $scope.isBegin = true;
                                $scope.leftTime = leftTime > 0 ? leftTime: 0;
                                //TODO 启动定时器检测考试状态
                                startTimeCounter();
                                clearInterval(queryAnswerStatusInterval);
                                queryAnswerStatus();
                                queryAnswerStatusInterval = setInterval(function(){
                                    queryAnswerStatus();
                                },config.roomEvaluateQueryInterval);
                            }else if(status === 2) {
                                //未开始考试
                                $scope.isBegin = false;
                                $scope.isOver = false;
                                $scope.leftTime = $scope.paperTime * 60;
                            }
                        } else {  //作业评测
                            if(beginOffset >= spendTimeTotal * 60) {
                                // 考试结束,如果未收卷，调结束考试接口收卷，否则跳转分析页面
                                $scope.isBegin = false;
                                $scope.leftTime = 0;
                                $scope.isOver = true;
                                // queryAnswerStatus();
                                // submitPaperAndAnalysis();
                            }else if(beginOffset > 0) {
                                //考试开始
                                var leftTime = spendTimeTotal * 60 - beginOffset;
                                $scope.isBegin = true;
                                $scope.leftTime = leftTime > 0 ? leftTime: 0;
                                // 启动定时器检测考试状态
                                startTimeCounter();
                                clearInterval(queryAnswerStatusInterval);
                                queryAnswerStatus();
                                queryAnswerStatusInterval = setInterval(function(){
                                    queryAnswerStatus();
                                },config.roomEvaluateQueryInterval);
                            }else if(status === 2) {
                                //未开始考试
                                $scope.isBegin = false;
                                $scope.isOver = false;
                                $scope.leftTime = $scope.paperTime * 60;
                            }
                        }

                    }else if(status === 2) {
                        //未开始考试
                        $scope.isBegin = false;
                        $scope.isOver = false;
                        $scope.leftTime = $scope.paperTime * 60;
                    }else if(status === 3) {
                        $scope.leftTime = 0;
                        $scope.isOver = true;
                    }
                });
            }
        }

        function startTimeCounter(){
            clearInterval(timeCountInterval);
            timeCountInterval = setInterval(function(){
                $scope.$apply(function(){
                    if($scope.leftTime > 0) {
                        $scope.leftTime -= 1;
                    }else {
                        $scope.leftTime = 0;
                        //考试结束，准备收卷
                        submitPaperAndAnalysis();
                    }
                });
            }, 1000);
        }

        function queryAnswerStatus(){
            getExamStatus($scope.currentClass.classId, ExamFlnkID).then(function(res){
                var stus = res.data.msg;
                setStusStatus(stus);
                if(_.isArray(stus)) {
                    var tLen = stus.length;
                    var leftStuSize = Math.ceil(tLen / 2);
                    $scope.leftStudentList = stus.slice(0, leftStuSize);
                    $scope.rightStudentList = stus.slice(leftStuSize);
                }
            });
        }

        function getExamStatus(classId, examId){
            return $http({
                method: 'post',
                url: $rootScope.baseUrl + '/Interface0284.ashx',
                data: {
                    classFLnkId: classId,
                    examFLnkId: examId
                },
                ignoreLoadingBar: true
            });
        }

        function setStusStatus(stus){
            var counts = _.map(stus, function(item, index){
                return +item.finishCount;
            });
            var minCount = Math.min.apply(this, counts),
                maxCount = Math.max.apply(this, counts);
            _.each(stus, function(item){
                item.percent = (+item.finishCount / +item.count) * 100;
                if(+item.finishCount === minCount) {
                    item.isLast = true;
                    item.isFirst = false;
                }else if(+item.finishCount === maxCount) {
                    item.isFirst = true;
                    item.isLast = false;
                }else {
                    item.isFirst = false;
                    item.isLast = false;
                }
            });
        }
    }];
});