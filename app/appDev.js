// 不需要 mock 的有问题的接口: Interface0031.ashx

'use strict';
var myAppDev = angular.module('myAppDev', ['myApp', 'ngMockE2E'])
    .run(function($httpBackend, baseUrl) {
        var mockInterfaces = [ /*'Interface0030.ashx',*/ /*'Interface0038.ashx',*/
            'Interface0040.ashx',
            'Interface0041.ashx',
            'Interface0042.ashx',
            'Interface0043.ashx', /*'Interface0057.ashx',*/ /*'Interface0058.ashx'*/
            'Interfacexcourselist.ashx'
        ];

        var questionsMockData = {
            msg: [{
                //题目Id
                "questionId": "1",
                //类型
                "questionType": "计算题",
                //问题
                "question": "1-2=3?",
                //题库类型
                "bankType": "系统题库",
                //难度等级
                "level": "1",
                //质量
                "quality": "一般",
                //分数
                "score": "1",
                //答案
                "answer": "answer answer answer",
                //分析
                "analyze": "analyze analyze analyze"
            }, {
                //题目Id
                "questionId": "2",
                //类型
                "questionType": "应用题",
                //问题
                "question": "5+6=11?",
                //题库类型
                "bankType": "系统题库",
                //难度等级
                "level": "1",
                //质量
                "quality": "一般",
                //分数
                "score": "2",
                //答案
                "answer": "",
                //分析
                "analyze": ""
            }, {
                //题目Id
                "questionId": "3",
                //类型
                "questionType": "应用题",
                //问题
                "question": "7+8=15?ok吴开东",
                //题库类型
                "bankType": "系统题库",
                //难度等级
                "level": "1",
                //质量
                "quality": "一般",
                //分数
                "score": "2",
                //答案
                "answer": "",
                //分析
                "analyze": ""
            }]
        };

        // $httpBackend.whenPOST(baseUrl + '/Interface0030.ashx').respond({
        //     //章节对应知识点数组
        //     "msg": [{
        //         //知识点名称
        //         "knowledge": "知识点1",
        //         //知识点性质
        //         "knowledgeNature": "易错点",
        //         //知识点内容
        //         "knowledgeContent": "这是一个容易出错的知识点",
        //         //知识点是否关联,0表示未关联，1表示已关联
        //         "isRelated": 0
        //     }]
        // });

        //$httpBackend.whenPOST(baseUrl + '/Interface0038.ashx').respond(questionsMockData);

        $httpBackend.whenPOST(baseUrl + '/Interface0040.ashx').respond(questionsMockData);

        $httpBackend.whenPOST(baseUrl + '/Interface0041.ashx').respond({
            questionQuantity: [{
                //题型
                "questionType": "多选题",
                //数量
                "questionNum": "10"
            }, {
                //题型
                "questionType": "作图题",
                //数量
                "questionNum": "11"
            }, {
                //题型
                "questionType": "应用题",
                //数量
                "questionNum": "12"
            }]
        });

        $httpBackend.whenPOST(baseUrl + '/Interface0042.ashx').respond(questionsMockData);

        $httpBackend.whenPOST(baseUrl + '/Interface0043.ashx').respond({
            examList: [{
                //试卷ID
                "examId": "1111",
                //试卷名称
                "examName": "第一次考试",
                //耗时
                "time": "120",
                //总分
                "totalScore": "100"
            }, {
                //试卷ID
                "examId": "2222",
                //试卷名称
                "examName": "第2次考试",
                //耗时
                "time": "90",
                //总分
                "totalScore": "150"
            }]
        });

        // $httpBackend.whenPOST(baseUrl + '/Interface0057.ashx').respond({
        //     examList: [{
        //         //试卷id
        //         "examId": "1111",
        //         //试卷名称
        //         "examName": "这是试卷名称",
        //         //耗时(mins)
        //         "time": "120",
        //         //总分
        //         "totalScore": "120",
        //         //得分
        //         "score": "120"
        //     }]
        // });


        // $httpBackend.whenPOST(baseUrl + '/Interface0058.ashx').respond({
        //     //试题列表
        //     "questions": [{
        //         //试题id
        //         "questionId": "123",
        //         //问题描述，选择题按照前面的接口要求添加选项
        //         "question": "这是问题描述",
        //         //答案
        //         "answer": "这是问题答案",
        //         //分析
        //         "analysis": "这是问题分析",
        //         //总分
        //         "totalScore": "12",
        //         //得分
        //         "score": "3"
        //     }]
        // });

        $httpBackend.whenPOST(baseUrl + '/Interfacexcourselist.ashx').respond({
            "course": [{
                "courseId": "51d8e569-e971-4725-830e-a30ceb77dac9",
                "courseName": "九年级数学上"
            }, {
                "courseId": "51d8e569-e971-4725-830e-a30ceb77daca",
                "courseName": "九年级物理上"
            }]
        });
        $httpBackend.whenPOST(baseUrl + '/Interfacexclasslist.ashx').respond({
            classList: [{
                id: '11111',
                name: '初三8班'
            }]
        });

        $httpBackend.whenPOST(baseUrl + '/Interfacexexamlist.ashx').respond({
            "msg": [{
                "examId": "8ec6a99f-c411-4366-ae9a-0260007524d8",
                "examName": "南京市 六合区2014年中考模拟测试（一）",
                "time": "120",
                "totalScore": "120",
                "comment": ""
            }, {
                "examId": "20733c8a-42ba-4079-8333-058bdf399578",
                "examName": "九年级数学上综合复习testfffff方婧懿",
                "time": "0",
                "totalScore": "0",
                "comment": ""
            }, {
                "examId": "9fcf3669-6cc9-4f65-83ec-070a4a2ed748",
                "examName": "九年级数学上期中考试test1234",
                "time": "45",
                "totalScore": "100",
                "comment": ""
            }, {
                "examId": "5fd48edf-e3e9-40df-89ba-0811ff276dad",
                "examName": "九年级数学上综合复习testfffff王心洋",
                "time": "0",
                "totalScore": "0",
                "comment": ""
            }, {
                "examId": "ffaf136d-6294-400f-a4a0-095f09bf687c",
                "examName": "九年级数学上综合复习testfffff朱梦轩",
                "time": "0",
                "totalScore": "0",
                "comment": ""
            }]
        });

        $httpBackend.whenPOST(baseUrl + '/Interfacexstatisticslist.ashx').respond({
            statisticsList: [{
                chapterName: '4.3 确定圆的条件',
                knowledge: '三角形的外接圆、外心和圆的内接三角形的概念',
                num: 15
            }]
        });

        var isMockInterface = function(url) {
            return _.find(mockInterfaces, function(name) {
                return url.indexOf(name) === -1;
            });
        };
        $httpBackend.whenGET(isMockInterface).passThrough();
        $httpBackend.whenPOST(isMockInterface).passThrough();
    });
