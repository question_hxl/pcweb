/**
 * Created by 贺小雷 on 2016/9/21.
 */
define([], function(){
    return ['$rootScope', '$scope', '$http', '$location', '$q', 'constantService', 'paperService', 'utilService', 'ngDialog', '$cookieStore',
        function($rootScope, $scope, $http, $location, $q, constantService, paperService, utilService, ngDialog, $cookieStore){
            $('#fastMarking').css('height', $(window).height());
            $(window).off('resize').on('resize', function(e){
                $('#fastMarking').css('height', $(window).height());
            });
            var stuFastScoreList = $cookieStore.get('stuFastScoreList');
            var windowHeight = window.innerHeight;
            var windowWidth = window.innerWidth;
            $scope.img1 = {"top" : "130px", "left" : "20px", "position" : "absolute"};
            $scope.img2 = {"top" : "340px", "left" : "385px", "position" : "absolute", "cursor" : "pointer"};
            $scope.img3 = {"top" : "80px", "right" : '200px', "position" : "absolute"};
            $scope.img4 = {"top" : "200px", "right" : "200px", "position" : "absolute", "cursor" : "pointer"};
            $scope.img5 = {"top" : "120px", "right" : '10px', "position" : "absolute"};
            $scope.img6 = {"top" : "500px", "right" : "200px", "position" : "absolute", "cursor" : "pointer"};
            //var currentUser = $cookieStore.get('currentUser').SelfName;
            //var cachedUser = window.localStorage.getItem('usedUserFast');
            //cachedUser = cachedUser ? JSON.parse(cachedUser) : [];
            /*if(!!_.find(cachedUser, function(item){
             return item === currentUser;
             })){
             $('.help').hide();
             } else {
             //展示新手帮助
             $('#step1').show();
             $('#step2').hide();
             $('#step3').hide();
             $('.next').on('click', function(){
             var obj = $(this).parents('.step');
             var step = obj.attr('step');
             obj.hide();
             $('#step' + (parseInt(step) + 1)).show();
             });
             $('.over').on('click', function(){
             $(this).parents('.step').hide();
             $('.help').hide();
             cachedUser.push(currentUser);
             window.localStorage.setItem('usedUserFast', JSON.stringify(cachedUser));
             });
             }*/
            $scope.newP = true;
            $scope.toggle = function(){
                $scope.newP = !$scope.newP;
                $('#step1').show();
                $('#step2').hide();
                $('#step3').hide();
                $('.next').on('click', function(){
                    var obj = $(this).parents('.step');
                    var step = obj.attr('step');
                    obj.hide();
                    $('#step' + (parseInt(step) + 1)).show();
                });
                $('.over').on('click', function(){
                    $(this).parents('.step').hide();
                    $scope.$apply(function(){
                        $scope.newP = true;
                    });
                });
            };

            var search = $location.search();
            var examId = search.ExamFlnkID;
            var classId = search.ClassFlnkID;
            $scope.isAllowMark = search.allowMark;
            var isRequesting = false;
            var isSubmiting = false;
            $scope.unmarkStore = [];
            $scope.logoSrc = '/assets/skin/' + window.config.theme + '/img/loading-logo.png';
            $scope.copy = window.config.themeCompanyNameMap[window.config.theme];
            $scope.no_data_img = '/assets/images/51jyfw-nopic.jpg';
            $scope.data = {
                currentBlock : 1,
                questionList : [],
                currentOrder : '1',
                currentQuestion : null,
                currentStuList : [],
                currentStu : null,
                currentStuMarked : 0,
                currentStuAve : 0,
                studentCutList : []
            };

            //根据班级和试卷Id获取切图详情
            $http.post('/BootStrap/Interface/Interface0251A.ashx', {
                examFId : examId,
                classFId : classId
            }).then(function(resp){
                setTimeout(function(){
                    isRequesting = false;
                }, 500);
                if(!_.isEmpty(resp.data.msg) && _.isArray(resp.data.msg)){
                    $scope.cutDetail = resp.data.msg[0];
                } else {
                    $scope.cutDetail = null;
                }
            });

            function setStudentAnswer(userInfo, orders){
                //如果有切片并且切片完成,并且有学生答卷JPG图片
                if($scope.cutDetail && $scope.cutDetail.isFinish === '1' && userInfo.eaAddress && userInfo.eaAddress.indexOf('.jpg') > 0){
                    var pos = JSON.parse($scope.cutDetail.position);
                    var posOrderList = [];
                    var qstOrderList = (orders + '').split('.');
                    var currentStuPos = _.find(pos, function(item){
                        posOrderList = (item.order + '').split('.');
                        return posOrderList[0] === qstOrderList[0];
                    });
                    return (currentStuPos && currentStuPos.position) ? currentStuPos.position : [];
                } else {
                    return [];
                }
            }

            /*$http.post('/BootStrap/Interface/Interface0135.ashx', {
             examid: examId
             }).then(function (resp) {
             if (!!resp && !!resp.data && !!resp.data.msg) {
             $scope.QuestionIdList = resp.data.msg;
             //根据题目获取题目详情
             var qids = _.pluck($scope.QuestionIdList, 'qid').join(',');
             $http.post('/BootStrap/Interface/Interface0208A.ashx', {
             qFlnkID: qids
             }).then(function(res){
             if(_.isArray(res.data.msg)) {
             $scope.questionList = _.map(res.data.msg, function(item, index) {
             var sub = _.map(item.sub, function(s, index){
             return {
             FLnkID: s.QFLnkID,
             title: s.title,
             OptionOne: s.OptionA,
             OptionTwo: s.OptionB,
             OptionThree: s.OptionC,
             OptionFour: s.OptionD,
             DifficultLevel: s.DifficultLevel,
             knowledges: s.knowledges,
             Answer: s.Answer,
             source: s.source,
             UseNum: s.UseNum,
             SucRate: s.SucRate,
             Analysis: s.Analysis,
             IsCollection: s.IsCollection ? item.IsCollection : 0,
             ShowType: _.find(utilService.showTypeList, function (type) {
             return type.id === s.ShowType
             }),
             orders: index + 1,
             isObjective:s.isObjective
             }
             });
             return {
             FLnkID: item.QFLnkID,
             title: item.title,
             OptionOne: item.OptionA,
             OptionTwo: item.OptionB,
             OptionThree: item.OptionC,
             OptionFour: item.OptionD,
             DifficultLevel: item.DifficultLevel,
             knowledges: item.knowledges,
             Answer: item.Answer,
             source: item.source,
             UseNum: item.UseNum,
             SucRate: item.SucRate,
             Analysis: item.Analysis,
             IsCollection: item.IsCollection ? item.IsCollection : 0,
             ShowType: _.find(utilService.showTypeList, function (type) {
             return type.id === item.ShowType
             }),
             mode: item.mode || (item.sub && item.sub.length > 0 ? 'B' : '') ,
             sub: sub,
             isObjective:item.isObjective
             };
             });
             mergeQuestion();
             $scope.data.currentQuestion = $scope.questionList[0];

             }else {
             questionCache = [];
             }
             });
             } else {
             $scope.QuestionList = [{
             'id': '-1',
             'name': '没有数据'
             }];
             }
             });*/

            $http.post('/BootStrap/Interface/Interface0181.ashx', {
                examid : examId
            }).then(function(resp){
                setTimeout(function(){
                    isRequesting = false;
                }, 500);
                if(_.isArray(resp.data.msg)){
                    $scope.questionList = getPaperQuestion(parsePaperData(resp.data.msg));
                    var temqst = null;
                    for(var i = 0; i < $scope.questionList.length; i++){
                        if($scope.questionList[i].isObjective === '0'){
                            temqst = $scope.questionList[i];
                            break;
                        }
                    }
                    $scope.data.currentQuestion = _.isEmpty(temqst) ? $scope.questionList[0] : temqst;

                }
            }, function(resp){
                setTimeout(function(){
                    isRequesting = false;
                }, 500);
                constantService.alert('获取试卷信息失败！');
            });
            function getPaperQuestion(list){
                var arr = [];
                for(var i = 0; i < list.length; i++){
                    for(var j = 0; j < list[i].question.length; j++){
                        if(list[i].question[j].mode === 'A'){
                            arr = arr.concat(list[i].question[j].sub);
                        } else if(list[i].question[j].mode === 'B'){
                            arr.push(list[i].question[j]);
                        } else {
                            arr.push(list[i].question[j]);
                        }
                    }
                }
                return arr;
            }

            function parsePaperData(data){
                return _.map(data, function(item, index){
                    var questionMeta = _.map(item.question, function(question){
                        var score = paperService.getQuestionScore(question, item);
                        var subs = _.map(question.sub, function(s){
                            var subScore = paperService.getSubQuestionScore(s, question, item);
                            return {
                                title : s.title,
                                orders : question.Mode === 'B' ? (s.orders + '.' + s.SubOrder) : s.orders,
                                ShowType : _.find(utilService.showTypeList, function(type){
                                    return type.id === s.ShowType
                                }),
                                OptionOne : s.OptionOne,
                                OptionTwo : s.OptionTwo,
                                OptionThree : s.OptionThree,
                                OptionFour : s.OptionFour,
                                knowledge : s.knowledge,
                                knowledgeNames : _.pluck(s.knowledge, 'name').join(',') || s.knowledges || '',
                                knowledges : _.pluck(s.knowledge, 'name').join(',') || s.knowledges || '',
                                score : subScore,
                                analysis : s.analysis,
                                qtypeName : s.QTypeName,
                                qtypeId : s.QTypeId,
                                typeId : s.TypeId,
                                IsCollection : s.IsCollection,
                                DifficultLevel : +s.DifficultLevel || 0,
                                Answer : s.Answer,
                                QFLnkID : s.FLnkID,
                                QID : s.QID,
                                quality : +s.quality || 0,
                                fLnkId : s.FLnkID,
                                FLnkID : s.FLnkID,
                                isSub : true,
                                mainId : question.FLnkID,
                                parent : question,
                                isObjective : s.isObjective,
                                SubOrder : s.SubOrder,
                                parentmode : question.Mode || '',
                                parentTitle : question.title

                            }
                        });
                        return {
                            mode : question.Mode || '',
                            title : question.title,
                            orders : question.orders,
                            ShowType : _.find(utilService.showTypeList, function(type){
                                return type.id === question.ShowType
                            }),
                            OptionOne : question.OptionOne,
                            OptionTwo : question.OptionTwo,
                            OptionThree : question.OptionThree,
                            OptionFour : question.OptionFour,
                            knowledge : question.knowledge,
                            knowledgeNames : _.pluck(question.knowledge, 'name').join(',') || question.knowledges || '',
                            knowledges : _.pluck(question.knowledge, 'name').join(',') || question.knowledges || '',
                            score : score,
                            analysis : question.analysis,
                            qtypeName : question.QTypeName,
                            qtypeId : question.QTypeId,
                            typeId : question.TypeId,
                            IsCollection : question.IsCollection,
                            DifficultLevel : +question.DifficultLevel || 0,
                            Answer : question.Answer,
                            QFLnkID : question.FLnkID,
                            QID : question.QID,
                            quality : +question.quality || 0,
                            fLnkId : question.FLnkID,
                            FLnkID : question.FLnkID,
                            sub : subs,
                            isMain : true,
                            isObjective : question.isObjective
                        };
                    });
                    return {
                        QTypeScores : +item.QTypeScores || 0,
                        QTypeName : item.QTypeName,
                        QNumber : item.question.length,
                        QTypeId : item.QTypeId,
                        question : questionMeta,
                        GradeId : item.GradeId,
                        GradeName : item.GradeName,
                        explain : item.Explain,
                        CNIndex : utilService.getCNNoByIndex(index + 1)
                    };
                });
            }

            function mergeQuestion(){
                for(var i = 0; i < $scope.QuestionIdList.length; i++){
                    for(var j = 0; j < $scope.questionList.length; j++){
                        if($scope.QuestionIdList[i].qid === $scope.questionList[j].FLnkID){
                            $scope.questionList[i].number = $scope.QuestionIdList[i].number;
                            $scope.questionList[i].IsChoose = $scope.QuestionIdList[i].IsChoose;
                            break;
                        }
                    }
                }
            }

            $scope.preQst = function(){
                if(isRequesting){
                    return;
                }
                var index = _.indexOf($scope.questionList, $scope.data.currentQuestion);
                if(index === -1){
                    return;
                }
                if(index === 0){
                    return;
                } else {
                    $scope.data.currentQuestion = $scope.questionList[index - 1];
                }
            };
            $scope.nextQst = function(){
                if(isRequesting){
                    return;
                }
                var index = _.indexOf($scope.questionList, $scope.data.currentQuestion);
                if(index === -1){
                    return;
                }
                if(index === $scope.questionList.length - 1){
                    return;
                } else {
                    $scope.data.currentQuestion = $scope.questionList[index + 1];
                }
            };

            $scope.$watch('data.currentQuestion', function(newValue, oldValue){
                if(newValue === oldValue){
                    return;
                }
                var ids = '';
                if($scope.data.currentQuestion.mode && ($scope.data.currentQuestion.mode === 'B')){
                    ids = _.pluck($scope.data.currentQuestion.sub, 'FLnkID').join(',');
                } else {
                    ids = $scope.data.currentQuestion.FLnkID;
                }
                /*getStuList($scope.data.currentQuestion.FLnkID).then(function (res) {
                 if(_.isArray(res.data.msg)){
                 handleStuList(res.data.msg);
                 }
                 });*/
                $http.post('/BootStrap/Interface/Interface0199.ashx', {
                    ClassFlnkID : classId,
                    ExamFlnkID : examId,
                    qFLnkID : $scope.data.currentQuestion.FLnkID
                }).then(function(res){
                    setTimeout(function(){
                        isRequesting = false;
                    }, 500);
                    if(_.isArray(res.data.msg)){
                        handleStuList(res.data.msg);
                        //qnumber 已与王哲儿子确认，儿子说 qnumber 随便传 故传 0
                        $http.post('/BootStrap/Interface/Interface0136A.ashx', {
                            qnumber : 0,
                            classId : classId,
                            examid : examId,
                            qFLnkID : ids
                        }).then(function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            if(_.isArray(res.data.msg)){
                                if($scope.data.currentQuestion.mode && ($scope.data.currentQuestion.mode === 'B')){
                                    //_.each(res.data.msg, function (item) {
                                    $scope.data.currentStuList = pushData2Stu4Sub($scope.data.currentStuList, res.data.msg);
                                    //});
                                    _.each($scope.data.currentStuList, function(i){
                                        if(i.status != 2){
                                            i.studentCutList = setStudentAnswer(i, $scope.data.currentQuestion.orders);
                                        }
                                    });
                                    _.each($scope.data.currentStuList, function(n){
                                        n.subOcrImg = [];
                                        if(n.sub && n.sub.length > 0){
                                            _.each(n.sub, function(m){
                                                if(m.eaAddress){
                                                    n.subOcrImg.push(m.eaiAddress);
                                                }
                                            });
                                        }
                                        n.subOcrImg = _.uniq(n.subOcrImg);
                                    });
                                } else {
                                    _.each(res.data.msg, function(item){
                                        pushData2Stu(item, $scope.data.currentQuestion.orders);
                                    });
                                }
                                _.each($scope.data.currentStuList, function(n){
                                    if(n.status === 2){
                                        n.sub = [];
                                        n.sub.push({eaiAddress : $scope.no_data_img});
                                    }
                                });
                                calCurrentQstReg();
                            }
                        }, function(resp){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            constantService.alert('获取学生答题详情失败！');
                        });
                    }
                }, function(resp){
                    setTimeout(function(){
                        isRequesting = false;
                    }, 500);
                    constantService.alert('获取学生列表失败！');
                });
                scrollToCurrentStu();
                getUnMarked();
                /*if($scope.data.currentQuestion.mode && ($scope.data.currentQuestion.mode === 'B')){
                 $scope.sublist = [];
                 $scope.subDetail = [];
                 var subList = [];
                 getQstData(0,_.pluck($scope.data.currentQuestion.sub, 'FLnkID').join(',')).then(function (res) {
                 if (_.isArray(res.data.msg)) {

                 $scope.subDetail.push(res.data.msg);
                 _.each(res.data.msg, function (item) {
                 $scope.data.currentStuList = pushData2Stu4Sub($scope.data.currentStuList, res.data.FLnkID, item, res.data.orders);
                 });
                 calCurrentQstReg();
                 }
                 });
                 $scope.sublist;
                 }else{
                 getQstData($scope.data.currentQuestion.orders,$scope.data.currentQuestion.FLnkID).then(function (res) {
                 if(_.isArray(res.data.msg)){
                 _.each(res.data.msg,function (item) {
                 pushData2Stu(item,$scope.data.currentQuestion.orders);
                 });
                 calCurrentQstReg();
                 }
                 });
                 }*/
            });
            /*function getStuList(id) {
             var defer = $q.defer();
             $http.post('/BootStrap/Interface/Interface0199.ashx', {
             ClassFlnkID: classId,
             ExamFlnkID: examId,
             qFLnkID: id
             }).then(function(res){
             defer.resolve(res);
             },function(res){
             defer.reject(res)
             });
             return defer.promise;
             }*/
            $scope.$watch('data.currentBlock', function(newValue, oldValue){
                if(newValue === oldValue){
                    return;
                }
                if($scope.data.currentQuestion.mode === 'B'){
                    setTimeout(function(){
                        $($('.sub-input-part input').get(0)).focus().select();
                    }, 0);
                } else {
                    setTimeout(function(){
                        $($('.score-input-part input').get(0)).focus().select();
                    }, 0);
                }
            });
            $scope.$watch('data.currentStu', function(newValue, oldValue){
                if(newValue === oldValue){
                    return;
                }
                if(!oldValue || !newValue){
                    return;
                }
                /*if(newValue.fastchoice){
                    newValue.fastchoice = null;
                }
                if(newValue.fastinput){
                    newValue.fastinput = null;
                }*/
                /*if(oldValue.fastchoice){
                    oldValue.fastchoice = null;
                }
                if(oldValue.fastinput){
                    oldValue.fastinput = null;
                }*/
                if($scope.data.currentQuestion.mode === 'B'){
                    setTimeout(function(){
                        $($('.sub-input-part input').get(0)).focus().select();
                    }, 0);
                } else {
                    setTimeout(function(){
                        $($('.score-input-part input').get(0)).focus().select();
                    }, 0);
                }
                scrollToCurrentStu();
            });
            $scope.init = function(){
                if($scope.data.currentQuestion.mode === 'B'){
                    setTimeout(function(){
                        $($('.sub-input-part input').get(0)).focus().select();
                    }, 0);
                } else {
                    setTimeout(function(){
                        $($('.score-input-part input').get(0)).focus().select();
                    }, 0);
                }
            };
            $scope.onInputFocus = function(e){
                $(e.currentTarget).select();
            };
            function handleStuList(data){
                $scope.data.currentStuList = data;
                //对学生列表进行排序
                $scope.data.currentStuList = _.sortBy($scope.data.currentStuList,'IsRight');
                _.each($scope.data.currentStuList, function(item, index){
                    item = setStuStatus(item);
                    item.index = index;
                });
                $scope.data.currentStu = $scope.data.currentStuList[0];
            }

            // 0 答案正确 1 答案错误 2 无数据 3 未批阅
            function setStuStatus(item){
                switch(+item.IsRight){
                    case 1.0:
                        item.status = 0;
                        break;
                    case 2.0:
                        item.status = 2;
                        break;
                    case -1:
                        item.status = 3;
                        break;
                    default:
                        item.status = 1;
                }
                return item;
            }

            $scope.showQstAndAnswer = function(type){
                if(type === 1){
                    $scope.data.currentBlock = 1;
                } else {
                    ngDialog.open({
                        template : 'questionViewer',
                        className : 'ngdialog-theme-default',
                        appendClassName : 'qst-answer-box',
                        closeByDocument : true,
                        plain : false,
                        scope : $scope
                    });
                }
            };

            $scope.markStudent = function(stu){
                if(stu.status === 2){
                    return;
                }
                $scope.data.currentStu = stu;
                $scope.data.currentBlock = 2;
            };

            /*function getQstData(orders,id) {
             var defer = $q.defer();
             $http.post('/BootStrap/Interface/Interface0136A.ashx', {
             qnumber: orders || $scope.data.currentQuestion.orders,
             classId: classId,
             examid: examId,
             qFLnkID: id
             }).then(function (res) {
             res.data.FLnkID = id;
             defer.resolve(res);
             },function(res){
             defer.reject(res)
             });
             return defer.promise;
             }*/

            function pushData2Stu(item, orders){
                _.each($scope.data.currentStuList, function(i){
                    if(item.userFLnkID === i.FLnkID){
                        i.IsChoose = item.IsChoose;
                        i.answer = item.answer;
                        i.eaAddress = item.eaAddress || $scope.no_data_img;
                        i.eaAddressA = item.eaAddressA;
                        i.eaId = item.eaId;
                        i.eaiAddress = item.eaiAddress || $scope.no_data_img;
                        i.eaiId = item.eaiId;
                        i.fullscore = +item.fullscore;
                        i.myscore = +item.myscore;
                        i.fastinput = i.myscore;
                        i.fastScoreList = _.range(0, +item.fullscore + (((+item.fullscore * 10) % 10 === 5) ? 0.5 : 1), ((+item.fullscore * 10) % 10 === 5) ? 0.5 : 1);
                        i.studentCutList = setStudentAnswer(item, orders);
                        i.qFLnkID = item.qFLnkID;
                    }
                    // 查询是否已经自定义过
                    if(_.isArray(stuFastScoreList)) {
                        _.each(stuFastScoreList, function (stu) {
                            if(item.qFLnkID === stu.FLnkID) {
                                i.fastScoreList = stu.fastScoreList
                            }
                        })
                    }
                });
            }

            function pushData2Stu4Sub(stulist, detaillist){
                var filtedList = [];
                _.each(stulist, function(i){
                    filtedList = _.filter(detaillist, function(m){
                        return m.userFLnkID === i.FLnkID;
                    });
                    _.each($scope.data.currentQuestion.sub, function(n){
                        _.each(filtedList, function(o){
                            if(n.FLnkID === o.qFLnkID){
                                o.eaAddress = o.eaAddress || o.no_data_img;
                                o.eaiAddress = (i.status === 2 ? $scope.no_data_img : (o.eaiAddress || $scope.no_data_img));
                                o.fullscore = +o.fullscore;
                                o.myscore = +o.myscore;
                                o.fastinput = o.myscore;
                                if(!(i.sub && _.isArray(i.sub))){
                                    i.sub = [];
                                }
                                i.sub.push(o);

                            }
                        });
                    });
                    _.each(i.sub, function(p){
                        if(!i.fullscore){
                            i.fullscore = 0;
                        }
                        i.fullscore = i.fullscore + p.fullscore;
                        if(!i.myscore){
                            i.myscore = 0;
                        }
                        i.myscore = i.myscore + p.myscore;
                        if(!i.eaAddress && p.eaAddress){
                            i.eaAddress = p.eaAddress || $scope.no_data_img;
                        }
                    });
                    /*_.each(filtedList,function (q) {
                     if(q.qFLnkID === $scope.data.currentQuestion.FLnkID){
                     i.IsChoose = q.IsChoose;
                     i.answer = q.answer;
                     i.eaAddress = q.eaAddress || $scope.no_data_img;
                     i.eaAddressA = q.eaAddressA;
                     i.eaId = q.eaId;
                     i.eaiAddress = (i.status === 2 ? $scope.no_data_img : (q.eaiAddress || $scope.no_data_img));
                     i.eaiId = q.eaiId;
                     i.qFLnkID = q.qFLnkID;
                     }
                     });*/
                });


                /*_.each(stulist,function (i) {
                 if(item.userFLnkID === i.FLnkID){
                 if(item.qFLnkID === $scope.data.currentQuestion.FLnkID){
                 i.IsChoose = item.IsChoose;
                 i.answer = item.answer;
                 i.eaAddress = item.eaAddress || $scope.no_data_img;
                 i.eaAddressA = item.eaAddressA;
                 i.eaId = item.eaId;
                 i.eaiAddress = item.eaiAddress || $scope.no_data_img;
                 i.eaiId = item.eaiId;
                 i.qFLnkID = item.qFLnkID;
                 }else{
                 _.each($scope.data.currentQuestion.sub,function (n) {
                 if(n.FLnkID = item.qFLnkID){
                 item.eaAddress = item.eaAddress || $scope.no_data_img;
                 item.eaiAddress = (i.status === 2 ? $scope.no_data_img : (item.eaiAddress || $scope.no_data_img));
                 item.fullscore = +item.fullscore;
                 item.myscore = +item.myscore;
                 if(!(i.sub && _.isArray(i.sub))){
                 i.sub = [];
                 }
                 i.sub.push(item);
                 }
                 });
                 }
                 _.each(i.sub,function (m) {
                 if(!i.fullscore){
                 i.fullscore = 0;
                 }
                 i.fullscore = i.fullscore + m.fullscore;
                 if(!i.myscore){
                 i.myscore = 0;
                 }
                 i.myscore = i.myscore + m.myscore;
                 });
                 }
                 });*/
                return stulist;
            }

            $scope.imgWidth = 660;
            $scope.scale = 1;

            $scope.showOriginal = function(){
                //$('.img-area img').get(0).style.cssText = '';
                $scope.imgWidth = 500;
                $scope.scale = 1;
            };

            $scope.minus = function(){
                /*var width = $('.img-area img').width();
                 $('.img-area img').css({
                 width: (width - 40 > 0) ? (width - 40) + 'px' : 0
                 });*/
                $scope.imgWidth = (($scope.imgWidth - 80 > 0) ? ($scope.imgWidth - 80) : 0);
                if($scope.scale > 0.1){
                    $scope.scale = $scope.scale - 0.1;
                }
            };

            $scope.plus = function(){
                /*var width = $('.img-area img').width();
                 $('.img-area img').css({
                 width: (width + 40) + 'px'
                 });*/
                $scope.imgWidth = $scope.imgWidth + 80;
                if($scope.scale < 2){
                    $scope.scale = $scope.scale + 0.1;
                }
            };
            $scope.startMarking = function(){
                if($scope.data.currentStuList && $scope.data.currentStuList.length > 0){
                    $scope.data.currentBlock = 2;

                }
            };
            $(window).on('keyup', function(e){
                e.preventDefault();
                e.stopPropagation();
                if(isRequesting){
                    return;
                }
                $scope.$apply(handleSmallkeybord(e));

            });
            function handleSmallkeybord(e){
                switch(e.keyCode){
                    case 187:
                    case 107:
                        //+号
                        if(closeDialog()){
                            ngDialog.closeAll();
                        } else {
                            setFullScore();
                        }
                        break;
                    case 189:
                    case 109:
                        //-号
                        if(closeDialog()){
                            ngDialog.closeAll();
                        } else {
                            setEggScore();
                        }
                        break;
                    case 39:
                        //右箭头
                        if(closeDialog()){
                            ngDialog.closeAll();
                        } else {
                            next();
                        }
                        break;
                    case 37:
                        //左箭头
                        if(closeDialog()){
                            ngDialog.closeAll();
                        } else {
                            pre();
                        }
                        break;
                    case 13:
                        if(closeDialog()){
                            ngDialog.closeAll();
                        }
                        break;
                    default:
                        break;
                }
            }

            function next(){
                if($scope.data.currentBlock === 1){
                    $scope.data.currentBlock = 2;
                    return;
                }
                var index = $scope.data.currentStu.index;
                for(; index < $scope.data.currentStuList.length;){
                    if(index === $scope.data.currentStuList.length - 1){
                        $scope.nextQst();
                        return;
                    } else {
                        index = index + 1;
                    }
                    if($scope.data.currentStuList[index].status === 2){
                        continue;
                    } else {
                        break;
                    }
                }
                $scope.data.currentStu = $scope.data.currentStuList[index];
            }

            function pre(){
                if($scope.data.currentBlock === 1){
                    $scope.data.currentBlock = 2;
                    return;
                }
                if($scope.data.currentStu.index === 0){
                    $scope.preQst();
                } else {
                    $scope.data.currentStu = $scope.data.currentStuList[$scope.data.currentStu.index - 1];
                }
            }

            function setFullScore(){
                /*if(isSubmiting){
                    return;
                }*/
                if($scope.isAllowMark != 1){
                    return;
                }
                if(+$scope.data.currentStu.IsRight === 2){
                    //next();
                    nextSkipChoice();
                    return;
                }
                if($scope.data.currentStu.sub && $scope.data.currentStu.sub.length > 0){
                    var requestarr = [];
                    for(var i = 0; i < $scope.data.currentStu.sub.length; i++){
                        requestarr.push(submitStuScore($scope.data.currentStu.sub[i].fullscore, $scope.data.currentStu.sub[i].eaId, $scope.data.currentStu.sub[i].eaiId));
                    }
                    if(requestarr.length > 0){
                        $q.all(requestarr).then(function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            if(_.isArray(res)){
                                for(var i = 0; i < res.length; i++){
                                    if(res[i].data.code != 0){
                                        constantService.alert(res[i].data.msg);
                                        return;
                                    }
                                }
                                _.each($scope.data.currentStu.sub, function(m){
                                    m.myscore = m.fullscore;
                                    m.fastinput = m.fullscore;
                                });
                                $scope.data.currentStu.myscore = $scope.data.currentStu.fullscore;
                                $scope.data.currentStu.status = 0;
                                calCurrentQstReg();
                                //next();
                                nextSkipChoice();
                                return;
                            }
                        }, function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            constantService.alert('提交失败!');
                            return;
                        });
                    }
                } else {
                    submitStuScore($scope.data.currentStu.fullscore, $scope.data.currentStu.eaId, $scope.data.currentStu.eaiId).then(function(res){
                        setTimeout(function(){
                            isRequesting = false;
                        }, 500);
                        if(res.data.code != 0){
                            constantService.alert(res.data.msg);
                            return
                        } else {
                            $scope.data.currentStu.myscore = $scope.data.currentStu.fullscore;
                            $scope.data.currentStu.fastinput = $scope.data.currentStu.fullscore;
                            $scope.data.currentStu.status = 0;
                            calCurrentQstReg();
                            //next();
                            nextSkipChoice();
                            return;
                        }
                    }, function(res){
                        setTimeout(function(){
                            isRequesting = false;
                        }, 500);
                        constantService.alert('提交失败!');
                        return;
                    });
                }
            }

            function setEggScore(){
                /*if(isSubmiting){
                    return;
                }*/
                if($scope.isAllowMark != 1){
                    return;
                }
                if(+$scope.data.currentStu.IsRight === 2){
                    //next();
                    nextSkipChoice();
                    return;
                }

                if($scope.data.currentStu.sub && $scope.data.currentStu.sub.length > 0){
                    var requestarr = [];
                    for(var i = 0; i < $scope.data.currentStu.sub.length; i++){
                        requestarr.push(submitStuScore(0, $scope.data.currentStu.sub[i].eaId, $scope.data.currentStu.sub[i].eaiId));
                    }
                    if(requestarr.length > 0){
                        $q.all(requestarr).then(function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            if(_.isArray(res)){
                                for(var i = 0; i < res.length; i++){
                                    if(res[i].data.code != 0){
                                        constantService.alert(res[i].data.msg);
                                        return;
                                    }
                                }
                                _.each($scope.data.currentStu.sub, function(m){
                                    m.myscore = 0;
                                    m.fastinput = 0;
                                });
                                $scope.data.currentStu.myscore = 0;
                                $scope.data.currentStu.status = 1;
                                calCurrentQstReg();
                                //next();
                                nextSkipChoice();
                                return;
                            }
                        }, function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            constantService.alert('提交失败!');
                            return;
                        });
                    }
                } else {
                    submitStuScore(0, $scope.data.currentStu.eaId, $scope.data.currentStu.eaiId).then(function(res){
                        setTimeout(function(){
                            isRequesting = false;
                        }, 500);
                        if(res.data.code != 0){
                            constantService.alert(res.data.msg);
                            return
                        } else {
                            $scope.data.currentStu.myscore = 0;
                            $scope.data.currentStu.fastinput= 0;
                            $scope.data.currentStu.status = 1;
                            calCurrentQstReg();
                            //next();
                            nextSkipChoice();
                            return;
                        }
                    }, function(res){
                        setTimeout(function(){
                            isRequesting = false;
                        }, 500);
                        constantService.alert('提交失败!');
                        return;
                    });
                }
            }

            $scope.listenEnter = function(e, score, max){
                e.preventDefault();
                e.stopPropagation();
                if(isSubmiting){
                    return
                }
                if(isRequesting){
                    return;
                }
                if(closeDialog()){
                    ngDialog.closeAll();
                    return;
                }
                if($scope.isAllowMark != 1){
                    return;
                }
                if(+$scope.data.currentStu.IsRight === 2){
                    return;
                }
                handleSmallkeybord(e);
                if(e.keyCode === 13){
                    /*if(!(max?((score<=max)&&(score>=0)):(score>=0))){
                     constantService.alert('请输入合适的分数!');
                     return;
                     }*/

                    if(checkScoreIllegal(score, max)){
                        submitStuScore(score, $scope.data.currentStu.eaId, $scope.data.currentStu.eaiId).then(function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            if(res.data.code != 0){
                                constantService.alert(res.data.msg);
                                return;
                            } else {
                                $scope.data.currentStu.myscore = score;
                                if(score === max){
                                    $scope.data.currentStu.status = 0;
                                }
                                if((score >= 0) && (score < max)){
                                    $scope.data.currentStu.status = 1;
                                }
                                calCurrentQstReg();
                                //回车批阅
                                //next();
                                nextSkipChoice();
                                return;
                            }
                        }, function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            constantService.alert('提交失败!');
                            return;
                        });
                    }
                }
            };
            $scope.listenSubEnter = function(e, subQst, score, index, isLast){
                if(isRequesting){
                    return;
                }
                if(isSubmiting){
                    return;
                }
                if(closeDialog()){
                    ngDialog.closeAll();
                    return;
                }
                if(+$scope.data.currentStu.IsRight === 2){
                    return;
                }
                if(e.keyCode === 13){
                    /*if(!(subQst.fullscore?((score<=subQst.fullscore)&&(score>=0)):(score>=0))){
                     constantService.alert('请输入合适的分数!');
                     return;
                     }*/

                    if(checkScoreIllegal(score, subQst.fullscore)){
                        submitStuScore(score, subQst.eaId, subQst.eaiId).then(function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            if(res.data.code != 0){
                                constantService.alert(res.data.msg);
                                return;
                            } else {
                                subQst.myscore = score;
                                if(isLast){
                                    var gotscore = 0;
                                    _.each($scope.data.currentStu.sub, function(i){
                                        gotscore = gotscore + i.myscore;
                                        /*if((i.fastinput || i.fastinput === 0) && (!(i.fullscore?((i.fastinput<=i.fullscore)&&(i.fastinput>=0)):(i.fastinput>=0)) || !(i.fastinput * 10 % 5 !== 0))){
                                         isIll = true;
                                         return false;
                                         }*/
                                    });
                                    $scope.data.currentStu.myscore = gotscore;
                                    if($scope.data.currentStu.myscore === $scope.data.currentStu.fullscore){
                                        $scope.data.currentStu.status = 0;
                                    }
                                    if(($scope.data.currentStu.myscore >= 0) && ($scope.data.currentStu.myscore < $scope.data.currentStu.fullscore)){
                                        $scope.data.currentStu.status = 1;
                                    }
                                    //next();
                                    nextSkipChoice();
                                } else {
                                    //如果不是最后一个子题，则切换到下一子题
                                    $('.sub-input-part input:eq(' + (index + 1) + ')').focus();
                                }
                                calCurrentQstReg();
                                return;
                            }
                        }, function(res){
                            setTimeout(function(){
                                isRequesting = false;
                            }, 500);
                            constantService.alert('提交失败!');
                            return;
                        });
                    }
                }
            };

            $scope.setScore = function(item){
                item === 0 ? setEggScore() : setFullScore();
            };

            $scope.fastMarking = function(score){
                if(isSubmiting){
                    return;
                }
                if($scope.isAllowMark != 1){
                    return;
                }
                submitStuScore(score, $scope.data.currentStu.eaId, $scope.data.currentStu.eaiId).then(function(res){
                    setTimeout(function(){
                        isRequesting = false;
                    }, 500);
                    if(res.data.code != 0){
                        constantService.alert(res.data.msg);
                        return
                    } else {
                        $scope.data.currentStu.myscore = score;
                        if(score === $scope.data.currentStu.fullscore){
                            $scope.data.currentStu.status = 0;
                        }
                        if((score >= 0) && (score < $scope.data.currentStu.fullscore)){
                            $scope.data.currentStu.status = 1;
                        }
                        calCurrentQstReg();
                        //next();
                        nextSkipChoice();
                        return;
                    }
                }, function(res){
                    setTimeout(function(){
                        isRequesting = false;
                    }, 500);
                    constantService.alert('提交失败!');
                    return;
                });
            };

            function submitStuScore(score, exaid, exaiid){
                isSubmiting = true;
                var defer = $q.defer();
                $http.post('/BootStrap/Interface/Interface0137.ashx', {
                    score : score,
                    exaId : exaid,
                    exaItemId : exaiid
                }).then(function(res){
                    setTimeout(function(){
                        isRequesting = false;
                        isSubmiting = false;
                    }, 500);
                    defer.resolve(res);
                }, function(res){
                    setTimeout(function(){
                        isRequesting = false;
                        isSubmiting = false;
                    }, 500);
                    defer.reject(res)
                });
                return defer.promise;
            }

            function calCurrentQstReg(){
                var markedStu = 0;
                var totalScore = 0;
                _.each($scope.data.currentStuList, function(item){
                    if(item.status === 0 || item.status === 1){
                        markedStu = markedStu + 1;
                        totalScore = totalScore + (item.myscore ? item.myscore : 0);
                    }
                });
                $scope.data.currentStuMarked = markedStu;
                $scope.data.currentStuAve = (markedStu === 0 ? 0 : (totalScore / markedStu).toFixed(1));
            }

            $scope.preventDefault = function(e){
                switch(e.keyCode){
                    case 187:
                    case 107:
                    case 189:
                    case 109:
                        e.preventDefault();
                        e.stopPropagation();
                        break;
                    default:
                        break;
                }
            };
            $rootScope.$on('cfpLoadingBar:started', function(){
                isRequesting = true;
            });
            /*$rootScope.$on('cfpLoadingBar:completed',function () {
             setTimeout(function () {
             isRequesting = false;
             },500);
             });*/
            function isFastInput(){
                if(($scope.data.currentQuestion.mode === 'B') && ($scope.data.currentStu.sub && $scope.data.currentStu.sub.length > 0)){
                    var isIll = false;
                    _.each($scope.data.currrentStu.sub, function(i){
                        if(!_.isEmpty(i.fastinput) && (!(i.fullscore ? ((i.fastinput <= i.fullscore) && (i.fastinput >= 0)) : (i.fastinput >= 0)) || !(i.fastinput * 10 % 5 !== 0))){
                            isIll = true;
                            return false;
                        }
                    });
                } else {
                    if(!_.isEmpty($scope.data.currentStu.fastinput) && (!($scope.data.currentStu.fullscore ? (($scope.data.currentStu.fastinput <=
                        $scope.data.currentStu.fullscore) && ($scope.data.currentStu.fastinput >= 0)) : ($scope.data.currentStu.fastinput >= 0)) || !($scope.data.currentStu.fastinput * 10 % 5 !== 0))){
                        isIll = true;
                    }
                }
                return isIll;
            }

            function closeDialog(){
                var loglist = ngDialog.getOpenDialogs();
                if(_.isArray(loglist) && loglist.length > 0){
                    return true
                } else {
                    return false;
                }
            }

            function scrollToCurrentStu(){
                var $keyBoards = $('.keyboards');
                var boxOffset = $keyBoards.offset().top,
                    boxHeight = $keyBoards.height(),
                    listHeight = $keyBoards.find('ul').height();
                setTimeout(function(){
                    if($('#' + $scope.data.currentStu.index).offset()){
                        var keyOffset = $('#' + $scope.data.currentStu.index).offset().top + $keyBoards.scrollTop();
                        $keyBoards.scrollTop(keyOffset - boxHeight / 2 - boxOffset);
                    }
                }, 100);
            }

            /**
             * 默认跳过选择题
             * TODO
             */
            function defaultSkipChoice(){
                var index = _.indexOf($scope.questionList, $scope.data.currentQuestion);
                if(index === -1){
                    return;
                }
                if(index === $scope.questionList.length - 1){
                    return;
                }
                for(var i = index + 1; i < $scope.questionList.length; i++){
                    if($scope.questionList[i].isObjective === '0'){
                        $scope.data.currentQuestion = $scope.questionList[i];
                        return;
                    }
                }
                return $scope.data.currentQuestion = $scope.questionList[index + 1];
            }

            function nextSkipChoice(){
                if($scope.data.currentBlock === 1){
                    $scope.data.currentBlock = 2;
                    return;
                }
                var index = $scope.data.currentStu.index;
                for(; index < $scope.data.currentStuList.length;){
                    if(index === $scope.data.currentStuList.length - 1){
                        getUnMarked();
                        var qstindex = _.indexOf($scope.questionList, $scope.data.currentQuestion);
                        if(qstindex === $scope.questionList.length - 1){
                            //TODO
                            check4end();
                        } else {
                            constantService.showConfirm('本题已批阅完成，是否继续批阅下一题的学生？', ['确定'], function(){
                                defaultSkipChoice();
                            });
                        }
                        return;
                    } else {
                        index = index + 1;
                    }
                    if($scope.data.currentStuList[index].status === 2){
                        continue;
                    } else {
                        break;
                    }
                }
                $scope.data.currentStu = $scope.data.currentStuList[index];
            }

            function checkScoreIllegal(score, max){
                if(+score < 0 || +score > max || +score * 10 % 5 !== 0 || score === null){
                    constantService.alert('分数必须为0.5的整数倍，且不能大于本题总分值，请重新批阅！');
                    return false;
                } else {
                    return true;
                }
            }

            function check4end(){
                //批阅到最后一题最后一个学生
                $http.post('/BootStrap/Interface/generalQuery.ashx',{
                    exmaFlnkID :examId,
                    classFlnkID :classId,
                    Proc_name:'GetClassExamUnFinish'
                }).then(function (resp) {
                    if((resp.data.code == 0) && _.isArray(resp.data.msg)){
                        if(resp.data.msg.length == 0){
                            constantService.showConfirm('已全部批阅完毕,是否结束批阅并提交分析？', ['确定'], function(){
                                handle2Analysis();
                            });
                        }else if(resp.data.msg.length > 0){
                            constantService.showConfirm('已批阅完最后一题，还有其他未批阅学生，是否结束批阅并提交分析？', ['确定'], function(){
                                handle2Analysis();
                            }, function(){
                                return;
                            });
                        }
                    }else{
                        constantService.alert(resp.msg);
                    }
                });
                /*if($scope.unmarkStore.length > 0){
                    var haveUnmark = false;
                    for(var i = 0; i < $scope.unmarkStore.length; i++){
                        if($scope.unmarkStore[i].num != 0){
                            haveUnmark = true;
                            break;
                        }
                    }
                    if(haveUnmark){
                        constantService.showConfirm('已批阅完最后一题，还有其他未批阅学生，是否结束批阅并提交分析？', ['确定'], function(){
                            handle2Analysis();
                        }, function(){
                            return;
                        });
                    } else {
                        constantService.showConfirm('已全部批阅完毕,是否结束批阅并提交分析？', ['确定'], function(){
                            handle2Analysis();
                        });
                    }
                } else {
                    constantService.showConfirm('已全部批阅完毕,是否结束批阅并提交分析？', ['确定'], function(){
                        handle2Analysis();
                    });
                }*/
            }

            function getUnMarked(){
                var stuNums = 0;
                var qstindex = 0;
                var exitqst = false;
                _.each($scope.data.currentStuList, function(item){
                    if(item.status === 3){
                        stuNums = stuNums + 1;
                    }
                });
                for(var i = 0; i < $scope.unmarkStore.length; i++){
                    if($scope.unmarkStore.orders === +$scope.data.currentQuestion.orders){
                        qstindex = i;
                        exitqst = true;
                        break;
                    }
                }
                if(exitqst){
                    $scope.unmarkStore[qstindex].num = stuNums;
                } else {
                    $scope.unmarkStore.push({orders : $scope.data.currentQuestion.orders, num : stuNums});
                }
            }

            function handle2Analysis(){
                constantService.showConfirm('提交分析后，分析数据将会发送到家长微信通知，点击确认继续。', ['确认'], function(){
                    $http.post($rootScope.baseUrl + '/Interface0207.ashx', {
                        classFlnkID : classId,
                        examFlnkID : examId
                    }).then(function(res){
                        if(res.data.code === 0){
                            constantService.alert('提交分析成功！');
                            location.href = "#/marking?";
                        } else {
                            constantService.showConfirm(res.data.msg + '；跳转至试卷列表页面重新提交.', ['确认'], function(){
                                location.href = "#/marking?";
                            });
                        }
                    }, function(res){
                        constantService.showConfirm('提交失败,跳转至试卷列表页面重新提交.', ['确认'], function(){
                            location.href = "#/marking?";
                        });
                    });
                });
            }
            $scope.submitSub = function(e, subQst, max, index, isLast){
                if(isLast){
                    return;
                }
                if(+subQst.fastinput >= 0 && subQst.fastinput !== '') {
                    if(checkScoreIllegal(+subQst.fastinput, subQst.fullscore)){
                        if(+subQst.fastinput === +subQst.myscore){
                            return;
                        }
                        submitStuScore(+subQst.fastinput, subQst.eaId, subQst.eaiId).then(function(res){
                            if(res.data.code != 0){
                                constantService.alert(res.data.msg);
                                return;
                            } else {
                                subQst.myscore = +subQst.fastinput;
                                calCurrentQstReg();
                                return;
                            }
                        });
                    }
                }
            };
            $scope.setFastBoard = function () {
                ngDialog.open({
                    template:'<div class="editFastInput-title">设置分数</div><div class="editFastInput-container">' +
                    '<div class="tip-input-infront">输入分数将用于快捷打分，输入的数字请用空格隔开，且每个分数仅能为<span style="color: #ac2925">0.5</span>的整数倍</div>' +
                    '<div class="tip-score-infront">本题分数<span style="color: #ac2925" ng-bind="qstScore"></span>分，请在合理范围内设置分值.</div>' +
                    '<div class="input-area"><input class="score-input" type="text" ng-model="scores"></div>' +
                    '<div class="btn-group"><div class="btn abolish-btn" ng-click="closeInput()">取消</div><div class="btn confirm-btn" ng-click="confirmInput()">确定</div></div>' +
                    '</div>',
                    className: 'ngdialog-theme-default',
                    appendClassName:'encryptmarkConfig-editFastInput',
                    plain: true,
                    closeByEscape: false,
                    showClose: false,
                    scope:$scope,
                    closeByDocument:false,
                    controller:function ($scope) {
                        return function () {
                            var qstitem = $scope.data.currentQuestion, isIn = false;
                            $scope.scores = qstitem.fastInputStr;
                            $scope.qstScore = qstitem.score;
                            $scope.closeInput = function () {
                                ngDialog.closeAll();
                            };
                            function dbc2sbc(str) {
                                return str.replace(/[\uff01-\uff5e]/g,function(a){return String.fromCharCode(a.charCodeAt(0)-65248);}).replace(/\u3000/g," ");
                            }
                            function trimLAndR(s){
                                return s.replace(/(^\s*)|(\s*$)/g, "");
                            }
                            $scope.confirmInput = function () {
                                if(!!$scope.scores || ($scope.scores == 0)){
                                    $scope.scores = dbc2sbc($scope.scores);
                                    $scope.scores = trimLAndR($scope.scores);
                                    var scorelist = $scope.scores.split(' ');
                                    var onlyNum = /^(\d+(\.\d{1,2})?)?$/;
                                    for(var i = 0; i < scorelist.length; i++){
                                        if(!onlyNum.test(scorelist[i])){
                                            constantService.showSimpleToast('仅能输入数字，小数点后仅能保留一位!');
                                            return;
                                        }
                                        if (+scorelist[i] < 0 || +scorelist[i] > +qstitem.score || +scorelist[i] * 10 % 5 !== 0) {
                                            constantService.showSimpleToast('分数必须为0.5的整数倍，且不能大于本题总分值！');
                                            return ;
                                        }
                                    }
                                }
                                if (!scorelist) {
                                    return
                                }
                                qstitem.fastInputStr = scorelist.join(' ');
                                qstitem.scoreList = _.uniq(scorelist);//数据去重
                                $scope.data.currentStu.fastScoreList = [];
                                _.each(qstitem.scoreList, function (sco, index) {
                                    $scope.data.currentStu.fastScoreList[index]  = +sco;
                                });
                                _.each($scope.data.currentStuList, function (stu) {
                                    stu.fastScoreList = $scope.data.currentStu.fastScoreList
                                });
                                if(_.isArray(stuFastScoreList)) {
                                    _.each(stuFastScoreList, function (stu) {
                                        if(stu.FLnkID === $scope.data.currentStu.qFLnkID) {
                                            isIn = true;
                                            stu.fastScoreList = $scope.data.currentStu.fastScoreList
                                        }
                                    });
                                } else {
                                    stuFastScoreList = [];
                                }
                                if(!isIn){
                                    stuFastScoreList.push({
                                        FLnkID: $scope.data.currentStu.qFLnkID,
                                        fastScoreList: $scope.data.currentStu.fastScoreList
                                    })
                                }
                                $cookieStore.put('stuFastScoreList', stuFastScoreList);
                                ngDialog.closeAll();
                            }
                        }
                    }
                })
            }
        }]
});