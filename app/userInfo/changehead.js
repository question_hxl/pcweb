﻿define(['jquery.datetimepicker', 'sitelogo', 'cropper'], function () {
	return ['$rootScope', '$http', '$scope', '$timeout', 'ngDialog', 'fileReader',function ($rootScope, $http, $scope, $timeout, ngDialog, fileReader) {

		//copy当前用户，用于数据绑定
		$scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));

		//确认修改密码
		$scope.isCur = true;
		$scope.successShow = false;
		$scope.errorShow = false;
		$scope.serrorShow = false;
		$scope.perrorShow = false;
		$scope.lerrorShow = false;
		$scope.n1errorShow = false;
		$scope.n2errorShow = false;

		$scope.getFile = function () {
			fileReader.readAsDataUrl($scope.file, $scope)
                          .then(function (result) {
                          	$scope.currentUser.Logo = result;
                          	var postData = {
                          		fileName: $scope.file,
                          	};
                          	console.log(postData);

                          	$scope.saveLogo = function () {
                          		var promise = postMultipart($rootScope.baseUrl + '/Interface0001C.ashx', postData);
                          		function postMultipart(url, data) {
                          			var fd = new FormData();
                          			angular.forEach(data, function (val, key) {
                          				fd.append(key, val);
                          			});

                          			var args = {
                          				method: 'POST',
                          				url: url,
                          				data: fd,
                          				headers: { 'Content-Type': undefined },
                          				transformRequest: angular.identity
                          			};
                          			return $http(args).success(function (resp) {
                          				if (resp.code == 0) {

                          					$scope.currentUser.Logo = resp.msg;
                          					sessionStorage.setItem('currentUser', angular.toJson($scope.currentUser));
                          					console.log(resp.msg);

                          					ngDialog.open({
                          						template:
													'<p>恭喜您，图片修改成功！</p>' +
													'<div class="ngdialog-buttons">' +
													'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                          						plain: true,
                          						className: 'ngdialog-theme-plain',
                          						preCloseCallback: function () {
                          							$timeout(function () {
                          								location.href = "#/userInfo"
                          							}, 1000);
                          						}
                          					});

                          					$timeout(function () {
                          						ngDialog.close();
                          					}, 2000);

                          				} else {
                          					alert('上传失败!');
                          				}
                          			});
                          		}

                          	}
                          });
		};
	}]

})

var app = angular.module('app', []);
app.directive('fileModel', ['$parse', 'ngDialog', '$timeout', function ($parse, ngDialog, $timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs, ngModel) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;
			console.log(modelSetter);
			element.bind('change', function (event) {
				scope.$apply(function () {
					modelSetter(scope, element[0].files[0]);
				});
				//附件预览 
				scope.file = (event.srcElement || event.target).files[0];

				if (scope.file === undefined) {
					return false;
				}
				if (scope.file.type.indexOf('image') === -1) {
					ngDialog.open({
						template:
                            '<p>请选择图片文件,如jpg、jpeg、png、bmp格式的图片！</p>' +
                            '<div class="ngdialog-buttons">' +
                            '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					$timeout(function () {
						ngDialog.close();
					}, 3000);
					return false;
				}
				//计算文件大小
				var size = Math.floor(scope.file.size / 1024);
				if (size > 1000) {
					ngDialog.open({
						template:
                            '<p>亲,上传图片不要超过1M哦!</p>' +
                            '<div class="ngdialog-buttons">' +
                            '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					$timeout(function () {
						ngDialog.close();
					}, 3000);
					return false;
				};

				scope.getFile();
			});
		}
	};
}]);


app.factory('fileReader', ["$q", "$log", function ($q, $log) {
	var onLoad = function (reader, deferred, scope) {
		return function () {
			scope.$apply(function () {
				deferred.resolve(reader.result);
			});
		};
	};
	var onError = function (reader, deferred, scope) {
		return function () {
			scope.$apply(function () {
				deferred.reject(reader.result);
			});
		};
	};
	var getReader = function (deferred, scope) {
		var reader = new FileReader();
		reader.onload = onLoad(reader, deferred, scope);
		reader.onerror = onError(reader, deferred, scope);
		return reader;
	};

	var readAsDataURL = function (file, scope) {

		//检测文件类型
		if (file.type.indexOf('image') === -1) {
			alert("亲,请选择图片文件哦！");
			return false;
		}
		//计算文件大小
		var size = Math.floor(file.size / 1024);
		if (size > 1000) {
			alert("亲,上传图片不要超过1M哦!");
			return false;
		};

		var deferred = $q.defer();
		var reader = getReader(deferred, scope);
		reader.readAsDataURL(file);
		return deferred.promise;
	};
	return {
		readAsDataUrl: readAsDataURL
	};
}])
