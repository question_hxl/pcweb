/**
 * Created by tangquanbin on 2016/10/25 0025.
 */
define(['underscore-min', 'jquery.dataTables', 'outexcel', 'jqprint', 'jqrcode'], function () {
    return ['$rootScope', '$http', '$scope', '$timeout', 'ngDialog', '$sce', '$location',
        function ($rootScope, $http, $scope, $timeout, ngDialog, $sce, $location) {

          //get student information details
            $scope.isCur = true;
            $scope.allOrSelf = $location.search().allOrSelf;
            $scope.ClassFlnkID = $location.search().ClassFlnkID;
            $scope.currStudentNo = $location.search().studentNo;
            $http.post($rootScope.baseUrl + '/Interface0298.ashx', {
                classFId: $scope.ClassFlnkID
            }).success(function (data) {
                if (data && Array.isArray(data.msg) && data.msg.length) {
                    $scope.className = data.msg[0].className;
                    $scope.stuCodes = data.msg;
                    var $parent = $('#examPrint');
                    if($scope.allOrSelf==='2'){
                        $scope.currStudentNoList =_.find($scope.stuCodes, function(item){
                            return item.empId === $scope.currStudentNo;
                        });
                        $scope.userName = $scope.currStudentNoList.userName;
                        for(var i=0 ;i<84;i++){
                            var $sc = $('<div class="print-code out-exam-code"> ' +
                                '<div class="examCode"></div>' +
                                '<div class="detail"><div class="name"></div><div class="classroom"></div><div class="schoolmn"></div></div>' +
                                '</div>');
                            var $codeArea = $sc.find('.examCode');
                            var $name = $sc.find('.name');
                            var $schoolmn = $sc.find('.schoolmn');
                            var $classroom = $sc.find('.classroom');
                            $name.text($scope.currStudentNoList.userName);
                            $classroom.text($scope.currStudentNoList.className);
                            $schoolmn.text($scope.currStudentNoList.schoolName);
                            var qrcode = new QRCode($codeArea.get(0), {
                                text: 'your content',
                                width: 65,
                                height: 65,
                                colorDark : '#000000',
                                colorLight : '#ffffff',
                                correctLevel : QRCode.CorrectLevel.H
                            });
                            qrcode.clear();
                            qrcode.makeCode($scope.currStudentNoList.schoolNo+','+$scope.currStudentNoList.empId);//二位码包含的信息
                            $sc.appendTo($parent);
                        }
                    }else if($scope.allOrSelf==='1'){
                        _.each($scope.stuCodes, function(s){
                            var $sc = $('<div class="print-code out-exam-code"> ' +
                                '<div class="examCode"></div>' +
                                '<div class="detail"><div class="name"></div><div class="classroom"></div><div class="schoolmn"></div></div>'  +
                                '</div>');
                            var $codeArea = $sc.find('.examCode');
                            var $name = $sc.find('.name');
                            var $schoolmn = $sc.find('.schoolmn');
                            var $classroom = $sc.find('.classroom');
                            $name.text(s.userName);
                            $schoolmn.text(s.schoolName);
                            $classroom.text(s.className);
                            var qrcode = new QRCode($codeArea.get(0), {
                                text: 'your content',
                                width: 65,
                                height: 65,
                                colorDark : '#000000',
                                colorLight : '#ffffff',
                                correctLevel : QRCode.CorrectLevel.H
                            });
                            qrcode.clear();
                            qrcode.makeCode(s.schoolNo+','+s.empId);//二位码包含的信息
                            $sc.appendTo($parent);
                        });
                    }
                }
            });
            
            //print   Attention: use print_css
            $scope.outExamprint = function () {
                $("#examPrint").jqprint({
                    operaSupport: false
                });
            }
        }]
})