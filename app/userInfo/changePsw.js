﻿define([], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$timeout',function ($http, $scope, $rootScope, $state, $timeout) {

        $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));

        //确认修改密码
        $scope.isCur = true;
        $scope.successShow = false;
        $scope.errorShow = false;
        $scope.serrorShow = false;
        $scope.perrorShow = false;
        $scope.lerrorShow = false;
        $scope.n1errorShow = false;
        $scope.n2errorShow = false;
        $scope.change = function () {


            if ($scope.PassWord == "" || $scope.PassWord == null) {
                $scope.errorShow = true;
                $timeout(function () {
                    $scope.errorShow = false;
                }, 900);
            }
            else {
                $scope.errorShow = false;
                if ($scope.NewPassWord1 == "" || $scope.NewPassWord1 == null) {
                    $scope.n1errorShow = true;
                    $timeout(function () {
                        $scope.n1errorShow = false;
                    }, 900);
                }
                else {
                    $scope.n1errorShow = false;
                    if ($scope.NewPassWord2 == "" || $scope.NewPassWord2 == null) {
                        $scope.n2errorShow = true;
                        $timeout(function () {
                            $scope.n2errorShow = false;
                        }, 900);
                    }
                    else {
                        $scope.n2errorShow = false;
                        var NullValue = "";//如果未改变，赋值""
                        var ChangePwd = $scope.NewPassWord1 == $scope.NewPassWord2 ? $scope.NewPassWord1 : NullValue;

                        if (ChangePwd == "" || ChangePwd == null) {
                            $scope.perrorShow = true;
                            $timeout(function () {
                                $scope.perrorShow = false;
                            }, 1000);
                        }
                        else {
                            $scope.perrorShow = false;
                            if (ChangePwd != null && ChangePwd.length < 6) {
                                console.log(ChangePwd.length);
                                $scope.lerrorShow = true;
                                $timeout(function () {
                                    $scope.lerrorShow = false;
                                }, 800);
                            } else {
                                $scope.lerrorShow = false;
                                $http({
                                    method: 'post',
                                    url: $rootScope.baseUrl + '/Interface0190.ashx',
                                    data: {
                                        SelfName: NullValue,
                                        Sex: NullValue,
                                        Birthday: NullValue,
                                        QQ: NullValue,
                                        Mobile: NullValue,
                                        Email: NullValue,
                                        PassWord: $scope.PassWord,
                                        NewPassWord: ChangePwd
                                    }

                                }).success(function (data) {
                                    console.log(data);
                                    if (data.code == 0) {

                                        $scope.successShow = true;
                                        sessionStorage.removeItem('currentUser');

                                        $timeout(function () {
                                            $state.go('login');
                                        }, 2000);

                                    } else {

                                        $scope.errorMsg = data.msg;
                                        $scope.serrorShow = true;
                                        $timeout(function () {
                                            $scope.serrorShow = false;
                                        }, 1000);
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    }]
})


