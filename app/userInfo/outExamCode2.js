/**
 * Created by Administrator on 2017/2/23 0023.
 */
define(['underscore-min', 'jqprint', 'jqrcode', 'pagination'], function () {
    return function ($rootScope, $http, $scope, $state, ngDialog, $timeout, $q, $location) {
        var user = angular.fromJson(sessionStorage.getItem('currentUser'));
        $scope.allOrSelf = $location.search().allOrSelf;
        $scope.QrCode = $location.search().QrCode;
        $scope.QrCode2 = $location.search().QrCode2;
        $scope.ClassFlnkID = $location.search().ClassFlnkID;
        $scope.currStudentNo = $location.search().studentNo;
        $scope.s = [];
        $scope.ClassFlnkID = $scope.ClassFlnkID.split(',');
        $scope.qrcodePageConf = {
            currentPage: 1,
            itemsPerPage: 1680
        };
        var qrcodeCache = {};
        var $container = $('#out-code');
        var $tempContainer = $('#temp-qr');
        var $clone = $('<div class="out-exam-code"> ' +
            '<div class="examCode"></div>' +
            '<div class="detail"><div class="name"></div><div class="classUserNo"></div><div class="schoolmn"></div>' +
            '</div></div>');

        var handGradeName = [
            {gradeName: '一年级', GradNum: 1}, {gradeName: '二年级', GradNum: 2}, {gradeName: '三年级', GradNum: 3},
            {gradeName: '四年级', GradNum: 4}, {gradeName: '五年级', GradNum: 5}, {gradeName: '六年级', GradNum: 6},
            {gradeName: '初一', GradNum: 7}, {gradeName: '初二', GradNum: 8}, {gradeName: '初三', GradNum: 9},
            {gradeName: '高一', GradNum: 10}, {gradeName: '高二', GradNum: 11}, {gradeName: '高三', GradNum: 12}];
        var deferList = [];
        _.each($scope.ClassFlnkID, function (val) {
            deferList.push($http.post($rootScope.baseUrl + '/Interface0233.ashx', {
                classFLnkId: val,
                schoolFLnkId: user.schoolFId
            }));
        });
        $q.all(deferList).then(function (res) {
            if ($scope.allOrSelf === '1') {
                _.each(res, function (item) {
                    $scope.stuCodes = [];
                    if (item.data && Array.isArray(item.data.msg) && item.data.msg.length) {
                        $scope.className = item.data.msg[0].className;
                        $scope.ss = item.data.msg;
                        for (var i = 0; i < $scope.QrCode || i < $scope.QrCode2; i++) {
                            _.each($scope.ss, function (item) {
                                $scope.stuCodes.push(item);
                            })
                        }
                        if ($scope.QrCode2 !== undefined) {
                            $scope.stuCodes.sort(function (a, b) {
                                return a.studentNo - b.studentNo;
                            });
                            $scope.stuCodes.sort(function (a, b) {
                                return a.studentSmallNo - b.studentSmallNo;
                            });
                        }
                    }
                    _.each($scope.stuCodes, function (item) {
                        $scope.s.push(item);
                    });
                });
            } else {
                _.each(res[0].data.msg, function (item) {
                    if (item.studentFLnkId === $scope.currStudentNo) {
                        for (var i = 0; i < 84; i++){
                            $scope.s.push(item);
                        }
                    }
                });
            }
            $scope.qrcodePageConf.totalItems = $scope.s.length;
            var currentPageData = $scope.s.slice(0, $scope.qrcodePageConf.itemsPerPage);
            showQRCode(currentPageData);
        });
        $scope.$watch('qrcodePageConf.currentPage', function (val, oldVal) {
            if (val && val !== oldVal) {
                var start = $scope.qrcodePageConf.currentPage === 1 ? 0 : ($scope.qrcodePageConf.currentPage - 1) * $scope.qrcodePageConf.itemsPerPage;
                var end = start + $scope.qrcodePageConf.itemsPerPage;
                var data = $scope.s.slice(start, end);
                showQRCode(data);
            }
        });
        function showQRCode(fileList) {
            $container.empty();
            var i = 1, n = 0, $sss;
            $sss = $('<div id="examPrint" class="examPrintUsl"><div class="bottom-page">&nbsp;' + $scope.qrcodePageConf.currentPage + '-' + i + '</div></div>');
            _.each(fileList, function (s, index) {
                _.each(handGradeName, function (GradeName) {
                    if (+s.gradeName === GradeName.GradNum) {
                        s.gradeNameNew = GradeName.gradeName;
                    }
                });
                if (s.classNo.toString().length === 1) {
                    s.classNo = '0' + s.classNo;
                }
                if(s.studentSmallNo) {
                    if (s.studentSmallNo.toString().length === 1) {
                        s.studentSmallNo = '0' + s.studentSmallNo;
                    }
                }else {
                    s.studentSmallNo = '00';
                }
                var $sc = $clone.clone();
                var $codeArea = $sc.find('.examCode');
                var $name = $sc.find('.name');
                var $classUserNo = $sc.find('.classUserNo');
                var $schoolmn = $sc.find('.schoolmn');
                $name.text(s.studentName);
                $schoolmn.text(s.schoolName);
                $classUserNo.text(s.gradeNameNew + s.classNo + '班(' + s.studentSmallNo + ')');
                var qrcodeData = getQrCode('', s.classNo + ',' + s.studentNo);
                $('<img>').attr('src', qrcodeData).appendTo($codeArea);
                if (n < 84) {
                    n += 1;
                    $sc.appendTo($sss);
                    if(index === fileList.length - 1) {
                        $sss.appendTo($container);
                    }
                } else {
                    i = i + 1;
                    n = 1;
                    $sss.appendTo($container);
                    $sss = $('<div id = examPrint' + i + ' class="examPrintUsl"><div class="bottom-page">&nbsp;' + $scope.qrcodePageConf.currentPage + '-' + i + '</div></div>');
                    $sc.appendTo($sss);
                }
            });
        }
        function getQrCode(node, text){
            if(qrcodeCache[text]) {
                return qrcodeCache[text];
            }else {
                node = $tempContainer.empty().get(0);
                var qrcode = new QRCode(node, {
                    text: text,
                    width: 64,
                    height: 64,
                    colorDark: '#000000',
                    colorLight: '#ffffff',
                    correctLevel: QRCode.CorrectLevel.L
                });
                var $el = $(qrcode._el).find('canvas').get(0);
                var imgData = $el.toDataURL();
                qrcodeCache[text] = imgData;
                qrcode = null;
                $tempContainer.empty();
                return imgData;
            }
        }
        $scope.doPrint = function() {
            $('#out-code').jqprint({
                operaSupport:false,
                importCss:true,
                debug: false,
                printContainer: true
            });
        };
    }
});


// define(['underscore-min', 'jquery.dataTables', 'outexcel', 'jqprint', 'jqrcode'], function () {
//     return ['$rootScope', '$http', '$scope', '$timeout', 'ngDialog', '$sce', '$location', '$q',
//         function ($rootScope, $http, $scope, $timeout, ngDialog, $sce, $location, $q) {
//             $scope.isCur = true;
//
//             $scope.allOrSelf = $location.search().allOrSelf;
//             $scope.QrCode = $location.search().QrCode;
//             $scope.QrCode2 = $location.search().QrCode2;
//             $scope.SchoolFid = $location.search().SchoolFid;
//             $scope.ClassFlnkID = $location.search().ClassFlnkID;
//             $scope.currStudentNo = $location.search().studentNo;
//
//             var $parent = $("#examPrint");
//             $scope.s = [];
//             var deferList = [];
//             $scope.ClassFlnkID = $scope.ClassFlnkID.split(',');
//
//             var handGradeName = [
//                 {gradeName: '一年级', GradNum: 1}, {gradeName: '二年级', GradNum: 2}, {gradeName: '三年级', GradNum: 3},
//                 {gradeName: '四年级', GradNum: 4}, {gradeName: '五年级', GradNum: 5}, {gradeName: '六年级', GradNum: 6},
//                 {gradeName: '初一', GradNum: 7}, {gradeName: '初二', GradNum: 8}, {gradeName: '初三', GradNum: 9},
//                 {gradeName: '高一', GradNum: 10}, {gradeName: '高二', GradNum: 11}, {gradeName: '高三', GradNum: 12}];
//
//             _.each($scope.ClassFlnkID, function (val) {
//                 deferList.push($http.post($rootScope.baseUrl + '/Interface0233.ashx', {
//                     classFLnkId: val,
//                     schoolFLnkId: $scope.SchoolFid
//                 }));
//             });
//
//             $q.all(deferList).then(function (res) {
//                 if ($scope.allOrSelf === '1') {
//                     _.each(res, function (item) {
//                         $scope.stuCodes = [];
//                         if (item.data && Array.isArray(item.data.msg) && item.data.msg.length) {
//                             $scope.className = item.data.msg[0].className;
//                             $scope.ss = item.data.msg;
//                             for (var i = 0; i < $scope.QrCode || i < $scope.QrCode2; i++) {
//                                 _.each($scope.ss, function (item) {
//                                     $scope.stuCodes.push(item);
//                                 })
//                             }
//                             if ($scope.QrCode2 !== undefined) {
//                                 $scope.stuCodes.sort(function (a, b) {
//                                     return a.studentSmallNo - b.studentSmallNo;
//                                 });
//                             }
//                         }
//                         _.each($scope.stuCodes, function (item) {
//                             $scope.s.push(item);
//                         });
//                     });
//                 } else {
//                     _.each(res[0].data.msg, function (item) {
//                         if (item.studentNo === $scope.currStudentNo) {
//                             $scope.s.push(item);
//                         }
//                     });
//                 }
//                 showQRCode();
//             });
//
//             function showQRCode() {
//                 _.each($scope.s, function (s) {
//                     _.each(handGradeName, function (GradeName) {
//                         if (+s.gradeName === GradeName.GradNum) {
//                             s.gradeNameNew = GradeName.gradeName;
//                         }
//                     });
//                     if (s.classNo.length === 1) {
//                         s.classNo = '0' + s.classNo;
//                     }
//                     if (s.studentSmallNo.length === 1) {
//                         s.studentSmallNo = '0' + s.studentSmallNo;
//                     }
//                     var $sc = $('<div class="out-exam-code2"> ' +
//                         '<div class="examCode2"></div>' +
//                         '<div class="detail2"><div class="name2"></div><div class="classUserNo"></div><div class="schoolmn2"></div>' +
//                         '</div></div>');
//                     var $codeArea = $sc.find('.examCode2');
//                     var $name = $sc.find('.name2');
//                     var $classUserNo = $sc.find('.classUserNo');
//                     var $schoolmn = $sc.find('.schoolmn2');
//                     $name.text(s.studentName);
//                     $schoolmn.text(s.schoolName);
//                     $classUserNo.text(s.gradeNameNew + s.classNo + '班(' + s.studentSmallNo + ')');
//                     var qrcode = new QRCode($codeArea.get(0), {
//                         text: 'your content',
//                         width: 74,
//                         height: 74,
//                         colorDark: '#000000',
//                         colorLight: '#ffffff',
//                         correctLevel: QRCode.CorrectLevel.L
//                     });
//                     qrcode.clear();
//                     qrcode.makeCode(s.classNo + ',' + s.studentNo);//二位码包含的信息
//                     $sc.appendTo($parent);
//                 });
//             }
//         }]
// });