﻿define(['underscore-min'], function () {
    return ['$rootScope', '$http', '$scope', 'ngDialog', '$timeout',function ($rootScope, $http, $scope, ngDialog, $timeout) {
        $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
        var compareClass = $scope.currentUser.CompareClass.split(',');
        var cookClassFlnkid = [];
        var newCookClassFlnkid = [];
        $scope.isCur = true;

        if (compareClass.length === 2) {//有对比班级
            getAllClassData().success(function (data) {
                for (var i = 0; i < data.msg.length; i++) {
                    cookClassFlnkid.push({
                        className: data.msg[i].ClassName,
                        classId: data.msg[i].ClassID,
                        isCurrent: false
                    })
                }
                //查找接口获取的对比班级在全部班级的索引（在集合中查找某属性集体一项索引_.findIndex）
                var f = _.findIndex(cookClassFlnkid, { classId: compareClass[0] });
                var t = _.findIndex(cookClassFlnkid, { classId: compareClass[1] });
                var strArr = [f, t];
                //将接口获取的对比班级标记为选中状体态
                cookClassFlnkid[f].isCurrent = true;
                cookClassFlnkid[t].isCurrent = true;
                //将对比班级信息存入新数组
                _.each(strArr, function (item, index) {
                    newCookClassFlnkid.push({
                        className: cookClassFlnkid[item].className,
                        classId: cookClassFlnkid[item].classId,
                        isCurrent: true
                    })
                });

                $scope.classList = cookClassFlnkid;
                console.log(cookClassFlnkid);
                console.log(newCookClassFlnkid);
            });
         

        } else {//没有对比班级

            //获取首页对比班级
            $http.get($rootScope.baseUrl + '/Interface0200.ashx').success(function (data) {
                if (!!data && data.msg.length == 2 && data.msg[0].ClassFlnkid != data.msg[1].ClassFlnkid) {
                    var t = data.msg[0].ClassFlnkid + ',' + data.msg[1].ClassFlnkid;
                    var str = t.split(',');

                    getAllClassData().success(function (data) {
                        for (var i = 0; i < data.msg.length; i++) {
                            cookClassFlnkid.push({
                                className: data.msg[i].ClassName,
                                classId: data.msg[i].ClassID,
                                isCurrent: false
                            })
                        }
                        //查找接口获取的对比班级在全部班级的索引（在集合中查找某属性集体一项索引_.findIndex）
                        var f = _.findIndex(cookClassFlnkid, { classId: str[0] });
                        var t = _.findIndex(cookClassFlnkid, { classId: str[1] });
                        var strArr = [f, t];
                        //将接口获取的对比班级标记为选中状体态
                        cookClassFlnkid[f].isCurrent = true;
                        cookClassFlnkid[t].isCurrent = true;
                        //将对比班级信息存入新数组
                        _.each(strArr, function (item, index) {
                            newCookClassFlnkid.push({
                                className: cookClassFlnkid[item].className,
                                classId: cookClassFlnkid[item].classId,
                                isCurrent: true
                            })
                        });

                        $scope.classList = cookClassFlnkid;
                        console.log(cookClassFlnkid);
                        console.log(newCookClassFlnkid);
                    });

                } else {

                    getAllClassData().success(function (data) {
                        for (var i = 0; i < data.msg.length; i++) {
                            cookClassFlnkid.push({
                                className: data.msg[i].ClassName,
                                classId: data.msg[i].ClassID,
                                isCurrent: false
                            })
                        }

                        $scope.classList = cookClassFlnkid;
                    });
                }

            });
 
        };
       

        //页面点击获取相应信息（班级id,name,选中状态，索引）
        $scope.changeShow = function (id, name, isCurrent, index) {
            if (isCurrent) {//del
                isCurrent = false;
                var r = _.findIndex(newCookClassFlnkid, { classId: cookClassFlnkid[index].classId });
                newCookClassFlnkid.splice(r, 1);
                console.log(newCookClassFlnkid);
            } else {//add
                if (newCookClassFlnkid.length == 2) {
                    ngDialog.open({
                        template:
                            '<p>只能选择2个不同班级比较！</p>' +
                            '<div class="ngdialog-buttons">' +
                            '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                        plain: true,
                        className: 'ngdialog-theme-plain'
                    });
                    return false;
                };

                isCurrent = true;
                newCookClassFlnkid.push({
                    className: name,
                    classId: id,
                    isCurrent: true
                });
                console.log(newCookClassFlnkid);
            }
            cookClassFlnkid[index].isCurrent = isCurrent;
        };
 
        function getAllClassData() {return  $http.get($rootScope.baseUrl + '/Interface0192.ashx')};
           
        //保存比较
        $scope.save = function () {
            $scope.successShow = false;
            console.log(newCookClassFlnkid);
            var compareID = '', ids = [];

            angular.forEach(newCookClassFlnkid, function (item) {
                ids.push(item.classId);
            });
            if (newCookClassFlnkid.length <= 1) {
                ngDialog.open({
                    template:
                        '<p>请选择2个不同班级比较！</p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                    plain: true,
                    className: 'ngdialog-theme-plain'
                });
                return false;
            };

            compareID = ids[0] + ',' + ids[1];
            console.log(compareID);
            $http({
                method: 'post',
                url: $rootScope.baseUrl + '/Interface0193.ashx',
                data: {
                    CompareClass: compareID
                }
            }).success(function (data) {
                if (data.code == 0) {
                    console.log('save ok ');
                    window.localStorage.setItem('CompareClass', compareID);
                    var user = angular.fromJson(sessionStorage.getItem('currentUser'));
                    user.CompareClass = compareID;
                    sessionStorage.setItem('currentUser', angular.toJson(user));
                    $scope.successShow = true;
                    $timeout(function () {
                        $scope.successShow = false;
                    }, 1500);
                } else {
                    console.log('save error');
                    $scope.errorMsg = data.msg;
                }
            });
        };

    }]
})