﻿define(['jquery.datetimepicker'], function(){
    return ['$rootScope', '$http', '$scope', '$timeout', 'ngDialog', '$state',function($rootScope, $http, $scope, $timeout, ngDialog, $state){
            $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
            $scope.isCur = true;
            $scope.save = function(){
                //点击保存方法
                $scope.errorShow = false;
                $scope.successShow = false;
                $scope.currentUser.birthday = $('#timepicker').val();
                console.log($scope.currentUser);
                //如果未改变，赋值""
                var NullValue = "";
                $http({
                    method : 'post',
                    url : $rootScope.baseUrl + '/Interface0190.ashx',
                    data : {
                        SelfName : $scope.currentUser.SelfName,
                        Sex : $scope.currentUser.sex,
                        Birthday : $scope.currentUser.birthday,
                        QQ : $scope.currentUser.qq,
                        Mobile : $scope.currentUser.tel,
                        Email : $scope.currentUser.email,
                        PassWord : $scope.currentUser.pw,
                        NewPassWord : NullValue
                    }
                }).success(function(data){
                    console.log(data);
                    if(data.code == 0){
                        console.log('ok');
                        sessionStorage.setItem('currentUser', angular.toJson($scope.currentUser));
                        $scope.successShow = true;
                        $timeout(function(){
                            $scope.successShow = false;
                        }, 1200);
                    } else if(data.code == 3){
                        console.log('error');
                        $scope.errorShow = true;
                        $timeout(function(){
                            $scope.errorShow = false;
                        }, 800);
                        $scope.currentUser = angular.copy($scope.user);
                    }
                });
            };
            //修改密码
            $scope.change = function(){
                if($scope.PassWord == "" || $scope.PassWord == null){
                    $scope.PWDserrorShow = true;
                    $timeout(function(){
                        $scope.PWDserrorShow = false;
                    }, 900);
                }
                else {
                    $scope.PWDserrorShow = false;
                    if($scope.NewPassWord1 == "" || $scope.NewPassWord1 == null){
                        $scope.n1errorShow = true;
                        $timeout(function(){
                            $scope.n1errorShow = false;
                        }, 900);
                    }
                    else {
                        $scope.n1errorShow = false;
                        if($scope.NewPassWord2 == "" || $scope.NewPassWord2 == null){
                            $scope.n2errorShow = true;
                            $timeout(function(){
                                $scope.n2errorShow = false;
                            }, 900);
                        }
                        else {
                            $scope.n2errorShow = false;
                            var NullValue = "";//如果未改变，赋值""
                            var ChangePwd = $scope.NewPassWord1 == $scope.NewPassWord2 ? $scope.NewPassWord1 : NullValue;
                            if(ChangePwd == "" || ChangePwd == null){
                                $scope.perrorShow = true;
                                $timeout(function(){
                                    $scope.perrorShow = false;
                                }, 1000);
                            }
                            else {
                                $scope.perrorShow = false;
                                if(ChangePwd != null && ChangePwd.length < 6){
                                    console.log(ChangePwd.length);
                                    $scope.lerrorShow = true;
                                    $timeout(function(){
                                        $scope.lerrorShow = false;
                                    }, 800);
                                } else {
                                    $scope.lerrorShow = false;
                                    $http({
                                        method : 'post',
                                        url : $rootScope.baseUrl + '/Interface0190.ashx',
                                        data : {
                                            SelfName : NullValue,
                                            Sex : NullValue,
                                            Birthday : NullValue,
                                            QQ : NullValue,
                                            Mobile : NullValue,
                                            Email : NullValue,
                                            PassWord : $scope.PassWord,
                                            NewPassWord : ChangePwd
                                        }
                                    }).success(function(data){
                                        console.log(data);
                                        if(data.code == 0){
                                            $scope.PWDsuccessShow = true;
                                            sessionStorage.removeItem('currentUser');
                                            $timeout(function(){
                                                $state.go('login');
                                            }, 2000);
                                        } else {
                                            $scope.errorMsg = data.msg;
                                            $scope.PWDserrorShow = true;
                                            $timeout(function(){
                                                $scope.PWDserrorShow = false;
                                            }, 1000);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            };
            //日历
            $('#timepicker').datetimepicker({
                lang : "ch",
                timepicker : false,
                format : 'Y-m-d',
                todayButton : true,
                maxDate : new Date(),
                scrollInput : false,
                onChangeDateTime : function(dp, $input){
                    $('#timepicker').datetimepicker('hide');
                }
            });
        }];
});



