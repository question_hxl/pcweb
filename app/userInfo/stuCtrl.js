﻿define(['underscore-min', 'jquery.dataTables', 'outexcel', 'jqprint'], function () {
    return ['$rootScope', '$http', '$scope', '$timeout', 'ngDialog', '$sce',function ($rootScope, $http, $scope, $timeout, ngDialog, $sce) {
        $scope.currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
        var CookClassFlnkid = [];
        var cookClassFlnkid = [];
        var classListTwo = [];
        $scope.isCur = true;
        $scope.noStudata = false;
        $scope.noClassdata = false;
        $scope.configUserFor = config.theme;

        //获取班级信息
        $http.get($rootScope.baseUrl + '/Interface0027.ashx').success(function (data) {
            if (data && data.msg && _.isArray(data.msg)) {
                for (var i = 0; i < data.msg.length; i++) { // 存储班级所有ID
                    cookClassFlnkid.push(data.msg[i].classId);
                }

                $http.get($rootScope.baseUrl + '/Interface0192.ashx').success(function (data) {
                    if (data && data.msg && _.isArray(data.msg)) {
                        $scope.noClassdata = false;
                        $scope.classList = data.msg;
                        for (var i = 0; i < data.msg.length; i++) { // 存储班级所有ID
                            CookClassFlnkid.push(data.msg[i].ClassID)
                        }

                        var sameClassId = _.intersection(cookClassFlnkid, CookClassFlnkid);
                        _.each(sameClassId, function (item) {
                            var f = _.findIndex($scope.classList, {ClassID: item});
                            classListTwo.push($scope.classList[f]);
                        });
                        $scope.classListTwo = classListTwo;
                        //初次进入显示选中   
                        $scope.className = $scope.classListTwo[0].ClassName;
                        //绑定下拉列表当前班级
                        $scope.addstuClassName = $scope.classListTwo[0];
                        $scope.currentClassID = $scope.classListTwo[0].ClassID;

                        //初次载入默认班级学生信息
                        getStudentList($scope.currentClassID);
                    } else {
                        //无班级信息
                        $scope.noClassdata = true;
                    }
                })
            } else {
                //无班级信息
                $scope.noClassdata = true;
            }
        });


        //获取学校列表
        /* var SchoolInfo = [];
         $http.get($rootScope.baseUrl + '/Interface0229.ashx').success(function (data) {

         if (!!data && data.msg) {
         for (var i = 0 ; i < data.msg.length ; i++) { // 存储班级所有ID
         SchoolInfo.push(data.msg[i].schoolFLnkId)
         };
         } else {

         }
         });*/


        //点击获取班级学生信息（同时切换选中状态）
        $scope.selectClass = function (classs, $index) {
            $scope.addstuClassName = classs;//绑定下拉列表当前班级
            $scope.currentClassID = classs.ClassID;
            $scope.className = classs.ClassName;//切换显示选中   
            getStudentList(classs.ClassID);//获取学生列表
        };


        //新增学生
        $scope.nullTip = false;
        $scope.submitted1 = false;//是否提交
        $scope.singleIpt = function () {
            ngDialog.open({
                template: 'template/sameStu.html',
                className: 'ngdialog-theme-plain',
                scope: $scope,
            });
            //新生查询
            $scope.searchstu = function (username) {
                if (!username) {
                    $scope.nullTip = true;
                    $timeout(function () {
                        $scope.nullTip = false;
                    }, 1200);
                } else {
                    $scope.nullTip = false;
                    $http.post($rootScope.baseUrl + '/Interface0233A.ashx', {
                        loginName: username || ""
                    }).success(function (data) {
                        if (!!data && data.code != 2 && data.msg[0]) {
                            var sameschoolName = (data.msg[0].schoolName === $scope.currentUser.schoolName) ? true : false;//同学校
                            if (sameschoolName) {
                                ngDialog.close();
                                $scope.editStu(data.msg[0], 0);//有学生信息，就去修改
                                return true;
                            }
                        }
                        ngDialog.close();
                        $scope.singleIptFun(username);
                    });
                }
            };
        };
        $scope.singleIptFun = function (username) {
            $scope.editState = "";
            ngDialog.open({
                template: 'template/editStu.html',
                className: 'ngdialog-theme-plain',
                scope: $scope
            });
            //构建学生对象，用于数据绑定
            $scope.student = {
                _addstuName: "",
                stuSchool: $scope.currentUser.schoolName,
                stuClassName: $scope.addstuClassName,
                _addstuEmpID: "",
                _addstuCno: "",
                _addloginName: username || "",
                _addstuPsw: '',
            };
            $scope.addStu = function (student_) {
                console.log(student_);
                if (!student_._addstuName || !student_._addstuCno || !student_._addstuEmpID || !student_._addloginName || !student_._addstuPsw) {
                    $scope.submitted1 = true;
                    $timeout(function () {
                        $scope.submitted1 = false
                    }, 1000);
                    return false;
                } else {
                    $scope.submitted1 = false;
                    if (checkeNum(student_._addstuEmpID, 1)) {
                        return false;
                    } else {
                        savestuinfo(student_);
                    }
                }
            };
        };

        //新增   
        function savestuinfo(student) {
            $scope.addSucss = false;
            $scope.addFail = false;
            $http.post($rootScope.baseUrl + '/Interface0139.ashx', {
                studentName: student._addstuName,
                schoolFLnkId: $scope.schoolId,
                classFLnkId: student.stuClassName.ClassID,
                studentNo: student._addstuEmpID,
                loginName: student._addloginName,
                password: student._addstuPsw,
                smallNum: student._addstuCno.length = 1 ? '0' + student._addstuCno : student._addstuCno
            }).then(function (resp) {
                if (resp.data.code == 0) {
                    $scope.addSucss = true;
                    $timeout(function () {
                        $scope.addSucss = false;
                        ngDialog.close();
                    }, 1200);
                    reView(student);
                } else {
                    $scope.addFail = true;
                    $timeout(function () {
                        $scope.addFail = false;
                    }, 1500);
                }
            });
        }

        //删除学生
        $scope.delStu = function (stu, $index) {
            ngDialog.openConfirm({
                template: '<p>确定删除？</p>' +
                '<div class="ngdialog-buttons">' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closedia()">取消</button>' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(del())">确定</button></div>',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                plain: true,
                preCloseCallback: function () { //回调
                    console.log("执行回调！");
                }
            });

            //执行删除
            $scope.del = function () {
                delstuinfo(stu)
            };

        };


        //删除
        function delstuinfo(student) {
            $http.post($rootScope.baseUrl + '/Interface0138A.ashx', {
                username: student.studentName,
                password: student.password || '123456',
                empid: student.studentNo,
                stufid: student.studentFLnkId,
                classid: student.classFLnkId,
                isChangeClass: "0",
                isdelete: "true"
            }).then(function (resp) {
                if (resp.data.code == 0) {
                    var r = _.findIndex($scope.stuList, {studentFLnkId: student.studentFLnkId});
                    $scope.stuList.splice(r, 1);
                }
            });
        };

        //修改学生
        $scope.submitted = false;//是否提交
        $scope.editStu = function (stu, $index) {
            $scope.editState = "edit";
            //构建学生对象，用于数据绑定
            $scope.stu = {
                schoolFLnkId: stu.schoolFLnkId,
                studentId: stu.studentFLnkId,
                stuName: stu.studentName,
                stuSchool: $scope.currentUser.schoolName,
                stuClassName: $scope.addstuClassName,
                stuGradeName: stu.gradeName,
                stuEmpID: stu.studentNo,
                stuCno: stu.studentSmallNo.length == 1 ? '0' + stu.studentSmallNo : stu.studentSmallNo,
                loginName: stu.loginName,
                stuPsw: stu.studentpwd || '',
                stuPow: stu.isBind,
                stuWxBind: stu.isBind == "已绑定" ? false : true,
                SFID:stu.SFID,
                Period:stu.Period
            };
            $scope.oldstudentNo = stu.studentNo;
            $scope.oldstudentSmallNo = stu.studentSmallNo;
            $scope.wxBindView = !$scope.stu.stuWxBind;

            // ngDialog.open({
            //     template: 'template/editStu.html',
            //     className: 'ngdialog-theme-plain',
            //     scope: $scope
            // });

            ngDialog.open({
                template: 'template/reeditStu.html',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                appendClassName: 'reeditStu-dialog'
            });

            $scope.saveStu = function (stu_) {
                if (!stu_.stuName || !stu_.stuCno || !stu_.stuEmpID || !stu_.loginName) {
                    $scope.submitted = true;
                    $timeout(function () {
                        $scope.submitted = false
                    }, 1000);
                    return false;
                } else {
                    $scope.submitted = false;
                    stu_.stuWxBind = (stu.isBind == false ? "已绑定" : "未绑定");
                    if ($scope.oldstudentNo == stu_.stuEmpID) {//没有修改学号动作，可以保存
                        stuedit(stu_);
                    } else if (checkeNum(stu_.stuEmpID, 1)) {//修改了则校验
                        return false;
                    } else {
                        stuedit(stu_);
                    }
                }
            };
        };

        //关闭ngDialog
        $scope.closedia = function () {
            console.log("取消删除");
            ngDialog.close();//.close()执行关闭方法后自动调用preCloseCallback
        };


        //提示信息
        $scope.showTipInfo = false;
        $scope.showTip = function () {
            $scope.showTipInfo = true;
            $timeout(function () {
                $scope.showTipInfo = false
            }, 1000);
        };

        //1校验学号、0班级小号
        $scope.checkLive = false;
        $scope.smallcheckLive = false;
        var checkeNum = function (num, type) {
            if (type == 1) {//校验学号
                var index = _.findIndex($scope.stuList, {studentNo: num});
                if (index >= 0) {
                    $scope.checkLive = true;
                    $timeout(function () {
                        $scope.checkLive = false;
                    }, 1500);
                    return true;
                } else {
                    $scope.checkLive = false;
                    return false;
                }
            } else if (type == 0) {
                var index = _.findIndex($scope.stuList, {studentSmallNo: num});
                if (index >= 0) {
                    $scope.smallcheckLive = true;
                    $timeout(function () {
                        $scope.smallcheckLive = false;
                    }, 1500);
                    return true;
                } else {
                    $scope.smallcheckLive = false;
                    return false;
                }
            }
        };

        //修改方法
        function stuedit(student) {
            $scope.saveSucss = false;
            $scope.saveFail = false;
            $http.post($rootScope.baseUrl + '/Interface0234.ashx', {
                studentFLnkId: student.studentId,              //学生FLnkId
                studentName: student.stuName,                //学生名称
                schoolFLnkId: student.schoolFLnkId,         //学校FLnkId
                classFLnkId: student.stuClassName.ClassID,  //班级FLnkId
                studentNo: student.stuEmpID,                //学生学号
                loginName: student.loginName,               //登录名
                password: student.stuPsw,               //密码
                stuSmallNum: student.stuCno
            }).then(function (resp) {
                if (resp.data.code == 0) {
                    $scope.saveSucss = true;
                    $timeout(function () {
                        $scope.saveSucss = false;
                        ngDialog.close();
                    }, 1200);
                    reView(student);
                    return;
                } else {
                    $scope.saveFail = true;
                    $timeout(function () {
                        $scope.saveFail = false;
                    }, 1500);
                    return;
                }
            });
        }

        //获取学生list
        var getStudentList = function (classId) {
            $http({
                method: 'post',
                url: $rootScope.baseUrl + '/Interface0233.ashx',
                data: {
                    classFLnkId: classId
                }
            }).success(function (data) {
                if (data && data.msg && _.isArray(data.msg)) {
                    $scope.noStudata = false;
                    $scope.stuList = data.msg;
                    $scope.schoolId = data.msg[0].schoolFLnkId;
                } else {
                    //无学生信息
                    $scope.noStudata = true;
                }
            });
        };

        //重绘选中班级
        var reView = function (student) {
            var index = _.findIndex($scope.classListTwo, {ClassID: student.stuClassName.ClassID});
            if (index >= 0) {
                $scope.addstuClassName = student.stuClassName;
                $scope.currentClassID = student.stuClassName.ClassID;
                $scope.className = student.stuClassName.ClassName;//切换显示选中                  
            } else {
                $scope.addstuClassName = $scope.classListTwo[0];
                $scope.currentClassID = $scope.classListTwo[0].ClassID;
                $scope.className = $scope.classListTwo[0].ClassName;//切换显示选中            
            }
            getStudentList($scope.currentClassID);//获取学生列表
        };


        //重置密码
        $scope.rsetPsw = function (stu, $index) {
            ngDialog.openConfirm({
                template: '<p>确定重置？</p>' +
                '<div class="ngdialog-buttons">' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closedia()">取消</button>' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(rsetPsw_())">确定</button></div>',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                plain: true,
                preCloseCallback: function () { //回调
                    console.log("执行回调！");
                }
            });
            $scope.rsetPsw_ = function () {
                $http.post($rootScope.baseUrl + '/Interface0247.ashx', {
                    FLnkId: stu.studentFLnkId,              //学生FLnkId
                    type: 1
                }).then(function (resp) {

                    if (!!resp && resp.data.code == 0) {
                        ngDialog.open({
                            template: '<p>您好：<span style="color:green">' + stu.studentName + '</span>&nbsp;同学的密码已重置成功!</p>' +
                            '<p>新密码默认为：<span style="color:green">123456</span></p>' +
                            '<div class="ngdialog-buttons">' +
                            '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                            plain: true,
                            scope: $scope,
                            className: 'ngdialog-theme-plain'
                        });
                    } else {
                        alert("重置失败，信息不全！");
                    }
                });
            };
        };


        //批量重置密码    
        $scope.allRePsw = function () {
            ngDialog.openConfirm({
                template: '<p>确定重置？</p>' +
                '<div class="ngdialog-buttons">' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closedia()">取消</button>' +
                '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(allRePsw_())">确定</button></div>',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                plain: true,
                preCloseCallback: function () { //回调
                    console.log("执行回调！");
                }
            });

            console.log($scope.currentClassID);

            $scope.allRePsw_ = function () {
                $http.post($rootScope.baseUrl + '/Interface0247.ashx', {
                    FLnkId: $scope.currentClassID,              //班级FLnkId
                    type: 2
                }).then(function (resp) {
                    if (!!resp && resp.data.code == 0) {
                        ngDialog.open({
                            template: '<p>您好：所有学生密码已重置成功!</p>' +
                            '<p>新密码默认为：<span style="color:green">123456</span></p>' +
                            '<div class="ngdialog-buttons">' +
                            '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                            plain: true,
                            scope: $scope,
                            className: 'ngdialog-theme-plain'
                        });
                    } else {
                        alert("重置失败，信息不全！");
                    }
                });
            };
        };

        //排序指令(数组)
        $scope.tablethKeys = ['studentSmallNo', 'isBind'];

        //导出二维码弹出层
        $scope.outqrcode = function () {
            ngDialog.open({
                template: 'template/outqrcode.html',
                className: 'ngdialog-theme-default',
                appendClassName: 'ngdialog-theme-alert',
                scope: $scope,
                controller: function ($scope) {
                    return function () {
                        $http({
                            method: 'post',
                            url: $rootScope.baseUrl + '/Interface0233.ashx',
                            data: {
                                classFLnkId: $scope.currentClassID,
                                needWx: 1
                            }
                        }).success(function (data) {
                            if (data && data.msg && _.isArray(data.msg)) {
                                $scope.noStudata = false;
                                $scope.stuList = data.msg;
                                $scope.schoolId = data.msg[0].schoolFLnkId;
                            } else {
                                //无学生信息
                                $scope.noStudata = true;
                            }
                        });
                        $scope.onprint = function () {
                            $("#btnPrint").jqprint({
                                operaSupport: false
                            });
                        }
                    }
                }
            });
        };

        //考试二维码
        $scope.outExamqrcode = function (val, stu) {
            if (val === 1) {
                ngDialog.open({
                    template: '<div class="QrCode clearfix"> ' +
                    '<p class="mainNar"></p> ' +
                    '<big><p style="padding-top: 30px;text-align: center"> 考试二维码打印</p>' +
                    '<div style="padding: 0 40px;text-align: left"> <div style="width: 18%;float: left;margin-top: 15px;line-height: 40px">&nbsp;班&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;级&nbsp;：</div>' +
                    '<div style="width: 82%;float: left;margin-top: 15px"><div ng-repeat="item in classList" ng-click="curClassed(item)" class="pull-left" style="line-height: 40px">' +
                    '<img ng-show="currentclassFLnkID.indexOf(item.ClassID) === -1" src="../img/weixuan.png">' +
                    '<img ng-show="currentclassFLnkID.indexOf(item.ClassID) !== -1" src="../img/yixuan.png">&nbsp;{{item.ClassName}}&nbsp;&nbsp;&nbsp;&nbsp;</div></div></div>' +
                    '<div style="padding: 20px 40px 15px;text-align: left;clear: both">排列模式：' +
                    '<span ng-click="QrcModel(2)"><img ng-show="QrCodeModel == 1" src="../img/weixuan.png"><img ng-show="QrCodeModel == 2" src="../img/yixuan.png">&nbsp;AABB</span>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;<span ng-click="QrcModel(1)"><img ng-show="QrCodeModel == 2" src="../img/weixuan.png"><img ng-show="QrCodeModel == 1" src="../img/yixuan.png"></span>&nbsp;ABAB</div>' +
                    '<div style="padding: 20px 40px;;text-align: left">每人显示：&nbsp;&nbsp;<input ng-model="QrCode">&nbsp;&nbsp;份</div></big>' +
                    '<button class="smBtn" ng-click="goQrCode()">确定</button></div>',
                    className: 'ngdialog-theme-plain',
                    appendClassName: 'QrCode-confirm-to-setting',
                    scope: $scope,
                    closeByDocument: true,
                    plain: true,
                    controller: function ($scope) {
                        return function () {
                            $scope.QrCodeModel =2;
                            $scope.QrCode =1;
                            if($scope.classList.length === 1){
                                $scope.currentclassFLnkID = $scope.currentClassID;
                            }else {
                                $scope.currentclassFLnkID = [];
                                $scope.currentclassFLnkID[0] = $scope.currentClassID;
                            }
                            $scope.QrcModel = function (QrCodeModel) {
                                if($scope.QrCodeModel !== QrCodeModel){
                                    $scope.QrCodeModel = QrCodeModel;
                                }
                            };
                            $scope.curClassed = function (item) {
                                var index = _.indexOf($scope.currentclassFLnkID, item.ClassID);
                                if (index >= 0) {
                                    $scope.currentclassFLnkID.splice(index, 1);
                                } else {
                                    $scope.currentclassFLnkID.push(item.ClassID);
                                }
                                $scope.currentclassFLnkID.indexOf(item.ClassID)
                            };
                            $scope.goQrCode = function () {
                                ngDialog.close();
                                if ($scope.QrCodeModel ===1){
                                    window.open("#/outExamCode2?allOrSelf=" + val + '&QrCode=' + $scope.QrCode + '&SchoolFid=' + $scope.currentUser.schoolFId + '&ClassFlnkID=' + $scope.currentclassFLnkID + '&isOpenNewWin=1');
                                }else {
                                    window.open("#/outExamCode2?allOrSelf=" + val + '&QrCode2=' + $scope.QrCode + '&SchoolFid=' + $scope.currentUser.schoolFId + '&ClassFlnkID=' + $scope.currentclassFLnkID + '&isOpenNewWin=1');
                                }
                                //location.href ="#/outExamCode?ClassFlnkID=" + $scope.currentClassID + '&allOrSelf=' + val;
                            };
                        }
                    }
                });
            } else {
                window.open("#/outExamCode2?allOrSelf=" + val + '&studentNo=' + stu.studentFLnkId + '&SchoolFid=' + $scope.currentUser.schoolFId + '&ClassFlnkID=' + $scope.currentClassID + '&isOpenNewWin=' + 1);
                //location.href="#/outExamCode?ClassFlnkID=" + $scope.currentClassID + '&allOrSelf=' + val + '&studentNo=' + stu.studentNo;
            }
        };
        //print   Attention: use print_css
        $scope.outExamprint = function () {
            $("#examPrint").jqprint({
                operaSupport: false
            });
        }
    }]
});