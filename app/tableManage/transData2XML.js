/**
 * Created by 贺小雷 on 2016/9/23.
 */
(function($, _){
    /**
     *
     * @param data  Array [{sheetName: 'name',meta: [{}]}]  为表格元数据，单sheet数组长度为1，多sheet数组长度>1
     * @param type  1,2,3,4,5  为表格类型
     * @param classId   班级ID
     */
    var typeGroup = [];//存储小题分报表分组信息名称
    $.transData2Xml4Excel = function(data, type, classId){
        //准备xml基本结构
        var xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><?mso-application progid="Excel.Sheet"?>'+
        '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office"'+
        ' xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"'+
        ' xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">';
        xml += '<Styles><Style ss:ID="s1"><Alignment ss:Horizontal="Center"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>' +
            '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>' +
            '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/></Borders></Style>' +
            '<Style ss:ID="s2"><Font ss:Bold="1"/><Alignment ss:Horizontal="Center"/><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>' +
            '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>' +
            '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/></Borders></Style>' +
            '<Style ss:ID="s50"><Borders><Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>' +
            '<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/><Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>' +
            '<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/></Borders></Style>' +
            '</Styles>';
        _.each(data, function(item, index){
            xml += '<Worksheet ss:Name="' + item.sheetName + '">';
            xml += '<Table x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="32" ss:DefaultRowHeight="14">';
            var fillXML = '';
            //根据不同类型的表格处理填充的内容
            switch (type){
                case 1:
                    fillXML = fillTable1(item.meta);
                    break;
                default:
                    break;
            }
            xml += fillXML;
            if(item.meta.subTotal.length > 1){
                xml += '<Row></Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">均分</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.AvgSc + '</Data></Cell>';
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">年级均分</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.GAvgSc + '</Data></Cell>';
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">标准差</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    if(subitem.SubjectName !== "综合") {
                        xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.BZC + '</Data></Cell>';
                    }
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">优分率</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    if(subitem.SubjectName !== "综合") {
                        xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.GoodRate + '</Data></Cell>';
                    }
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">及格率</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    if(subitem.SubjectName !== "综合") {
                        xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.PassRate + '</Data></Cell>';
                    }
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">差分率</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    if(subitem.SubjectName !== "综合") {
                        xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.BZC + '%</Data></Cell>';
                    }
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">最高分</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    if(subitem.SubjectName !== "综合") {
                        xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.MaxSc + '</Data></Cell>';
                    }
                });
                xml += '</Row>';
                xml += '<Row><Cell ss:MergeAcross="2" ss:StyleID="s2"><Data ss:Type="String">最低分</Data></Cell>';
                _.each(item.meta.subTotal, function (subitem) {
                    if(subitem.SubjectName !== "综合") {
                        xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + subitem.MinSc + '</Data></Cell>';
                    }
                });
                xml += '</Row>';
            }
            xml += '</Table>';
            xml += '</Worksheet>';
        });
        xml += '</Workbook>';
        return xml;
    };

    function fillTable1(data) {
        var xml = '';
        if(data.subTotal.length > 1){
            xml = '<Column ss:AutoFitWidth="0" ss:Width="60"/><Column ss:AutoFitWidth="0" ss:Width="60"/>';
            xml += '<Row>';
            xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">学号</Data></Cell>';
            xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">姓名</Data></Cell>';
            xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">班级</Data></Cell>';
            _.each(data.subTotal, function (item) {
                if(item.SubjectName !== "综合"){
                    xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + item.SubjectName + '</Data></Cell>';
                }
            });
            xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">' + data.num + '门总分</Data></Cell>';
            xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">年级排名</Data></Cell>';
            xml += '<Cell ss:StyleID="s2"><Data ss:Type="String">班级排名</Data></Cell>';
            xml += '</Row>';
            _.each(data.studentList, function(item){
                xml += '<Row>';
                if(+item.EmpID && _.isNumber(+item.EmpID)) {
                    xml += '<Cell ss:Index="1" ss:StyleID="s1"><Data ss:Type="String">' + item.EmpID + '</Data></Cell>';
                }else {
                    xml += '<Cell ss:Index="1" ss:StyleID="s1"><Data ss:Type="String">' + item.EmpID + '</Data></Cell>';
                }
                xml += '<Cell ss:Index="2" ss:StyleID="s2"><Data ss:Type="String">' + item.UserName + '</Data></Cell>';
                xml += '<Cell ss:Index="3" ss:StyleID="s2"><Data ss:Type="Number">' + (+item.SmallNum) + '</Data></Cell>';
                var indexSub;
                _.each(data.subTotal, function (value, index) {
                    indexSub = 0;
                    _.each(item.subjects, function (subjectScore) {
                        if(subjectScore.subjectName === value.SubjectName){
                            indexSub = +subjectScore.score;
                        }
                    });
                    if(value.SubjectName !== "综合"){
                        xml += '<Cell ss:Index="'+ (index + 4) +'" ss:StyleID="s2"><Data ss:Type="Number">' + indexSub + '</Data></Cell>';
                    }
                });
                xml += '<Cell ss:Index="'+ (data.subTotal.length + 3) + '" ss:StyleID="s2"><Data ss:Type="Number">' + (+item.AllScores || 0)+ '</Data></Cell>';
                xml += '<Cell ss:Index="'+ (data.subTotal.length + 4) + '" ss:StyleID="s2"><Data ss:Type="Number">' + (+item.GOrder || 0)+ '</Data></Cell>';
                xml += '<Cell ss:Index="'+ (data.subTotal.length + 5) +'" ss:StyleID="s2"><Data ss:Type="Number">' + (+item.Corder || 0) + '</Data></Cell>';
                xml += '</Row>';
            });
        }
        typeGroup = [];
        return xml;
    }
})(jQuery, _);