﻿/**
 * Created by zxz on 2016-6-8.
 */
define(['outexcel', 'underscore-min', 'transData2XML', 'FileSaver'], function (outExcel) {
    return ['$rootScope', '$http', '$scope', '$state','$timeout','$location', '$q', 'resourceUrl',
        function ($rootScope, $http, $scope, $state, $timeout, $location, $q, resourceUrl) {
            var searchfrom = $location.search(),
                schoolId = searchfrom.schoolId,
                paperId = searchfrom.paperId,//空默认所有
                period = searchfrom.period,
                unifiedId = searchfrom.unifiedId,
                subjectNames = searchfrom.subjectNames,
                examName = decodeURIComponent(searchfrom.examName || '');
            var user = angular.fromJson(sessionStorage.getItem('currentUser'));

            //科目总数
            var subjectList = {
                0: '零',
                1: '一',
                2: '两',
                3: '三',
                4: '四',
                5: '五',
                6: '六',
                7: '七',
                8: '八',
                9: '九',
                10: '十',
                11: '十一',
                12: '十二',
                13: '十三'
            };
            $scope.examName = examName || '';
            //返回
            $scope.backView = function () {
                history.go(-1);
            };

            $scope.exportExcel = function(table, name){
                if(table !== "targetTb1") {
                    outExcel.exportTable(table, name);
                } else {
                    var deferList = [],deferListTwo = [];
                    _.each($scope.classList, function(item){
                        deferList.push($http.post($rootScope.baseUrl + '/Interface0263.ashx', {
                            examFlnkIds: paperId, //试卷ID(集合，不同学科试卷)
                            schoolFlnkId: schoolId,//学校id
                            period: period,//2015届
                            classflnkid: item.ClassID
                        }));
                        deferListTwo.push($http.post($rootScope.baseUrl + '/Interface0264.ashx', {
                            examFlnkIds: paperId, //试卷ID(集合，不同学科试卷)
                            schoolFlnkId: schoolId,//学校id
                            period: period,//2015届
                            classflnkid: item.ClassID
                        }));
                    });
                    $q.all(deferList).then(function(res){
                        $scope.stuListDetail = res;
                        $q.all(deferListTwo).then(function(resp){
                            $scope.subTotal= resp;
                            var metaData = [];
                            if(!_.find($scope.stuListDetail, function(item){
                                    return !!item;
                                })){
                                alert('获取数据失败！');
                                return;
                            }
                            if(!_.find($scope.subTotal, function(item){
                                    return !!item;
                                })){
                                alert('获取数据失败！');
                                return;
                            }
                            var len = $scope.stuListDetail.length > $scope.subTotal.length ? $scope.stuListDetail.length : $scope.subTotal.length;
                            _.each($scope.stuListDetail, function (item) {
                                if(!item.data.code) {
                                    _.each(item.data.msg, function (stuItem) {
                                        stuItem.subjects = [];
                                        if(+stuItem.ScoresA !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '语文',
                                                score: stuItem.ScoresA
                                            });
                                        }
                                        if(+stuItem.ScoresB !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '数学',
                                                score: stuItem.ScoresB
                                            });
                                        }
                                        if(+stuItem.ScoresC !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '英语',
                                                score: stuItem.ScoresC
                                            });
                                        }
                                        if(+stuItem.ScoresD !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '历史',
                                                score: stuItem.ScoresD
                                            });
                                        }
                                        if(+stuItem.ScoresE !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '地理',
                                                score: stuItem.ScoresE
                                            });
                                        }
                                        if(+stuItem.ScoresF !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '政治',
                                                score: stuItem.ScoresF
                                            });
                                        }
                                        if(+stuItem.ScoresG !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '生物',
                                                score: stuItem.ScoresG
                                            });
                                        }
                                        if(+stuItem.ScoresH !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '物理',
                                                score: stuItem.ScoresH
                                            });
                                        }
                                        if(+stuItem.ScoresI !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '化学',
                                                score: stuItem.ScoresI
                                            });
                                        }
                                        if(+stuItem.ScoresJ !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '信息技术',
                                                score: stuItem.ScoresJ
                                            });
                                        }
                                        if(+stuItem.ScoresK !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '科学',
                                                score: stuItem.ScoresK
                                            });
                                        }
                                        if(+stuItem.ScoresL !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '思社',
                                                score: stuItem.ScoresL
                                            });
                                        }
                                        if(+stuItem.ScoresM !== -1) {
                                            stuItem.subjects.push({
                                                subjectName: '英语复习',
                                                score: stuItem.ScoresM
                                            });
                                        }
                                    });
                                }
                            });
                            for(var i = 0; i < len; i++) {
                                var className = $scope.classList[i].ClassName;
                                metaData.push({
                                    sheetName: className,
                                    meta: {
                                        studentList: $scope.stuListDetail[i].data.msg,
                                        subTotal: $scope.subTotal[i].data.msg,
                                        num: subjectList[$scope.subTotal[i].data.msg.length - 1]
                                    },
                                    classId: $scope.classList[i].ClassID
                                });
                            }
                            var xml = $.transData2Xml4Excel(metaData, 1);
                            var blob = new Blob([xml], {
                                type: "text/plain;charset=utf-8"
                            });
                            saveAs(blob, name + ".xls");
                        });
                    });
                }
            };

            //底部切换
            $scope.isChoseTab = "";
            $scope.noclass = false;
            $scope.tabTb1 = function () {
                $scope.noclassData = false;
                if ($scope.isChoseTab == "tab1") {
                    return;
                }
                $scope.isChoseTab = "tab1";
                var classList = []; //通过gradeId获取班级列表
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0192.ashx',
                    data: {
                        schoolFId: schoolId,
                        period: period
                    }
                }).success(function (data) {
                    if (!!data && _.isArray(data.msg)) {
                        for (var i = 0; i < data.msg.length; i++) { // 存储班级所有ID
                            classList.push(
                                {
                                    ClassID: data.msg[i].ClassID,
                                    ClassName: data.msg[i].ClassName
                                })
                        }
                        /*初始化数据*/
                        $scope.cName = classList[0].ClassName;
                        getSubject4Tab1(classList[0].ClassID);
                        getClassData(classList[0].ClassID);
                        $scope.classList = classList;//班级列表
                        /*   $scope.classList =  _.sortBy(classList,function (item) {
                         return Number(item.ClassName.slice(2));
                         });*/
                    } else {//无班级数据
                        $scope.noclass = true;
                    }
                })
            };

            /*切换班级*/
            $scope.Switching = function (clas) {
                $scope.cName = clas.ClassName;//选中样式
                getSubject4Tab1(clas.ClassID);
                getClassData(clas.ClassID);//切换班级数据
            };

            //区分编外学生
            function OrganizeStus(array) {
                return _.map(array, function (item, index) {
                    return {
                        EmpID: item.EmpID,
                        UserName: item.UserName,
                        SmallNum: item.SmallNum,
                        ScoresA: !isThisSubject('A') ? undefined : item.ScoresA,
                        ScoresB: !isThisSubject('B') ? undefined : item.ScoresB,
                        ScoresC: !isThisSubject('C') ? undefined : item.ScoresC,
                        ScoresD: !isThisSubject('D') ? undefined : item.ScoresD,
                        ScoresE: !isThisSubject('E') ? undefined : item.ScoresE,
                        ScoresF: !isThisSubject('F') ? undefined : item.ScoresF,
                        ScoresG: !isThisSubject('G') ? undefined : item.ScoresG,
                        ScoresH: !isThisSubject('H') ? undefined : item.ScoresH,
                        ScoresI: !isThisSubject('I') ? undefined : item.ScoresI,
                        ScoresJ: !isThisSubject('J') ? undefined : item.ScoresJ,
                        ScoresK: !isThisSubject('K') ? undefined : item.ScoresK,
                        ScoresL: !isThisSubject('L') ? undefined : item.ScoresL,
                        ScoresM: !isThisSubject('M') ? undefined : item.ScoresM,
                        AllScores: item.AllScores,
                        GOrder: item.GOrder,
                        Corder: item.Corder
                    }
                })
            }

            /*传入ClassID加载各班级数据*/
            $scope.noclassData = false;
            function getClassData(cId) {//表1-1.0
                $scope.noclassData = false;
                $http({
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0263.ashx',
                    data: {
                        examFlnkIds: paperId, //试卷ID(集合，不同学科试卷)
                        schoolFlnkId: schoolId,//学校id
                        period: period,//2015届
                        classflnkid: cId
                    }
                }).success(function (res) {
                    if (!!res && _.isArray(res.msg)) {
                        $scope.tkList_Fir = res.msg;
                        $scope.OrganizeStu = _.groupBy(res.msg, 'OrganizeNum');
                        if ($scope.OrganizeStu[1]) {
                            $scope.tkList1 = OrganizeStus($scope.OrganizeStu[1]);//编外学生
                        }
                        $scope.tkList = OrganizeStus($scope.OrganizeStu[0]);
                        $scope.key1s = retKeys($scope.tkList[0]);
                    } else {
                        console.log('无表1-1.0数据！');
                        $scope.noclassData = true;
                    }
                }).then(function () {
                    $http({//表1-2
                        method: 'post',
                        url: $rootScope.baseUrl + '/Interface0264.ashx',
                        data: {
                            examFlnkIds: paperId,
                            schoolFlnkId: schoolId,//学校id
                            period: period,//2015届
                            classflnkid: cId
                        }
                    }).success(function (res) {
                        if (!!res && _.isArray(res.msg)) {
                            $scope.tb2List = res.msg;
                            $scope.tb2List_AvgSc = [];
                            $scope.tb2List_BZC = [];
                            $scope.tb2List_GAvgSc = [];
                            $scope.tb2List_GoodRate = [];
                            $scope.tb2List_MinSc = [];
                            $scope.tb2List_PassRate = [];
                            $scope.tb2List_SubjectName = [];
                            $scope.tb2List_MaxSc = [];
                            _.each($scope.tb2List, function (item, index) {
                                $scope.tb2List_AvgSc.push(item.AvgSc);
                                $scope.tb2List_BZC.push(item.BZC);
                                $scope.tb2List_GAvgSc.push(item.GAvgSc);
                                $scope.tb2List_GoodRate.push(item.GoodRate);
                                $scope.tb2List_MinSc.push(item.MinSc);
                                $scope.tb2List_SubjectName.push(item.SubjectName);
                                $scope.tb2List_PassRate.push(item.PassRate);
                                $scope.tb2List_MaxSc.push(item.MaxSc);
                            });
                        } else {
                            console.log('无表1-2数据！')
                        }
                    })
                })
            }

            //显示tqb2
            $scope.tabTb2 = function () {
                $scope.noclassData = false;
                if ($scope.isChoseTab == "tab2") {
                    return;
                }
                $scope.isChoseTab = "tab2";
                var defer = $q.defer();
                var avgPercent = [40, -30];
                $http({//表2
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0261.ashx',
                    data: {
                        examFlnkIds: paperId,
                        schoolFlnkId: schoolId,
                        period: period
                    }
                }).success(function (res) {
                    if (!!res && _.isArray(res.msg)) {
                        $scope.tb261 = res.msg;
                        var gradeAvgTotal = 0;
                        _.each($scope.tb261, function(item){
                            gradeAvgTotal += +item.avgValue
                        });
                        $scope.tb261.push({
                            avgValue: (gradeAvgTotal / $scope.tb261.length).toFixed(2),
                            examNums: "",
                            fLnkID: "",
                            smallNum: "0",
                            userName: ""
                        });
                        $http({
                            method: 'post',
                            url: $rootScope.baseUrl + '/Interface0262.ashx',
                            data: {
                                examFlnkIds: paperId,
                                schoolFlnkId: schoolId,
                                period: period
                            }
                        }).success(function (res) {
                            if (!!res && _.isArray(res.msg)) {
                                var deferArr = [];
                                _.each(avgPercent, function(item){
                                    deferArr.push($http.post('/BootStrap/Report/getExamsAvg.ashx', {
                                        examFlnkIds: paperId,
                                        period: period,
                                        schoolFlnkId: schoolId,
                                        part: item
                                    }));
                                });
                                $q.all(deferArr).then(function(){
                                    _.each(arguments[0], function(respItem, index){
                                        _.each(res.msg, function(item){
                                            var temp = _.find(respItem.data.msg, function(tempItem){
                                                return +item.SmallNum === +tempItem.SmallNum && item.SubjectName === tempItem.subjectname;
                                            });
                                            item['avgsc_' + (index+1)] = (temp && temp.avgsc || 0).toFixed(1);
                                        });
                                    });
                                    $scope.tb262 = res.msg;
                                    var subjectGroup = _.groupBy($scope.tb262, function (item) {
                                        return item.SubjectName;
                                    });
                                    for (var key in subjectGroup) {
                                        var temp = subjectGroup[key];
                                        var uniqArray = _.uniq(temp, function (item) {
                                            return item.SmallNum;
                                        });
                                        uniqArray.sort(function (a, b) {
                                            return +a - +b > 0;
                                        });
                                        subjectGroup[key] = uniqArray;
                                    }

                                    $scope.subjectMap = subjectGroup;
                                    var keys = _.keys(subjectGroup);
                                    var vals = _.values(subjectGroup);
                                    _.each(vals, function(sg){
                                        var sbjAvgTotal = 0, avg1Total = 0, avg2Total = 0;
                                        _.each(sg, function(item){
                                            sbjAvgTotal += +item.AvgScores;
                                            avg1Total += +item.avgsc_1;
                                            avg2Total += +item.avgsc_2
                                        });
                                        sbjAvgTotal = (sbjAvgTotal / sg.length).toFixed(2);
                                        avg1Total = (avg1Total / sg.length).toFixed(2);
                                        avg2Total = (avg2Total / sg.length).toFixed(2);
                                        sg.push({
                                            SmallNum: "0",
                                            SubjectName: sg[0].SubjectName,
                                            UserName: "",
                                            avgsc_1: avg1Total,
                                            avgsc_2: avg2Total,
                                            AvgScores: sbjAvgTotal
                                        });
                                    });
                                    $scope.subjects = keys;
                                    $scope.subjectsNum = subjectList[keys.length] + '门总分';
                                    $scope.subjectsTemp = keys;
                                    var curTime = new Date();
                                    $scope.curTime = curTime.getFullYear() + '年' + (curTime.getMonth() + 1) + '月';
                                    var $row2 = $('#row2');
                                    var $row3 = $('#row3');
                                    var $table = $('#targetTb2 table#origin');
                                    for (var i = 0; i < $scope.subjectsTemp.length; i++) {
                                        var $tdTeacher = $('<td rowspan="1" style="text-align: center">任课老师</td>'),
                                            $tdTime = $('<td colspan="3" style="text-align: center"></td>');
                                        $tdTime.text($scope.curTime);
                                        $tdTeacher.appendTo($row2);
                                        // $tdTime.appendTo($row2);
                                        var $avg = $('<td style="text-align: center">均分</td>'),
                                            $good = $('<td style="text-align: center">前40%均分</td>'),
                                            $pass = $('<td style="text-align: center">后30%均分</td>');
                                        $avg.appendTo($row2);
                                        $good.appendTo($row2);
                                        $pass.appendTo($row2);
                                    }
                                    var avgs = _.map($scope.tb261, function(item){
                                        return {
                                            smallNum: item.smallNum,
                                            avgValue: item.avgValue
                                        };
                                    });
                                    avgs = _.filter(avgs, function(item){
                                        return item.smallNum !== '0';
                                    });
                                    avgs.sort(function(a, b){
                                        return b.avgValue - a.avgValue
                                    });
                                    _.each(avgs, function(item, index){
                                        var meta = _.find($scope.tb261, function(data){
                                            return data.smallNum === item.smallNum;
                                        });
                                        meta.avgOrder = index + 1;
                                    });
                                    for (var j = 0; j < $scope.tb261.length; j++) {
                                        var item = $scope.tb261[j];
                                        var $tr = $('<tr style="text-align: center">');
                                        var $tdNum = $('<td style="text-align: center"></td>').text($scope.classSmallNumNameMap[item.smallNum]),
                                            $tdTeacherName = $('<td style="text-align: center">').text(item.userName),
                                            $totalScore = $('<td style="text-align: center">').text(item.avgValue);
                                        $tdNum.appendTo($tr);
                                        $tdTeacherName.appendTo($tr);
                                        $totalScore.appendTo($tr);
                                        $('<td style="text-align: center">').text(item.avgOrder).appendTo($tr);
                                        var currentVals = _.filter(vals, function (subjectVal) {
                                            return subjectVal.SmallNum === item.smallNum;
                                        });
                                        _.each(vals, function (val) {
                                            var currentClass = _.find(val, function (s) {
                                                return s.SmallNum === item.smallNum;
                                            });
                                            $('<td style="text-align: center">').text(currentClass && currentClass.UserName || '').appendTo($tr);
                                            $('<td style="text-align: center">').text(currentClass && currentClass.AvgScores || 0).appendTo($tr);
                                            $('<td style="text-align: center">').text((currentClass && currentClass.avgsc_1 || 0)).appendTo($tr);
                                            $('<td style="text-align: center">').text((currentClass && currentClass.avgsc_2 || 0)).appendTo($tr);
                                        });
                                        $tr.appendTo($table);
                                    }
                                });
                            } else {
                                console.log('无表2.1数据！')
                            }
                        })
                    } else {
                        console.log('无表2.0数据！')
                    }
                });

                if(user.schoolInfo && user.schoolInfo.RelationSchool) {
                    $scope.haveRelatedSchool = true;
                    $http({//表2
                        method: 'post',
                        url: $rootScope.baseUrl + '/Interface0261.ashx',
                        data: {
                            examFlnkIds: paperId,
                            schoolFlnkId: user.schoolInfo.RelationSchool,
                            period: period
                        }
                    }).success(function (res) {
                        if (!!res && _.isArray(res.msg)) {
                            var tb261 = res.msg;
                            var gradeAvgTotal = 0;
                            _.each(tb261, function(item){
                                gradeAvgTotal += +item.avgValue
                            });
                            tb261.push({
                                avgValue: (gradeAvgTotal / tb261.length).toFixed(2),
                                examNums: "",
                                fLnkID: "",
                                smallNum: "0",
                                userName: ""
                            });
                            $http({
                                method: 'post',
                                url: $rootScope.baseUrl + '/Interface0262.ashx',
                                data: {
                                    examFlnkIds: paperId,
                                    schoolFlnkId: user.schoolInfo.RelationSchool,
                                    period: period
                                }
                            }).success(function (res) {
                                if (!!res && _.isArray(res.msg)) {
                                    var deferArr = [];
                                    _.each(avgPercent, function(item){
                                        deferArr.push($http.post('/BootStrap/Report/getExamsAvg.ashx', {
                                            examFlnkIds: paperId,
                                            period: period,
                                            schoolFlnkId: user.schoolInfo.RelationSchool,
                                            part: item
                                        }));
                                    });
                                    $q.all(deferArr).then(function() {
                                        _.each(arguments[0], function (respItem, index) {
                                            _.each(res.msg, function (item) {
                                                var temp = _.find(respItem.data.msg, function (tempItem) {
                                                    return +item.SmallNum === +tempItem.SmallNum && item.SubjectName === tempItem.subjectname;
                                                });
                                                item['avgsc_' + (index + 1)] = (temp && temp.avgsc || 0).toFixed(1);
                                            });
                                        });
                                        var data = res.msg;
                                        var subjectGroup = _.groupBy(data, function (item) {
                                            return item.SubjectName;
                                        });
                                        for (var key in subjectGroup) {
                                            var temp = subjectGroup[key];
                                            var uniqArray = _.uniq(temp, function (item) {
                                                return item.SmallNum;
                                            });
                                            uniqArray.sort(function (a, b) {
                                                return +a - +b > 0;
                                            });
                                            subjectGroup[key] = uniqArray;
                                        }
                                        var keys = _.keys(subjectGroup);
                                        var vals = _.values(subjectGroup);
                                        _.each(vals, function(sg){
                                            var sbjAvgTotal = 0, avg1Total = 0, avg2Total = 0;
                                            _.each(sg, function(item){
                                                sbjAvgTotal += +item.AvgScores;
                                                avg1Total += +item.avgsc_1;
                                                avg2Total += +item.avgsc_2
                                            });
                                            sbjAvgTotal = (sbjAvgTotal / sg.length).toFixed(2);
                                            avg1Total = (avg1Total / sg.length).toFixed(2);
                                            avg2Total = (avg2Total / sg.length).toFixed(2);
                                            sg.push({
                                                SmallNum: "0",
                                                SubjectName: sg[0].SubjectName,
                                                UserName: "",
                                                avgsc_1: avg1Total,
                                                avgsc_2: avg2Total,
                                                AvgScores: sbjAvgTotal
                                            });
                                        });
                                        $scope.subjectsRelated = keys;
                                        $scope.subjectsNumRelated = subjectList[keys.length] + '门总分';
                                        var $row2 = $('#related-row2');
                                        var $table = $('#targetTb2 table#related');
                                        for (var i = 0; i < keys.length; i++) {
                                            var $tdTeacher = $('<td rowspan="1" style="text-align: center">任课老师</td>');
                                            $tdTeacher.appendTo($row2);
                                            var $avg = $('<td style="text-align: center">均分</td>'),
                                                $good = $('<td style="text-align: center">前40%均分</td>'),
                                                $pass = $('<td style="text-align: center">后30%均分</td>');
                                            $avg.appendTo($row2);
                                            $good.appendTo($row2);
                                            $pass.appendTo($row2);
                                        }
                                        var avgs = _.map(tb261, function(item){
                                            return {
                                                smallNum: item.smallNum,
                                                avgValue: item.avgValue
                                            };
                                        });
                                        avgs = _.filter(avgs, function(item){
                                            return item.smallNum !== '0';
                                        });
                                        avgs.sort(function(a, b){
                                            return b.avgValue - a.avgValue
                                        });
                                        _.each(avgs, function(item, index){
                                            var meta = _.find(tb261, function(data){
                                                return data.smallNum === item.smallNum;
                                            });
                                            meta.avgOrder = index + 1;
                                        });
                                        for (var j = 0; j < tb261.length; j++) {
                                            var item = tb261[j];
                                            var $tr = $('<tr style="text-align: center">');
                                            var $tdNum = $('<td style="text-align: center"></td>').text($scope.relatedClassSmallNumNameMap[item.smallNum]),
                                                $tdTeacherName = $('<td style="text-align: center">').text(item.userName),
                                                $totalScore = $('<td style="text-align: center">').text(item.avgValue);
                                            $tdNum.appendTo($tr);
                                            $tdTeacherName.appendTo($tr);
                                            $totalScore.appendTo($tr);
                                            $('<td style="text-align: center">').text(item.avgOrder).appendTo($tr);
                                            var currentVals = _.filter(vals, function (subjectVal) {
                                                return subjectVal.SmallNum === item.smallNum;
                                            });
                                            _.each(vals, function (val) {
                                                var currentClass = _.find(val, function (s) {
                                                    return s.SmallNum === item.smallNum;
                                                });
                                                $('<td style="text-align: center">').text(currentClass && currentClass.UserName || '').appendTo($tr);
                                                $('<td style="text-align: center">').text(currentClass && currentClass.AvgScores || 0).appendTo($tr);
                                                $('<td style="text-align: center">').text((currentClass && currentClass.avgsc_1 || 0)).appendTo($tr);
                                                $('<td style="text-align: center">').text((currentClass && currentClass.avgsc_2 || 0)).appendTo($tr);
                                            });
                                            $tr.appendTo($table);
                                        }
                                    });
                                } else {
                                    console.log('无表2.1数据！')
                                }
                            })
                        } else {
                            console.log('无表2.0数据！')
                        }
                    })
                }
            };

            $scope.redrawReportWithHalf = function(){
                var selected = _.filter($scope.subjects4Tab3, function(item){
                    return !!item.isHalfScore;
                });
                $scope.tabTb3(_.pluck(selected, 'SubjectName'));
            };

            //显示tqb3
            $scope.tabTb3 = function (subjects) {
                $scope.noclassData = false;
                if ($scope.isChoseTab == "tab3" && !subjects) {
                    return;
                }
                $scope.isChoseTab = "tab3";
                $http({//表3所有班级
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0260.ashx',
                    data: {
                        //examFlnkIds: paperId,//试卷ID(集合，不同学科试卷)
                        examFlnkIds: paperId,
                        schoolFlnkId: schoolId,//学校id
                        period: period,//2015届
                        //classflnkid: "737bd99e-e94b-4a86-a5e8-260d32e2b91d"
                        subjectName: subjects && subjects.join(',')
                    }
                }).success(function (res) {
                    if (!!res && _.isArray(res.msg)) {
                        $scope.tkList_Fir3 = res.msg;
                        $scope.issearch = true;
                        $scope.tkList3 = _.map(res.msg, function (item, index) {
                            return {
                                EmpID: item.EmpID,
                                UserName: item.UserName,
                                SmallNum: item.SmallNum,
                                ScoresA: !isThisSubject('A') ? undefined : item.ScoresA,
                                ScoresB: !isThisSubject('B') ? undefined : item.ScoresB,
                                ScoresC: !isThisSubject('C') ? undefined : item.ScoresC,
                                ScoresD: !isThisSubject('D') ? undefined : item.ScoresD,
                                ScoresE: !isThisSubject('E') ? undefined : item.ScoresE,
                                ScoresF: !isThisSubject('F') ? undefined : item.ScoresF,
                                ScoresG: !isThisSubject('G') ? undefined : item.ScoresG,
                                ScoresH: !isThisSubject('H') ? undefined : item.ScoresH,
                                ScoresI: !isThisSubject('I') ? undefined : item.ScoresI,
                                ScoresJ: !isThisSubject('J') ? undefined : item.ScoresJ,
                                ScoresK: !isThisSubject('K') ? undefined : item.ScoresK,
                                ScoresL: !isThisSubject('L') ? undefined : item.ScoresL,
                                ScoresM: !isThisSubject('M') ? undefined : item.ScoresM,
                                AllScores: item.AllScores,
                                GOrder: item.GOrder
                            }
                        });
                        $scope.tkList3.sort(function(a, b){
                            return +a.GOrder - +b.GOrder;
                        });
                        $scope.keys = retKeys($scope.tkList3[0]);
                    } else {
                        console.log('无表3数据！')
                    }
                }).then(function () {
                    $http({//表3-20
                        method: 'post',
                        url: $rootScope.baseUrl + '/Interface0265.ashx',
                        data: {
                            examFlnkIds: paperId,
                            //examFlnkIds: "8a872f80-f2fd-44fe-8964-90373729e03b",//试卷ID们
                            schoolFlnkId: schoolId,//学校id
                            period: period,//2015届
                            subjectName: subjects && subjects.join(',')
                        }
                    }).success(function (res) {
                        if (!!res && _.isArray(res.msg)) {
                            $scope.tb3_2List = res.msg;
                            if(!$scope.subjects4Tab3) {
                                $scope.subjects4Tab3 = _.filter(res.msg, function(item){
                                    return item.SubjectName !== '综合';
                                });
                            }
                            $scope.tb3List_AvgSc = [];
                            $scope.tb3List_BZC = [];
                            $scope.tb3List_QFD = [];
                            $scope.tb3List_NDXS = [];
                            $scope.tb3List_GoodRate = [];
                            $scope.tb3List_MinSc = [];
                            $scope.tb3List_PassRate = [];
                            $scope.tb3List_SubjectName = [];
                            $scope.tb3List_MaxSc = [];
                            _.each($scope.tb3_2List, function (item, index) {
                                $scope.tb3List_AvgSc.push(item.AvgSc);
                                $scope.tb3List_BZC.push(item.BZC);
                                $scope.tb3List_NDXS.push(item.NDXS);
                                $scope.tb3List_QFD.push(item.QFD);
                                $scope.tb3List_GoodRate.push(item.GoodRate);
                                $scope.tb3List_MinSc.push(item.MinSc);
                                $scope.tb3List_SubjectName.push(item.SubjectName);
                                $scope.tb3List_PassRate.push(item.PassRate);
                                $scope.tb3List_MaxSc.push(item.MaxSc);
                            });
                        } else {
                            console.log('无表3-2数据！')
                        }
                    })
                });
            };

            //显示tqb4
            $scope.checkRateList = [{
                val: 5
            },{
                val: 20
            },{
                val: 40
            },{
                val: -30
            }];
            $scope.tabTb4 = function (force) {
                var requestIndex= 0;
                $scope.noclassData = false;
                if ($scope.isChoseTab === "tab4" && !force) {
                    return;
                }
                var isInvalid = _.find($scope.checkRateList, function(item){
                    return isNaN(item.val);
                });
                if(isInvalid) {
                    return;
                }
                $('#targetTb4').empty();
                $scope.isChoseTab = "tab4";
                analysisLayer(requestIndex);
            };

            $scope.scoreRange = 10;
            $scope.tabTb5 = function() {
                $scope.noclassData = false;
                if ($scope.isChoseTab === "tab5") {
                    return;
                }
                $scope.isChoseTab = "tab5";
                // analysisLayer(10);
                getScoreRangeReport();
            };

            $scope.setScoreRange = function(){
                getScoreRangeReport();
            };

            function getScoreRangeReport (){
                $http.post('/BootStrap/Report/getExamsScSubsection.ashx', {
                    examFlnkIds: paperId,
                    period: period,
                    schoolFlnkId: schoolId,
                    step: $scope.scoreRange
                }).then(function(res){
                    $scope.classCounts = res.data.msg.result1;
                    _.each($scope.classCounts, function(item){
                        if(!item.SmallNum) {
                            item.SmallNum = 0;
                        }
                    });
                    $scope.classCounts.sort(function(a, b){
                        return a.SmallNum - b.SmallNum;
                    });
                    var rangeData = res.data.msg.result2;
                    var groupedByClass = _.groupBy(rangeData, 'g');
                    var list = [];
                    for(var key in groupedByClass) {
                        var value = groupedByClass[key];
                        _.each(value, function(item){
                            if(!item.SmallNum) {
                                item.SmallNum = 0;
                            }
                        });
                        value.sort(function(a, b){
                            return a.SmallNum - b.SmallNum;
                        });
                        list.push({
                            rangeStart: key,
                            data: value
                        });
                    }
                    list.sort(function(a, b){
                        return a.rangeStart - b.rangeStart;
                    });
                    _.each(list, function(item, index){
                        item.showRange = item.rangeStart + '--' + ( +item.rangeStart + +$scope.scoreRange - (index === list.length - 1 ? 0 : 1))
                    });
                    $scope.rangeReportData = list.reverse();
                    var totalScore = +$scope.rangeReportData[0].rangeStart + $scope.scoreRange;
                    $scope.topRangeReportData = _.map($scope.rangeReportData, function(item, index){
                        var showRange = item.rangeStart + '--' + totalScore;
                        var data = _.map(item.data, function(d, sindex){
                            var gnum = 0;
                            for(var i = 0; i <= index; i++) {
                                gnum += $scope.rangeReportData[i].data[sindex].gnum;
                            }
                            return {
                                g: d.g,
                                SmallNum: d.SmallNum,
                                gnum: gnum,
                                gradenum: d.gradenum,
                                ClassName: d.ClassName
                            };
                        });
                        return {
                            rangeStart: item.rangeStart,
                            showRange: showRange,
                            data: data
                        }
                    });
                });
            }

            // 显示具体报表
            function analysisLayer(index) {
                var tableIndex = $scope.checkRateList[index].val;
                $http({
                    method: 'post',
                    url: '/BootStrap/Report/getCLassScsReport.ashx',
                    data: {
                        examFlnkIds: paperId,
                        schoolFlnkId: schoolId,
                        period: period,
                        unifidId: unifiedId,
                        reportType: tableIndex,
                        goodScore: 0
                    }
                }).success(function (res) {
                    if (!!res && _.isArray(res.msg)) {
                        $scope.subjectDeList = res.msg;
                        var f = [];
                        var tableTitle = '';
                        if(tableIndex > 0) {
                            tableTitle = examName + '年级总分排名前'+tableIndex+'%分析';
                        }else {
                            tableTitle = examName + '年级总分排名后' + Math.abs(tableIndex) + '%分析';
                        }
                        var tableExample = $('<table id="table' + tableIndex + '" border="1" width="100%">' +
                            '<caption style="text-align: center;">'+tableTitle+
                            '<div>（年级总分排名'+ (tableIndex > 0 ? '前': '后') + Math.abs(tableIndex) + '%的学生各科平均分和优秀率、及格率情况。比如：年级'+
                            (tableIndex > 0 ? '前': '后') + Math.abs(tableIndex)+'%有60人，这60人里一班有7人，这7人的各科情况）</div>' +
                            '</caption>' +
                            '<tbody>' +
                            '<tr id="' + tableIndex + 'tab4Row2"><th rowspan="2">班级 </th><th colspan="3">总分</th></tr>' +
                            '<tr id="' + tableIndex + 'tab4Row3"></tr></tbody></table>');
                        tableExample.appendTo($('#targetTb4'));
                        _.each($scope.subjectDeList, function (item) {
                            _.each(item.subjectsDetail, function (sub) {
                                f.push(sub)
                            });
                        });
                        var subjectGroup = _.groupBy(f, function (item) {
                            return item.subjectname;
                        });
                        for (var key in subjectGroup) {
                            var temp = subjectGroup[key];
                            var uniqArray = _.uniq(temp, function (item) {
                                return item.SmallNum;
                            });
                            uniqArray.sort(function (a, b) {
                                return +a - +b > 0;
                            });
                            subjectGroup[key] = uniqArray;
                        }
                        $scope.subjectMap = subjectGroup;
                        var orderSbjs = [];
                        for(var k in subjectGroup) {
                            orderSbjs.push({
                                subjectid: subjectGroup[k][0].subjectid,
                                subjectname: subjectGroup[k][0].subjectname
                            });
                        }
                        orderSbjs.sort(function(a, b){
                            return a.subjectid - b.subjectid;
                        });
                        $scope.subjects = _.pluck(orderSbjs, 'subjectname');
                        var $tab4Row3 = $('#' + tableIndex + 'tab4Row3'), $tab4Row2 = $('#' + tableIndex + 'tab4Row2'), $table = $('#table' + tableIndex);
                        $tab4Row3.empty();
                        var $totalAvg = $('<td style="text-align: center">人数</td><td style="text-align: center">平均分</td><td style="text-align: center">排名</td>');
                        $totalAvg.appendTo($tab4Row3);
                        for (var i = 0, len = $scope.subjects.length; i < len; i++) {
                            var $subTitle = $('<th colspan="6">' + $scope.subjects [i] + '</th>'),
                                $avg = $('<td style="text-align: center">平均分</td>'),
                                $avgOrder = $('<td style="text-align: center">平均分排名</td>'),
                                $good = $('<td style="text-align: center">优秀率</td>'),
                                $goodOrder = $('<td style="text-align: center">优秀率排名</td>'),
                                $pass = $('<td style="text-align: center">及格率</td>'),
                                $passOrder = $('<td style="text-align: center">及格率排名</td>');
                            $subTitle.appendTo($tab4Row2);
                            $avg.appendTo($tab4Row3);
                            $avgOrder.appendTo($tab4Row3);
                            $good.appendTo($tab4Row3);
                            $goodOrder.appendTo($tab4Row3);
                            $pass.appendTo($tab4Row3);
                            $passOrder.appendTo($tab4Row3);
                        }
                        for (var j = 0; j < $scope.subjectDeList.length; j++) {
                            var item = $scope.subjectDeList[j];
                            var $tr = $('<tr style="text-align: center">');
                            var $tdNum = $('<td style="text-align: center"></td>').text(item.studentCount),
                                $tdTeacherName = $('<td style="text-align: center">').text(item.ClassName),
                                $totalScore = $('<td style="text-align: center">').text(item.avgScore ? item.avgScore.toFixed(1) : '0.0'),
                                $totalOrder = $('<td style="text-align: center">').text(item.avgOrder);
                            $tdTeacherName.appendTo($tr);
                            $tdNum.appendTo($tr);
                            $totalScore.appendTo($tr);
                            $totalOrder.appendTo($tr);
                            var currentClass;
                            for(var m = 0, len = $scope.subjects.length; m < len; m ++) {
                                _.each(item.subjectsDetail, function (val) {
                                    if($scope.subjects[m] === val.subjectname) {
                                        currentClass = val;
                                    }
                                });
                                if(!currentClass || $scope.subjects[m] !== currentClass.subjectname) {
                                    currentClass = {
                                        avgScore: 0,
                                        avgOrder: 0,
                                        goodRate: 0,
                                        goodOrder: 0,
                                        passRate: 0,
                                        passOrder: 0
                                    };
                                }
                                $('<td style="text-align: center">').text(currentClass && (currentClass.avgScore ? currentClass.avgScore.toFixed(1): '0.0')).appendTo($tr);
                                $('<td style="text-align: center">').text(currentClass.avgOrder).appendTo($tr);
                                $('<td style="text-align: center">').text((currentClass && (currentClass.goodRate ? currentClass.goodRate.toFixed(1) : '0.0')) + '%').appendTo($tr);
                                $('<td style="text-align: center">').text(currentClass.goodOrder).appendTo($tr);
                                $('<td style="text-align: center">').text((currentClass && (currentClass.passRate ? currentClass.passRate.toFixed(1) : '0.0')) + '%').appendTo($tr);
                                $('<td style="text-align: center">').text(currentClass.passOrder).appendTo($tr);
                            }
                            $tr.appendTo($table);
                        }
                        $('<tr style="text-align: center">').appendTo($table);
                    } else {
                        console.log('无表4数据！')
                    }
                    if(index < $scope.checkRateList.length - 1) {
                        analysisLayer(index + 1);
                    }
                })
            }

            $scope.tabTb6 = function(){
                $scope.noclassData = false;
                if ($scope.isChoseTab === "tab6") {
                    return;
                }
                $scope.isChoseTab = "tab6";
                var $category = $('#targetTb6').find('#categories').empty(),
                    $tbody = $('#targetTb6').find('tbody').empty();
                $http({//表1-2
                    method: 'post',
                    url: '/BootStrap/Report/getSingleSubSvg.ashx',
                    data: {
                        examFlnkIds: paperId,
                        schoolFlnkId: schoolId,//学校id
                        period: period,//2015届
                        subs: '10,15,20,25'
                    }
                }).then(function(res){
                    var data = res.data.msg;
                    if(_.isArray(data) && data.length > 0) {
                        var categoryList = data[0].category;
                        var classList = _.pluck(categoryList[0].classAvgList, 'className');
                        _.each(data, function(item){
                            _.each(item.category, function(c){
                                var $th = $('<th>').text(c.categoryName);
                                $category.append($th);
                            });
                        });
                        _.each(classList, function(item){
                            var $tr = $('<tr>');
                            var $classNameTd = $('<td>').text(item).appendTo($tr);
                            _.each(data, function(dataTmp){
                                _.each(dataTmp.category, function(category){
                                    var avg = _.find(category.classAvgList, function(avg){
                                        return avg.className === item;
                                    });
                                    $('<td>').text((+avg.avg|| 0).toFixed(2)).appendTo($tr);
                                });
                            });
                            $tr.appendTo($tbody);
                        });
                        $scope.firstNSubjects = data;
                    }else {
                        $scope.firstNSubjects = [];
                    }
                });
            };

            $scope.tabTb7 = function(){
                $scope.noclassData = false;
                if ($scope.isChoseTab === "tab7") {
                    return;
                }
                $scope.isChoseTab = "tab7";
                $http({//表1-2
                    method: 'post',
                    url: '/BootStrap/Report/getPreSubNums.ashx',
                    data: {
                        examFlnkIds: paperId,
                        schoolFlnkId: schoolId,//学校id
                        period: period,//2015届
                        subs: '5,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200'
                    }
                }).then(function(res){
                    var data = res.data.msg;
                    if(_.isArray(data) && data.length > 0) {
                        var group = _.groupBy(data, 'SmallNum');
                        var grouppedData = [];
                        var cols;
                        for(var k in group) {
                            if(!cols) {
                                cols = _.map(group[k], function(item){
                                    return +item.col;
                                });
                                cols.sort(function(a, b){
                                    return a - b;
                                });
                            }
                            var countData = group[k];
                            countData.sort(function(a, b){
                                return +a.col - +b.col;
                            });
                            grouppedData.push({
                                SmallNum: k,
                                data: countData
                            });
                        }
                        $scope.firstNRangeList = cols;
                        $scope.firstNClassGroup = grouppedData;
                    }else {
                        $scope.firstNRangeList = [];
                        $scope.firstNClassGroup = [];
                    }
                });
            };
            //判断所选科目试卷
            function isThisSubject(name) {
                if($scope.isChoseTab == "tab3"){
                    return subjectNames.indexOf(name) != -1;
                }
                if($scope.isChoseTab == "tab1" && $scope.subNames4Tab1){
                    return $scope.subNames4Tab1.indexOf(name) != -1;
                }
            }
            function retKeys(obj) {
                var keys = [],subnum = 0;
                for (var key in obj) {
                    if (obj[key]) {
                        if (key == 'EmpID') {
                            key = '学号';
                        }
                        if (key == 'UserName') {
                            key = '姓名';
                        }
                        if (key == 'SmallNum') {
                            key = '班级';
                        }
                        if (key == 'ScoresA') {
                            if(isThisSubject('A')){
                                subnum += 1;
                                key = '语文';
                            }else{
                                key = -2;
                            }
                        }

                        if (key == 'ScoresB' ) {
                            if(isThisSubject('B')){
                                subnum += 1;
                                key = '数学';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresC' ) {
                            if(isThisSubject('C')){
                                subnum += 1;
                                key = '英语';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresD') {
                            if(isThisSubject('D')){
                                subnum += 1;
                                key = '历史';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresE') {
                            if(isThisSubject('E')){
                                subnum += 1;
                                key = '地理';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresF') {
                            if(isThisSubject('F')){
                                subnum += 1;
                                key = '政治';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresG') {
                            if(isThisSubject('G')){
                                subnum += 1;
                                key = '生物';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresH') {
                            if(isThisSubject('H')){
                                subnum += 1;
                                key = '物理';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresI') {
                            if(isThisSubject('I')){
                                subnum += 1;
                                key = '化学';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresJ') {
                            if(isThisSubject('J')){
                                subnum += 1;
                                key = '信息技术';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresK') {
                            if(isThisSubject('K')){
                                subnum += 1;
                                key = '科学';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresL') {
                            if(isThisSubject('L')){
                                subnum += 1;
                                key = '思社';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'ScoresM') {
                            if(isThisSubject('M')){
                                subnum += 1;
                                key = '英语复习';
                            }else{
                                key = -2;
                            }
                        }
                        if (key == 'AllScores') {
                            key = subjectList[subnum] + '门总分';
                        }
                        if (key == 'GOrder') {
                            key = '年级排名';
                        }
                        if (key == 'Corder') {
                            key = '班级排名';
                        }
                        keys.push(key);
                    }
                }
                return keys;
            }
            function getSubject4Tab1(cId) {
                $http({//表1-2
                    method: 'post',
                    url: $rootScope.baseUrl + '/Interface0264.ashx',
                    data: {
                        examFlnkIds: paperId,
                        schoolFlnkId: schoolId,//学校id
                        period: period,//2015届
                        classflnkid: cId
                    }
                }).success(function (data) {
                    $scope.subNames4Tab1 = [];var key;
                    _.each(data.msg,function (item) {
                        key = '';
                        if (item.SubjectName == '语文') {
                            key = 'A';
                        }
                        if (item.SubjectName == '数学') {
                            key = 'B';
                        }
                        if (item.SubjectName == '英语') {
                            key = 'C';
                        }
                        if (item.SubjectName == '历史') {
                            key = 'D';
                        }
                        if (item.SubjectName == '地理') {
                            key = 'E';
                        }
                        if (item.SubjectName == '政治') {
                            key = 'F';
                        }
                        if (item.SubjectName == '生物') {
                            key = 'G';
                        }
                        if (item.SubjectName == '物理') {
                            key = 'H';
                        }
                        if (item.SubjectName == '化学') {
                            key = 'I';
                        }
                        if (item.SubjectName == '信息技术') {
                            key = 'J';
                        }
                        if (item.SubjectName == '科学') {
                            key = 'K';
                        }
                        if (item.SubjectName == '思社') {
                            key = 'L';
                        }
                        if (item.SubjectName == '英语复习') {
                            key = 'M';
                        }
                        if(key){
                            $scope.subNames4Tab1.push(key);
                        }
                    })
                });
            }

            function init() {
                //获取班级SmallNum和ClassName map列表
                $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                    Proc_name:'Proc_getPeriodCLass',
                    schoolFlnkid: schoolId,
                    period : period
                }).then(function(res){
                    var data = res.data.msg;
                    var map = {};
                    _.each(data, function(item){
                        map[item.SmallNum] = item.ClassName;
                    });
                    map['0'] = '年级';
                    $scope.classSmallNumNameMap = map;
                    if(user.schoolInfo && user.schoolInfo.RelationSchool) {
                        $http.post($rootScope.baseUrl + '/generalQuery.ashx', {
                            Proc_name:'Proc_getPeriodCLass',
                            schoolFlnkid: user.schoolInfo.RelationSchool,
                            period : period
                        }).then(function(res){
                            var data = res.data.msg;
                            var map = {};
                            _.each(data, function(item){
                                map[item.SmallNum] = item.ClassName;
                            });
                            map['0'] = '年级';
                            $scope.relatedClassSmallNumNameMap = map;
                            $scope.tabTb3();//初始化执行
                        }, $scope.tabTb3);
                    }else {
                        $scope.tabTb3();//初始化执行
                    }
                }, $scope.tabTb3);
                $http.get(resourceUrl + '/KmUnified/GetSingleBaseSchool?kmUnifiedFLnkID=' + unifiedId).then(function(res) {
                    if (res.data.Flag) {
                        $scope.checkRateList = _.map(res.data.ResultObj.BedRate.split(','), function (item) {
                            return {val: +item || 0}
                        });
                    }
                });
            }
            init();

            var tabIdMap = {
                'tab1': 'targetTb1',
                'tab3': 'targetTb3'
            };

            (function(){
                var headerTop;
                $scope.$watch('isChoseTab', function(tab){
                    if(tab && tabIdMap[tab]) {
                        headerTop = undefined;
                        $(window).off('scroll').on('scroll', function(e){
                            var $node = $('#'+tabIdMap[tab]).find('thead');
                            if(headerTop === undefined) {
                                headerTop = $node.offset().top;
                            }
                            if(window.scrollY > headerTop) {
                                $node.css({
                                    transform: 'translateY(' + (window.scrollY - headerTop) + 'px)'
                                });
                            }else {
                                $node.css({
                                    transform: 'translateY(0px)'
                                });
                            }
                        });
                    }
                });
            })();

    }]
});