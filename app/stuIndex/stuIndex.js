 define(['highcharts'], function () {
	return ['$http', '$scope', '$rootScope', '$state', '$location', 'ngDialog',function ($http, $scope, $rootScope, $state, $location, ngDialog) {
		var user = JSON.parse(sessionStorage.getItem('currentUser'));
		var Sfid = user.sfid;
		var ClassFlnkid = user.scid;
		$scope.ClassFlnkid = ClassFlnkid;
		$scope.UserId = Sfid;
		$scope.noklgdata = false;
		// 页面初始数据
		var init = function (Sfid) {
			// 用户战斗力变化
			$http({
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0166.ashx',
				data: {
					Limit: 10,
					UserFlnkID: Sfid
				}
			}).success(function (data) {
				if (data.msg != '') {
					$scope.Combat = data.msg;
					$scope.ClassName = data.msg[0].ClassName;
					$scope.MyName = data.msg[0].MyName;
					sessionStorage.setItem('MyName', $scope.MyName);
					// 班级战斗力
					var ClassPow = [], MyPow = [], Day = [];
					for (var i = 0 ; i < data.msg.length ; i++) {
						ClassPow.push(Number(data.msg[i].ClassPow))
					}
					// 自身战斗力
					for (var i = 0 ; i < data.msg.length ; i++) {
						MyPow.push(Number(data.msg[i].MyPow))
					}
					// 日期
					for (var i = 0 ; i < data.msg.length ; i++) {
						Day.push(data.msg[i].Day)
					}
					highchartsline(ClassPow, MyPow, Day)

				} else {
					ngDialog.open({
						template:
								'<p>暂无数据</p>' +
								'<div class="ngdialog-buttons">' +
								'<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
						plain: true,
						className: 'ngdialog-theme-plain'
					});
					return false;
				}
			}).then(function () {
				//用户战斗力信息
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0165.ashx',
					data: {
						UserFlnkID: Sfid
					}
				}).success(function (data) {
					console.log(data)
					$scope.UserCombat = data.msg[0];
				})
			}).then(function () {
				// 成绩数据
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0167.ashx',
					data: {
						Limit: 999,
						UserFlnkID: Sfid
					}
				}).success(function (data) {
					var scoreList = [];
					for (var i = 0; i < data.msg.length; i++) {
						scoreList.push({
							AllSc: data.msg[i].AllSc,
							ClassAvgSc: data.msg[i].ClassAvgSc,
							ExAnswerFlnkID: data.msg[i].ExAnswerFlnkID,
							ExamFlnkID: data.msg[i].ExamFlnkID,
							ExamName: data.msg[i].ExamName,
							ExamTime: data.msg[i].ExamTime.slice(0, 10),
							LostKnowledge: data.msg[i].LostKnowledge,
							Order: data.msg[i].Order,
							Rank: data.msg[i].Rank,
							Sc: data.msg[i].Sc,
						})
					}
					$scope.mentData = scoreList;

					var userPaperinfo = JSON.stringify(scoreList);

					console.log("开始存储");
					localStorage.setItem('userPaperinfo', userPaperinfo);
					console.log('存储完毕');

				})
			}).then(function () {
				// 知识点数据
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0168.ashx',
					data: {
						UserFlnkID: Sfid
					}
				}).success(function (data) {
					if (data.msg != '') {
						$scope.KnowledgeData = data.msg;
					} else {
						$scope.noklgdata = true;
					}

				})
			})
		}
		init(Sfid);

		// 战斗力提升
		var highchartsline = function (ClassPow, MyPow, Day) {
			$('#containers').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: false
				},
				xAxis: {
					categories: Day
				},
				credits: {
					enabled: false,
				},
				yAxis: {
					title: false
				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					floating: true
				},

				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						}
					}
				},
				exporting: {
					enabled: false
				},
				series: [{
					name: '战斗力变化',
					color: '#ff4c79',
					data: MyPow
				}, {
					name: '班级平均战斗力',
					color: '#2dc0e8',
					data: ClassPow
				}]
			});
		}

	}]
})


