/**
 * Created by clc
 */
define(['underscore-min', 'jquerymedia', 'ckeditor', '../service/assignRouter'], function () {
	return ['$scope', '$state', '$location', '$q', 'ngDialog', '$timeout', '$sce', 'constantService', 'paperService', 'apiCommon',
		function ($scope, $state, $location, $q, ngDialog, $timeout, $sce, constantService, paperService, apiCommon) {
			var unifiedId = $location.search().unifiedId;
			var action = $location.search().action;
			var Checked = $location.search().Checked;
			var user = JSON.parse(sessionStorage.getItem('currentUser'));
			var onNewList = [];
			var onOldList = [];
            $scope.nature = true;
			$scope.status = $location.search().status;
			$scope.marktype = $location.search().marktype;
			$scope.examname = $location.search().examname;
			$scope.isModified = false;
			//获取批阅信息
			$scope.getDetailByUnId = function () {
				apiCommon.getEncryptQList({
					unifid: unifiedId,
					getall: '1'
				}).then(function (res) {
					if (_.isArray(res.data.msg)) {
						$scope.getDetailList = res.data.msg;
					} else {
						$scope.getDetailList = [];
					}
				})
			};
			$scope.queueList = [
				{name: '首页', url: 'myApp.masterHome'},
				{name: '考试中心', url: 'myApp.examCenter'},
				{name: '设置分值', url: 'myApp.setValue'}];
			$scope.getDetailByUnId();
			//获取学生答卷信息
			$scope.classList = [];
			$scope.getClassList = function () {
				apiCommon.generalQuery({
						Proc_name: 'getUnifiedItem',
						UnifiedItemFid: unifiedId//子统考Id
				}).then(function (res) {
					$scope.classList = res.data.msg[0];
				})
			};
			$scope.getClassList();
			//获取试卷
			$scope.answerList = ['A', 'B', 'C', 'D'];
			$scope.answerListTwo = ['A', 'B'];
			$scope.getPapers = function () {
				apiCommon.getPaperConfig({
					unifiedId: unifiedId
				}).success(function (data) {
					$scope.mainScore = [];
					$scope.paperData = data.msg;
					$scope.groupData = data.msg.meta;
					$scope.totalPoints = 0;
					var ExamTime = $scope.paperData.ExamTime.split(' '), curIndex, isSmaller;
					$scope.ExamTime = ExamTime[0];
					//获取初始值
					_.each($scope.groupData, function (g) {
						_.each(g.question, function (q) {
							q.qtypeName = g.qType;
							onNewList.push(q);
						})
					});
					for (i = 0; i < onNewList.length; i++) {
						onOldList[i] = {
							showDis: onNewList[i].showDis,
							answer: onNewList[i].answer,
							difficultLevel: onNewList[i].difficultLevel === '' ? 1 : onNewList[i].difficultLevel,
							knowledge: onNewList[i].knowledge === undefined ? '知识点' : onNewList[i].knowledge,
							fullScore: onNewList[i].fullScore,
							missScore: onNewList[i].missScore,
							orders: onNewList[i].orders,
							mixedParent: onNewList[i].mixedParent
						}
					}
					_.each($scope.groupData, function (s) {
						$scope.totalPoints = +s.fullScore + $scope.totalPoints;
						if (s.question[0].isObjective === 1) {
							s.showType = 1;
						} else {
							if (s.qType === '选择题' || s.qType === '多选题') {
								s.showType = 1;
								_.each(s.question, function (item) {
									item.answer = item.answer.replace(/[^ABCDEFG]/gi, "");
								});
							} else {
								s.showType = 0;
							}
						}
						$scope.mainScore = _.filter(s.question, function (ss) {
							return ss.mixedParent === '' && ss.isMixed === 1;
						});
                        curIndex = 0;
						_.each(s.question, function (qus) {
							qus.missScore = parseInt(qus.missScore);
							qus.knowledges = '';
							qus.knowledgeID = '';
							if (qus.knowledge === undefined) {
								qus.knowledges = '知识点';
								qus.knowledgeID = '';
							} else {
								qus.knowledges = _.pluck(qus.knowledge, 'name').join(',');
								qus.knowledgeID = _.pluck(qus.knowledge, 'id').join(',');
							}
							_.each($scope.mainScore, function (score) {
								if (score.orders === qus.mixedParent) {
									score.fullScore = score.fullScore + qus.fullScore;
								}
							});
                            isSmaller = +qus.showDis.split('.')[0];
                            curIndex = isSmaller > curIndex ? isSmaller : curIndex;
                            if(isSmaller !== curIndex){
                                $scope.nature = true;
                            }
						});
					});

					apiCommon.knowledgeList({
							subjectId: $scope.paperData.SubjectId,
							pharseId: $scope.paperData.pharseId
					}).success(function (res) {
						if (!_.isArray(res.msg)) {
							$scope.knowledgeList = [];
						} else {
							$scope.knowledgeList = res.msg;
						}
					});
					for (i = 0; i < onNewList.length; i++) {
						if (onNewList[i].isObjective == 1 && onNewList[i].answer === '' || onNewList[i].fullScore === 0) {
							var isShow = true;
							break;
						} else {
							isShow = false;
						}
					}
					// if (Checked != 1 && isShow) {
					//     constantService.alert('温馨提示，客观题必须设置答案，分值不能为0!');
					// }
				})
			};
			$scope.getPapers();

			//查看原卷
			$scope.thispath = $location.path();
			$scope.checkPaper = function () {
				window.open('#/composing?pathss=' + $scope.thispath + '&ExamFlnkID=' + $scope.paperData.FLnkID + '&action=0&isExamCenter=1&isOpenNewWin=1');
			};

			//返回
			$scope.back = function () {
				$state.go('myApp.examCenter');
			};

			//修改分数
			$scope.checkIgel = function (question, group) {
				if (question.fullScore * 10 % 5 !== 0 || question.fullScore === 0) {
					constantService.alert('分值只能为0-100并且为0.5的整数倍!');
					question.fullScore = 0;
				} else {
					var qScores = 0;
					var Scores = 0;
					_.each(group.question, function (res) {
						qScores = +res.fullScore + qScores;
					});
					group.fullScore = qScores;
					_.each($scope.groupData, function (s) {
						Scores = +s.fullScore + Scores;
					});
					$scope.totalPoints = Scores;
				}
			};
			$scope.checkScore = function (question, group) {
				if (question.fullScore === null) {
					question.fullScore = 0
				}
			};
			//修改半分分数
			$scope.checkIllgel = function (question) {
				question.answer = question.answer.replace(/(^\s+)|(\s+$)/g, "");
				if (question.answer == '') {
					constantService.alert('请勿设置半分!');
					question.missScore = 0;
				} else if (question.missScore > 0 && $scope.answerList.indexOf(question.answer.toUpperCase()) >= 0) {
					constantService.alert('单选题请勿设置半分!');
					question.missScore = 0;
				} else {
					if (question.missScore > question.fullScore || question.missScore < 0 || question.missScore * 10 % 5 !== 0) {
						question.missScore = 0;
						constantService.alert('请输入合适的分数!');
					}
				}
			};

			//修改题号
			$scope.changeShowDis = function (question, group) {
				var patt = new RegExp(/\$/);
				if (patt.test(question.showDis)) {
					constantService.alert('输入含有特殊字符!');
					question.showDis = parseInt(question.orders);
					return;
				} else {
					if (question.showDis != undefined && question.showDis != '') {
						if (question.showDis != question.orders) {
							var isRepeat = !!_.find(group.question, function (resq) {
								if (question.orders != resq.orders && resq.mixedParent != question.orders) {
									return question.showDis === resq.showDis
								}
							});
							if (isRepeat) {
								constantService.alert('输入重复!');
								question.showDis = parseInt(question.orders);
								return;
							}
						}
					} else {
						constantService.alert('题号不能为空!');
						question.showDis = parseInt(question.orders);
						return
					}
				}
			};

			$scope.selectContentList = [];
			//跳转
			$scope.goMao = function (group, index) {
				$scope.selectContentList = [];
				if ($scope.isContentChecked(group)) {
					var index = _.findIndex($scope.selectContentList, function (qstitem) {
						return qstitem.qType === group.qType;
					});
					$scope.selectContentList.splice(index, 1);
				} else {
					$scope.selectContentList.push(group);
				}
				var topL1 = $("#" + index + "").offset().top;
				$(document).scrollTop(topL1);
			};
			$scope.isContentChecked = function (group) {
				return !!_.find($scope.selectContentList, function (qstitem) {
					return qstitem.qType === group.qType;
				});
			}

			//返回顶部
			$scope.goTop = function () {
				$scope.selectContentList = [];
				$(document).scrollTop(0);
			};

			//选择题答案
			$scope.choiceClick = function (question, index, isTwo) {
				if (+isTwo === 2) {
					question.answer = $scope.answerListTwo[index];
				} else {
					if (question.answer.toUpperCase().indexOf($scope.answerList[index]) >= 0) {
						question.answer = question.answer.replace($scope.answerList[index], '');
						if ($scope.answerList.indexOf(question.answer.toUpperCase()) >= 0) {
							question.missScore = 0;
						}
					} else {
						question.answer = question.answer + $scope.answerList[index];
						if ($scope.answerList.indexOf(question.answer.toUpperCase()) >= 0) {
							question.missScore = 0;
						}
					}
					var d = !(question.answer == '' || question.fullScore == 0 || $scope.answerList.indexOf(question.answer.toUpperCase()) >= 0) || $scope.status > 5;
					d = d
				}
			};
			$scope.checkedQuestion = [];
			$scope.isAll = false;

			//checkbox
			$scope.checkAllClick = function (group) {
				var isCheckObject = _.find(group.question, function (qus) {
					return qus.isObjective == 1;
				});
				$scope.checkedQuestion = [];
				if (!isCheckObject) {
					if (group.isAll) {
						group.isAll = false;
						$scope.checkedQuestion = [];
					} else {
						_.each($scope.groupData, function (item) {
							item.isAll = false;
						});
						group.isAll = true;
						_.each(group.question, function (ques) {
							if (ques.combineHide === false || ques.combineHide == undefined) {
								$scope.checkedQuestion.push(ques);
							}
						});
					}
				} else {
					constantService.alert('组内有客观题无法全部合并!')
				}
			};

			$scope.checkClick = function (question, g) {
				if ($scope.isQTypeChecked(question)) {
					sortIndex($scope.checkedQuestion);
					var index = _.findIndex($scope.checkedQuestion, function (res) {
						return res.orders === question.orders
					});
					var x = $scope.checkedQuestion.length - index;
					$scope.checkedQuestion.splice(index, x + 1);
				} else {
					if (_.isEmpty($scope.checkedQuestion)) {
						if (question.mixedParent === '') {
							_.each(g.question, function (add) {
								if (question.orders === add.mixedParent) {
									$scope.checkedQuestion.push(add);
								}
							});
							$scope.checkedQuestion.push(question);
						} else {
							$scope.checkedQuestion.push(question);
						}
						$scope.checkedQuestion[0].qType = g.qType;
					} else if (g.qType === $scope.checkedQuestion[0].qType) {
						$scope.editQuestion = [];
						$scope.editQuestionList = [];
						_.each($scope.groupData, function (res) {
							_.each(res.question, function (resq) {
								$scope.editQuestionList.push(resq)
							});
						});
						var firstIndex = _.findIndex($scope.editQuestionList, function (index) {
							return index.orders == question.orders;
						});
						var lastCheck = $scope.checkedQuestion[$scope.checkedQuestion.length - 1];
						var lastIndex = _.findIndex($scope.editQuestionList, function (index) {
							return index.orders == lastCheck.orders;
						});
						var series = Math.abs(lastIndex - firstIndex);
						if (series >= 1) {
							$scope.addQus = [];
							if (question.mixedParent == '') {
								_.each(g.question, function (add) {
									if (question.orders == add.mixedParent) {
										$scope.checkedQuestion.push(add);
									}
								});
								$scope.checkedQuestion.push(question);
							} else {
								$scope.checkedQuestion.push(question);
							}
							sortIndex($scope.checkedQuestion);
							_.each($scope.checkedQuestion, function (val) {
								val.qType = g.qType;
							});
							var indexs = _.findIndex(g.question, function (item) {
								return item.orders === $scope.checkedQuestion[0].orders;
							});
							var end = _.findIndex(g.question, function (item) {
								return item.orders === $scope.checkedQuestion[$scope.checkedQuestion.length - 1].orders;
							});
							$scope.addQus = g.question.slice(indexs + 1, end);
							var isCheckObject = _.find($scope.addQus, function (qus) {
								return qus.isObjective == 1;
							});
							if (!isCheckObject) {
								_.each($scope.addQus, function (qs) {
									if (_.indexOf($scope.checkedQuestion, qs) == -1) {
										$scope.checkedQuestion.push(qs);
									}
								});
							} else {
								constantService.alert('客观题无法合并');
								var splice = _.findIndex($scope.checkedQuestion, function (sp) {
									return sp.orders === question.orders;
								});
								$scope.checkedQuestion.splice(splice, 1);
							}
						} else {
							if (question.mixedParent == '') {
								_.each(g.question, function (add) {
									if (question.orders == add.mixedParent) {
										$scope.checkedQuestion.push(add);
									}
								});
								$scope.checkedQuestion.push(question);
							} else {
								$scope.checkedQuestion.push(question);
							}
						}
					} else {
						constantService.alert("请勿跨题型选题!");
					}
				}
				if ($scope.checkedQuestion.length === g.question.length) {
					g.isAll = true;
				} else {
					g.isAll = false;
				}
			};
			$scope.isQTypeChecked = function (q) {
				return !!_.find($scope.checkedQuestion, function (ques) {
					return ques.orders === q.orders;
				});
			};

			function resetQuestionMarkRecord(params) {//重置批阅结果
				var defer = $q.defer();
				apiCommon.resetQuestionMarkRecord({
					unifiedId: unifiedId,
					qFlnkId: params
				}).then(function (res) {
					defer.resolve(res);
				}, function (res) {
					defer.reject();
				});
				return defer.promise;
			}

			//设置题号
			$scope.setQstNum = function () {
				var curIndex, isSmaller, isChange;
				$scope.nature = !$scope.nature;
				_.each($scope.groupData, function (group) {
                    curIndex = 0;
					_.each(group.question, function (item, index) {
                        isChange = false;
						if ($scope.nature) {
							item.showDis = item.orders;
						} else {
                            isSmaller = item.showDis.split('.');
                            if(index) {
                                isChange = +isSmaller[0] > +group.question[index - 1].orders.split('.')[0];
							} else {
                            	isChange = true;
							}
                            curIndex = isChange ? (curIndex + 1 ) : curIndex;
							item.showDis = curIndex + (isSmaller[1] ? ( '.' + isSmaller[1]) : '');
						}
					})
				});
			};
			//合并给分
			$scope.combineScore = function () {
				var isCombineModeB = _.find($scope.checkedQuestion, function(item){
					return item.mode === 'B';
				});
				if(isCombineModeB) {
					var orders = _.uniq(_.map(_.pluck($scope.checkedQuestion, 'orders'), function(item){
						return item.split('.')[0];
					}));
					if(orders.length > 1) {
						constantService.alert("不允许跨子题合并！");
						return;
					}
				}
				if ($scope.checkedQuestion.length <= 1) {
					constantService.alert("请选择2个或2个以上的题目!");
				} else if ($scope.status > 5) {
					constantService.alert('本次考试已经提交分析，如需合并请先取消分析!')
				} else {
					if (_.isArray($scope.getDetailList) && $scope.getDetailList.length > 0) {
						var currentDetail = [];
						_.each($scope.checkedQuestion, function (res) {
							_.each($scope.getDetailList, function (resq) {
								if (res.qFlnkId === resq.qfinkid) {
									currentDetail.push(resq);
								}
							})
						});
						var currentDetailShow = [];
						_.each(currentDetail, function (item) {
							var count = parseInt(item.allCount) - parseInt(item.noFinishCount);
							if (count != 0) {
								currentDetailShow.push(item);
							}
						});
						if (currentDetailShow.length > 0) {
							constantService.confirm('提示', '重置将清除批阅配置及批阅结果，是否重置', ['重置', '取消'], function () {
								$scope.comReSet();
							}, function () {
								ngDialog.closeAll();
							});
							$scope.comReSet = function () {
								var newFidList = [];
								_.each(currentDetailShow, function (item) {
									newFidList.push(resetQuestionMarkRecord(item.qfinkid));
								});
								if (newFidList.length > 0) {
									$q.all(newFidList).then(function (resq) {
										var comScore = 0;
										sortIndex($scope.checkedQuestion);
										var firstQst = $scope.checkedQuestion[0];
										var lastQst = $scope.checkedQuestion[$scope.checkedQuestion.length - 1];
										firstQst.showDis = firstQst.orders + '-' + lastQst.orders;
										_.each($scope.checkedQuestion, function (res) {
											comScore = +res.fullScore + comScore;
										});
										firstQst.fullScore = comScore;
										firstQst.combineHide = false;
										firstQst.isMixed = 1;
										_.each($scope.checkedQuestion.slice(1), function (item) {
											item.combineHide = true;
											item.isMixed = 1;
											item.mixedParent = firstQst.orders;
											item.showDis = firstQst.showDis;
											_.each(onOldList, function (score) {
												if (item.orders === score.orders) {
													item.fullScore = score.fullScore;
												}
											});
										});
										ngDialog.closeAll();
										$scope.checkedQuestion = [];
										_.each(currentDetailShow, function (item) {
											item.noFinishCount = item.allCount
										});
									}, function () {
										ngDialog.closeAll();
									})
								}
							}
						} else {
							combineRset()
						}
					} else {
						combineRset()
					}
				}

				function combineRset() {//合并给分
					constantService.confirm('提示', '合并给分', ['合并', '取消'], function () {
						$scope.sure();
					}, function () {
						ngDialog.closeAll();
					});
					var comScore = 0;
					sortIndex($scope.checkedQuestion);
					$scope.sure = function () {
						var firstQst = $scope.checkedQuestion[0];
						var lastQst = $scope.checkedQuestion[$scope.checkedQuestion.length - 1];
						firstQst.showDis = firstQst.orders + '-' + lastQst.orders;
						_.each($scope.checkedQuestion, function (res) {
							if (res.mixedParent === '') {
								comScore = +res.fullScore + comScore;
							}
						});
						firstQst.fullScore = comScore;
						firstQst.combineHide = false;
						firstQst.isMixed = 1;
						_.each($scope.checkedQuestion.slice(1), function (item) {
							item.combineHide = true;
							item.isMixed = 1;
							item.mixedParent = firstQst.orders;
							item.showDis = firstQst.showDis;
							_.each(onOldList, function (score) {
								if (item.orders === score.orders) {
									item.fullScore = score.fullScore;
								}
							});
						});
						ngDialog.closeAll();
						$scope.checkedQuestion = [];
						$scope.isModified = true;
					}
				}
			};

			//按index值排序
			function sortIndex(sortData) {
				sortData.sort(function (a, b) {
					$scope.editQuestion = [];
					$scope.editQuestionList = [];
					_.each($scope.groupData, function (res) {
						_.each(res.question, function (resq) {
							$scope.editQuestionList.push(resq)
						});
					});
					var aIndex = _.findIndex($scope.editQuestionList, function (index) {
						return index.orders == a.orders;
					});
					var bIndex = _.findIndex($scope.editQuestionList, function (indexs) {
						return indexs.orders == b.orders;
					});
					return +aIndex - +bIndex;
				});
			}

			//取消合并
			$scope.cancelCombine = function (question, g) {
				if (+$scope.status > 5) {
					constantService.alert('本次考试已经提交分析，如需拆分请先取消分析!')
				} else {
                    $scope.curQuestion = question;
					if (_.isArray($scope.getDetailList) && $scope.getDetailList.length > 0) {
						var currentDetail = [];
						$scope.editQue = [];
						$scope.isReset = false;
						var comScore = 0;
						//获取到所有子题
						var subs = _.filter(g.question, function (item) {
							return item.mixedParent === question.orders;
						});
						$scope.editQue = subs.concat(question);
						_.each($scope.editQue, function (res) {
							_.each(onOldList, function (score) {
								if (res.orders === score.orders) {
									comScore = score.fullScore + comScore;
								}
							});
							if (res.mixedParent === '') {
								if (res.fullScore === comScore) {
									$scope.isReset = true;
								} else {
									$scope.isReset = false;
								}
							}
							_.each($scope.getDetailList, function (resq) {
								if (res.qFlnkId === resq.qfinkid) {
									currentDetail.push(resq);
								}
							})
						});
						var currentDetailShow = [];
						_.each(currentDetail, function (item) {
							var count = parseInt(item.allCount) - parseInt(item.noFinishCount);
							if (count != 0) {
								currentDetailShow.push(item);
							}
						});
						if (currentDetailShow.length > 0 && !$scope.isReset) {
							constantService.confirm('提示', '重置将清除批阅配置及批阅结果，是否重置', ['重置', '取消'], function () {
								$scope.reSet();
							}, function () {
								ngDialog.closeAll();
							});
							$scope.reSet = function () {
								var newFidList = [];
								_.each(currentDetail, function (item) {
									newFidList.push(resetQuestionMarkRecord(item.qfinkid));
								});
								if (newFidList.length > 0) {
									$q.all(newFidList).then(function (resq) {
										$scope.editQue = [];
										//获取到所有子题
										var subs = _.filter(g.question, function (item) {
											return item.mixedParent === question.orders;
										});
										$scope.editQue = subs.concat(question);
										_.each($scope.editQue, function (res) {
											var comScoreOld = _.find(onNewList, function (score) {
												return res.orders === score.orders;
											});
											if (res.mixedParent === '') {
												if (res.fullScore === comScoreOld.fullScore) {
													var oldFullScore = _.find(onOldList, function (old) {
														return res.orders === old.orders
													});
													res.fullScore = oldFullScore.fullScore;
												} else {
													assignScore(subs, question);
												}
											}
										});
										_.each($scope.editQue, function (s) {
											s.showDis = s.orders;
											s.isMixed = 0;
											s.mixedParent = '';
											s.combineHide = false;
										});
										_.each(currentDetailShow, function (item) {
											item.noFinishCount = item.allCount
										});
										ngDialog.closeAll();
									}, function () {
										ngDialog.closeAll();
									});
								}
							};
						} else {
							cancelRset();
						}
					} else {
						cancelRset()
					}
				}

				function cancelRset() {//取消合并
					constantService.confirm('取消合并', '请确认需要取消的题号: 第' + $scope.curQuestion.showDis + '题', ['确定', '取消'], function () {
						$scope.cancel();
					}, function () {
						ngDialog.closeAll();
					});
					$scope.curQuestion = question;
					$scope.cancel = function () {
						$scope.editQue = [];
						var comScore = 0;
						//获取到所有子题
						var subs = _.filter(g.question, function (item) {
							return item.mixedParent === question.orders;
						});
						$scope.editQue = subs.concat(question);
						_.each($scope.editQue, function (res) {
							_.each(onOldList, function (score) {
								if (res.orders === score.orders) {
									comScore = score.fullScore + comScore;
								}
							});
							if (res.mixedParent === '') {
								if (res.fullScore === comScore) {
									var oldFullScore = _.find(onOldList, function (old) {
										return res.orders === old.orders
									});
									res.fullScore = oldFullScore.fullScore;
								} else {
									assignScore(subs, question);
								}
							}
						});
						_.each($scope.editQue, function (s) {
							s.showDis = s.orders;
							s.isMixed = 0;
							s.mixedParent = '';
							s.combineHide = false;
						});
						ngDialog.closeAll();
						$scope.isModified = true;
					}
				}
			};
			//批量给分
			$scope.batchScore = function () {
				ngDialog.open({
					template: '<div class="batchScore">' + ' <div class="batchScore-nav">批量设置给分</div>' + ' <div class="batchScore-choice">' +
					'<div class="batchScore-table">起止序号</div>' +
					'<div class="batchScore-row"><div class="batchScore-rows" ng-repeat="row in rowList">' +
					'<input type="text" min="1" minlength="1" maxlength="10" ng-model="row.start" ng-change="getFirst(row)"/ style="margin-left:25px"> <span class="setValue-51color">一</span> ' +
					'<input type="text" min="1" minlength="1" maxlength="10" ng-model="row.end" ng-change="getLast(row)"/> <span style="margin-left:110px">每题 </span>' +
					'<input type="number" ng-model="row.setScores" ng-change="getScore(row)"/><span> 分</span></div></div>' +
					'<div class="batchScore-add"><span style="cursor:pointer" ng-click="addRow()">添加一行</span></div>' +
					'<div class="batchScore-false" ng-show="falseOne">请正确输入题号或分数!</div>' +
					'<div class="batchScore-false" ng-show="falseTwo">题号在1~{{arrayLength}}之间!</div>' +
					'<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()"  style="margin:20px 20px 0 116px">取消</div>' +
					'<div class="batchScore-btn" ng-click="addSure()" style="margin-top:20px">确定</div></div>' +
					'</div></div>',
					className: 'ngdialog-theme-default ngdialog-setValue',
					scope: $scope,
					closeByDocument: false,
					plain: true
				});
				$scope.rowList = [{start: null, end: null, setScores: null}];
				$scope.getFirst = function (row) {
					$scope.firstOrd = row.start;
				};
				$scope.getLast = function (row) {
					$scope.lastOrd = row.end;
				};
				$scope.getScore = function (row) {
					$scope.setScore = row.setScores;
				};
				$scope.addSure = function () {
					var newList = [], newIndex = 0;
					_.each($scope.groupData, function (res) {
						for (var i = newIndex, j = +res.qNumber + newIndex; i < j; i++) {
							newList.push({
								order: i + 1,
								type: res.qType,
								fullScore: res.fullScore
							})
						}
						newIndex += +res.qNumber;
					});
					$scope.arrayLength = newList.length;
					_.each($scope.rowList, function (editScore) {
						var ordNum = editScore.end - editScore.start + 1;
						if (ordNum <= 0 || editScore.setScores <= 0 || editScore.setScores * 10 % 5 !== 0) {
							$scope.falseOne = true;
							$timeout(function () {
								$scope.falseOne = false;
							}, 900);
							return false
						} else if (editScore.start < 1 || editScore.end > newList.length) {
							$scope.falseTwo = true;
							$timeout(function () {
								$scope.falseTwo = false;
							}, 900);
							return false
						} else {
							$scope.editQuestion = [];
							$scope.editQuestionList = [];
							_.each($scope.groupData, function (res) {
								_.each(res.question, function (resq) {
									$scope.editQuestionList.push(resq)
								});
							});
							console.log(editScore.start);
							console.log(editScore.end);
							var firstIndex = _.findIndex($scope.editQuestionList, function (index) {
								return index.orders == editScore.start;
							});
							var lastIndex = _.findIndex($scope.editQuestionList, function (index) {
								return index.orders == editScore.end;
							});
							console.log(firstIndex);
							console.log(lastIndex);
							$scope.editQuestion = $scope.editQuestionList.slice(firstIndex, lastIndex + 1);
							_.each($scope.editQuestion, function (score) {
								score.fullScore = editScore.setScores;
							});
							var Scores = 0;
							_.each($scope.groupData, function (res) {
								var qScores = 0;
								_.each(res.question, function (resq) {
									qScores = resq.fullScore + qScores;
								});
								res.fullScore = qScores;
								Scores = res.fullScore + Scores;
							});
							$scope.totalPoints = Scores;
							ngDialog.closeAll();
						}
					});
				};
				$scope.addRow = function () {
					var newRow = [{start: null, end: null, setScores: null}];
					$scope.rowList.push(newRow[0])
				};
			};

			//修改答案
			$scope.modifyAnswer = function (question) {
				$scope.currentQuestion = question;
				ngDialog.open({
					template: '/AnswerEditor.html',
					className: 'ngdialog-theme-default',
					appendClassName: 'dialog-edit-paper dialog-modify-paper',
					closeByDocument: false,
					scope: $scope,
					preCloseCallback: function () {
						$scope.$broadcast('$destory');
					}
				});
			};
			$scope.doModifyAnswer = function (data) {
				$scope.currentQuestion.answer = data.answer.replace(/<.*?>/ig,"");;
			};
			//编辑知识点
			$scope.modifyKlg = function (question) {
				$scope.currentQuestion = question;
				ngDialog.open({
					template: '/klgEditor.html',
					className: 'ngdialog-theme-default',
					appendClassName: 'dialog-edit-paper dialog-modify-knowlg',
					closeByDocument: false,
					scope: $scope
				});
			};
			$scope.doModifyKlg = function (data) {
				$scope.currentQuestion.knowledge = _.map(data, function (item) {
					return {
						id: item.unitId,
						name: item.unitName
					};
				});
				$scope.currentQuestion.knowledges = _.pluck(data, 'unitName').join(',');
				$scope.currentQuestion.knowledgeID = _.pluck(data, 'unitId').join(',')
			};
			//修改知识点难度
			$scope.saveStarAndChange = function (klgitem) {
				var param = {};
				if ('diff' === flag) {//修改难度
					param = {
						'diff': klgitem.diff,
						'unitId': klgitem.unitId || klgitem.knowledgeID
					};
				}
				doChangeKlg(param).success(function (data) {
					if (!!data && data.code === 0) {
						constantService.alert('修改成功！');
					} else {
						constantService.alert('修改失败！存在同名知识点，请重新修改！');
					}
				});
			};
			//保存试卷
			$scope.isNeedShow = false;
			$scope.isNewChange = false;
			$scope.saveClick = function () {//点击保存
                $scope.needResetEncrypt = 0;
				$scope.newChangeAnswer = [];
				for (var j = 0; j < onNewList.length; j++) {//判断哪些选择题答案或分数变动过
					var oldVal = _.find(onOldList, function (res) {
						return onNewList[j].orders === res.orders
					});
					if (onNewList[j].isObjective == 1) {//判断是否为选择题
						if (oldVal.answer === '' || $scope.classList.ExamNum === null) {//判断是否选择题是否为空
							$scope.isNewChange = true;
							break;
						}
					}
				}

                for (var n = 0; n < onNewList.length; n++) {
                    if (onNewList[n].isObjective == 1) {
                        if ($scope.isNewChange) {
                            $scope.newChangeAnswer = [];
                            break;
                        } else {
                            var oldVal = _.find(onOldList, function (res) {
                                return onNewList[n].orders === res.orders;
                            });
                            if (!isAnswerSame(onNewList[n].answer, oldVal.answer) || onNewList[n].fullScore != oldVal.fullScore || onNewList[n].missScore != oldVal.missScore) {
                                $scope.newChangeAnswer.push(onNewList[n]);
                            }
                        }
                    }
                }
                for(var n = 0; n < onNewList.length; n++)
                for (var i = 0; i < onNewList.length; i++) {//判断客观题是否改动或分值不为0，是否进行试卷审核
                    if(onNewList[i].isObjective == 1){
                        if ( onNewList[i].answer === '' || onNewList[i].fullScore === 0) {
                            $scope.isNeedShow = false;
                            break;
                        } else {
                            $scope.isNeedShow = true
                        }
                    }else {
                        $scope.isNeedShow = true
                    }
                }
                if (+$scope.status > 5) {//已经提交分析
                    constantService.alert('本次考试已经提交分析，如需修改请先取消分析!')
                }else if( !$scope.isNeedShow && Checked == 1){
                    constantService.alert('请录入所有客观题答案及试卷分值!')
                } else {
                    $scope.getSaveFunction();
                }
            };
			function isAnswerSame(newVal, oldVal) {
				var result = true, i = 0;
				var newArr = newVal.split(','), oldArr = oldVal.split(',');
				if(newArr.length === oldArr.length) {
					if(newVal.length > 1){
						newArr.sort(function(a, b){
							return a-b;
						});
						oldArr.sort(function(a,b){
							return a-b;
						});
						_.each(newVal, function (item, index) {
							if(item !== oldVal[index]) {
								result = false;
							}
                        })
					}else {
						result = newVal === oldVal;
					}
				}else {
					result = false;
				}
                return result;
            }
            $scope.getSaveFunction = function () {//保存提示说明
                if (action === '1' || $scope.status === '1') {//开始默认保存形式,step跳转到2
                    $scope.saveNewPager();
                    // ngDialog.open({
                    //     template: '<div class="batchScore">' + '<div class="batchScore-nav">保存试卷</div>' +
                    //     '<div class="batchScore-title">核对无误后点击确定保存试卷</div>' +
                    //     '<div class="batchScore-title" ng-show="!isNeedShow">（请录入所有客观题答案及试卷分值）</div>' +
                    //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()"  style="margin:20px 20px 0 116px">取消</div>' +
                    //     '<div class="batchScore-btn" ng-click="saveNewPager()" style="margin-top:20px">确定</div></div>' +
                    //     '</div>',
                    //     className: 'ngdialog-theme-default ngdialog-setValue',
                    //     scope: $scope,
                    //     closeByDocument: false,
                    //     plain: true
                    // });
                } else {
                    for (var j = 0; j < onNewList.length; j++) {//根据题号判断，是否需要清除配置(批阅人)
                        var oldVal = _.find(onOldList, function (res) {
                            return onNewList[j].orders === res.orders
                        });
                        if (onNewList[j].showDis !== oldVal.showDis || $scope.newChangeAnswer.length > 0) {
                            $scope.isChangeModified = true;
                            $scope.needResetEncrypt = 1;
                            break;
                        }
                    }
                    if (+$scope.status >= 3) {//试卷已经形参阅卷配置
                        //改变分数，清除所有阅卷信息
                        $scope.currentDetailShow = [];
                        var currentDetail = [],currentDetails = [];
                        for (var i = 0; i < onNewList.length; i++) {
                            if(onNewList[i].isObjective != 1){
                                var oldVals = _.find(onOldList, function (res) {
                                    return onNewList[i].orders === res.orders
                                });
                                if (onNewList[i].fullScore !== oldVals.fullScore) {
                                    currentDetail.push(onNewList[i]);
                                }
                            }
                        }
                        _.each(currentDetail, function (res) {
                            _.each($scope.getDetailList, function (resq) {
                                if (res.qFlnkId === resq.qfinkid) {
                                    currentDetails.push(resq);
                                }
                            })
                        });
                        _.each(currentDetails, function (item) {
                            var count = parseInt(item.allCount) - parseInt(item.noFinishCount);
                            if (count != 0 || item.noFinishCount === 0) {
                                $scope.currentDetailShow.push(item);
                            }
                        });
                        if ($scope.needResetEncrypt === 1 && $scope.isChangeModified || $scope.currentDetailShow.length> 0) {//清除题目配置（批阅人）
                            if(+$scope.classList.MarkMode !== 2) {
                                constantService.confirm('保存试卷', '修改题号清空阅卷配置，修改非选择题分值清空老师批阅结果及记录!' + ($scope.newChangeAnswer.length ? '（修改客观题答案及分数将刷新本题数据）' : ''), ['确定', '取消'], function () {
                                    $scope.savePaper();
                                },function () {
                                    ngDialog.closeAll();
                                });
							} else {
                                $scope.savePaper();
							}
                            // ngDialog.open({
                            //     template: '<div class="batchScore">' + '<div class="batchScore-nav">保存试卷</div>' +
                            //     '<div class="batchScore-title">修改题号清空阅卷配置，修改非选择题分值清空老师批阅结果及记录!</div>' +
                            //     '<div class="batchScore-title" ng-show="!isNeedShow">（请录入所有客观题答案及试卷分值）</div>' +
                            //     '<div class="batchScore-title" ng-show="newChangeAnswer.length > 0">（修改客观题答案及分数将刷新本题数据）</div>' +
                            //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()"  style="margin:20px 20px 0 116px">取消</div>' +
                            //     '<div class="batchScore-btn" ng-click="savePaper()" style="margin-top:20px">确定</div></div>' +
                            //     '</div>',
                            //     className: 'ngdialog-theme-default ngdialog-setValue',
                            //     scope: $scope,
                            //     closeByDocument: false,
                            //     plain: true
                            // });
                        } else {
                            //正常保存，setp维持原状态
                            if($scope.newChangeAnswer.length) {
                                constantService.confirm('保存试卷', '修改客观题答案及分数将刷新本题数据', ['确定', '取消'], function () {
                                    $scope.savePaper();
                                },function () {
                                    ngDialog.closeAll();
                                });
                            } else {
                                $scope.savePaper();
                            }
                            // ngDialog.open({
                            //     template: '<div class="batchScore">' + '<div class="batchScore-nav">保存试卷</div>' +
                            //     '<div class="batchScore-title">核对无误后点击确定保存试卷</div>' +
                            //     '<div class="batchScore-title" ng-show="!isNeedShow">（请录入所有客观题答案及试卷分值）</div>' +
                            //     '<div class="batchScore-title" ng-show="newChangeAnswer.length > 0">（修改客观题答案及分数将刷新本题数据）</div>' +
                            //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()"  style="margin:20px 20px 0 116px">取消</div>' +
                            //     '<div class="batchScore-btn" ng-click="saveNewPager()" style="margin-top:20px">确定</div></div>' +
                            //     '</div>',
                            //     className: 'ngdialog-theme-default ngdialog-setValue',
                            //     scope: $scope,
                            //     closeByDocument: false,
                            //     plain: true
                            // });
                        }
                    } else {
                        $scope.saveNewPager();
                        // ngDialog.open({//正常保存，setp维持原状态
                        //     template: '<div class="batchScore">' + '<div class="batchScore-nav">保存试卷</div>' +
                        //     '<div class="batchScore-title">核对无误后点击确定保存试卷</div>' +
                        //     '<div class="batchScore-title" ng-show="!isNeedShow">（请录入所有客观题答案及试卷分值）</div>' +
                        //     '<div class="batchScore-click"><div class="batchScore-btn1" ng-click="closeThisDialog()"  style="margin:20px 20px 0 116px">取消</div>' +
                        //     '<div class="batchScore-btn" ng-click="saveNewPager()" style="margin-top:20px">确定</div></div>' +
                        //     '</div>',
                        //     className: 'ngdialog-theme-default ngdialog-setValue',
                        //     scope: $scope,
                        //     closeByDocument: false,
                        //     plain: true
                        // });
                    }
                }
            };
            //试卷审核
            function paperCheck() {
                if ($scope.isNeedShow && Checked != 1) {//判断试卷未审核
                    apiCommon.paperCheck({//试卷审核
                        examFLnkId: $scope.paperData.FLnkID
                    })
                }
            }
            //保存接口
            //首次进入，状态跳为2
            $scope.saveNewPager = function () {//普通保存step=2
                $scope.param = [];
                $scope.metaData = [];
                var comScoreOld = 0;
                _.each($scope.groupData, function (res) {
                    _.each(res.question, function (resq) {
                        if (resq.isMixed === 1 && resq.mixedParent === '') {//本题是合并题父题
                            _.each(onOldList,function (val) {
                                if(resq.orders === val.orders){
                                    comScoreOld = val.fullScore
                                }
                                if(val.mixedParent === resq.orders){
                                    comScoreOld = comScoreOld + val.fullScore;
                                }
                            });
                            if(comScoreOld === resq.fullScore){//如果合并后分数有变动，则分数按平均分传，否则按初始值传
                                var oldFullScore =  _.find(onOldList,function (old) {
                                    return resq.orders === old.orders
                                });
                                resq.fullScore = oldFullScore.fullScore;
                            }else{
                                //获取到所有子题
                                var subs = _.filter(res.question, function (item) {
                                    return item.mixedParent === resq.orders;
                                });
                                assignScore(subs, resq);
                            }
                        }
                        if (resq.mixedParent !== '') {
                            var parent = _.find(res.question, function (par) {
                                return par.orders === resq.mixedParent;
                            });
                            resq.showDis = parent === undefined?resq.orders:parent.showDis;
                        }
                        if (resq.isObjective == 1) {
                            var answer = resq.answer.replace(/,/g, '');
                            var saveAnswer = answer.split('').join(',');
                        } else {
                            saveAnswer = resq.answer
                        }
                        $scope.param = {
                            dType: res.dType,
                            qFlnkId: resq.qFlnkId,
                            orders: resq.orders,
                            showDis: resq.showDis,
                            answer: saveAnswer,
                            difficultLevel: resq.difficultLevel,
                            knowledge: resq.knowledgeID,
                            fullScore: resq.fullScore,
                            missScore: resq.missScore || 0
                        };
                        $scope.metaData.push($scope.param);
                    });
                });
                apiCommon.updatePaperConfig({//保存页面修改
                    unifiedId: unifiedId,
                    meta: $scope.metaData,
                    needResetEncrypt: $scope.needResetEncrypt
                }).then(function (res) {
                    if (res.data.code === 0) {
                        apiCommon.updateUnifiedItemStep({//设置子统考
                            unifiedItemId: unifiedId,
                            step: 2
                        }).then(function (re) {
                            if (re.data.code === 0) {
                                paperCheck();
                                constantService.confirm('提示', '试卷保存成功！', ['返回考试中心', '制作答题卡'], function () {
                                    $state.go('myApp.examCenter');
                                },function () {
                                    ngDialog.closeAll();
                                    $state.go('myApp.paperBankSheet', {
                                        ExName: $scope.paperData.ExName,
                                        FID: $scope.paperData.FID,
                                        FLnkID: $scope.paperData.FLnkID,
                                        CreateDate: $scope.paperData.CreateDate,
                                        unifiedId: unifiedId,
                                        action: +$scope.status < 3 ? '1' : '',
                                        MarkMode: $scope.marktype,
                                        examname:$scope.paperData.ExName
                                    });
                                });
                                $scope.isModified = true;
                                $scope.isChangeModified = false;
                                apiCommon.getPaperConfig({
                                    unifiedId: unifiedId
                                }).success(function (data) {
                                    $scope.mainScore = [];
                                    $scope.paperData = data.msg;
                                    $scope.groupData = data.msg.meta;
                                    $scope.totalPoints = 0;
                                    onNewList = [];
                                    var ExamTime = $scope.paperData.ExamTime.split(' ');
                                    $scope.ExamTime = ExamTime[0];
                                    //获取初始值
                                    _.each($scope.groupData,function (g) {
                                        _.each(g.question,function (q) {
                                            onNewList.push(q);
                                        })
                                    });
                                    for (i = 0; i < onNewList.length; i++) {
                                        onOldList[i] = {
                                            showDis: onNewList[i].showDis,
                                            answer: onNewList[i].answer,
                                            difficultLevel: onNewList[i].difficultLevel === '' ? 1 : onNewList[i].difficultLevel,
                                            knowledge: onNewList[i].knowledge === undefined ? '知识点': onNewList[i].knowledge,
                                            fullScore: onNewList[i].fullScore,
                                            missScore: onNewList[i].missScore,
                                            orders: onNewList[i].orders
                                        }
                                    }
                                    _.each($scope.groupData, function (s) {
                                        if (s.question[0].isObjective === 1) {
                                            s.showType = 1;
                                        } else {
                                            if (s.qType === '选择题' || s.qType === '多选题') {
                                                s.showType = 1;
                                                _.each(s.question, function (item) {
                                                    item.answer = item.answer.replace(/[^ABCDEFG]/gi, "");
                                                });
                                            } else {
                                                s.showType = 0;
                                            }
                                        }
                                        $scope.totalPoints = +s.fullScore + $scope.totalPoints;
                                        $scope.mainScore = _.filter(s.question, function (ss) {
                                            return ss.mixedParent === '' && ss.isMixed === 1;
                                        });
                                        _.each(s.question, function (qus) {
                                            qus.missScore = parseInt(qus.missScore);
                                            qus.knowledges = '';
                                            qus.knowledgeID = '';
                                            if (qus.knowledge === undefined) {
                                                qus.knowledges = '知识点';
                                            } else {
                                                qus.knowledges = _.pluck(qus.knowledge, 'name').join(',');
                                                qus.knowledgeID = _.pluck(qus.knowledge, 'id').join(',');
                                            }
                                            _.each($scope.mainScore, function (score) {
                                                if (score.orders === qus.mixedParent) {
                                                    score.fullScore = score.fullScore + qus.fullScore;
                                                }
                                            });
                                        });
                                    });
                                    apiCommon.knowledgeList({
                                            subjectId: $scope.paperData.SubjectId,
                                            pharseId: $scope.paperData.pharseId
                                    }).success(function (res) {
                                        if (!_.isArray(res.msg)) {
                                            $scope.knowledgeList = [];
                                        } else {
                                            $scope.knowledgeList = res.msg;
                                        }
                                    });
                                })
                            } else {
                                constantService.alert(re.data.msg);
                            }
                        });
                    } else {
                        constantService.alert(res.data.msg);
                    }
                });
                ngDialog.closeAll();
            };
            //普通保存，不改变状态
            $scope.savePaper = function () {
                $scope.param = [];
                $scope.metaData = [];
                var comScoreOld = 0;
                _.each($scope.groupData, function (res) {
                    _.each(res.question, function (resq) {
                        if (resq.isMixed === 1 && resq.mixedParent === '') {//本题是合并题父题
                            _.each(onOldList,function (val) {
                                if(resq.orders === val.orders){
                                    comScoreOld = val.fullScore
                                }
                                if(val.mixedParent === resq.orders){
                                    comScoreOld = comScoreOld + val.fullScore;
                                }
                            });
                            if(comScoreOld === resq.fullScore){//如果合并后分数有变动，则分数按平均分传，否则按初始值传
                                var oldFullScore =  _.find(onOldList,function (old) {
                                    return resq.orders === old.orders
                                });
                                resq.fullScore = oldFullScore.fullScore;
                            }else{
                                //获取到所有子题
                                var subs = _.filter(res.question, function (item) {
                                    return item.mixedParent === resq.orders;
                                });
                                assignScore(subs, resq);
                            }
                        }
                        if (resq.mixedParent !== '') {
                            var parent = _.find(res.question, function (par) {
                                return par.orders === resq.mixedParent;
                            });
                            resq.showDis = parent === undefined?resq.orders:parent.showDis;
                        }
                        if (resq.isObjective == 1) {
                            var answer = resq.answer.replace(/,/g, '');
                            var saveAnswer = answer.split('').join(',');
                        } else {
                            saveAnswer = resq.answer
                        }

						$scope.param = {
							dType: res.dType,
							qFlnkId: resq.qFlnkId,
							orders: resq.orders,
							showDis: resq.showDis,
							answer: saveAnswer,
							difficultLevel: resq.difficultLevel,
							knowledge: resq.knowledgeID,
							fullScore: resq.fullScore,
							missScore: resq.missScore || 0
						};
						$scope.metaData.push($scope.param);
					});
				});
				if ($scope.currentDetailShow && $scope.currentDetailShow.length > 0) {
					var newFidList = [];
					_.each($scope.currentDetailShow, function (item) {
						newFidList.push(resetQuestionMarkRecord(item.qfinkid));
					});
					if (newFidList.length > 0) {
						$q.all(newFidList).then(function () {
							saveExam()
						})
					}
				} else {
					saveExam();
				}
			};

			function changeAnswer(data) {
				var defer = $q.defer();
				resetAnswerMarkRecord(data, 0).then(defer.resolve, defer.reject);
				return defer.promise;
			}

			var deferModify = $q.defer();

			function resetAnswerMarkRecord(toModifyList, index) {//客观题更改答案
				var params = toModifyList[index];
				apiCommon.resetAnswerMarkRecord({
					qFlnkId: params.qFlnkId,
					answer: params.answer,
					forceFreshSc: params.forceFreshSc
				}).then(function (res) {
					if (res.data.code === 0) {
						//如果修改结束，resolve
						if (index === toModifyList.length - 1) {
							deferModify.resolve();
						} else {
							//没结束继续修改下一题
							resetAnswerMarkRecord(toModifyList, ++index);
						}
					} else {
						deferModify.reject();
					}
				}, function (res) {
					deferModify.reject();
				});
				return deferModify.promise;
			}

			function saveExam() {
				$scope.isModified = true;
				$scope.isChangeModified = false;
				_.each($scope.newChangeAnswer, function (item) {
					_.each(onOldList, function (items) {
						if ((item.fullScore !== items.fullScore || item.answer !== items.answer) && item.orders === items.orders) {
							item.forceFreshSc = 1;
						}
					})
				});
				if (!!$scope.newChangeAnswer && $scope.newChangeAnswer.length > 0) {
                    apiCommon.updatePaperConfig({//保存页面修改
                        unifiedId: unifiedId,
                        meta: $scope.metaData,
                        needResetEncrypt: $scope.needResetEncrypt
                    }).then(function (res) {
                        if (res.data.code === 0) {
                            changeAnswer($scope.newChangeAnswer).then(function (resq) {
                                paperCheck();
                                if ($scope.classList.ExamNum === null) {
                                    constantService.alert('客观题数据刷新完成!')
                                } else {
                                    constantService.alert('客观题数据刷新完成，已经影响' + $scope.classList.ExamNum + '份答卷！');
                                }
                                getNewPaperConfig();
                            }, function () {
                                constantService.alert('保存答案失败，请重试！')
                            })
                        } else {
                            constantService.alert('保存答案失败，请重试！');
                            getNewPaperConfig();
                        }
                    });
				} else {
					apiCommon.updatePaperConfig({//保存页面修改
						unifiedId: unifiedId,
						meta: $scope.metaData,
						needResetEncrypt: $scope.needResetEncrypt
					}).then(function (res) {
						if (res.data.code === 0) {
							paperCheck();
							constantService.alert('保存成功！');
							getNewPaperConfig();
						} else {
							constantService.alert('保存答案失败，请重试！');
							getNewPaperConfig();
						}
					});
				}
				ngDialog.closeAll();
			}

			function getNewPaperConfig() {
				apiCommon.getPaperConfig({
					unifiedId: unifiedId
				}).success(function (data) {
					$scope.mainScore = [];
					$scope.paperData = data.msg;
					$scope.groupData = data.msg.meta;
					$scope.totalPoints = 0;
					onNewList = [];
					var ExamTime = $scope.paperData.ExamTime.split(' ');
					$scope.ExamTime = ExamTime[0];
					//获取初始值
					_.each($scope.groupData, function (g) {
						_.each(g.question, function (q) {
							onNewList.push(q);
						})
					});
					for (i = 0; i < onNewList.length; i++) {
						onOldList[i] = {
							showDis: onNewList[i].showDis,
							answer: onNewList[i].answer,
							difficultLevel: onNewList[i].difficultLevel === '' ? 1 : onNewList[i].difficultLevel,
							knowledge: onNewList[i].knowledge === undefined ? '知识点' : onNewList[i].knowledge,
							fullScore: onNewList[i].fullScore,
							missScore: onNewList[i].missScore,
							orders: onNewList[i].orders
						}
					}
					_.each($scope.groupData, function (s) {
						if (s.question[0].isObjective === 1) {
							s.showType = 1;
						} else {
							if (s.qType === '选择题' || s.qType === '多选题') {
								s.showType = 1;
								_.each(s.question, function (item) {
									item.answer = item.answer.replace(/[^ABCDEFG]/gi, "");
								});
							} else {
								s.showType = 0;
							}
						}
						$scope.totalPoints = +s.fullScore + $scope.totalPoints;
						$scope.mainScore = _.filter(s.question, function (ss) {
							return ss.mixedParent === '' && ss.isMixed === 1;
						});
						_.each(s.question, function (qus) {
							qus.missScore = parseInt(qus.missScore);
							qus.knowledges = '';
							qus.knowledgeID = '';
							if (qus.knowledge === undefined) {
								qus.knowledges = '知识点';
							} else {
								qus.knowledges = _.pluck(qus.knowledge, 'name').join(',');
								qus.knowledgeID = _.pluck(qus.knowledge, 'id').join(',');
							}
							_.each($scope.mainScore, function (score) {
								if (score.orders === qus.mixedParent) {
									score.fullScore = score.fullScore + qus.fullScore;
								}
							});
						});
					});
					apiCommon.knowledgeList({
							subjectId: $scope.paperData.SubjectId,
							pharseId: $scope.paperData.pharseId
					}).success(function (res) {
						if (!_.isArray(res.msg)) {
							$scope.knowledgeList = [];
						} else {
							$scope.knowledgeList = res.msg;
						}
					});
				})
			}

			//离开本页，如果有修改过提醒
			$scope.isChangeModified = false;
			$scope.$on('$stateChangeStart', function (event, toState) {
				_.each(onNewList, function (item) {
					if (item.isMixed === 1 && item.mixedParent === '') {
						var oldScore = _.find(onOldList, function (score) {
							return item.orders === score.orders
						});
						item.fullScore = oldScore.fullScore;
					}
				});
				for (var i = 0; i < onNewList.length; i++) {
					var oldVal = _.find(onOldList, function (res) {
						return onNewList[i].orders === res.orders
					});
					if (onNewList[i].knowledges === '') {
						onNewList[i].knowledges = '知识点';
					}
					if (oldVal.knowledge === '知识点') {
						oldVal.knowledges = '知识点';
					} else {
						oldVal.knowledges = _.pluck(oldVal.knowledge, 'name').join(',');
					}
					oldVal.answer = oldVal.answer.replace(/[^ABCDEFG]/gi, "");
					onNewList[i].answer = onNewList[i].answer.replace(/[^ABCDEFG]/gi, "");
					if (onNewList[i].showDis != oldVal.showDis || onNewList[i].answer != oldVal.answer || onNewList[i].difficultLevel != oldVal.difficultLevel
						|| onNewList[i].knowledges != oldVal.knowledges || onNewList[i].fullScore != oldVal.fullScore || onNewList[i].missScore != oldVal.missScore) {
						$scope.isChangeModified = true;
						break;
					}
				}
				if (toState.name !== 'setValue' && $scope.isModified) {
					if ($scope.isChangeModified) {
						$scope.isChangeModified = false;
						var isLeave = confirm('确定离开编辑页面？如果尚未保存，已修改的内容将无法恢复。');
						if (!isLeave) {
							event.preventDefault();
						} else {
							window.onbeforeunload = null;
						}
					} else {
						window.onbeforeunload = null;
					}
				} else {
					if ($scope.isChangeModified) {
						$scope.isChangeModified = false;
						var isLeave = confirm('确定离开编辑页面？如果尚未保存，已修改的内容将无法恢复。');
						if (!isLeave) {
							event.preventDefault();
						} else {
							window.onbeforeunload = null;
						}
					} else {
						window.onbeforeunload = null;
					}
				}
			});
			window.onbeforeunload = function () {
				if ($scope.isModified) {
					return true;
				}
			};
			//拆分子题
			// $scope.splitQst = function(question, g){
			//     ngDialog.open({
			//         template : '<div class="batchScore">' + '<div class="batchScore-nav">拆分子题</div>' +
			//         '<div class="batchScore-title">第{{curQst.orders}}题拆分子题:  ' +
			//         '拆成<input type="number" style="width:60px" ng-model="subOrder" ng-change="getSuborder(subOrder)"/>题</div>' +
			//         '<div class="batchScore-click"><div class="batchScore-btn" ng-click="split()" style="margin:20px 20px 0 116px">确定</div>' +
			//         '<div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin-top:20px">取消</div></div>' +
			//         '</div>',
			//         className : 'ngdialog-theme-default ngdialog-setValue',
			//         scope : $scope,
			//         closeByDocument : false,
			//         plain : true
			//     });
			//     $scope.curQst = question;
			//     $scope.getSuborder = function(sub){
			//         $scope.subOrders = sub;
			//     };
			//     $scope.split = function(){
			//         if($scope.subOrders != undefined){
			//             if($scope.subOrders == 1){
			//                 constantService.alert('请正确输入的题目数');
			//             } else {
			//                 $scope.newQuestion = [];
			//                 var avgScores = question.fullScore / $scope.subOrders;
			//                 var ordersList = _.range(1, $scope.subOrders + 1);
			//                 var order = question.orders,
			//                     showDis = question.showDis,
			//                     fullScore = question.fullScore,
			//                     isSub = question.isSub,
			//                     mixedParent = '',
			//                     subParent = question.subParent;
			//                 var newList = [];
			//                 var index = _.findIndex(g.question, function(i){
			//                     return i.orders == question.orders
			//                 });
			//                 _.each(ordersList, function(item){
			//                     if(item === 1){
			//                         question.showDis = question.orders + '.' + 1;
			//                         question.orders = question.orders + '.' + 1;
			//                         question.fullScore = avgScores;
			//                         question.isSub = 1;
			//                         question.splitShow = true;
			//                         question.subParent = question.orders;
			//                     } else {
			//                         var newQuestion = {
			//                             orders : order,
			//                             showDis : showDis,
			//                             fullScore : fullScore,
			//                             isSub : isSub,
			//                             subParent : subParent,
			//                             mixedParent : mixedParent
			//                         };
			//                         newQuestion.showDis = newQuestion.orders + '.' + item;
			//                         newQuestion.orders = newQuestion.showDis;
			//                         newQuestion.subParent = question.subParent;
			//                         newQuestion.fullScore = avgScores;
			//                         newQuestion.isSub = 1;
			//                         newList.push(newQuestion);
			//                     }
			//                 });
			//                 g.question.splice(index + 1, 0, newList);
			//                 var x = [];
			//                 _.each(g.question, function(res){
			//                     if(_.isArray(res)){
			//                         _.each(res, function(resq){
			//                             x.push(resq);
			//                         })
			//                     } else {
			//                         x.push(res);
			//                     }
			//                 });
			//                 g.question = x;
			//                 ngDialog.close();
			//             }
			//         } else {
			//             constantService.alert('请正确输入的题目数');
			//         }
			//     };
			// };
			//取消子题
			// $scope.cancelQst = function(question, g){
			//     ngDialog.open({
			//         template : '<div class="batchScore">' + '<div class="batchScore-nav">取消子题</div>' +
			//         '<div class="batchScore-click"><div class="batchScore-btn" ng-click="cancelQuestion()" style="margin:20px 20px 0 116px">确定</div>' +
			//         '<div class="batchScore-btn1" ng-click="closeThisDialog()" style="margin-top:20px">取消</div></div>' +
			//         '</div>',
			//         className : 'ngdialog-theme-default ngdialog-setValue',
			//         scope : $scope,
			//         closeByDocument : false,
			//         plain : true
			//     });
			//     $scope.cancelQuestion = function(){
			//         var spliceList = [];
			//         var comScore = 0;
			//         var index = _.findIndex(g.question, function(item){
			//             return item.orders == question.orders
			//         });
			//         _.each(g.question, function(res){
			//             if(question.subParent == res.subParent){
			//                 spliceList.push(res);
			//             }
			//         });
			//         _.each(spliceList, function(resq){
			//             comScore = comScore + resq.fullScore;
			//         });
			//         g.question.splice(index + 1, spliceList.length - 1);
			//         var x = g.question[index].showDis.split('.');
			//         g.question[index].showDis = x[0];
			//         g.question[index].orders = x[0];
			//         g.question[index].fullScore = comScore;
			//         g.question[index].splitShow = false;
			//         g.question[index].isSub = 0;
			//         g.question[index].subParent = '';
			//         g.question.sort(function(a, b){
			//             return +a.orders > +b.orders;
			//         });
			//         ngDialog.close();
			//     }
			// };

			function assignScore(subs, parent) {
				var newArray = [parent].concat(subs);
				var score = parent.fullScore;
				var len = newArray.length;
				var step = 0.5;
				if (score >= len) {
					step = 1;
				}
				var list = new Array(len);
				for (var j = 0; j < list.length; j++) {
					list[j] = 0;
				}
				(function doAssign(leftScore) {
					for (var i = 0; i < len; i++) {
						if (leftScore > step) {
							list[i] += step;
							leftScore -= step;
						} else {
							if (leftScore > 0) {
								list[i] += leftScore;
								leftScore = 0
							} else {
								break;
							}

						}
					}
					if (leftScore > 0) {
						doAssign(leftScore);
					}
				})(score);
				_.each(newArray, function (item, index) {
					item.fullScore = list[index];
				});
			}
            //tips
            $scope.moveOver = function () {
                $("[data-toggle='tooltip']").tooltip({
                    html: true
                });
            };
			$scope.textFocus = function ($event) {
                $event.currentTarget.select();
            }
		}]
});