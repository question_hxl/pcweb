//Perforsis
define(["echarts"], function (echarts) {
	return ['$http', '$scope', '$rootScope', '$state', '$location', '$sce', function ($http, $scope, $rootScope, $state, $location, $sce) {
		var user = JSON.parse(sessionStorage.getItem('currentUser'));
        var subjectId = $location.search().subjectId,
            userFlnkid = $location.search().userFlnkid,
        	ExAnswerFlnkID = $location.search().ExAnswerFlnkID,
            ExamFlnkID = $location.search().ExamFlnkID;
		console.log(user);
		var ClassFlnkid = $location.search().ClassFlnkid || user.classs[0].FLnkID;
		var MyName = $location.search().MyName || user.username;
		// $scope.showNoData = !(!!ExamFlnkID && !!ExAnswerFlnkID);
		$scope.username = name || MyName;
		$scope.ClassName = user.classs[0].ClassName;
		$scope.MyName = MyName;

		var usernames = MyName == null ? $scope.MyName : MyName;

		// 获取试卷信息
		$http({
			method: 'post',
			url: $rootScope.baseUrl + '/Interface0177.ashx',
			data: {
				subjectId: subjectId || '',
                userFlnkid: userFlnkid || ''
			}
		}).success(function (res) {
			if(_.isArray(res.msg)) {
				$scope.ExAm = res.msg;
				$scope.currentExam = $scope.ExAm[0];
				if(ExamFlnkID) {
                    var curExam = _.filter($scope.ExAm, function (item) {
						return item.ExamFLnkID === ExamFlnkID;
                    });
                    $scope.currentExam = curExam[0];
				}
				init($scope.currentExam.ExamFLnkID, $scope.currentExam.AFlnkID, ClassFlnkid);
			}else {
				$scope.ExAm = [];
			}
		});
		//试题报告
		$scope.gotoViewAllQuestions = function () {
			$location.path('/UserViewAllQuestions').search({
				ExAnswerFlnkID: $scope.currentExam.AFlnkID,
				ClassFlnkid: ClassFlnkid,
				ExamFlnkID: $scope.currentExam.ExamFLnkID,
				MyName: usernames
			});
			//location.href = "#/?=" +  + "&=" +  + "&=" +  + "&=" + ;
		}
		// 切换试卷
		$scope.selectCur = function (exam, index) {
			//试题报告
			//alert(data);
			$scope.gotoViewAllQuestions = function () {
				//alert(data + "12313");
				$location.path('/UserViewAllQuestions').search({
					ExAnswerFlnkID: exam.AFlnkID,
					ClassFlnkid: ClassFlnkid,
					ExamFlnkID: exam.ExamFLnkID,
					MyName: usernames
				});
				//location.href = "#/?=" +  + "&=" +  + "&=" +  + "&=" + ;
			}
			init(exam.ExamFLnkID, exam.AFlnkID, index);
			$scope.currentExam = exam;
		}
		var init = function (data, AFlnkID, index) {
			$http({ // 试卷其他信息
				method: 'post',
				url: $rootScope.baseUrl + '/Interface0169.ashx',
				data: {
					ExamFlnkID: data,
					ClassFlnkid: ClassFlnkid
				}
			}).success(function (res) {
				if (res.code == "2") {
					$scope.exaninfo = false;
				} else {
					$scope.exaninfo = true;
				}
				$scope.ExamDetail = res.msg[0];
			}).then(function () {
				// 试卷分析概要
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0172.ashx',
					data: {
						ExAnswerFlnkID: AFlnkID
					}
				}).success(function (res) {
					$scope.ExStudent = res.msg[0];
				})
			}).then(function () {
				// 试卷知识点
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0173.ashx',
					data: {
						ExAnswerFlnkID: AFlnkID
					}
				}).success(function (res) {
					if(_.isArray(res.msg) && res.msg.length > 0) {
						$scope.KnowregBox = res.msg;
						var Know = [];
						var total_fScores = 0;
						$.each($scope.KnowregBox, function (item, index) {
							total_fScores += parseFloat(index.LostRate);
							Know.push([index.KnowledgeName, Number(index.LostRate)]);
							//Know.push({
							//	'name': index.KnowledgeName,
							//	'y': Number(index.LostRate)
							//})
						})
						var pie3_other = [];
						if (Know.length > 0) {
							pie3_other.push(['其他', Number((100 - total_fScores).toFixed(1)) < 0 ? 0 : Number((100 - total_fScores).toFixed(1))]);
							//pie3_other.push({
							//	'name': '其他',
							//	'y': Number((100 - total_fScores).toFixed(1)) < 0 ? 0 : Number((100 - total_fScores).toFixed(1))

							//});
						}
						console.log(Know);
						//知识点分布
						var pie3_name = [];
						var pie3_data = [];
						_.each(Know.slice(0,5), function(item){
							pie3_name.push(item[0]);
							pie3_data.push({ 'value': '' + item[1] + '', 'name': '' + item[0] + '' })
						});
						pie3_name.push(pie3_other[0][0]);
						pie3_data.push({ 'value': '' + pie3_other[0][1] + '', 'name': '' + pie3_other[0][0] + '' });
						// 基于准备好的dom，初始化echarts实例
						var myChart = echarts.init(document.getElementById('knows'));
						// 绘制图表
						myChart.setOption({
							tooltip: {
								trigger: 'item',
								formatter: "{a} <br/>{b} : {c}%"
							},
							legend: {
								data: pie3_name
							},
							calculable: true,
							series: [
								{
									name: '丢分知识点分布',
									type: 'funnel',
									left: '10%',
									top: 60,
									//x2: 80,
									//bottom: 60,
									width: '80%',
									// height: {totalHeight} - y - y2,
									min: 0,
									max: 100,
									minSize: '0%',
									maxSize: '100%',
									sort: 'descending',
									gap: 2,
									label: {
										normal: {
											show: true,
											position: 'right'
										},
										emphasis: {
											textStyle: {
												fontSize: 20
											}
										}
									},
									labelLine: {
										normal: {
											length: 10,
											lineStyle: {
												width: 1,
												type: 'solid'
											}
										}
									},
									itemStyle: {
										normal: {
											borderColor: '#fff',
											borderWidth: 1
										}
									},
									data: pie3_data
								}
							]
						});
					}else {
						$scope.KnowregBox = [];
					}
				})
			}).then(function () {
				$http({
					method: 'post',
					url: $rootScope.baseUrl + '/Interface0170.ashx',
					data: {
						ExAnswerFlnkID: AFlnkID,
						type:0
					}
				}).success(function (data) {
					var sub1 = [],
						sub2 = [];
					var soure = data.msg;


					var newdate = _.groupBy(data.msg, 'Dtype');
					var Dtype = _.keys(newdate);
					var questionCon = _.values(newdate)
					var question = _.map(Dtype, function (item, i) {
						var questions = questionCon[i].slice(0, 3);
						var childen = _.map(questions, function (item) {
							//丢失分数 lostsc = 0 没有错题
							if (item.LostSc != 0) {
								return {
									'Serial': $sce.trustAsHtml(item.Serial),
									'Title': $sce.trustAsHtml(item.Title),
									'ClassErrorRate': $sce.trustAsHtml(item.ClassErrorRate + '%'),
									'GradeErrorRate': $sce.trustAsHtml(item.GradeErrorRate + '%'),
									'Source': $sce.trustAsHtml(item.Source),
									'LostSc': item.LostSc,
									'Knowledges': $sce.trustAsHtml(item.Knowledges)

								}
							}
						})

						return {
							'Dtype': item,
							'questionBox': childen
						}
					})

					$scope.questionList = _.sortBy(question, 'LostSc')
					console.log($scope.questionList)
				})
			})
		}
		// 滑动事件
		var $cur = 1;

		var $w = $('.moveBox').width();
		var $showbox = $('.box ul');
		$scope.scrollLeft = function () {
			var $len = $('.box ul li').length;
			var $pages = Math.ceil($len / 5);
			$('.box ul').css('width', $len * 210);
			if ($cur == 1) {
				$showbox.animate({
					left: '-=' + $w * ($pages - 1)
				});
				$cur = $pages;
			} else {
				$showbox.animate({
					left: '+=' + $w
				});
				$cur--;
			}
		}
		$scope.scrollRight = function () {
			var $len = $('.box ul li').length;
			$('.box ul').css('width', $len * 210);
			var $pages = Math.ceil($len / 5);
			if ($cur == $pages) {
				$showbox.animate({
					left: 0
				});
				$cur = 1;
			} else {
				$showbox.animate({
					left: '-=' + $w
				});
				$cur++;
			}


		}
		// tips
		$scope.moveOver = function () {
			$("[data-toggle='tooltip']").tooltip({
				html: true
			});
		}

	}]
})