﻿
//scorereport
define(['jquery.dataTables'], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$location', function ($http, $scope, $rootScope, $state, $location) {
        var searchfrom = $location.search();

        $scope.ClassFlnkID = searchfrom.ClassFlnkID;
        $scope.ExamFlnkID = searchfrom.ExamFlnkID;

        $http({
            method: 'post',
            url: $rootScope.baseUrl + '/Interface0169.ashx',
            data: {
                ClassFlnkID: searchfrom.ClassFlnkID,
                ExamFlnkID: searchfrom.ExamFlnkID
            }
        }).success(function (res) {
            $scope.topinfoList = res.msg;
        });


        $http({
            method: 'post',
            url: $rootScope.baseUrl + '/Interface0176.ashx',
            data: {
                ClassFlnkID: searchfrom.ClassFlnkID,
                ExamFlnkID: searchfrom.ExamFlnkID
            }
        }).success(function (data) {

            $scope.tableDatas = data.msg;

            //$scope.table.fnAddData(tableDatas);var tableDatas = 1;
        });

        //$scope.table = $('#test').dataTable({
        //    "aoColumns": [{
        //        "mDataProp": "Serial",
        //        "sWidth": "8%"
        //    }, {
        //        "mDataProp": "EmpID",
        //        "sWidth": "15%"
        //    }, {
        //        "mDataProp": "Name",
        //        "sWidth": "10%"
        //    }, {
        //        "mDataProp": "Sc",
        //        "sWidth": "8%"
        //    }, {
        //        "mDataProp": "LostQuestionDiss",
        //        "sWidth": "54%",
        //    }, {
        //        "mDataProp": "ExAnswerFlnkID",
        //        "sWidth": "10%"
        //    }],
        //    "oLanguage": {
        //        "sProcessing": "处理中...",
        //        "sLengthMenu": "显示 _MENU_ 项结果",
        //        "sZeroRecords": "没有匹配结果",
        //        "sInfo": "",//显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项
        //        "sInfoEmpty": "",//显示第 0 至 0 项结果，共 0 项
        //        "sInfoFiltered": "",//(由 _MAX_ 项结果过滤)
        //        "sInfoPostFix": "",
        //        "sSearch": "搜索：",
        //        "sUrl": "",
        //        "sEmptyTable": "表中数据为空",
        //        "sLoadingRecords": "载入中...",
        //        "sInfoThousands": ",",
        //        "oPaginate": {
        //            "sFirst": "首页",
        //            "sPrevious": "上页",
        //            "sNext": "下页",
        //            "sLast": "末页"
        //        }
        //    },
        //    "aoColumnDefs": [
		//		{
		//		    "aTargets": [5], "mRender": function (data, type, full) {
		//		        return '<a class="homeviewbtnana" href="#/Perforsis?ExAnswerFlnkID=' + data + '&ClassFlnkid=' + searchfrom.ClassFlnkID + '&ExamFlnkID=' + searchfrom.ExamFlnkID + '&MyName=' + full.Name + '">查看详细</a>';
		//		    }
		//		},
		//		{
		//		    "aTargets": [4], "mRender": function (data, type, full) {
		//		        return '<div class="t_overflow" style="text-align:left" data-toggle="tooltip" data-placement="right"  ng-mouseover="moveOver()" title="' + data + '">' + data + '</div>';
		//		    }
		//		},
		//		{
		//		    "aTargets": [3], "mRender": function (data, type, full) {
		//		        return '<div class="f_red">' + data + '</div>';
		//		    }
		//		},
		//		{
		//		    "aTargets": [2], "mRender": function (data, type, full) {
		//		        return '<div class="t_overflow">' + data + '</div>';
		//		    }
		//		},
		//		{
		//		    "aTargets": [1], "mRender": function (data, type, full) {
		//		        return '<div class="t_overflow" data-toggle="tooltip" data-placement="right"  ng-mouseover="moveOver()" title="' + data + '">' + data + '</div>';
		//		    }
		//		}
        //    ],
        //    "bDestroy": true,//允许重新加载表格对象数据
        //    "bRetrieve": true,
        //    "bProcessing": false,
        //    "bLengthChange": false,
        //    "bPaginate": false,
        //    "bAutoWidth": true,
        //    "bFilter": false,//禁用搜索
        //    "bLengthChange": false,//禁用分页显示
        //});
        //$('#test').dataTable().fnClearTable();

        //表格排序
        $scope.tablethKeys = ['Serial', 'EmpID', 'Name', 'Sc', 'LostQuestionDiss'];
    }]
})