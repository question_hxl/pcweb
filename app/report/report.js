/**
 * Created by 贺小雷 on 2017-07-31.
 */
define(['jquery.datetimepicker'], function(){
	return ['$rootScope', '$scope', '$state', '$q', '$cookieStore', 'bes','ngDialog', 'constantService','$http', 'resourceUrl','$cookies','managerUrl', 'apiCommon',
		function ($rootScope, $scope, $state, $q,$cookieStore, bes, ngDialog, constantService, $http, resourceUrl,$cookies,managerUrl, apiCommon) {
			var user = JSON.parse(sessionStorage.getItem('currentUser'));
			var userClassId;
			if(user.classs && user.classs[0]) {
                userClassId = user.classs[0].FLnkID;
			}
			var cachedList = [];
			function getMyUnifiedList() {
				bes.send('/BootStrap/schoolmanager/getMyUnifieds.ashx',{}, {
					method: 'post'
				}).then(function (res) {
					if (_.isArray(res.data.msg)) {
						var list = res.data.msg;
						$scope.examList = cachedList = _.filter(list, function(item){
							if(item.unitTasks.length === 1) {
								//如果是单科统考，判断是否已提交分析，如果没有则过滤掉
								if(+item.unitTasks[0].status && +item.unitTasks[0].status > 5) {
									item.reportViewable = true;
									return true;
								}else {
									return false;
								}
							}else {
								//如果是多科统考，判断子统考是否有提交分析的，如果没有则过滤掉；如果没有全部提交分析，则不展示统考的查看分析按钮
								var analysisdTask = _.find(item.unitTasks, function(titem){
									return +titem.status && +titem.status > 5;
								});
								var unanalysisdTask = _.find(item.unitTasks, function(titem){
									return !titem.status || +titem.status <= 5;
								});
								if(analysisdTask) {
									item.reportViewable = !unanalysisdTask;
									return true;
								}else {
									return false;
								}
							}
						});
						getGradeListByList($scope.examList);
						getSubjectListByList($scope.examList);
					} else {
						$scope.examList = [];
					}
				}, function () {
					$scope.examList = [];
				});
			}

			getMyUnifiedList();

			$scope.checkReport = function(exam){
				if(exam.unitTasks) {
					//如果是统考，则根据子统考数据判断是否是多科或单科统考
					if(exam.unitTasks.length > 1) {
						//多科统考
						var subjectNames = [];
						var examId = [];
						_.each(exam.unitTasks, function (item) {
							subjectNames.push(item.subjectName);
							examId.push(item.ExamFId);
						});
						examId = examId.join(',');
						var keys = getSelectPaper(subjectNames);
						var period = exam.Period;
						var href = '#/tableManage_View?paperId=' + examId + '&schoolId=' + user.schoolFId + '&period=' + period
							+ '&pharseId=' + user.pharseId + '&nianjiId=' + exam.GradeNum + '&subjectNames=' + (keys.join(',')) +
							'&isOpenNewWin=1&examName=' + encodeURIComponent(exam.unifiedName) + '&unifiedId=' + exam.unifiedId;
						window.open(href);
					}else {
						//单科统考
						exam = exam.unitTasks[0];
						window.open(managerUrl + '/class-grid/index/index_new.html#' + '/?schoolId=' + user.schoolFId + '&paperId=' +
							exam.ExamFId + '&session=' + exam.Period + '&isOpenNewWin=' + 1 + '&isHalf=' + exam.ExamFactor + (userClassId ? '&classId=' + userClassId : ''));
					}
				}else {
					//单科统考
					window.open(managerUrl + '/class-grid/index/index_new.html#' + '/?schoolId=' + user.schoolFId + '&paperId=' +
						exam.ExamFId + '&session=' + exam.Period + '&isOpenNewWin=' + 1 + '&isHalf=' + exam.ExamFactor + (userClassId ? '&classId=' + userClassId : ''));
				}
			};

			$scope.unifiedReportSetting = function(exam){
				$http.get(resourceUrl + '/KmUnified/GetSingleBaseSchool?kmUnifiedFLnkID=' + exam.unifiedId).then(function(res){
					if(res.data.Flag) {
						$scope.examRateList = _.map((res.data.ResultObj.BedRate || ',,,').split(','), function(item){
							return {val: +item || 0}
						});
                        $scope.examRateList.length = 4;
                        constantService.modal({
                            title: '分层统计分段比例设置',
                            scope: $scope,
                            template: '<div>' +
                            '<div style="padding: 10px;">' +
                            '            <label>分层统计比例设置：</label>' +
                            '            <div class="red">(说明：正数N表示前N%学生，负数-N表示后N%学生)</div>' +
                            '        </div><div>' +
                            '        <div style="display: inline-block;margin-left: 10px;" ng-repeat="checkRate in examRateList track by $index">' +
                            '            <span>比例{{$index + 1}}：</span><input ng-model="checkRate.val" type="number" max="100" style="width: 50px;text-align: center;">%' +
                            '        </div></div>' +
                            '</div>',
                            type: 'confirm',
                            cancelText: '取消',
                            okText: '确定',
                            okFn: function(){
                                this.doSettingUnified(exam);
                            }
                        });
					}else {
						constantService.alert('获取统考配置失败,请重试！');
					}
				});
			};

			$scope.doSettingUnified = function(exam){
				var rate = this.examRateList;
                $http.post(resourceUrl + '/KmUnified/UpdateKmUnifiedRates?kmUnifiedFLnkID='+exam.unifiedId+'&BedRate=' + _.pluck(rate, 'val').join(',')).then(function(res){
                	if(res.data.Flag) {
                		ngDialog.close();
					}else {
                		constantService.alert('修改失败，请重试！');
					}
				});
			};

			$scope.filterExam = function(){
				var gradeFilter = $scope.currentGrade && ($scope.currentGrade + '') || '',
					subjectFilter = $scope.currentSubject && ($scope.currentSubject + '') || '',
					paperName = $scope.paperName || '';
				$scope.examList = _.filter(cachedList, function(item){
					var isSubjectExist = _.find(item.unitTasks, function(t){
						return subjectFilter ? ((t.SubjectId + '') === subjectFilter) : true;
					});
					var isNameExist = _.find(item.unitTasks, function(t){
						return paperName ? t.Name.indexOf(paperName) >= 0  : true;
					});
					return (gradeFilter ? ((item.GradeNum + '') === gradeFilter) : true) && !!isSubjectExist && (!!isNameExist || item.unifiedName.indexOf(paperName) >= 0);
				});
			};

			$http.get(resourceUrl + '/BaseSchool/GetSingleBaseSchool?schoolFLnkID=' + user.schoolFId).then(function(res){
				if(res.data.Flag) {
                    $scope.goodRate = +res.data.ResultObj.OddsRate;
                    $scope.checkRateList = _.map(res.data.ResultObj.BedRate.split(','), function(item){
                        return {
                            val: +item || 0
                        }
                    });
				}
			});

			$scope.reportSetting = function(){
				constantService.modal({
                    title: '报表统一设置',
                    scope: $scope,
                    template: '<div>' +
					'<div style="padding: 10px;"><label>优分率：<input type="number" min="0" max="100" ng-model="goodRate" style="text-align: center">%</label></div>' +
					'<div style="padding: 10px;">' +
                    '            <label>分层统计比例设置：</label>' +
                    '            <div class="red">(说明：正数N表示前N%学生，负数-N表示后N%学生)</div>' +
                    '        </div><div>' +
                    '        <div style="display: inline-block;margin-left: 10px;" ng-repeat="checkRate in checkRateList track by $index">' +
                    '            <span>比例{{$index + 1}}：</span><input ng-model="checkRate.val" type="number" max="100" style="width: 50px;text-align: center;">%' +
                    '        </div></div>' +
					'</div>',
                    type: 'confirm',
                    cancelText: '取消',
                    okText: '确定',
                    okFn: function(){
                    	this.doSetting();
					}
				});
			};

			$scope.doSetting = function(){
				$scope.goodRate = this.goodRate;
				var self = this;
				$http.post(resourceUrl + '/BaseSchool/UpdateBaseSchoolRates?schoolFLnkID='+user.schoolFId + '&OddsRate='
					+ this.goodRate + '&BedRate=' + _.pluck(this.checkRateList, 'val').join(',')).then(function(res){
					if(res.data.Flag) {
						$scope.checkRateList = self.checkRateList;
					}else {
						constantService.alert('修改失败！');
					}
				});
				ngDialog.close();
			};

			function getSelectPaper(names) {
				var klys = [];
				var subjectNoMap = {
					'语文': 'A',
					'数学': 'B',
					'英语': 'C',
					'历史': 'D',
					'地理': 'E',
					'政治': 'F',
					'生物': 'G',
					'物理': 'H',
					'化学': 'I',
					'信息技术': 'J',
					'科学': 'K',
                    '思社': 'L',
                    '英语复习': 'M'
				};
				_.each(names, function (item) {
					klys.push(subjectNoMap[item]);
				});
				return klys;
			}

			function getGradeListByList(list){
				var grades = {};
				_.each(list, function(item){
					grades[item.GradeNum] = item.gradeName;
				});
				var gradeList = _.map(grades, function(val, key){
					return {
						gradeNum: key,
						gradeName: val
					};
				});
				gradeList.sort(function(a,b){
					return +a.gradeNum - +b.gradeNum;
				});
				$scope.gradeList = gradeList;
			}

			function getSubjectListByList(list){
				var subjects = {};
				_.each(list, function(item){
					_.each(item.unitTasks, function(task){
						subjects[task.SubjectId] = task.subjectName;
					});
				});
				$scope.subjectList = _.map(subjects, function(val, key){
					return {
						subjectName: val,
						subjectId: key
					};
				});
			}
		}];
});