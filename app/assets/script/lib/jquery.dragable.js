/**
 * Created by Hexl on 2016/3/27.
 */
(function($){
    $.fn.dragable = function(option){
        var defaults = {
            ele: '.drag',
            container: '.box',
            dragstartCb: $.noop,
            ondragCb: $.noop,
            dragendCb: $.noop
        };
        $.extend(defaults, option);
        /**
         * flags
         * @type {*|HTMLElement}
         */
        var isMouseDown = false;
        var $curEle = null, $cloneEle = null;
        var sy = 0, my = 0;
        var $parent = $(this),
            $containers = $parent.find(defaults.container),
            $eleBox,
            $allEles;
        var eleBoxTop;
        var otArray = [], len, targetIndex, maxOffsetTop;
        var windowHeight = $(window).height(), pageHeight;
        var scrollPageInterval,
            deferId;
        var dataToTransfer = {};
        // to check if is mouse click, set variable to check if mouse moved
        var downY, moveY, isClick = true;
        $containers.css({
                position: 'relative'
            });
        $(document).on('mouseup.stopMouseDown', function(e){
            // isMouseDown = false;
            isClick = true;
            clearTimeout(deferId);
        });
        $parent.delegate(defaults.ele, 'mousedown', function(e){
            downY = e.clientY;
            isMouseDown = true;
            isClick = false;
            clearTimeout(deferId);
            deferId = setTimeout(function(){
                if(!isClick) {
                    e.preventDefault();
                    clearInterval(scrollPageInterval);
                    sy = e.pageY;
                    $curEle = $(e.currentTarget);
                    $eleBox = $curEle.parents(defaults.container);
                    $eleBox.css({
                        position: 'relative'
                    });
                    $allEles = $eleBox.find(defaults.ele);
                    otArray = [];
                    $allEles.each(function(){
                        otArray.push($(this).offset().top);
                    });
                    len = otArray.length;
                    eleBoxTop = $eleBox.offset().top;
                    maxOffsetTop = eleBoxTop + $eleBox.height();
                    pageHeight = $(document).height();
                    $cloneEle = $curEle.clone();
                    $cloneEle.appendTo($eleBox).addClass('drag-clone').css({
                        'position': 'absolute',
                        'left': 0,
                        top: sy - eleBoxTop - 20 + 'px',
                        'opacity': '0.8',
                        'z-index': '10',
                        'border': '1px solid #83DAC9'
                    });
                    defaults.dragstartCb(e, dataToTransfer);
                }

                $(document).off('mousemove').on('mousemove', function(e){
                    if(isMouseDown) {
                        moveY = e.clientY;
                        var os = moveY - downY;
                        isClick = os === 0;
                        if(!isClick && $cloneEle) {
                            my = e.pageY;
                            var oy = my - sy;
                            $cloneEle.css({
                                top: my - eleBoxTop - 20 + 'px'
                            });
                            var clientY = e.clientY,
                                currentTop,
                                scrollOffset;
                            if(windowHeight - clientY < 100 && my < pageHeight) {
                                clearInterval(scrollPageInterval);
                                scrollOffset = 0;
                                currentTop = $(window).scrollTop();
                                scrollPageInterval = setInterval(function(){
                                    scrollOffset += 5;
                                    if($(window).scrollTop() + windowHeight < pageHeight) {
                                        $(window).scrollTop(currentTop + scrollOffset);
                                        $cloneEle.css({
                                            top: my - eleBoxTop + scrollOffset + 'px'
                                        });
                                    }
                                }, 10);
                            }else if(clientY < 50) {
                                clearInterval(scrollPageInterval);
                                scrollOffset = 0;
                                currentTop = $(window).scrollTop();
                                scrollPageInterval = setInterval(function(){
                                    scrollOffset -= 5;
                                    if($(window).scrollTop() > 0) {
                                        $(window).scrollTop(currentTop + scrollOffset);
                                        $cloneEle.css({
                                            top: my - eleBoxTop + scrollOffset + 'px',
                                            cursor: 'move'
                                        });
                                    }
                                }, 10);
                            }else {
                                clearInterval(scrollPageInterval);
                            }
                            defaults.ondragCb(e, dataToTransfer);
                        }
                    }
                });

                $(document).off('mouseup.onDrag').on('mouseup.onDrag', function(e){
                    clearInterval(scrollPageInterval);
                    if(isMouseDown) {
                        var upY = e.clientY;
                        isClick = upY - downY === 0;
                        if(!isClick && $cloneEle) {
                            targetIndex = -1;
                            var offsetTop = $cloneEle.offset().top;
                            for(var i = 0; i< len; i++) {
                                if(i !== len - 1) {
                                    if(offsetTop > otArray[i] && offsetTop < otArray[i + 1]){
                                        targetIndex = i;
                                    }
                                }else {
                                    if(otArray[i] < offsetTop && offsetTop < maxOffsetTop) {
                                        targetIndex = i;
                                    }
                                }
                            }
                            var targetEle = $($allEles[targetIndex]);
                            //$curEle.insertAfter(targetEle);
                            $curEle.css({
                                top: 0,
                                'z-index': 0
                            });
                            if(!_.isEmpty(targetEle)) {
                                defaults.dragendCb(e, targetEle);
                            }
                            dataToTransfer = {};
                        }
                    }
                    $cloneEle && $cloneEle.remove();
                    $('.drag-clone').remove();
                    isMouseDown = false;
                });
            }, 200);
        });

        return {
            setDataTransfer: function(key, val){
                if(typeof dataToTransfer !== 'object') {
                    dataToTransfer = {};
                }
                dataToTransfer[key] = val;
            },
            getDataTransfer: function(key){
                return dataToTransfer[key];
            }
        }
    }
})(jQuery);