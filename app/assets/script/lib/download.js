 
var Downer = (function(files){
    var h5Down = !/Trident|MSIE/.test(navigator.userAgent);
    // try{
    // 	h5Down = document.createElement("a").hasOwnProperty("download");
    // } catch(e){
    // 	h5Down = document.createElement("a").download;
    // }
 
    function downloadFile(fileName, contentOrPath){
        var aLink = document.createElement("a"),
            evt = document.createEvent("HTMLEvents"),
            isData = contentOrPath.slice(0, 5) === "data:",
            isPath = contentOrPath.lastIndexOf(".") > -1;

 
        evt.initEvent("click",false,false);

    
        aLink.download = fileName;
 
        aLink.href = isPath || isData ? contentOrPath
            : URL.createObjectURL(new Blob([contentOrPath]));

        aLink.dispatchEvent(evt);
    }

    /**
     * [IEdownloadFile description]
     * @param  {String} fileName
     * @param  {String|FileObject} contentOrPath
     */
    function IEdownloadFile(fileName, contentOrPath, bool){
        var isImg = contentOrPath.slice(0, 10) === "data:image",
            ifr = document.createElement('iframe');

        ifr.style.display = 'none';
        ifr.src = contentOrPath;

        document.body.appendChild(ifr);

 
        isImg && ifr.contentWindow.document.write("<img src='" +
            contentOrPath + "' />");

    
        // alert(ifr.contentWindow.document.body.innerHTML)
        if(bool){
            ifr.contentWindow.document.execCommand('SaveAs', false, fileName);
            document.body.removeChild(ifr);
        } else {
            setTimeout(function(){
                ifr.contentWindow.document.execCommand('SaveAs', false, fileName);
                document.body.removeChild(ifr);
            }, 0);
        }
    }

    /**
     * [parseURL description]
     * @param  {String} str [description]
     * @return {String}     [description]
     */
    function parseURL(str){
        return str.lastIndexOf("/") > -1 ? str.slice(str.lastIndexOf("/") + 1) : str;
    }

    return function(files){
   
        var downer = h5Down ? downloadFile : IEdownloadFile;
 
        if(files instanceof Array) {
            for(var i = 0, l = files.length; i < l ; i++)
                // bug 
                downer(parseURL(files[i]), files[i], true);
        } else if(typeof files === "string") {
            downer(parseURL(files), files);
        } else {
          
            for(var file in files) downer(file, files[file]);
        }
    }

})();
