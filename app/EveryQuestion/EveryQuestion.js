//EveryQuestionCtrl
define(["underscore-min", "star-rating"], function () {
    return ['$http', '$scope', '$rootScope', '$state', '$sce', 'constantService', 'ngDialog', '$location', 'paperService',
        function ($http, $scope, $rootScope, $state, $sce, constantService, ngDialog, $location, paperService) {

        var searchfrom = $location.search();
        var ClassFlnkID = $scope.ClassFlnkID = searchfrom.ClassFlnkID;
        var ExamFlnkID = $scope.ExamFlnkID = searchfrom.ExamFlnkID;
        //var inum = 0;
        $scope.inum = 0;
        $scope.skipQst = null;

        $scope.mousemove = true;
        //加载切图位置列表
        $http.post($rootScope.baseUrl + '/Interface0251A.ashx', {
            examFId: ExamFlnkID,
            classFId: ClassFlnkID
        }).then(function (resp) {
            if (!_.isEmpty(resp.data.msg) && _.isArray(resp.data.msg)) {
                $scope.cutDetail = resp.data.msg[0];
                if ($scope.cutDetail.position) {
                    $scope.position = JSON.parse($scope.cutDetail.position);
                } else {
                    $scope.position = null;
                }
            } else {
                $scope.cutDetail = null;
                $scope.position = null;
            }
        });
        //获取题目
        var sub2List;
        paperService.getPaperDataById(ExamFlnkID).then(function(res){
            var paperMeta = res.data.msg;
            $http.post($rootScope.baseUrl + '/Interface0161B.ashx', {
                ClassFlnkid: ClassFlnkID,
                ExamFlnkID: ExamFlnkID,
                Type: 0
            }).then(function(resp){
                var errorQuestions = [], questionExamList;
                if(resp.data.code === 0) {
                    var allErrors = _.filter(resp.data.msg, function(item){
                        return +item.clostrate > 0;
                    });
                    _.each(allErrors, function(errorItem){
                        var group = _.find(paperMeta, function(g){
                            return g.Dtype === errorItem.dtype;
                        });
                        var tempG = _.find(errorQuestions, function(item){
                            return item.Dtype === group.Dtype;
                        });
                        if(!tempG) {
                            tempG = {Dtype: errorItem.dtype, question: []};
                            errorQuestions.push(tempG);
                        }
                        var qst = _.find(group.question, function(q){
                            if(q.isMain && q.sub.length){
                                var isSub = false;
                                _.each(q.sub, function (sub) {
                                    if(sub.QFLnkID === errorItem.qflnkid || sub.FLnkID === errorItem.qflnkid) {
                                        isSub = true;
                                        if(sub.Mode === 'a' || sub.Mode === 'A') {
                                            errorItem.Serial = sub.orders ? sub.orders : sub.dis;
                                        } else {
                                            errorItem.Serial = sub.dis ? sub.dis : (sub.orders + '.' + sub.SubOrder);
                                        }
                                        errorItem.Score = sub.score;
                                        errorItem.Parse = sub.analysis;
                                        errorItem.Diff = sub.DifficultLevel;
                                        errorItem.startDiff = viewStar(parseInt(sub.DifficultLevel));
                                        errorItem.Answer = sub.Answer;
                                        errorItem.SceTitle = sub.title;
                                        errorItem.Knowledges = sub.knowledges;
                                        errorItem.Option_a = sub.OptionOne;
                                        errorItem.Option_b = sub.OptionTwo;
                                        errorItem.Option_c = sub.OptionThree;
                                        errorItem.Option_d = sub.OptionFour;
                                    }
                                });
                                return isSub;
                            } else {
                                if(q.QFLnkID === errorItem.qflnkid) {
                                    errorItem.Serial = q.orders;
                                    errorItem.Score = q.score;
                                    errorItem.Parse = q.analysis;
                                    errorItem.Diff = q.DifficultLevel;
                                    errorItem.startDiff = viewStar(parseInt(q.DifficultLevel));
                                    errorItem.Answer = q.Answer;
                                    errorItem.SceTitle = q.title;
                                    errorItem.Knowledges = q.knowledges;
                                    errorItem.Option_a = q.OptionOne;
                                    errorItem.Option_b = q.OptionTwo;
                                    errorItem.Option_c = q.OptionThree;
                                    errorItem.Option_d = q.OptionFour;
                                }
                                return q.QFLnkID === errorItem.qflnkid
                            }
                        });
                        if(qst) {
                            errorItem.ClassRightNum = +errorItem.totalnum - +errorItem.lostnum;
                            errorItem.ClassErrorNum = errorItem.lostnum;
                            errorItem.ClassErrorRate = errorItem.clostrate.toFixed(1);
                            errorItem.GradeErrorRate = errorItem.glostrate.toFixed(1);
                            errorItem.QFlnkID = errorItem.qflnkid;
                            errorItem.Source = qst.source || res.data.examName;
                            qst && tempG.question.push(errorItem);
                            if (qst.isMain && qst.sub.length) {
                                errorItem.PTitle = qst.title;
                            }
                        }
                    });
                }
                questionExamList = errorQuestions;
                $scope.sub1List = [];
                _.each(questionExamList, function (item, index) {
                    if(index === 0) {
                        $scope.sub1List = item.question;
                    } else {
                        $scope.sub1List = $scope.sub1List.concat(item.question);
                    }
                });
                $scope.sub1List = _.sortBy($scope.sub1List, 'Serial');
                $scope.skipQst = $scope.sub1List[0];
                sub2List = $scope.sub2List = [];
                $scope.sub2List = $scope.sub1List[+$scope.inum];
                $scope.nowqueplance = +$scope.inum;
            })
        });

        // $http({
        //     method: 'post',
        //     url: $rootScope.baseUrl + '/Interface0161.ashx',
        //     data: {
        //         ClassFlnkID: ClassFlnkID,
        //         ExamFlnkID: ExamFlnkID,
        //         Type: 0
        //     }
        // }).success(function (data) {
        //     var allques = $scope.allques = data.msg.length;
        //     $scope.allques = data.msg;
        //     //$scope.allquenum = data.msg.length;
        //     var sub1 = [];
        //     var useranswer = '';
        //     for (var i = 0; i < data.msg.length; i++) {
        //         if (data.msg[i].UserAnswer.indexOf("OCR/ocrftp/") >= 0) {
        //             useranswer = '<img src=' + data.msg[i].UserAnswer + '>';
        //         }
        //         else {
        //             useranswer = data.msg[i].UserAnswer;
        //         }
        //         sub1.push({
        //             'Serial': +data.msg[i].Serial,
        //             'Title': data.msg[i].Title,
        //             'SceTitle': data.msg[i].Title,
        //             'Diff': data.msg[i].Diff,
        //             'Score': data.msg[i].Score,
        //             'LostSc': data.msg[i].LostSc,
        //             'Qtype': data.msg[i].Qtype,
        //             'ClassErrorRate': data.msg[i].ClassErrorRate,
        //             'Parse': data.msg[i].Parse,
        //             'UserAnswer': useranswer,
        //             'ClassRightNum': data.msg[i].ClassRightNum,
        //             'ClassErrorNum': data.msg[i].ClassErrorNum,
        //             'GradeErrorRate': data.msg[i].GradeErrorRate,
        //             'Source': data.msg[i].Source,
        //             'Option_a': data.msg[i].Option_a,
        //             'Option_b': data.msg[i].Option_b,
        //             'Option_c': data.msg[i].Option_c,
        //             'Option_d': data.msg[i].Option_d,
        //             'Knowledges': data.msg[i].Knowledges,
        //             'QFlnkID': data.msg[i].QFlnkID,
        //             'Answer': data.msg[i].Answer,
        //             'SceAnswer': data.msg[i].Answer,
        //             'startDiff': viewStar(data.msg[i].Diff),
        //             'ShowType': data.msg[i].ShowType,
        //             'Option': data.msg[i].Option_a + data.msg[i].Option_b + data.msg[i].Option_c + data.msg[i].Option_d,
        //             PTitle: data.msg[i].PTitle
        //         });
        //     }
        //     sub1List = $scope.sub1List = _.sortBy(sub1, 'Serial');
        //     $scope.skipQst = $scope.sub1List[0];
        //     sub2List = $scope.sub2List = [];
        //     $scope.sub2List = sub1List[+$scope.inum];
        //     console.log(+$scope.inum);
        //     $scope.nowqueplance = +$scope.inum;
        // });

        //下一题
        $scope.next = function () {
            if (+$scope.inum == $scope.sub1List.length - 1) {
                constantService.alert('到最后了！');
                return false;
            }
            $scope.inum = +$scope.inum + 1;
            $scope.nowqueplance = +$scope.inum;
            $scope.sub2List = $scope.sub1List[+$scope.inum];
            $scope.skipQst = $scope.sub1List[+$scope.inum];
        };

        //上一题
        $scope.prev = function () {
            if (+$scope.inum <= 0) {
                sub2List = $scope.sub1List[0];
                constantService.alert('到头了！');
                return false;
            }
            sub2List = $scope.sub1List[+$scope.inum];
            $scope.inum = +$scope.inum - 1;
            $scope.nowqueplance = +$scope.inum;
            $scope.sub2List = $scope.sub1List[+$scope.inum];
            $scope.skipQst = $scope.sub1List[+$scope.inum];
        };
        //select跳题
        $scope.skip2Select = function () {
            $scope.inum = _.findIndex($scope.sub1List, function (item) {
                return item === $scope.skipQst;
            });
            $scope.nowqueplance = +$scope.inum;
            $scope.sub2List = $scope.sub1List[+$scope.inum];
        }
        //查看解析
        $scope.viewAna = function (values) {
            if (values == "") {
                $scope.viewAnapage = '<div>暂无数据</div>';
            } else {
                $scope.viewAnapage = values
            }
            ngDialog.open({
                template: 'template/viewAna.html',
                className: 'ngdialog-theme-default',
                appendClassName: 'ngdialog-theme-zxz',
                scope: $scope
            })
        };

        //同题测试
        $scope.sameTest = function (QFlnkID, Diff) {
            $scope.checkshowanswer = false;
            $scope.currentDiff = Diff;
            $scope.currentQFlnkID = QFlnkID;
            $http.post($rootScope.baseUrl + '/Interface0185.ashx', {
                QFlnkID: QFlnkID,
                Diff: Diff,
                ExamFlnkID: ExamFlnkID
            }).success(function (data) {
                if (data.code != '3') {
                    var samequestionList = [];
                    for (var i = 0; i < data.msg.length; i++) {
                        samequestionList.push({
                            'Answer': $sce.trustAsHtml(data.msg[i].Answer),
                            'Diff': data.msg[i].Diff,
                            'Knowledges': data.msg[i].Knowledges,
                            'Option_a': $sce.trustAsHtml(data.msg[i].Option_a),
                            'Option_b': $sce.trustAsHtml(data.msg[i].Option_b),
                            'Option_c': $sce.trustAsHtml(data.msg[i].Option_c),
                            'Option_d': $sce.trustAsHtml(data.msg[i].Option_d),
                            'Qtpye': data.msg[i].Qtpye,
                            'Title': $sce.trustAsHtml(data.msg[i].Title)
                        })
                    }
                    $scope.samequestionList = samequestionList;

                    ngDialog.open({
                        template: 'template/everySameTest.html',
                        className: 'ngdialog-theme-default',
                        appendClassName: 'ngdialog-theme-zxz',
                        scope: $scope,
                        preCloseCallback: function () { //回调

                        }
                    });
                    $scope.$on('ngDialog.templateLoaded', function () {
                        setTimeout(function () {
                            $('.dialog-start').raty({ // 等级
                                score: function () {
                                    return '' + $scope.currentDiff;
                                },
                                path: function () {
                                    return this.getAttribute('data-path');
                                },
                                click: function (score, evt) {
                                    if ($scope.currentDiff !== score) {
                                        $scope.currentDiff = '' + score;
                                        $http.post($rootScope.baseUrl + '/Interface0185.ashx', {
                                            QFlnkID: $scope.currentQFlnkID,
                                            Diff: $scope.currentDiff,
                                            ExamFlnkID: ExamFlnkID
                                        }).success(function (data) {
                                            if (data.code == '3') {
                                                $scope.samenodata = true;
                                                $scope.samequestionList = '';
                                                return false;
                                            } else {
                                                $scope.samenodata = false;

                                                var samequestionList = [];
                                                for (var i = 0; i < data.msg.length; i++) {
                                                    samequestionList.push({
                                                        'Answer': $sce.trustAsHtml(data.msg[i].Answer),
                                                        'Diff': data.msg[i].Diff,
                                                        'Knowledges': data.msg[i].Knowledges,
                                                        'Option_a': $sce.trustAsHtml(data.msg[i].Option_a),
                                                        'Option_b': $sce.trustAsHtml(data.msg[i].Option_b),
                                                        'Option_c': $sce.trustAsHtml(data.msg[i].Option_c),
                                                        'Option_d': $sce.trustAsHtml(data.msg[i].Option_d),
                                                        'Qtpye': data.msg[i].Qtpye,
                                                        'Title': $sce.trustAsHtml(data.msg[i].Title)
                                                    })
                                                }
                                                $scope.samequestionList = samequestionList;
                                            }
                                        });
                                    }
                                }
                            });
                        }, 100);
                    });
                } else {
                    ngDialog.open({
                        template: '<p>' + data.msg + '</p>' +
                        '<div class="ngdialog-buttons">' +
                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog()">确定</button></div>',
                        plain: true,
                        className: 'ngdialog-theme-default'
                    });
                    return false;
                }
            });
        };

        //查看答题情况
        $scope.viewAnswerState = function (QFlnkID) {

            $scope.answerArea = '<div>&nbsp;</div>';
            $http.post($rootScope.baseUrl + '/Interface0186A.ashx', {
                ClassFlnkID: ClassFlnkID,
                ExamFlnkID: ExamFlnkID,
                QFlnkID: QFlnkID
            }).success(function (data) {
                var answerAnalysisList = $scope.answerAnalysisList = data.msg;

                var analysisListChoseRight = [];
                var analysisListChoseWrong = [];
                for (var i = 0; i < answerAnalysisList.length; i++) {
                    if (+answerAnalysisList[i].fIsRight == 1) {
                        analysisListChoseRight.push({
                            'Accessory': answerAnalysisList[i].Accessory,
                            'Answer': answerAnalysisList[i].Answer,
                            'IsObjective': answerAnalysisList[i].IsObjective,//0非选择，1选择
                            'UserAnswer': answerAnalysisList[i].UserAnswer,
                            'UserName': answerAnalysisList[i].UserName,
                            'fIsRight': answerAnalysisList[i].fIsRight,
                            'fScores': answerAnalysisList[i].fScores,
                            'Image': answerAnalysisList[i].Image ? answerAnalysisList[i].Image : ''
                        })
                    }

                    if (+answerAnalysisList[i].fIsRight < 1) {
                        analysisListChoseWrong.push({
                            'Accessory': answerAnalysisList[i].Accessory,
                            'Answer': answerAnalysisList[i].Answer,
                            'IsObjective': answerAnalysisList[i].IsObjective,//0非选择，1选择
                            'UserAnswer': answerAnalysisList[i].UserAnswer,
                            'UserName': answerAnalysisList[i].UserName,
                            'fIsRight': answerAnalysisList[i].fIsRight,
                            'fScores': answerAnalysisList[i].fScores,
                            'Image': answerAnalysisList[i].Image ? answerAnalysisList[i].Image : ''
                        });
                        $scope.order = "fScores"
                    }
                }
                $scope.analysisListChoseRight = analysisListChoseRight;
                $scope.analysisListChoseWrong = analysisListChoseWrong;
                $scope.rightren = analysisListChoseRight.length;
                $scope.wrongren = analysisListChoseWrong.length;

                $scope.stuListA = _.filter(answerAnalysisList, function (item) {
                    return $.trim(item.UserAnswer).indexOf('A') >= 0;
                });
                $scope.stuListB = _.filter(answerAnalysisList, function (item) {
                    return $.trim(item.UserAnswer).indexOf('B') >= 0;
                });
                $scope.stuListC = _.filter(answerAnalysisList, function (item) {
                    return $.trim(item.UserAnswer).indexOf('C') >= 0;
                });
                $scope.stuListD = _.filter(answerAnalysisList, function (item) {
                    return $.trim(item.UserAnswer).indexOf('D') >= 0;
                });
            });
            ngDialog.open({
                template: 'template/answerAnalysisEvery.html',
                className: 'ngdialog-theme-default',
                appendClassName: 'ngdialog-theme-zxz',
                scope: $scope,
                preCloseCallback: function () { //回调

                }
            })
        };

        //显示答案
        $scope.showanswer = function (analysisstuW) {
            $scope.toShowAnswerStudent = analysisstuW;
            $scope.ImageCanLoad = false;
            $('.ocr_cut').css('display', (!!$scope.toShowAnswerStudent && (!$scope.currentPos || $scope.currentPos.length === 0)) ? 'block' : 'none');
            $('<img>').attr('src', analysisstuW.Image).on('error', function () {
                $scope.$apply(function () {
                    $scope.ImageCanLoad = false;
                });
            }).on('load', function () {
                $scope.$apply(function () {
                    $scope.ImageCanLoad = true;
                });
            });
            if (analysisstuW.Accessory) {
                $scope.answerArea = '<img src=' + analysisstuW.Accessory + '>';
            } else if(analysisstuW.UserAnswer){
                $scope.answerArea = '<div class="nopicdata"> '+ analysisstuW.UserAnswer + '</div>';
            } else {
                $scope.answerArea = '<div class="nopicdata">暂无数据</div>';
            }
        };
        $scope.hideAnswer = function (analysisstuW) {
            $scope.toShowAnswerStudent = null;
        };

        $scope.$watch('sub2List', function () {
            if (!_.isEmpty($scope.sub2List) && $scope.position) {
                var currentCut = _.find($scope.position, function (p) {
                    return (p.order + '' === $scope.sub2List.Serial + '') || (p.order + '' === ($scope.sub2List.Serial + '').split('.')[0]);
                });
                if (currentCut) {
                    $scope.currentPos = currentCut.position;
                } else {
                    $scope.currentPos = [];
                }
            } else {
                $scope.currentPos = [];
            }
            setTimeout(function () {
                $('.EveryQuestion .everyBottom img').each(function (index,item) {
                    var $ele = $(this);
                    var $eleNoChosean = _.find($('.EveryQuestion .everyBottom .nochoseanswer img'),function (h) {
                        return h === item;
                    });
                    if(!!$eleNoChosean){
                        var viewHeight = $('.EveryQuestion .everyBottom .nochoseanswer')[0].clientHeight;
                        var viewWidth = $('.EveryQuestion .everyBottom .nochoseanswer')[0].clientWidth;
                        if($ele[0].complete){
                            if(($ele.width()>viewWidth) || ($ele.height()>viewHeight)){
                                $ele.css({
                                    width: 100 + '%',
                                    height:100 + '%'
                                });
                            }else{
                                $ele.css({
                                    width: $ele.width() * 2 + 'px'
                                });
                            }
                        }else{
                            $ele.on('load', function () {
                                if(($ele.width()>viewWidth) || ($ele.height()>viewHeight)){
                                    $ele.css({
                                        width: 100 + '%',
                                        height:100 + '%'
                                    });
                                }else{
                                    $ele.css({
                                        width: $ele.width() * 2 + 'px'
                                    });
                                }
                            });
                        }
                    }else{
                        if ($ele.width() > 0) {
                            $ele.css({
                                width: $ele.width() * 2 + 'px'
                            });
                        } else {
                            $ele.on('load', function () {
                                $ele.css({
                                    width: $ele.width() * 2 + 'px'
                                });
                            });
                        }
                    }
                });
            }, 0);
        });

        /// 根据当前数字显示五角星
        function viewStar(num) {
            var str = '';
            for (var i = 0; i < num; i++) {
                str = str + '<span class="fa fa-star white"></span>'
            }
            return str;
        }
    }]
});