/**
 * Created by 赵文东 on 2017/3/2.
 */
define([],function () {
    return ['$q', '$rootScope','$scope','$http','constantService','ngDialog','$location','$state',function ($q, $rootScope, $scope, $http, constantService,ngDialog,$location,$state) {
        var unifiedid = $location.search().unifiedid;
        var examfid = $location.search().examfid;
        $scope.examname = $location.search().examname;
        $scope.examtime = $location.search().time;
        $scope.marktype = $location.search().marktype;
        $scope.subject = $location.search().subject;
        $scope.examid = $location.search().examid;
        var gradenum = $location.search().gradenum;
        var subjectid = $location.search().subjectid;
        $scope.submitnum = $location.search().submitnum;
        $scope.unifedname = $location.search().unifedname;
        $scope.currentExamDetail = undefined;
        var currentUser = angular.fromJson(sessionStorage.getItem('currentUser'));
        var schoolfid = currentUser.schoolFId;
        $scope.selectContentList = [];
        $scope.allQstList = [];
        $scope.currentPaperQstGroupList = [];
        $scope.teacherSubjectList = [];
        $scope.teacherGradeList = [];
        var allQstWithOutFilter = [];
        var respTeacherList = [];
        // 批阅展示方式 1：一页一题 2：多题在一页批阅（分组中每题单独给分） 3：多题在一页批阅（多题共用一个给分框）
        $scope.markingShowType = '1';//1：一页一题 21：多题在一页批阅（分组中每题单独给分） 22：多题在一页批阅（多题共用一个给分框）
        //isCut 是否已经切图
        $scope.isCut = 0;
        //切图是否已经完成
        $scope.isFinish = 0;
        //存储切图试卷信息
        var cutPaper = null;
        //applySta 1：全都未应用 2：应用部分 3：已全应用
        $scope.applySta = '1';

        $http.post($rootScope.baseUrl + '/Interface0181.ashx', {
            examId: examfid
        }).then(function(res){
            // 过滤筛选出选择题 非选择题
            $scope.paperQstGroupDetial = getQTypeScoreFromPaper(res.data);
        });
        /**
         * 获取该学校下的统考数据 过滤出当前试卷信息
         */
        getExamList(schoolfid);

        //获取试卷切图状态
        searchTask();

        /*$http.post($rootScope.baseUrl + '/Interface0222.ashx').then(function(res){
            if(_.isArray(res.data.msg)) {
                $scope.schoolList = res.data.msg;
                $scope.filterSchool = $scope.schoolList[0];
            }else {
                $scope.schoolList = [];
                $scope.filterSchool = $scope.schoolList[0];
            }
        });*/

        function getExamList(schoolfid){
            $http.post('' + '/BootStrap/EncryptUnified/GetEncryptEList.ashx', {
                schoolFId:schoolfid
            }).then(function(res){
                console.log(res);
                $scope.currentExamDetail = _.find(res.data.msg,function (item) {
                    return item.examFId == examfid;
                });
                /*if(_.isArray(res.data.msg)) {
                    $scope.examList = res.data.msg;
                    $scope.filterExam = $scope.examList[0];
                }else {
                    $scope.examList = [];
                    $scope.filterExam = $scope.examList[0];
                }*/
            }, function(){
                /*$scope.examList = [];
                $scope.filterExam = $scope.examList[0];*/
                constantService.alert('获取试卷信息失败!');
            });
        }

        $scope.setEncryptExam = function(){
            $http.post('' + '/BootStrap/EncryptUnified/ChangeIsEncryptByFId.ashx', {
                uiFId: unifiedid,
                isEncrypt: $scope.currentExamDetail.isEncrypt === '0' ? '1' : '0'
            }).then(function(res){
                if(res.data.code === 0) {
                    $scope.currentExamDetail.isEncrypt = $scope.currentExamDetail.isEncrypt === '0' ? '1' : '0';
                    constantService.showSimpleToast('设置成功！');
                    if($scope.currentExamDetail.isEncrypt === '0'){
                        $scope.currentPaperQstGroupList = [];
                        $scope.allQstList = [];
                    }
                }else {
                    constantService.showSimpleToast('操作失败！');
                }
            });
        };
        //监听当前是否是机密批阅
        $scope.$watch('currentExamDetail.isEncrypt',function (newVal,oldVal) {
            if((newVal ==1 && oldVal == undefined) ||(oldVal == 0 && newVal == 1)){
                $scope.getTeachers();
                getDetailByUnId();
            }
        });

        $scope.getTeachers = function(){
            $http.post('' + '/BootStrap/EncryptUnified/GetTUserBySE.ashx', {
                schoolFId: schoolfid,
                examFId: examfid
            }).then(function(res){
                if(_.isArray(res.data.msg)) {
                    respTeacherList = res.data.msg;
                    $scope.teacherList = removeDuplicate(res.data.msg);
                    _.each($scope.teacherList,function (item) {
                        var temp = null;
                        if(!!item.GradeNum){
                            temp = _.find(constantService.GRADE_LIST,function (i) {
                                return +i.id === item.GradeNum
                            });
                            item.gradename = !!temp?temp.name:'其他';
                        }else{
                            item.gradename = '其他';
                        }
                        //将不执教老师填充为 '未执教学科'
                        if(item.subjectName === null || item.subjectName === undefined){
                            item.subjectName = '未执教学科';
                        }
                    });
                    $scope.teacherSubjectList = getSubjectList4Teacher();
                    $scope.teacherGradeList = getGradeList4Teacher();
                }else {
                    $scope.teacherList = [];
                }
            }, function(){
                $scope.teacherList = [];
            });
        };

        /*$scope.getQsts = function(){
            $http.post('' + '/BootStrap/EncryptUnified/GetQListByEUnifiedItem.ashx', {
                uiFId: $scope.filterExam.uiFId,
                userFId: $scope.currentTeacher.userFId
            }).then(function(res){
                if(_.isArray(res.data.msg)) {
                    $scope.questionList = _.map(res.data.msg, function(qst){
                        return {
                            qFId: qst.qFId,
                            isAuth: qst.isAuth === '1',
                            order: qst.order
                        };
                    });
                }else {
                    $scope.questionList = [];
                }
            })
        };*/

        /*$scope.saveSetting = function(){
            var qIds = _.pluck(_.filter($scope.questionList, function(q){
                return !!q.isAuth;
            }), 'qFId').join(',');
            $http.post('' + '/BootStrap/EncryptUnified/AddEncryptUnified.ashx', {
                uiFId: $scope.filterExam.uiFId,
                userFId: $scope.currentTeacher.userFId,
                qFId: qIds
            }).then(function(res){
                if(res.data.code === 0) {
                    constantService.showSimpleToast('保存成功！');
                }else {
                    constantService.showSimpleToast('操作失败，请重试！');
                }
            })
        };*/

        /*$scope.$watch('filterExam', function(val, oldVal){
            if(val && val.isEncrypt === '1') {
                $scope.getTeachers();
                $scope.selectContentList = [];
                getDetailByUnId();
            }else {
                $scope.currentPaperQstGroupList = [];
                $scope.allQstList = [];
            }
        },true);*/

        /*$scope.$watch('currentTeacher', function(val){
            if(val) {
                $scope.getQsts();
            }else {
                $scope.questionList = [];
            }
        }, true);*/

        function getDetailByUnId() {
            $http.post(''+'/BootStrap/EncryptUnified/WJFGetEncryptQList.ashx', {
                unifid : unifiedid,
                getall:'1'
            }).then(function(res){
                if(_.isArray(res.data.msg)&&res.data.msg.length>0){
                    res.data.msg = removeDtypeFromGroupname(res.data.msg);
                    var handledlist = handleOrginShowType(res.data.msg);
                    allQstWithOutFilter = handledlist;
                    $scope.allQstList = handledlist;//uniqByShowDis(handledlist);
                    $scope.currentPaperQstGroupList = groupedQst($scope.allQstList);
                }else{
                    $scope.currentPaperQstGroupList = [];
                    $scope.allQstList = [];
                }
            },function (res) {
                $scope.currentPaperQstGroupList = [];
                $scope.allQstList = [];
            });
        }

        $scope.checkSelectConten = function (qstitem) {
            if($scope.isContentSelected(qstitem)){
                var index = _.findIndex($scope.selectContentList, function(item){
                    return item.qfinkid === qstitem.qfinkid;
                });
                $scope.selectContentList.splice(index, 1);
            }else{
                if(!$scope.selectContentList.length>0){
                    $scope.selectContentList.push(qstitem);
                }else if(checkDtype()[0] === qstitem.dtype){
                    $scope.selectContentList.push(qstitem);
                }else {
                    constantService.showSimpleToast('只能对同组题目合并批阅');
                }

            }
        };
        $scope.isContentSelected = function (qstitem) {
            return !!_.find($scope.selectContentList,function (item) {
                return item.qfinkid === qstitem.qfinkid
            })
        };
        $scope.showSetWin = function () {
            if($scope.selectContentList.length<=0){
                constantService.showSimpleToast('请先选择题目!');
                return;
            }
            var dialog = ngDialog.open({
                template:'<div class="assignTeacherContent">' +
                '<div class="assingnedQstTitle">第<span ng-repeat="qstno in $parent.selectContentList" ng-bind="qstno.showDis+($last?\'\':\',\')"></span>题分配阅卷老师</div>' +
                '<div class="subjectsArea"><label ng-repeat="tecsub in $parent.teacherSubjectList" ng-click="checkSelectTecSub(tecsub)" style="padding: 0px;float: none;margin-bottom: 0px;"><div class="checkSubContent" ng-class="{\'selectSubContent\':isTecSubSelected(tecsub)}" style="float: left;"><div class="checkSubFlag" style="width: 18px;height: 18px;"></div></div><div ng-bind="tecsub" style="float: left;margin-left: 7px;line-height: 18px;margin-right: 9px;"></div></label></div>' +
                '<div ng-show="selectedTecSub.length === 0" style="padding: 10px 10px 10px 35px"><span style="font-size: 12px;color: #dd4b39;">请选择科目!</span></div>' +
                '<div class="teacher-grade">' +
                '<div class="gradeteacherlist" ng-repeat="tecgrade in tecGroupByGrade">' +
                '<label ng-click="isGroupShow($index)" class="teacherGradeTitle" style="margin-bottom: 0px;"><span ng-bind="tecgrade.gradename + \'老师\'" style="margin-left: 20px"></span><i class="fa fa-angle-up" ng-class="{\'fa-angle-down\':!tecgrade.isShow,\'fa-angle-up\':tecgrade.isShow}" style="margin-left: 30px;"></i></label>' +
                '<div class="teachernamelist" ng-show="tecgrade.isShow">' +
                '<label ng-repeat="tecitem in tecgrade.teacherlist" ng-click="checkSelectTecUser(tecitem)" style="padding: 0px;margin-top: 10px;margin-bottom:0px;float: none;"><div class="checkTecContent" ng-class="{\'selectTecContent\':isTecUserSelected(tecitem)}" style="float: left;"><div class="selectflag-teacher" style="width: 18px;height: 18px;"></div></div><div ng-bind="tecitem.UserName" style="float: left;margin-left: 7px;line-height: 18px;margin-right: 9px;"></div></label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="choiceShowMode" ng-show="selectTecUserList.length>0"><span>批阅方式:</span><label style="font-weight: normal;margin:0px 20px;"><input type="radio" name="mode" ng-model="showMode" ng-value="1">一页一题批阅(默认)</label><label style="font-weight: normal;margin:0px 20px;"><input type="radio" name="mode" ng-model="showMode" ng-value="21">多题在同一界面批阅(每题单独给分)</label></div>' +
                '<div class="row" style="margin: 0px;text-align: center;padding: 10px;"><button style="width: 100px;height: 35px;border: none;background-color: #FEA002;border-radius: 6px;color: #FFFFFF;" ng-click="updateAssigedTec()">确认</button></div>' +
                '</div>',
                className: 'ngdialog-theme-default',
                appendClassName:'encryptmarkConfig-assiginteacher',
                plain: true,
                scope:$scope,
                closeByDocument:false,
                controller:function ($scope) {
                    return function () {
                        $scope.selectedTecSub = [];
                        $scope.tecGroupBySub = [];
                        $scope.tecGroupBySub = [];
                        $scope.tecGroupByGrade = [];
                        $scope.selectTecUserList = [];
                        $scope.showMode = 1;
                        putOiriginSelectTec();
                        $scope.checkSelectTecSub = function (item) {
                            if($scope.isTecSubSelected(item)){
                                var index = _.findIndex($scope.selectedTecSub, function(i){
                                    return i === item;
                                });
                                $scope.selectedTecSub.splice(index, 1);
                            }else{
                                $scope.selectedTecSub.push(item);
                            }
                            selectedSubChanged();
                        };
                        $scope.isTecSubSelected = function (item) {
                            return !!_.find($scope.selectedTecSub,function (i) {
                                return i === item;
                            });
                        };

                        function selectedSubChanged() {
                            $scope.tecGroupBySub = [];
                            $scope.tecGroupByGrade = [];
                            _.each($scope.selectedTecSub,function (item) {
                                $scope.tecGroupBySub = $scope.tecGroupBySub.concat(_.filter($scope.teacherList,function (i) {
                                    return i.subjectName === item;
                                }));
                            });
                            _.each($scope.teacherGradeList,function (h,index) {
                                var temp = {isShow:false,teacherlist:[],gradename:''};
                                temp.isShow = true;//(index === 0? true:false);
                                temp.gradename = h;
                                temp.teacherlist = _.filter($scope.tecGroupBySub,function (n) {
                                    return h === n.gradename;
                                });
                                $scope.tecGroupByGrade.push(temp);
                            });
                        }
                        $scope.isGroupShow = function (index) {
                            $scope.tecGroupByGrade[index].isShow = !$scope.tecGroupByGrade[index].isShow;
                        };
                        $scope.checkSelectTecUser = function (tecitem) {
                            if($scope.isTecUserSelected(tecitem)){
                                var index = _.findIndex($scope.selectTecUserList, function(i){
                                    return i === tecitem;
                                });
                                $scope.selectTecUserList.splice(index, 1);
                            }else{
                                $scope.selectTecUserList.push(tecitem);
                            }
                        };
                        $scope.isTecUserSelected = function (tecitem) {
                            return !!_.find($scope.selectTecUserList,function (i) {
                                return tecitem === i;
                            });
                        };
                        function putOiriginSelectTec() {
                            _.each($scope.selectContentList,function (item) {
                                _.each(item.finish,function (i) {
                                    var temp = _.find($scope.teacherList,function (h) {
                                        return h.FLnkID === i.UserFId
                                    });
                                    if(!!temp){
                                        $scope.selectTecUserList.push(temp);
                                        $scope.selectedTecSub.push(temp.subjectName);
                                        $scope.selectedTecSub = _.uniq($scope.selectedTecSub);
                                    }
                                });
                            });
                            $scope.selectTecUserList = _.uniq($scope.selectTecUserList);
                            selectedSubChanged();
                        }
                        $scope.updateAssigedTec = function () {
                            if($scope.showMode === 21){
                                var reqfid = [];
                                _.each($scope.$parent.selectContentList,function (item) {
                                    var temp = _.filter($scope.allQstList,function (s) {
                                        return s.dtype === item.dtype && item.showDis === s.showDis;
                                    });
                                    if(!!temp && temp.length> 0){
                                        _.each(temp,function (t) {
                                            if(+t.allCount - +t.noFinishCount > 0){
                                                reqfid.push(t.qfinkid);
                                            }
                                        });
                                    }
                                });
                                if(reqfid.length>0){
                                    constantService.confirm('提示','您所选的题目中存在已有批阅记录的题目，若选择多题在同一界面批阅方式，需要将已有批阅记录重置，是否重置?',['重置','取消'],function () {
                                        var reqlist = [];
                                        _.each(reqfid,function (n) {
                                            reqlist.push($http.post('/BootStrap/schoolmanager/resetQuestionMarkRecord.ashx', {
                                                unifiedId: unifiedid,
                                                qFlnkId: n
                                            }));
                                        });
                                        $q.all(reqlist).then(function (resp) {
                                            var flg = 0;
                                            for(var i = 0; i < resp.length; i++){
                                                if(resp[i].data.code === 0){
                                                    flg = flg + 1;
                                                }
                                            }
                                            if(flg === resp.length){
                                                removeRecord4List(reqfid,$scope.$parent.allQstList);
                                                removeRecord4List(reqfid,$scope.$parent.selectContentList);
                                                applyAssiginChange($scope.showMode,$scope.selectTecUserList);
                                                dialog.close();
                                            }else{
                                                constantService.alert('重置失败，请稍后再试!');
                                            }
                                        },function () {
                                            constantService.alert('重置失败，请稍后再试!');
                                        });
                                    });
                                }else{
                                    applyAssiginChange($scope.showMode,$scope.selectTecUserList);
                                    dialog.close();
                                }
                            }else{
                                applyAssiginChange($scope.showMode,$scope.selectTecUserList);
                                dialog.close();
                            }
                        }
                    }
                }
            });
        };
        function getSubjectList4Teacher() {
            var templist = [];
            _.each($scope.teacherList,function (item) {
                templist.push(item.subjectName);
            });
            return _.uniq(templist);
        }
        function getGradeList4Teacher() {
            var templist = [];
            _.each($scope.teacherList,function (item) {
                templist.push(item.gradename);
            });
            return _.uniq(templist);
        }
        $scope.showSaveWin = function () {
            var assignMap = [];
            _.each(allQstWithOutFilter,function (q) {
                _.each($scope.allQstList,function (item) {
                    if(q.showDis === item.showDis && q.dtype === item.dtype){
                        var tempteaList = [];
                        _.each(item.finish,function (h) {
                            tempteaList.push({UserName:h.UserName,UserFId:h.UserFId});
                        });
                        assignMap.push({showDis:item.showDis,dis:q.dis,qfinkid:q.qfinkid,teacherlist:tempteaList,groupname:item.groupname + '$*'+item.dtype,markingShowType:!!item.markingShowType?(item.markingShowType+''):'1',fastInputStr:item.fastInputStr || ''})
                    }
                });
            });
            //给assignMap数组去重
            var ret = [];
            var tempqFlnkid = [];
            _.each(assignMap,function (item) {
                tempqFlnkid.push(item.qfinkid);
            });
            _.each(tempqFlnkid,function (item,index) {
                if( _.indexOf(tempqFlnkid,item) === index){
                    ret.push(index);
                }
            });
            var tempMap = [];
            _.each(ret,function (item) {
                tempMap.push(assignMap[item]);
            });
            assignMap = tempMap;
            var req={
                uiFId:unifiedid,
                assignMap:assignMap
            };
            saveAssigin(req).then(function (res) {
                if(res.data.code === 0){
                    // $http.post('' + '/BootStrap/schoolmanager/updateUnifiedItemStep.ashx', {//设置子统考
                    //     unifiedItemId: unifiedid,
                    //     step: '3'
                    // }).then(function (re) {
                    //     if (re.data.code === 0) {
                            constantService.showSimpleToast('设置成功!');
                            $scope.selectContentList = [];
                        // /} else {
                        //     constantService.alert(re.data.msg);
                        // }
                    // });
                }else {
                    constantService.showSimpleToast(res.data.msg);
                }
            },function (res) {
                constantService.showSimpleToast('设置失败!');
            });
        };
        $scope.calWidth = function (item) {
            return {width:Math.floor(((+item.allCount - (+item.noFinishCount))/(+item.allCount))*100)+'%'};
        };
        function removeDuplicate(arr) {
            var templist = [];
            _.each(arr,function (item) {
                templist.push(item.FLnkID);
            });
            templist = _.uniq(templist);
            var afterUniq = [];
            _.each(templist,function (i) {
                var temp = _.find(arr,function (h) {
                    return h.FLnkID === i;
                });
                if(!!temp){
                    afterUniq.push(temp);
                }
            });
            return afterUniq;
        }
        function groupedQst(arr) {
            var tempgrouplist = [];
            var grouplist = [];
            _.each(arr,function (item) {
                item.groupname = !!item.groupname?item.groupname:item.showDis;
                tempgrouplist.push({groupname:item.groupname,dtype:item.dtype});
            });
            //tempgrouplist = _.uniq(tempgrouplist);
            tempgrouplist = uniq4GroupBykey(tempgrouplist);
            for(var i = 0;i<tempgrouplist.length;i++){
                for(var j = i+1;j<tempgrouplist.length;j++){
                    var a = _.sortBy(tempgrouplist[i].groupname.split(','))[0];
                    var b = _.sortBy(tempgrouplist[j].groupname.split(','))[0];
                    if(tempgrouplist[i].dtype === tempgrouplist[j].dtype && +a > +b){
                        var c = tempgrouplist[i];
                        tempgrouplist[i] = tempgrouplist[j];
                        tempgrouplist[j] = c;
                    }
                }
            }
            _.each(tempgrouplist,function (i) {
                var temp = _.filter(arr,function (h) {
                    return i.groupname === h.groupname && h.dtype === i.dtype;
                });
                var tShowDisList = [];
                _.each(temp,function (w) {
                    tShowDisList.push(w.showDis);
                });
                var resultlist = [];
                _.each(tShowDisList,function(k,index) {
                    if (tShowDisList.indexOf(k) === index) {
                        resultlist.push(temp[index]);
                    }
                });
                grouplist.push({groupname:i,qstlist:resultlist})
            });
            _.each(grouplist,function (n) {
                var tempteclist = [];
                _.each(n.qstlist[0].finish,function (q) {
                    tempteclist.push({UserName:q.UserName,ufinish:0})
                });
                _.each(n.qstlist,function (m) {
                    if(_.isArray(m.finish) && m.finish.length>0){
                        _.each(tempteclist,function (s) {
                            var tep = _.find(m.finish,function (r) {
                                return r.UserName === s.UserName;
                            });
                            if(!!tep){
                                s.ufinish = s.ufinish + (+tep.ufinish);
                            }
                        });
                    }
                });
                n.tecUfinishList = tempteclist;
            });
            return grouplist;
        }
        function applyAssiginChange(showMode,teacherlist) {
            var newGroupname = '';
            if(showMode != 1){
                var tempname = [];
                _.each($scope.selectContentList,function (item) {
                    tempname.push(item.showDis);
                });
                tempname = _.sortBy(tempname);
                newGroupname = tempname.join(',');
            }

            _.each($scope.selectContentList,function (item) {
                //将编辑的题目全部从其他分组名中剔除出来
                _.each($scope.allQstList,function (i) {
                    var tempgrouplist = i.groupname.split(',');
                    for(var n = 0;n<tempgrouplist.length;n++){
                        if(item.dtype === i.dtype && tempgrouplist[n] === item.showDis && tempgrouplist.length > 1){
                            tempgrouplist.splice(n,1);
                        }
                    }
                    i.groupname = tempgrouplist.join(',');
                });
            });
            _.each($scope.selectContentList,function (item) {
                _.each($scope.allQstList,function (i) {
                    //if(item.qfinkid === i.qfinkid){
                    if(item.dtype === i.dtype && item.showDis === i.showDis){
                        i.groupname = showMode != 1?newGroupname:i.showDis;
                        i.markingShowType = showMode;
                        var tempTecList = i.finish;
                        i.finish = [];
                        _.each(teacherlist,function (h) {
                            var findUser = _.find(tempTecList,function (n) {
                                return n.UserFId === h.FLnkID;
                            });
                            i.finish.push(!!findUser?findUser:{UserFId:h.FLnkID,QFLnkId:i.qfinkid,UserName:h.UserName,ufinish:0})
                        });
                    }
                });
            });
            //刷新页面数据
            $scope.currentPaperQstGroupList = groupedQst($scope.allQstList);
            $scope.selectContentList = [];
        }
        function saveAssigin(req) {
            var defer = $q.defer();
            $http.post(''+'/BootStrap/EncryptUnified/saveEncryptUnifiedAssignedList.ashx',req).then(function(res){
                defer.resolve(res);
            }, function(res){
                defer.reject(res)
            });
            return defer.promise;
        }
        /*$scope.getPaperByNo = function () {
            if(!$scope.searchKey){
                constantService.showSimpleToast('请输入试卷号!');
                return;
            }
            var findRe = _.filter($scope.examList,function (item) {
                return item.eFid === $scope.searchKey;
            });
            if(findRe.length>0){
                $scope.filterExam = findRe[0];
            }else{
                constantService.showSimpleToast('宝宝很努力了,但是还是没有查询到!');
            }
        };*/
        function checkDtype() {
            var tempDtypelist = _.pluck($scope.selectContentList,'dtype');
            tempDtypelist=_.uniq(tempDtypelist);
            return tempDtypelist;
        }
        function uniqByShowDis(list) {
            var repeatindex = [];
            var norepeat = [];
            _.each(list,function (item,index) {
                _.each(list,function (i,j) {
                    if(index === j){
                        return true;
                    }else if(item.dtype === i.dtype && item.showDis === i.showDis && j > index){
                        repeatindex.push(j);
                    }
                });
            });
            _.each(list,function (m,n) {
                var p = _.find(repeatindex,function (r) {
                    return n === r;
                });
                if(p === 0 || !!p){
                    return true;
                }else{
                    norepeat.push(m);
                }
            });
            return norepeat;
        }
        function handleOrginShowType(list) {
            _.each(list,function (item) {
                item.markingShowType = !!item.showType?item.showType:'1';
            });
            return list;
        }
        function getQTypeScoreFromPaper(paper) {
            var totalscore = 0;
            var list = [];
            _.each(paper.msg,function (item) {
                list.push({name:item.QTypeName,num:item.QNumber,score:item.QTypeScores});
                totalscore += (+item.QTypeScores)
            });
            list.push({name:'总分值',num:'',score:totalscore});
            return list;
        }
        $scope.goMarkRecord = function () {
            $state.go('myApp.markPaperRecord',{
                unifiedid:unifiedid
            });
        };
        $scope.thispath = $location.path();
        $scope.checkPaper = function () {
            window.open('#/composing?pathss=' + $scope.thispath + '&ExamFlnkID=' + examfid + '&action=0&isExamCenter=1');
            // $location.path('/composing').search({
            //     'pathss': $scope.thispath,
            //     'ExamFlnkID': examfid,
            //     'action': 0,
            //     'isExamCenter':1
            // });
        };
        $scope.goPaperCut = function () {
            if((+$scope.isCut !== 0 && +$scope.isFinish !== 0) && $scope.applySta < 3){
                constantService.confirm('提示','还有班级未应用切图是否全部应用?',['应用','重新切图'],function () {
                    //TODO调用应用切图接口
                    $http.post('' + '/BootStrap/schoolmanager/applyCutPaperData.ashx',{
                        unifiedId:unifiedid,
                        cutId:cutPaper.CutId
                    }).then(function (res) {
                        if(res.data.code == 0){
                            constantService.alert('应用成功!');
                            //刷新页面状态
                            searchTask();
                        }else{
                            constantService.alert('应用失败!');
                        }
                    },function (res) {
                        constantService.alert('应用失败!');
                    });
                },function () {
                    $state.go('paperCutUnified', {
                        taskId: cutPaper.FLnkID,
                        examId: cutPaper.ExExamFLnkID,
                        unifiedId: unifiedid,
                        action: $scope.isCut === 0 ? 'add' : ($scope.isFinish === 0 ? 'edit' : 'add')
                    });
                })
            }else{
                $state.go('paperCutUnified', {
                    taskId: cutPaper.FLnkID,
                    examId: cutPaper.ExExamFLnkID,
                    unifiedId: unifiedid,
                    action: $scope.isCut === 0 ? 'add' : ($scope.isFinish === 0 ? 'edit' : 'add')
                });
            }
        };
        $scope.checkPaperCut = function (task) {
            $state.go('paperCutUnified', {
                taskId: cutPaper.FLnkID,
                examId: cutPaper.ExExamFLnkID,
                unifiedId: unifiedid,
                action: 'view'
            });
        };
        //TODO 获取试卷的切图信息
        function searchTask() {
            $http.post($rootScope.baseUrl + '/generalQuery.ashx',{
                UnifiedItemFId:unifiedid,
                Proc_name :'Proc_GetUnifiedTasks'
            }).then(function (res) {
                if(res.data.msg.length > 0){
                    var noapply = _.countBy(res.data.msg,function (item) {
                        return (item.CutId === null || item.CutId === undefined)?'Y':'N';
                    });
                    if(noapply.N === res.data.msg.length){
                        $scope.applySta = '3';
                    }else if(noapply.Y === res.data.msg.length){
                        $scope.applySta = '1'
                    }else {
                        $scope.applySta = '2';
                    }
                    cutPaper = res.data.msg[0];
                    $http.post($rootScope.baseUrl + '/Interface0250.ashx', {
                        schoolFLnkID: schoolfid,
                        gradeNum: gradenum || '',
                        subjectID: subjectid || '',
                        examName: $scope.examname || ''
                    }).then(function (resg) {
                        if (_.isArray(resg.data.msg)) {
                            /*var tempTask = _.filter(resg.data.msg,function (item) {
                                return cutPaper.ExExamFLnkID === item.examFlnkID;
                            });*/
                            var tempTask = _.filter(resg.data.msg,function (item) {
                                return cutPaper.FLnkID === item.taskFLnkID;
                            });
                            $scope.isCut = +tempTask[0].isCut;
                            $scope.isFinish = +tempTask[0].isFinish;
                        }
                    });
                }
            });
        }
        function uniq4GroupBykey(list) {
            var typeList = [];
            var finallist = [];
            _.each(list,function (item) {
                typeList.push(item.dtype);
            });
            typeList = _.uniq(typeList);
            _.each(typeList,function (item) {
                var templist = _.filter(list,function (i) {
                    return i.dtype === item;
                });
                var temnamelist = [];
                _.each(templist,function (j) {
                    temnamelist.push(j.groupname);
                });
                var resultlist = [];
                _.each(temnamelist,function(k,index) {
                    if (temnamelist.indexOf(k) === index) {
                        resultlist.push(templist[index]);
                    }
                });
                finallist = finallist.concat(resultlist);
            });
            return finallist;
        }
        $scope.editFastInput = function (e,qstitem) {
            e.preventDefault();
            e.stopPropagation();
            var dialog_editFastInput = ngDialog.open({
                template:'<div class="editFastInput-title">设置分数</div><div class="editFastInput-container">' +
                '<div class="tip-input-infront">输入分数将用于快捷打分，输入的数字请用空格隔开，且每个分数仅能为<span style="color: #ac2925">0.5</span>的整数倍</div>' +
                '<div class="tip-score-infront">本题分数<span style="color: #ac2925" ng-bind="qstScore"></span>分，请在合理范围内设置分值.</div>' +
                '<div class="input-area"><input class="score-input" type="text" ng-model="scores"></div>' +
                '<div class="btn-group"><div class="btn abolish-btn" ng-click="closeInput()">取消</div><div class="btn confirm-btn" ng-click="confirmInput()">确定</div></div>' +
                '</div>',
                className: 'ngdialog-theme-default',
                appendClassName:'encryptmarkConfig-editFastInput',
                plain: true,
                scope:$scope,
                closeByDocument:false,
                controller:function ($scope) {
                    return function () {
                        $scope.scores = qstitem.fastInputStr;
                        $scope.qstScore = qstitem.score;
                        $scope.closeInput = function () {
                            dialog_editFastInput.close();
                        };
                        $scope.confirmInput = function () {
                            if(!!$scope.scores || ($scope.scores == 0)){
                                $scope.scores = dbc2sbc($scope.scores);
                                $scope.scores = trimLAndR($scope.scores);
                                var scorelist = $scope.scores.split(' ');
                                var onlyNum = /^(\d+(\.\d{1,2})?)?$/;
                                for(var i = 0; i < scorelist.length; i++){
                                    if(!onlyNum.test(scorelist[i])){
                                        constantService.showSimpleToast('仅能输入数字，小数点后仅能保留一位!');
                                        return;
                                    }
                                    if (+scorelist[i] < 0 || +scorelist[i] > +qstitem.score || +scorelist[i] * 10 % 5 !== 0) {
                                        constantService.showSimpleToast('分数必须为0.5的整数倍，且不能大于本题总分值！');
                                        return ;
                                    }
                                }
                            }
                            qstitem.fastInputStr = scorelist.join(' ');
                            dialog_editFastInput.close();
                        }
                    }
                }
            });
        };
        function dbc2sbc(str) {
            return str.replace(/[\uff01-\uff5e]/g,function(a){return String.fromCharCode(a.charCodeAt(0)-65248);}).replace(/\u3000/g," ");
        }
        function trimLAndR(s){
            return s.replace(/(^\s*)|(\s*$)/g, "");
        }
        $scope.useExistCut = function () {
            $http.post($rootScope.baseUrl + '/Interface0226.ashx', {
                schoolFLnkId: schoolfid,
                examFLnkId: examfid
            }).then(function (res) {
                if (_.isArray(res.data.msg)) {
                    $scope.cutList = res.data.msg;
                    showCutListDialog();
                } else {
                    constantService.showSimpleToast('暂无切图数据！');
                }
            })
        };
        function showCutListDialog() {
            var dialog = ngDialog.open({
                template: 'cutList.html',
                className: 'ngdialog-theme-default',
                appendClassName: 'cut-list-dialog',
                closeByDocument: true,
                scope: $scope,
                controller: function ($scope, $http) {
                    return function () {
                        $scope.useCut = function (cut) {
                            $http.post($rootScope.baseUrl + '/Interface0227.ashx', {
                                cutId: cut.cutFLnkId,
                                taskId: cutPaper.FLnkID
                            }).then(function (res) {
                                if (+res.data.code === 0) {
                                    ngDialog.close(dialog.id);
                                    constantService.showSimpleToast('该任务已使用切图《' + cut.cutName + '》！');
                                    searchTask();
                                } else {
                                    constantService.showSimpleToast('使用切图失败！');
                                }
                            });
                        };
                    }
                }
            });
        }
        //去除groupname中的的dtype
        function removeDtypeFromGroupname(list) {
            _.each(list,function (item) {
                if(!!item.groupname){
                    item.groupname = item.groupname.split('$*')[0];
                }
            });
            return list;
        }
        $scope.goTop = function () {
            $(document).scrollTop(0)
        };
        //清除批阅记录后 清除scope中list列表中的批阅记录数据
        function removeRecord4List(list,aimlist) {
            _.each(list,function (item) {
                var tempele = _.findIndex(aimlist,function (m) {
                    return m.qfinkid === item;
                });
                if(tempele != -1){
                    aimlist[tempele].noFinishCount = aimlist[tempele].allCount;
                    if(!!aimlist[tempele].finish && aimlist[tempele].finish.length>0){
                        _.each(aimlist[tempele].finish,function (n) {
                            n.ufinish = 0;
                        });
                    }
                }
            });
            return aimlist;
        }
    }];
});